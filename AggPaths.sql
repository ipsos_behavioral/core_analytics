use Samsung

SELECT TOP 100 * FROM dbo.SequenceInput_whitelist

DROP TABLE #Users

SELECT DISTINCT UserID INTO #Users FROM (
SELECT * FROM dbo.UserQualificationMobile WHERE DailyQualified = '1' UNION 
SELECT * FROM dbo.UserQualificationPC WHERE DailyQualified = '1')w

DROP TABLE #Cleandays

 SELECT UserID, DayID, PurchaseStage INTO #CleanDays FROM(
 SELECT * FROM dbo.UserQualificationMobile
 UNION
 SELECT * FROM dbo.UserQualificationPC
 )a
 GROUP BY UserID, DayID, PurchaseStage

SELECT * FROM #Cleandays WHERE UserID = 7497410


DROP TABLE #AggPaths

SELECT ROW_NUMBER() OVER (ORDER BY [Panelist_ID], actionday,StartTime ASC) AS RowNum, Device,[Panelist_ID],actionday,StartTime,EndTime,category, 
CASE b.type 
	WHEN '' THEN 'END'
	WHEN 'SEARCH' THEN CAST(b.subcategory AS varchar) + ' Search'
	ELSE CAST(b.subcategory AS varchar)
	END AS subcategory, Property,
CASE b.type
	WHEN 'APP' THEN b.Type
	ELSE 'WEB'
	END AS Digital_Type, b.PurchaseStage
	INTO #AggPaths
FROM
(
SELECT Device,[Panelist_ID],CAST(w.DateTime AS date) AS actionday, CAST(MIN(w.DateTime) AS DateTime) AS StartTime, 
CAST(MAX(w.DateTime) AS DateTime) AS EndTime,Property, category, 
CASE subcategory WHEN '' THEN category ELSE subcategory END AS subcategory,[type], PurchaseStage from 
(
SELECT si.*, CASE type
	WHEN 'SEARCH' THEN Notes + ' (' + Value + ')'
	ELSE Notes
	END AS Property,category,subcategory FROM dbo.SequenceInput_whitelist si 
LEFT JOIN dbo.[TaxonomyMasterList_Custom] t ON si.TaxID_Whitelist=t.taxid
) w
GROUP BY Device,[Panelist_ID],Property,category,subcategory,[type],w.DateTime,PurchaseStage
UNION
SELECT '' AS Device,[Panelist_ID],DATEADD(day,1,MAX(DayID)) AS actionday, '' AS StartTime, '' AS EndTime, 'END' AS Property,'END' AS category, 'END' AS subcategory, '' AS type, NULL AS PurchaseStage
FROM dbo.SequenceInput_Whitelist siw
GROUP BY [Panelist_ID]
)b
INNER JOIN #Users u ON b.[Panelist_ID] = u.UserID 
LEFT JOIN #CleanDays cd ON b.[Panelist_ID] = cd.UserID AND b.actionday = cd.DayID AND b.PurchaseStage= cd.PurchaseStage
ORDER BY [Panelist_ID], actionday,StartTime

SELECT * FROM #AggPaths WHERE PurchaseStage IS NULL

DROP TABLE #AggPathsEdited

SELECT ROW_NUMBER() OVER (ORDER BY [Panelist_ID], actionday,StartTime ASC) AS RowNum2,* INTO #AggPathsEdited 
FROM #AggPaths

SELECT * FROM #AggPathsEdited

--Device, DigitalType,Detail, Cat, SubCat,Date,StartTime,EndTime, Prev_Detail, Prev_Cat,Prev_Subcat, Prev_Date,Prev_End

DROP TABLE #AggPathsFinal

SELECT a1.RowNum2 AS RowNum, 
a1.[Panelist_ID] AS Panelist_ID, 
a1.Device AS Device, 
a1.Digital_Type AS Digital_Type,
a1.Property AS Property,
a1.category AS Category,
a1.subcategory AS Subcategory,
a1.actionday AS actionday,
CAST(a1.StartTime AS Time(0)) AS StartTime,
CAST(a1.EndTime AS Time(0)) AS EndTime,
ISNULL(a1.PurchaseStage, LAG(a1.PurchaseStage,1, NULL) OVER(PARTITION BY a1.Panelist_ID ORDER BY a1.actionday, a1.StartTime, a1.EndTime)) AS PurchaseStage, 
ISNULL(a2.Property,'START') AS Previous_Note,ISNULL(a2.category,'START') AS Previous_Category,
ISNULL(a2.subcategory,'START') AS Previous_SubCategory, a2.actionday AS Previous_Day,
CAST(a2.EndTime AS Time(0)) AS Previous_EndTime,
CASE a1.Category
WHEN 'END' THEN  NULL
ELSE CAST(DATEDIFF(SECOND, a2.EndTime, A1.StartTime) AS numeric)/60
END AS Offset, c.PurchaseDate AS PurchaseDate,
c.PurchaserFlag AS PurchaserFlag
INTO #AggPathsFinal
FROM #AggPathsEdited a1 LEFT JOIN #AggPathsEdited a2 
ON a1.RowNum2 = a2.RowNum2 + 1 AND a1.[Panelist_ID] = a2.[Panelist_ID]
LEFT JOIN dbo.PurchaseDay c ON a1.[Panelist_ID]=c.UserID
WHERE (a1.Property != a2.Property OR a2.Property IS NULL OR (a1.Property = a2.Property AND a1.subcategory != a2.subcategory)) AND c.Keep_Revised=1

SELECT * FROM #AggPathsFinal


 SELECT * FROM #CleanDays

DROP TABLE dbo.PurchaserFlag

SELECT * FROM dbo.PurchaserFlag

SELECT * FROM dbo.SequenceInput WHERE Type = 'URL' AND TaxID_Custom IS NOT NULL
SELECT * FROM dbo.TaxonomyMasterList_Custom