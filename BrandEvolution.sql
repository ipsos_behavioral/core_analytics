
-- BRAND EVOLUTION
-- Script created by Dana 4/30/19


  ---------------------
  -- GET SPAN DATES
  ---------------------
  begin try drop table #span_dates end try begin catch end catch;

  select panelist_id, 
		min(day_id) as min_date,
		max(day_id) as max_date,
		datediff(day, min(day_id), max(day_id))+1 as span_dates
  into #span_dates
  from [GoogleAuto].[run].[v_Report_Sequence_Event_Ecosystem]
  group by panelist_id;
  -- 868 rows affected
  -- select * from #span_Dates where panelist_id = 'C1670802404';


  -- ASSIGN ROW NUMBER TO DAY_ID
  /**
  -- drop table #dayid_row;
  select panelist_id, 
		day_id,
		ROW_NUMBER() over(partition by panelist_id order by panelist_id, day_id) as row_num
  into #dayid_row
  from [GoogleAuto].[run].[v_Report_Sequence_Event_Ecosystem]
  group by panelist_id, day_id
  order by panelist_id;
  -- 6720 rows affected
  -- select * from #dayid_row order by panelist_id, day_id; **/

  
  -- GET ALL QUAL DAYS FOR SPAN
  begin try drop table #mindate_ecosystem end try begin catch end catch;

  select distinct(Panelist_ID), min(day_id) as min_date
  into #mindate_ecosystem
  from [GoogleAuto].[run].[v_Report_Sequence_Event_Ecosystem] 
  group by panelist_id;
  -- select * from #mindate_ecosystem;

  begin try drop table #dayid_row end try begin catch end catch;

  select a.Panelist_ID, 
		b.Day_ID,
		datediff(day, a.min_Date, b.day_id) as row_num
  into #dayid_row
  from #mindate_ecosystem a
  join [GoogleAuto].[run].[v_Report_Sequence_Event_Ecosystem] b
  on a.Panelist_ID=b.Panelist_ID
  group by a.panelist_id, b.day_id, a.min_date
  order by panelist_id;
  -- 6720 rows affected
  -- select * from #Dayid_row order by panelist_id, day_id;

 
  begin try drop table #span_join end try begin catch end catch;

  select b.*, a.span_dates,
	span_dates/5 as mark_point
  into #span_join
  from #span_dates a
  join #dayid_row b
  on a.Panelist_ID=b.Panelist_ID;
  -- 6720 rows affected;
  -- select * from #span_join where panelist_id = 'C1696123525';
  -- select * from #spandate_join where panelist_id = 'C1717879012';
  -- select * from #spandate_join where span_dates <7; --464 rows 
  -- select distinct(panelist_id) from #spandate_join where span_Dates < 7 and span_Dates != 1; -- 108 distinct panelists
 
 begin try drop table #mark_join end try begin catch end catch;

 select
	panelist_id, 
	row_num,
	day_id, 
	case 
		when span_dates = 1 then '0-20%' -- revisit doing this...may skew the mark
		when row_num <= (span_dates/5) then '0-20%'
		when row_num > (mark_point) and row_num <= (mark_point*2) then '20-40%'
		when row_num > (mark_point*2) and row_num <= (mark_point*3) then '40-60%'
		when row_num > (mark_point*3) and row_num <= (mark_point*4) then '60-80%'
		when row_num > (mark_point*4) then '80-100%'
	ELSE '0' end as mark_groups
	into #mark_join
	from #span_join
	group by panelist_id, day_id, row_num, span_dates, mark_point;
	-- 6720 rows affected;
    -- select * from #mark_join where panelist_id = 'C1696123525'

	-- QA: issues arise when ct_Dates <= 4. Since it's / 5, they're all getting assigned 80-100%
		-- select distinct(panelist_id) from #spandate_join where ct_Dates = 4; -- 54 distinct panelists
		-- select distinct(panelist_id) from #spandate_join where ct_Dates = 3; -- 92 distinct panelists


-- JOIN BACK TO V_REPORT_SEQUENCE_EVENT_ECOSYSTEM TO PULL IN BRANDS 
begin try drop table #mark_cuts end try begin catch end catch;

select a.*, b.mark_groups
into #mark_cuts
from [GoogleAuto].[run].[v_Report_Sequence_Event_Ecosystem] a
join #mark_join b
on a.Day_ID=b.Day_ID
and a.Panelist_ID=b.Panelist_ID;
-- 107781 rows affected
-- select  top 1000* from #mark_cuts; 
-- select * from [GoogleAuto].[run].[v_Report_Sequence_Event_Ecosystem] ; --107781 rows affected
-- select distinct(Day_id), panelist_id from #mark_cuts where mark_groups = '0-20%';
	


-------------------------------
-- NOW TO DEAL WITH THE BRANDS
-------------------------------

-- SPLIT OUT EACH MARK GROUP FOR THE LEFT OUTER JOINS LATER
-- MARK GROUP 1 (20%)

  begin try drop table #mark_group1 end try begin catch end catch;

  select distinct(content_Category) as Brand1, mark_groups as mark_Groups1, panelist_id, Day_ID
  into #mark_Group1
  from #mark_cuts
  where mark_groups = '0-20%'
  group by panelist_id, content_Category, day_id, mark_groups, day_id;
  -- 4310 rows affected (ct)
  -- select distinct(day_id), panelist_id from #mark_Group1;
  -- select * from #mark_group1;


-- MARK GROUP 2 (40%)
  begin try drop table #mark_group2 end try begin catch end catch;

  select distinct(content_Category) as Brand2, mark_groups as mark_groups2, Panelist_ID, day_id
  into #mark_Group2
  from #mark_cuts
  where mark_groups = '20-40%'
  group by panelist_id, content_Category, day_id, mark_groups, day_id;
  -- 2297 rows affected; (ct)
  -- select * from #mark_Group2;

-- MARK GROUP 3 (60%)
  begin try drop table #mark_group3 end try begin catch end catch;

  select distinct(content_Category) as Brand3, mark_groups as mark_groups3, Panelist_ID, day_id
  into #mark_Group3
  from #mark_cuts
  where mark_groups = '40-60%'
  group by panelist_id, content_Category, day_id, mark_groups, day_id;
  -- 2037 rows affected; (ct)
  -- select * from #mark_Group3;

-- MARK GROUP 4 (80%)
  begin try drop table #mark_group4 end try begin catch end catch;

  select distinct(content_Category) as Brand4, mark_groups as mark_groups4, Panelist_ID, Day_ID
  into #mark_Group4
  from #mark_cuts
  where mark_groups = '60-80%'
  group by panelist_id, content_Category, day_id, mark_groups, day_id;
  -- 1997 rows affected (ct);
  -- select * from #mark_group4;


-- MARK GROUP 5 (100%)
  begin try drop table #mark_group5 end try begin catch end catch;

  select distinct(content_Category) as Brand5, mark_groups as mark_groups5, Panelist_ID, Day_ID
  into #mark_Group5
  from #mark_cuts
  where mark_groups = '80-100%'
  group by panelist_id, content_Category, day_id, mark_groups, day_id;
  -- 3213 rows affected; (ct)
  -- select distinct(brand5) from #mark_Group5 where panelist_id = 'C1671466165';


  -- CREATE TABLE OF ALL PANELISTS FROM THE MARKS (revist on how to code this...for now, export all to excel, dedupe, and reload --> ref.Panelist_Marks)
  -- select * from GoogleAuto.ref.Panelist_Marks; -- 1504 rows affected (span)
  -- select * from GoogleAuto.ref.Panelist_Marks2 where panelist_id = 'C1670893930'; -- 2029 rows affected (ct)

 

  begin try drop table #brand_joinstart end try begin catch end catch;

  select a.panelist_id, a.mark_Groups, b.brand1, b.mark_Groups1
  into #brand_joinstart
  from GoogleAuto.ref.Panelist_Marks2 a
  left outer join #mark_Group1 b
  on a.panelist_id=b.panelist_id
  and a.mark_Groups=b.mark_Groups1
  group by a.panelist_id, mark_Groups, brand1, mark_Groups1;
  -- 3741 rows affected; (ct)
  -- QA to check: select * from #brand_joinstart order by panelist_id, mark_Groups
 


  begin try drop table #brand12_join end try begin catch end catch;

  select a.panelist_id, a.mark_groups, a.brand1, b.brand2
  into #brand12_join
  from #brand_joinstart a
  left outer join #mark_group2 b
  on a.Panelist_ID=b.Panelist_ID
  and a.mark_groups=b.mark_groups2
  group by a.panelist_id, mark_groups, brand1, brand2;
  -- 4517 rows affected; (count)



  begin try drop table #brand13_join end try begin catch end catch;

  select a.panelist_id, a.mark_groups, a.brand1, a.brand2,  b.brand3
  into #brand13_join
  from #brand12_join a
  left outer join #mark_group3 b
  on a.Panelist_ID=b.Panelist_ID
  and a.mark_groups=b.mark_groups3
  group by a.panelist_id, mark_groups, brand1, brand2, brand3;
  -- 5188 rows affected (ct)



  begin try drop table #brand14_join end try begin catch end catch;

  select a.panelist_id, a.mark_groups, a.brand1, a.brand2, a.brand3, b.brand4
  into #brand14_join
  from #brand13_join a
  left outer join #mark_Group4 b
  on a.panelist_id = b.Panelist_ID
  and a.mark_groups=b.mark_groups4
  group by a.panelist_id, mark_groups, brand1, brand2, brand3, brand4;
  -- 5857 rows affected (count)


  begin try drop table #brand15_join end try begin catch end catch;

  select a.panelist_id, a.mark_groups, a.brand1, a.brand2, a.brand3, a.brand4,  b.brand5
  into #brand15_join
  from #brand14_join a
  left outer join #mark_Group5 b
  on a.panelist_id = b.Panelist_ID
  and a.mark_groups=b.mark_groups5
  group by a.panelist_id, mark_groups, brand1, brand2, brand3, brand4, brand5;
  -- 6953 rows affected;
  -- select * from #brand15_join;


  ---------------------------
  -- NOW FOR AGGS & NETS
  ---------------------------

  -- 1) COUNT OF BRANDS AT EACH MARK

  -- PUT ALL MARKS ON THE SAME LINE FOR EACH PANELIST
  begin try drop table #count_brands end try begin catch end catch;

  select panelist_id, 
	count(distinct brand1) as mark1, 
	count(distinct brand2) as mark2,
	count(distinct brand3) as mark3, 
	count(distinct brand4) as mark4, 
	count(distinct brand5) as mark5
  into #count_brands
  from #brand15_join
  group by panelist_id
  order by panelist_id;
  -- 868 rows affected (ct)
  -- select * from #count_brands where panelist_id = 'C1671466165';
  -- select * from #brand15_join;
  -- select * from #count_brands;
  
  -- NEED TO CONVERT TO NUMERIC FOR AVERAGES
  begin try drop table #count_brands_numeric end try begin catch end catch;

  select
	panelist_id,
	cast(mark1 as numeric) as mark1,
	cast(mark2 as numeric) as mark2,
	cast(mark3 as numeric) as mark3,
	cast(mark4 as numeric) as mark4,
	cast(mark5 as numeric) as mark5
  into #count_brands_numeric
  from #count_brands;
  -- 868 rows affected;
  -- select * from #count_brands_numeric

  -- AVERAGE AT EACH MARK
  select 
	cast(avg(mark1) AS NUMERIC(10,2)) as mark1,
	cast(avg(mark2) AS NUMERIC(10,2)) as mark2,
	cast(avg(mark3) AS NUMERIC(10,2)) as mark3,
	cast(avg(mark4) AS NUMERIC(10,2)) as mark4,
	cast(avg(mark5) AS NUMERIC(10,2)) as mark5
  from #count_brands_numeric;

  -- ISSUE: example --> if the span is 155 and the usage days are 37, then using the mark_point isn't completely accurate because the mark point is 31, which makes the majority
  -- of the marks in group 1, and doesn't get any into groups 3 & 4 (because the mark point and total # of usage days are close together...)
	-- ISSUE SOLVED



 -- 2) NUMBER OF BRANDS DROPPED AT EACH MARK


 ---------------
 -- 0-20% MARK1
 ----------------
 begin try drop table #dropped_mark1 end try begin catch end catch;

 select a.panelist_id, a.brand1
 into #dropped_mark1
 from #brand15_join a
 where not exists (select panelist_id, brand2 from #brand15_join b where a.panelist_id=b.panelist_id and b.mark_groups = '20-40%' and a.brand1=b.brand2)
 and a.mark_groups='0-20%'
 group by a.panelist_id, a.brand1;
 -- 1297 rows affected;
 -- select * from #dropped_mark1 where panelist_id = 'C1670802404';

 ----------------
 -- 20-40% MARK2
 ----------------
 begin try drop table #dropped_mark2 end try begin catch end catch;

 select a.panelist_id, a.brand2
 into #dropped_mark2
 from #brand15_join a
 where not exists (select panelist_id, brand2 from #brand15_join b where a.panelist_id=b.panelist_id and b.mark_groups = '40-60%' and a.brand2=b.brand3)
 and a.mark_groups='20-40%'
 group by a.panelist_id, a.brand2;
 -- 693 rows affected;
 -- select * from #dropped_mark2;
 -- select * from #dropped_mark2 where panelist_id = 'C1670802404';

 ----------------
 -- 40-60% MARK3
 ----------------
 begin try drop table #dropped_mark3 end try begin catch end catch;

 select a.panelist_id, a.brand3
 into #dropped_mark3
 from #brand15_join a
 where not exists (select panelist_id, brand3 from #brand15_join b where a.panelist_id=b.panelist_id and b.mark_groups = '60-80%' and a.brand3=b.brand4)
 and a.mark_groups='40-60%'
 group by a.panelist_id, a.brand3;
 -- 641 rows affected;
 -- select * from #dropped_mark3;
 -- select * from #dropped_mark3 where panelist_id = 'C1671466165';


 ----------------
 -- 60-80% MARK4
 ----------------
 begin try drop table #dropped_mark4 end try begin catch end catch;

 select a.panelist_id, a.Brand4
 into #dropped_mark4
 from #brand15_join a
 where not exists (select panelist_id, brand4 from #brand15_join b where a.panelist_id=b.panelist_id and b.mark_groups = '80-100%' and a.brand4=b.brand5)
 and a.mark_groups='60-80%'
 group by a.panelist_id, a.brand4;
 -- 543 rows affected;
 -- select * from #dropped_mark4;
 -- select * from #dropped_mark4 where panelist_id = 'C1670802404';


 -- JOIN THE DROPPED MARKS TOGETHER
begin try drop table #count_dropped1 end try begin catch end catch;

select panelist_id,
	count(distinct brand1) as dropped_brand1
into #count_dropped1
from #dropped_mark1
group by panelist_id;
-- 524 rows affected
-- select * from #count_Dropped1;

begin try drop table #count_dropped2 end try begin catch end catch;

select panelist_id,
	count(distinct brand2) as dropped_brand2
into #count_dropped2
from #dropped_mark2
group by panelist_id;
-- 335 rows affected

begin try drop table #count_dropped3 end try begin catch end catch;

select panelist_id,
	count(distinct brand3) as dropped_brand3
into #count_dropped3
from #dropped_mark3
group by panelist_id;
-- 328 rows affected

begin try drop table #count_dropped4 end try begin catch end catch;

select panelist_id,
	count(distinct brand4) as dropped_brand4
into #count_dropped4
from #dropped_mark4
group by panelist_id;
-- 289 rows affected
-- select * from #dropped_mark4


begin try drop table #total_brandsdropped end try begin catch end catch;

select d.panelist_id,
	a.dropped_brand1, 
	b.dropped_brand2,
	c.dropped_brand3,
	d.dropped_brand4
into #total_brandsdropped
from #count_dropped1 a
right outer join #count_dropped2 b
	on a.panelist_id=b.panelist_id
right outer join #count_dropped3 c
	on a.panelist_id=c.panelist_id
right outer join #count_dropped4 d
	on a.panelist_id=d.panelist_id
group by d.panelist_id, dropped_brand1, dropped_brand2, dropped_brand3, dropped_brand4;
-- 289 rows affected
-- select * from #total_brandsdropped ;

-----------------------
-- NOW FOR AGGS & NETS
-----------------------

	-- 1) COUNT OF DROPPED BRANDS AT EACH MARK

	begin try drop table #dropped_brand_marks end try begin catch end catch;

	select panelist_id,
		dropped_brand1,
		dropped_brand2,
		dropped_brand3,
		dropped_brand4
	into #dropped_brand_marks
	from #total_brandsdropped
	group by panelist_id, dropped_brand1, dropped_brand2, dropped_brand3, dropped_brand4
	order by panelist_id;
	-- 289 rows affected
	-- select * from #dropped_brand_marks;

	-- 2) NEED TO COVERT TO NUMERIC FOR AVERAGES
	begin try drop table #count_droppedbrands_numeric end try begin catch end catch;

	select
		panelist_id,
		cast(dropped_brand1 as numeric) as dropped_brand1,
		cast(dropped_brand2 as numeric) as dropped_brand2,
		cast(dropped_brand3 as numeric) as dropped_brand3,
		cast(dropped_brand4 as numeric) as dropped_brand4
	into #count_droppedbrands_numeric
	from #dropped_brand_marks;
	-- select * From #count_droppedbrands_numeric;

	-- 3) AVERAGE AT EACH MARK

	begin try drop table #avg_droppedbrands end try begin catch end catch;

	select
		cast(avg(dropped_brand1) AS NUMERIC(10,2)) as dropped_mark1,
		cast(avg(dropped_brand2) AS NUMERIC(10,2)) as dropped_mark2,
		cast(avg(dropped_brand3) AS NUMERIC(10,2)) as dropped_mark3,
		cast(avg(dropped_brand4) AS NUMERIC(10,2)) as dropped_mark4
	into #avg_droppedbrands
	from #count_droppedbrands_numeric;
	-- select * from #avg_droppedbrands;





 -- 3) NUMBER OF BRANDS ADDED AT EACH MARK

 ---------------
 -- 0-20% MARK1
 ----------------
 select a.panelist_id, a.Brand2
 into #added_mark1
 from #brand15_join a
 where not exists (select panelist_id, Brand1 from #brand15_join b where a.panelist_id=b.panelist_id and b.mark_groups = '0-20%' and a.brand2=b.brand1)
 and a.mark_groups='20-40%'
 group by a.panelist_id, a.Brand2;
 -- 553 rows affected;
 -- select * From #added_mark1 where panelist_id = 'C1670802404';
 -- select distinct(panelist_id) from #added_mark1;

 ----------------
 -- 20-40% MARK2
 ----------------
 select a.panelist_id, a.Brand3
 into #added_mark2
 from #brand15_join a
 where not exists (select panelist_id, Brand2 from #brand15_join b where a.panelist_id=b.panelist_id and b.mark_groups = '20-40%' and a.brand3=b.brand2)
 and a.mark_groups='40-60%'
 group by a.panelist_id, a.Brand3;
 -- 588 rows affected;

 ----------------
 -- 40-60% MARK3
 ----------------
 select a.panelist_id, a.Brand4
 into #added_mark3
 from #brand15_join a
 where not exists (select panelist_id, Brand3 from #brand15_join b where a.panelist_id=b.panelist_id and b.mark_groups = '40-60%' and a.brand4=b.brand3)
 and a.mark_groups='60-80%'
 group by a.panelist_id, a.Brand4;
 -- 639 rows affected;

 ----------------
 -- 60-80% MARK4
 ----------------
 select a.panelist_id, a.Brand5
 into #added_mark4
 from #brand15_join a
 where not exists (select panelist_id, Brand4 from #brand15_join b where a.panelist_id=b.panelist_id and b.mark_groups = '60-80%' and a.brand5=b.brand4)
 and a.mark_groups='80-100%'
 group by a.panelist_id, a.Brand5;
 -- 1256 rows affected;

 -- select * from #added_mark4 where panelist_id = 'C1670893930';


 -- JOIN ALL THE ADDED MARKS TOGETHER
 -- drop table #count_Added1;
 select panelist_id, 
	count(distinct brand2) as added_brand2
 into #count_added1
 from #added_mark1
 group by panelist_id;
 -- 295 rows affected;
 -- select * from #added_mark1;

 select panelist_id, 
	count(distinct brand3) as added_brand3
 into #count_added2
 from #added_mark2
 group by panelist_id;
 -- 323 rows affected;
 -- select distinct(panelist_id) from #added_mark2;

 select panelist_id, 
	count(distinct brand4) as added_brand4
 into #count_added3
 from #added_mark3
 group by panelist_id;
 -- 321 rows affected;
 -- select distinct(panelist_id) from #added_mark3;

 
 select panelist_id, 
	count(distinct brand5) as added_brand5
 into #count_added4
 from #added_mark4
 group by panelist_id;
 -- 597 rows affected;
 -- select distinct(panelist_id) from #added_mark4;

 select d.panelist_id, 
	a.added_brand2, 
	b.added_brand3, 
	c.added_brand4, 
	d.added_brand5
 into #total_brandsadd
 from #count_added1 a
 right outer join #count_added2 b
	 on a.panelist_id=b.panelist_id
 right outer join #count_added3 c
	 on b.panelist_id=c.panelist_id
 right outer join #count_added4 d
	 on c.panelist_id=d.panelist_id
 group by d.panelist_id, added_brand2, added_brand3, added_brand4, added_brand5;
	-- select * from #total_brandsadd;
