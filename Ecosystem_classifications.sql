------------------
--ECOSYSTEM CORE CLASSIFICATIONS
--select top 100 * from Core.ref.Ecosystem_Classifications
-- select distinct categorization_source from Core.ref.Ecosystem_Classifications order by categorization_source
-- TO DO: have column which removes the top-level domain country codes and have that as a secondary matchup
	-- also add in vendor?? useful for app names
--ADD IN OMNI ENTITY column
-- Call TLD the parent property name (for apps to count as well)
--in cloud its easier to have in one table
------------------

use Core;

----------------------------Cox Wireless
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Telcom varchar(50);
*/
begin try drop table #cox_insert end try begin catch end catch;
create table #cox_insert (
source_property_key int, 
property_name nvarchar(4000),
digital_type_id int,
carrier_upgrade varchar(50),
ecosystem_category varchar(300),
ecosystem_subcategoy varchar(300),
property_hash bigint,
category_update int
);
insert into #cox_insert select 4175, 'bestphoneplans.net', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 5195122594916122834, 1;
insert into #cox_insert select 4097, 'bestmvno.com', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -3340501695533414789, 1;
insert into #cox_insert select 7997, 'cellphoneplan.com', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -5510922117066333281, 1;
insert into #cox_insert select 37468, 'switch2t-mobile.com', 235, 'white', 'Mobile Carrier', 'Mobile Carrier', -1498969780541440888, 1;
insert into #cox_insert select 7151, 'cell-phone-plans-for-seniors.com', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 8246036229373873358, 1;
insert into #cox_insert select 37488, 'switch2tmobile.com', 235, 'white', 'Mobile Carrier', 'Mobile Carrier', 424658407500673550, 1;
insert into #cox_insert select 34209, 'searchcellphoneplansplus.com', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 4912801470333074070, 1;
insert into #cox_insert select 16335, 'governmentphoneplan.com', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 6204906234559067540, 1;
insert into #cox_insert select 68674, 'swiftcontractphones.com', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 5078421152804865421, 1;
insert into #cox_insert select 6769, 'cellularphoneplansforseniors.com', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -5325193592478298338, 1;
insert into #cox_insert select 27021, 'nocontractcellular.org', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 1282923418198979993, 1;
insert into #cox_insert select 50475, 'comparewirelesscarriers.com', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -5516341155645733476, 1;
insert into #cox_insert select 53876, 'guaranteedmobilephonecontract.com', 235, 'white', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 7609222665513590146, 1;
insert into #cox_insert select 39624, 't-mobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -6111597985736969277, 0;
insert into #cox_insert select 2994, 'att.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 8151477425277259467, 0;
insert into #cox_insert select 42118, 'verizon.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 8758517301657899693, 0;
insert into #cox_insert select 24352, 'metrobyt-mobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -1395805275363601494, 0;
insert into #cox_insert select 9529, 'cricketwireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 1684197210956615032, 1;
insert into #cox_insert select 5299, 'boostmobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 7079492777689764312, 0;
insert into #cox_insert select 42120, 'visible.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -168798387376889162, 0;
insert into #cox_insert select 45106, 'xfinity.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 417386976899598064, 1;
insert into #cox_insert select 41478, 'usmobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 7488355825520467786, 1;
insert into #cox_insert select 36389, 'sprint.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -3613153414952338607, 0;
insert into #cox_insert select 36899, 'straighttalk.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 7704606911676895240, 0;
insert into #cox_insert select 31377, 'qlinkwireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 3608135579790082854, 0;
insert into #cox_insert select 35954, 'spectrum.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 7447845027031884990, 0;
insert into #cox_insert select 2908, 'assurancewireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -4498896929252343452, 1;
insert into #cox_insert select 24843, 'mintmobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -9095070216211104510, 0;
insert into #cox_insert select 35005, 'simplemobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -6773880360482593100, 0;
insert into #cox_insert select 8346, 'consumercellular.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 8270936012374080379, 0;
insert into #cox_insert select 37932, 'tello.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -8895051692802311757, 1;
insert into #cox_insert select 39863, 'tracfone.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 6416753377201855353, 0;
insert into #cox_insert select 25103, 'moneysavingpro.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -5544808568506959576, 1;
insert into #cox_insert select 10798, 'credomobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 6701569392452728764, 0;
insert into #cox_insert select 33298, 'safelinkwireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -3341485175607701182, 1;
insert into #cox_insert select 41434, 'uscellular.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -2995293407039045361, 0;
insert into #cox_insert select 41788, 'verizonwireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 9085773568785065051, 0;
insert into #cox_insert select 39828, 'totalwireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -4470239169810678707, 1;
insert into #cox_insert select 39638, 'ting.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 4640302310588184612, 1;
insert into #cox_insert select 31273, 'puretalkusa.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 8205671405772337404, 1;
insert into #cox_insert select 14522, 'freedompop.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 9020910106606626616, 1;
insert into #cox_insert select 33376, 'safelinkupgrades.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 8704743801191727490, 1;
insert into #cox_insert select 26649, 'net10wireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -6706189536520734548, 1;
insert into #cox_insert select 2981, 'att-promotions.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -140978041879499849, 1;
insert into #cox_insert select 32141, 'redpocket.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 2593392815322509296, 1;
insert into #cox_insert select 40987, 'ultramobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -3171843602321128306, 1;
insert into #cox_insert select 17372, 'hellomobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -4672091941183874490, 1;
insert into #cox_insert select 32523, 'republicwireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 8904781835449221635, 1;
insert into #cox_insert select 28372, 'optimum.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -8936439671577529717, 1;
insert into #cox_insert select 1309, 'accesswireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -90871827229818991, 1;
insert into #cox_insert select 1802, 'alticemobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -7070122983760628309, 1;
insert into #cox_insert select 36772, 'standupwireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 6910921646691465029, 1;
insert into #cox_insert select 25862, 'myfamilymobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 2135575869965121159, 1;
insert into #cox_insert select 22047, 'lifelinesupport.org', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -6180926861807272884, 1;
insert into #cox_insert select 1811, 'allconnect.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 6130413210932936630, 1;
insert into #cox_insert select 5348, 'attwirelessonline.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 4119095652164889307, 1;
insert into #cox_insert select 24240, 'metropcs.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -2889575275420913998, 1;
insert into #cox_insert select 24240, 'metropcs.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -2889575275420913998, 0;
insert into #cox_insert select 40598, 'twigby.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -4792540858342217594, 1;
insert into #cox_insert select 43552, 'wingalpha.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -8946383870461328246, 1;
insert into #cox_insert select 12146, 'entouchwireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 8442543080845967715, 1;
insert into #cox_insert select 1228, 'affinitycellular.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 6080331850621673642, 1;
insert into #cox_insert select 28674, 'pagepluscellular.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 4856537581709287173, 1;
insert into #cox_insert select 66905, 'rogers.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -6713002301851396977, 1;
insert into #cox_insert select 14576, 'freegovernmentcellphoneguide.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -7517332991092439718, 1;
insert into #cox_insert select 50555, 'dailywireless.org', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 8909751703427805858, 1;
insert into #cox_insert select 20603, 'keepcalling.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 8990717407734136311, 1;
insert into #cox_insert select 36226, 'speedtalkmobile.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 6966870589121710619, 1;
insert into #cox_insert select 36809, 'straighttalksmartpay.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -3797915362228409405, 0;
insert into #cox_insert select 44114, 'xfinityprepaid.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -8492319509279495742, 0;
insert into #cox_insert select 42282, 'vzw.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -2871053514360800876, 0;
insert into #cox_insert select 24226, 'metro-tmo.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -7863908089983753498, 1;
insert into #cox_insert select 3035, 'att-bundles.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 81085030661310001, 1;
insert into #cox_insert select 70808, 'virginmobile.ca', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -9177374615231200135, 1;
insert into #cox_insert select 31332, 'qlinkportal.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', -2012964493727622510, 1;
insert into #cox_insert select 4030, 'bestbuy.com', 235, 'green', 'Retailer', 'Mass Merchandiser', -4491217108001145126, 1;
insert into #cox_insert select 44283, 'whistleout.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -8322228638167181755, 1;
insert into #cox_insert select 32733, 'reviews.org', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 2566539191409545571, 1;
insert into #cox_insert select 39875, 'tomsguide.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 2791023970005953010, 0;
insert into #cox_insert select 3835, 'amazon.com', 235, 'green', 'Retailer', 'Amazon', -76485819139516796, 0;
insert into #cox_insert select 32182, 'reddit.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -6608813955292750754, 0;
insert into #cox_insert select 42451, 'walmart.com', 235, 'green', 'Retailer', 'Mass Merchandiser', -3034774944025059528, 0;
insert into #cox_insert select 26582, 'nerdwallet.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 6239469133900730772, 0;
insert into #cox_insert select 7760, 'cnet.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 4891416541794787555, 1;
insert into #cox_insert select 38093, 'techradar.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -5230765215380759146, 1;
insert into #cox_insert select 31729, 'rakuten.com', 235, 'green', 'Media/Reviews', 'Coupons/Freebie/Sweepstakes', 7575066160698256193, 1;
insert into #cox_insert select 7342, 'clark.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 4196171936693227805, 0;
insert into #cox_insert select 13432, 'ebay.com', 235, 'green', 'Retailer', 'Online Retailer', 5681545045206564693, 0;
insert into #cox_insert select 41508, 'usnews.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -7440856851551053560, 0;
insert into #cox_insert select 39710, 'tmonews.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 7749034079485264560, 1;
insert into #cox_insert select 44259, 'yahoo.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 9101809637679198610, 0;
insert into #cox_insert select 28047, 'official-coupons.com', 235, 'green', 'Media/Reviews', 'Coupons/Freebie/Sweepstakes', 2803088017889809381, 1;
insert into #cox_insert select 42640, 'top10.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -7947765283470710037, 1;
insert into #cox_insert select 21897, 'letstalk.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 1149109423292120234, 1;
insert into #cox_insert select 24491, 'membershipwireless.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -1233704657510684957, 0;
insert into #cox_insert select 35117, 'slickdeals.net', 235, 'green', 'Media/Reviews', 'Coupons/Freebie/Sweepstakes', 846861473259073141, 1;
insert into #cox_insert select 8768, 'coveragecritic.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -1009922358312945653, 1;
insert into #cox_insert select 13192, 'fiercewireless.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -8157309576505054832, 1;
insert into #cox_insert select 27598, 'nytimes.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 7951929796602746881, 0;
insert into #cox_insert select 10177, 'digitaltrends.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -7336343187475442553, 1;
insert into #cox_insert select 22341, 'lifewire.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -7440136915328893884, 1;
insert into #cox_insert select 22018, 'libertywireless.com', 235, 'green', 'Mobile Carrier', 'Mobile Carrier', 4378331477160190143, 1;
insert into #cox_insert select 6779, 'cellularphonesforseniors.net', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -333277366734654280, 1;
insert into #cox_insert select 17081, 'h2owireless.com', 235, 'purple', 'Mobile Carrier', 'Mobile Carrier', 7079512524673968380, 1;
insert into #cox_insert select 31691, 'quora.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -6457916762909056244, 0;
insert into #cox_insert select 29990, 'phonedealstoday.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -263373381126337054, 1;
insert into #cox_insert select 42635, 'upphone.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -3786056285143635634, 1;
insert into #cox_insert select 20340, 'joinhoney.com', 235, 'green', 'Media/Reviews', 'Coupons/Freebie/Sweepstakes', -1581989657560414643, 1;
insert into #cox_insert select 23343, 'makeuseof.com', 235, 'purple', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 3043980777496336209, 1;
insert into #cox_insert select 22580, 'lowincomerelief.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -3516635705882879400, 1;
insert into #cox_insert select 22670, 'losmobileusa.com', 235, 'green', 'Retailer', 'Retailer: All Other', -230603326178181605, 1;
insert into #cox_insert select 42086, 'veteransadvantage.com', 235, 'green', 'Media/Reviews', 'Coupons/Freebie/Sweepstakes', -6241651319027586573, 1;
insert into #cox_insert select 34490, 'sfonewireless.com', 235, 'green', 'Mobile Carrier', 'Mobile Carrier', -6009325986135128686, 1;
insert into #cox_insert select 9372, 'creditshout.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 4305474224957022684, 1;
insert into #cox_insert select 26288, 'myrateplan.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 8922888049230709279, 1;
insert into #cox_insert select 4637, 'bixwireless.com', 235, 'green', 'Mobile Carrier', 'Mobile Carrier', -9193687628396379755, 1;
insert into #cox_insert select 34400, 'seniorliving.org', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -4530050869572509354, 1;
insert into #cox_insert select 41759, 'valuepenguin.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 1581328239862935092, 0;
insert into #cox_insert select 2237, 'androidpolice.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -8489998084332119168, 1;
insert into #cox_insert select 43615, 'wired.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -5032008516998907348, 1;
insert into #cox_insert select 1711, 'androidauthority.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 611086428492894768, 1;
insert into #cox_insert select 66256, 'searchandshopping.org', 235, 'green', 'Search Engine', 'Search: All Other', 6301611004832856539, 1;
insert into #cox_insert select 35455, 'smarttechtune.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 4183923659957254787, 1;
insert into #cox_insert select 43378, 'woot.com', 235, 'green', 'Retailer', 'Online Retailer', 7962843933361223838, 0;
insert into #cox_insert select 4204, 'bestlifeonline.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 4912392289332998206, 1;
insert into #cox_insert select 13087, 'fcc.gov', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -5848521678893250707, 1;
insert into #cox_insert select 21799, 'l-com.com', 235, 'green', 'Retailer', 'Online Retailer', -8493554250894291937, 1;
insert into #cox_insert select 4158, 'benefithub.com', 235, 'green', 'Retailer', 'Retailer: All Other', 976815302161784171, 1;
insert into #cox_insert select 75697, 'karmanow.com', 235, 'green', 'Retailer', 'Retailer: All Other', -7462624150220242397, 1;
insert into #cox_insert select 30654, 'prepaidphonezone.com', 235, 'green', 'Retailer', 'Online Retailer', 7439927658077792359, 1;
insert into #cox_insert select 38492, 'theconsumeradvisory.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -2872276294087773720, 1;
insert into #cox_insert select 73885, 'phones4badcredit.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -7396098679695743962, 1;
insert into #cox_insert select 56157, 'helpdocs.io', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -3088312854518917216, 1;
insert into #cox_insert select 19011, 'infinitimobile.com', 235, 'green', 'Mobile Carrier', 'Mobile Carrier', 6819275212106431626, 1;
insert into #cox_insert select 1059, 'afterpay.com', 235, 'green', 'Retailer', 'Retailer: All Other', -8834325313442953604, 1;
insert into #cox_insert select 24715, 'mhealthcoach.net', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', 1331390571583549078, 1;
insert into #cox_insert select 10922, 'doctorsim.com', 235, 'green', 'Retailer', 'Online Retailer', 4656685265429285054, 0;
insert into #cox_insert select 14100, 'forbes.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -4337549941328668374, 0;
insert into #cox_insert select 32467, 'resettips.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 3984370183035650075, 1;
insert into #cox_insert select 11370, 'droid-life.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 2314586458063205888, 1;
insert into #cox_insert select 26404, 'nation.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -3767547349600141406, 0;
insert into #cox_insert select 15368, 'getakko.com', 235, 'green', 'Retailer', 'Retailer: All Other', 6319670388575941943, 1;
insert into #cox_insert select 53526, 'findinfoonline.com', 235, 'green', 'Search Engine', 'Search: All Other', -5887508957952082692, 1;
insert into #cox_insert select 19793, 'istheservicedown.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 3858747737215934163, 1;
insert into #cox_insert select 21988, 'imore.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 6145744280985271340, 1;
insert into #cox_insert select 34830, 'signalbooster.com', 235, 'green', 'Retailer', 'Online Retailer', 5048233653959497940, 1;
insert into #cox_insert select 16127, 'good2gomobile.com', 235, 'green', 'Mobile Carrier', 'Mobile Carrier', -5632099767654257790, 1;
insert into #cox_insert select 40312, 'topicanswers.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -1627821439637015634, 1;
insert into #cox_insert select 41551, 'usatoday.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -1375993956610832210, 0;
insert into #cox_insert select 2353, 'arstechnica.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', -4414885644483383445, 1;
insert into #cox_insert select 2334, 'androidguys.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 4272007828285383033, 1;
insert into #cox_insert select 43216, 'wikipedia.org', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -2667028083807682303, 0;
insert into #cox_insert select 16240, 'globenewswire.com', 235, 'green', 'Media/Reviews', 'General News/Info/Forums/Reviews', -9013882604017330041, 0;
insert into #cox_insert select 37838, 'techwalla.com', 235, 'green', 'Media/Reviews', 'Electronics News/Info/Forums/Reviews', 3005731792362369634, 1;

begin try drop table #cox_additions end try begin catch end catch;
select source_property_key, property_hash, digital_type_id, property_name, '' as property_name_TLD,
'CoxWireless2021' as categorization_source, 'US' as categorization_country, ecosystem_category, ecosystem_subcategoy as ecosystem_subcategory,
'UNCLASSIFIED' as Alcohol_Beverage, 
'UNCLASSIFIED' as Apparel,  'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Auto_Parts, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Books_Magazines,
carrier_upgrade as Consumer_Electronics, 'UNCLASSIFIED' as Family_Children,  'UNCLASSIFIED' as Fashion_Accessories, 'UNCLASSIFIED' as Food_Groceries,
'UNCLASSIFIED' as Games, 'UNCLASSIFIED' as Gift_Cards, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Home_Garden, 'UNCLASSIFIED' as Live_Events, 
'UNCLASSIFIED' as Media_Entertainment,  'UNCLASSIFIED' as Pets, 'UNCLASSIFIED' as Shoes,  'UNCLASSIFIED' as Sporting_Goods, 'UNCLASSIFIED' as Tobacco,  
'UNCLASSIFIED' as Toys, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Liquor, carrier_upgrade as Telcom
into #cox_additions
from #cox_insert
;
--(154 rows affected)




-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'Mattel'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #cox_additions a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(81 rows affected)


--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--new vert insert
update Core.ref.Ecosystem_Classifications
set Telcom  = a.Telcom
from #cox_additions a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Telcom = 'UNCLASSIFIED' or b.Telcom is null)
;
--(72 rows affected)

update Core.ref.Ecosystem_Classifications
set Consumer_Electronics= a.Consumer_Electronics
from #cox_additions a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Consumer_Electronics = 'UNCLASSIFIED' or b.Consumer_Electronics is null)
;
--(8 rows affected)

--Update to purple if green
-- select top 100 * from #cox_additions;
-- select * from Core.ref.Ecosystem_Classifications where property_name ='lifewire.com'
update Core.ref.Ecosystem_Classifications
set Consumer_Electronics= a.Consumer_Electronics
from #cox_additions a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Consumer_Electronics = 'green' and a.Consumer_Electronics = 'purple')
;
--(0 rows affected)


--------Overwrites based on previous on upload
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #cox_additions a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
join #cox_insert c on a.property_hash = c.property_hash and a.digital_type_id = c.digital_type_id and c.category_update = 1 -- determined before upload if category should be updated
;
--(113 rows affected)


update Core.ref.Ecosystem_Classifications set Telcom = 'UNCLASSIFIED' where Telcom is null; --(28110 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'CoxWireless2021';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; -- (28263 rows affected)

/*
----------------------------Beam Suntory
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Liquor varchar(50);
*/
begin try drop table #beam_insert end try begin catch end catch;
create table #beam_insert (
source_property_key int, 
property_name nvarchar(4000),
digital_type_id int,
liquor varchar(50),
ecosystem_category varchar(300),
ecosystem_subcategoy varchar(300),
property_hash bigint,
category_update int
);
insert into #beam_insert select 9425, 'My Cocktail Bar', 100, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 5514513573191990258, 1;
insert into #beam_insert select 5048, 'Distiller - Your Personal Liquor Expert', 100, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 74576791958552581, 1;
insert into #beam_insert select 12200, 'Tequila Matchmaker', 100, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 6444557616956168689, 1;
insert into #beam_insert select 10699, 'Whisky Suggest', 100, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 3304691165143737932, 1;
insert into #beam_insert select 7333, 'Medea Vodka', 100, 'white', 'Brand', 'Brand', -5216432529677833824, 1;
insert into #beam_insert select 84608, 'smirnoff.com', 235, 'white', 'Brand', 'Brand', 1834510536374697755, 1;
insert into #beam_insert select 34780, 'jackdaniels.com', 235, 'white', 'Brand', 'Brand', 1627699568589284706, 1;
insert into #beam_insert select 14127, 'crownroyal.com', 235, 'white', 'Brand', 'Brand', -8876564960217675654, 1;
insert into #beam_insert select 26764, 'flaviar.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -7249414169647490038, 1;
insert into #beam_insert select 43241, 'minibardelivery.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -2645755299958727087, 1;
insert into #beam_insert select 16588, 'distiller.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 6942557639805334284, 1;
insert into #beam_insert select 53582, 'patrontequila.com', 235, 'white', 'Brand', 'Brand', -7187807574507336933, 1;
insert into #beam_insert select 43017, 'makersmark.com', 235, 'white', 'Brand', 'Brand', 3336510137644426063, 1;
insert into #beam_insert select 80312, 'ciroc.com', 235, 'white', 'Brand', 'Brand', -2391828513130116288, 1;
insert into #beam_insert select 42775, 'maliburumdrinks.com', 235, 'white', 'Brand', 'Brand', -923467576301338620, 1;
insert into #beam_insert select 73868, 'titosvodka.com', 235, 'white', 'Brand', 'Brand', 4837245339667290681, 1;
insert into #beam_insert select 73582, 'whiskyadvocate.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -498770379412071786, 1;
insert into #beam_insert select 37170, 'johnniewalker.com', 235, 'white', 'Brand', 'Brand', 5411881503573571020, 1;
insert into #beam_insert select 8937, 'breakingbourbon.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -2846434590020673329, 1;
insert into #beam_insert select 49634, 'sipwhiskey.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -3416471758344729585, 1;
insert into #beam_insert select 6148, 'baileys.com', 235, 'white', 'Brand', 'Brand', 5499727171390343563, 1;
insert into #beam_insert select 19011, 'donjulio.com', 235, 'white', 'Brand', 'Brand', -1300900310068690089, 1;
insert into #beam_insert select 75177, 'tequilamatchmaker.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -9017566793889420757, 1;
insert into #beam_insert select 10986, 'captainmorgan.com', 235, 'white', 'Brand', 'Brand', -5493844264667250321, 1;
insert into #beam_insert select 35689, 'krakenrum.com', 235, 'white', 'Brand', 'Brand', -662788754125211801, 1;
insert into #beam_insert select 81181, 'fireballwhisky.com', 235, 'white', 'Brand', 'Brand', -2539655904957473232, 1;
insert into #beam_insert select 7210, 'bacardi.com', 235, 'white', 'Brand', 'Brand', 1801905073857017728, 1;
insert into #beam_insert select 38159, 'jimbeam.com', 235, 'white', 'Brand', 'Brand', 7673332472324767396, 1;
insert into #beam_insert select 38883, 'ketelone.com', 235, 'white', 'Brand', 'Brand', -6399288708315142610, 1;
insert into #beam_insert select 74869, 'wildturkeybourbon.com', 235, 'white', 'Brand', 'Brand', -6027197820097977873, 1;
insert into #beam_insert select 14835, 'cuervo.com', 235, 'white', 'Brand', 'Brand', -7788932926205102539, 1;
insert into #beam_insert select 30732, 'heavenhilldistillery.com', 235, 'white', 'Brand', 'Brand', -2568432813445979173, 1;
insert into #beam_insert select 37699, 'jamesonwhiskey.com', 235, 'white', 'Brand', 'Brand', -5120753007488990315, 1;
insert into #beam_insert select 83675, 'olesmoky.com', 235, 'white', 'Brand', 'Brand', -8301108863032705217, 1;
insert into #beam_insert select 30181, 'evanwilliams.com', 235, 'white', 'Brand', 'Brand', -5593770058238831104, 1;
insert into #beam_insert select 48562, 'mainespirits.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -3250417732037740590, 1;
insert into #beam_insert select 28204, 'grancentenario.com', 235, 'white', 'Brand', 'Brand', 286970877733096692, 1;
insert into #beam_insert select 6912, 'bittersandbottles.com', 235, 'white', 'Category Retailer', 'Brick + Click Liquor', 6852736233443112690, 1;
insert into #beam_insert select 77059, 'whiskybase.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -1401652841311190131, 1;
insert into #beam_insert select 62423, 'thewhiskeyshelf.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -2551389878269800, 1;
insert into #beam_insert select 48920, 'ohlq.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -1522891256890552606, 1;
insert into #beam_insert select 48447, 'jagermeister.com', 235, 'white', 'Brand', 'Brand', -1418115053901989827, 1;
insert into #beam_insert select 67750, 'themacallan.com', 235, 'white', 'Brand', 'Brand', -5521938150855258159, 1;
insert into #beam_insert select 10003, 'bulleit.com', 235, 'white', 'Brand', 'Brand', -588324005682114577, 1;
insert into #beam_insert select 29983, 'hennessy.com', 235, 'white', 'Brand', 'Brand', -2483759474671206562, 1;
insert into #beam_insert select 16987, 'evanwilliamsdugout.com', 235, 'white', 'Brand', 'Brand', 2269162488362319623, 1;
insert into #beam_insert select 70257, 'thebottlehaus.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -1128554167183383667, 1;
insert into #beam_insert select 46348, 'newamsterdamvodka.com', 235, 'white', 'Brand', 'Brand', 6792153075409125045, 1;
insert into #beam_insert select 3388, 'angelsenvy.com', 235, 'white', 'Brand', 'Brand', -9099519125971108691, 1;
insert into #beam_insert select 40614, 'michters.com', 235, 'white', 'Brand', 'Brand', 6909772800162796029, 1;
insert into #beam_insert select 80338, 'claseazul.com', 235, 'white', 'Brand', 'Brand', 9077893634357753297, 1;
insert into #beam_insert select 31932, 'greygoose.com', 235, 'white', 'Brand', 'Brand', -3236275324309899811, 1;
insert into #beam_insert select 70115, 'thebourbonconcierge.com', 235, 'white', 'Category Retailer', 'Brick + Click Liquor', -8219421534578269399, 1;
insert into #beam_insert select 68392, 'titosholiday.com', 235, 'white', 'Brand', 'Brand', -883062325284946227, 1;
insert into #beam_insert select 73653, 'whistlepigwhiskey.com', 235, 'white', 'Brand', 'Brand', 7042198248853759883, 1;
insert into #beam_insert select 81226, 'finedrams.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -7476715531241925089, 1;
insert into #beam_insert select 17513, 'crystalheadvodka.com', 235, 'white', 'Brand', 'Brand', -5097150230483091622, 1;
insert into #beam_insert select 1713, 'absolut.com', 235, 'white', 'Brand', 'Brand', -3420511384611089654, 1;
insert into #beam_insert select 69634, 'thebourbonculture.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -7285684213478559495, 1;
insert into #beam_insert select 33786, 'highwest.com', 235, 'white', 'Brand', 'Brand', 4830444211255915624, 1;
insert into #beam_insert select 66642, 'ritualzeroproof.com', 235, 'white', 'Brand', 'Brand', 5837541240672581361, 1;
insert into #beam_insert select 74761, 'unclenearest.com', 235, 'white', 'Brand', 'Brand', 1488002533694378594, 1;
insert into #beam_insert select 17655, 'deepeddyvodka.com', 235, 'white', 'Brand', 'Brand', -5428226105024212594, 1;
insert into #beam_insert select 43969, 'malt-review.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 1920439167759182910, 1;
insert into #beam_insert select 63946, 'scotchwhisky.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -3201659955579146933, 1;
insert into #beam_insert select 30278, 'heavenhill.com', 235, 'white', 'Brand', 'Brand', 8924519536164682470, 1;
insert into #beam_insert select 79067, 'woodfordreserve.com', 235, 'white', 'Brand', 'Brand', -8991328908197380962, 1;
insert into #beam_insert select 60914, 'whiskeyraiders.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -1292107834990998803, 1;
insert into #beam_insert select 43565, 'moodymixologist.com', 235, 'white', 'Media/Reviews', 'Recipe/Cocktail Info', 2726893635585800800, 1;
insert into #beam_insert select 7282, 'bluechairbayrum.com', 235, 'white', 'Brand', 'Brand', 4536012349846939384, 1;
insert into #beam_insert select 88412, 'bacardilimited.com', 235, 'white', 'Brand', 'Brand', 2033300805700535664, 1;
insert into #beam_insert select 63424, 'tanqueray.com', 235, 'white', 'Brand', 'Brand', 3351318269097661404, 1;
insert into #beam_insert select 42742, 'makersmarkpersonalize.com', 235, 'white', 'Brand', 'Brand', 1677638220116551069, 1;
insert into #beam_insert select 80665, 'cutwaterspirits.com', 235, 'white', 'Brand', 'Brand', 3770725713712644367, 1;
insert into #beam_insert select 53768, 'sharedpour.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -8596985697359429699, 1;
insert into #beam_insert select 12017, 'brothersbondbourbon.com', 235, 'white', 'Brand', 'Brand', 143710630284886003, 1;
insert into #beam_insert select 23466, 'empressgin.com', 235, 'white', 'Brand', 'Brand', 4184203233627673029, 1;
insert into #beam_insert select 9521, 'blantonsbourbon.com', 235, 'white', 'Brand', 'Brand', 8081566144567870312, 1;
insert into #beam_insert select 30274, 'goslingsrum.com', 235, 'white', 'Brand', 'Brand', -2226976777120221532, 1;
insert into #beam_insert select 32648, 'htfw.com', 235, 'white', 'Category Retailer', 'Brick + Click Liquor', -6958924052811543227, 1;
insert into #beam_insert select 26202, 'gobourbon.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 3512448380008836552, 1;
insert into #beam_insert select 26718, 'fourrosesbourbon.com', 235, 'white', 'Brand', 'Brand', -7913761686006542312, 1;
insert into #beam_insert select 124944, 'whisky.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -895012752168758742, 1;
insert into #beam_insert select 9484, 'blantonsbourbonshop.com', 235, 'white', 'Brand', 'Brand', -2952107998889608857, 1;
insert into #beam_insert select 65726, 'siptequila.com', 235, 'white', 'Category Retailer', 'Brick + Click Liquor', 1853644901184768388, 1;
insert into #beam_insert select 70459, 'theglenlivet.com', 235, 'white', 'Brand', 'Brand', 2475483855332773671, 1;
insert into #beam_insert select 66302, 'thewhiskypedia.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 930596643432493329, 1;
insert into #beam_insert select 69512, 'supercall.com', 235, 'white', 'Media/Reviews', 'Recipe/Cocktail Info', 8063137705607265007, 1;
insert into #beam_insert select 93708, 'dekanta.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -2786368500162107878, 1;
insert into #beam_insert select 53519, 'pristinevodka.com', 235, 'white', 'Brand', 'Brand', -169113473004802904, 1;
insert into #beam_insert select 76989, 'whiskeybon.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -5195281317042739631, 1;
insert into #beam_insert select 85079, 'thewhiskeyjug.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 2663525121777423818, 1;
insert into #beam_insert select 73817, 'vadistillery.com', 235, 'white', 'Brand', 'Brand', 2299268245380278338, 1;
insert into #beam_insert select 47275, 'mrblack.co', 235, 'white', 'Brand', 'Brand', -892420382036218013, 1;
insert into #beam_insert select 60319, 'skrewballwhiskey.com', 235, 'white', 'Brand', 'Brand', -7511018966815283400, 1;
insert into #beam_insert select 94358, 'drink.haus', 235, 'white', 'Brand', 'Brand', -558445101308589121, 1;
insert into #beam_insert select 64181, 'tequilacamarena.com', 235, 'white', 'Brand', 'Brand', 3040817302196561272, 1;
insert into #beam_insert select 4192, 'ardbeg.com', 235, 'white', 'Brand', 'Brand', -4751774333015774875, 1;
insert into #beam_insert select 87399, 'amass.com', 235, 'white', 'Brand', 'Brand', -3844708837497375, 1;
insert into #beam_insert select 37135, 'laphroaig.com', 235, 'white', 'Brand', 'Brand', 1768171782053672956, 1;
insert into #beam_insert select 54569, 'thebourbonfinder.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 4308481569715948177, 1;
insert into #beam_insert select 118336, 'spirithub.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', 6235427357335623330, 1;
insert into #beam_insert select 71897, 'therarewhiskeyshop.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -5008659394880872273, 1;
insert into #beam_insert select 76870, 'whiskysuggest.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -5173089602849859193, 1;
insert into #beam_insert select 68138, 'whiskeywatch.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', 7375870625718838265, 1;
insert into #beam_insert select 107584, 'mezcalreviews.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -804076230340061040, 1;
insert into #beam_insert select 15954, 'ejbrandy.com', 235, 'white', 'Brand', 'Brand', -7457040204490989008, 1;
insert into #beam_insert select 71370, 'tincupwhiskey.com', 235, 'white', 'Brand', 'Brand', -1251841903658018989, 1;
insert into #beam_insert select 63000, 'straightbourbon.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -2723146743923678741, 1;
insert into #beam_insert select 89031, 'beamsuntory.com', 235, 'white', 'Brand', 'Brand', -8605923558580460502, 1;
insert into #beam_insert select 58341, 'sazerac.com', 235, 'white', 'Brand', 'Brand', 5093509187871867593, 1;
insert into #beam_insert select 26424, 'georgedickel.com', 235, 'white', 'Brand', 'Brand', 6967445727750448481, 1;
insert into #beam_insert select 10901, 'bumbu.com', 235, 'white', 'Brand', 'Brand', -255122772544887324, 1;
insert into #beam_insert select 80158, 'casadragones.com', 235, 'white', 'Brand', 'Brand', -3942182160061808571, 1;
insert into #beam_insert select 49858, 'newamsterdamgin.com', 235, 'white', 'Brand', 'Brand', 3839675827130840780, 1;
insert into #beam_insert select 9306, 'bombaysapphire.com', 235, 'white', 'Brand', 'Brand', -8598402567469498200, 1;
insert into #beam_insert select 68305, 'suntory.com', 235, 'white', 'Brand', 'Brand', 9011638748139582023, 1;
insert into #beam_insert select 69599, 'teremana.com', 235, 'white', 'Brand', 'Brand', 8034215702511544698, 1;
insert into #beam_insert select 82238, 'vangoghvodka.com', 235, 'white', 'Brand', 'Brand', 6225573138391853807, 1;
insert into #beam_insert select 83922, 'pinnaclevodka.com', 235, 'white', 'Brand', 'Brand', -2739627664609468125, 1;
insert into #beam_insert select 90112, 'bookersbourbon.com', 235, 'white', 'Brand', 'Brand', 5025885011115850120, 1;
insert into #beam_insert select 63591, 'sidetrackdistillery.com', 235, 'white', 'Brand', 'Brand', -1259321125725696027, 1;
insert into #beam_insert select 46874, 'lobos1707.com', 235, 'white', 'Brand', 'Brand', 908786989222472444, 1;
insert into #beam_insert select 62039, 'seagrams7.com', 235, 'white', 'Brand', 'Brand', -7312463164028767297, 1;
insert into #beam_insert select 112691, 'oldforester.com', 235, 'white', 'Brand', 'Brand', 4691779335791931698, 1;
insert into #beam_insert select 22338, 'crownroyalshop.com', 235, 'white', 'Brand', 'Brand', -7116202775715848382, 1;
insert into #beam_insert select 97556, 'glenmorangie.com', 235, 'white', 'Brand', 'Brand', -1831154296117866471, 1;
insert into #beam_insert select 110403, 'rumhaven.com', 235, 'white', 'Brand', 'Brand', -991569180063825580, 1;
insert into #beam_insert select 13884, 'cognac-expert.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', 3208995839193904676, 1;
insert into #beam_insert select 41218, 'jeffersonsbourbon.com', 235, 'white', 'Brand', 'Brand', -285846319096450263, 1;
insert into #beam_insert select 66494, 'tequila.net', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -6924668703451072151, 1;
insert into #beam_insert select 64695, 'skyyvodka.com', 235, 'white', 'Brand', 'Brand', -7482799528814748548, 1;
insert into #beam_insert select 117588, 'santafespirits.com', 235, 'white', 'Brand', 'Brand', 6080401069314259614, 1;
insert into #beam_insert select 68561, 'svedka.com', 235, 'white', 'Brand', 'Brand', 528339342649405387, 1;
insert into #beam_insert select 75820, 'woodsonwhiskey.com', 235, 'white', 'Brand', 'Brand', -8128890507495450057, 1;
insert into #beam_insert select 106615, 'oldripvanwinkle.com', 235, 'white', 'Brand', 'Brand', 4727076148635033167, 1;
insert into #beam_insert select 107958, 'malts.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -6993996923106839627, 1;
insert into #beam_insert select 52474, 'pyratrum.com', 235, 'white', 'Brand', 'Brand', -1949193227094056251, 1;
insert into #beam_insert select 77780, 'westwardwhiskey.com', 235, 'white', 'Brand', 'Brand', 1135808511848983114, 1;
insert into #beam_insert select 75142, 'tequilaliquorstore.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', 9076865061534509764, 1;
insert into #beam_insert select 26686, 'doshombres.com', 235, 'white', 'Brand', 'Brand', 7928563492093451665, 1;
insert into #beam_insert select 47153, 'mundocuervo.com', 235, 'white', 'Brand', 'Brand', 5334395655864486612, 1;
insert into #beam_insert select 7480, 'bittercube.com', 235, 'white', 'Brand', 'Brand', -404156764308428247, 1;
insert into #beam_insert select 70819, 'volcan.com', 235, 'white', 'Brand', 'Brand', -6857726207720885035, 1;
insert into #beam_insert select 84315, 'rumratings.com', 235, 'white', 'Media/Reviews', 'Alcohol News/Info/Forums', -8623570221808074132, 1;
insert into #beam_insert select 106483, 'luxardo.it', 235, 'white', 'Brand', 'Brand', -2063344163058422555, 1;
insert into #beam_insert select 105947, 'noletsgin.com', 235, 'white', 'Brand', 'Brand', -7721506872792472274, 1;
insert into #beam_insert select 106507, 'lyres.com', 235, 'white', 'Brand', 'Brand', -3096674275055754655, 1;
insert into #beam_insert select 129249, 'sailorjerry.com', 235, 'white', 'Brand', 'Brand', -1267511178595230549, 1;
insert into #beam_insert select 109497, 'nikka.com', 235, 'white', 'Brand', 'Brand', -490650397654163155, 1;
insert into #beam_insert select 66897, 'topwhiskies.com', 235, 'white', 'Category Retailer', 'Alcohol Online Delivery', -7556279288287566464, 1;
insert into #beam_insert select 26372, 'genuinebritishrum.com', 235, 'white', 'Brand', 'Brand', -1373398737143083613, 1;
insert into #beam_insert select 35819, 'kahlua.com', 235, 'white', 'Brand', 'Brand', -818321907247183563, 1;
insert into #beam_insert select 41334, 'jimbeamhighballsweeps.com', 235, 'white', 'Brand', 'Brand', 5929516568824147149, 1;
insert into #beam_insert select 42619, 'montelobos.com', 235, 'white', 'Brand', 'Brand', 1625195486240257222, 1;
insert into #beam_insert select 63540, 'torresbrandyzerochallenge.com', 235, 'white', 'Brand', 'Brand', -2164323155003185056, 1;
insert into #beam_insert select 74550, 'totalwine.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 83298504326354370, 1;
insert into #beam_insert select 20453, 'drizly.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -7680155785523360454, 1;
insert into #beam_insert select 42224, 'liquor.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -7698877251598764119, 1;
insert into #beam_insert select 84204, 'reservebar.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -6775062617034054371, 1;
insert into #beam_insert select 77763, 'yelp.com', 235, 'purple', 'Media/Reviews', 'General News/Info', -2244306793826350042, 1;
insert into #beam_insert select 34700, 'instacart.com', 235, 'purple', 'General Retailer', 'Online FMCG', -5841005759506244082, 0;
insert into #beam_insert select 73581, 'vinepair.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -2444302061694632178, 1;
insert into #beam_insert select 82872, 'wine-searcher.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -5207344372656146518, 1;
insert into #beam_insert select 2237, 'acouplecooks.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6750042068926435790, 1;
insert into #beam_insert select 70563, 'thewhiskyexchange.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -5335576573415578636, 1;
insert into #beam_insert select 2221, 'allrecipes.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 3643417106880268250, 1;
insert into #beam_insert select 11626, 'caskcartel.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 4358317968217331132, 1;
insert into #beam_insert select 37769, 'meijer.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 3928308567954617166, 1;
insert into #beam_insert select 85085, 'thewhiskeywash.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -772750750847849885, 1;
insert into #beam_insert select 8543, 'buffalotracedistillery.com', 235, 'purple', 'Brand', 'Brand', -8923535705964125176, 1;
insert into #beam_insert select 79847, 'bevmo.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 7183025166726622798, 1;
insert into #beam_insert select 28215, 'finewineandgoodspirits.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -8444460866136992928, 1;
insert into #beam_insert select 24796, 'foodnetwork.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 194885558703464492, 1;
insert into #beam_insert select 65579, 'skinnygirlcocktails.com', 235, 'purple', 'Brand', 'Brand', -9143752721667473813, 1;
insert into #beam_insert select 12512, 'caskers.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -8645175750887287285, 1;
insert into #beam_insert select 1099, 'abcfws.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 6692499085813327583, 1;
insert into #beam_insert select 17273, 'cocktailbuilder.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -8883768917031373432, 1;
insert into #beam_insert select 79485, 'specsonline.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -9072204108762195891, 1;
insert into #beam_insert select 21371, 'doordash.com', 235, 'purple', 'General Retailer', 'Online FMCG', -188050129032597773, 0;
insert into #beam_insert select 68269, 'topshelfwineandspirits.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -1672284995752900776, 1;
insert into #beam_insert select 75571, 'woodencork.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 1939554139540498798, 1;
insert into #beam_insert select 26226, 'foodandwine.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 7517486344152890176, 1;
insert into #beam_insert select 72364, 'wineliquordeliver.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -5517201305483670488, 1;
insert into #beam_insert select 68717, 'tasteofhome.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 6942421297845764624, 1;
insert into #beam_insert select 73074, 'thespiritsbusiness.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -5811375113955082847, 1;
insert into #beam_insert select 46685, 'oldtowntequila.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -8814140875288298486, 1;
insert into #beam_insert select 41092, 'grubhub.com', 235, 'purple', 'General Retailer', 'Online FMCG', -3492642459839144740, 0;
insert into #beam_insert select 35802, 'kroger.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -1806506659121083630, 1;
insert into #beam_insert select 61564, 'wine.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -771007249347462881, 1;
insert into #beam_insert select 39783, 'gopuff.com', 235, 'purple', 'General Retailer', 'Online FMCG', 8391568004970065198, 0;
insert into #beam_insert select 20002, 'eater.com', 235, 'purple', 'Media/Reviews', 'Lifestyle', 3149294728746187125, 1;
insert into #beam_insert select 9116, 'bonappetit.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6652045260592392935, 1;
insert into #beam_insert select 79817, 'binnys.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -395976537011797722, 1;
insert into #beam_insert select 46071, 'nestorliquor.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -7245480113915538295, 1;
insert into #beam_insert select 22427, 'diffordsguide.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -3490731226474457843, 1;
insert into #beam_insert select 15493, 'eatthis.com', 235, 'purple', 'Media/Reviews', 'Lifestyle', -8446497751596640388, 1;
insert into #beam_insert select 37032, 'masterofmalt.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -3009797138366878673, 1;
insert into #beam_insert select 44380, 'marketviewliquor.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -5307040627321339877, 1;
insert into #beam_insert select 44205, 'missionliquor.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 3225684041176651626, 1;
insert into #beam_insert select 13726, 'cocktailsdistilled.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2651996511502331022, 1;
insert into #beam_insert select 85555, 'winechateau.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 3680926949105881511, 1;
insert into #beam_insert select 32301, 'liquorbarn.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 7505284297072719741, 1;
insert into #beam_insert select 58976, 'seriouseats.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2418584896260924803, 1;
insert into #beam_insert select 84099, 'qualityliquorstore.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -5212337568606123688, 1;
insert into #beam_insert select 58558, 'mashed.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 1419586533487155772, 1;
insert into #beam_insert select 16146, 'delmesaliquor.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -7703641957689459313, 1;
insert into #beam_insert select 75738, 'ubereats.com', 235, 'purple', 'General Retailer', 'Online FMCG', -3618271071182421992, 0;
insert into #beam_insert select 83262, 'mixthatdrink.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 6398573679491487532, 1;
insert into #beam_insert select 18896, 'crownwineandspirits.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 1696089966765327772, 1;
insert into #beam_insert select 80867, 'drinkhacker.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 3012743216450914396, 1;
insert into #beam_insert select 26864, 'food52.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 1410768693909342375, 1;
insert into #beam_insert select 68281, 'saucey.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -5989276658032893292, 1;
insert into #beam_insert select 79185, 'yummly.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 8967489146172746211, 1;
insert into #beam_insert select 4799, 'bbcgoodfood.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 9096927416052243420, 1;
insert into #beam_insert select 7831, 'bevvy.co', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -4379770163843527762, 1;
insert into #beam_insert select 60406, 'thebourboncentral.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -7581016326635201835, 1;
insert into #beam_insert select 57983, 'punchdrink.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 1531569844951333390, 1;
insert into #beam_insert select 57977, 'publix.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -1484273095790474632, 1;
insert into #beam_insert select 26940, 'food.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6995686254624453544, 1;
insert into #beam_insert select 84018, 'primetimeliquor.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 8021377612964569240, 1;
insert into #beam_insert select 42908, 'liquorbardelivery.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 5600755882360598923, 1;
insert into #beam_insert select 13854, 'distillerytrail.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -4210702932458311720, 1;
insert into #beam_insert select 62802, 'tipsybartender.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 1313781613144097326, 1;
insert into #beam_insert select 39921, 'gotoliquorstore.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 7029950509912185336, 1;
insert into #beam_insert select 70658, 'theliquorbarn.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -7464118784763485037, 1;
insert into #beam_insert select 6921, 'brit.co', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 4154453563855165669, 1;
insert into #beam_insert select 23722, 'epicurious.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 4957757542602190483, 1;
insert into #beam_insert select 28349, 'frootbat.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -1513031876446329807, 1;
insert into #beam_insert select 82847, 'winemag.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 494232326596329565, 1;
insert into #beam_insert select 6618, 'bottledprices.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -4487401095812466190, 1;
insert into #beam_insert select 67411, 'troprockin.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 6505830063107317543, 1;
insert into #beam_insert select 73388, 'uptownspirits.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 4616197465496253439, 1;
insert into #beam_insert select 23026, 'curiada.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -8772794261048361447, 1;
insert into #beam_insert select 37068, 'ishopliquor.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -8784607339556738819, 1;
insert into #beam_insert select 44316, 'kybourbontrail.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 1915648048984922804, 1;
insert into #beam_insert select 61969, 'worldmarket.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 7365038988601519795, 0;
insert into #beam_insert select 42393, 'liquorama.net', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 7552245576120936110, 1;
insert into #beam_insert select 44132, 'mrbostondrinks.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 4612537839877833589, 1;
insert into #beam_insert select 62983, 'saveur.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 5676139636952597876, 1;
insert into #beam_insert select 32886, 'hy-vee.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -3801085488558854211, 1;
insert into #beam_insert select 46154, 'myrecipes.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -2042073193290922567, 1;
insert into #beam_insert select 40481, 'mantitlement.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -100126741743951244, 1;
insert into #beam_insert select 74443, 'warehousewinesandspirits.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -5217205318835058444, 1;
insert into #beam_insert select 25636, 'gimmesomeoven.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -9179806577830791577, 1;
insert into #beam_insert select 17002, 'cwspirits.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 3282824051763935232, 1;
insert into #beam_insert select 54352, 'remedyliquor.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -6461072148376835019, 1;
insert into #beam_insert select 46615, 'liquorstars.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -8158351928437057093, 1;
insert into #beam_insert select 69518, 'twinliquors.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 6792158597005228979, 1;
insert into #beam_insert select 1309, 'absolutdrinks.com', 235, 'purple', 'Brand', 'Brand', -7364162631751080643, 1;
insert into #beam_insert select 67037, 'thecocktailproject.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -174472614803146806, 1;
insert into #beam_insert select 15686, 'completecocktails.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -5912360175101160092, 1;
insert into #beam_insert select 53325, 'talesofthecocktail.org', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 6035119290500891624, 1;
insert into #beam_insert select 37250, 'gastronomblog.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2631462289400145993, 1;
insert into #beam_insert select 14042, 'craftshack.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -549587851011627948, 1;
insert into #beam_insert select 54819, 'thecookierookie.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -1640545792289814242, 1;
insert into #beam_insert select 39432, 'kegnbottle.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -3831768689857085228, 1;
insert into #beam_insert select 65426, 'thebarreltap.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 5675833725506720775, 1;
insert into #beam_insert select 23112, 'dominos.com', 235, 'purple', 'General Retailer', 'Online FMCG', 1629992075488270959, 0;
insert into #beam_insert select 16543, 'craftybartending.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -7716717530683627831, 1;
insert into #beam_insert select 57832, 'sgproof.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -2464288586673895540, 1;
insert into #beam_insert select 235, '1000corks.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -5873893648415531337, 1;
insert into #beam_insert select 29675, 'hellofresh.com', 235, 'purple', 'General Retailer', 'Online FMCG', 816130909018941842, 0;
insert into #beam_insert select 49701, 'mashandgrape.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 5264425438647608186, 1;
insert into #beam_insert select 40601, 'leesliquorlv.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 5599364658591219337, 1;
insert into #beam_insert select 8340, 'bartesian.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 6628780277053798683, 1;
insert into #beam_insert select 15519, 'cocktailpartyapp.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -5173163012914644380, 1;
insert into #beam_insert select 27029, 'heb.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -2933959173513067795, 1;
insert into #beam_insert select 61164, 'wikiliq.org', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -7872400661711677727, 1;
insert into #beam_insert select 71864, 'thefeedfeed.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -7524078804138637728, 1;
insert into #beam_insert select 9042, 'beveragedynamics.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 7861955967845756228, 1;
insert into #beam_insert select 44399, 'nationwideliquor.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 4430470432339749416, 1;
insert into #beam_insert select 43917, 'makemeacocktail.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -9166795093808285, 1;
insert into #beam_insert select 14718, 'chipsliquor.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 6661224271854686567, 1;
insert into #beam_insert select 12461, 'bottlebuzz.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 4868692085658943517, 1;
insert into #beam_insert select 66118, 'tasty.co', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -341086939996026383, 1;
insert into #beam_insert select 39704, 'goodygoody.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -7660986961513218030, 1;
insert into #beam_insert select 11749, 'chilledmagazine.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 589953576167103042, 1;
insert into #beam_insert select 12964, 'cookieandkate.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -4565377524737426538, 1;
insert into #beam_insert select 5681, 'bakingbeauty.net', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -5601322206950532316, 1;
insert into #beam_insert select 71632, 'thirtyonewhiskey.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 5162172994351024631, 1;
insert into #beam_insert select 77159, 'wideopeneats.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2244620820125288695, 1;
insert into #beam_insert select 52414, 'oregonliquorsearch.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -795725142385223145, 1;
insert into #beam_insert select 24906, 'hitimewine.net', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 8817789964783836858, 1;
insert into #beam_insert select 21023, 'dutyfreeamericas.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 5489650798167665681, 1;
insert into #beam_insert select 6614, 'bardstownbourbon.com', 235, 'purple', 'Brand', 'Brand', -4285725284081078903, 1;
insert into #beam_insert select 35799, 'kegworks.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 5852756696988243805, 1;
insert into #beam_insert select 81791, 'halfbakedharvest.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -161952397469833061, 1;
insert into #beam_insert select 73541, 'vsliquor.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -4531741906072755328, 1;
insert into #beam_insert select 7825, 'bourbonbanter.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 5953836933400955957, 1;
insert into #beam_insert select 11860, 'breadboozebacon.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 6598076826007506630, 1;
insert into #beam_insert select 78966, 'wellplated.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 1802626503784097010, 1;
insert into #beam_insert select 68872, 'thekitchn.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -2995771553875733725, 1;
insert into #beam_insert select 76513, 'vivino.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -5825470902030885942, 1;
insert into #beam_insert select 15380, 'cocktailcontessa.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 4599777389231596266, 1;
insert into #beam_insert select 84489, 'sendgifts.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -7956525169846684007, 1;
insert into #beam_insert select 19462, 'distilledspirits.org', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -501269866648536213, 1;
insert into #beam_insert select 30253, 'inspiredtaste.net', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 3326546209640828310, 1;
insert into #beam_insert select 69085, 'tastingtable.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 7488263720862995691, 1;
insert into #beam_insert select 24725, 'foodfornet.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -5124521988897817242, 1;
insert into #beam_insert select 36274, 'insanelygoodrecipes.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 8460498841747132229, 1;
insert into #beam_insert select 77459, 'woodswholesalewine.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 3960847170886145280, 1;
insert into #beam_insert select 22764, 'fredminnick.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -8192926336834822253, 1;
insert into #beam_insert select 34240, 'homemadefoodjunkie.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -7793774887420006065, 1;
insert into #beam_insert select 14882, 'costplusliquors.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -3920698071029329947, 1;
insert into #beam_insert select 73571, 'whiskyshopusa.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -8194275001197813375, 1;
insert into #beam_insert select 128661, 'whiskyshop.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 7169768323325707317, 1;
insert into #beam_insert select 68231, 'sudsandspirits.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 4945293854970219975, 1;
insert into #beam_insert select 31494, 'homemadehooplah.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6205106972256640297, 1;
insert into #beam_insert select 80507, 'crownliquors.net', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 2468370164752735114, 1;
insert into #beam_insert select 25777, 'freshdirect.com', 235, 'purple', 'General Retailer', 'Online FMCG', 7235151100691250599, 0;
insert into #beam_insert select 50022, 'smartandfinal.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 1745208813600210852, 0;
insert into #beam_insert select 83084, 'luekensliquors.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -7718440595689730665, 1;
insert into #beam_insert select 70425, 'shopsk.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -3647967156588547694, 1;
insert into #beam_insert select 35987, 'kentuckybourbonwhiskey.com', 235, 'purple', 'Brand', 'Brand', 5720069837017592072, 1;
insert into #beam_insert select 8591, 'bigtimemarketandliquor.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -6619926147149918428, 1;
insert into #beam_insert select 64898, 'shopboozy.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 2515890559014435174, 1;
insert into #beam_insert select 59449, 'realhousemoms.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 464198251980209119, 1;
insert into #beam_insert select 18140, 'dandm.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -8319088919748113822, 1;
insert into #beam_insert select 77883, 'whiskyliquorstore.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -1847950737717988585, 1;
insert into #beam_insert select 118346, 'spiritedgifts.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 8453578298400348400, 1;
insert into #beam_insert select 1949, 'acommunaltable.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -195756345387489972, 1;
insert into #beam_insert select 60292, 'shipliquor101.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 5497305838461953014, 1;
insert into #beam_insert select 6556, 'barnonedrinks.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 5929237107423082048, 1;
insert into #beam_insert select 31347, 'haveacocktail.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -5206064756388188560, 1;
insert into #beam_insert select 11818, 'crazyforcrust.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -4432015501160753583, 1;
insert into #beam_insert select 2546, 'alcademics.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -3399442236663600203, 1;
insert into #beam_insert select 67316, 'theflavorbender.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -2852614255242837163, 1;
insert into #beam_insert select 49814, 'mybevstore.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 6065431517017036411, 1;
insert into #beam_insert select 9595, 'californiawineryadvisor.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 1292156266657336506, 1;
insert into #beam_insert select 26162, 'finecask.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 5604167329120651632, 1;
insert into #beam_insert select 73641, 'winetoship.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 1372903696525017877, 1;
insert into #beam_insert select 37762, 'liquorista.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 5011016938658636952, 1;
insert into #beam_insert select 54119, 'redh.co.uk', 235, 'purple', 'Brand', 'Brand', -7759499033120949080, 1;
insert into #beam_insert select 71772, 'urban-drinks.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -2721981491014642639, 1;
insert into #beam_insert select 63511, 'shopwinedirect.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -2405938400030119292, 1;
insert into #beam_insert select 49969, 'justapinch.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 5594559460520196001, 1;
insert into #beam_insert select 60932, 'thedrinksbusiness.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 9079887310133214208, 1;
insert into #beam_insert select 40334, 'maxliquor.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 2119097319925193346, 1;
insert into #beam_insert select 76445, 'wedeliverstores.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -498102891551716151, 1;
insert into #beam_insert select 65971, 'regencyliquor.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -2933605562337862515, 1;
insert into #beam_insert select 56466, 'thirdbasemarketandspirits.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -7075186339351092144, 1;
insert into #beam_insert select 124532, 'whiskymag.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -6801808345709618351, 1;
insert into #beam_insert select 67765, 'tastings.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 6337312663936653059, 1;
insert into #beam_insert select 31656, 'gianteagle.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -3919746109459252961, 1;
insert into #beam_insert select 109133, 'rabbitholedistillery.com', 235, 'purple', 'Brand', 'Brand', -8216864646486548708, 1;
insert into #beam_insert select 49137, 'naturallight.com', 235, 'purple', 'Brand', 'Brand', -3472214482224245093, 1;
insert into #beam_insert select 13793, 'cocktailcourier.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -8235622337064116956, 1;
insert into #beam_insert select 26183, 'gacraftspirits.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -3173172619517033746, 1;
insert into #beam_insert select 2372, 'albertsons.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -2123573172971618633, 1;
insert into #beam_insert select 52864, 'originalabsinthe.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -6864147114713600667, 1;
insert into #beam_insert select 63575, 'seelbachs.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -2885935922883175260, 1;
insert into #beam_insert select 17853, 'festfoods.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -679192478343112531, 1;
insert into #beam_insert select 13565, 'calorieking.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2652364847358262085, 1;
insert into #beam_insert select 52747, 'montaigneimports.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 4440866178673538220, 1;
insert into #beam_insert select 61287, 'southerncomfort.com', 235, 'white', 'Brand', 'Brand', 2450710479431533462, 1;
insert into #beam_insert select 111654, 'norfolkwineandspirits.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 955043839982953677, 1;
insert into #beam_insert select 65184, 'thebarrelroom.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 7388120966717517289, 1;
insert into #beam_insert select 82823, 'liquorwinetime.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -4633456391681760199, 1;
insert into #beam_insert select 71423, 'thebottledprices.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 365008353437544548, 1;
insert into #beam_insert select 37529, 'liquorstore-online.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -1698142284825904076, 1;
insert into #beam_insert select 50211, 'pamperedchef.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -890233097141398033, 1;
insert into #beam_insert select 12479, 'bottlerepublic.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -4560475827334846935, 1;
insert into #beam_insert select 15823, 'copykat.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6451009390358909012, 1;
insert into #beam_insert select 45936, 'nc-whiskey.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 1748051951398638376, 1;
insert into #beam_insert select 57948, 'rancholiquoronline.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', 5507003139581340942, 1;
insert into #beam_insert select 12233, 'cakenknife.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -1243241264235898911, 1;
insert into #beam_insert select 51592, 'negroniweek.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -2427469911931777576, 1;
insert into #beam_insert select 61954, 'spendwithpennies.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 1253571088298664604, 1;
insert into #beam_insert select 89812, 'bestbuyliquors.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -3390360723875933151, 1;
insert into #beam_insert select 46383, 'moonshinerecipe.org', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -5212768051272671136, 1;
insert into #beam_insert select 4244, 'argonautliquor.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -8911016191358951944, 1;
insert into #beam_insert select 109099, 'mybottleshop.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 190682531646599407, 1;
insert into #beam_insert select 77880, 'whisky.fr', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 9066710809614182857, 1;
insert into #beam_insert select 108007, 'mintjuleptours.com', 235, 'purple', 'Brand', 'Brand', -5741333332442346722, 1;
insert into #beam_insert select 17548, 'damndelicious.net', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -3708894809673474027, 1;
insert into #beam_insert select 62160, 'thestayathomechef.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -7906525733048482371, 1;
insert into #beam_insert select 70331, 'thedailymeal.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -9165838846643044807, 1;
insert into #beam_insert select 56156, 'safeway.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -6522025765500419840, 1;
insert into #beam_insert select 112531, 'spiritsandspice.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -6033331480601874058, 1;
insert into #beam_insert select 34235, 'howsweeteats.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -9079111045015521920, 1;
insert into #beam_insert select 78697, 'untappd.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 1199698959361842443, 1;
insert into #beam_insert select 60317, 'simplyrecipes.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 3138329025272098496, 1;
insert into #beam_insert select 84005, 'postmates.com', 235, 'purple', 'General Retailer', 'Online FMCG', -3016718816913539915, 0;
insert into #beam_insert select 26436, 'fredmeyer.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 5931113975325073817, 1;
insert into #beam_insert select 51983, 'stayrunners.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', -6192274891316309414, 1;
insert into #beam_insert select 68697, 'tastetequila.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 3911515809012750505, 1;
insert into #beam_insert select 3014, 'alcoholprofessor.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 1395999653149447265, 1;
insert into #beam_insert select 81496, 'thetakeout.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 3770881999841894576, 1;
insert into #beam_insert select 59934, 'tequilareviews.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -3701989126687394368, 1;
insert into #beam_insert select 89247, 'bevindustry.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 5827023331781439142, 1;
insert into #beam_insert select 75760, 'wineonlinedelivery.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 5869603316440362075, 1;
insert into #beam_insert select 119196, 'spiritedbiz.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -2987127077996770691, 1;
insert into #beam_insert select 57730, 'platingsandpairings.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 599148276865353244, 1;
insert into #beam_insert select 28318, 'firstwefeast.com', 235, 'purple', 'Media/Reviews', 'Lifestyle', 8874182041456863691, 1;
insert into #beam_insert select 84824, 'taketwotapas.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2897244154165359953, 1;
insert into #beam_insert select 77235, 'youbooze.com', 235, 'purple', 'Category Retailer', 'Alcohol Online Delivery', 7028518102016989322, 1;
insert into #beam_insert select 22262, 'foodwithfeeling.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 4789137723235508455, 1;
insert into #beam_insert select 28873, 'foodiecrush.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6736401615647962747, 1;
insert into #beam_insert select 60907, 'whiskymonster.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', -4173143321980588471, 1;
insert into #beam_insert select 65683, 'thechunkychef.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 5128202791674473717, 1;
insert into #beam_insert select 2529, 'aldi.us', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 1567842471313198161, 1;
insert into #beam_insert select 33742, 'marianos.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 5529124271519129624, 1;
insert into #beam_insert select 77263, 'winndixie.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 2671677287655991086, 1;
insert into #beam_insert select 85058, 'thereciperebel.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 1838932315292652940, 1;
insert into #beam_insert select 87720, 'applejack.com', 235, 'purple', 'Category Retailer', 'Brick + Click Liquor', -7805995728861983651, 1;
insert into #beam_insert select 84126, 'ralphs.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -8579642818630516394, 1;
insert into #beam_insert select 40801, 'lidl.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -6829537671604846345, 1;
insert into #beam_insert select 24385, 'farmwifedrinks.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 1971666224057459680, 1;
insert into #beam_insert select 50884, 'keyingredient.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -991351768177058403, 1;
insert into #beam_insert select 41050, 'mexicoinmykitchen.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -749424673435145350, 1;
insert into #beam_insert select 27580, 'foxtrotco.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 6765673813254101429, 1;
insert into #beam_insert select 29074, 'garnishwithlemon.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -4468199109942113892, 1;
insert into #beam_insert select 68714, 'stevethebartender.com.au', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -843216191851830406, 1;
insert into #beam_insert select 33317, 'homebrewacademy.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 3860337922468531488, 1;
insert into #beam_insert select 81609, 'torani.com', 235, 'purple', 'Brand', 'Brand', 7240090528689869473, 1;
insert into #beam_insert select 17539, 'culinaryhill.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 7686364968840743556, 1;
insert into #beam_insert select 18114, 'dinneratthezoo.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 518115735611759057, 1;
insert into #beam_insert select 44522, 'natashaskitchen.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 89647157012151540, 1;
insert into #beam_insert select 69570, 'thekitchenmagpie.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 3998780482321376059, 1;
insert into #beam_insert select 81275, 'foodtown.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 7812190937636829104, 1;
insert into #beam_insert select 75211, 'vons.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 4538400074196631726, 1;
insert into #beam_insert select 82613, 'kitchenfunwithmy3sons.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 4987393570807993248, 1;
insert into #beam_insert select 59168, 'rasamalaysia.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 5916644619348120624, 1;
insert into #beam_insert select 8110, 'biggerbolderbaking.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 1322371478579048242, 1;
insert into #beam_insert select 46848, 'ingles-markets.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -3776033768863732110, 1;
insert into #beam_insert select 83393, 'myfoodandfamily.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 7934984451592661610, 1;
insert into #beam_insert select 7403, 'butteryourbiscuit.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6848943593853026022, 1;
insert into #beam_insert select 23907, 'fareway.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -7583572713680186780, 1;
insert into #beam_insert select 70091, 'theslowroasteditalian.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -5383422272015063452, 1;
insert into #beam_insert select 74777, 'wholesomeyum.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2896450421760868924, 1;
insert into #beam_insert select 33449, 'grouprecipes.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 4756571134120330503, 1;
insert into #beam_insert select 17800, 'dailyappetite.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6094692631843354907, 1;
insert into #beam_insert select 5250, 'aspicyperspective.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2496981148580526996, 1;
insert into #beam_insert select 19635, 'dinnerthendessert.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -3067509934839155999, 1;
insert into #beam_insert select 47492, 'momontimeout.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 7019248887037348701, 1;
insert into #beam_insert select 65780, 'thecountrycook.net', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 8824435035521749572, 1;
insert into #beam_insert select 31653, 'giantfoodstores.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -9032131511115947457, 1;
insert into #beam_insert select 77075, 'wholefoodsmarket.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -915929712736986832, 1;
insert into #beam_insert select 57436, 'traderjoes.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -4551582098299931591, 1;
insert into #beam_insert select 26013, 'haagendazs.us', 235, 'purple', 'Brand', 'Brand', 1862352099544713093, 1;
insert into #beam_insert select 74639, 'whattheforkfoodblog.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6711642631295502119, 1;
insert into #beam_insert select 36939, 'leitesculinaria.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2827350076220868151, 1;
insert into #beam_insert select 40276, 'lecremedelacrumb.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -8991195671537195048, 1;
insert into #beam_insert select 2621, 'alldayidreamaboutfood.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -1511099306371529532, 1;
insert into #beam_insert select 31773, 'lemonblossoms.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -1416127371652036210, 1;
insert into #beam_insert select 6622, 'beermenus.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 8292862654725151754, 1;
insert into #beam_insert select 21509, 'delishably.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 3651448196838131681, 1;
insert into #beam_insert select 1960, 'alcoholvolume.com', 235, 'purple', 'Media/Reviews', 'Alcohol News/Info/Forums', 924369344511335366, 1;
insert into #beam_insert select 28505, 'jamieoliver.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6259355287517224092, 1;
insert into #beam_insert select 36395, 'jessicagavin.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -4471659736756538330, 1;
insert into #beam_insert select 29106, 'hannaford.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -8223956144004546724, 1;
insert into #beam_insert select 25977, 'fooducate.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 5132749285699773830, 1;
insert into #beam_insert select 52241, 'picknsave.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', -6203547627451344400, 1;
insert into #beam_insert select 2439, 'alexandracooks.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -5542499827299089605, 1;
insert into #beam_insert select 6306, 'bakingbites.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 3139145961751187055, 1;
insert into #beam_insert select 22543, 'foxandbriar.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -9015397625556458439, 1;
insert into #beam_insert select 46058, 'noblepig.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2801370418337172821, 1;
insert into #beam_insert select 66784, 'super1foods.com', 235, 'purple', 'General Retailer', 'Brick + Click Grocery', 2805435496801553872, 1;
insert into #beam_insert select 24997, 'finecooking.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -4762465014358810560, 1;
insert into #beam_insert select 48466, 'myincrediblerecipes.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -5533981738601109176, 1;
insert into #beam_insert select 65993, 'steamykitchen.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -7496475598168218959, 1;
insert into #beam_insert select 32465, 'how2heroes.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 3691337462838957328, 1;
insert into #beam_insert select 27667, 'gimmedelicious.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 1748040143348530278, 1;
insert into #beam_insert select 66859, 'therecipecritic.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 2135347352475193470, 1;
insert into #beam_insert select 16187, 'cookingclassy.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -7155122696063496570, 1;
insert into #beam_insert select 34667, 'hostessatheart.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 6163982646353529568, 1;
insert into #beam_insert select 29851, 'ketoconnect.net', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -1599660329939364501, 1;
insert into #beam_insert select 69844, 'theviewfromgreatisland.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -7550364439806146951, 1;
insert into #beam_insert select 79461, 'amazon.com', 235, 'green', 'General Retailer', 'Online FMCG', -76485819139516796, 0;
insert into #beam_insert select 22256, 'facebook.com', 235, 'green', 'Social', 'Social: Facebook (Web)', 2013244425497865015, 0;
insert into #beam_insert select 75450, 'wikipedia.org', 235, 'green', 'Media/Reviews', 'General News/Info', -2667028083807682303, 1;
insert into #beam_insert select 72120, 'thespruceeats.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', -6226252391353436256, 1;
insert into #beam_insert select 53991, 'reddit.com', 235, 'green', 'Media/Reviews', 'General News/Info', -6608813955292750754, 1;
insert into #beam_insert select 77887, 'youtube.com', 235, 'green', 'Video', 'YouTube', -5467178492790369656, 1;
insert into #beam_insert select 24185, 'etsy.com', 235, 'green', 'General Retailer', 'Online FMCG', 1746144908312548178, 0;
insert into #beam_insert select 76829, 'walmart.com', 235, 'green', 'General Retailer', 'Brick + Click Grocery', -3034774944025059528, 0;
insert into #beam_insert select 73374, 'uproxx.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 1839652258355926261, 1;
insert into #beam_insert select 64401, 'target.com', 235, 'green', 'General Retailer', 'Brick + Click Grocery', 5057445096335334564, 0;
insert into #beam_insert select 33912, 'instagram.com', 235, 'green', 'Social', 'Social: Instagram (Web)', 3575504484751647278, 1;
insert into #beam_insert select 17552, 'delish.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 3844061705166012151, 1;
insert into #beam_insert select 72525, 'thrillist.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 1807427927042955238, 1;
insert into #beam_insert select 78539, 'twitter.com', 235, 'green', 'Social', 'Social: Twitter (Web)', 407662039927155139, 1;
insert into #beam_insert select 26992, 'forbes.com', 235, 'green', 'Media/Reviews', 'General News/Info', -4337549941328668374, 1;
insert into #beam_insert select 28301, 'ebay.com', 235, 'green', 'General Retailer', 'Online FMCG', 5681545045206564693, 0;
insert into #beam_insert select 48597, 'quora.com', 235, 'green', 'Media/Reviews', 'General News/Info', -6457916762909056244, 1;
insert into #beam_insert select 80500, 'craigslist.org', 235, 'green', 'General Retailer', 'Online FMCG', 5471149359687583867, 0;
insert into #beam_insert select 69049, 'townandcountrymag.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 4188402558852415738, 1;
insert into #beam_insert select 79139, 'yahoo.com', 235, 'green', 'Media/Reviews', 'General News/Info', 9101809637679198610, 1;
insert into #beam_insert select 23239, 'esquire.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -1060290994869448433, 1;
insert into #beam_insert select 29161, 'juliesfreebies.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 2886671968707668315, 0;
insert into #beam_insert select 50146, 'ndtv.com', 235, 'green', 'Media/Reviews', 'General News/Info', -8088739364986844622, 1;
insert into #beam_insert select 107815, 'luxe.digital', 235, 'green', 'Media/Reviews', 'Lifestyle', -3906974964978868517, 1;
insert into #beam_insert select 26315, 'gearpatrol.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 8094562985059481972, 1;
insert into #beam_insert select 57396, 'pinterest.com', 235, 'green', 'Social', 'Social: All Other (Web)', 3274868168098172422, 1;
insert into #beam_insert select 56936, 'lovetoknow.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -577370384719199978, 1;
insert into #beam_insert select 57756, 'nytimes.com', 235, 'green', 'Media/Reviews', 'General News/Info', 7951929796602746881, 1;
insert into #beam_insert select 56230, 'robbreport.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -2426306897863566468, 1;
insert into #beam_insert select 66507, 'themanual.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -1138365213399844504, 1;
insert into #beam_insert select 66533, 'spoonuniversity.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 5172656891848710727, 1;
insert into #beam_insert select 45461, 'msn.com', 235, 'green', 'Media/Reviews', 'General News/Info', -6652869225107881923, 1;
insert into #beam_insert select 32759, 'insidehook.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -3226324476286980797, 1;
insert into #beam_insert select 14749, 'costco.com', 235, 'green', 'General Retailer', 'Brick + Click Grocery', -1135238890807417723, 0;
insert into #beam_insert select 25430, 'freebieshark.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 3556026094238447917, 0;
insert into #beam_insert select 63263, 'samsclub.com', 235, 'green', 'General Retailer', 'Brick + Click Grocery', 5251637678462463719, 0;
insert into #beam_insert select 55820, 'rumble.com', 235, 'green', 'Media/Reviews', 'General News/Info', 230351691001851600, 1;
insert into #beam_insert select 8339, 'binwise.com', 235, 'green', 'Media/Reviews', 'Alcohol News/Info/Forums', -7439884443414416847, 1;
insert into #beam_insert select 56479, 'rollingstone.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 6355407739309850706, 1;
insert into #beam_insert select 31025, 'hiconsumption.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 3630190671402566095, 1;
insert into #beam_insert select 16327, 'couponxoo.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 2269317933878738114, 0;
insert into #beam_insert select 85140, 'touchofmodern.com', 235, 'green', 'General Retailer', 'Online FMCG', 7768022221971504374, 1;
insert into #beam_insert select 25006, 'fandom.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -5018560460662888179, 1;
insert into #beam_insert select 81283, 'freebiemom.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', -2916871545295918330, 0;
insert into #beam_insert select 65700, 'redbubble.com', 235, 'green', 'General Retailer', 'Online FMCG', 3121477590089538477, 1;
insert into #beam_insert select 43308, 'manofmany.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 6032437424673140846, 1;
insert into #beam_insert select 48015, 'myfitnesspal.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -2603347868737422264, 1;
insert into #beam_insert select 50816, 'nypost.com', 235, 'green', 'Media/Reviews', 'General News/Info', -3748552520602820656, 1;
insert into #beam_insert select 78286, 'yellowpages.com', 235, 'green', 'Media/Reviews', 'General News/Info', 1777486456843303635, 1;
insert into #beam_insert select 71230, 'tiktok.com', 235, 'green', 'Social', 'Social: All Other (Web)', 8119134723042495674, 1;
insert into #beam_insert select 324, '10best.com', 235, 'green', 'Media/Reviews', 'General News/Info', 2760377292734850240, 1;
insert into #beam_insert select 10514, 'buzzfeed.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -6709714723963301790, 1;
insert into #beam_insert select 26526, 'hypebeast.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 3593833869450622453, 1;
insert into #beam_insert select 48957, 'people.com', 235, 'green', 'Media/Reviews', 'General News/Info', -8890439181273809415, 1;
insert into #beam_insert select 14133, 'celebrations.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -6279985609459904179, 1;
insert into #beam_insert select 77444, 'worthpoint.com', 235, 'green', 'General Retailer', 'Online FMCG', -3239459222553694581, 1;
insert into #beam_insert select 74959, 'prnewswire.com', 235, 'green', 'Media/Reviews', 'General News/Info', -4339302579042954840, 1;
insert into #beam_insert select 79226, 'yumpu.com', 235, 'green', 'Media/Reviews', 'General News/Info', -3204819779772892808, 1;
insert into #beam_insert select 3442, 'amazon.co.uk', 235, 'green', 'General Retailer', 'Online FMCG', 6403030917794401674, 0;
insert into #beam_insert select 45530, 'medium.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -8986612675140284538, 1;
insert into #beam_insert select 64478, 'skinnymixes.com', 235, 'green', 'Brand', 'Brand', -3679944516832290719, 1;
insert into #beam_insert select 70926, 'thriftbooks.com', 235, 'green', 'Media/Reviews', 'Alcohol News/Info/Forums', -69715902757381961, 1;
insert into #beam_insert select 40752, 'mensjournal.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -4180230567683193100, 1;
insert into #beam_insert select 68468, 'statista.com', 235, 'green', 'Media/Reviews', 'General News/Info', 1507466346522244521, 1;
insert into #beam_insert select 40437, 'mancrates.com', 235, 'green', 'General Retailer', 'Online FMCG', -2711083659385688419, 1;
insert into #beam_insert select 20043, 'dukecannon.com', 235, 'green', 'General Retailer', 'Online FMCG', 6085430900701832841, 1;
insert into #beam_insert select 18754, 'cnn.com', 235, 'green', 'Media/Reviews', 'General News/Info', -2627166575185236643, 1;
insert into #beam_insert select 72740, 'tumblr.com', 235, 'green', 'Social', 'Social: All Other (Web)', -3068571390253582806, 1;
insert into #beam_insert select 22168, 'elitetraveler.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 4871781628028966897, 1;
insert into #beam_insert select 40785, 'menshealth.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -8949299826430956960, 1;
insert into #beam_insert select 42846, 'magsweeps.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 318268003421258827, 0;
insert into #beam_insert select 65705, 'thefreebieguy.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', -8230520541125024149, 0;
insert into #beam_insert select 63847, 'sweetrecipeas.com', 235, 'green', 'Media/Reviews', 'Recipe/Cocktail Info', -4012425679517774641, 1;
insert into #beam_insert select 75428, 'walgreens.com', 235, 'green', 'General Retailer', 'Brick + Click Grocery', -6544929760581969030, 0;
insert into #beam_insert select 4652, 'answerstoall.com', 235, 'green', 'Media/Reviews', 'General News/Info', -5366909154869906453, 1;
insert into #beam_insert select 34662, 'foodviva.com', 235, 'green', 'Media/Reviews', 'Recipe/Cocktail Info', -402448554567813616, 1;
insert into #beam_insert select 42083, 'loc8nearme.com', 235, 'green', 'Media/Reviews', 'General News/Info', -5561935165851379181, 1;
insert into #beam_insert select 42175, 'life-stylez.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -6170938858243414105, 1;
insert into #beam_insert select 84623, 'southernliving.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -2630274073439594134, 1;
insert into #beam_insert select 82813, 'williams-sonoma.com', 235, 'green', 'Brand', 'Brand', -6942639292688108448, 1;
insert into #beam_insert select 30498, 'kohls.com', 235, 'green', 'General Retailer', 'Brick + Click Grocery', -8977301377759961660, 0;
insert into #beam_insert select 82111, 'hunt4freebies.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 8538371131306685640, 1;
insert into #beam_insert select 10334, 'cnbc.com', 235, 'green', 'Media/Reviews', 'General News/Info', 8608022249241212804, 1;
insert into #beam_insert select 74312, 'sweepstakesfanatics.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 6178768487358577617, 1;
insert into #beam_insert select 61301, 'missinthekitchen.com', 235, 'purple', 'Media/Reviews', 'Recipe/Cocktail Info', 6186879011549424495, 1;
insert into #beam_insert select 46519, 'npr.org', 235, 'green', 'Media/Reviews', 'General News/Info', 4190857658020962455, 1;
insert into #beam_insert select 73877, 'washingtonpost.com', 235, 'green', 'Media/Reviews', 'General News/Info', -1121410210939006116, 1;
insert into #beam_insert select 72649, 'vice.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -5363561733444365829, 1;
insert into #beam_insert select 59199, 'review.com.np', 235, 'green', 'Media/Reviews', 'Alcohol News/Info/Forums', -3925627150498878658, 1;
insert into #beam_insert select 101125, 'graziamagazine.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 3194701322666369171, 1;
insert into #beam_insert select 5144, 'askinglot.com', 235, 'green', 'Media/Reviews', 'General News/Info', -8393528053076001563, 1;
insert into #beam_insert select 79977, 'brobible.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -6025945357591241786, 1;
insert into #beam_insert select 79692, 'barandrestaurant.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -4233609071978469699, 1;
insert into #beam_insert select 45488, 'patch.com', 235, 'green', 'Media/Reviews', 'General News/Info', 5064200490865706281, 1;
insert into #beam_insert select 39962, 'onlycontests.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 4139463164552364703, 0;
insert into #beam_insert select 14989, 'countryliving.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -6551280790328933531, 1;
insert into #beam_insert select 33618, 'infinitesweeps.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 6912895594144904062, 1;
insert into #beam_insert select 46299, 'mysavings.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 7449191850835717620, 0;
insert into #beam_insert select 52548, 'pagesix.com', 235, 'green', 'Media/Reviews', 'General News/Info', 6319212572610584435, 1;
insert into #beam_insert select 47295, 'instyle.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -871370858481049503, 1;
insert into #beam_insert select 78127, 'you-him-me.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 3392025657919433479, 1;
insert into #beam_insert select 65266, 'theguardian.com', 235, 'green', 'Media/Reviews', 'General News/Info', -1080128935210355783, 1;
insert into #beam_insert select 58843, 'refinery29.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -4313815869838935931, 1;
insert into #beam_insert select 43124, 'muckrack.com', 235, 'green', 'Media/Reviews', 'General News/Info', 7604448666828160594, 1;
insert into #beam_insert select 21467, 'delawareonline.com', 235, 'green', 'Media/Reviews', 'General News/Info', 6100079852422655818, 1;
insert into #beam_insert select 26839, 'freebieslovers.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 3506762559165372714, 0;
insert into #beam_insert select 74574, 'westernjournal.com', 235, 'green', 'Media/Reviews', 'General News/Info', 2811928625482900545, 1;
insert into #beam_insert select 60560, 'slickdeals.net', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 846861473259073141, 0;
insert into #beam_insert select 45012, 'realsimple.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -6034146100957444517, 1;
insert into #beam_insert select 58278, 'sfgate.com', 235, 'green', 'Media/Reviews', 'General News/Info', -8280337607944108560, 1;
insert into #beam_insert select 33478, 'hgtv.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -4555060800708061095, 1;
insert into #beam_insert select 9336, 'blogspot.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -7631125018215644376, 1;
insert into #beam_insert select 16646, 'couponbirds.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 6306489643911621091, 0;
insert into #beam_insert select 16380, 'cvs.com', 235, 'green', 'General Retailer', 'Brick + Click Grocery', -1180691970267346038, 0;
insert into #beam_insert select 19901, 'dispatch.com', 235, 'green', 'Media/Reviews', 'General News/Info', -7200229174435671902, 1;
insert into #beam_insert select 26452, 'freebiesfrenzy.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 8230823749787602845, 1;
insert into #beam_insert select 7638, 'bespokeunit.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -5344926084285025754, 1;
insert into #beam_insert select 55598, 'popsugar.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -2967457407172390549, 1;
insert into #beam_insert select 43967, 'marthastewart.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -8494477973664226402, 1;
insert into #beam_insert select 36719, 'insider.com', 235, 'green', 'Media/Reviews', 'General News/Info', 1745297517473124998, 1;
insert into #beam_insert select 63834, 'statesman.com', 235, 'green', 'Media/Reviews', 'General News/Info', 342053066700734502, 1;
insert into #beam_insert select 75461, 'usatoday.com', 235, 'green', 'Media/Reviews', 'General News/Info', -1375993956610832210, 1;
insert into #beam_insert select 77338, 'thepioneerwoman.com', 235, 'green', 'Media/Reviews', 'Recipe/Cocktail Info', -6805329352574314169, 1;
insert into #beam_insert select 64256, 'simplemost.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 5694653080730837133, 1;
insert into #beam_insert select 28100, 'gq.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 5514578287616209203, 1;
insert into #beam_insert select 30536, 'goodhousekeeping.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -7398497845331692371, 1;
insert into #beam_insert select 47591, 'oureverydaylife.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 884801435071441929, 1;
insert into #beam_insert select 7331, 'bestlifeonline.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 4912392289332998206, 1;
insert into #beam_insert select 13257, 'britannica.com', 235, 'green', 'Media/Reviews', 'General News/Info', -503270308895369581, 0;
insert into #beam_insert select 5360, 'axios.com', 235, 'green', 'Media/Reviews', 'General News/Info', 3966076650798316338, 1;
insert into #beam_insert select 6171, 'bbc.com', 235, 'green', 'Media/Reviews', 'General News/Info', 349689323521349147, 1;
insert into #beam_insert select 25357, 'fox2detroit.com', 235, 'green', 'Media/Reviews', 'General News/Info', 1482633305427928095, 1;
insert into #beam_insert select 37511, 'hotdeals.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 4105683976360252302, 1;
insert into #beam_insert select 45457, 'mindbodygreen.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -5224806740123719570, 1;
insert into #beam_insert select 70080, 'zebit.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', -1551378529312269700, 0;
insert into #beam_insert select 29648, 'freeflys.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 7858631958991509931, 1;
insert into #beam_insert select 53551, 'observer.com', 235, 'green', 'Media/Reviews', 'General News/Info', 7166424892555848936, 1;
insert into #beam_insert select 18838, 'dmarge.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -8205304982539272340, 1;
insert into #beam_insert select 15440, 'cheapism.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 556125089031428018, 1;
insert into #beam_insert select 52279, 'onmilwaukee.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -4989545911556718846, 1;
insert into #beam_insert select 22878, 'freebieselect.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 2451792451163894830, 0;
insert into #beam_insert select 13079, 'dealspotr.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 8173080984343050185, 1;
insert into #beam_insert select 74733, 'wthr.com', 235, 'green', 'Media/Reviews', 'General News/Info', -8467871331283581822, 1;
insert into #beam_insert select 84880, 'tennessean.com', 235, 'green', 'Media/Reviews', 'General News/Info', 7146517664590684474, 1;
insert into #beam_insert select 67894, 'spoofee.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 5276678272242048960, 1;
insert into #beam_insert select 84058, 'purewow.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 5780052752620154544, 1;
insert into #beam_insert select 75237, 'wlky.com', 235, 'green', 'Media/Reviews', 'General News/Info', 8979683959412651559, 1;
insert into #beam_insert select 80730, 'definition.org', 235, 'green', 'Media/Reviews', 'General News/Info', -3975814215765320240, 0;
insert into #beam_insert select 11828, 'crazyfreebie.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', -2542553520091007391, 0;
insert into #beam_insert select 16837, 'coveteur.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 5071181109429635117, 1;
insert into #beam_insert select 10356, 'boston.com', 235, 'green', 'Media/Reviews', 'General News/Info', -3285470570246853355, 1;
insert into #beam_insert select 8293, 'bhg.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 794581077710144680, 1;
insert into #beam_insert select 9898, 'cbs19news.com', 235, 'green', 'Media/Reviews', 'General News/Info', -7755353102159050119, 1;
insert into #beam_insert select 10175, 'businesswire.com', 235, 'green', 'Media/Reviews', 'General News/Info', 2548441960056059472, 1;
insert into #beam_insert select 3497, 'americanmilitarynews.com', 235, 'green', 'Media/Reviews', 'General News/Info', 7032961254525078430, 1;
insert into #beam_insert select 79618, 'archive.org', 235, 'green', 'Media/Reviews', 'General News/Info', -5630322119852964153, 1;
insert into #beam_insert select 38996, 'globenewswire.com', 235, 'green', 'Media/Reviews', 'General News/Info', -9013882604017330041, 1;
insert into #beam_insert select 39723, 'ktvn.com', 235, 'green', 'Media/Reviews', 'General News/Info', -798982822734092889, 1;
insert into #beam_insert select 82505, 'journalstar.com', 235, 'green', 'Media/Reviews', 'General News/Info', -7455820348187660549, 1;
insert into #beam_insert select 82585, 'kentucky.com', 235, 'green', 'Media/Reviews', 'General News/Info', 3715268440120438542, 1;
insert into #beam_insert select 59432, 'oprah.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 1931239779410476083, 1;
insert into #beam_insert select 83644, 'nymag.com', 235, 'green', 'Media/Reviews', 'General News/Info', 727411936292791257, 1;
insert into #beam_insert select 41159, 'maternityweek.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -9131078820387293438, 1;
insert into #beam_insert select 41751, 'mirror.co.uk', 235, 'green', 'Media/Reviews', 'General News/Info', 2268251721485308777, 1;
insert into #beam_insert select 39117, 'mnggo.net', 235, 'green', 'Media/Reviews', 'Lifestyle', -88537549097432383, 1;
insert into #beam_insert select 41817, 'mentalfloss.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 3560747580994077548, 1;
insert into #beam_insert select 44880, 'mercurynews.com', 235, 'green', 'Media/Reviews', 'General News/Info', -4788505951892127436, 1;
insert into #beam_insert select 59779, 'metro.co.uk', 235, 'green', 'Media/Reviews', 'General News/Info', -8387097741606781727, 1;
insert into #beam_insert select 44780, 'mymilitarysavings.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', -3009315107859082385, 0;
insert into #beam_insert select 45500, 'narcity.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -7244742670166469317, 1;
insert into #beam_insert select 51391, 'nbc12.com', 235, 'green', 'Media/Reviews', 'General News/Info', -7420815003142791791, 1;
insert into #beam_insert select 44610, 'newsofstjohn.com', 235, 'green', 'Media/Reviews', 'General News/Info', -5422210031174913756, 1;
insert into #beam_insert select 38243, 'newyorker.com', 235, 'green', 'Media/Reviews', 'General News/Info', 3078784406372416941, 1;
insert into #beam_insert select 84048, 'propublica.org', 235, 'green', 'Media/Reviews', 'General News/Info', -8530954660236543453, 1;
insert into #beam_insert select 54605, 'phoenixnewtimes.com', 235, 'green', 'Media/Reviews', 'General News/Info', 988378407395050041, 1;
insert into #beam_insert select 72018, 'pennlive.com', 235, 'green', 'Media/Reviews', 'General News/Info', -2629600137972970550, 1;
insert into #beam_insert select 48897, 'peoplesworld.org', 235, 'green', 'Media/Reviews', 'General News/Info', 8281214991306625174, 1;
insert into #beam_insert select 70990, 'parade.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -6279020920982207371, 1;
insert into #beam_insert select 47165, 'oregonlive.com', 235, 'green', 'Media/Reviews', 'General News/Info', -827653069017224021, 1;
insert into #beam_insert select 43264, 'livestrong.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 7534121160065754186, 1;
insert into #beam_insert select 51564, 'kingsumo.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 8198656857457248304, 0;
insert into #beam_insert select 32304, 'hellobeautiful.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 8076935529293971181, 1;
insert into #beam_insert select 6079, 'azcentral.com', 235, 'green', 'Media/Reviews', 'General News/Info', 4297499910958001919, 1;
insert into #beam_insert select 2877, 'allure.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 8812381346089110229, 1;
insert into #beam_insert select 2528, 'amazon.com.br', 235, 'green', 'General Retailer', 'Online FMCG', -289740968803540400, 0;
insert into #beam_insert select 4010, 'amazon.com.mx', 235, 'green', 'General Retailer', 'Online FMCG', 1203754932668859087, 1;
insert into #beam_insert select 1955, 'adweek.com', 235, 'green', 'Media/Reviews', 'General News/Info', 1160387291621745297, 1;
insert into #beam_insert select 423, 'abc7chicago.com', 235, 'green', 'Media/Reviews', 'General News/Info', 7714324879834509545, 1;
insert into #beam_insert select 535, 'abeautifulmess.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -6500799728553546574, 1;
insert into #beam_insert select 501, '10news.com', 235, 'green', 'Media/Reviews', 'General News/Info', -2408268321766350640, 1;
insert into #beam_insert select 823, '5280.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -7038817328724209823, 1;
insert into #beam_insert select 8923, 'buzzfeednews.com', 235, 'green', 'Media/Reviews', 'General News/Info', -196912443919567849, 1;
insert into #beam_insert select 79833, 'biglots.com', 235, 'green', 'General Retailer', 'Brick + Click Grocery', -4709152540819267067, 0;
insert into #beam_insert select 7849, 'bloomberg.com', 235, 'green', 'Media/Reviews', 'General News/Info', 207147014984015617, 1;
insert into #beam_insert select 8259, 'brides.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 2106939774951343395, 1;
insert into #beam_insert select 24428, 'fortune.com', 235, 'green', 'Media/Reviews', 'General News/Info', 3040477887254397004, 1;
insert into #beam_insert select 26139, 'fox5ny.com', 235, 'green', 'Media/Reviews', 'General News/Info', -2607164983961567929, 1;
insert into #beam_insert select 25376, 'foxnews.com', 235, 'green', 'Media/Reviews', 'General News/Info', -4804116222532137103, 1;
insert into #beam_insert select 22800, 'freebie-depot.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', -6707400698565163492, 1;
insert into #beam_insert select 21580, 'essence.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 7298963863081904809, 1;
insert into #beam_insert select 21029, 'elitedaily.com', 235, 'green', 'Media/Reviews', 'General News/Info', 1687402186501008359, 1;
insert into #beam_insert select 22148, 'elle.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 2055947386159931858, 1;
insert into #beam_insert select 18392, 'clickondetroit.com', 235, 'green', 'Media/Reviews', 'General News/Info', -1151571732406150591, 1;
insert into #beam_insert select 16307, 'courant.com', 235, 'green', 'Media/Reviews', 'General News/Info', -9075340476726351206, 1;
insert into #beam_insert select 18751, 'dailymail.co.uk', 235, 'green', 'Media/Reviews', 'General News/Info', 9088022898374018810, 1;
insert into #beam_insert select 19418, 'dailyrecord.co.uk', 235, 'green', 'Media/Reviews', 'General News/Info', -3684630917933504586, 1;
insert into #beam_insert select 23990, 'dealdrop.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 8901550954597799554, 1;
insert into #beam_insert select 23105, 'domino.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -7776686454729030938, 1;
insert into #beam_insert select 63070, 'today.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -3433784206931045854, 1;
insert into #beam_insert select 67238, 'trendingpolitics.com', 235, 'green', 'Media/Reviews', 'General News/Info', -6796681089093248217, 1;
insert into #beam_insert select 81469, 'thetrendspotter.net', 235, 'green', 'Media/Reviews', 'Lifestyle', -9037130707811271361, 1;
insert into #beam_insert select 72092, 'thestar.com', 235, 'green', 'Media/Reviews', 'General News/Info', -888291823892748615, 1;
insert into #beam_insert select 71962, 'the-sun.com', 235, 'green', 'Media/Reviews', 'General News/Info', 2165796199787781506, 1;
insert into #beam_insert select 78175, 'wish.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 6901550163493898587, 1;
insert into #beam_insert select 82883, 'wkyt.com', 235, 'green', 'Media/Reviews', 'General News/Info', 8264805460547104450, 1;
insert into #beam_insert select 68799, 'womenshealthmag.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -496739335480711751, 1;
insert into #beam_insert select 78153, 'woot.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 7962843933361223838, 1;
insert into #beam_insert select 79130, 'wpxi.com', 235, 'green', 'Media/Reviews', 'General News/Info', -6232752784034848269, 1;
insert into #beam_insert select 85575, 'wsmv.com', 235, 'green', 'Media/Reviews', 'General News/Info', 400644707860664072, 1;
insert into #beam_insert select 76375, 'wibw.com', 235, 'green', 'Media/Reviews', 'General News/Info', -3806484723361608741, 1;
insert into #beam_insert select 75923, 'westword.com', 235, 'green', 'Media/Reviews', 'General News/Info', 7084031546195254010, 1;
insert into #beam_insert select 76626, 'whas11.com', 235, 'green', 'Media/Reviews', 'General News/Info', 7135946526651664902, 1;
insert into #beam_insert select 60443, 'wbay.com', 235, 'green', 'Media/Reviews', 'General News/Info', 2032414182090441331, 1;
insert into #beam_insert select 72495, 'wboc.com', 235, 'green', 'Media/Reviews', 'General News/Info', -8678048608957411773, 1;
insert into #beam_insert select 76726, 'wdrb.com', 235, 'green', 'Media/Reviews', 'General News/Info', 47069524260677703, 1;
insert into #beam_insert select 78765, 'usmagazine.com', 235, 'green', 'Media/Reviews', 'Lifestyle', 2393246589545875015, 1;
insert into #beam_insert select 72111, 'vanityfair.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -4248515586090314350, 1;
insert into #beam_insert select 56878, 'reviewjournal.com', 235, 'green', 'Media/Reviews', 'General News/Info', 4364185030803952364, 1;
insert into #beam_insert select 54783, 'reuters.com', 235, 'green', 'Media/Reviews', 'General News/Info', 799373862316304957, 1;
insert into #beam_insert select 55663, 'reason.com', 235, 'green', 'Media/Reviews', 'General News/Info', -8812511156426528599, 1;
insert into #beam_insert select 59757, 'rebatekey.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', -2702938713395300848, 1;
insert into #beam_insert select 63797, 'sendmeasample.net', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', -6843925846457445402, 1;
insert into #beam_insert select 57202, 'salon.com', 235, 'green', 'Media/Reviews', 'General News/Info', -3214678533895294610, 1;
insert into #beam_insert select 84413, 'seattletimes.com', 235, 'green', 'Media/Reviews', 'General News/Info', 2186444924221605605, 1;
insert into #beam_insert select 119878, 'standard.co.uk', 235, 'green', 'Media/Reviews', 'General News/Info', 6773691582941036433, 1;
insert into #beam_insert select 67481, 'startribune.com', 235, 'green', 'Media/Reviews', 'General News/Info', -484495442955032002, 1;
insert into #beam_insert select 70570, 'thecut.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -3201596334536212297, 1;
insert into #beam_insert select 76377, 'wweek.com', 235, 'green', 'Media/Reviews', 'General News/Info', -6184404356457390479, 1;
insert into #beam_insert select 56317, 'popculture.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -1610853160582542013, 1;
insert into #beam_insert select 50919, 'melmagazine.com', 235, 'green', 'Media/Reviews', 'Lifestyle', -1949517947339652104, 1;
insert into #beam_insert select 31888, 'lex18.com', 235, 'green', 'Media/Reviews', 'General News/Info', 2887449239417114447, 1;
insert into #beam_insert select 14506, 'chicagotribune.com', 235, 'green', 'Media/Reviews', 'General News/Info', -130792571570167683, 1;
insert into #beam_insert select 17725, 'citybeat.com', 235, 'green', 'Media/Reviews', 'General News/Info', -2749507486337446799, 1;
insert into #beam_insert select 13664, 'cincinnati.com', 235, 'green', 'Media/Reviews', 'General News/Info', 2473266335406903249, 1;
insert into #beam_insert select 65755, 'thedailybeast.com', 235, 'green', 'Media/Reviews', 'General News/Info', -8925267668292205345, 1;
insert into #beam_insert select 50240, 'retailmenot.com', 235, 'green', 'Media/Reviews', 'Freebie/Sweepstakes/Coupons', 836012384050781932, 0;
insert into #beam_insert select 55244, 'theglobeandmail.com', 235, 'green', 'Media/Reviews', 'General News/Info', -1111763614019418432, 1;

begin try drop table #beam_additions end try begin catch end catch;
select source_property_key, property_hash, digital_type_id, property_name, '' as property_name_TLD,
'CoxWireless2021' as categorization_source, 'US' as categorization_country, ecosystem_category, ecosystem_subcategoy as ecosystem_subcategory,
liquor as Alcohol_Beverage, 
'UNCLASSIFIED' as Apparel,  'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Auto_Parts, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Books_Magazines,
'UNCLASSIFIED' as Consumer_Electronics, 'UNCLASSIFIED' as Family_Children,  'UNCLASSIFIED' as Fashion_Accessories, liquor as Food_Groceries,
'UNCLASSIFIED' as Games, 'UNCLASSIFIED' as Gift_Cards, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Home_Garden, 'UNCLASSIFIED' as Live_Events, 
'UNCLASSIFIED' as Media_Entertainment,  'UNCLASSIFIED' as Pets, 'UNCLASSIFIED' as Shoes,  'UNCLASSIFIED' as Sporting_Goods, 'UNCLASSIFIED' as Tobacco,  
'UNCLASSIFIED' as Toys, 'UNCLASSIFIED' as Travel, liquor as Liquor
into #beam_additions
from #beam_insert
;
--(716 rows affected)




-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'Mattel'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #beam_additions a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(228 rows affected)


--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--new vert insert
update Core.ref.Ecosystem_Classifications
set Liquor = a.Liquor
from #beam_additions a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Liquor = 'UNCLASSIFIED' or b.Liquor is null)
;
--(488 rows affected)

update Core.ref.Ecosystem_Classifications
set Alcohol_Beverage = a.Alcohol_Beverage
from #beam_additions a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Alcohol_Beverage = 'UNCLASSIFIED' or b.Alcohol_Beverage is null)
;
--(272 rows affected)

update Core.ref.Ecosystem_Classifications
set Food_Groceries = a.Food_Groceries
from #beam_additions a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Food_Groceries = 'UNCLASSIFIED' or b.Food_Groceries is null)
;
--(357 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #beam_additions a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
join #beam_insert c on a.property_hash = c.property_hash and a.digital_type_id = c.digital_type_id and c.category_update = 1 -- determined before upload if category should be updated
;
--(671 rows affected)

update Core.ref.Ecosystem_Classifications set Liquor = 'UNCLASSIFIED' where Liquor is null; --(27466 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'CoxWireless2021';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; -- (28182 rows affected)
*/
/*
----------------------------YT Shopping

begin try drop table #YT_addition_prep end try begin catch end catch;
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name as property_name,
'YouTubeShopping' as categorization_source, 'US' as categorization_country, eco_category1 as ecosystem_category, 'UNCLASSIFIED' as  ecosystem_subcategory,retail_category,'white' as retail_category_color
into #YT_addition_prep
FROM [YouTubeShopping].[dbo].[TaxonomyEcosystemFull] a
join [YouTubeShopping].[Ref].[App_Name] b on a.source = 'App' and a.source_unique_key = b.App_Name
;
--(192 rows affected)

insert into #YT_addition_prep
select b.Domain_Name_Key source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'YouTubeShopping' as categorization_source, 'US' as categorization_country, eco_category1 as ecosystem_category,  'UNCLASSIFIED' as  ecosystem_subcategory,retail_category,'white' as retail_category_color
FROM [YouTubeShopping].[dbo].[TaxonomyEcosystemFull] a
join [YouTubeShopping].[Ref].[Domain_Name] b on a.source = 'Web: Domain' and a.source_unique_key = b.Domain_Name
;
--(8429 rows affected)

insert into Core.ref.Ecosystem_Classifications
select a.source_property_key,a.property_hash,a.digital_type_id,a.property_name,'',a.categorization_source,a.categorization_country,a.ecosystem_category,a.ecosystem_subcategory, 
null as Alcohol_Beverage, null as Apparel, null as Automotive, null as Auto_Parts, null as Beauty, null as Books_Magazines,null as Consumer_Electronics,null as Family_Children,
null as Fashion_Accessories, null as Food_Groceries, null as Games, null as Gift_Cards, null as Health_Medicine, null as Home_Garden,null as Live_Events, null as Media_Entertainment,
null as Pets, null as Shoes, null as Sporting_Goods, null as Tobacco, null as Toys, null as Travel
from #YT_addition_prep a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(6802 rows affected)

select distinct retail_category from #YT_addition_prep order by retail_category;

update Core.ref.Ecosystem_Classifications set Automotive = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Automobiles';
--(432 rows affected)

update Core.ref.Ecosystem_Classifications set Auto_Parts = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Automobiles Parts or Accessories';
--(60 rows affected)

update Core.ref.Ecosystem_Classifications set Beauty = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Beauty, Skincare and Cosmetics';
--(361 rows affected)

update Core.ref.Ecosystem_Classifications set Books_Magazines = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Books or Magazines';
--(176 rows affected)

update Core.ref.Ecosystem_Classifications set Apparel = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Clothing or Apparel';
--(1077 rows affected)

update Core.ref.Ecosystem_Classifications set Consumer_Electronics = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Consumer Electronics';
--(473 rows affected)

update Core.ref.Ecosystem_Classifications set Fashion_Accessories = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Fashion Accessories';
--(251 rows affected)

update Core.ref.Ecosystem_Classifications set Food_Groceries = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Food and Groceries';
--(496 rows affected)

update Core.ref.Ecosystem_Classifications set Gift_Cards = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Gift Cards';
--(23 rows affected)

update Core.ref.Ecosystem_Classifications set Home_Garden = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category in ('Home Appliances','Home Furnishings');
--(864 rows affected)


update Core.ref.Ecosystem_Classifications set Media_Entertainment = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Media and Entertainment';
--(203 rows affected)

update Core.ref.Ecosystem_Classifications set Travel = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Personal or Vacation Travel';
--(2193 rows affected)

update Core.ref.Ecosystem_Classifications set Pets = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Pet Supplies';
--(29 rows affected)

update Core.ref.Ecosystem_Classifications set Shoes = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Shoes or Footwear';
--(89 rows affected)

update Core.ref.Ecosystem_Classifications set Sporting_Goods = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Sporting or Fitness Goods';
--(276 rows affected)

update Core.ref.Ecosystem_Classifications set Toys = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Toys';
--(26 rows affected)

update Core.ref.Ecosystem_Classifications set Alcohol_Beverage = 'white'
from Core.ref.Ecosystem_Classifications a join #YT_addition_prep b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.retail_category = 'Wine, Beer, Spirits or Beverage';
--(172 rows affected)

update Core.ref.Ecosystem_Classifications set Alcohol_Beverage = 'UNCLASSIFIED' where Alcohol_Beverage is null;
update Core.ref.Ecosystem_Classifications set Apparel = 'UNCLASSIFIED' where Apparel is null;
update Core.ref.Ecosystem_Classifications set Automotive= 'UNCLASSIFIED' where Automotive is null;
update Core.ref.Ecosystem_Classifications set Auto_Parts = 'UNCLASSIFIED' where Auto_Parts is null;
update Core.ref.Ecosystem_Classifications set Beauty = 'UNCLASSIFIED' where Beauty is null;
update Core.ref.Ecosystem_Classifications set Books_Magazines = 'UNCLASSIFIED' where Books_Magazines is null;
update Core.ref.Ecosystem_Classifications set Consumer_Electronics = 'UNCLASSIFIED' where Consumer_Electronics is null;
update Core.ref.Ecosystem_Classifications set Family_Children = 'UNCLASSIFIED' where Family_Children is null;
update Core.ref.Ecosystem_Classifications set Fashion_Accessories = 'UNCLASSIFIED' where Fashion_Accessories is null;
update Core.ref.Ecosystem_Classifications set Food_Groceries = 'UNCLASSIFIED' where Food_Groceries is null;
update Core.ref.Ecosystem_Classifications set Games = 'UNCLASSIFIED' where Games is null;
update Core.ref.Ecosystem_Classifications set Gift_Cards = 'UNCLASSIFIED' where Gift_Cards is null;
update Core.ref.Ecosystem_Classifications set Health_Medicine = 'UNCLASSIFIED' where Health_Medicine is null;
update Core.ref.Ecosystem_Classifications set Home_Garden = 'UNCLASSIFIED' where Home_Garden is null;
update Core.ref.Ecosystem_Classifications set Live_Events = 'UNCLASSIFIED' where Live_Events is null;
update Core.ref.Ecosystem_Classifications set Media_Entertainment = 'UNCLASSIFIED' where Media_Entertainment is null;
update Core.ref.Ecosystem_Classifications set Pets = 'UNCLASSIFIED' where Pets is null;
update Core.ref.Ecosystem_Classifications set Shoes = 'UNCLASSIFIED' where Shoes is null;
update Core.ref.Ecosystem_Classifications set Sporting_Goods = 'UNCLASSIFIED' where Sporting_Goods is null;
update Core.ref.Ecosystem_Classifications set Tobacco = 'UNCLASSIFIED' where Tobacco is null;
update Core.ref.Ecosystem_Classifications set Toys = 'UNCLASSIFIED' where Toys is null;
update Core.ref.Ecosystem_Classifications set Travel = 'UNCLASSIFIED' where Travel is null;

update Core.ref.Ecosystem_Classifications set ecosystem_subcategory = 'UNCLASSIFIED' where ecosystem_subcategory ='';
*/
------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications;
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; -- (27814 rows affected)
*/
/*
----------------------------Mattel
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Toys varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #Mattel_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Mattel' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Games, 'UNCLASSIFIED' as Tobacco, 'white' as Toys
into #Mattel_addition_prep
from Mattel.dbo.Taxonomy_Ecosystem a
join Mattel.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(24 rows affected)

--select top 100 * from #Mattel_addition;

-----Pulls white apps
-- select count(*) from Mattel.dbo.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #Mattel_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'Mattel' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Games, 'UNCLASSIFIED' as Tobacco, 'white' as Toys
from Mattel.dbo.Taxonomy_Ecosystem  a
join Mattel.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(13 rows affected)



-- select * from #green_pull
begin try drop table #green_pull end try begin catch end catch;
select replace(REPLACE(bdg_display,'url: ',''),'url: ','') as bdg_display,source_name, category, subcategory
into #green_pull
from Mattel.dbo.Taxonomy_Ecosystem 
where source_name = 'Web: URL'
group by bdg_display,source_name, category, subcategory
;
--(196 rows affected)



----Pulls url domains (aka green props)
-- select top 100 * from #Mattel_addition_prep where Family_Children = 'green' order by property_name;
insert into #Mattel_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Mattel' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Games, 'UNCLASSIFIED' as Tobacco, 'green' as Toys
from #green_pull  a
join Mattel.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #Mattel_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(184 rows affected)



--One Final group by to ensure no dupes
begin try drop table #Mattel_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel, Games, Tobacco, Toys
into #Mattel_addition
from #Mattel_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel, Games, Tobacco, Toys
;
--(221 rows affected)


--dupe check:
select property_hash, digital_type_id, count(*) as count from #Mattel_addition group by property_hash, digital_type_id order by count desc;



-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'Mattel'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #Mattel_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(190 rows affected)


--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Toys = a.Toys
from #Mattel_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Toys = 'UNCLASSIFIED' or b.Toys is null)
;
--(31 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Toys = a.Toys
from #Mattel_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Toys  = 'green' AND a.Toys = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Mattel_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(0 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #Mattel_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'Mattel'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_category,new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Mattel_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'Mattel'
where-- a.ecosystem_subcategory in ('') or
a.ecosystem_category in ('Brands','Toy Retailer')
;
--(5 rows affected)



update Core.ref.Ecosystem_Classifications set Toys = 'UNCLASSIFIED' where Toys is null; --(20791 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'Mattel';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; -- (21012 rows affected)

*/
/*
----------------------------GSKSmoking
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Tobacco varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #GSKSmoking_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GSKSmoking' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Games, 'white' as Tobacco
into #GSKSmoking_addition_prep
from GSKSmoking.ref.Taxonomy_Ecosystem a
join GSKSmoking.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(30 rows affected)

--select top 100 * from #GSKSmoking_addition;

-----Pulls white apps
-- select count(*) from GSKSmoking.dbo.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #GSKSmoking_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'GSKSmoking' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Games, 'white' as Tobacco
from GSKSmoking.ref.Taxonomy_Ecosystem  a
join GSKSmoking.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(0 rows affected)

/*
-- select * from #green_pull
begin try drop table #green_pull end try begin catch end catch;
select replace(REPLACE(bdg_display,'url: ',''),'url: ','') as bdg_display,source_name, category, subcategory
into #green_pull
from GSKSmoking.dbo.Taxonomy_Ecosystem 
where source_name = 'Web: URL'
group by bdg_display,source_name, category, subcategory
;
--(806 rows affected)
*/


----Pulls url domains (aka green props)
-- select top 100 * from #GSKSmoking_addition_prep where Family_Children = 'green' order by property_name;
insert into #GSKSmoking_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GSKSmoking' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Games, 'green' as Tobacco
from GSKSmoking.ref.Taxonomy_Ecosystem  a
join GSKSmoking.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #GSKSmoking_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(72 rows affected)



--One Final group by to ensure no dupes
begin try drop table #GSKSmoking_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel, Games, Tobacco
into #GSKSmoking_addition
from #GSKSmoking_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel, Games, Tobacco
;
--(102 rows affected)




--dupe check:
select property_hash, digital_type_id, count(*) as count from #GSKSmoking_addition group by property_hash, digital_type_id order by count desc;



-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'GSKSmoking'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #GSKSmoking_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(48 rows affected)


--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Tobacco = a.Tobacco
from #GSKSmoking_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Tobacco = 'UNCLASSIFIED' or b.Tobacco is null)
;
--(50 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Tobacco = a.Tobacco
from #GSKSmoking_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Tobacco  = 'green' AND a.Tobacco = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GSKSmoking_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(0 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #GSKSmoking_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'GSKSmoking'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_category,new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GSKSmoking_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'GSKSmoking'
where a.ecosystem_subcategory in ('Amazon','Social: Facebook (Web)') --or
--a.ecosystem_category in ('Accommodation','DMO','DMO Look Alikes','In Destination Activities and Attractions','Transportation','Travel Aggregator / OTA')
;
--(2 rows affected)



update Core.ref.Ecosystem_Classifications set Tobacco = 'UNCLASSIFIED' where Tobacco is null; --(20755 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'GSKSmoking';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; -- (20822 rows affected)
*/
/*

----------------------------GSK
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Tobacco varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #GSK_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GSK' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Games, 'white' as Tobacco
into #GSK_addition_prep
from GSK.ref.Taxonomy_Ecosystem a
join GSK.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(15 rows affected)

--select top 100 * from #GSK_addition;

-----Pulls white apps
-- select count(*) from GSK.dbo.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #GSK_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'GSK' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Games, 'white' as Tobacco
from GSK.ref.Taxonomy_Ecosystem  a
join GSK.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(0 rows affected)

/*
-- select * from #green_pull
begin try drop table #green_pull end try begin catch end catch;
select replace(REPLACE(bdg_display,'url: ',''),'url: ','') as bdg_display,source_name, category, subcategory
into #green_pull
from GSK.dbo.Taxonomy_Ecosystem 
where source_name = 'Web: URL'
group by bdg_display,source_name, category, subcategory
;
--(806 rows affected)
*/


----Pulls url domains (aka green props)
-- select top 100 * from #GSK_addition_prep where Family_Children = 'green' order by property_name;
insert into #GSK_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GSK' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'UNCLASSIFIED' as Games, 'green' as Tobacco
from GSK.ref.Taxonomy_Ecosystem  a
join GSK.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #GSK_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(4 rows affected)



--One Final group by to ensure no dupes
begin try drop table #GSK_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel, Games, Tobacco
into #GSK_addition
from #GSK_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel, Games, Tobacco
;
--(19 rows affected)




--dupe check:
select property_hash, digital_type_id, count(*) as count from #GSK_addition group by property_hash, digital_type_id order by count desc;



-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'GSK'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #GSK_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(15 rows affected)


--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Tobacco = a.Tobacco
from #GSK_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Tobacco = 'UNCLASSIFIED' or b.Tobacco is null)
;
--(4 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Tobacco = a.Tobacco
from #GSK_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Tobacco  = 'green' AND a.Tobacco = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GSK_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(0 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #GSK_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'GSK'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_category,new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GSK_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'GSK'
where a.ecosystem_subcategory in ('Amazon','Social: Facebook (Web)') --or
--a.ecosystem_category in ('Accommodation','DMO','DMO Look Alikes','In Destination Activities and Attractions','Transportation','Travel Aggregator / OTA')
;
--(2 rows affected)



update Core.ref.Ecosystem_Classifications set Tobacco = 'UNCLASSIFIED' where Tobacco is null; --(20755 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'GSK';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; -- (20774 rows affected)
*/
/*
----------------------------GoogleGaming
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Games varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #GoogleGaming_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GoogleGaming' as categorization_source, 'PL' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'white' as Games
into #GoogleGaming_addition_prep
from GoogleGaming.dbo.Taxonomy_Ecosystem a
join GoogleGaming.reference.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(214 rows affected)

--select top 100 * from #GoogleGaming_addition;

-----Pulls white apps
-- select count(*) from GoogleGaming.dbo.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #GoogleGaming_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'GoogleGaming' as categorization_source,'PL' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'white' as Games
from GoogleGaming.dbo.Taxonomy_Ecosystem a
join GoogleGaming.reference.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(20 rows affected)

-- select * from #green_pull
begin try drop table #green_pull end try begin catch end catch;
select replace(REPLACE(bdg_display,'url: ',''),'url: ','') as bdg_display,source_name, category, subcategory
into #green_pull
from GoogleGaming.dbo.Taxonomy_Ecosystem 
where source_name = 'Web: URL'
group by bdg_display,source_name, category, subcategory
;
--(806 rows affected)



----Pulls url domains (aka green props)
-- select top 100 * from #GoogleGaming_addition_prep where Family_Children = 'green' order by property_name;
insert into #GoogleGaming_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GoogleGaming' as categorization_source, 'PL' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel, 'green' as Games
from #green_pull a
join GoogleGaming.reference.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #GoogleGaming_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(606 rows affected)



--One Final group by to ensure no dupes
begin try drop table #GoogleGaming_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel, Games
into #GoogleGaming_addition
from #GoogleGaming_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel, Games
;
--(840 rows affected)



--dupe check:
select property_hash, digital_type_id, count(*) as count from #GoogleGaming_addition group by property_hash, digital_type_id order by count desc;



-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'GoogleGaming'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #GoogleGaming_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(617 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Games = a.Games
from #GoogleGaming_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Games = 'UNCLASSIFIED' or b.Games is null)
;
--(218 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Games = a.Games
from #GoogleGaming_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Games  = 'green' AND a.Games = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GoogleGaming_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(9 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #GoogleGaming_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'GoogleGaming'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_category,new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GoogleGaming_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'GoogleGaming'
where --a.ecosystem_subcategory in ('Baby Media/Blogs','Ratings and Reviews') --or
--a.ecosystem_category in ('Accommodation','DMO','DMO Look Alikes','In Destination Activities and Attractions','Transportation','Travel Aggregator / OTA')
;
--(209 rows affected)



update Core.ref.Ecosystem_Classifications set Games = 'UNCLASSIFIED' where Games is null; --(19924 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'GoogleGaming';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; -- (20759 rows affected)
*/

/*
----------------------------GooglePoland2019
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Travel varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #GooglePoland2019_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GooglePoland2019' as categorization_source, 'PL' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'white' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel
into #GooglePoland2019_addition_prep
from GooglePoland2019.ref.Taxonomy_Ecosystem a
join GooglePoland2019.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(842 rows affected)

--select top 100 * from #GooglePoland2019_addition;

-----Pulls white apps
-- select count(*) from GooglePoland2019.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #GooglePoland2019_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'GooglePoland2019' as categorization_source,'PL' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'white' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel
from GooglePoland2019.ref.Taxonomy_Ecosystem a
join GooglePoland2019.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(11 rows affected)


----Pulls url domains (aka green props)
-- select top 100 * from #GooglePoland2019_addition_prep where Family_Children = 'green' order by property_name;
insert into #GooglePoland2019_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GooglePoland2019' as categorization_source, 'PL' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'green' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'UNCLASSIFIED' as Travel
from GooglePoland2019.ref.Taxonomy_Ecosystem a
join GooglePoland2019.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #GooglePoland2019_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(306 rows affected)



--One Final group by to ensure no dupes
begin try drop table #GooglePoland2019_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel
into #GooglePoland2019_addition
from #GooglePoland2019_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel
;
--(1159 rows affected)



--dupe check:
select property_hash, digital_type_id, count(*) as count from #GooglePoland2019_addition group by property_hash, digital_type_id order by count desc;



-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'GooglePoland2019'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #GooglePoland2019_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(1128 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Home_Garden = a.Home_Garden
from #GooglePoland2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Home_Garden = 'UNCLASSIFIED' or b.Home_Garden is null)
;
--(4 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Home_Garden  = a.Home_Garden
from #GooglePoland2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Home_Garden = 'green' AND a.Home_Garden = 'white');
--(2 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GooglePoland2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(1 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #GooglePoland2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'GooglePoland2019'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_category,new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GooglePoland2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'GooglePoland2019'
where --a.ecosystem_subcategory in ('Baby Media/Blogs','Ratings and Reviews') --or
a.ecosystem_category in ('Accommodation','DMO','DMO Look Alikes','In Destination Activities and Attractions','Transportation','Travel Aggregator / OTA')
;
--(58 rows affected)



update Core.ref.Ecosystem_Classifications set Home_Garden = 'UNCLASSIFIED' where Home_Garden is null; --(18259 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'GooglePoland2019';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; -- (20142 rows affected)

*/

/*
----------------------------VirginiaTourism
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Travel varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #VirginiaTourism_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'VirginiaTourism' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'white' as Travel
into #VirginiaTourism_addition_prep
from VirginiaTourism.ref.Taxonomy_Ecosystem a
join VirginiaTourism.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(525 rows affected)

--select top 100 * from #VirginiaTourism_addition;

-----Pulls white apps
-- select count(*) from VirginiaTourism.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #VirginiaTourism_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'VirginiaTourism' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'white' as Travel
from VirginiaTourism.ref.Taxonomy_Ecosystem a
join VirginiaTourism.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(33 rows affected)

begin try drop table #green_pull end try begin catch end catch;
select replace(REPLACE(bdg_display,'url: ',''),'url: ','') as bdg_display,source_name, category, subcategory
into #green_pull
from VirginiaTourism.ref.Taxonomy_Ecosystem
where source_name = 'Web: URL'
;
--(511 rows affected)

----Pulls url domains (aka green props)
-- select top 100 * from #VirginiaTourism_addition_prep where Family_Children = 'green' order by property_name;
insert into #VirginiaTourism_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'VirginiaTourism' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'UNCLASSIFIED' as Family_Children, 'green' as Travel
from #green_pull a
join VirginiaTourism.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #VirginiaTourism_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(209 rows affected)



--One Final group by to ensure no dupes
begin try drop table #VirginiaTourism_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel
into #VirginiaTourism_addition
from #VirginiaTourism_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children, Travel
;
--(767 rows affected)


--dupe check:
select property_hash, digital_type_id, count(*) as count from #VirginiaTourism_addition group by property_hash, digital_type_id order by count desc;



-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'VirginiaTourism'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #VirginiaTourism_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(624 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Travel = a.Travel
from #VirginiaTourism_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Travel = 'UNCLASSIFIED' or b.Travel is null)
;
--(131 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Travel  = a.Travel
from #VirginiaTourism_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Travel = 'green' AND a.Travel = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #VirginiaTourism_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(0 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #VirginiaTourism_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'VirginiaTourism'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_category,new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #VirginiaTourism_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'VirginiaTourism'
where --a.ecosystem_subcategory in ('Baby Media/Blogs','Ratings and Reviews') --or
a.ecosystem_category in ('Accommodation','DMO','DMO Look Alikes','In Destination Activities and Attractions','Transportation','Travel Aggregator / OTA')
;
--(58 rows affected)



update Core.ref.Ecosystem_Classifications set Travel = 'UNCLASSIFIED' where Travel is null; --(18259 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'VirginiaTourism';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; --(19014 rows affected)
*/

/*
----------------------------Newell
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Family_Children varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #Newell_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Newell' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'white' as Family_Children
into #Newell_addition_prep
from Newell.ref.Taxonomy_Ecosystem a
join Newell.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(0 rows affected)

--select top 100 * from #Newell_addition;

-----Pulls white apps
-- select count(*) from Newell.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #Newell_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'Newell' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'white' as Family_Children
from Newell.ref.Taxonomy_Ecosystem a
join Newell.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(0 rows affected)


----Pulls url domains (aka green props)
-- select top 100 * from #Newell_addition_prep where Family_Children = 'green' order by property_name;
insert into #Newell_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Newell' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'green' as Family_Children
from Newell.ref.Taxonomy_Ecosystem a
join Newell.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #Newell_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(105 rows affected)



--One Final group by to ensure no dupes
begin try drop table #Newell_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children
into #Newell_addition
from #Newell_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children
;
--(105 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #Newell_addition group by property_hash, digital_type_id order by count desc;


-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'Newell'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #Newell_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(43 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Family_Children = a.Family_Children
from #Newell_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Family_Children = 'UNCLASSIFIED' or b.Family_Children is null)
;
--(52 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Family_Children  = a.Family_Children
from #Newell_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Family_Children = 'green' AND a.Family_Children  = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Newell_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(1 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #Newell_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'Newell'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_category,new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Newell_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'Newell'
where a.ecosystem_subcategory in ('Baby Media/Blogs','Ratings and Reviews') --or
--a.ecosystem_category in ('Mobile Carrier')
;
--(13 rows affected)



update Core.ref.Ecosystem_Classifications set Family_Children = 'UNCLASSIFIED' where Family_Children is null; --(18051 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'Newell';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; --(18390 rows affected)
*/

/*
----------------------------BayerNappy
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Family_Children varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #BayerNappy_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'BayerNappy' as categorization_source, 'BR' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'white' as Family_Children
into #BayerNappy_addition_prep
from BayerNappy.ref.Taxonomy_Ecosystem a
join BayerNappy.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(75 rows affected)

--select top 100 * from #BayerNappy_addition;

-----Pulls white apps
-- select count(*) from BayerNappy.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #BayerNappy_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'BayerNappy' as categorization_source,'BR' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'white' as Family_Children
from BayerNappy.ref.Taxonomy_Ecosystem a
join BayerNappy.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(35 rows affected)


----Pulls url domains (aka green props)
-- select top 100 * from #BayerNappy_addition_prep where Family_Children = 'green' order by property_name;
insert into #BayerNappy_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'BayerNappy' as categorization_source, 'BR' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage,
'green' as Family_Children
from BayerNappy.ref.Taxonomy_Ecosystem a
join BayerNappy.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #BayerNappy_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(186 rows affected)



--One Final group by to ensure no dupes
begin try drop table #BayerNappy_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children
into #BayerNappy_addition
from #BayerNappy_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage, Family_Children
;
--(296 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #BayerNappy_addition group by property_hash, digital_type_id order by count desc;


-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'BayerNappy'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #BayerNappy_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(282 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Family_Children = a.Family_Children
from #BayerNappy_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Family_Children = 'UNCLASSIFIED' or b.Family_Children is null)
;
--(14 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Family_Children  = a.Family_Children
from #BayerNappy_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Family_Children = 'green' AND a.Family_Children  = 'white');
--(1 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #BayerNappy_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(1 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #BayerNappy_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'BayerNappy'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_category,new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #BayerNappy_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'BayerNappy'
where a.ecosystem_subcategory in ('Brand: All Other','Mass Merchandiser','Microsoft','Online Retailer') or
a.ecosystem_category in ('Mobile Carrier')
;
--(50 rows affected)



update Core.ref.Ecosystem_Classifications set Family_Children = 'UNCLASSIFIED' where Family_Children is null; --(18051 rows affected)


------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'BayerNappy';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; --(18347 rows affected)
*/

/*
----------------------------Samsung2019
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Consumer_Electronics varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #Samsung2019_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Samsung2019' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'white' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage
into #Samsung2019_addition_prep
from Samsung2019.ref.Taxonomy_Ecosystem a
join Samsung2019.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(59 rows affected)

--select top 100 * from #Samsung2019_addition;

-----Pulls white apps
-- select count(*) from Samsung2019.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #Samsung2019_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'Samsung2019' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel,'white' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage
from Samsung2019.ref.Taxonomy_Ecosystem a
join Samsung2019.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(77 rows affected)


----Pulls url domains (aka green props)
-- select top 100 * from #Samsung2019_addition_prep where  Automotive = 'green' order by property_name;
insert into #Samsung2019_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Samsung2019' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'green' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'UNCLASSIFIED' as Food_Beverage
from Samsung2019.ref.Taxonomy_Ecosystem a
join Samsung2019.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #Samsung2019_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(186 rows affected)



--One Final group by to ensure no dupes
begin try drop table #Samsung2019_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage
into #Samsung2019_addition
from #Samsung2019_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage
;
--(322 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #Samsung2019_addition group by property_hash, digital_type_id order by count desc;
select * from #Samsung2019_addition;


-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'Samsung2019'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #Samsung2019_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(131 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Computer_Electronics = a.Computer_Electronics 
from #Samsung2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Computer_Electronics = 'UNCLASSIFIED' or b.Computer_Electronics is null)
;
--(38 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Computer_Electronics  = a.Computer_Electronics 
from #Samsung2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Computer_Electronics = 'green' AND a.Computer_Electronics  = 'white');
--(1 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Samsung2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(35 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #Samsung2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'Samsung2019'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_category,new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Samsung2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'Samsung2019'
where a.ecosystem_subcategory in ('Brand: All Other','Mass Merchandiser','Microsoft','Online Retailer') or
a.ecosystem_category in ('Mobile Carrier')
;
--(50 rows affected)



update Core.ref.Ecosystem_Classifications set Food_Beverage = 'UNCLASSIFIED' where Food_Beverage is null; --(16933 rows affected)

------------------MAKE SURE TO CHECK AND THEN BACKUP
--select * from Core.ref.Ecosystem_Classifications where categorization_source = 'Samsung2019';
--drop table core.ref.Ecosystem_Classifications_bkup; select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications; --(18065 rows affected)

*/
/*
----------------------------Nestle2019
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Food_Beverage varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #Nestle2019_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Nestle2019' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'white' as Food_Beverage
into #Nestle2019_addition_prep
from Nestle2019.ref.Taxonomy_Ecosystem a
join Nestle2019.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(402 rows affected)

--select top 100 * from #Nestle2019_addition;

-----Pulls white apps
-- select count(*) from Nestle2019.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #Nestle2019_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'Nestle2019' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'white' as Food_Beverage
from Nestle2019.ref.Taxonomy_Ecosystem a
join Nestle2019.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(121 rows affected)


----Pulls url domains (aka green props)
-- select top 100 * from #Nestle2019_addition_prep where  Automotive = 'green' order by property_name;
insert into #Nestle2019_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Nestle2019' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine, 'green' as Food_Beverage
from Nestle2019.ref.Taxonomy_Ecosystem a
join Nestle2019.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #Nestle2019_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(478 rows affected)



--One Final group by to ensure no dupes
begin try drop table #Nestle2019_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage
into #Nestle2019_addition
from #Nestle2019_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine, Food_Beverage
;
--(1001 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #Nestle2019_addition group by property_hash, digital_type_id order by count desc;


-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'Nestle2019'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #Nestle2019_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(776 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Food_Beverage = a.Food_Beverage 
from #Nestle2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Food_Beverage = 'UNCLASSIFIED' or b.Food_Beverage is null)
;
--(225 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Food_Beverage = a.Food_Beverage 
from #Nestle2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Food_Beverage = 'green' AND a.Food_Beverage = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Nestle2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(2 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #Nestle2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'Nestle2019'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Nestle2019_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'Nestle2019'
where a.ecosystem_subcategory in ('Food Gifts','Grocery Store') 
--a.ecosystem_category in ('Brand')
;
--(59 rows affected)



update Core.ref.Ecosystem_Classifications set Food_Beverage = 'UNCLASSIFIED' where Food_Beverage is null; --(16933 rows affected)

--select * from Core.ref.Ecosystem_Classifications;
--select * into core.ref.Ecosystem_Classifications_bkup from core.ref.Ecosystem_Classifications;

*/
/*
----------------------------PGSkincare
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Health_Medicine varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #PGSkincare_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'PGSkincare' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'white' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine
into #PGSkincare_addition_prep
from PGSkincare.ref.Taxonomy_Ecosystem a
join PGSkincare.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(124 rows affected)

--select top 100 * from #PGSkincare_addition;

-----Pulls white apps
-- select count(*) from PGSkincare.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #PGSkincare_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'PGSkincare' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'white' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine
from PGSkincare.ref.Taxonomy_Ecosystem a
join PGSkincare.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(0 row affected)


----Pulls url domains (aka green props)
-- select top 100 * from #PGSkincare_addition_prep where  Automotive = 'green' order by property_name;
insert into #PGSkincare_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'PGSkincare' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'green' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine
from PGSkincare.ref.Taxonomy_Ecosystem a
join PGSkincare.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #PGSkincare_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(322 rows affected)



--One Final group by to ensure no dupes
begin try drop table #PGSkincare_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
into #PGSkincare_addition
from #PGSkincare_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
;
--(446 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #PGSkincare_addition group by property_hash, digital_type_id order by count desc;


-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'PGSkincare'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #PGSkincare_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(156 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Beauty = a.Beauty
from #PGSkincare_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Beauty = 'UNCLASSIFIED' or b.Beauty is null)
;
--(37 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Beauty = a.Beauty
from #PGSkincare_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Beauty = 'green' AND a.Beauty = 'white');
--(8 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #PGSkincare_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(0 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #PGSkincare_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'PGSkincare'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #PGSkincare_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'PGSkincare'
where a.ecosystem_subcategory in ('Brand: Mass Market') --or 
--a.ecosystem_category in ('Brand')
;
--(21 rows affected)



update Core.ref.Ecosystem_Classifications set Beauty = 'UNCLASSIFIED' where Beauty is null; --(0 rows affected)
*/

/*
----------------------------Colgate2020
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Health_Medicine varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #Colgate2020_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Colgate2020' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'white' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine
into #Colgate2020_addition_prep
from Colgate2020.ref.Taxonomy_Ecosystem a
join Colgate2020.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(9 rows affected)

--select top 100 * from #Colgate2020_addition;

-----Pulls white apps
-- select count(*) from Colgate2020.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #Colgate2020_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'Colgate2020' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'white' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine
from Colgate2020.ref.Taxonomy_Ecosystem a
join Colgate2020.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(0 row affected)


----Pulls url domains (aka green props)
-- select top 100 * from #Colgate2020_addition_prep where  Automotive = 'green' order by property_name;
insert into #Colgate2020_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Colgate2020' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'green' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine
from Colgate2020.ref.Taxonomy_Ecosystem a
join Colgate2020.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #Colgate2020_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(40 rows affected)


--One Final group by to ensure no dupes
begin try drop table #Colgate2020_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
into #Colgate2020_addition
from #Colgate2020_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
;
--(49 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #Colgate2020_addition group by property_hash, digital_type_id order by count desc;


-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'Colgate2020'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #Colgate2020_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(27 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Beauty = a.Beauty
from #Colgate2020_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Beauty = 'UNCLASSIFIED' or b.Beauty is null)
;
--(1 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Beauty = a.Beauty
from #Colgate2020_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Beauty = 'green' AND a.Beauty = 'white');
--(4 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Colgate2020_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(4 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #Colgate2020_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'Colgate2020'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Colgate2020_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'Colgate2020'
where --a.ecosystem_subcategory in ('Mass Merchandiser') or 
a.ecosystem_category in ('Brand')
;
--(43 rows affected)



update Core.ref.Ecosystem_Classifications set Beauty = 'UNCLASSIFIED' where Beauty is null; --(0 rows affected)
*/
/*

----------------------------Loreal2018
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Health_Medicine varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #Loreal2018_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Loreal2018' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'white' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine
into #Loreal2018_addition_prep
from Loreal2018.ref.Taxonomy_Ecosystem a
join Loreal2018.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(355 rows affected)

--select top 100 * from #Loreal2018_addition;

-----Pulls white apps
-- select count(*) from Loreal2018.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #Loreal2018_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'Loreal2018' as categorization_source,'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'white' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine
from Loreal2018.ref.Taxonomy_Ecosystem a
join Loreal2018.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(4 row affected)


----Pulls url domains (aka green props)
-- select top 100 * from #Loreal2018_addition_prep where  Automotive = 'green' order by property_name;
insert into #Loreal2018_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'Loreal2018' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'green' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'UNCLASSIFIED' as Health_Medicine
from Loreal2018.ref.Taxonomy_Ecosystem a
join Loreal2018.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #Loreal2018_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(390 rows affected)


--One Final group by to ensure no dupes
begin try drop table #Loreal2018_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
into #Loreal2018_addition
from #Loreal2018_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
;
--(749 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #Loreal2018_addition group by property_hash, digital_type_id order by count desc;


-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'Loreal2018'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #Loreal2018_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(517 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Beauty = a.Beauty
from #Loreal2018_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Beauty = 'UNCLASSIFIED' or b.Beauty is null)
;
--(125 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Beauty = a.Beauty
from #Loreal2018_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Beauty = 'green' AND a.Beauty = 'white');
--(4 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Loreal2018_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(4 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #Loreal2018_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'Loreal2018'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #Loreal2018_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'Loreal2018'
where --a.ecosystem_subcategory in ('Mass Merchandiser') or 
a.ecosystem_category in ('Brand')
;
--(43 rows affected)



update Core.ref.Ecosystem_Classifications set Beauty = 'UNCLASSIFIED' where Beauty is null; --(0 rows affected)
*/
/*
----------------------------GSKNasal
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Health_Medicine varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #GSKNasal_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GSKNasal' as categorization_source, 'DE' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'white' as Health_Medicine
into #GSKNasal_addition_prep
from GSKNasal.ref.Taxonomy_Ecosystem a
join GSKNasal.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(17 rows affected)

--select top 100 * from #GSKNasal_addition;

-----Pulls white apps
-- select count(*) from GSKNasal.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #GSKNasal_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'GSKNasal' as categorization_source, 'DE' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'white' as Health_Medicine
from GSKNasal.ref.Taxonomy_Ecosystem a
join GSKNasal.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(0 row affected)


----Pulls url domains (aka green props)
-- select top 100 * from #GSKNasal_addition_prep where  Automotive = 'green' order by property_name;
insert into #GSKNasal_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GSKNasal' as categorization_source, 'DE' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'green' as Health_Medicine
from GSKNasal.ref.Taxonomy_Ecosystem a
join GSKNasal.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #GSKNasal_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(139 rows affected)


--One Final group by to ensure no dupes
begin try drop table #GSKNasal_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
into #GSKNasal_addition
from #GSKNasal_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
;
--(156 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #GSKNasal_addition group by property_hash, digital_type_id order by count desc;


-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'GSKNasal'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #GSKNasal_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(140 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Health_Medicine = a.Health_Medicine
from #GSKNasal_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Health_Medicine = 'UNCLASSIFIED' or b.Health_Medicine is null)
;
--(2 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Health_Medicine = a.Health_Medicine
from #GSKNasal_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Health_Medicine = 'green' AND a.Health_Medicine = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GSKNasal_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(0 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #GSKNasal_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'GSKNasal'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GSKNasal_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'GSKNasal'
where a.ecosystem_subcategory in ('Mass Merchandiser') --or a.ecosystem_category in ('Health','Medicine/Products','Social','Wellness')
;
--(7 rows affected)



update Core.ref.Ecosystem_Classifications set Health_Medicine = 'UNCLASSIFIED' where Health_Medicine is null; --(0 rows affected)
*/
/*
----------------------------Sanofi Allergy 2021
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Health_Medicine varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #SanofiAllergy2021_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'SanofiAllergy2021' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'white' as Health_Medicine
into #SanofiAllergy2021_addition_prep
from SanofiAllergy2021.ref.Taxonomy_Ecosystem a
join SanofiAllergy2021.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(15 rows affected)

--select top 100 * from #SanofiAllergy2021_addition;

-----Pulls white apps
-- select count(*) from SanofiAllergy2021.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #SanofiAllergy2021_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'SanofiAllergy2021' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'white' as Health_Medicine
from SanofiAllergy2021.ref.Taxonomy_Ecosystem a
join SanofiAllergy2021.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(1 row affected)


----Pulls url domains (aka green props)
-- select top 100 * from #SanofiAllergy2021_addition_prep where  Automotive = 'green' order by property_name;
insert into #SanofiAllergy2021_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'SanofiAllergy2021' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'green' as Health_Medicine
from SanofiAllergy2021.ref.Taxonomy_Ecosystem a
join SanofiAllergy2021.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #SanofiAllergy2021_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(242 rows affected)


--One Final group by to ensure no dupes
begin try drop table #SanofiAllergy2021_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
into #SanofiAllergy2021_addition
from #SanofiAllergy2021_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
;
--(258 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #SanofiAllergy2021_addition group by property_hash, digital_type_id order by count desc;

-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'SanofiAllergy2021'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #SanofiAllergy2021_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(117 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Health_Medicine = a.Health_Medicine
from #SanofiAllergy2021_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Health_Medicine = 'UNCLASSIFIED' or b.Health_Medicine is null)
;
--(63 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Health_Medicine = a.Health_Medicine
from #SanofiAllergy2021_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Health_Medicine = 'green' AND a.Health_Medicine = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #SanofiAllergy2021_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(0 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #SanofiAllergy2021_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'SanofiAllergy2021'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #SanofiAllergy2021_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'SanofiAllergy2021'
where a.ecosystem_subcategory in ('Mass Merchandiser') --or a.ecosystem_category in ('Health','Medicine/Products','Social','Wellness')
;
--(7 rows affected)


update Core.ref.Ecosystem_Classifications set Health_Medicine = 'UNCLASSIFIED' where Health_Medicine is null; --(0 rows affected)
*/

/*
----------------------------Mayo Clinic
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Health_Medicine varchar(50);
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #MayoClinic_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'MayoClinic' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'white' as Health_Medicine
into #MayoClinic_addition_prep
from MayoClinic.ref.Taxonomy_Ecosystem a
join MayoClinic.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(1793 rows affected)

--select top 100 * from #MayoClinic_addition;

-----Pulls white apps
-- select count(*) from MayoClinic.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #MayoClinic_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'MayoClinic' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'white' as Health_Medicine
from MayoClinic.ref.Taxonomy_Ecosystem a
join MayoClinic.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(90 rows affected)


----Pulls url domains (aka green props)
-- select top 100 * from #MayoClinic_addition_prep where  Automotive = 'green' order by property_name;
insert into #MayoClinic_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'MayoClinic' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events, 'green' as Health_Medicine
from MayoClinic.ref.Taxonomy_Ecosystem a
join MayoClinic.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #MayoClinic_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(677 rows affected)


--One Final group by to ensure no dupes
begin try drop table #MayoClinic_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
into #MayoClinic_addition
from #MayoClinic_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events, Health_Medicine
;
--(2560 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #MayoClinic_addition group by property_hash, digital_type_id order by count desc;

-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'MayoClinic'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #MayoClinic_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(2015 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Health_Medicine = a.Health_Medicine
from #MayoClinic_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Health_Medicine = 'UNCLASSIFIED' or b.Health_Medicine is null)
;
--(545 rows affected)

------Want to stay as broad as possible, so overwrites to white if already green
update Core.ref.Ecosystem_Classifications
set Health_Medicine = a.Health_Medicine
from #MayoClinic_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Health_Medicine = 'green' AND a.Health_Medicine = 'white');
--(0 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #MayoClinic_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and (b.ecosystem_category = 'UNCLASSIFIED' or b.ecosystem_subcategory = 'UNCLASSIFIED')
;
--(6 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat,
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat
from #MayoClinic_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'MayoClinic'
where a.ecosystem_subcategory != b.ecosystem_subcategory
order by new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories)
--Generally want to be more specific so long as its accurate
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #MayoClinic_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'MayoClinic'
where a.ecosystem_subcategory in ('General Info and Reviews','General News') or a.ecosystem_category in ('Health','Medicine/Products','Social','Wellness')
;
--(447 rows affected)


update Core.ref.Ecosystem_Classifications set Health_Medicine = 'UNCLASSIFIED' where Health_Medicine is null; --(13641 rows affected)
*/

----------------------------KNOWLEDGE PANEL (JUST FOR CLASSIFICATIONS, NO COLOR)
--------First add a column for the new projects vertical if there isnt one already

/* FOR CHECKING HOW DATA LOOKS
SELECT 
      [source_name]
      ,[source_particle_key]
      ,[source_particle]
      ,[category]
      ,[subcategory]
      ,[bdg_display]
  FROM [KnowledgePanel2020].[Ref].[Taxonomy_Ecosystem]
  --where source_name in ('Web: Domain','App: Name','Web: URL')
  where source_name = 'Web: URL'
  ;

  select
  [category]
      ,[subcategory]
  FROM [KnowledgePanel2020].[Ref].[Taxonomy_Ecosystem]
  where source_name in ('Web: Domain','App: Name','Web: URL')
  group by [category]
      ,[subcategory]
	  order by category, subcategory

*/
/*

begin try drop table #tax_eco_pull end try begin catch end catch;
select source_name, source_particle_key, source_particle, bdg_display, 
	case when category = 'Covid Communication' then 'Communication'
	when category ='Covid Economic Effect' then 'Economic Effect'
	when category = 'Covid Entertainment' then 'Entertainment'
	when category = 'Covid Health/Wellness' then 'Health/Wellness'
	when category = 'Covid Media/Reviews' then 'Media/Reviews'
	when category = 'Covid Retailer' then 'Retailer'
	when category = 'Covid Social' then 'Social'
	else category 
	end as category,
	subcategory
into #tax_eco_pull
 FROM [KnowledgePanel2020].[Ref].[Taxonomy_Ecosystem]
  where source_name in ('Web: Domain','App: Name','Web: URL')
  group by source_name, source_particle_key, source_particle, bdg_display, category, subcategory;
--(160333 rows affected)


--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #KnowledgePanel2020_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'KnowledgePanel2020' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events
into #KnowledgePanel2020_addition_prep
from #tax_eco_pull a
join KnowledgePanel2020.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(332 rows affected)

--select top 100 * from #KnowledgePanel2020_addition;

-----Pulls white apps
-- select count(*) from KnowledgePanel2020.dbo.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #KnowledgePanel2020_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'KnowledgePanel2020' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events
from #tax_eco_pull a
join KnowledgePanel2020.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(100 rows affected)



----Pulls url domains (aka green props)
-- select top 100 * from #KnowledgePanel2020_addition_prep where  Automotive = 'green' order by property_name;
insert into #KnowledgePanel2020_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'KnowledgePanel2020' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'UNCLASSIFIED' as Live_Events
from #tax_eco_pull a
join KnowledgePanel2020.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #KnowledgePanel2020_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(6069 rows affected)


--One Final group by to ensure no dupes
begin try drop table #KnowledgePanel2020_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events
into #KnowledgePanel2020_addition
from #KnowledgePanel2020_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events
;
--(6501 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #KnowledgePanel2020_addition group by property_hash, digital_type_id order by count desc;

select * from #KnowledgePanel2020_addition where property_hash = 5497574072331586938 and digital_type_id = 235

delete from #KnowledgePanel2020_addition where property_name = 'nebraska.gov' and ecosystem_subcategory = 'Jobs/Unemployment'; --(1 row affected)
delete from #KnowledgePanel2020_addition where property_name = 'in.gov' and ecosystem_subcategory = 'Jobs/Unemployment'; --(1 row affected)
delete from #KnowledgePanel2020_addition where property_name = 'iowa.gov' and ecosystem_subcategory = 'Jobs/Unemployment'; --(1 row affected)

-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'KnowledgePanel2020'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #KnowledgePanel2020_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(5687 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #KnowledgePanel2020_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and b.ecosystem_category = 'UNCLASSIFIED'
;
--(86 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat, a.Live_Events as new_color, 
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat, b.Live_Events as old_color
from #KnowledgePanel2020_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'KnowledgePanel2020' 
order by new_category, new_subcat;


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #KnowledgePanel2020_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'KnowledgePanel2020'
where a.ecosystem_subcategory in ('Gaming/Games','TV and Video','General News','Lifestyle','Online Retailer','Schools/Education')
;
--(292 rows affected)
*/

----------------------------Google Ticketing
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Live_Events varchar(50);
*/

/* FOR CHECKING HOW DATA LOOKS
SELECT 
      [source_name]
      ,[source_particle_key]
      ,[source_particle]
      ,[category]
      ,[subcategory]
      ,[bdg_display]
  FROM [GoogleAuto2019].[Ref].[Taxonomy_Ecosystem]
  --where source_name in ('Web: Domain','App: Name','Web: URL')
  where source_name = 'Web: URL'
  ;
*/
/*

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #GoogleTicketing_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GoogleTicketing' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'white' as Live_Events
into #GoogleTicketing_addition_prep
from GoogleTicketing.dbo.Taxonomy_Ecosystem a
join GoogleTicketing.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(132 rows affected)

--select top 100 * from #GoogleTicketing_addition;

-----Pulls white apps
-- select count(*) from GoogleTicketing.dbo.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #GoogleTicketing_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'GoogleTicketing' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'white' as Live_Events
from GoogleTicketing.dbo.Taxonomy_Ecosystem a
join GoogleTicketing.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(20 rows affected)


begin try drop table #green_pull end try begin catch end catch;
select replace(REPLACE(bdg_display,'url: ',''),'url: ','') as bdg_display,source_name, category, subcategory
into #green_pull
from GoogleTicketing.dbo.Taxonomy_Ecosystem
where source_name = 'Web: URL'
;
--(5829 rows affected)


----Pulls url domains (aka green props)
-- select top 100 * from #GoogleTicketing_addition_prep where  Automotive = 'green' order by property_name;
insert into #GoogleTicketing_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GoogleTicketing' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'UNCLASSIFIED' as Automotive, 'green' as Live_Events
from #green_pull a
join GoogleTicketing.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #GoogleTicketing_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(1082 rows affected)


--One Final group by to ensure no dupes
begin try drop table #GoogleTicketing_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events
into #GoogleTicketing_addition
from #GoogleTicketing_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive, Live_Events
;
--(1234 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #GoogleTicketing_addition group by property_hash, digital_type_id order by count desc;

select * from #GoogleTicketing_addition where property_hash = 9101809637679198610 and digital_type_id = 235

delete from #GoogleTicketing_addition where property_name = 'wikia.com'; --(2 rows affected)
delete from #GoogleTicketing_addition where property_name = 'reddit.com' and ecosystem_category = 'News/Info'; --(1 row affected)
delete from #GoogleTicketing_addition where property_name = 'yahoo.com' and ecosystem_subcategory = 'News/Info: Fantasy Sports'; --(1 row affected)

-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'GoogleTicketing'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #GoogleTicketing_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(1122 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Live_Events = a.Live_Events
from #GoogleTicketing_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Live_Events = 'UNCLASSIFIED' or b.Live_Events is null)
;
--(108 rows affected)


--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GoogleTicketing_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id 
and b.ecosystem_category = 'UNCLASSIFIED'
;
--(27 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat, a.Live_Events as new_color, 
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat, b.Live_Events as old_color
from #GoogleTicketing_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'GoogleTicketing';


---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories
/* EXISTING IS BETTER
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GoogleTicketing_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'GoogleTicketing'
;
--(2 rows affected)
*/

update Core.ref.Ecosystem_Classifications set Live_Events = 'UNCLASSIFIED' where Live_Events is null; --(7269 rows affected)

*/
/*
----------------------------Google Auto (ORIGINAL)
--------First add a column for the new projects vertical if there isnt one already

/* FOR CHECKING HOW DATA LOOKS
SELECT 
      [source_name]
      ,[source_particle_key]
      ,[source_particle]
      ,[category]
      ,[subcategory]
      ,[bdg_display]
  FROM [GoogleAuto].[Ref].[Taxonomy_Ecosystem]
  --where source_name in ('Web: Domain','App: Name','Web: URL')
 -- where source_name = 'Web: Domain'
 order by source_name
  ;
*/

--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #GoogleAuto_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GoogleAuto' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'white' as Automotive
into #GoogleAuto_addition_prep
from GoogleAuto.ref.Taxonomy_Ecosystem a
join GoogleAuto.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(1673 rows affected)

--select top 100 * from #GoogleAuto_addition;

-----Pulls white apps
-- select count(*) from GoogleAuto.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #GoogleAuto_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'GoogleAuto' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'white' as Automotive
from GoogleAuto.ref.Taxonomy_Ecosystem a
join GoogleAuto.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(12 rows affected)


----Pulls url domains (aka green props)
-- select top 100 * from #GoogleAuto_addition_prep where  Automotive = 'green' order by property_name;
-- select * from GoogleAuto.ref.Taxonomy_Ecosystem where source_name = 'Web: URL' order by category, subcategory;
begin try drop table #green_pull end try begin catch end catch;
select bdg_display,source_name, category, case when category = 'Dealerships' then 'Dealerships' when category = 'Endemics/Retail' then 'Endemics/Retail' else subcategory end as subcategory
into #green_pull
from GoogleAuto.ref.Taxonomy_Ecosystem
where source_name = 'Web: URL'
;
--(28359 rows affected)

insert into #GoogleAuto_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GoogleAuto' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'green' as Automotive
from #green_pull a
join GoogleAuto.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #GoogleAuto_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(391 rows affected)


--One Final group by to ensure no dupes
begin try drop table #GoogleAuto_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive
into #GoogleAuto_addition
from #GoogleAuto_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive
;
--(2076 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #GoogleAuto_addition group by property_hash, digital_type_id order by count desc;


-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'GoogleAuto'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #GoogleAuto_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(1978 rows affected)


--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Automotive = a.Automotive
from #GoogleAuto_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Automotive = 'UNCLASSIFIED' or b.Automotive is null)
;
--(42 rows affected)

--------Always overwrites previously unclassified category
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #GoogleAuto_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'GoogleAuto'
and b.ecosystem_category = 'UNCLASSIFIED'
;
--(2 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
--OLDER WAS BETTER
select a.property_name, a.ecosystem_category as new_category, a.ecosystem_subcategory as new_subcat, a.Automotive as new_color, 
b.categorization_source as old_source, b.ecosystem_category as old_category, b.ecosystem_subcategory as old_subcat, b.Automotive as old_color
from #GoogleAuto_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'GoogleAuto'
order by new_category, new_subcat;
*/


----------------------------Google Auto 2019
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Automotive varchar(50);
*/

/* FOR CHECKING HOW DATA LOOKS
SELECT 
      [source_name]
      ,[source_particle_key]
      ,[source_particle]
      ,[category]
      ,[subcategory]
      ,[bdg_display]
  FROM [GoogleAuto2019].[Ref].[Taxonomy_Ecosystem]
  --where source_name in ('Web: Domain','App: Name','Web: URL')
  where source_name = 'Web: URL'
  ;
*/
/*
--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #VW_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GoogleAuto2019' as categorization_source, 'INT' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'white' as Automotive
into #VW_addition_prep
from GoogleAuto2019.ref.Taxonomy_Ecosystem a
join GoogleAuto2019.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(1639 rows affected)

--select top 100 * from #VW_addition;

-----Pulls white apps
-- select count(*) from GoogleAuto2019.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #VW_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'GoogleAuto2019' as categorization_source, 'INT' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'white' as Automotive
from GoogleAuto2019.ref.Taxonomy_Ecosystem a
join GoogleAuto2019.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(52 rows affected)


----Pulls url domains (aka green props)
-- select top 100 * from #VW_addition_prep where  Automotive = 'green' order by property_name;
insert into #VW_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'GoogleAuto2019' as categorization_source, 'INT' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'UNCLASSIFIED' as Alcohol_Beverage, 'green' as Automotive
from GoogleAuto2019.ref.Taxonomy_Ecosystem a
join GoogleAuto2019.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #VW_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(30 rows affected)

--One Final group by to ensure no dupes
begin try drop table #VW_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive
into #VW_addition
from #VW_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage, Automotive
;
--(1721 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #VW_addition group by property_hash, digital_type_id order by count desc;

-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'GoogleAuto2019'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #VW_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(1719 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Automotive = a.Automotive
from #VW_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Automotive = 'UNCLASSIFIED' or b.Alcohol_Beverage is null)
;
--(2 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.*
from #VW_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'GoogleAuto2019';

---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #VW_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'GoogleAuto2019'
;
--(2 rows affected)

update Core.ref.Ecosystem_Classifications set Automotive = 'UNCLASSIFIED' where Automotive is null; --(3678 rows affected)
*/

----------------------------------ABI2020
--------First add a column for the new projects vertical if there isnt one already
/*
alter table core.ref.Ecosystem_Classifications
add Alcohol_Beverage varchar(50);
*/

/* FOR CHECKING HOW DATA LOOKS
SELECT 
      [source_name]
      ,[source_particle_key]
      ,[source_particle]
      ,[category]
      ,[subcategory]
      ,[bdg_display]
  FROM [ABI2020].[Ref].[Taxonomy_Ecosystem]
  --where source_name in ('Web: Domain','App: Name','Web: URL')
  where source_name = 'Web: URL'
  ;
*/
/*
--select top 100 * FROM [Core].[ref].[Ecosystem_Classifications];
--------Pulls white domains
begin try drop table #ABI_addition_prep end try begin catch end catch;
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'ABI2020' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'white' as Alcohol_Beverage
into #ABI_addition_prep
from ABI2020.ref.Taxonomy_Ecosystem a
join ABI2020.ref.domain_name b on a.source_particle_key = b.Domain_Name_Key and a.source_name = 'Web: Domain'
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(491 rows affected)
--select top 100 * from #ABI_addition;

-----Pulls white apps
-- select count(*) from ABI2020.ref.Taxonomy_Ecosystem where  source_name = 'App: Name';
insert into #ABI_addition_prep
select b.App_Name_Key as source_property_key, b.App_Name_Hash as property_hash, 100 as digital_type_id, b.App_Name  property_name,
'ABI2020' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'white' as Alcohol_Beverage
from ABI2020.ref.Taxonomy_Ecosystem a
join ABI2020.ref.app_name b on a.source_particle_key = b.App_Name_Key and a.source_name = 'App: Name'
group by b.App_Name_Key, b.App_Name_Hash, b.App_Name, category, subcategory
;
--(25 rows affected)

----Pulls url domains (aka green props)
-- select top 100 * from #ABI_addition where  Alcohol_Beverage = 'green';
insert into #ABI_addition_prep
select b.Domain_Name_Key as source_property_key, b.Domain_Name_Hash as property_hash, 235 as digital_type_id, b.Domain_Name as property_name,
'ABI2020' as categorization_source, 'US' as categorization_country, category as ecosystem_category,  subcategory as  ecosystem_subcategory,
'UNCLASSIFIED' as Apparel, 'UNCLASSIFIED' as Computer_Electronics, 'UNCLASSIFIED' as Beauty, 'UNCLASSIFIED' as Home_Garden,  'UNCLASSIFIED' as Sporting_Goods, 
'green' as Alcohol_Beverage
from ABI2020.ref.Taxonomy_Ecosystem a
join ABI2020.ref.domain_name b on a.bdg_display = b.Domain_Name and a.source_name = 'Web: URL'
left join #ABI_addition_prep c on b.Domain_Name_Key = c.source_property_key and c.digital_type_id = 235
where c.source_property_key is null
group by b.Domain_Name_Key, b.Domain_Name_Hash, b.Domain_Name, category, subcategory
;
--(201 rows affected)

--One Final group by to ensure no dupes
begin try drop table #ABI_addition end try begin catch end catch;
select source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage
into #ABI_addition
from #ABI_addition_prep
group by source_property_key, property_hash,digital_type_id,property_name,
categorization_source, categorization_country, ecosystem_category, ecosystem_subcategory,
Apparel, Computer_Electronics,Beauty, Home_Garden, Sporting_Goods, 
Alcohol_Beverage
;
--(717 rows affected)

--dupe check:
select property_hash, digital_type_id, count(*) as count from #ABI_addition group by property_hash, digital_type_id order by count desc;

-----------First inserts in new properties into core
-- delete from Core.ref.Ecosystem_Classifications where categorization_source = 'ABI2020'
insert into Core.ref.Ecosystem_Classifications
select a.*
from #ABI_addition a
left join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_hash is null;
--(661 rows affected)

--------For properties that already exist inserts in new vertical coloring if it doesnt exist 
--Also potentially overwrite the color even if it already exists if you think the new color is better (aka just delete the where statement)
update Core.ref.Ecosystem_Classifications
set Alcohol_Beverage = a.Alcohol_Beverage
from #ABI_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.Alcohol_Beverage = 'UNCLASSIFIED' or b.Alcohol_Beverage is null)
;
--(56 rows affected)

--------If the categorization already exists, it overwrites if the new project has either a) updated info or b) a more useful categorization
--Pulls the new categorization where there is already a match to see if it is more useful than previous categorization
select a.*
from #ABI_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
and b.categorization_source != 'ABI2020';

---Overwrites (can do in multiple stages and parts if only want to overwrite for some categories
update Core.ref.Ecosystem_Classifications
set source_property_key = a.source_property_key, categorization_source = a.categorization_source, categorization_country = a.categorization_country,
ecosystem_category = a.ecosystem_category, ecosystem_subcategory = a.ecosystem_subcategory
from #ABI_addition a
join Core.ref.Ecosystem_Classifications b on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id and b.categorization_source != 'ABI2020'
;
--(56 rows affected)

update Core.ref.Ecosystem_Classifications set Alcohol_Beverage = 'UNCLASSIFIED' where Alcohol_Beverage is null; --(1943 rows affected)
*/