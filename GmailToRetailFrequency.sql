-- drop table #retailClickstream
-- drop table #gmailClickstream
-- drop table #conversionClickstream
-- drop table #lapseFrequency

SELECT SSE.[Panelist_ID] AS ID, SSE.[Datetime], SSE.[Notes] AS Domain, TEC.[eco_category1] AS category
INTO #retailClickstream
FROM [YouTubeShopping].[dbo].[vw_SequenceSessionEcosystem] SSE
INNER JOIN [YouTubeShopping].[dbo].[TaxonomyEcosystemCorrected] TEC ON SSE.TaxID_Whitelist = TEC.taxid_whitelist
WHERE eco_category1 = 'Brand' OR eco_category1 = 'Category Retailer' OR eco_category1 = 'General Retailer' OR eco_category1 = 'Other: Online Marketplace'

SELECT [PanelistId] AS ID, [StartTimeUtc], [PageUrl]
INTO #gmailClickstream
FROM [YouTubeShopping].[dbo].[RealLifeWeb]
WHERE [PageUrl] LIKE '%mail.google.com%'

SELECT RC.[ID], GM.[StartTimeUtc] AS searchTime, RC.[Datetime] AS retailTime, DATEDIFF(SECOND, GM.[StartTimeUtc], RC.[Datetime]) AS timeElapsed
INTO #conversionClickstream
FROM #retailClickstream RC
INNER JOIN #gmailClickstream GM ON RC.ID = GM.ID
WHERE DATEDIFF(SECOND, GM.[StartTimeUtc], RC.[Datetime]) >= 0 AND  DATEDIFF(SECOND, GM.[StartTimeUtc], RC.[Datetime]) <= 299

SELECT COUNT(*) AS frequency, timeElapsed
INTO #lapseFrequency
FROM #conversionClickstream
GROUP BY timeElapsed

SELECT * FROM #lapseFrequency
ORDER BY timeElapsed