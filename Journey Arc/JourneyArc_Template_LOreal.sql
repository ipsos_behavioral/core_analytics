---------------------------------------------------------------------------------------------------
-- Journey Arc created with Loreal 2018 database 
-- Shirley 2018/4/30
---------------------------------------------------------------------------------------------------

-- 0) DEFINE GLOBAL VARIABLE
---------------------------------------------------------------------------------------------------
use Loreal2018;
declare @sql varchar(3000);

-- 1) CONTENT PATCHES. E.G. LOREAL SKINCARE LISTED WITH OTHER
---------------------------------------------------------------------------------------------------
select * from ref.pattern_table_beauty where filter_name='Topic';

-- select top 100 * from ref.taxonomy_ecosystem_jk;
begin try drop table ref.taxonomy_ecosystem_jk end try begin catch end catch;
select *
into ref.taxonomy_ecosystem_jk
from ref.taxonomy_ecosystem a;

select a.*
from ref.taxonomy_ecosystem_jk a
inner join  ref.pattern_table_beauty b
	on a.source_particle like b.pattern
	and a.source_name='';

-- select * from #survey_purchase_date_prep5;

begin try drop table #survey_purchase_date_prep5 end try begin catch end catch;
SELECT a.Respondent_ID as respondent_id, 
b.Panelist_Key as panelist_key,
1 as survey_type_id,
NULL as purchase_location,
NULL as purchase_onoffline,
dateadd(second, -1, dateadd(day, 1, cast (a.Cat_A_Purchase_Date as datetime))) as purchase_date
-- [Brand Purchased] as brand_purchase
into #survey_purchase_date_prep5
	from dbo.Survey_Consolidated_purchase a
	inner join ref.panelist b
	on a.respondent_ID = b.Vendor_Panelist_ID and a.Cat_A_Purchase_Date is not null;
-- (4673 row(s) affected)

-- 2) CREATE LOOKUPS 
---------------------------------------------------------------------------------------------------
-- select * from #platform_lookup;
select val_id as platform_id, val_name as platform_name
into #platform_lookup
from ref.variable_names a
where var_name='platform_id' and val_id in (1,2); 
insert into #platform_lookup select 0, 'All Platforms';

create index platform_lookup1 on #platform_lookup (platform_id);

insert into #platform_lookup select 90, 'Other';
--select top 100 * from ref.variable_names

-- select * from #digital_type_lookup;
select val_id as digital_type_id, val_name as digital_type
into #digital_type_lookup
from ref.variable_names a
where var_name='digital_type_id';

insert into #digital_type_lookup select 0, 'All Digital';

create index digital_type_lookup1 on #digital_type_lookup (digital_type_id);

insert into #digital_type_lookup select 999, '...';

-- DEFINE CLUSTERS HERE
------------------------
-- SUBCAT MAPS TO CLUSTER
-- select * from #subcat_to_cluster;
-- drop table #subcat_to_cluster;
select subcat_id, subcategory, cat_id, category,
	subcat_id+2000 as cluster_id, subcategory as cluster,
	subcat_id as internal_group
	--case when cat_id in (2100, 2200, 2400, 2600, 2700) then subcat_id else cat_id end as internal_group -- touchpoints so similar, we consider sequential visits the same activity.
into #subcat_to_cluster
from ref.taxonomy_ecosystem
group by cat_id, category, subcat_id, subcategory;

-- (Brands)
update #subcat_to_cluster set cluster_id=5100, cluster='Brand' where cat_id=2100;

-- (Category Retailer)
update #subcat_to_cluster set cluster_id=5200, cluster='Beauty Retailer' where cat_id=2200;

-- (General Retailer)
update #subcat_to_cluster set cluster_id=5300, cluster='General Retailer' where cat_id in (2300,2400);

-- (News/Info)
update #subcat_to_cluster set cluster_id=5500, cluster='Reviews/News/Info' where cat_id in (2500);

-- (Search)
update #subcat_to_cluster set cluster_id=5600, cluster='Search' where cat_id in (2600);

-- (Social)
update #subcat_to_cluster set cluster_id=5700, cluster='Social' where cat_id in (2700);

-- (Video)
update #subcat_to_cluster set cluster_id=5800, cluster='Video' where cat_id in (2800);

-- (Other)
update #subcat_to_cluster set cluster_id=5900, cluster='Other' where cat_id in (2900);

create index subcat_to_cluster1 on #subcat_to_cluster (subcat_id);

-------------------------------------------
-- PARTICLIE MAPS TO CONTENT TYPE
-------------------------------------------
-- QA check
-- select purchase_location as subcategory, purchase_onoffline from dbo.Survey_Purchase_Dates group by purchase_location, purchase_onoffline order by 1;
-- select * from dbo.Survey_Purchase_Dates where panelist_key=1353;

-- select top 1000 * from ref.taxonomy_ecosystem;
-- select distinct subcat_id, subcategory, cluster_id, cluster, internal_group from #ecosystem_lookup;
-- select * from #ecosystem_lookup;
-- select * from #ecosystem_lookup where cluster_id<=999;
-- select cluster_id, cluster, count(*) from #ecosystem_lookup group by cluster_id, cluster order by 1;
-- drop table #ecosystem_lookup;
select a.taxonomy_ecosystem_key as tax_id, a.subcat_id, a.subcategory, cluster_id, cluster, b.internal_group,
	a.[3011] as flg_makeup,
	a.[3012] as flg_skincare,
	a.[3013] as flg_haircare,
	a.[3014] as flg_haircolor
into #ecosystem_lookup
from ref.taxonomy_ecosystem a
inner join #subcat_to_cluster b
	on a.subcat_id=b.subcat_id;

insert into #ecosystem_lookup select 0 as tax_id, 0, '[End]', 0, '[End]', 9, 1, 1, 1, 1;
insert into #ecosystem_lookup select -1, 1, '[Buy]', 1, '[Buy]', 1, 0, 0, 0, 1;
insert into #ecosystem_lookup select -10, 1, '[Buy]', 1, '[Buy]', 1, 0, 0, 1, 0;
insert into #ecosystem_lookup select -100, 1, '[Buy]', 1, '[Buy]', 1, 0, 1, 0, 0;
insert into #ecosystem_lookup select -1000, 1, '[Buy]', 1, '[Buy]', 1, 1, 0, 0, 0;

create index ecosystem_lookup1 on #ecosystem_lookup (tax_id);

-- ## QA ##. LOOKS LIKE bedbathbeyond.com mis-classified as Category Retailer, and volume is really high.
-- Patch. Remove non-semantic bedbathbeyond.com. Reclassify semantic bedbathbeyond.com as Gen Retailer. Superstore?
-- select * from ref.taxonomy_ecosystem where source_particle like '%bed%bath%beyond%';
-- Upon checking none of the semantic records look legit. Removing all traffic.
delete from #ecosystem_lookup where tax_id in ( 1965, 6815, 6816, 6817);

-- VERTICAL PURE ECOSYSTEM ENTRIES
-------------------------------------------
select a.tax_id, a.subcat_id, a.subcategory, cluster_id, cluster, a.internal_group,
	b.source_name, b.source_particle
into #ecosystem_pure_makeup
from #ecosystem_lookup a
inner join ref.taxonomy_ecosystem  b
	on a.tax_id=b.Taxonomy_Ecosystem_Key
where flg_makeup=1
	and flg_skincare=0
	and flg_haircare=0
	and flg_haircolor=0;

-- select * from #ecosystem_pure_skincare;
select a.tax_id, a.subcat_id, a.subcategory, cluster_id, cluster, a.internal_group,
	b.source_name, b.source_particle
into #ecosystem_pure_skincare
from #ecosystem_lookup a
inner join ref.taxonomy_ecosystem  b
	on a.tax_id=b.Taxonomy_Ecosystem_Key
where flg_makeup=0
	and flg_skincare=1
	and flg_haircare=0
	and flg_haircolor=0;

-- select * from #ecosystem_pure_haircare;
select a.tax_id, a.subcat_id, a.subcategory, cluster_id, cluster, a.internal_group,
	b.source_name, b.source_particle
into #ecosystem_pure_haircare
from #ecosystem_lookup a
inner join ref.taxonomy_ecosystem  b
	on a.tax_id=b.Taxonomy_Ecosystem_Key
where flg_makeup=0
	and flg_skincare=0
	and flg_haircare=1
	-- and flg_haircolor=0 -- allow for hair color mention
	;

-- select * from #ecosystem_pure_haircolor;
select a.tax_id, a.subcat_id, a.subcategory, cluster_id, cluster, a.internal_group,
	b.source_name, b.source_particle
into #ecosystem_pure_haircolor
from #ecosystem_lookup a
inner join ref.taxonomy_ecosystem  b
	on a.tax_id=b.Taxonomy_Ecosystem_Key
where flg_makeup=0
	and flg_skincare=0
	-- and flg_haircare=0 allow for haircare mention
	and flg_haircolor=1;

-- 3) MAKE SESSION LEVEL VIEW OF SEQUENCE EVENT
---------------------------------------------------------------------------------------------------
-- select top 100 * from dbo.v_Processed_Sequence_Event_Ecosystem;

-- select top 100 * from dbo.taxonomy_ecosystem
-- select top 1000 * from [Loreal2018].[dbo].[v_Report_Sequence_Events] where  panelist_id='30378808' and source_id = 285818 

-- Remove entries designated as not relevant to journey.
-- select * from #temp_sequence_ecosystem; 
-- drop table #temp_sequence_ecosystem;
select panelist_key, platform_id, digital_type_id, day_id, session_id,
	start_time_local, cast(active_seconds/60.0 as numeric(20,5)) as raw_minutes, 
	a.tax_id, a.notes, a.value,
	flg_makeup, flg_skincare, flg_haircare, flg_haircolor
into #temp_sequence_ecosystem
from run.v_Sequence_Event_Ecosystem a
inner join ref.taxonomy_ecosystem b
	on a.tax_id=b.taxonomy_ecosystem_key
inner join #ecosystem_lookup c
	on a.tax_id=c.tax_id
	and c.cluster!='[Remove]'
--where digital_type_id <> 100; -- ### QA Patch for Loreal2018. Remove Apps. ###

-- select top 100 * from #temp_sequence_session_ecosystem;
-- drop table #temp_sequence_session_ecosystem;
select panelist_key, platform_id, digital_type_id, day_id, session_id, 
	flg_makeup, flg_skincare, flg_haircare, flg_haircolor,
	min(start_time_local) as start_time, sum(raw_minutes) as raw_minutes, count(*) as raw_events 
into #temp_sequence_session_ecosystem
from #temp_sequence_ecosystem a
group by panelist_key, platform_id, digital_type_id, day_id, session_id,
		flg_makeup, flg_skincare, flg_haircare, flg_haircolor;
-- (23415 row(s) affected)

create index temp_sequence_session_ecosystem1 on #temp_sequence_session_ecosystem (panelist_key);
create index temp_sequence_session_ecosystem2 on #temp_sequence_session_ecosystem (session_id);
create index temp_sequence_session_ecosystem3 on #temp_sequence_session_ecosystem (start_time);

-- particle detail.... but duplicates removed for cases where session timestamp is identical
---------------------------------------------------------------------------------------------
-- drop table #temp_representative_ids_that_millisecond;
select panelist_key, session_id, start_time_local as start_time, min(tax_id) as min_tax_id, min(notes) as min_domain_hash, min(value) as min_url_hash,
	count(*) as simultaneous_pages
into #temp_representative_ids_that_millisecond
from #temp_sequence_ecosystem a
group by panelist_key, session_id, start_time_local;
-- (102954 row(s) affected)

create index temp_representative_ids_that_millisecond1 on #temp_representative_ids_that_millisecond (panelist_key);
create index temp_representative_ids_that_millisecond2 on #temp_representative_ids_that_millisecond (session_id);
create index temp_representative_ids_that_millisecond3 on #temp_representative_ids_that_millisecond (start_time);

-- select top 100 * from #temp_sequence_session_ecosystem2;
-- select count(*) from #temp_sequence_session_ecosystem2;
-- drop table #temp_sequence_session_ecosystem2;
select a.panelist_key, platform_id, digital_type_id, day_id, a.session_id, a.start_time, raw_minutes,
	cast(raw_events as numeric(20,3)) as raw_events,
	b.min_tax_id, c.internal_group,
	a.flg_makeup, a.flg_skincare, a.flg_haircare, a.flg_haircolor
into #temp_sequence_session_ecosystem2
from #temp_sequence_session_ecosystem a
inner join #temp_representative_ids_that_millisecond b
	on a.panelist_key=b.panelist_key
	and a.session_id = b.session_id
	and a.start_time = b.start_time
inner join #ecosystem_lookup c
	on b.min_tax_id=c.tax_id;
-- (31556 row(s) affected)

---------------------------------
--OFFLINE PURCHASE DATES
---------------------------------

insert into #temp_sequence_session_ecosystem2
select panelist_key, 
	0 as platform_id,
	0 as digital_type_id,
	cast(purchase_date as date) as day_id,
	0 as session_id,
	--purchase_date as start_time, 
	purchase_date AS start_time, -- chose last minute of buy date
	0 as raw_minutes,
	1 as raw_events,
	-1 as min_tax_id,
	1 as internal_group,
	1 as flg_makeup, 1 as flg_skincare, 1 as flg_haircare, 1 as flg_haircolor
from #survey_purchase_date_prep5;

-- Insert end touchpoint session
-- Note: This should run after the stated purchases are inserted, because we want opportunity for purchase to be end event.
---------------------------------------------------------------------------------------------------------------------------
insert into #temp_sequence_session_ecosystem2
select panelist_key,
	0 as platform_id, 0 as digital_type_id, max(day_id) as day_id, 0 as session_id,
	dateadd(minute,1,max(start_time)) AS start_time, -- choose time right after last event
	0 as raw_minutes, 0 as raw_events, 0 as min_tax_id, 0 as internal_group,
	1 as flg_makeup, 1 as flg_skincare, 1 as flg_haircare, 1 as flg_haircolor
from #temp_sequence_session_ecosystem2
group by panelist_key;
-- (947 row(s) affected)


-- Remove panelists only 1 touchpoint (therefore no path)
---------------------------------------------------------

-- Identify panelists that only had 1 activity. Remove them from journy analysis.
-- select * from #panelist_touchpoints where ct_groups <=1;
-- select * from #temp_sequence_session_ecosystem2 where panelist_key=1826 order by start_time
-- drop table #panelist_touchpoints;

select 1 as report_vertical, panelist_key, min(start_time) as first_event_time, count(start_time) as ct_groups
into #panelist_touchpoints
from #temp_sequence_session_ecosystem2
where min_tax_id not in (0) -- endpoints do not count
group by panelist_key;

insert into #panelist_touchpoints
select 2 as report_vertical, panelist_key, min(start_time) as first_event_time, count(start_time) as ct_groups
from #temp_sequence_session_ecosystem2
where min_tax_id not in (0) -- endpoints do not count
	and flg_makeup=1
group by panelist_key;

insert into #panelist_touchpoints
select 3 as report_vertical, panelist_key, min(start_time) as first_event_time, count(start_time) as ct_groups
from #temp_sequence_session_ecosystem2
where min_tax_id not in (0) -- endpoints do not count
	and flg_skincare=1
group by panelist_key;

insert into #panelist_touchpoints
select 4 as report_vertical, panelist_key, min(start_time) as first_event_time, count(start_time) as ct_groups
from #temp_sequence_session_ecosystem2
where min_tax_id not in (0) -- endpoints do not count
	and flg_haircare=1
group by panelist_key;

insert into #panelist_touchpoints
select 5 as report_vertical, panelist_key, min(start_time) as first_event_time, count(start_time) as ct_groups
from #temp_sequence_session_ecosystem2
where min_tax_id not in (0) -- endpoints do not count
	and flg_haircolor=1
group by panelist_key;

create index panelist_touchpoints1 on #panelist_touchpoints (panelist_key);

-- select report_vertical, count(*) from #panelist_touchpoints group by report_vertical;

-- create content views. append subcat and cluster
-- select top 100 * from #temp_sequence_session_ecosystem3;
-- drop table #temp_sequence_session_ecosystem3;
select c.report_vertical,
	a.*, b.subcat_id, b.subcategory, b.cluster_id, b.cluster
into #temp_sequence_session_ecosystem3
from #temp_sequence_session_ecosystem2 a
inner join #ecosystem_lookup b
	on a.min_tax_id=b.tax_id
inner join #panelist_touchpoints c
	on a.panelist_key=c.panelist_key

-- identify first event
-- select * from #panelist_first_event_time;
-- drop table #panelist_first_event_time;
select report_vertical, Panelist_Key, min(start_time) as first_event_time
into #panelist_first_event_time
from #temp_sequence_session_ecosystem3
group by report_vertical, Panelist_Key;

create index panelist_first_event_time1 on #panelist_first_event_time (panelist_key);
create index panelist_first_event_time2 on #panelist_first_event_time (report_vertical);
create index panelist_first_event_time3 on #panelist_first_event_time (first_event_time);

-- remove records where buy is first event;
-- select top 100 * from #temp_sequence_session_ecosystem4 order by panelist_key, start_time, report_vertical
-- drop table #temp_sequence_session_ecosystem4;
select a.*
into #temp_sequence_session_ecosystem4
from #temp_sequence_session_ecosystem3 a
left outer join #panelist_first_event_time b
	on a.report_vertical=b.report_vertical
	and a.Panelist_Key=b.Panelist_Key
	and a.start_time=b.first_event_time
	and a.cluster_id=1 -- buy records only
where b.panelist_key is null;

create index temp_sequence_session_ecosystem1 on #temp_sequence_session_ecosystem4 (panelist_key);
create index temp_sequence_session_ecosystem2 on #temp_sequence_session_ecosystem4 (report_vertical);

-- L'Oreal Specific
delete from #temp_sequence_session_ecosystem4 where report_vertical=2 and flg_makeup=0;
delete from #temp_sequence_session_ecosystem4 where report_vertical=3 and flg_skincare=0;
delete from #temp_sequence_session_ecosystem4 where report_vertical=4 and flg_haircare=0;
delete from #temp_sequence_session_ecosystem4 where report_vertical=5 and flg_haircolor=0;

-- drop table #temp_content_panelist_group_counts;
select report_vertical, panelist_key, count(distinct internal_group) as ct_groups
into #temp_content_panelist_group_counts
from #temp_sequence_session_ecosystem4
where min_tax_id != 0 -- endpoints do not count
group by report_vertical, panelist_key;

create index temp_content_panelist_group_counts1 on #temp_content_panelist_group_counts (panelist_key);
create index temp_content_panelist_group_counts2 on #temp_content_panelist_group_counts (report_vertical);

-- remove cases where only 1 touchpoint in ecosystem (no journey)
-- select top 100 * from #temp_sequence_session_ecosystem5
-- drop table #temp_sequence_session_ecosystem5;
select a.*
into #temp_sequence_session_ecosystem5
from #temp_sequence_session_ecosystem4 a
inner join #temp_content_panelist_group_counts b
	on a.Panelist_Key=b.Panelist_Key
	and a.report_vertical=b.report_vertical
	and b.ct_groups>1;
-- (24075 row(s) affected)


-- 4) PARTITION BY PERSON. AND ALSO VERTICAL IF SUB-ECOSYSTEMS EXIST (e.g. Verticals within Shopping Retail)
------------------------------------------------------------------------------------------------------------
-- select top 100 * from #partitioned_points_ordered;
begin try drop table #partitioned_points_ordered end try begin catch end catch;
select row_number() over (partition by report_vertical, panelist_key order by report_vertical, panelist_key, day_id, start_time asc) as row_num,
	report_vertical, panelist_key, platform_id, digital_type_id, day_id, session_id, start_time, subcategory, cluster_id, cluster, internal_group
into #partitioned_points_ordered
from #temp_sequence_session_ecosystem5 a
order by report_vertical, panelist_key, day_id, start_time;
-- (24075 row(s) affected)

-- select top 100 * from #agg_paths;
begin try drop table #agg_paths end try begin catch end catch;
select a.report_vertical, a.panelist_key, a.session_id, a.platform_id, a.digital_type_id, 
	a.day_id, a.start_time, a.subcategory, a.cluster_id, a.cluster, a.internal_group,
	isnull(b.platform_id, a.platform_id) as prev_platform_id,
	isnull(b.digital_type_id, a.digital_type_id) as prev_digital_type_id,
	isnull(b.day_id, a.day_id) as prev_day_id,
	isnull(b.start_time, dateadd(minute,-1,a.start_time)) as prev_start_time,
	isnull(b.subcategory, '') as prev_subcategory,
	isnull(b.cluster_id,0) as prev_cluster_id,
	isnull(b.cluster, '') as prev_cluster,
	isnull(b.internal_group,0) as prev_internal_group
into #agg_paths
from #partitioned_points_ordered a
left join #partitioned_points_ordered b
	on a.report_vertical=b.report_vertical
	and a.panelist_key=b.panelist_key
	and a.row_num=b.row_num+1;
-- (24075 row(s) affected)

-- 5) JOIN TO LOOKUPS AND EXPORT
------------------------------------------------------------------------------------------------------------
-- select count(*) from rpt.Output_Web_Journey;
-- select top 1000 * from #agg_path2 order by report_vertical, panelist_key, start_time;
begin try drop table #agg_path2 end try begin catch end catch;
select report_vertical, a.panelist_key, A.Session_ID, p.platform_name, start_time,
	cluster, cluster_id, subcategory, 
	prev_start_time, isnull(p2.platform_name,'') as prev_platform_name,
	case when prev_cluster = '' then '[Start]' else prev_cluster end as prev_cluster,
	case when prev_cluster_id = '' then 0 else prev_cluster_id end as prev_cluster_id,
	case when prev_subcategory = '' then '[Start]' else prev_subcategory end as prev_subcategory
--into rpt.Journey_Sessions
into #agg_path2
from #agg_paths a
left outer join #platform_lookup p
	on a.platform_id=p.platform_id
left outer join #platform_lookup p2
	on a.prev_platform_id=p2.platform_id
where (a.prev_internal_group <> a.internal_group) 
or ((a.prev_day_id <> a.Day_ID) and a.internal_group != 1) -- remove internals
;
-- (20401 row(s) affected)

-- identify first event
-- select * from #panelist_first_event_time;
-- drop table #panelist_first_event_time;
begin try drop table #panelist_first_event_time_v1 end try begin catch end catch;
select report_vertical, Panelist_Key, min(start_time) as first_event_time
into #panelist_first_event_time_v1
from #agg_path2
group by report_vertical, Panelist_Key;

create index panelist_first_event_time1 on #panelist_first_event_time_v1 (panelist_key);
create index panelist_first_event_time2 on #panelist_first_event_time_v1 (report_vertical);
create index panelist_first_event_time3 on #panelist_first_event_time_v1 (first_event_time);

-- remove records where buy is first event;
-- select top 1000 * from #agg_path3 order by report_vertical, panelist_key, start_time;
begin try drop table #agg_path3 end try begin catch end catch;
select a.*
into #agg_path3
from #agg_path2 a
left outer join #panelist_first_event_time_v1 b
	on a.report_vertical=b.report_vertical
	and a.Panelist_Key=b.Panelist_Key
	and a.start_time=b.first_event_time
	and a.cluster_id=1 -- buy records only
where b.panelist_key is null
;
-- (1094 row(s) affected)
-- 1089

create index temp_sequence_session_ecosystem1 on #agg_path3 (panelist_key);
create index temp_sequence_session_ecosystem2 on #agg_path3 (report_vertical);

-- identify first event
-- select * from #panelist_first_event_time;
-- drop table #panelist_first_event_time;
begin try drop table #panelist_first_event_time_v2 end try begin catch end catch;
select report_vertical, Panelist_Key, min(start_time) as first_event_time
into #panelist_first_event_time_v2
from #agg_path3
group by report_vertical, Panelist_Key;
-- (408 row(s) affected)

create index panelist_first_event_time1 on #panelist_first_event_time_v2 (panelist_key);
create index panelist_first_event_time2 on #panelist_first_event_time_v2 (report_vertical);
create index panelist_first_event_time3 on #panelist_first_event_time_v2 (first_event_time);

-- select top 100 * from #journey_report order by report_vertical, panelist_key, start_time;
begin try drop table #journey_report end try begin catch end catch;
select a.report_vertical, a.Panelist_Key, a.session_id, a.platform_name, a.start_time, a.cluster, a.cluster_id, a.subcategory,a.prev_start_time, a.prev_platform_name,
case when b.Panelist_Key is not NULL then '[Start]' 
else a.prev_cluster end as prev_cluster,
case when b.Panelist_Key is not null then 0 
else a.prev_cluster_id end as prev_cluster_id,
case when b.Panelist_Key is not null then '[Start]' 
else a.prev_subcategory end as prev_subcategory
into #journey_report
from #agg_path3 a
left outer join #panelist_first_event_time_v2 b
	on a.report_vertical=b.report_vertical
	and a.Panelist_Key=b.Panelist_Key
	and a.start_time=b.first_event_time
;
-- (1094 row(s) affected)
-- 1089

create index temp_sequence_session_ecosystem1 on #journey_report (panelist_key);
create index temp_sequence_session_ecosystem2 on #journey_report(report_vertical);

-- Identify panelists who only had 1 activity after removing buy event (if buy was the first activity).
-- select * from #event_count;
-- drop table #event_count;
select Panelist_Key,count(*) as event_count
into #event_count
from #journey_report
where cluster_id <> 0 -- end point doesn't count
group by Panelist_Key
order by Panelist_Key;


-------------------------------------------------------------------------------------------------------------
-- Permanent table created here:
-------------------------------------------------------------------------------------------------------------
-- select top 1000 * from rpt.Journey_Sessions_include_buy order by report_vertical, panelist_key, start_time;
-- select top 1000 * from #journey_report;
begin try drop table rpt.Journey_Sessions_include_buy end try begin catch end catch;
select 
	report_vertical,
	a.panelist_key,
	Session_ID,
	platform_name,
	start_time,
	cluster,
	cluster_id,
	subcategory,
	prev_start_time,
	prev_platform_name,
	prev_cluster,
	prev_cluster_id,
	prev_subcategory
	--DATEDIFF(hh, prev_start_time,start_time) as hours_diff
into rpt.Journey_Sessions_include_buy
from #journey_report a
join ref.Panelist b
	on a.panelist_key = b.Panelist_Key
inner join #event_count c
	on a.Panelist_Key = c.Panelist_Key
	and c.event_count > 1
;

--------------------------------------------
-- Individual p2p table
--------------------------------------------
-- select top 100 * from Loreal2018.run.v_Sequence_Event_Ecosystem;
-- select count(*) from Loreal2018.run.v_Sequence_Event_Ecosystem; -- 45668
-- select top 100 * from ref.url_ecosystem_additions;

-- select top 1000 * from #domain_url_lookup order by panelist_key, start_time_local;
-- drop table #domain_url_lookup;
select a.*, c.domain_name
into #domain_url_lookup
from run.v_Sequence_Event_Ecosystem a
left join ref.Domain_Name c
on a.Notes = c.Domain_Name_Key
;
--(45668 row(s) affected)

-- select * from #p2p_search order by panelist_key, date;
-- drop table #p2p_search;
select
	Session_ID, 
	DATEPART(dw,Day_ID) as dow,
	Panelist_Key, 
	digital_type_ID, 
	Platform_ID,
	domain_name as domain_or_app, 
	Day_ID as date, 
	Start_Time_Local, 
	end_time_local, 
	active_seconds
into #p2p_search
from #domain_url_lookup
	order by Start_Time_Local;
-- (45668 row(s) affected)

----------------------------------------------------------------
-- Combine journey & p2p tables to create Journey Arc table: 
----------------------------------------------------------------
-- select * from #p2p_search order by panelist_key, start_time_local;
-- select * from #journey_arc order by panelist_key, report_vertical, start_time,Start_Time_Local;
-- drop table #journey_arc;
select report_vertical,a.session_id,a.Panelist_Key, b.digital_type_ID, b.dow, start_time, CONVERT(VARCHAR(10), start_time, 126) as start_date, 
prev_cluster, prev_subcategory, prev_start_time as prev_cluster_start_time, CONVERT(VARCHAR(10), prev_start_time, 126) as prev_cluster_start_date, 
b.domain_or_app,
cluster, subcategory, b.Start_Time_Local, b.active_seconds
into #journey_arc
from rpt.Journey_Sessions_include_buy a
left join #p2p_search b
on a.Session_ID = b.Session_ID and a.start_time = b.Start_Time_Local
join ref.Panelist c
	on a.panelist_key = c.Panelist_Key
inner join #event_count d
	on a.Panelist_Key = d.Panelist_Key
	and d.event_count > 1
where report_vertical = 1 
order by panelist_key, report_vertical, start_time;
-- (10970 row(s) affected)


----------------------------------------------------------------------------------------------------------
---------------------------------- Calculate Journey Arc Metrics -----------------------------------------
----------------------------------------------------------------------------------------------------------
-- First/last/most engaged days:
	-- most common 1st touchpoint
	-- is it the most engaged day?
	-- % of days that are weekends
	-- median time of day
	-- median time of day of the 1st touchpoint
	-- (# of brands)
	-- (when strated research?)

-- day of buy:
	-- most common 1st touchpoint
	-- most common touchpoint before buy
	-- is it the most engaged day?
	-- % of days that are weekends
	-- median time of day
	-- median time of day of the 1st touchpoint
	-- (# of brands)

-- Create Journey Arc final table:
-- select * from #journey_arc_final;
-- drop table #journey_arc_final;
create table #journey_arc_final(
journey_stage varchar(255),
median_date date, 
total_engagement_mins float,
most_common_first_touchpoint varchar(255), 
most_common_touchpoint_beforebuy varchar(255),
most_common_first_domain varchar(255), 
most_engaged_day_ratio float, 
weekend_ratio float, 
median_timeofday_all time, 
median_timeofday_first_touchpoint time
);

insert into #journey_arc_final(journey_stage) select 'Start Day';
insert into #journey_arc_final(journey_stage) select 'Day of Buy';
insert into #journey_arc_final(journey_stage) select 'Most Engaged Day';
insert into #journey_arc_final(journey_stage) select 'End Day';




-------------------------------------------------------------------------------------------------------------

-- prep: daily engagement (sum of dur_mins)
	-- select top 1000 * from #daily_engagement order by panelist_key, start_date;
	-- drop table #daily_engagement;
	select Panelist_Key, start_date, sum(active_seconds) as total_seconds 
	into #daily_engagement
	from #journey_arc
	where cluster <> '[End]' and cluster <> '[Buy]' --exclude buy and end events
	group by Panelist_Key, start_date
	order by Panelist_Key, start_date;

	-- select * from #max_records1 order by panelist_key;
	-- drop table #max_records1; 
	select Panelist_Key, max(total_seconds) as max_records 
	into #max_records1
	from #daily_engagement 
	group by Panelist_Key;

-- Flag most engaged days
	-- select * from #daily_engagement_flag order by Panelist_Key, start_date;
	-- drop table #daily_engagement_flag;
	select a.panelist_key, start_date, total_seconds, 
	case when a.total_seconds = b.max_records then 1 else 0 end as max_engagement_flag
	into #daily_engagement_flag
	from #daily_engagement a
	join #max_records1 b
	on a.panelist_key = b.panelist_key
	order by a.Panelist_Key, start_date;

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------First Day---------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------
-- prep: first day activities
	-- select * from #first_day_activities order by panelist_key, start_time;
	-- select * from #first_day_activities where prev_cluster = '[Start]' order by panelist_key, start_time;
	-- drop table #first_day_activities;
	select a.*
	into #first_day_activities
	from #journey_arc a
	inner join (select Panelist_Key, min(start_date) as first_day from #journey_arc group by Panelist_Key) b
	on a.Panelist_Key = b.Panelist_Key and a.start_date = b.first_day
	order by a.Panelist_Key, a.start_time;

	-- first touchpoint activities
	-- select * from #first_touchpoint_activities order by panelist_key, start_time;
	-- drop table #first_touchpoint_activities;
	select a.*
	into #first_touchpoint_activities
	from #journey_arc a
	inner join (select Panelist_Key, min(start_time) as first_day from #journey_arc group by Panelist_Key) b
	on a.Panelist_Key = b.Panelist_Key and a.start_time = b.first_day
	order by a.Panelist_Key, a.start_time;
------------------------------------------- total engagement (secs)-----------------------------------------------------
	-- select * from #first_day_engagement;
	-- drop table #first_day_engagement;
	select sum(active_seconds)/60 as total_engagement_mins
	into #first_day_engagement
	from #first_day_activities;

--------------------------------------------Most common 1st touchpoint-----------------------------------------------------
	-- select * from #top_firsttouchpoint_firstday order by cluster_counts desc;
	-- drop table #top_firsttouchpoint_firstday;
	select 'Start Day' as journey_stage, cluster, count(*) as cluster_counts
	into #top_firsttouchpoint_firstday
	from #first_touchpoint_activities
	group by cluster
	order by cluster_counts desc;

	/**
	insert into #journey_arc_final(most_common_first_touchpoint)
	select top(1) cluster
	from #top_firsttouchpoint_firstday
	order by cluster_counts desc;
	**/

--------------------------------------------Most common 1st domain---------------------------------------------------------
	-- select * from #top_firstdomain_firstday order by domain_counts desc;
	-- drop table #top_firstdomain_firstday;
	select 'Start Day' as journey_stage, domain_or_app, count(*) as domain_counts
	into #top_firstdomain_firstday
	from #first_touchpoint_activities
	group by domain_or_app
	order by domain_counts desc;

	/**
	insert into #journey_arc_final(most_common_first_domain)
	select top(1) domain_or_app
	from #top_firstdomain_firstday
	order by domain_counts desc;
	**/



----------------------- Identify the most engaged day and calculate % of the most engaged first day.------------------------
	-- identify first days
	-- select * from #firstday_mostengaged_check order by panelist_key;
	-- drop table #firstday_mostengaged_check;
	select a.Panelist_Key, start_date as first_day, total_seconds, b.max_engagement_flag
	into #firstday_mostengaged_check
	from
	(select Panelist_Key, min(start_date) as first_day from #daily_engagement_flag group by Panelist_Key) as a 
	inner join #daily_engagement_flag as b 
	on a.Panelist_Key = b.Panelist_Key and a.first_day = b.start_date;

	-- Calculate % of firstdays that are most engaged 
    ------------------------------------------------------------------------------------------------------------
	-- select * from #most_engagedday_ratio_startday;
	-- drop table #most_engagedday_ratio_startday;
	select sum(max_engagement_flag) as total_mostengaged_firstday, count(max_engagement_flag) as count_all_firstdays, 
	CAST(sum(max_engagement_flag) as float)/cast(count(max_engagement_flag) as float) as mostengaged_firstday_ratio
	into #most_engagedday_ratio_startday
	from #firstday_mostengaged_check;
	------------------------------------------------------------------------------------------------------------


-------------------------------------- % first day is on weekends-----------------------------------------------
	-- select * from #weekend_ratio_starday;
	-- drop table #weekend_ratio_starday;
	select
	(select count(*) from #first_touchpoint_activities where dow in (1,7)) as weekend_count,
	count(*) as total_count,
	CAST((select count(*) from #first_touchpoint_activities where dow in (1,7)) as float)/cast(count(*) as float) as firstday_weekend_percentage
	into #weekend_ratio_starday
	from #first_touchpoint_activities; 


----------------------------------------- Median time of day for *all first day touchpoints* -------------------------------------------------
	-- select * from #first_day_activities order by Panelist_Key, start_date;
	-- select * from #firstday_timerank order by rank;
	-- drop table #firstday_timerank;
	select ROW_NUMBER() OVER(ORDER BY cast(start_time as time) ASC) AS Rank, Panelist_Key, start_time, cast(start_time as time) as time_of_day
	into #firstday_timerank
	from #first_day_activities
	order by time_of_day;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_timeofday_startday;
	-- drop table #median_timeofday_startday;
	SELECT
	   dateadd(ms, datediff(ms, min(g.time_of_day), max(g.time_of_day))/2, min(g.time_of_day)) as median_timeofday
	into #median_timeofday_startday
	FROM
	   (SELECT rank, a.time_of_day 
		FROM #firstday_timerank a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #firstday_timerank ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #firstday_timerank ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------

----------------------------------------- Median time of day of *first touchpoint*-------------------------------------------------
	-- select * from #first_day_activities order by Panelist_Key, start_date;
	-- select * from #first_touchpoint_timerank order by rank;
	-- drop table #first_touchpoint_timerank;
	select ROW_NUMBER() OVER(ORDER BY cast(a.start_time as time) ASC) AS Rank, a.Panelist_Key, a.start_time, cast(a.start_time as time) as time_of_day
	into #first_touchpoint_timerank
	from #first_touchpoint_activities a
	order by start_time;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_timeofday_firsttouchpoint_startday;
	-- drop table #median_timeofday_firsttouchpoint_startday;
	SELECT
	   dateadd(ms, datediff(ms, min(g.time_of_day), max(g.time_of_day))/2, min(g.time_of_day)) as median_timeofday
	into #median_timeofday_firsttouchpoint_startday
	FROM
	   (SELECT rank, a.time_of_day 
		FROM #first_touchpoint_timerank a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #first_touchpoint_timerank ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #first_touchpoint_timerank ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------

----------------------------------------- Median *date* of the first day -------------------------------------------------
	-- select * from #first_day_activities order by Panelist_Key, start_date;
	-- select * from #firstday_daterank order by rank;
	-- drop table #firstday_daterank;
	select ROW_NUMBER() OVER(ORDER BY start_date ASC) AS Rank, Panelist_Key, start_date as first_day
	into #firstday_daterank
	from #first_touchpoint_activities a
	order by first_day;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_date_startday;
	-- drop table #median_date_startday;
	SELECT
	   cast(dateadd(ms, datediff(ms, min(g.first_day), max(g.first_day))/2, min(g.first_day)) as date) as median_start_date
	into #median_date_startday
	FROM
	   (SELECT rank, a.first_day
		FROM #firstday_daterank a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #firstday_daterank ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #firstday_daterank ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------
	-- update values to journey arc final table
	-- select * from #journey_arc_final;
	update #journey_arc_final set
	most_common_first_touchpoint = (select top(1) cluster from #top_firsttouchpoint_firstday order by cluster_counts desc),
	most_common_touchpoint_beforebuy = 'N/A',
	most_common_first_domain = (select top(1) domain_or_app from #top_firstdomain_firstday order by domain_counts desc),
	most_engaged_day_ratio = (select mostengaged_firstday_ratio from #most_engagedday_ratio_startday),
	weekend_ratio = (select firstday_weekend_percentage from #weekend_ratio_starday),
	median_timeofday_all = (select median_timeofday from #median_timeofday_startday),
	median_timeofday_first_touchpoint = (select median_timeofday from #median_timeofday_firsttouchpoint_startday),
	median_date = (select median_start_date from #median_date_startday),
	total_engagement_mins = (select total_engagement_mins from #first_day_engagement)
	where journey_stage = 'Start Day';

-----------------------------------------------------*END*---------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------Last Day--------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------
-- prep: last day activities
	-- select * from #last_day_activities order by panelist_key, start_time;
	-- drop table #last_day_activities;
	select a.*
	into #last_day_activities
	from #journey_arc a
	inner join (select Panelist_Key, max(start_date) as last_day from #journey_arc group by Panelist_Key) b
	on a.Panelist_Key = b.Panelist_Key and a.start_date = b.last_day
	order by a.Panelist_Key, a.start_time;

	-- first touchpoint activities
	-- select * from #first_touchpoint_activities_lastday order by panelist_key, start_time;
	-- drop table #first_touchpoint_activities_lastday;
	select a.*
	into #first_touchpoint_activities_lastday
	from #last_day_activities a
	inner join (select Panelist_Key, min(start_time) as first_touchpoint from #last_day_activities group by Panelist_Key) b
	on a.Panelist_Key = b.Panelist_Key and a.start_time = b.first_touchpoint
	order by a.Panelist_Key, a.start_time;

------------------------------------------- total engagement (secs)-----------------------------------------------------
	-- select * from #last_day_engagement;
	-- drop table #last_day_engagement;
	select sum(active_seconds)/60 as total_engagement_mins
	into #last_day_engagement
	from #last_day_activities;
--------------------------------------------Most common 1st touchpoint (exclude end)-----------------------------------------------------
	-- select * from #firsttouchpoint_lastday order by cluster_counts desc;
	-- drop table #firsttouchpoint_lastday;
	select 'End Day' as journey_stage, cluster, count(*) as cluster_counts
	into #firsttouchpoint_lastday
	from #first_touchpoint_activities_lastday
	where cluster <> '[End]'
	group by cluster
	order by cluster_counts desc;


--------------------------------------------Most common 1st domain---------------------------------------------------------
	-- select * from #firstdomain_lastday order by domain_counts desc;
	-- drop table #firstdomain_lastday;
	select 'End Day' as journey_stage, domain_or_app, count(*) as domain_counts
	into #firstdomain_lastday
	from #first_touchpoint_activities_lastday
	where cluster <> '[End]'
	group by domain_or_app
	order by domain_counts desc;


----------------------- Identify the most engaged day and calculate % of the most engaged last day.------------------------
	-- identify last days
	-- select * from #daily_engagement_flag order by panelist_key, start_date;
	-- select * from #lastday_mostengaged_check order by panelist_key;
	-- drop table #lastday_mostengaged_check;
	select a.Panelist_Key, a.last_day, b.total_seconds, b.max_engagement_flag
	into #lastday_mostengaged_check
	from (select Panelist_Key, max(start_date) as last_day from #daily_engagement_flag group by Panelist_Key) as a 
	inner join #daily_engagement_flag as b 
	on a.Panelist_Key = b.Panelist_Key and a.last_day = b.start_date;

	-- Calculate % of lastdays that are most engaged 
    ------------------------------------------------------------------------------------------------------------
	-- select * from #most_engagedday_ratio_lastday;
	-- drop table #most_engagedday_ratio_lastday;
	select sum(max_engagement_flag) as total_mostengaged_lastday, count(max_engagement_flag) as count_all_lastdays, 
	CAST(sum(max_engagement_flag) as float)/cast(count(max_engagement_flag) as float) as mostengaged_lastday_ratio
	into #most_engagedday_ratio_lastday
	from #lastday_mostengaged_check;
	------------------------------------------------------------------------------------------------------------


-------------------------------------- % last day is on weekends-----------------------------------------------
	-- select * from #weekend_ratio_lastday;
	-- drop table #weekend_ratio_lastday;
	select
	(select count(*) from #first_touchpoint_activities_lastday where dow in (1,7)) as weekend_count,
	count(*) as total_count,
	CAST((select count(*) from #first_touchpoint_activities_lastday where dow in (1,7)) as float)/cast(count(*) as float) as lastday_weekend_percentage
	into #weekend_ratio_lastday
	from #first_touchpoint_activities_lastday; 


----------------------------------------- Median time of day for *all last day touchpoints* -------------------------------------------------
	-- select * from #last_day_activities order by Panelist_Key, start_date;
	-- select * from #lastday_timerank order by rank;
	-- drop table #lastday_timerank;
	select ROW_NUMBER() OVER(ORDER BY cast(start_time as time) ASC) AS Rank, Panelist_Key, start_time, cast(start_time as time) as time_of_day
	into #lastday_timerank
	from #last_day_activities
	order by time_of_day;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_timeofday_lastday;
	-- drop table #median_timeofday_lastday;
	SELECT
	   dateadd(ms, datediff(ms, min(g.time_of_day), max(g.time_of_day))/2, min(g.time_of_day)) as median_timeofday
	into #median_timeofday_lastday
	FROM
	   (SELECT rank, a.time_of_day 
		FROM #lastday_timerank a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #lastday_timerank ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #lastday_timerank ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------

----------------------------------------- Median time of day of *first touchpoint*-------------------------------------------------
	-- select * from #first_touchpoint_activities_lastday order by Panelist_Key, start_date;
	-- select * from #first_touchpoint_timerank_lastday order by rank;
	-- drop table #first_touchpoint_timerank_lastday;
	select ROW_NUMBER() OVER(ORDER BY cast(a.start_time as time) ASC) AS Rank, a.Panelist_Key, a.start_time, cast(a.start_time as time) as time_of_day
	into #first_touchpoint_timerank_lastday
	from #first_touchpoint_activities_lastday a
	order by start_time;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from  #median_timeofday_firsttouchpoint_lastday;
	-- drop table #median_timeofday_firsttouchpoint_lastday;
	SELECT
	   dateadd(ms, datediff(ms, min(g.time_of_day), max(g.time_of_day))/2, min(g.time_of_day)) as median_timeofday
	into #median_timeofday_firsttouchpoint_lastday
	FROM
	   (SELECT rank, a.time_of_day 
		FROM #first_touchpoint_timerank_lastday a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #first_touchpoint_timerank_lastday ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #first_touchpoint_timerank_lastday ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------

----------------------------------------- Median *date* of the last day -------------------------------------------------
	-- select * from #lastday_daterank order by Panelist_Key, last_day;
	-- select * from #lastday_daterank order by rank;
	-- drop table #lastday_daterank;
	select ROW_NUMBER() OVER(ORDER BY start_date ASC) AS Rank, Panelist_Key, start_date as last_day
	into #lastday_daterank
	from #first_touchpoint_activities_lastday a
	order by last_day;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_date_lastday;
	-- drop table #median_date_lastday;
	SELECT
	   cast(dateadd(ms, datediff(ms, min(g.last_day), max(g.last_day))/2, min(g.last_day)) as date) as median_last_date
	into #median_date_lastday
	FROM
	   (SELECT rank, a.last_day
		FROM #lastday_daterank a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #lastday_daterank ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #lastday_daterank ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------
	-- update values to journey arc final table
	-- select * from #journey_arc_final;
	update #journey_arc_final set
	most_common_first_touchpoint = (select top(1) cluster from #firsttouchpoint_lastday order by cluster_counts desc),
	most_common_touchpoint_beforebuy = 'N/A',
	most_common_first_domain = (select top(1) domain_or_app from #firstdomain_lastday order by domain_counts desc),
	most_engaged_day_ratio = (select mostengaged_lastday_ratio from #most_engagedday_ratio_lastday),
	weekend_ratio = (select lastday_weekend_percentage from #weekend_ratio_lastday),
	median_timeofday_all = (select median_timeofday from #median_timeofday_lastday),
	median_timeofday_first_touchpoint = (select median_timeofday from #median_timeofday_firsttouchpoint_lastday),
	median_date = (select median_last_date from #median_date_lastday),
	total_engagement_mins = (select total_engagement_mins from #last_day_engagement)
	where journey_stage = 'End Day';

-----------------------------------------------------*END*---------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------- Day of Buy------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------
-- prep: day of buy activities
	-- select * from #buy_day_activities order by panelist_key, start_time;
	-- drop table #buy_day_activities;
	select a.*
	into #buy_day_activities
	from #journey_arc a
	inner join #journey_arc b
	on a.Panelist_Key = b.Panelist_Key and a.start_date = b.start_date and b.cluster = '[Buy]'
	order by a.Panelist_Key, a.start_time;

	-- first touchpoint activities
	-- select * from #first_touchpoint_activities_buy order by panelist_key, start_time;
	-- drop table #first_touchpoint_activities_buy;
	select a.*
	into #first_touchpoint_activities_buy
	from #buy_day_activities a
	inner join (select Panelist_Key, min(start_time) as first_touchpoint from #buy_day_activities group by Panelist_Key) b
	on a.Panelist_Key = b.Panelist_Key and a.start_time = b.first_touchpoint
	order by a.Panelist_Key, a.start_time;

	------------------------------------------- total engagement (secs)-----------------------------------------------------
	-- select * from #buy_day_engagement;
	-- drop table #buy_day_engagement;
	select sum(active_seconds)/60 as total_engagement_mins
	into #buy_day_engagement
	from #buy_day_activities;

	--------------------------------------------Most common 1st touchpoint (Exclude buy)-----------------------------------------------------
	-- select * from #firsttouchpoint_buy order by cluster_counts desc;
	-- drop table #firsttouchpoint_buy;
	select 'Day of Buy' as journey_stage, cluster, count(*) as cluster_counts
	into #firsttouchpoint_buy
	from #first_touchpoint_activities_buy
	where cluster <> '[Buy]'
	group by cluster
	order by cluster_counts desc;

	--------------------------------------------Most common touchpoint *before buy* -----------------------------------------------------
	-- select * from #firsttouchpoint_before_buy order by cluster_counts desc;
	-- drop table #firsttouchpoint_before_buy;
	select 'Before Buy' as journey_stage, prev_cluster, count(*) as cluster_counts
	into #firsttouchpoint_before_buy
	from #first_touchpoint_activities_buy
	where cluster = '[Buy]'
	group by prev_cluster
	order by cluster_counts desc;


--------------------------------------------Most common 1st domain---------------------------------------------------------
	-- select * from #firstdomain_buy order by domain_counts desc;
	-- drop table #firstdomain_buy;
	select 'Day of Buy' as journey_stage, domain_or_app, count(*) as domain_counts
	into #firstdomain_buy
	from #first_touchpoint_activities_buy
	where cluster <> '[Buy]'
	group by domain_or_app
	order by domain_counts desc;


----------------------- Identify the most engaged day and calculate % of the most engaged buy day.------------------------
	-- identify buy days
	-- select * from #buy_day_activities order by panelist_key, start_time;
	-- select * from #daily_engagement_flag order by panelist_key, start_date;
	-- select * from #buyday_mostengaged_check order by panelist_key;
	-- drop table #buyday_mostengaged_check;
	select a.Panelist_Key, a.buy_day, b.total_seconds, b.max_engagement_flag
	into #buyday_mostengaged_check
	from (select Panelist_Key, start_date as buy_day from #buy_day_activities where cluster = '[Buy]') as a 
	inner join #daily_engagement_flag as b 
	on a.Panelist_Key = b.Panelist_Key and a.buy_day = b.start_date;

	-- Calculate % of buy days that are most engaged 
    ------------------------------------------------------------------------------------------------------------
	-- select * from #most_engagedday_ratio_buyday;
	-- drop table #most_engagedday_ratio_buyday;
	select sum(max_engagement_flag) as total_mostengaged_buyday, count(max_engagement_flag) as count_all_buyday, 
	CAST(sum(max_engagement_flag) as float)/cast(count(max_engagement_flag) as float) as mostengaged_buyday_ratio
	into #most_engagedday_ratio_buyday
	from #buyday_mostengaged_check;
	------------------------------------------------------------------------------------------------------------


-------------------------------------- % buy day is on weekends-----------------------------------------------
	-- select * from #weekend_ratio_buyday;
	-- drop table #weekend_ratio_buyday;
	select
	(select count(*) from #first_touchpoint_activities_buy where dow in (1,7)) as weekend_count,
	count(*) as total_count,
	CAST((select count(*) from #first_touchpoint_activities_buy where dow in (1,7)) as float)/cast(count(*) as float) as buyday_weekend_percentage
	into #weekend_ratio_buyday
	from #first_touchpoint_activities_buy; 


----------------------------------------- Median time of day for *all last day touchpoints* -------------------------------------------------
	-- select * from #buy_day_activities order by Panelist_Key, start_date;
	-- select * from #buyday_timerank order by rank;
	-- drop table #buyday_timerank;
	select ROW_NUMBER() OVER(ORDER BY cast(start_time as time) ASC) AS Rank, Panelist_Key, start_time, cast(start_time as time) as time_of_day
	into #buyday_timerank
	from #buy_day_activities
	order by time_of_day;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_timeofday_buyday;
	-- drop table #median_timeofday_buyday;
	SELECT
	   dateadd(ms, datediff(ms, min(g.time_of_day), max(g.time_of_day))/2, min(g.time_of_day)) as median_timeofday
	into #median_timeofday_buyday
	FROM
	   (SELECT rank, a.time_of_day 
		FROM #buyday_timerank a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #buyday_timerank ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #buyday_timerank ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------

----------------------------------------- Median time of day of *first touchpoint*-------------------------------------------------
	-- select * from #first_touchpoint_activities_buy order by Panelist_Key, start_date;
	-- select * from #first_touchpoint_timerank_buyday order by rank;
	-- drop table #first_touchpoint_timerank_buyday;
	select ROW_NUMBER() OVER(ORDER BY cast(a.start_time as time) ASC) AS Rank, a.Panelist_Key, a.start_time, cast(a.start_time as time) as time_of_day
	into #first_touchpoint_timerank_buyday
	from #first_touchpoint_activities_buy a
	order by start_time;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_timeofday_firsttouchpoint_buy;
	-- drop table #median_timeofday_firsttouchpoint_buy;
	SELECT
	   dateadd(ms, datediff(ms, min(g.time_of_day), max(g.time_of_day))/2, min(g.time_of_day)) as median_timeofday
	into #median_timeofday_firsttouchpoint_buy
	FROM
	   (SELECT rank, a.time_of_day 
		FROM #first_touchpoint_timerank_buyday a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #first_touchpoint_timerank_buyday ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #first_touchpoint_timerank_buyday ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------

----------------------------------------- Median *date* of the buy day -------------------------------------------------
	-- select * from #first_touchpoint_activities_buy order by Panelist_Key, last_day;
	-- select * from #buyday_daterank order by rank;
	-- drop table #buyday_daterank;
	select ROW_NUMBER() OVER(ORDER BY start_date ASC) AS Rank, Panelist_Key, start_date as buy_day
	into #buyday_daterank
	from #first_touchpoint_activities_buy a
	order by buy_day;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_date_buyday;
	-- drop table #median_date_buyday;
	SELECT
	   cast(dateadd(ms, datediff(ms, min(g.buy_day), max(g.buy_day))/2, min(g.buy_day)) as date) as median_buy_date
	into #median_date_buyday
	FROM
	   (SELECT rank, a.buy_day
		FROM #buyday_daterank a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #buyday_daterank ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #buyday_daterank ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------
	-- update values to journey arc final table
	-- select * from #journey_arc_final;
	update #journey_arc_final set
	most_common_first_touchpoint = (select top(1) cluster from #firsttouchpoint_buy order by cluster_counts desc),
	most_common_touchpoint_beforebuy = (select top(1) prev_cluster from #firsttouchpoint_before_buy order by cluster_counts desc),
	most_common_first_domain = (select top(1) domain_or_app from #firstdomain_buy order by domain_counts desc),
	most_engaged_day_ratio = (select mostengaged_buyday_ratio from #most_engagedday_ratio_buyday),
	weekend_ratio = (select buyday_weekend_percentage from #weekend_ratio_buyday),
	median_timeofday_all = (select median_timeofday from #median_timeofday_buyday),
	median_timeofday_first_touchpoint = (select median_timeofday from #median_timeofday_firsttouchpoint_buy),
	median_date = (select median_buy_date from #median_date_buyday),
	total_engagement_mins = (select total_engagement_mins from #buy_day_engagement)
	where journey_stage = 'Day of Buy';

-----------------------------------------------------*END*---------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------- Most Engaged Day-------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------
-- prep: most engaged day activities
	-- select * from #daily_engagement_flag order by Panelist_Key, start_date;
	-- select * from #most_engaged_day_activities order by panelist_key, start_time;
	-- drop table #most_engaged_day_activities;
	select a.*
	into #most_engaged_day_activities
	from #journey_arc a
	inner join #daily_engagement_flag b
	on a.Panelist_Key = b.Panelist_Key and a.start_date = b.start_date and b.max_engagement_flag = 1
	order by a.Panelist_Key, a.start_time;

	-- first touchpoint activities
	-- select * from #first_touchpoint_activities_most_engaged_day order by panelist_key, start_time;
	-- drop table #first_touchpoint_activities_most_engaged_day
	select a.*
	into #first_touchpoint_activities_most_engaged_day
	from #most_engaged_day_activities a
	inner join (select Panelist_Key, min(start_time) as first_touchpoint from #most_engaged_day_activities group by Panelist_Key) b
	on a.Panelist_Key = b.Panelist_Key and a.start_time = b.first_touchpoint
	order by a.Panelist_Key, a.start_time;
	------------------------------------------- total engagement (secs)-----------------------------------------------------
	-- select * from #most_engaged_day_engagement;
	-- drop table #most_engaged_day_engagement;
	select sum(active_seconds)/60 as total_engagement_mins
	into #most_engaged_day_engagement
	from #most_engaged_day_activities;

	--------------------------------------------Most common 1st touchpoint (exclude end)-----------------------------------------------------
	-- select * from #firsttouchpoint_most_engaged_day order by cluster_counts desc;
	-- drop table #firsttouchpoint_most_engaged_day;
	select 'Most Engaged Day' as journey_stage, cluster, count(*) as cluster_counts
	into #firsttouchpoint_most_engaged_day
	from #first_touchpoint_activities_most_engaged_day
	--where cluster <> '[End]'
	group by cluster
	order by cluster_counts desc;


--------------------------------------------Most common 1st domain---------------------------------------------------------
	-- select * from #firstdomain_most_engaged_day order by domain_counts desc;
	-- drop table #firstdomain_most_engaged_day;
	select 'Most Engaged Day' as journey_stage, domain_or_app, count(*) as domain_counts
	into #firstdomain_most_engaged_day
	from #first_touchpoint_activities_most_engaged_day
	--where cluster <> '[End]'
	group by domain_or_app
	order by domain_counts desc;


-------------------------------------- % Most engaged day is on weekends-----------------------------------------------
	-- select * from #weekend_ratio_most_engaged_day;
	-- drop table #weekend_ratio_most_engaged_day;
	select
	(select count(*) from #first_touchpoint_activities_most_engaged_day where dow in (1,7)) as weekend_count,
	count(*) as total_count,
	CAST((select count(*) from #first_touchpoint_activities_most_engaged_day where dow in (1,7)) as float)/cast(count(*) as float) as most_engaged_day_weekend_percentage
	into #weekend_ratio_most_engaged_day
	from #first_touchpoint_activities_most_engaged_day; 


----------------------------------------- Median time of day for *all last day touchpoints* -------------------------------------------------
	-- select * from #most_engaged_day_activities order by Panelist_Key, start_date;
	-- select * from #most_engaged_day_timerank order by rank;
	-- drop table #most_engaged_day_timerank;
	select ROW_NUMBER() OVER(ORDER BY cast(start_time as time) ASC) AS Rank, Panelist_Key, start_time, cast(start_time as time) as time_of_day
	into #most_engaged_day_timerank
	from #most_engaged_day_activities
	order by time_of_day;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_timeofday_most_engaged_day;
	-- drop table #median_timeofday_most_engaged_day;
	SELECT
	   dateadd(ms, datediff(ms, min(g.time_of_day), max(g.time_of_day))/2, min(g.time_of_day)) as median_timeofday
	into #median_timeofday_most_engaged_day
	FROM
	   (SELECT rank, a.time_of_day 
		FROM #most_engaged_day_timerank a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #most_engaged_day_timerank ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #most_engaged_day_timerank ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------

----------------------------------------- Median time of day of *first touchpoint*-------------------------------------------------
	-- select * from #first_touchpoint_activities_most_engaged_day order by Panelist_Key, start_date;
	-- select * from #first_touchpoint_timerank_most_engaged_day order by rank;
	-- drop table #first_touchpoint_timerank_most_engaged_day;
	select ROW_NUMBER() OVER(ORDER BY cast(a.start_time as time) ASC) AS Rank, a.Panelist_Key, a.start_time, cast(a.start_time as time) as time_of_day
	into #first_touchpoint_timerank_most_engaged_day
	from #first_touchpoint_activities_most_engaged_day a
	order by start_time;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from  #median_timeofday_firsttouchpoint_most_engaged_day;
	-- drop table #median_timeofday_firsttouchpoint_most_engaged_day;
	SELECT
	   dateadd(ms, datediff(ms, min(g.time_of_day), max(g.time_of_day))/2, min(g.time_of_day)) as median_timeofday
	into #median_timeofday_firsttouchpoint_most_engaged_day
	FROM
	   (SELECT rank, a.time_of_day 
		FROM #first_touchpoint_timerank_most_engaged_day a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #first_touchpoint_timerank_most_engaged_day ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #first_touchpoint_timerank_most_engaged_day ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------

----------------------------------------- Median *date* of the most_engaged_day -------------------------------------------------
	-- select * from #most_engaged_day_daterank order by Panelist_Key, last_day;
	-- select * from #most_engaged_day_daterank order by rank;
	-- drop table #most_engaged_day_daterank;
	select ROW_NUMBER() OVER(ORDER BY start_date ASC) AS Rank, Panelist_Key, start_date as most_engaged_day
	into #most_engaged_day_daterank
	from #first_touchpoint_activities_most_engaged_day a
	order by most_engaged_day;

	-- calculate median time of day
	---------------------------------------------------------------------------------------------------------------------
	-- select * from #median_date_most_engaged_day;
	-- drop table #median_date_most_engaged_day;
	SELECT
	   cast(dateadd(ms, datediff(ms, min(g.most_engaged_day), max(g.most_engaged_day))/2, min(g.most_engaged_day)) as date) as median_most_engaged_day
	into #median_date_most_engaged_day
	FROM
	   (SELECT rank, a.most_engaged_day
		FROM #most_engaged_day_daterank a) AS g
	WHERE
	g.rank IN (
	(SELECT MAX(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #most_engaged_day_daterank ORDER BY Rank) AS BottomHalf), 
    (SELECT MIN(Rank) FROM (SELECT TOP 50 PERCENT Rank FROM #most_engaged_day_daterank ORDER BY Rank DESC) AS TopHalf)
	);
	---------------------------------------------------------------------------------------------------------------------
	-- update values to journey arc final table
	-- select * from #journey_arc_final;
	update #journey_arc_final set
	most_common_first_touchpoint = (select top(1) cluster from #firsttouchpoint_most_engaged_day order by cluster_counts desc),
	most_common_touchpoint_beforebuy = 'N/A',
	most_common_first_domain = (select top(1) domain_or_app from #firstdomain_most_engaged_day order by domain_counts desc),
	most_engaged_day_ratio = 1,
	weekend_ratio = (select most_engaged_day_weekend_percentage from #weekend_ratio_most_engaged_day),
	median_timeofday_all = (select median_timeofday from #median_timeofday_most_engaged_day),
	median_timeofday_first_touchpoint = (select median_timeofday from #median_timeofday_firsttouchpoint_most_engaged_day),
	median_date = (select median_most_engaged_day from #median_date_most_engaged_day),
	total_engagement_mins = (select total_engagement_mins from #most_engaged_day_engagement)
	where journey_stage = 'Most Engaged Day';
-----------------------------------------------------*END*---------------------------------------------------------------

--*****************************************************************************************************************
----------------------------------------------Journey Arc Final Output---------------------------------------------
--*****************************************************************************************************************
-- 1. Journey Arc
-- select * from #journey_arc_final order by median_date;

-- 2. Most common 1st touchpoint
/**
select * from #top_firsttouchpoint_firstday
union
select * from #firsttouchpoint_buy
union
select * from #firsttouchpoint_before_buy
union
select * from #firsttouchpoint_most_engaged_day
union
select * from #firsttouchpoint_lastday
order by journey_stage, cluster_counts desc;
**/

-- 3. Most common 1st domain
/**
select * from #top_firstdomain_firstday
union
select * from #firstdomain_buy
union
select * from #firstdomain_most_engaged_day
union
select * from #firsttouchpoint_lastday
order by journey_stage, domain_counts desc;
**/

--END OF FILE--