-------------------------------------------------
-- Shirley
-- 12/20/2019
-- Script for core taxonomy property update prep
-------------------------------------------------

use [PROJECTNAME];
-----------------------------------------------------------------------------------
-- 1. get list of all [PROJECTNAME] properties from [[PROJECTNAME]].[Rps].[sequence_event]:
-----------------------------------------------------------------------------------
-- select top 100 * from [PROJECTNAME].[Rps].[sequence_event];
-- select digital_type_id, count(distinct property_key) from [[PROJECTNAME]].[Run].[UserByDay_Total] group by digital_type_id order by count(distinct property_key); -- 32731
-- select digital_type_id, count(distinct notes) from [[PROJECTNAME]].[Rps].[sequence_event] group by digital_type_id order by count(distinct notes); 
-- select digital_type, count(distinct notes) from [[PROJECTNAME]].[Run].[v_Report_Sequence_Event] group by digital_type order by count(distinct notes); 
-- select top 100 * from #digital_property_final order by num_panelists desc;
/**
begin try drop table #digital_property_final end try begin catch end catch;

select digital_type_id, Notes, 
sum(dur_seconds)/60 as total_mins, count(distinct panelist_key) as num_panelists, min(date_id) as date_created
into #digital_property_final
from [PROJECTNAME].[Rps].[sequence_event]
where (digital_type_id = 235 or digital_type_id = 100)
group by digital_type_id, Notes; 
--(140171 row(s) affected)
**/
-- domain
begin try drop table #digital_property_web end try begin catch end catch;

select digital_type_id, Domain_Name_Key as Notes, 
sum(dur_minutes) as total_mins, count(distinct panelist_key) as num_panelists, min(date_id) as date_created
into #digital_property_web
from [PROJECTNAME].[Run].[Web_Events]
group by digital_type_id, Domain_Name_Key; 
--(120235 row(s) affected)

-- app
begin try drop table #digital_property_app end try begin catch end catch;

select digital_type_id, App_Name_Key as Notes, 
sum(dur_minutes) as total_mins, count(distinct panelist_key) as num_panelists, min(date_id) as date_created
into #digital_property_app
from [PROJECTNAME].[Run].[App_Events]
group by digital_type_id, App_Name_Key; 
--(20166 row(s) affected)

-- Combine domain and app
-- select top 100 * from #digital_property_final order by num_panelists desc;
begin try drop table #digital_property_final end try begin catch end catch;

(select a.* into #digital_property_final
from #digital_property_web a)
union
(select b.* from #digital_property_app b);
--(140401 row(s) affected)

-- select top 100 * from #property_name where property_name = 'digi.me';
-- select digital_type_id, count(distinct property_name) as count from #property_name group by digital_type_id;
begin try drop table #property_name end try begin catch end catch;

select a.*,
(case when a.Digital_Type_ID = 100 then b.App_Name 
when a.Digital_Type_ID = 235 then c.domain_name end) as property_name
into #property_name
from #digital_property_final a 
left outer join [PROJECTNAME].ref.App_Name b 
on a.notes = b.App_Name_Key and a.Digital_Type_ID = 100
left outer join [PROJECTNAME].ref.Domain_Name c
on a.notes = c.Domain_Name_Key and a.Digital_Type_ID = 235
where notes is not null;
--(140401 row(s) affected)


-- select top 1000 * from #taxonomy_properties order by date_created;
-- select digital_type_id, count(distinct property_name) from #taxonomy_properties group by digital_type_id
begin try drop table #taxonomy_properties end try begin catch end catch;

select a.digital_type_id, property_name, 
case when a.digital_type_id = 235 then e.category when a.digital_type_id = 100 then f.category end as category, 
case when a.digital_type_id = 235 then e.subcategory when a.digital_type_id = 100 then f.subcategory end as subcategory, 
total_mins, num_panelists, date_created
into #taxonomy_properties
from #property_name a
left outer join [PROJECTNAME].[Ref].[v_Domain_Categorization_Taxonomy] e
	on a.Notes = e.domain_name_key
	and a.digital_type_id = 235
left outer join [PROJECTNAME].[Ref].[v_App_Categorization_Taxonomy] f
	on a.Notes = f.App_Name_Key
	and a.digital_type_id = 100;
-- (140401 row(s) affected)

--############################################
-- PERMANENT TABLE CREATED HERE:
-- Final table ref.taxonomy_properties:
--############################################
-- select count(distinct panelist_key) from [[PROJECTNAME]].[Rps].[sequence_event]; 
-- select digital_type_id, count(distinct property_name) from ref.taxonomy_properties group by digital_type_id;
-- select top 100 * from ref.taxonomy_properties order by PROJECTNAME_m_reach;
begin try drop table ref.taxonomy_properties end try begin catch end catch;
select *, 
((1.0*num_panelists)/(1.0*(select count(distinct panelist_key) from [PROJECTNAME].[Rps].[sequence_event]))) as PROJECTNAME_m_reach, 
core.dbo.text_hash(property_name) as property_hash
into ref.taxonomy_properties
from #taxonomy_properties;
--(140171 row(s) affected)
-- (140401 row(s) affected)

--------------------------------------------------------------------------------------------------------------
-- 2. get list of all new properties from [PROJECTNAME] that not yet exist in core.core.ref.Taxonomy_common_2020:
-- copy this part to c1 code to update new properties
--------------------------------------------------------------------------------------------------------------
-- select * from ref.taxonomy_properties -- 39424
-- select count(distinct panelist_key) from [PROJECTNAME].[Rps].[sequence_event]; --3123
-- select * from core.ref.Taxonomy_common_2020 where project_sample_source = '2019 GooglePoland';
-- select top 100 * from core.ref.Taxonomy_Common_2020 where sw_subcategory = 'visual arts and design' order by project_m_reach desc
--insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '' as project_sample_source,
	'' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    PROJECTNAME_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '' as [date_created],
	--a.date_created,
	GETDATE() as[date_updated], 'c1. new project properties' as step_assigned
from [PROJECTNAME].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,PROJECTNAME_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 67775


/**
Select *
from core.ref.Taxonomy_Common_2020 
where project_sample_source = '2020 ABI' and date_created = '2020-04-13' and vetted_flag = 0
order by project_m_reach desc
; 
**/
-- END OF CORE TABLE UPDATES -- 

-----------------------------------------------------------------------------------------------------
-- QA & Classification Update
-- all new properties from current project 
-----------------------------------------------------------------------------------------------------
-- select top 100 * from core.ref.Taxonomy_common_2020
-- select * from core.ref.Taxonomy_common_2020 where common_supercategory='shopping' and common_vertical = 'Travel'
-- select top 100 * from core.ref.Taxonomy_common_2020 where project_sample_source = '2019 GooglePoland'
select 
--	a.property_hash,
	a.property_name,
	a.digital_type_id,
	b.PROJECTNAME_m_reach as project_reach,
    project_m_reach as core_reach,
--	sw_category,
--  sw_subcategory,
	'' as bdg_color,
	'' as domain_category,
	'' as domain_subcategory,
	'' as lean_score,
	'' as local_flag,
	'' as news_org,
    common_supercategory,
    common_category,
    common_subcategory,
	common_vertical,
    rps_flag,
    rpt_flag,
	vetted_flag,
--	project_sample_size as DB_sample_size,
	b.num_panelists as KP_sample_size
--	case when c.property_name is not null then 1 else 0 end as pattern_match
from core.ref.Taxonomy_common_2020 a
inner join [PROJECTNAME].ref.taxonomy_properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where 
--b.[PROJECTNAME]_m_reach >= 0.03 -- kp high reach
(a.project_m_reach >= 0.03 and b.PROJECTNAME_m_reach < 0.03) -- core high reach other than kp
and (common_subcategory like '%TBD%' or common_supercategory = '[TBD]' or vetted_flag = 0)
order by PROJECTNAME_m_reach desc;



----------------------------------------------------------------------
--3. update reach of existing properties using [PROJECTNAME] numbers:
----------------------------------------------------------------------
-- run this part as part after project end
/**
update core.ref.Taxonomy_Common_2019 
set project_country = b.country,
	project_sample_source = '2019 GooglePoland',
	property_sample_size = b.property_sample_size,
	project_sample_size = b.project_sample_size,
    project_m_reach =  b.project_m_reach,
	step_assigned = 'b1. new sample size/reach'
from core.ref.Taxonomy_Common_2019 a 
inner join (select property, country, case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id, 
	max(du) as property_sample_size, max(dp) as project_sample_size, max((1.0*mu_usemonths)/(1.0*mp_qualmonths)) as project_m_reach 
	from [[PROJECTNAME]].[rpt].[Standard_Metrics_Property] 
	where mp_qualmonths > 0 and property not like '%*%' -- removes '* Total Internet *' 
	and device_type = 'All Devices' and digital_type in ('Web','App') and du>1 group by property, country, digital_type) b
on a.property_name=b.property and a.digital_type_id = b.digital_type_id
where a.project_sample_source = '2019 GooglePoland' or (a.project_country = b.country and a.project_sample_size <= b.project_sample_size) 
;
**/

