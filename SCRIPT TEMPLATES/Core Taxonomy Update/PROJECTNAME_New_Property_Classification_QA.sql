-- Name
-- Date
-- New Project Property Classification QA
---------------------------------------------------------------------------------------------------

USE [PROEJCTNAME];  --replace this and every instance of PROEJCTNAME in the script with your project schema (CTRL+F for ABI2020 and replace)

-- WHAT IS THIS REPORT
------------------------------------------------------------------
-- Modified from the original "Share of digital" script for core new property QA


-- COMBINE COMMON TAXONOMY AND ECOSYSTEM INTO INTEGRATED SEQUENCE
------------------------------------------------------------------
-- select top 100 * from rps.v_Report_Sequence_Event where platform is null;
-- select count(distinct Panelist_ID) from rps.v_Report_Sequence_Event;
-- select distinct category from rps.v_Report_Sequence_Event;
-- select top 1000 * from #sequence_integrated_tax where platform is null;
-- select top 1000 * from #sequence_integrated_tax where best_category is null;

begin try drop table #sequence_integrated_tax end try begin catch end catch;

select record_id, a.Panelist_ID as panelist_key, digital_type,
	case when digital_type = 'app' then 100 when digital_type = 'web' then 235 end as digital_type_id, 
	platform, notes as property, dur_seconds, session_id,
	cast('Common' as varchar(30)) as eco_type
into #sequence_integrated_tax
from rps.v_Report_Sequence_Event a
where a.notes is not null and (digital_type='APP' or digital_type = 'Web');
--(35648359 row(s) affected)

create index var1 on #sequence_integrated_tax(record_id);


-- AGGREGATE AT PERSON + PROPERTY LEVEL
------------------------------------------------------------------
-- select distinct best_category from #sequence_integrated_tax;
-- select top 1000 * from #share_session_agg where best_category is null;

begin try drop table #share_session_agg end try begin catch end catch;
select platform, digital_type, digital_type_id, panelist_key, eco_type, property, cast(sum(dur_seconds)/60.0 as numeric(10,2)) as dur_minutes
into #share_session_agg
from #sequence_integrated_tax
group by platform, digital_type, digital_type_id, panelist_key, eco_type, property;


-- AGGREGATE AT PROPERTY LEVEL
------------------------------------------------------------------
-- select top 100 * from #share_session_prop_v1 where property = 'Cashman Blast';
begin try drop table #share_session_prop_v1 end try begin catch end catch;
select platform, digital_type, digital_type_id, property, eco_type, count(distinct panelist_key) as total_panelists, 
	count(*) as total_visits, sum(dur_minutes) as dur_minutes
into #share_session_prop_v1
from #share_session_agg
group by platform, digital_type, digital_type_id, property, eco_type;
--(140955 row(s) affected)

insert into #share_session_prop_v1
select 'Total Platform' as platform, digital_type, digital_type_id, property, eco_type, count(distinct panelist_key) as total_panelists, 
	count(*) as total_visits, sum(dur_minutes) as dur_minutes
from #share_session_agg
group by digital_type, digital_type_id, property, eco_type;

-- Add Core Classification
------------------------------------------------------------------
-- select top 1000 * from core.ref.Taxonomy_Common_2021 order by project_m_reach desc;
-- select top 1000 * from #share_session_prop where best_category = 'utility/other'
-- select top 1000 * from #share_session_prop where property = 'Hidden Objects House Cleaning';
begin try drop table #share_session_prop end try begin catch end catch;

select a.*, b.common_supercategory, b.common_category, b.common_subcategory, b.common_vertical,
b.rps_flag, b.rpt_flag, b.vetted_flag
into #share_session_prop
from #share_session_prop_v1 a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property=b.property_name and a.digital_type_id = b.digital_type_id
where a.eco_type='Common';
-- 40140 rows affected

-- Unclassified best category (shouldn't have any since all properties are uploaded to core):
update #share_session_prop	set common_supercategory = '[TBD]', common_category = '[Alert: TBD]', common_subcategory = '[Alert: TBD]'
from #share_session_prop where (common_supercategory is null) and eco_type='Common';


--- FINAL AGG TO CATEGORY LEVEL
-- select * from #share_best_category order by 1,2,3;
begin try drop table #share_best_category end try begin catch end catch;

select platform, digital_type, digital_type_id, common_supercategory, 
sum(total_visits) as total_visits, cast(sum(dur_minutes) as numeric(30,5)) as dur_minutes
into #share_best_category
from #share_session_prop
group by platform, digital_type, digital_type_id, common_supercategory
order by 1,2,3;


/**
---- QA
select count(distinct a.panelist_key) 
from #share_session_agg a
inner join rps.User_Qualification b
on a.panelist_key = b.Panelist_Key and Daily_Qualified > 0;

-- QA largest contributors
select * 
from #share_session_prop
where dur_minutes>500 
order by platform, Digital_Type, dur_minutes desc;

-- QA check if any one panelist is creative ridiuculous skew
select top 100 * from #share_session_agg order by dur_minutes desc;


-- QA on alert/remove/tbd properties
select distinct common_supercategory from #share_session_prop;
Select * from #share_session_prop where common_supercategory = '[Alert]';
Select * from #share_session_prop where common_supercategory = '[Remove]';
Select * from #share_session_prop where common_supercategory = '[TBD]';
Select * from #share_session_prop where rpt_flag = 0;
**/

--########################################
-- QA TABLES OUTPUT FOR CORE UPDATE
--########################################
-- 1. CHECK TBD & NON-VETTED PROPERTIES
-- make sure unclassified portion is under 3% of total minutes or project expectations
-- join to the color tables: prioritize white/red properties 

-- Check current total TBD mins share:
select sum(a.dur_minutes)/(select sum(b.dur_minutes) from #share_session_prop b) 
from #share_session_prop a where a.common_supercategory like '%TBD%';

-- Copy to excel to see category breakdown
select * from #share_best_category;

-- App
-- select top 1000 * from #share_session_prop where property = 'Solitaire Classic';
-- TBD
select a.property, a.digital_type_id, 
a.total_panelists, a.dur_minutes, prop_color, '' as eco_category, '' as eco_subcategory,
a.common_supercategory, a.common_category, a.common_subcategory, a.common_vertical,
a.rps_flag, a.rpt_flag, a.vetted_flag,
a.dur_minutes/b.dur_minutes as cat_dur_min_share,
a.dur_minutes/(select sum(d.dur_minutes) from #share_session_prop d) as total_dur_min_share 
from #share_session_prop a
left join #share_best_category b
on a.Platform = b.Platform and a.Digital_Type = b.Digital_Type and a.common_supercategory = b.common_supercategory
left join [Ref].[app_color_ABInBev] c
on a.property = c.app_name 
where a.platform = 'Total Platform' and a.Digital_Type = 'app'
and a.common_supercategory like '%TBD%' 
and (total_panelists>=15 or a.dur_minutes/b.dur_minutes > 0.05)
order by total_panelists desc;

-- Non-vetted high reach
-- select top 100 * from [Ref].[app_color_ABInBev] where app_name = 'Photo Editor Pro'
select a.property, a.digital_type_id, 
a.total_panelists, a.dur_minutes, prop_color, '' as eco_category, '' as eco_subcategory,
a.common_supercategory, a.common_category, a.common_subcategory, a.common_vertical,
a.rps_flag, a.rpt_flag, a.vetted_flag,
a.dur_minutes/b.dur_minutes as cat_dur_min_share,
a.dur_minutes/(select sum(d.dur_minutes) from #share_session_prop d) as total_dur_min_share 
from #share_session_prop a
left join #share_best_category b
on a.Platform = b.Platform and a.Digital_Type = b.Digital_Type and a.common_supercategory = b.common_supercategory
left join [Ref].[app_color_ABInBev] c
on a.property = c.app_name 
where a.platform = 'Total Platform' and a.Digital_Type = 'app'
and a.common_supercategory not like '%Alert%' and a.common_supercategory not like '%TBD%' and a.common_supercategory not like '%Remove%' 
and a.vetted_flag = 0
and (total_panelists>=15 or a.dur_minutes/b.dur_minutes > 0.05)
--and (total_panelists < 15 and a.dur_minutes/b.dur_minutes > 0.1) -- check for outliers
order by total_panelists desc;

-- Web
-- select top 100 * from ref.eco_domains;
select a.property, a.digital_type_id, 
a.total_panelists, a.dur_minutes, domain_color as prop_color, '' as eco_category, '' as eco_subcategory,
a.common_supercategory, a.common_category, a.common_subcategory, a.common_vertical,
a.rps_flag, a.rpt_flag, a.vetted_flag,
a.dur_minutes/b.dur_minutes as cat_dur_min_share,
a.dur_minutes/(select sum(d.dur_minutes) from #share_session_prop d) as total_dur_min_share 
from #share_session_prop a
left join #share_best_category b
on a.Platform = b.Platform and a.Digital_Type = b.Digital_Type and a.common_supercategory = b.common_supercategory
left join [ABI2020].[Ref].[eco_domains] c
on a.property = c.domain_name
where a.platform = 'Total Platform' and a.Digital_Type = 'web'
and a.common_supercategory like '%TBD%' 
and (total_panelists>=15 or a.dur_minutes/b.dur_minutes > 0.05)
order by total_panelists desc;

-- Non-vetted high reach
select a.property, a.digital_type_id, 
a.total_panelists, a.dur_minutes, domain_color as prop_color, '' as eco_category, '' as eco_subcategory,
a.common_supercategory, a.common_category, a.common_subcategory, a.common_vertical,
a.rps_flag, a.rpt_flag, a.vetted_flag,
a.dur_minutes/b.dur_minutes as cat_dur_min_share,
a.dur_minutes/(select sum(d.dur_minutes) from #share_session_prop d) as total_dur_min_share 
from #share_session_prop a
left join #share_best_category b
on a.Platform = b.Platform and a.Digital_Type = b.Digital_Type and a.common_supercategory = b.common_supercategory
left join [ABI2020].[Ref].[eco_domains] c
on a.property = c.domain_name
where a.platform = 'Total Platform' and a.Digital_Type = 'web'
and a.common_supercategory not like '%Alert%' and a.common_supercategory not like '%TBD%' and a.common_supercategory not like '%Remove%' 
and a.vetted_flag = 0
and (total_panelists>=15 or a.dur_minutes/b.dur_minutes > 0.05)
--and (total_panelists < 15 and a.dur_minutes/b.dur_minutes > 0.1) -- check for outliers
order by total_panelists desc;



-- END OF FILE --