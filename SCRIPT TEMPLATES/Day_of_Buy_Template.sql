-- Days Before Buy TEMPLATE
-- OWNER
-- DATE



use PROJECTNAME;

-- Make temp tables to limit to each purchase segment
-- drop table #totalsurvey_panelists;
-- select top 100 * from #panelist_list order by panelist_key;
begin try drop table #panelist_list end try begin catch end catch;
select panelist_key, segment_name
into #panelist_list
from ref.Panelist_Segmentation
where segment_name in (
'Total Alcohol Buyers'
,'Beer'
,'Wine'
,'Spirits'
,'Hard Seltzer'
,'Ready-To-Drink'
,'Total RTD'
,'Beer Aisle'
);
--(XX rows affected)

begin try drop table #temp_userbyday end try begin catch end catch;
select b.segment_name,country, a.panelist_key, PLATFORM_id, digital_type_id, report_level, date_id, supercategory, property_hash, Property_Key, raw_usage_days, raw_visits, raw_minutes
into #temp_userbyday
from rpt.Person_Standard_Metrics_Common_DU a
join #panelist_list b
on a.panelist_key=b.panelist_key
where report_level in (
0 -- Property
,3 -- Supercategory
, 5 -- Total Internet
)
	;
-- (XX rows affected)

---------------------------------
-- CREATE PROPERTY LOOKUP TABLE
---------------------------------

-- drop table #property_lookup;
begin try drop table #property_lookup end try begin catch end catch;
select Domain_Name as property_name, Domain_Name_Key as property_key, Domain_Name_Hash as property_hash, 235 as digitial_type_id
into #property_lookup
from ref.Domain_Name;
-- (XX rows affected)

set identity_insert #property_lookup on;

insert into #property_lookup (property_name, property_key, property_hash, digitial_type_id)
select app_name as property_name, app_name_key as property_key, app_name_hash as property_hash, 100 as digital_type_id
from ref.App_Name;
-- (XX rows affected)


--select * from #property_lookup;

----------------------------------
-- CREATE VARIABLE LOOKUP TABLE
----------------------------------

begin try drop table #variable_names end try begin catch end catch;
select var_id, var_name, val_id, val_name
into #variable_names
from ref.Variable_Names;

insert into #variable_names select 1 as var_id, 'platform_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 2 as var_id, 'device_type_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 4 as var_id, 'digital_type_id' as var_name, 0 as val_id, 'All Digital' as val_name; -- ### Use 1 in the future

delete from #variable_names where var_id = 6;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 0 as val_id, 'Property' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 1 as val_id, 'Subcategory' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 2 as val_id, 'Category' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 3 as val_id, 'Supercategory' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 4 as val_id, 'Total Vertical' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 5 as val_id, 'Total Internet' as val_name;


create index variable_names1 on #variable_names(val_id);

-- select * from #variable_names

-----------------------------------
-- CREATE PURCHASE LOOKUP TABLES
-----------------------------------


begin try drop table #purchases end try begin catch end catch;
select segment_name,a.Panelist_Key, a.purchase_date
into #purchases
from ref.purchase_date a join #panelist_list b on a.Panelist_Key = b.panelist_key
where purchase_date is not null;
-- (XX rows affected)
-- select * from #purchases_beer;


------------------------------------------------
-- Makes USERBYDAY with reference to purchase date
------------------------------------------------
--------IF MULTIPLE PURCHASE DATES, DATA IS DUPLICATED. SINCE DELETE OUTSIDE OF PURCHASE DAYS THOUGH, PROBABLY FINE
-- select top 100 * from #temp_dayofbuy_v1;
begin try drop table #temp_dayofbuy_v1 end try begin catch end catch;
select a.*, b.purchase_date
into #temp_dayofbuy_v1
from #temp_userbyday a 
left outer join #purchases b
	on a.Panelist_Key=b.panelist_key and a.segment_name=b.segment_name;
-- (XX rows affected)



begin try drop table #temp_dayofbuy end try begin catch end catch;
select a.*, 
	case when date_id=purchase_date then 'Day Of'
		when date_id BETWEEN DATEADD(day,-1,purchase_date) and purchase_date then cast('Day -1' as varchar(25))
		when date_id BETWEEN DATEADD(day,-2,purchase_date) and purchase_date then cast('Day -2' as varchar(25))
		--when date_id BETWEEN DATEADD(day,-3,purchase_date) and purchase_date then cast('Day -3' as varchar(25))
	else '' end as dayofbuy
into #temp_dayofbuy
from #temp_dayofbuy_v1 a;
--(XX rows affected)


delete from #temp_dayofbuy where dayofbuy = ''; --(XX rows affected)

-- select top 100 * from #panelist_buydays order by panelist_key, segment_name, date_id, platform_id;
begin try drop table #panelist_buydays end try begin catch end catch;
select panelist_key, date_id, segment_name, platform_id 
into #panelist_buydays
from #temp_dayofbuy
where dayofbuy != ''
group by panelist_key, Date_ID, segment_name, platform_id
order by panelist_key;
--(XX rows affected)


-- select top 100 * from #dayofbuy_qual order by panelist_key,  platform_id;
begin try drop table #dayofbuy_qual end try begin catch end catch;

select panelist_key, platform_id, segment_name, count(*) as dp_qualdays
into #dayofbuy_qual
from #panelist_buydays
group by panelist_key, platform_id, segment_name;
--(XX rows affected)

-- select top 100 * from #rps_dayofbuy_qual order by segment_name, platform_id;
begin try drop table #rps_dayofbuy_qual end try begin catch end catch; 
select platform_id, segment_name, sum(dp_qualdays) as dp_qualdays, count(*) as dp
into #rps_dayofbuy_qual
from #dayofbuy_qual 
group by PLATFORM_id, segment_name;
-- (XX rows affected)

-- select * from rpt.dayofbuy_qual order by segment_name, platform_id;

-----all of the entries
begin try drop table  #temp_dayofbuy_2  end try begin catch end catch;
select country, panelist_key, platform_id, digital_type_id, report_level, date_id, supercategory, property_hash, Property_Key, raw_usage_days, raw_visits, raw_minutes, 
	cast('' as varchar(50)) as dayofbuy, segment_name
into #temp_dayofbuy_2
from #temp_userbyday;
--(XX rows affected)

-- select * from #temp_dayofbuy_2 where dayofbuy='' order by segment_name;

------inserts day of buy
insert into #temp_dayofbuy_2
select country, panelist_key, platform_id, digital_type_id, report_level, date_id, supercategory, property_hash, Property_Key, raw_usage_days, raw_visits, raw_minutes, dayofbuy, segment_name
from #temp_dayofbuy;
-- (XX rows affected)

-- select * From #temp_dayofbuy_beeraisle;
-- select * from #temp_dayofbuy_2 where segment_name='buyer: beer aisle'



-- select top 1000* from #temp_dayofbuy_2 where panelist_key=47 and segment_name in ('wine', 'total') and date_id = '2019-11-13' and property_key = 366  order by date_id, dayofbuy;;
-- select * from #temp_dayofbuy_2 where panelist_key=47 and segment_name in ('wine', 'total') and date_id = '2019-11-13' and property_key = 366  order by date_id, dayofbuy;
-- select * from #temp_dayofbuy_2 where property_hash =0;


-- FINAL DAILY PROPERTY LEVEL AGG
begin try drop table #temp_panel_agg_total end try begin catch end catch;

select country, platform_id, digital_type_id, report_level, supercategory, property_key, property_hash, dayofbuy, segment_name,
	count(distinct panelist_key) as raw_persons,
	sum(raw_usage_days) as raw_usagedays,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into #temp_panel_agg_total
from #temp_dayofbuy_2
group by country, platform_id, digital_type_id, report_level, supercategory, property_key, property_hash, dayofbuy, segment_name;
-- (XX rows affected)

-- select distinct(segment_name) from #temp_panel_agg_Total;

-- select * from #temp_panel_agg_Total where property_name='google.com';


begin try drop table #temp_standardmetrics1 end try begin catch end catch;
select country, platform_id, digital_type_id, report_level, supercategory, Property_Key, property_hash, dayofbuy, segment_name,
	raw_persons as du, raw_usagedays as du_usedays, raw_visits as du_visits, raw_minutes as du_minutes
into #temp_standardmetrics1
from #temp_panel_agg_total;
-- (XX rows affected)


--------------------------
-- INTEGRATE EVERYTHING
--------------------------

begin try drop table #dayofbuy_total_mins end try begin catch end catch; -- switch to 

select country, platform_id, digital_type_id, dayofbuy, segment_name, du_minutes
into #dayofbuy_total_mins
from #temp_standardmetrics1 
where report_level=5;
-- (XX rows affected)


create index temp_total_internet_minutes1 on #dayofbuy_total_mins(platform_id);
create index temp_total_internet_minutes2 on #dayofbuy_total_mins(digital_type_id);


begin try drop table #temp_standardmetrics_output1 end try begin catch end catch;
select
	a.country, 
	a.platform_id,
	a.digital_type_id, 
	a.segment_name,
	a.report_level,
	a.supercategory,
	a.property_hash, 
	a.Property_Key,
	a.dayofbuy,
	b.dp,
	a.du,
	b.dp_qualdays,
	a.du_usedays,
	a.du_visits,
	cast(a.du_minutes as numeric(30,2)) as du_mins
into #temp_standardmetrics_output1
from #temp_standardmetrics1 a
inner join #rps_dayofbuy_qual b
	on a.platform_id=b.platform_id
	and a.segment_name=b.segment_name
where dayofbuy <>'';
--(XX rows affected)

-- select top 100 * from #temp_Standardmetrics_output1  order by du_usedays desc;
-- select * from #temp_standardmetrics_output1;


insert into #temp_standardmetrics_output1
select
	a.country, 
	a.platform_id,
	a.digital_type_id, 
	a.segment_name,
	a.report_level,
	a.supercategory,
	a.property_hash, 
	a.Property_Key,
	a.dayofbuy,
	b.dp,
	a.du,
	b.dp_qualdays,
	a.du_usedays,
	a.du_visits,
	cast(a.du_minutes as numeric(30,2)) as du_mins
from #temp_standardmetrics1 a
inner join rps.device_topline_qual_unified b
	on a.platform_id=b.platform_id
	and a.segment_name = b.segment_name
where dayofbuy = ''
;
-- (XX rows affected)

begin try drop table rpt.Standard_Metrics_Report_DayOfBuy end try begin catch end catch;
select
	a.country,
	isnull(c.val_name, 'All Devices') as device_type,
	isnull(d.val_name, 'All Digital') as digital_type,
	segment_name,
	e.val_name as report_level,
	a.supercategory,
	dayofbuy,
	dp,
	du,
	dp_qualdays,
	du_usedays,
	du_visits,
	du_mins
into rpt.Standard_Metrics_Report_DayOfBuy
from #temp_standardmetrics_output1 a
left outer join #variable_names c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name= 'digital_type_id'
left outer join #variable_names e
	on a.digital_type_id=e.val_id
	and e.var_name= 'ReportLevel'
where a.report_level <>0 -- removes property level
;
-- (XX rows affected)

-- select * from rpt.Standard_Metrics_Report_DayOfBuy where digital_type='all digital' and dayofbuy = '' ;


begin try drop table rpt.Standard_Metrics_Property_DayOfBuy end try begin catch end catch;
select
	country,
	isnull(c.val_name,'All Devices') as device_type,
	isnull(d.val_name,'All Digital') as digital_type,
	segment_name,
	supercategory,
	s.property_name,
	dayofbuy, 
	dp,
	du,
	dp_qualdays,
	du_usedays,
	du_visits,
	du_mins
into rpt.Standard_Metrics_Property_DayOfBuy 
from #temp_standardmetrics_output1 a
inner join ref.Taxonomy_Common_Properties_Ecosystem_New s
	on a.property_hash=s.property_hash
	and a.digital_type_id=s.digital_type_id
	and s.rpt_flag=1
left outer join #variable_names c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id';
-- (XX rows affected)


-- select * from rpt.Standard_Metrics_Property_DayOfBuy  where property_name='google.com' and segment_name in ('Total Survey', 'beer: Buyer') and device_type= 'All Devices' order by dayofbuy 

-- select * from #temp_standardmetrics_output1 where report_level=3;


--------------------------------------------
-- INSERT IN TOTAL INTERNET TO PROP TABLE
--------------------------------------------
insert into rpt.Standard_Metrics_Property_DayOfBuy 
select 
	country, 
	isnull(c.val_name,'All Devices') as device_type,
	isnull(d.val_name,'All Digital') as digital_type,
	segment_name,
	'*Total Internet*' as supercategory,
	'*Total Internet*' as property_name,
	dayofbuy, 
	dp,
	du,
	dp_qualdays,
	du_usedays,
	du_visits,
	du_mins
from #temp_standardmetrics_output1 a
left outer join #variable_names c 
	on a.platform_id=c.val_id
	and c.var_name='platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name='digital_type_id'
inner join #variable_names g
	on a.report_level=g.val_id
	and g.var_name='reportlevel'
where a.report_level=5
;
-- (XX rows affected)

----------------------------------------------------
-- CALCULATE DAILY REACH FOR TOTAL - PROPERTY LEVEL
-----------------------------------------------------

begin try drop table #temp_total_dailyreach_property end try begin catch end catch;
select country, device_type, digital_type,segment_name, supercategory, property_name, dp, du, dp_qualdays, du_usedays, --du_visits, du_mins,
--	du_mins/du_usedays as du_mpu,
	du_usedays/ cast(dp_qualdays as decimal(10,2)) as daily_reach
into #temp_total_dailyreach_property
from rpt.Standard_Metrics_Property_DayOfBuy a
where dayofbuy = '' 
;
-- (XX rows affected)

--------------------------------------------------------------
-- CALCULATE AVG DAILY REACH FOR DAY OF BUY -- PROPERTY LEVEL
--------------------------------------------------------------
-- first, calculate average day of buy (across the 3)
-- select top 100 * from rpt.Standard_Metrics_Property_DayOfBuy;
begin try drop table  #average_useday1 end try begin catch end catch;
select country, device_type, digital_type,segment_name, supercategory, property_name,  dp, dp_qualdays, 
	sum(du_usedays) as average_useday
--	sum(du_mins) as average_mins
into #average_useday1
from rpt.Standard_Metrics_Property_DayOfBuy  a
where dayofbuy in ('Day of', 'Day -1', 'Day -2') 
group by country, device_type, digital_type, supercategory, property_name, segment_name, dp, dp_qualdays;
-- (XX rows affected)


begin try drop table #dailyreach_dayofbuy end try begin catch end catch;
select a.*, 
	average_useday/ cast(dp_qualdays as decimal(10,2)) as daily_reach
into #dailyreach_dayofbuy
from #average_useday1 a;
-- (XX rows affected)

begin try drop table #booya end try begin catch end catch;
select a.country, a.device_type, a.digital_type, a.segment_name, a.supercategory, a.property_name, b.du as total_study_du, 
a.dp as three_day_dp, a.dp_qualdays as three_day_dp_qualdays, a.average_useday as three_day_average_useday,
 a.daily_reach as three_day_avg_daily_reach, b.daily_reach as total_study_daily_reach
into #booya 
from #dailyreach_dayofbuy a
join #temp_total_dailyreach_property b 
on a.country = b.country
and  a. device_type = b.device_type
and a.digital_type = b.digital_type
and a.supercategory = b.supercategory
and a.segment_name = b.segment_name
and a.property_name = b.property_name
;
--(XX rows affected)

begin try drop table #final_index end try begin catch end catch;
select country, device_type, digital_type,segment_name, supercategory, property_name,total_study_du, three_day_dp, three_day_dp_qualdays, three_day_average_useday, --average_mins, daily_mpu, 
three_day_avg_daily_reach, total_study_daily_reach,
case when three_day_average_useday < 10 then NULL 
else cast ( 100 * (three_day_avg_daily_reach/total_study_daily_reach) as int) end as monthly_reach_index
into #final_index
from #booya;
--(XX rows affected)

-- select * from #dailyreach_dayofbuy where property_name='google.com' order by device_type, digital_type, supercategory, segment_name;

-----------------
-- FINAL TABLE
-----------------
begin try drop table rpt.DayOfBuy_Indexing end try begin catch end catch;
select country, device_type, digital_type,  segment_name,supercategory,property_name,total_study_du,three_day_dp, three_day_dp_qualdays, three_day_average_useday, --average_mins, daily_mpu, 
three_day_avg_daily_reach, total_study_daily_reach, monthly_reach_index
into rpt.DayOfBuy_Indexing
from #final_index;
-- (XX rows affected)

----------
--EXPORT
---------
select  device_type, digital_type, segment_name,supercategory, a.property_name,total_study_du, three_day_dp, three_day_dp_qualdays, three_day_average_useday, --average_mins, daily_mpu, 
three_day_avg_daily_reach, total_study_daily_reach, monthly_reach_index
from rpt.DayOfBuy_Indexing a
where supercategory not in ('[Alert]', '[TBD]','[REMOVE]') 
and monthly_reach_index is not null
and (
(device_type = 'All Devices' and digital_type = 'Web')
OR (device_type = 'Mobile' and digital_type = 'App')
)
order by device_type, digital_type, segment_name,supercategory, a.property_name
;



