---------------------------------------------------------------------------------------------------
-- SEMANTIC PROPERTY Coloring: NO BONUS WORDS
-- SCRIPT 01
---------------------------------------------------------------------------------------------------
-- OWNER
-- DATE
---------------------------------------------------------------------------------------------------

use [PROJECTNAME];

/*
AFTER PROJECT SPECIFIC CORE IS LOCKED DOWN FIND AND REPLACE 
Core.ref.taxonomy_Common_properties with the new table
*/





-- ================================================================================================
-- 2) PROPERTY COLOR ASSIGNMENT
-- ================================================================================================
-- Note: This section is expected to undergo multiple rounds of edits based on inputs from
-- Eco Workshop I (planning), Workshop II (50%), and Workshop III (data close).
-- A single list assigning domains and apps red/green/white color is done.
-- Processing order is:

-- CATEGORY COLOR PLANNING. (a-c) RED GREEN WHITE PLAN.
--	(a) start with all legimate properties in ref.domain_name
--		this should already have all black list categories removed (props that should be removed from all projects). (e.g. market research sites.)
--  (b) flag all defaults in red categories (props that should be removed from this specific project). (e.g. gambling sites in a project about healthcare)
--  (c) assign all defaults in white categories. (BDG determined from vendor/common category/subcat review)

-- AUTOMATED PROPERTY ASSIGNMENTS, BASED ON PATTERN APPLY AND LEFTOVER LOGIC. (d-e)
--  (d) assign all properties matching semantic structures of pattern list. (e.g. any domain matching "health" in healthcare project.)
--      flag all properties matching red semantic exculusions. (e.g. animalhospital.org should be red if animal is a "remove" keyword.)
--- (e) remaining unassigned domains are green properties.

-- PROPERTY COLOR PLANNING. MANUAL. (f-j) RED GREEN WHITE PLAN.
--  (f) flag red list planned single properties (from BDG Color Plan)
--  (g) assign/add white list requested single properties (requested by client. should add even if doesn't currently exist in data)
--  (h) assign/add white list suggested single properties (suggested by client serive or BDG. add even if doesn't currently exist in data)
--  (i) assign/add green list requested single properties (sites such as amazon.com client wants to be sure are counted, but aren't white)
--  (j) assign/add green list suggested single properties (sites such as target.com BDG wants to be sure is counted, but aren't white)
-- PROPERTY COLOR DISCOVERY/EDITTING. MANUAL. (k-m)
--  (k) flag red list discovered single properties (based on BDG review)
--  (l) assign/add white list discovered single properties (based on BDG review)
--  (m) assign/add green list discovered single properties (based on BDG review)
--  (n) flag red list discovered apps (based on BDG review)

-- FINAL PERMANENT TABLES CREATED FROM THIS PROCESS
-- select * from ref.domain_color;
-- select * from ref.app_color;


-- 2a) start with all legimate properties in ref.domain
----------------------------------------------------

begin try drop table #domain_color end try begin catch end catch;

select property_name as domain_name, lower(core.dbo.alphanum(property_name)) as domain_name_gra,property_hash as domain_name_hash,domain_name_key, common_supercategory, common_category, common_vertical,
	cast('' as varchar(300)) as semantic_lemma, --cast('' as varchar(300)) as semantic_lemma_english, 
	cast('' as varchar(300)) as semantic_priority_group,  
	0 as request_flag,
	cast('2a. core prop Common Taxonomy' as varchar(300)) as step_assigned, cast('' as varchar(10)) as prop_color, vetted_flag
into #domain_color
from Core.ref.taxonomy_common_properties a
join ref.domain_name b on a.property_hash = b.domain_name_hash and a.digital_type_id=235
;
--(XX rows affected)

update #domain_color
set step_assigned='2a. new core prop SW'
from #domain_color
where common_supercategory in ('[TBD]') 
or (common_supercategory =  '[Alert]' and common_category = '[Alert: TBD]')
and step_assigned != '2a. core prop Common Taxonomy';
--(XX rows affected)

--- ASSIGN SOCIAL NETWORK DOMAINS GREEN
update #domain_color
set prop_color = 'green'
where common_category = 'Social Network'
and vetted_flag = 1;
--(XX rows affected)



-- add domains that are new to this project have been missed from prior projects
-- check on this with Jimmy, but if the core table is updated with new domains, we don't need this step
insert into #domain_color
select a.domain_name, lower(core.dbo.alphanum(domain_name)) as domain_name_gra,a.domain_name_hash, domain_name_key,'' as common_supercategory, '' as common_category, '' as common_vertical,
	'' as semantic_lemma, --'' as semantic_lemma_english, 
	'' as semantic_priority_group,
	0 as request_flag, '2a. new prop' as step_assigned, '' as prop_color, vetted_flag
from ref.domain_name a
left join Core.ref.taxonomy_common_properties b
	on a.Domain_Name_hash=b.property_hash
where b.property_name is null;
--(XX rows affected)



begin try drop table #app_color end try begin catch end catch;

select  property_name as app_name, lower(core.dbo.alphanum(property_name)) as app_name_gra, property_hash as app_name_hash, d.app_name_key,
	common_supercategory, common_category, common_vertical, 
	b.category as google_play_category, c.category as itunes_category,
	cast('' as varchar(300)) as semantic_lemma, --cast('' as varchar(300)) as semantic_lemma_english,
	cast('' as varchar(300)) as semantic_priority_group, 
	0 as request_flag,
	cast('2a. core prop Common Taxonomy' as varchar(300)) as step_assigned, cast('' as varchar(10)) as prop_color, vetted_flag
into #app_color
from Core.ref.taxonomy_common_properties a 
left join core.ref.app_google_play_details b on a.property_hash = b.App_Name_Hash
left join core.ref.app_apple_itunes_details c on a.property_hash = c.App_Name_Hash
join ref.app_name d on a.property_hash = d.app_name_hash
where digital_type_id=100;
--(XX rows affected)


update #app_color
set step_assigned='2a. new core prop SW'
from #app_color
where common_supercategory in ('[TBD]') 
or (common_supercategory =  '[Alert]' and common_category = '[Alert: TBD]')
and step_assigned != '2a. core prop Common Taxonomy';
--(XX rows affected)


--- ASSIGN SOCIAL NETWORK APPS GREEN
update #app_color
set prop_color = 'green'
where common_category = 'Social Network'
and vetted_flag = 1;
--(XX rows affected)


-- 2b) flag all defaults in red categories
----------------------------------------------------
--'2b. cat plan red Common Taxonomy'
update #domain_color
set prop_color = 'red', step_assigned = '2b. cat plan red Common Taxonomy'
from #domain_color a
join ref.color_plan b 
on a.common_supercategory = b.supercategory 
and a.common_category = b.category
and a.common_vertical = b.vertical  
and vetted_flag=1
where b.color = 'red' and b.categorization_source = 'core'
 ;
--(XX rows affected)


--'2b. cat plan red Common Taxonomy'
update #app_color
set  prop_color = 'red', step_assigned = '2b. cat plan red Common Taxonomy'
from #app_color a
join ref.color_plan b 
on a.common_supercategory = b.supercategory 
and a.common_category = b.category
and a.common_vertical = b.vertical  
and vetted_flag=1
where b.color = 'red' and b.categorization_source = 'core'
;
--(XX rows affected)


----------Based on google play
--'2b. cat plan red google play'
update #app_color
set  prop_color = 'red', step_assigned = '2b. cat plan red google play'
from #app_color a join ref.color_plan b 
on  a.google_play_category = b.category
and step_assigned not in ('2b. cat plan red Common Taxonomy')
where b.color = 'red' and b.categorization_source = 'google play'
;
--(XX rows affected)



----------Based on itunes
--'2b. cat plan red itunes'
update #app_color
set  prop_color = 'red', step_assigned = '2b. cat plan red itunes'
from #app_color a join ref.color_plan b 
on  a.itunes_category = b.category
and step_assigned not in ('2b. cat plan red Common Taxonomy','2b. cat plan red Google Play')
where b.color = 'red' and b.categorization_source = 'itunes'
;
--(XX rows affected)



-- 2c) assign all defaults in white categories 
----------------------------------------------------

-- IF THERE ARE ANY SUPERCATEGORIES/CATEGORIES/SUBCATEGORIES WE KNOW SHOULD BE WHITELISTED
--'2c. core taxonomy cat plan white'
update #domain_color
set prop_color = 'white', step_assigned = '2c. core taxonomy cat plan white'
from #domain_color a
join ref.color_plan b 
on a.common_supercategory = b.supercategory 
and a.common_category = b.category
and a.common_vertical = b.vertical  
where b.color = 'white' and b.categorization_source = 'core'	
; 
--(XX rows affected)


--'2c. core taxonomy cat plan white'
update #app_color
set prop_color = 'white', step_assigned = '2c. core taxonomy cat plan white'
from #app_color a
join ref.color_plan b 
on a.common_supercategory = b.supercategory 
and a.common_category = b.category
and a.common_vertical = b.vertical  
where b.color = 'white' and b.categorization_source = 'core'	
; 
--(XX rows affected)




-------------Google Play
update #app_color
set prop_color = 'white', step_assigned = '2c. google play cat plan white'
from #app_color a join ref.color_plan b 
on  a.google_play_category = b.category
where b.color = 'white' and b.categorization_source = 'google play'
and step_assigned not in ('2c. core taxonomy cat plan white')
;
--(XX rows affected)

-------------iTunes
update #app_color
set prop_color = 'white', step_assigned = '2c. itunes cat plan white'
from #app_color a join ref.color_plan b 
on  a.itunes_category = b.category
where b.color = 'white' and b.categorization_source = 'itunes'
and step_assigned not in ('2c. core taxonomy cat plan white','2c. google play cat plan white')
;
--(XX rows affected)

----------EXISTING ECOSYTEM CLASSIFICIATIONS (ADD IN MATCH ON PARENT ENTITY WHEN COMPLETE)
-- check column names: select top 5 * from Core.ref.Ecosystem_Classifications;
update #domain_color
set prop_color = 'white', step_assigned = '2c. previous eco'
from #domain_color a
join Core.ref.Ecosystem_Classifications b 
on (a.domain_name = b.property_name OR a.domain_name_hash = b.property_hash)
where b.COLUMN_NAME_RELEVANT_ECOSYSTEM = 'white' and b.digital_type_id = 235
; 
--(XX rows affected)

update #app_color
set prop_color = 'white', step_assigned = '2c. previous eco'
from #app_color a
join Core.ref.Ecosystem_Classifications b 
on (a.app_name = b.property_name OR a.app_name_hash = b.property_hash)
where b.COLUMN_NAME_RELEVANT_ECOSYSTEM = 'white' and b.digital_type_id = 100
; 
--(XX rows affected)

----Does green as well just in case something is red that shouldn't be
update #domain_color
set prop_color = 'green', step_assigned = '2c. previous eco'
from #domain_color a
join Core.ref.Ecosystem_Classifications b 
on (a.domain_name = b.property_name OR a.domain_name_hash = b.property_hash)
where b.COLUMN_NAME_RELEVANT_ECOSYSTEM = 'green' and b.digital_type_id = 235 and a.prop_color = 'red'
; 
--(XX rows affected)

update #app_color
set prop_color = 'green', step_assigned = '2c. previous eco'
from #app_color a
join Core.ref.Ecosystem_Classifications b 
on (a.app_name = b.property_name OR a.app_name_hash = b.property_hash)
where b.COLUMN_NAME_RELEVANT_ECOSYSTEM = 'green' and b.digital_type_id = 100 and a.prop_color = 'red'
; 
--(XX rows affected)


-- AUTOMATED PROPERTY ASSIGNMENTS, BASED ON PATTERN APPLY AND LEFTOVER LOGIC. (d-e)

-- 2d) assign all properties matching semantic structures of pattern list.
----------------------------------------------------

-- select * from #priority_group_map order by semantic_priority_id;
-- select * from ref.pattern_table_home_garden;

begin try drop table #priority_group_map end try begin catch end catch;
select semantic_priority_id, semantic_priority_group
into #priority_group_map
from ref.pattern_table
group by semantic_priority_id, semantic_priority_group;
--(XX rows affected)

-- select * from ref.pattern_table_home_garden;
begin try drop table #domain_automatching_white end try begin catch end catch;
select a.domain_name, min(b.semantic_lemma) as semantic_lemma, --min(b.semantic_lemma_english) as semantic_lemma_english, 
min(b.semantic_priority_id) as semantic_priority_id, min(pattern) as example_pattern
into #domain_automatching_white
from #domain_color a
inner join ref.pattern_table b
	on a.domain_name_gra like b.pattern
	and b.run_on_green = 1
group by domain_name;
--(XX rows affected)


begin try drop table #app_automatching_white end try begin catch end catch;

select a.app_name, min(b.semantic_lemma) as semantic_lemma, --min(b.semantic_lemma_english) as semantic_lemma_english, 
 min(b.semantic_priority_id) as semantic_priority_id, min(pattern) as example_pattern
into #app_automatching_white
from #app_color a
inner join ref.pattern_table b
	on a.app_name_gra like b.pattern -- NOTE: Match on cleaned app name
	and run_on_green = 1
group by app_name;
--(XX rows affected)

begin try drop table #domain_automatching_red end try begin catch end catch;
select a.domain_name, min(b.semantic_lemma) as semantic_lemma, --min(b.semantic_lemma_english) as semantic_lemma_english, 
min(b.semantic_priority_id) as semantic_priority_id, min(pattern) as example_pattern
into #domain_automatching_red
from #domain_color a
inner join ref.pattern_table b
	on a.domain_name_gra like b.pattern
	and b.semantic_priority_group ='Remove'
group by domain_name;
--(XX rows affected)



begin try drop table #app_automatching_red end try begin catch end catch;
select a.app_name, min(b.semantic_lemma) as semantic_lemma, --min(b.semantic_lemma_english) as semantic_lemma_english, 
min(b.semantic_priority_id) as semantic_priority_id, min(pattern) as example_pattern
into #app_automatching_red
from #app_color a
inner join ref.pattern_table b
	on a.app_name_gra like b.pattern -- NOTE: Match on cleaned app name
	and b.semantic_priority_group ='Remove'
group by app_name;
--(XX rows affected)



update #domain_color
set prop_color = 'white', step_assigned = '2d. auto pattern white', semantic_lemma=b.semantic_lemma, --semantic_lemma_english=b.semantic_lemma_english, 
semantic_priority_group=c.semantic_priority_group
from #domain_color a
inner join #domain_automatching_white b
	on a.domain_name=b.domain_name
inner join #priority_group_map c
	on b.semantic_priority_id=c.semantic_priority_id;
--(XX rows affected)


update #app_color
set prop_color = 'white', step_assigned = '2d. auto pattern white', semantic_lemma=b.semantic_lemma, --semantic_lemma_english=b.semantic_lemma_english, 
semantic_priority_group=c.semantic_priority_group
from #app_color a
inner join #app_automatching_white b
	on a.app_name=b.app_name
inner join #priority_group_map c
	on b.semantic_priority_id=c.semantic_priority_id;
--(XX rows affected)


update #domain_color
set prop_color = 'red', step_assigned = '2d. auto pattern red', semantic_lemma=b.semantic_lemma,-- semantic_lemma_english=b.semantic_lemma_english, 
semantic_priority_group=c.semantic_priority_group
from #domain_color a
inner join #domain_automatching_red b
	on a.domain_name=b.domain_name
inner join #priority_group_map c
	on b.semantic_priority_id=c.semantic_priority_id;
--(XX rows affected)



update #app_color
set prop_color = 'red', step_assigned = '2d. auto pattern red', semantic_lemma=b.semantic_lemma, --semantic_lemma_english=b.semantic_lemma_english, 
semantic_priority_group=c.semantic_priority_group
from #app_color a
inner join #app_automatching_red b
	on a.app_name=b.app_name
inner join #priority_group_map c
	on b.semantic_priority_id=c.semantic_priority_id;
--(XX rows affected)




-- 2e) remaining unassigned domains are green properties.
----------------------------------------------------

update #domain_color
set prop_color = 'green', step_assigned = '2e. auto leftover green Core Taxonomy'
from #domain_color where prop_color = '' and step_assigned='2a. core prop Common Taxonomy';
--(XX rows affected)

update #domain_color
set prop_color = 'green', step_assigned = '2e. auto leftover  green SW'
from #domain_color where prop_color = '' and step_assigned='2a. new core prop SW';
--(XX rows affected)

update #domain_color
set prop_color = 'green', step_assigned = '2e. auto leftover new green'
from #domain_color where prop_color = '' and step_assigned='2a. new prop';
--(XX rows affected)


update #domain_color 
set prop_color = 'blue', step_assigned = '2e. auto search blue'
from #domain_color where domain_name 
like 'bing.%' 
or domain_name like 'google.%'
or  common_category = 'Search Engine';
--(XX rows affected)


update #app_color
set prop_color = 'green', step_assigned = '2e. auto leftover core green Common Taxonomy'
from #app_color where prop_color = '' and step_assigned='2a. core prop Common Taxonomy';
--(XX rows affected)


update #app_color
set prop_color = 'green', step_assigned = '2e. auto leftover core green SW'
from #app_color where prop_color = '' and step_assigned='2a. new core prop SW';
--(XX rows affected)

update #app_color 
set prop_color = 'blue', step_assigned = '2e. auto search blue'
from #app_color
where app_name in ('Google Search','Google', 'Bing Rewards', 'Bing')
or common_category = 'Search Engine';
--(XX row affected)



-- PROPERTY COLOR PLANNING. MANUAL.

-- 2f) flag red (or blue or black) list suggested/requested single properties
----------------------------------------------------
update #domain_color
set prop_color = 'red', step_assigned = '2f. domain plan red'
from #domain_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 235
and (a.domain_name_key = b.property_key OR a.domain_name = b.property_name or a.domain_name_hash = b.property_hash) -- since at the beginning of projects wont always have prop key
and b.new_prop_color in ('red','black','blue')
and b.assignment_type in ('suggested', 'requested')
where b.digital_type_id = 235;
--(XX row affected)


update #app_color
set prop_color = 'red', step_assigned = '2f. app plan red'
from #app_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 100
and (a.app_name_key = b.property_key OR a.app_name = b.property_name or a.app_name_hash = b.property_hash)
and b.new_prop_color in ('red','black','blue')
and b.assignment_type in ('suggested', 'requested')
where digital_type_id = 100;
--(XX row affected)


-- 2g) assign/add white list requested single properties 
----------------------------------------------------
--select * from #domain_color where domain_name like '%
--select new_prop_color, assignment_type from ref.eco_color_assignment group by new_prop_color, assignment_type order by new_prop_color, assignment_type;
update #domain_color
set prop_color = 'white', step_assigned = '2g. domain request white', request_flag=1
from #domain_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 235
and (a.domain_name_key = b.property_key OR a.domain_name = b.property_name or a.domain_name_hash = b.property_hash)
and b.new_prop_color = 'white'
and b.assignment_type in ('requested')
where b.digital_type_id = 235;
--(XX rows affected)


update #app_color
set prop_color = 'white', step_assigned = '2g. app request white', request_flag=1
from #app_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 100
and (a.app_name_key = b.property_key OR a.app_name = b.property_name or a.app_name_hash = b.property_hash)
and b.new_prop_color = 'white'
and b.assignment_type in ('requested')
where digital_type_id = 100
;
--(XX rows affected)


-- 2h) assign/add white list suggested single properties 
----------------------------------------------------
--select new_prop_color, assignment_type from ref.eco_color_assignment group by new_prop_color, assignment_type order by new_prop_color, assignment_type;
update #domain_color
set prop_color = 'white', step_assigned = '2h. domain suggest white'
from #domain_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 235
and (a.domain_name_key = b.property_key OR a.domain_name = b.property_name or a.domain_name_hash = b.property_hash)
and b.new_prop_color = 'white'
and b.assignment_type in ('suggested')
where step_assigned ! = '2g. domain request white' and b.digital_type_id = 235;
-- (XX rows affected)


update #app_color
set prop_color = 'white', step_assigned = '2h. app suggest white'
from #app_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 100
and (a.app_name_key = b.property_key OR a.app_name = b.property_name or a.app_name_hash = b.property_hash)
and b.new_prop_color = 'white'
and b.assignment_type in ('suggested')
where step_assigned ! = '2g. app request white' and digital_type_id = 100; 
-- (XX row affected)


-- 2i) assign/add green list requested single properties
----------------------------------------------------
--select new_prop_color, assignment_type from ref.eco_color_assignment group by new_prop_color, assignment_type order by new_prop_color, assignment_type;
update #domain_color
set prop_color = 'green', step_assigned = '2i. domain request green', request_flag=1
from #domain_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 235
and (a.domain_name_key = b.property_key OR a.domain_name = b.property_name or a.domain_name_hash = b.property_hash)
and b.new_prop_color = 'green'
and b.assignment_type in ('requested') and b.digital_type_id = 235;
--(XX rows affected)


-- 2j) assign/add green list suggested single properties
----------------------------------------------------
update #domain_color
set prop_color = 'green', step_assigned = '2j. domain suggest green'
from #domain_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 235
and  (a.domain_name_key = b.property_key OR a.domain_name = b.property_name or a.domain_name_hash = b.property_hash)
and b.new_prop_color = 'green'
and b.assignment_type in ('suggested')
or domain_name in (
'amazon.com'
,'walmart.com'
,'target.com'
,'youtube.com'
)
where step_assigned != '2i. domain request green'and b.digital_type_id = 235;
--(XX rows affected)


-- PROPERTY COLOR DISCOVERY/EDITTING. MANUAL. (k-m)

-- 2k) flag red list discovered single properties (based on BDG review)
----------------------------------------------------
--select new_prop_color, assignment_type from ref.eco_color_assignment group by new_prop_color, assignment_type order by new_prop_color, assignment_type;
update #domain_color
set prop_color = 'red', step_assigned = '2k. domain discover red'
from #domain_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 235
and a.domain_name_key = b.property_key
and b.new_prop_color in ('red','black','blue')
and b.assignment_type in ('discovered')
where a.prop_color != 'red' -- allows us to make the color assignment list with the full data but only changes step assigned if its different
;
-- (XX row affected)

update #app_color
set prop_color = 'red', step_assigned = '2k. app discover red'
from #app_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 100
and a.app_name_key = b.property_key
and b.new_prop_color in ('red','black','blue')
and b.assignment_type in ('discovered')
where a.prop_color != 'red' -- allows us to make the color assignment list with the full data but only changes step assigned if its different
;
-- (XX row affected)

-- 2l) assign/add white list discovered single properties (based on BDG review)
----------------------------------------------------
update #domain_color
set prop_color = 'white', step_assigned = '2l. domain discover white'
from #domain_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 235
and a.domain_name_key = b.property_key
and b.new_prop_color in ('white')
and b.assignment_type in ('discovered')
where a.prop_color != 'white' -- allows us to make the color assignment list with the full data but only changes step assigned if its different
;
-- (XX row affected)

update #app_color
set prop_color = 'white', step_assigned = '2l. app discover white'
from #app_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 100
and a.app_name_key = b.property_key
and b.new_prop_color in ('white')
and b.assignment_type in ('discovered')
where a.prop_color != 'white' -- allows us to make the color assignment list with the full data but only changes step assigned if its different
;
-- (XX row affected)

-- 2m) assign/add green list discovered single properties (based on BDG review)
----------------------------------------------------
update #domain_color
set prop_color = 'green', step_assigned = '2m. domain discover green'
from #domain_color a
join ref.eco_color_assignment b 
on b.digital_type_id = 235
and a.domain_name_key = b.property_key
and b.new_prop_color in ('green')
and b.assignment_type in ('discovered')
where a.prop_color != 'green' -- allows us to make the color assignment list with the full data but only changes step assigned if its different
;
-- (XX row affected)


-- ###########################################################################################
-- 2z) create permanent color table
-- EXPORT: PROPERTY COLOR TABLE THAT CAN BE REVIEWED FOR QA
-- ###########################################################################################
-- select * from ref.domain_color;
-- select * from ref.domain_color where step_assigned not like '%2e%' and step_assigned not like '%2b%';
-- select count(*) from ref.domain_color;
begin try drop table ref.domain_color end try begin catch end catch;

select domain_name_key, domain_name,domain_name_gra,common_supercategory, common_category, common_vertical, semantic_lemma, --semantic_lemma_english
	 semantic_priority_group, 
		prop_color, step_assigned, request_flag, vetted_flag
into ref.domain_color
from #domain_color
;
-- (XX rows affected)


begin try drop table ref.app_color end try begin catch end catch;

select app_name_key, app_name,app_name_gra, common_supercategory, common_category, common_vertical, semantic_lemma, --semantic_lemma_english
	semantic_priority_group,
		prop_color, step_assigned, request_flag, vetted_flag
into ref.app_color
from #app_color a
;
-- (XX rows affected)

/*

select prop_color, step_assigned, min(domain_name) as first_domain, max(domain_name) as last_domain, count(*) as ct_domains, sum(request_flag) as ct_requested_domains
from ref.domain_color a
group by prop_color, step_assigned
order by step_assigned,prop_color ;

select prop_color, step_assigned, min(app_name) as first_app, max(app_name) as last_app, count(*) as ct_apps, sum(request_flag) as ct_requested_apps
from ref.app_color a
group by prop_color, step_assigned
order by step_assigned,prop_color ;

-- select * from ref.domain_color where prop_color = 'white';
*/