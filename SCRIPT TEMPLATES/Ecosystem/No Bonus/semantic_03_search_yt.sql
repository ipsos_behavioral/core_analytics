---------------------------------------------------------------------------------------------------
-- SEMANTIC MATCHING: TO SEARCH AND VIDEO FILES: NO BONUS WORDS
--Script 03
---------------------------------------------------------------------------------------------------
-- Owner
-- Date

---------------------------------------------------------------------------------------------------

use [PROJECTNAME];

-- 2) MATCH TO SEARCH
---------------------------------------------------------------------------------------------------

-- select * from ref.search_term;

-- set up gra table
-- select * from #search_term_gra;
-- select distinct property_name from ref.search_term
begin try drop table #search_term_gra end try begin catch end catch;

select search_term_key, search_term, core.dbo.alphanum(search_term) as search_term_gra
into #search_term_gra
from ref.search_term
group by search_term_key, search_term
;
-- (XX rows affected)




-- select * from ref.pattern_table_beauty where semantic_lemma like '%tv%';
-- select * from #search_term_gra_best_pattern;

begin try drop table #search_term_gra_best_pattern end try begin catch end catch;

select search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern
from #search_term_gra a
inner join ref.pattern_table b
	on a.search_term_gra like b.pattern
	and (run_on_green=1 or semantic_priority_group = 'REMOVE')
group by search_term_key;
-- (XX rows affected)


-- select top 100 * from #search_semantic_match where search_term_key = '516214';
-- select top 100 * from #search_semantic_match where search_term_key = '516214';
begin try drop table #search_semantic_match_v1 end try begin catch end catch;
select a.search_term_key, 
	 a.search_term, 
	 c.pattern_id as search_pattern_id, 
	 c.pattern as search_pattern,
	 cast('' as nvarchar(300)) as search_semantic_keyword, 
	 c.semantic_priority_group as search_semantic_group, -- keeps in just for removal
	 cast('' as nvarchar(300)) as brand,
	cast('' as nvarchar(300)) as retailer
into #search_semantic_match_v1
from #search_term_gra a
inner join #search_term_gra_best_pattern b
	on a.search_term_key=b.search_term_key
inner join ref.pattern_table c
	on b.best_pattern_id=c.pattern_id
group by a.search_term_key, a.search_term, c.pattern_id, c.pattern,
	c.semantic_priority_group, 
	c.semantic_lemma
	;
-- (XX rows affected)

delete from #search_semantic_match_v1 where search_semantic_group = 'REMOVE'; --(XX rows affected)

----Changes to blank afterwards for repass
update #search_semantic_match_v1 set search_semantic_group = ''; --(XX rows affected)


--------- Non retailer or branded
-- select distinct semantic_priority_group from ref.pattern_table;
begin try drop table #search_term_gra_best_pattern_no_brand_retailer end try begin catch end catch;

select a.search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern_no_brand_retailer
from #search_term_gra a
inner join ref.pattern_table b
	on a.search_term_gra like b.pattern
	and b.semantic_priority_group not in (
	'Brand'
	,'Category Retailer'
	)
inner join #search_semantic_match_v1 c on a.Search_Term_Key = c.Search_Term_Key
group by a.search_term_key;
--(XX rows affected)




begin try drop table #search_term_gra_best_pattern_no_brand_retailer end try begin catch end catch;

select a.search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern_no_brand_retailer
from #search_term_gra a
inner join ref.pattern_table b
	on a.search_term_gra like b.pattern
	and b.semantic_priority_group not in (
	'Brand'
	,'Category Retailer'
	)
inner join #search_semantic_match_v1 c on a.Search_Term_Key = c.Search_Term_Key
group by a.search_term_key;
--(64 rows affected)


begin try drop table #best_search_patterns_no_brand_retailer  end try begin catch end catch;
select a.search_term_key, a.best_pattern_id, b.semantic_lemma, --b.semantic_lemma_english,
b.semantic_priority_group
into #best_search_patterns_no_brand_retailer 
from #search_term_gra_best_pattern_no_brand_retailer a
join ref.pattern_table b on a.best_pattern_id = b.pattern_id
--left join #search_gra_best_bonus_match  b on a.Search_Term_Key = b.Search_Term_Key
group by a.search_term_key, a.best_pattern_id, b.semantic_lemma,--b.semantic_lemma_english,
 b.semantic_priority_group;
--(64 rows affected)

update #search_semantic_match_v1
set search_semantic_keyword = b.semantic_lemma, --search_semantic_keyword_english = b.semantic_lemma_english,
search_semantic_group = b.semantic_priority_group
from #search_semantic_match_v1 a
join #best_search_patterns_no_brand_retailer b on a.Search_Term_Key = b.Search_Term_Key;
--(64 rows affected)


--------- Brand
begin try drop table #search_term_gra_best_pattern_brand end try begin catch end catch;

select a.search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern_brand
from #search_term_gra a
inner join ref.pattern_table b
	on a.search_term_gra like b.pattern
	and b.semantic_priority_group = 'Brand'
inner join #search_semantic_match_v1 c on a.Search_Term_Key = c.Search_Term_Key
group by a.search_term_key;
--(8 rows affected)

begin try drop table #best_search_patterns_brand end try begin catch end catch;
select a.search_term_key, a.best_pattern_id, b.semantic_lemma, --b.semantic_lemma_english,
b.semantic_priority_group
into #best_search_patterns_brand
from #search_term_gra_best_pattern_brand a
join ref.pattern_table b on a.best_pattern_id = b.pattern_id
group by a.search_term_key, a.best_pattern_id, b.semantic_lemma,-- b.semantic_lemma_english,
b.semantic_priority_group;
--(8 rows affected)


update #search_semantic_match_v1
set brand = b.semantic_lemma
from #search_semantic_match_v1 a
join #best_search_patterns_brand b on a.Search_Term_Key = b.Search_Term_Key;
--(8 rows affected)


----Makes keyword brand if no other content
update #search_semantic_match_v1
set search_semantic_keyword = b.semantic_lemma, --search_semantic_keyword_english = b.semantic_lemma_english,
search_semantic_group = semantic_priority_group
from #search_semantic_match_v1 a
join #best_search_patterns_brand b on a.Search_Term_Key = b.Search_Term_Key
where a.search_semantic_keyword = '';
--(4 rows affected)

-- select distinct semantic_priority_group from ref.pattern_table;
--------- Retailer Pass
--select * from ref.pattern_table;
begin try drop table #retailer_pattern end try begin catch end catch;
select pattern_id, run_on_white, run_on_green, pattern, semantic_lemma,
semantic_priority_id, semantic_priority_group
into #retailer_pattern 
from ref.pattern_table
where semantic_priority_group in (
'Category Retailer'
);
--(0 rows affected)

set identity_insert #retailer_pattern on;
insert into #retailer_pattern  (pattern_id, run_on_white, run_on_green, pattern, semantic_lemma,semantic_priority_id, semantic_priority_group)
select pattern_id,run_on_white, run_on_green, pattern, semantic_lemma,
semantic_priority_id, semantic_priority_group
from core.ref.general_retailer_pattern;
--(24 rows affected)


begin try drop table #search_term_gra_best_pattern_retailer end try begin catch end catch;

select a.search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern_retailer
from #search_term_gra a
inner join #retailer_pattern b
	on a.search_term_gra like b.pattern
inner join #search_semantic_match_v1 c on a.Search_Term_Key = c.Search_Term_Key
group by a.search_term_key;
--(0 rows affected)



begin try drop table #best_search_patterns_retailer end try begin catch end catch;
select a.search_term_key, a.best_pattern_id, b.semantic_lemma,b.semantic_lemma_english ,b.semantic_priority_group
into #best_search_patterns_retailer
from #search_term_gra_best_pattern_retailer a
join #retailer_pattern b on a.best_pattern_id = b.pattern_id
group by a.search_term_key, a.best_pattern_id, b.semantic_lemma,b.semantic_lemma_english, b.semantic_priority_group;
--(0 rows affected)


update #search_semantic_match_v1
set retailer= b.semantic_lemma
from #search_semantic_match_v1 a
join #best_search_patterns_retailer b on a.Search_Term_Key = b.Search_Term_Key;
--(0 rows affected)


----Makes keyword retailer if no other content or brand 
update #search_semantic_match_v1
set search_semantic_keyword = b.semantic_lemma,--search_semantic_keyword = b.semantic_lemma_english, 
search_semantic_group = semantic_priority_group
from #search_semantic_match_v1 a
join #best_search_patterns_retailer b on a.Search_Term_Key = b.Search_Term_Key
where a.search_semantic_keyword = '';
--(0 rows affected)


begin try drop table #search_semantic_match end try begin catch end catch;
select a.search_term_key, 
	 a.search_term, 
	 a.search_pattern_id, 
	 a.search_pattern,
	 a.search_semantic_keyword, 
--	 a.search_semantic_keyword_english,
	 a.search_semantic_group,
	 a.brand,
	 a.retailer
into #search_semantic_match 
from #search_semantic_match_v1 a
group by 
a.search_term_key, 
	 a.search_term, 
	 a.search_pattern_id, 
	 a.search_pattern,
	 a.search_semantic_keyword, 
--	 a.search_semantic_keyword_english,
	 a.search_semantic_group,
	 a.brand,
	 a.retailer
	;
--(68 rows affected)



begin try drop table #temp_search_event end try begin catch end catch;

select p.Record_ID, 
	p.panelist_key,
	'web' as digital_type,
	d.domain_name as property_name,
	 m.search_term_key, 
	 m.search_term, 
	 m.search_pattern_id, 
	 m.search_pattern,
	 m.search_semantic_keyword, 
--	 m.search_semantic_keyword_english,
	 m.search_semantic_group,
	 m.brand,
	 m.retailer
into #temp_search_event
from run.web_events p
inner join ref.domain_name d
	on p.domain_name_key = d.domain_name_key
inner join ref.search_term s
	on p.search_term_key = s.search_term_key
inner join rps.sequence_event e 
	on  p.record_id = e.record_id
inner join #search_semantic_match m on s.search_term_key = m.search_term_key --- just ecosystem searches
;
--(281 rows affected)


insert into #temp_search_event
select 
	p.Record_ID,  
	p.panelist_key,
	'app' as digital_type,
	d.app_name as property_name,
	m.search_term_key, 
	 m.search_term, 
	 m.search_pattern_id, 
	 m.search_pattern,
	 m.search_semantic_keyword, 
--	 m.search_semantic_keyword_english,
	 m.search_semantic_group,
	 m.brand,
	 m.retailer
from run.app_events p
inner join ref.app_name d
	on p.app_name_key = d.app_name_key
inner join ref.search_term s
	on p.search_term_key = s.search_term_key
inner join #search_semantic_match_V1 m on s.search_term_key = m.search_term_key --- just ecosystem searches
;
--(0 rows affected)

-- NOT NECESSARY EVERY TIME THIS SCRIPT IS RUN -- ONLY FOR QA
--------------------------------------------------------------------------------------------------------------------------------------------------
-- *** check what websites panelists went to in 1 min after they searched for the keyword to find if more urls could be matched to white list;
--------------------------------------------------------------------------------------------------------------------------------------------------
-- select top 1000* from run.sequence_event;
-- select top 1000* from ref.url;
-- select top 1000 * from #search_url;

/**
begin try drop table #search_url end try begin catch end catch;

select a.*, b.domain_name,b.URL 
into #search_url
from run.sequence_event a 
inner join ref.url b
on a.Value = b.URL_Key
order by Date_ID; 

-- select top 1000* from run.sequence_event;
-- select top 1000* from #search_semantic_match;
-- select * from #search_events;

begin try drop table #search_events end try begin catch end catch;

select a.Search_Term_Key, Search_Term, search_pattern, search_semantic_group, search_semantic_keyword, Record_ID, Panelist_Key, Start_Time, Date_ID, Platform_ID
into #search_events
from #search_semantic_match a
inner join run.sequence_event b
on a.search_term_key = b.Search_Term_Key
order by Record_ID;

-- select * from ref.URL where URL_Key = 481;

begin try drop table #search_inference_candidates end try begin catch end catch;

select a.Search_Term_Key, a.Search_Term, a.Record_ID, a.Panelist_Key, a.Start_Time, a.Date_ID, b.Start_Time as start_time_b, DATEDIFF(second, a.start_time, b.Start_Time) AS time_elapsed_s,
b.Value as url_key,b.DOMAIN_NAME,b.URL, a.search_semantic_keyword, a.search_semantic_group
into #search_inference_candidates
from #search_events a
inner join #search_url b
on a.Panelist_Key = b.Panelist_Key and a.Platform_ID = b.Platform_ID and a.Date_ID = b.Date_ID
AND (DATEDIFF(SECOND, a.start_time, b.Start_Time) >= 0)     -- seach precedes event
AND (DATEDIFF(SECOND, a.start_time, b.Start_Time) <= 60)    -- 60 sec (1 min) analyzed.
;

select url_key, DOMAIN_NAME, url, search_semantic_keyword, search_semantic_group from #search_inference_candidates 
where domain_name not like 'google%' AND domain_name not like'bing%' AND domain_name not like 'yahoo%'
order by panelist_key, start_time, start_time_b;



**/



----------------------------------------------------------------------------------------------------------------------------------------

-- 4) PATTERN MATCH TO YOUTUBE VIDEOS
-------------------------------------------

-- CREATE MEDIA GRA FILE
-------------------------
--select top 100 * from #video_channel_title_gra;
-- drop table #video_channel_title_gra;
select media_id, title, channel_id, b.category, description, core.dbo.alphanum(lower(concat(channel_id,' ',title))) as video_channel_title_gra 
into #video_channel_title_gra
from [PROJECTNAME].[wrk].[Youtube_Media_Details] a
join [PROJECTNAME].[wrk].[Youtube_Cats] b
on a.category=b.category_id;


-- select top 100* from ProjectName.wrk.youtube_media_details
-- select top 1000* from #video_channel_Title_gra;


create index v1 on #video_channel_title_gra (media_id);

-- drop table #pattern_table_projectname;
select pattern_id, pattern, run_on_white, run_on_green, semantic_lemma, semantic_priority_id, semantic_priority_group
into #pattern_table_projectname
from [PROJECTNAME].ref.pattern_table_projectname  a
where (run_on_green=1 or semantic_priority_group='Remove')
;


-- select count(*) from #video_green_bestpattern;
-- drop table #video_green_bestpattern;
select a.media_id, a.category, min(b.pattern_id) as best_match_pattern
into #video_green_bestpattern
from #video_channel_title_gra a
inner join #pattern_table_projectname b
	on a.video_channel_title_gra like b.pattern
	and b.run_on_green=1
group by a.media_id, a.category;


-- drop table #video_ecosystem_additions;
select c.run_on_white, c.run_on_green, a.media_id, a.title, channel_id, description, a.video_channel_title_gra,
	c.pattern, c.semantic_lemma, a.category, c.priority_group_id, c.semantic_priority_group
into #video_ecosystem_additions
from #video_channel_title_gra a
inner join #video_green_bestpattern b
	on a.media_id=b.media_id
inner join #pattern_table_projectname c
	on b.best_match_pattern=c.pattern_id;


select  media_id, concat('www.youtube.com/watch?v=',media_id) as media_link,
	title, channel_id, description, semantic_lemma, category, semantic_priority_group
from #video_ecosystem_additions a
where semantic_lemma not in ('')
order by semantic_priority_group, semantic_lemma, category, channel_id, title, description;

-- END OF FILE --