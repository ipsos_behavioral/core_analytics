--------------
-- Semantic Match for shopper product: NO BONUS WORDS
-- Scipt 04
-------------
-- Owner
-- DATE
-------------

use PROJECTNAME;


-------------ref.shopper_product
-- select top 100 * from ref.shopper_product;
-- select count(*) from ref.shopper_product;
begin try drop table #shopper_product_gra end try begin catch end catch;

select *, core.dbo.alphanum(shopper_product_name) as shopper_product_gra
into #shopper_product_gra
from ref.shopper_product
;
--(XX rows affected)


-- select * from #shopper_product_Gra;

-- select * from #shopper_product_gra_best_pattern;
begin try drop table #shopper_product_gra_best_pattern end try begin catch end catch;

select shopper_product_key, min(pattern_id) as best_pattern_id
into #shopper_product_gra_best_pattern
from #shopper_product_gra a
inner join ref.pattern_table b
	on a.shopper_product_gra like b.pattern
	and (run_on_green=1 or semantic_priority_group='Remove')
group by shopper_product_key;
-- (XX rows affected)


begin try drop table #shopper_product_semantic_match end try begin catch end catch;

select a.shopper_product_key, a.shopper_product_name, c.pattern_id as shopper_product_pattern_id, c.pattern as shopper_product_pattern,
	c.semantic_priority_group as shopper_product_semantic_group, c.semantic_lemma as shopper_product_semantic_keyword
	--c.semantic_lemma_english as shopper_product_Semantic_keyword_english
into #shopper_product_semantic_match
from #shopper_product_gra a
inner join #shopper_product_gra_best_pattern b
	on a.shopper_product_key=b.shopper_product_key
inner join ref.pattern_table c
	on b.best_pattern_id=c.pattern_id;
--(XX rows affected)


delete from #shopper_product_semantic_match where shopper_product_semantic_group='Remove'; 
--(XX rows affected)


-- Does initial group by to cut down on time
begin try drop table #unified_data end try begin catch end catch;
select record_id, shopper_product_Hash, retailer
into #unified_data
from raw.unified_shop_events
group by record_id, shopper_product_Hash, retailer;
--(XX rows affected)

begin try drop table #tax_eco end try begin catch end catch;
select bdg_display, category, subcategory
into #tax_eco
from ref.Taxonomy_Ecosystem
group by bdg_display, category, subcategory;
--(XX rows affected)
-- select distinct(vertical), vertical_id from googlesearchintent2020.ref.taxonomy_Ecosystem;

-- select top 100 * from raw.unified_shop_events where shopper_product_hash = -4176107705096201773;
--select shopper_product_Hash, count(*) as count from raw.unified_shop_events group by shopper_product_Hash;
-- select distinct package_name  from raw.unified_shop_events;

begin try drop table #domain_data end try begin catch end catch;
select c.record_id, a.shopper_product_key, a.shopper_product_name, a.shopper_product_semantic_keyword, a.shopper_product_semantic_group, c.retailer as property_name,d.category as domain_category,d.subcategory as domain_subcategory
into #domain_data
from #shopper_product_semantic_match a
join #unified_data c on a.shopper_product_hash = c.shopper_product_hash -- inner join since if not a match then entry in ref.shopper_product is for invalidated date
left join #tax_eco d on c.retailer = d.bdg_display
group by c.record_id,a.shopper_product_key, a.shopper_product_name, a.shopper_product_semantic_keyword, a.shopper_product_semantic_group, c.retailer,d.category,d.subcategory
;
--(XX rows affected)
-- select * from #shopper_product_semantic_match where shopper_product_key = 31819;


---*********
--EXPORT
--*********

select *
from #domain_data a
order by shopper_product_semantic_keyword
;


-- select * from googlesearchintent2020.raw.unified_shop_events where shopper_product_key=3096;
-- select * from googlesearchintent2020.ref.shopper_product where shopper_product_key=3096;
-- select top 100* from googlesearchintent2020.ref.shopper_product;
-- select top 100* from googlesearchintent2020.run.shop_events where shopper_product_key=3096;;