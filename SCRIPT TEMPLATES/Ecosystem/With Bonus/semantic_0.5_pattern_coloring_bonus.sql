﻿----------------------
--Prep work for ecosystem: With BONUS
--Includes pattern loads, property color assignment, and color plan
---------------------
-------------EXAMPLE EXCEL TEMPLATES ARE IN ECOSYSTEM_SET_UP_TEMPLATE in
------PROJECTS>1B)ANALYTICS DELIVERY TEMPLATE> ECOSYSTEM_SET_UP
Use PROJECTNAME;

-----------------------------Pattern Table
begin try drop table #pattern_import end try begin catch end catch;
create table #pattern_import(
	run_on_white	int,
	run_on_green	int,
	pattern			nvarchar(300),
	semantic_lemma	nvarchar(300),
--	semantic_lemma_english	nvarchar(300), -- COMMENT OUT IF NOT NEEDED 
	semantic_priority_id	int,
	semantic_priority_group	nvarchar(300)
);
---------------------INSERT IN FROM EXCEL

begin try drop table ref.pattern_table end try begin catch end catch;
create table ref.pattern_table (
	pattern_id bigint identity,
	run_on_white	int,
	run_on_green	int,
	pattern			nvarchar(300),
	semantic_lemma	nvarchar(300),
--	semantic_lemma_english	nvarchar(300), -- COMMENT OUT IF NOT NEEDED 
	semantic_priority_id	int,
	semantic_priority_group	nvarchar(300)
);

-- First 4 digits of pattern_id is the semantic_priority_id. Last 7 digits is hash of pattern
--SET IDENTITY_INSERT ref.general_retailer OFF;
SET IDENTITY_INSERT ref.pattern_table ON;

insert into ref.pattern_table (
	pattern_id, 	
	run_on_white,	
	run_on_green,	
	pattern	,		
	semantic_lemma,
--	semantic_lemma_english, -- COMMENT OUT IF NOT NEEDED 
	semantic_priority_id,
	semantic_priority_group
)
	select convert(bigint,semantic_priority_id)*10000000+abs(convert(bigint,hashbytes('SHA1',pattern)))%10000000 as pattern_id,
	run_on_white,	
	run_on_green,	
	pattern	,		
	semantic_lemma,
--	semantic_lemma_english, -- COMMENT OUT IF NOT NEEDED 
	semantic_priority_id,
	semantic_priority_group
from #pattern_import
;

--(XX rows affected)

create index pattern_table1 on ref.pattern_table(pattern_id);
create index pattern_table2 on ref.pattern_table(semantic_lemma);
--create index pattern_table3 on ref.pattern_table (semantic_lemma_english);
create index pattern_table4 on ref.pattern_table(pattern);
create index pattern_table5 on ref.pattern_table(semantic_priority_id);


----------------------Bonus Table Load

begin try drop table #bonus_load end try begin catch end catch;
create table #bonus_load(
	bonus_pattern			nvarchar(300),
	bonus_lemma	nvarchar(300),
	bonus_priority_id	int,
	bonus_priority_group	nvarchar(300)
)
;
---------------Insert in from excel


begin try drop table ref.bonus_pattern_table end try begin catch end catch;
create table ref.bonus_pattern_table (
	bonus_pattern_id bigint identity,
	bonus_pattern			nvarchar(300),
	bonus_lemma	nvarchar(300),
	bonus_priority_id	int,
	bonus_priority_group	nvarchar(300)
);
-- First 4 digits of pattern_id is the semantic_priority_id. Last 7 digits is hash of pattern
SET IDENTITY_INSERT ref.pattern_table OFF;
SET IDENTITY_INSERT ref.bonus_pattern_table ON;

insert into ref.bonus_pattern_table (
	bonus_pattern_id,bonus_pattern, bonus_lemma,
	bonus_priority_id,
	bonus_priority_group
)
	select convert(bigint,bonus_priority_id)*10000000+abs(convert(bigint,hashbytes('SHA1',bonus_pattern)))%10000000 as bonus_pattern_id,
	bonus_pattern, bonus_lemma,
	bonus_priority_id,
	bonus_priority_group
from #bonus_load
;
--(XX rows affected)

create index bonus_pattern_table1 on ref.bonus_pattern_table (bonus_pattern_id);
create index bonus_pattern_table2 on ref.bonus_pattern_table (bonus_lemma);
create index bonus_pattern_table4 on ref.bonus_pattern_table (bonus_pattern);
create index bonus_pattern_table5 on ref.bonus_pattern_table (bonus_priority_id);


----------------------Color Assignment
begin try drop table ref.eco_color_assignment end try begin catch end catch;
create table ref.eco_color_assignment (
digital_type_id int,
property_key int,
property_hash bigint,
property_name varchar(300),
new_prop_color varchar(300),
assignment_type varchar(300) --- 'suggested', 'discovered', 'requested'
)
;
--------------------INSERT IN FROM EXCEL




----------------------------------------Color Plan
begin try drop table ref.color_plan end try begin catch end catch;
create table ref.color_plan (
categorization_source varchar(300),
supercategory varchar(300),
category varchar(300),
subcategory varchar(300),
vertical varchar(300),
color varchar(300),
)
;
--------------------INSERT IN FROM EXCEL