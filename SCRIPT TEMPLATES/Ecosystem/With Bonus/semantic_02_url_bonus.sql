---------------------------------------------------------------------------------------------------
-- SEMANTIC URL MATCHING TO PATTERN: With BONUS 
-- SCRIPT 02
---------------------------------------------------------------------------------------------------
-- OWNER
-- DATE
---------------------------------------------------------------------------------------------------

use [PROJECTNAME];

/*
AFTER PROJECT SPECIFIC CORE IS LOCKED DOWN FIND AND REPLACE 
Core.ref.taxonomy_Common_properties with the new table
*/


-- ================================================================================================
-- 1) EXPAND AND ANALYZE SOME (NOT ALL) WHITE DOMAINS
-- ================================================================================================
-- For white domains, we already understand them to be 95-100% relevant. So this URL QA step isn't
-- for the revelance, but output to be apply semantic context for content classigifation. Example:
-- Identify if a WebMD page is related to a Serious/Complex topic or something general.
---------------------------------------------------------------------------------------------------
-- select * from #whitedomain order by domain_name;
-- select top 100 * from ref.domain_color;
begin try drop table #whitedomain end try begin catch end catch;
select a.Domain_Name_Key, a.Domain_Name, a.semantic_lemma,  a.semantic_priority_group, request_flag, 
	a.common_supercategory, a.common_category, a.common_Vertical
into #whitedomain
from ref.domain_color a
where prop_color='white';
create index var1 on #whitedomain (domain_name_key);
create index var2 on #whitedomain (domain_name);
-- (XX rows affected)

-- select * from #whiteapp order by app_name;
begin try drop table #whiteapp end try begin catch end catch;
select a.app_name_key, cast(a.app_name as varchar(900)) as app_name, a.semantic_lemma, 
a.semantic_priority_group, request_flag,
a.common_supercategory, a.common_category, a.common_Vertical
into #whiteapp
from ref.app_color a
where prop_color='white';
create index var1 on #whiteapp (app_name_key);
create index var2 on #whiteapp (app_name);
-- (XX rows affected)


begin try drop table #whitedomain_url_best_pattern end try begin catch end catch;
select a.url_key, b.domain_name_key, min(isnull(c.pattern_id,0)) as best_content_pattern_id
into #whitedomain_url_best_pattern
from ref.url a
inner join #whitedomain b
	on a.domain_name=b.domain_name
left join ref.pattern_table c
	on a.url_gra like c.pattern
	and b.semantic_lemma <> c.semantic_lemma -- semantic keyword join is on something other than domain.
	and c.run_on_white=1
group by a.url_key, b.domain_name_key;
-- (XX rows affected)



begin try drop table #whitedomain_url_content_detail_1 end try begin catch end catch;
select a.*, b.domain_name_key, c.pattern_id, c.pattern, c.semantic_priority_group,
c.semantic_lemma
into #whitedomain_url_content_detail_1
from ref.url a
inner join #whitedomain_url_best_pattern b
	on a.URL_key=b.URL_key
inner join ref.pattern_table c
	on b.best_content_pattern_id=c.pattern_id
;
--(XX rows affected)

--------Extra de-dupe just in case
begin try drop table #whitedomain_url_content_detail end try begin catch end catch;
select url_key, domain_name_key, domain_name, url, semantic_lemma, semantic_priority_group
into #whitedomain_url_content_detail
from #whitedomain_url_content_detail_1
group by url_key, domain_name_key, domain_name, url, semantic_lemma, semantic_priority_group;
--(XX rows affected)


	-- select * from #whitedomain_url_content_detail where url_key=856005;
-- select * from #whitedomain_url_content_detail where domain_name = 'toneitup.com'; (599 rows affected)

-- ##################################################################
-- EXPORT: WHITE DOMAIN URLS FOR REVIEW (THIS IS ONLY A SUBSET OF URLS IN WHITE DOMAINS)
-- Note that semantic keyword join is on something other than domain.
-- ##################################################################

select url_key, domain_name_key, domain_name, url, semantic_lemma, semantic_priority_group, '' as accuracy, '' as note
from #whitedomain_url_content_detail
where semantic_priority_Group <> 'remove'
order by domain_name, semantic_priority_group, semantic_lemma, url;



-- ================================================================================================
-- 2) ANALYZE GREEN DOMAIN URLS
-- ================================================================================================

-- CHECK TO SEE WHERE THE MAJORITY OF THE DATA IS COMING FROM IF QUERY TAKING A WHILE
/**
select a.domain_name, count(*) as ct_records
from ref.domain_color a
inner join GoogleSearchIntent2020.ref.url b
       on a.domain_name=b.domain_name
	   and a.prop_color='green'
group by a.domain_name
order by ct_records desc;
	**/


-- Note: This first step identify all URLs elgible in green domains

begin try drop table #greendomain_all_urls end try begin catch end catch;
select a.url_key, a.url_gra, b.domain_name_key, b.common_supercategory, b.common_category, b.common_Vertical
into #greendomain_all_urls
from ref.url a
inner join ref.domain_color b
       on a.domain_name=b.domain_name
	   and b.prop_color='green'
group by a.URL_Key, a.url_Gra, b.domain_name_key, b.common_supercategory, b.common_category, b.common_Vertical;
-- select * from  #greendomain_all_urls where domain_name_key = 968
--(XX rows affected)




-----------------------------
-- REMOVE COMMON FALSE POSITIVES BEFORE THEY ARE PROCESSED FURTHER
-- such as Search, Ad tracking, Clicks, and redirects.
-- Search is addressed in a separate process. And we don't want the others.
-----------------------------

---- remove google ad tracking, clicks, and redirects
delete from #greendomain_all_urls where url_gra like ('%google_com%com%');
-- (XX rows affected)

delete from #greendomain_all_urls where url_gra like ('%adservice%');
-- (XX rows affected)

delete from #greendomain_all_urls where url_gra like ('%search_yahoo%');
-- (XX rows affected)



-- select * from #greendomain_url_best_pattern;
-- Note: This checks each eligible URL for semantic matching, then identifies best match based on priority. Tables about 3 minutes to step through 1MM URLs.

---Gets bonus match
begin try drop table #greendomain_bonus_match end try begin catch end catch;
select a.URL_Key, a.domain_name_key,common_supercategory,common_category, common_Vertical, a.url_gra, min(bonus_pattern_id) as best_bonus_pattern
into #greendomain_bonus_match
from #greendomain_all_urls a 
join ref.bonus_pattern_table b
on a.url_gra like b.bonus_pattern
group by a.URL_Key, a.domain_name_key,common_supercategory,common_category, common_Vertical, a.url_gra;
--(XX rows affected)
-- select top 100* from googlesearchintent2020.ref.url;


begin try drop table #greendomain_best_bonus_match end try begin catch end catch;
select a.URL_Key, a.domain_name_key,common_supercategory,common_category, common_Vertical,a.url_gra, b.bonus_lemma as bonus_word, b.bonus_priority_group
into #greendomain_best_bonus_match
from #greendomain_bonus_match a 
join ref.bonus_pattern_table b
on a.best_bonus_pattern = b.bonus_pattern_id
group by a.URL_Key, a.domain_name_key,common_supercategory,common_category, common_Vertical, a.url_gra, b.bonus_lemma, b.bonus_priority_group
;
--(XX rows affected)


begin try drop table #greendomain_url_best_pattern end try begin catch end catch;
select a.url_key, a.domain_name_key, common_supercategory,common_category, common_Vertical, min(pattern_id) as best_pattern
into #greendomain_url_best_pattern
from #greendomain_all_urls a
inner join ref.pattern_table b
	on a.url_gra like b.pattern
	and (b.run_on_green=1 or b.semantic_priority_group = 'Remove')
group by a.url_key, a.domain_name_key, common_supercategory,common_category, common_Vertical;
-- (XX rows affected)

-- select top 100 * from #greendomain_url_semantic_match;
begin try drop table #greendomain_url_semantic_match_v1 end try begin catch end catch;
select a.*, b.domain_name_key, c.pattern_id, c.pattern, c.semantic_priority_group,c.semantic_lemma, 
d.bonus_word, d.bonus_priority_group, b.common_supercategory,b.common_category, b.common_Vertical
into #greendomain_url_semantic_match_v1
from ref.url a
inner join #greendomain_url_best_pattern b
	on a.URL_key=b.URL_key
inner join ref.pattern_table c
	on b.best_pattern=c.pattern_id
	and c.semantic_priority_group <> 'Remove' -- excludes false postives
left join #greendomain_best_bonus_match d
	on a.URL_Key = d.URL_Key
;
-- (XX rows affected)





--------For words with a bonus word, it runs the ambiguous words
begin try drop table #greendomain_url_best_pattern_ambiguous end try begin catch end catch;
select a.url_key, a.domain_name_key, common_supercategory,common_category, common_Vertical,bonus_word,bonus_priority_group,min(pattern_id) as best_pattern
into #greendomain_url_best_pattern_ambiguous
from #greendomain_best_bonus_match  a
inner join ref.pattern_table b
	on a.url_gra like b.pattern
	and (b.run_on_green=0 or b.semantic_priority_group = 'Remove')
group by a.url_key, a.domain_name_key,common_supercategory,common_category, common_Vertical,bonus_word,bonus_priority_group;
--(XX rows affected)



-------Similar to with the unambiguous words
-- select * from #greendomain_url_semantic_match_ambiguous ;
begin try drop table #greendomain_url_semantic_match_ambiguous end try begin catch end catch;
select a.*, b.domain_name_key, c.pattern_id, c.pattern, c.semantic_priority_group, c.semantic_lemma, b.bonus_word, b.bonus_priority_group ,common_supercategory,common_category, common_Vertical
into #greendomain_url_semantic_match_ambiguous
from ref.url a
inner join #greendomain_url_best_pattern_ambiguous b
	on a.URL_key=b.URL_key
inner join ref.pattern_table c
	on b.best_pattern=c.pattern_id
	and c.semantic_priority_group <> 'Remove' -- excludes false postives
;
--(XX rows affected)

-----This inserts in the matches from the ambiguous one
--It creates dupes if the url matched on both ambiguous and unambiguous words, so next step will choose the lower priority word
insert into #greendomain_url_semantic_match_v1
select * from #greendomain_url_semantic_match_ambiguous;
--(XX rows affected)

begin try drop table #true_best_pattern end try begin catch end catch;
select url_key, min(pattern_id) as true_best_pattern
into #true_best_pattern
from #greendomain_url_semantic_match_v1
group by url_key;
--(XX rows affected)


-- select top 100 * from #greendomain_url_semantic_match_v2;
begin try drop table #greendomain_url_semantic_match_v2 end try begin catch end catch;
select a.*
into #greendomain_url_semantic_match_v2
from #greendomain_url_semantic_match_v1 a
join #true_best_pattern b on a.url_key = b.url_key and a.pattern_id = b.true_best_pattern;
--(XX rows affected)

------Final repass
begin try drop table #greendomain_url_best_pattern_repass end try begin catch end catch;
select a.url_key, a.domain_name_key, a.common_supercategory,a.common_category, a.common_Vertical,a.bonus_word,a.bonus_priority_group,min(b.pattern_id) as best_pattern
into #greendomain_url_best_pattern_repass
from #greendomain_url_semantic_match_v2  a
inner join ref.pattern_table b
	on a.url_gra like b.pattern -- no requirements about ambiguity since want actual lowest
group by a.url_key, a.domain_name_key,a.common_supercategory,a.common_category, a.common_Vertical,a.bonus_word,a.bonus_priority_group;
--(XX rows affected)

begin try drop table #greendomain_url_semantic_match end try begin catch end catch;
select a.*, b.domain_name_key, c.pattern_id, c.pattern, c.semantic_priority_group,c.semantic_lemma, 
d.bonus_word, d.bonus_priority_group, b.common_supercategory,b.common_category, b.common_Vertical
into #greendomain_url_semantic_match
from ref.url a
inner join #greendomain_url_best_pattern_repass b
	on a.URL_key=b.URL_key
inner join ref.pattern_table c
	on b.best_pattern=c.pattern_id
	and c.semantic_priority_group <> 'Remove' -- excludes false postives
	and c.run_on_white = 1 --- if run on white = 0 then it is a run on search only word
left join #greendomain_best_bonus_match d
	on a.URL_Key = d.URL_Key
;
--(XX rows affected)



-- ##################################################################
-- EXPORT: GREEN DOMAIN URLS FOR REVIEW
-- ##################################################################
-- drop table nestle2019.dbo.greendomain_url_semantic_match;

select url_key, domain_name,domain_name_key, url, semantic_lemma, semantic_priority_group,  
bonus_word, bonus_priority_group, '' as accuracy, '' as note
from #greendomain_url_semantic_match
where semantic_priority_group <> 'remove'
order by domain_name, semantic_priority_group, semantic_lemma,  url;
-- XX rows affected


-- select * from #greendomain_url_semantic_match where domain_name='christmastreeshops.com';

-- ================================================================================================
-- 3) INCIDENCE RATE CHECKS FOR URLS BEING CONSIDERED FOR CONSOLIDATION FILE. FOR QA
-- ================================================================================================
--------
-- White Domains
----------------------------------------------------------------
-- 3A) White Domains All URLS event records (Not just ones in QA)
----------------------------------------------------------------

-- select * from #whitedomain;
-- select * from #whitedomain_url_records order by Panelist_Key,Record_ID;
-- select count(distinct panelist_key) as distinct_panelist_count from #whitedomain_url_records; 
--select top 100 * from #whitedomain;
begin try drop table #whitedomain_url_records end try begin catch end catch;

select b.panelist_key,
	b.Record_ID, 
	a.domain_name_key,
	a.domain_name, 
	a.common_supercategory,
	a.common_category,
	a.common_vertical,
	a.semantic_lemma,
	a.semantic_priority_group,
	b.tax_id,
	b.digital_type_id,
	1 as ct_record
into #whitedomain_url_records
from #whitedomain a
left join rps.sequence_event b
	on a.domain_name_key = b.Notes
where b.digital_type_id=235
	;
-- (XX rows affected)

begin try drop table #white_app_records end try begin catch end catch;

select b.panelist_key,
	b.Record_ID, 
	a.app_name_key,
	a.app_name, 
	a.common_supercategory,
	a.common_category,
	a.common_vertical,
	a.semantic_lemma,
	a.semantic_priority_group,
	b.tax_id,
	b.digital_type_id,
	1 as ct_record
into #white_app_records
from #whiteapp a
left join rps.sequence_event b
	on a.app_name_key = b.Notes
where b.digital_type_id=100
	;
--(XX rows affected)

----------------------------------------------------------------
-- 3B) White domains count reach
----------------------------------------------------------------
-- by domain name 
-- select * from #whitedomain_overall_counts order by panelist_count desc;

begin try drop table #whitedomain_overall_counts end try begin catch end catch;

select domain_name_key, domain_name,
	count(distinct panelist_key) as panelist_count, 
	sum(ct_record) as record_count
into #whitedomain_overall_counts
from #whitedomain_url_records
group by domain_name_key, domain_name
;
-- (XX rows affected)

begin try drop table #whiteapp_overall_counts end try begin catch end catch;

select app_name_key, app_name,
	count(distinct panelist_key) as panelist_count, 
	sum(ct_record) as record_count
into #whiteapp_overall_counts
from #white_app_records
group by app_name_key, app_name
;
-- (XX rows affected)

----------------------------------------------------------------
-- 3B pt 2) Brand/Retailer Pattern
----------------------------------------------------------------


begin try drop table #brand_retailer_pattern end try begin catch end catch;
select pattern_id, run_on_white, run_on_green, pattern, semantic_lemma,
semantic_priority_id, semantic_priority_group
into #brand_retailer_pattern 
from ref.pattern_table
where semantic_priority_group like '%brand%' OR semantic_priority_group like '%retailer%'
;
--(18 rows affected)

set identity_insert #brand_retailer_pattern on;
insert into #brand_retailer_pattern  (pattern_id, run_on_white, run_on_green, pattern, semantic_lemma,semantic_priority_id, semantic_priority_group)
select pattern_id,run_on_white, run_on_green, pattern, semantic_lemma,
semantic_priority_id, semantic_priority_group
from core.ref.general_retailer_pattern;
--(31 rows affected)

begin try drop table #white_domain_brand_retailer end try begin catch end catch;
select a.domain_name_key,min(b.pattern_id) as best_pattern
into #white_domain_brand_retailer
from ref.domain_color  a
inner join #brand_retailer_pattern b
	on a.domain_name_gra like b.pattern -- no requirements about ambiguity since want actual lowest
inner join #whitedomain_overall_counts c on a.domain_name_key = c.domain_name_key
group by a.domain_name_key;
--(11 rows affected)

begin try drop table #white_app_brand_retailer end try begin catch end catch;
select a.app_name_key,min(b.pattern_id) as best_pattern
into #white_app_brand_retailer 
from ref.app_color  a
inner join #brand_retailer_pattern b
	on a.app_name_gra like b.pattern -- no requirements about ambiguity since want actual lowest
inner join #whitedomain_overall_counts c on a.app_name_key = c.domain_name_key
group by a.app_name_key;
--(0 rows affected)

-- ##################################################################
-- 3C) EXPORT: whitelist domains & apps
-- ##################################################################

--- EXPORT TO DIVIDE&CONQUER.CORETABLEUPDATES FOR CORE PROPERTY TABLE UPDATES

-------------------US Based
select distinct(a.domain_name_key), a.domain_name, b.semantic_lemma,b.semantic_priority_group,
isnull(e.pattern,'') as brand_retailer_pattern, isnull(e.semantic_lemma,'') as brand_retailer_lemma, isnull(e.semantic_priority_group,'') as brand_retailer_group,
b.request_flag, panelist_count, record_count, b.prop_color as bdg_color, 
'' as domain_category, '' as domain_subcategory, 
 b.step_assigned,
 case when eco.property_name is not null then eco.ecosystem_category else '' end as prev_eco_category,
 case when eco.property_name is not null then eco.ecosystem_subcategory else '' end as prev_eco_subcategory,
 case when eco.property_name is not null then eco.categorization_source else '' end as prev_eco_source,
c.common_supercategory, c.common_category, c.common_subcategory,c.common_vertical,c.project_m_reach,c.property_hash,c.rps_flag, c.rpt_flag, c.vetted_flag
from #whitedomain_overall_counts a
inner join ref.domain_color b
	on a.domain_name=b.domain_name
left outer join Core.ref.taxonomy_Common_properties c
	on a.domain_name=c.property_name
	and c.digital_type_id = '235'
left join #white_domain_brand_retailer d 
	on a.domain_name_key = d.domain_name_key
left join #brand_retailer_pattern  e
	on d.best_pattern = e.pattern_id
left join core.ref.Ecosystem_Classifications eco
	on a.domain_name = eco.property_name
	and eco.digital_type_id = 235
order by panelist_count desc, record_count desc; -- order here
-- XX rows affected


-------------International
select distinct(a.domain_name_key), a.domain_name, b.semantic_lemma,b.semantic_priority_group, 
isnull(e.pattern,'') as brand_retailer_pattern, isnull(e.semantic_lemma,'') as brand_retailer_lemma, isnull(e.semantic_priority_group,'') as brand_retailer_group,
b.request_flag, panelist_count, record_count, b.prop_color as bdg_color, 
'' as domain_category, '' as domain_subcategory, 
 b.step_assigned,
 case when eco.property_name is not null then eco.ecosystem_category when eco.property_name is null and ctry.property_name is not null then ctry.ecosystem_category else '' end as prev_eco_category,
 case when eco.property_name is not null then eco.ecosystem_subcategory when eco.property_name is null and ctry.property_name is not null then ctry.ecosystem_subcategory else '' end as prev_eco_subcategory,
 case when eco.property_name is not null then eco.categorization_source when eco.property_name is null and ctry.property_name is not null then ctry.categorization_source else '' end as prev_eco_source,
 case when eco.property_name is not null then 'Exact Match' when eco.property_name is null and ctry.property_name is not null then 'Ctry Code Removed' else '' end as prev_eco_match_type,
c.common_supercategory, c.common_category, c.common_subcategory,c.common_vertical,c.project_m_reach,c.property_hash,c.rps_flag, c.rpt_flag, c.vetted_flag
from #whitedomain_overall_counts a
inner join ref.domain_color b
	on a.domain_name=b.domain_name
left outer join Core.ref.taxonomy_Common_properties c
	on a.domain_name=c.property_name
	and c.digital_type_id = '235'
left join #white_domain_brand_retailer d 
	on a.domain_name_key = d.domain_name_key
left join #brand_retailer_pattern e
	on d.best_pattern = e.pattern_id
left join core.ref.Ecosystem_Classifications eco
	on a.domain_name = eco.property_name
	and eco.digital_type_id = 235
left join core.ref.Ecosystem_Classifications ctry
	on replace(REPLACE(a.domain_name,'.COUNTRYCODE',''),'.COUNTRYCODE','')  = ctry.property_name 
	and ctry.digital_type_id = 235
	--- IF FOR MEXICO AND domains are amazon.com.mx, remove .mx and matches on US version
order by panelist_count desc, record_count desc; -- order here
-- XX rows affected


--- EXPORT TO DIVIDE&CONQUER.CORETABLEUPDATES FOR CORE PROPERTY TABLE UPDATES

select b.app_name_key, b.app_name, b.semantic_lemma,b.semantic_priority_group,
isnull(e.pattern,'') as brand_retailer_pattern, isnull(e.semantic_lemma,'') as brand_retailer_lemma, isnull(e.semantic_priority_group,'') as brand_retailer_group,
b.request_flag, panelist_count, record_count, b.prop_color as bdg_color, 
'' as app_category, '' as app_subcategory,
b.step_assigned,
 case when eco.property_name is not null then eco.ecosystem_category  else '' end as prev_eco_category,
 case when eco.property_name is not null then eco.ecosystem_subcategory else '' end as prev_eco_subcategory,
 case when eco.property_name is not null then eco.categorization_source else '' end as prev_eco_source,
 case when eco.property_name is not null then 'Exact Match'  else '' end as prev_eco_match_type, 
c.common_supercategory, c.common_category, c.common_subcategory,c.common_vertical, c.project_m_reach,c.property_hash,c.rps_flag, c.rpt_flag, c.vetted_flag
from #whiteapp_overall_counts a
inner join ref.app_color b
	on a.app_name_key = b.app_name_key
left outer join Core.ref.taxonomy_Common_properties c
	on b.app_name=c.property_name
	and c.digital_type_id = '100'
left join #white_app_brand_retailer d 
	on a.app_name_key = d.app_name_key
left join #brand_retailer_pattern  e
	on d.best_pattern = e.pattern_id
left join core.ref.Ecosystem_Classifications eco
	on a.app_name = eco.property_name
	and eco.digital_type_id = 100
where prop_color = 'white'
and c.common_supercategory <>'[remove]'
order by panelist_count desc, record_count desc; -- order here
-- XX rows affected




-- ##########################
-- WHITELIST INCIDENCE RATES
-- ##########################
-- distinct panelists
select count(distinct Panelist_Key) from #whitedomain_url_records; -- XX



-- by category VERTICAL
begin try drop table #whitedomain_vertical end try begin catch end catch;

select common_vertical, 
	count(distinct panelist_key) as panelist_count,
	count(distinct domain_name) as domain_count,
	count(*) as record_count,
	count(distinct semantic_lemma) as semantic_lemma_count,
	count(distinct semantic_priority_group) as semantic_priority_group_count
into #whitedomain_vertical
from #whitedomain_url_records
group by common_vertical;
--(XX rows affected)
select * from #whitedomain_vertical order by panelist_count desc, record_count desc;
-- select * from #whitedomain_url_records where sw_category = 'Career and Education';


-- by category CORE
begin try drop table #whitedomain_categories end try begin catch end catch;

select common_supercategory, common_category,
	count(distinct panelist_key) as panelist_count,
	count(distinct domain_name) as domain_count,
	count(*) as record_count,
	count(distinct semantic_lemma) as semantic_lemma_count,
	count(distinct semantic_priority_group) as semantic_priority_group_count
into #whitedomain_categories 
from #whitedomain_url_records
group by common_supercategory, common_category;
--(XX rows affected)

select * from #whitedomain_categories order by panelist_count desc, record_count desc;


-- by semantic group 
begin try drop table #whitedomain_semanticgroups end try begin catch end catch;

select semantic_priority_group,
	count(distinct panelist_key) as panelist_count, 
	count(distinct domain_name) as domain_count,
	count(*) as record_count,
	count(distinct semantic_lemma) as semantic_lemma_count
into #whitedomain_semanticgroups
from #whitedomain_url_records
group by semantic_priority_group;
--(XX rows affected)

select * from #whitedomain_Semanticgroups order by panelist_count desc, record_count desc;



-- by semantic lemma 
begin try drop table #whitedomain_lemma end try begin catch end catch;

select semantic_lemma, 
	semantic_priority_group,
	count(distinct panelist_key) as panelist_count, 
	count(distinct domain_name) as domain_count,
	count(*) as record_count
into #whitedomain_lemma
from #whitedomain_url_records
group by semantic_lemma, semantic_priority_group;
--(XX rows affected)
select * from #whitedomain_lemma order by panelist_count desc, record_count desc;


-- by domain
begin try drop table #white_domains end try begin catch end catch;

select domain_name,
	count(distinct panelist_key) as panelist_count,
	count(*) as record_count,
	count(distinct semantic_lemma) as semantic_lemma_count,
	count(distinct semantic_priority_group) as semantic_priority_group_count
into #white_domains
from #whitedomain_url_records
group by domain_name;
--(XX rows affected)

select * from #white_domains order by panelist_count desc, record_count desc;



----------------------------------------------------------------
-- 3D) GREEN DOMAIN URL DIAGNOSITICS
----------------------------------------------------------------

-- select * from #greendomain_url_records order by Panelist_Key,Record_ID;
-- select count(distinct panelist_key) as distinct_panelist_count from #greendomain_url_records; 
-- select top 100 * from #greendomain_url_semantic_match

begin try drop table #greendomain_url_records end try begin catch end catch;

select b.panelist_key,
	b.Record_ID, 
	a.domain_name_key,
	a.domain_name, 
	a.common_supercategory,
	a.common_category,
	a.common_vertical,
	a.semantic_lemma,
	a.semantic_priority_group,
	b.digital_type_id,
	1 as ct_record
into #greendomain_url_records
from #greendomain_url_semantic_match a
left join rps.sequence_event b
	on a.url_key = b.Value

	;
-- (XX rows affected)

-- select * from #greendomain_url_records;



----------------------------------------------------------------
-- 3E) Green domains count reach
----------------------------------------------------------------
-- by domain name 
-- select * from #greendomain_overall_counts order by panelist_count desc;
-- select * from #greendomain_url_records;

begin try drop table #greendomain_overall_counts end try begin catch end catch;

select domain_name_key, domain_name, 
	count(distinct panelist_key) as panelist_count, 
	sum(ct_record) as record_count
into #greendomain_overall_counts
from #greendomain_url_records
group by domain_name_key, domain_name
;
--(XX rows affected)

----------------------------------------------------------------
-- 3E pt 2) Brand/Retailer Pattern
----------------------------------------------------------------
begin try drop table #green_domain_brand_retailer end try begin catch end catch;
select a.domain_name_key,min(b.pattern_id) as best_pattern
into #green_domain_brand_retailer
from ref.domain_color  a
inner join #brand_retailer_pattern b
	on a.domain_name_gra like b.pattern -- no requirements about ambiguity since want actual lowest
inner join #greendomain_overall_counts c on a.domain_name_key = c.domain_name_key
group by a.domain_name_key;

-- ##################################################################
-- PERMANENT TABLE FOR CORE CLASSIFICATIONS
-- ##################################################################
/*
begin try drop table ref.eco_domains end try begin catch end catch;
select 'green' as domain_color, domain_name_key, domain_name, panelist_count, record_count
into ref.eco_domains
from #greendomain_overall_counts;
--(XX rows affected)

insert into ref.eco_domains
select 'white' as domain_color, domain_name_key, domain_name, panelist_count, record_count
from #whitedomain_overall_counts;
--(XX rows affected)
*/

-- ##################################################################
-- 3F) EXPORT: greenlist domains tab
-- ##################################################################
-- EXPORT TO DIVIDE&CONQUER.CORETABLEUPDATES FOR CORE PROPERTY TABLE UPDATES
-------------------------US Based
select a.domain_name_key, a.domain_name, 
isnull(e.pattern,'') as brand_retailer_pattern, isnull(e.semantic_lemma,'') as brand_retailer_lemma, isnull(e.semantic_priority_group,'') as brand_retailer_group,
b.request_flag, panelist_count, record_count, b.prop_color as bdg_color, 
'' as domain_category, '' as domain_subcategory, 
b.step_assigned,
case when eco.property_name is not null then eco.ecosystem_category else '' end as prev_eco_category,
 case when eco.property_name is not null then eco.ecosystem_subcategory else '' end as prev_eco_subcategory,
 case when eco.property_name is not null then eco.categorization_source else '' end as prev_eco_source,
c.common_supercategory, c.common_category, c.common_subcategory,c.common_vertical, c.project_m_reach,c.property_hash,c.rps_flag, c.rpt_flag, c.vetted_flag
from #greendomain_overall_counts a
inner join ref.domain_color b
	on a.domain_name=b.domain_name
left outer join Core.ref.taxonomy_Common_properties c
	on a.domain_name=c.property_name
	and c.digital_type_id = '235'
left join #green_domain_brand_retailer d 
	on a.domain_name_key = d.domain_name_key
left join #brand_retailer_pattern e
	on d.best_pattern = e.pattern_id
left join core.ref.Ecosystem_Classifications eco
	on a.domain_name = eco.property_name
	and eco.digital_type_id = 235
where c.common_supercategory <> '[remove]'
order by panelist_count desc,record_count desc;

-------------------------International
select a.domain_name_key, a.domain_name,
isnull(e.pattern,'') as brand_retailer_pattern, isnull(e.semantic_lemma,'') as brand_retailer_lemma, isnull(e.semantic_priority_group,'') as brand_retailer_group,
b.request_flag, panelist_count, record_count, b.prop_color as bdg_color, 
'' as domain_category, '' as domain_subcategory, 
b.step_assigned,
case when eco.property_name is not null then eco.ecosystem_category when eco.property_name is null and ctry.property_name is not null then ctry.ecosystem_category else '' end as prev_eco_category,
 case when eco.property_name is not null then eco.ecosystem_subcategory when eco.property_name is null and ctry.property_name is not null then ctry.ecosystem_subcategory else '' end as prev_eco_subcategory,
 case when eco.property_name is not null then eco.categorization_source when eco.property_name is null and ctry.property_name is not null then ctry.categorization_source else '' end as prev_eco_source,
 case when eco.property_name is not null then 'Exact Match' when eco.property_name is null and ctry.property_name is not null then 'Ctry Code Removed' else '' end as prev_eco_match_type,
c.common_supercategory, c.common_category, c.common_subcategory,c.common_vertical, c.project_m_reach,c.property_hash,c.rps_flag, c.rpt_flag, c.vetted_flag
from #greendomain_overall_counts a
inner join ref.domain_color b
	on a.domain_name=b.domain_name
left outer join Core.ref.taxonomy_Common_properties c
	on a.domain_name=c.property_name
	and c.digital_type_id = '235'
left join #green_domain_brand_retailer d 
	on a.domain_name_key = d.domain_name_key
left join #brand_retailer_pattern  e
	on d.best_pattern = e.pattern_id
left join core.ref.Ecosystem_Classifications eco
	on a.domain_name = eco.property_name
	and eco.digital_type_id = 235
left join core.ref.Ecosystem_Classifications ctry
	on replace(REPLACE(a.domain_name,'.COUNTRYCODE',''),'.COUNTRYCODE','')  = ctry.property_name 
	and ctry.digital_type_id = 235
	--- IF FOR MEXICO AND domains are amazon.com.mx, remove .mx and matches on US version
where c.common_supercategory <> '[remove]'
order by panelist_count desc,record_count desc;
--XX rows affected

-- ##########################
-- GREENLIST INCIDENCE RATES
-- ##########################

-- distinct panelists
select count(distinct Panelist_Key) from #greendomain_url_records; --XX

-- by vertical
begin try drop table #greendomain_vertical end try begin catch end catch;

select 
	common_vertical, 
	count(distinct panelist_key) as panelist_count,
	count(distinct domain_name) as domain_count,
	count(*) as record_count,
	count(distinct semantic_lemma) as semantic_lemma_count,
	count(distinct semantic_priority_group) as semantic_priority_group_count
into #greendomain_vertical
from #greendomain_url_records
group by common_vertical;
--(XX rows affected)

select * from #greendomain_vertical order by panelist_count desc, record_count desc;


-- by category
begin try drop table #greendomain_categories end try begin catch end catch;

select common_supercategory,common_category, 
	count(distinct panelist_key) as panelist_count,
	count(distinct domain_name) as domain_count,
	count(*) as record_count,
	count(distinct semantic_lemma) as semantic_lemma_count,
	count(distinct semantic_priority_group) as semantic_priority_group_count
into #greendomain_categories
from #greendomain_url_records
group by common_supercategory,common_category;
-- (XX rows affected)

select * from #greendomain_categories order by panelist_count desc, record_count desc;


-- by semantic lemma 
begin try drop table #greendomain_keywords end try begin catch end catch;

select semantic_lemma, 
	semantic_priority_group,
	count(distinct panelist_key) as panelist_count, 
	count(distinct domain_name) as domain_count,
	count(*) as record_count
into #greendomain_keywords
from #greendomain_url_records
group by semantic_lemma, semantic_priority_group;
-- (XX rows affected)

select * from #greendomain_keywords order by panelist_count desc, record_count desc;



-- by semantic group 
begin try drop table #greendomain_semanticgroups end try begin catch end catch;

select semantic_priority_group,
	count(distinct panelist_key) as panelist_count, 
	count(distinct domain_name) as domain_count,
	count(*) as record_count,
	count(distinct semantic_lemma) as semantic_lemma_count
into #greendomain_semanticgroups
from #greendomain_url_records
group by semantic_priority_group;
--(XX rows affected)

select * from #greendomain_semanticgroups order by panelist_count desc, record_count desc;


-- by domain
begin try drop table #greendomains end try begin catch end catch;

select domain_name,
	count(distinct panelist_key) as panelist_count,
	count(*) as record_count,
	count(distinct semantic_lemma) as semantic_lemma_count,
	count(distinct semantic_priority_group) as semantic_priority_group_count
into #greendomains
from #greendomain_url_records
group by domain_name;
--(XX rows affected)

select * from #greendomains order by panelist_count desc, record_count desc;






---------------------------------
--URL TITLE PASS
-----------------------------------
-- select top 100 * from run.v_rpt_url_title_mapping;
-- select top 100 * from #ref_url_title;
begin try drop table #ref_url_title end try begin catch end catch;
select record_id,a.URL_title_key, a.URL_title, core.dbo.alphanum(a.URL_title) as url_title_gra,
b.domain_name_key, b.domain_name
into #ref_url_title
from ref.URL_title a
join run.v_rpt_url_title_mapping b on a.URL_title_key = b.url_title_key
group by record_id,a.URL_title_key, a.URL_title,
b.domain_name_key, b.domain_name
;
--(XX rows affected)



-- select * from #whitedomain_url_best_pattern where best_content_pattern_id <>0;
-- select * from ref.url where domain_name = 'adidas.com';
-- select * from #whitedomain;
begin try drop table #whitedomain_url_title_best_pattern end try begin catch end catch;
select a.URL_title_key,a.domain_name_key, min(isnull(c.pattern_id,0)) as best_content_pattern_id
into #whitedomain_url_title_best_pattern
from #ref_url_title a
inner join #whitedomain b
	on a.domain_name=b.domain_name
left join ref.pattern_table c
	on a.url_title_gra like c.pattern
	and b.semantic_lemma <> c.semantic_lemma -- semantic keyword join is on something other than domain.
	and c.run_on_white=1
group by a.URL_title_key,a.domain_name_key;
-- (XX rows affected)

-- select * from #whitedomain_url_best_pattern where url_key = 856005;
-- select * from nestle2019.ref.pattern_table_beauty_georgiapacific order by pattern_id;
-- select * from #whitedomain;
-- select * from nestle2019.ref.url where url_key = 856005
-- select top 100 * from #whitedomain_url_title_content_detail;

begin try drop table #whitedomain_url_title_content_detail end try begin catch end catch;
select a.record_id,a.URL_title_key, a.URL_title, url_title_gra,
a.domain_name_key, a.domain_name, common_supercategory, common_category,common_vertical,
c.semantic_lemma, c.semantic_priority_group
into #whitedomain_url_title_content_detail
from #ref_url_title a
left join #whitedomain_url_title_best_pattern b -- left since we want every white record
	on a.URL_title_key=b.URL_title_key
	and a.domain_name_key = b.domain_name_key
left join ref.pattern_table c -- left since we want every white record
	on b.best_content_pattern_id=c.pattern_id
inner join #whitedomain d -- since we only want white domains
	on a.domain_name_key=d.domain_name_key
group by a.record_id,a.URL_title_key, a.URL_title, url_title_gra,
a.domain_name_key, a.domain_name,common_supercategory, common_category,common_vertical,
 c.semantic_lemma, c.semantic_priority_group
;
-- (XX rows affected)



	-- select * from #whitedomain_url_content_detail where url_key=856005;
-- select * from #whitedomain_url_content_detail where domain_name = 'toneitup.com'; (599 rows affected)

-- ##################################################################
-- EXPORT: WHITE URL TITLES FOR REVIEW
-- ##################################################################

select record_id,URL_title_key, URL_title, url_title_gra,
domain_name_key, domain_name, '' as domain_cat, '' as domain_subcat, 
semantic_lemma, semantic_priority_group, '' as accuracy, '' as notes
from #whitedomain_url_title_content_detail
order by domain_name, semantic_priority_group, semantic_lemma, url_title;

/* IMPORTANT QA: Ensure that all url title domains have a color
select a.* from #ref_url_title a left join ref.domain_color_beauty b on a.domain_name_key = b.domain_name_key where b.domain_name_key is null;
*/

-- ================================================================================================
--  ANALYZE GREEN DOMAIN URL Titles
-- ================================================================================================



-- select top 100 * from #greendomain_url_best_pattern_1;
-- Note: This first step identify all URLs elgible in green domains
-- select top 100 * from #ref_url_title;
--select top 100 * from #greendomain_url_semantic_match;

begin try drop table #greendomain_all_url_titles end try begin catch end catch;
select a.URL_title_key, a.URL_title, core.dbo.alphanum(a.URL_title) as url_title_gra,
a.domain_name_key, a.domain_name, b.common_supercategory, b.common_category, b.common_Vertical
into #greendomain_all_url_titles
from #ref_url_title a
inner join ref.domain_color b
       on a.domain_name=b.domain_name
	   and b.prop_color='green'
group by a.URL_title_key, a.URL_title,
a.domain_name_key, a.domain_name,
b.common_supercategory, b.common_category, b.common_Vertical;
-- select * from  #greendomain_all_urls where domain_name_key = 968
--(XX rows affected)




-- select * from #greendomain_url_best_pattern;
-- Note: This checks each eligible URL for semantic matching, then identifies best match based on priority. Tables about 3 minutes to step through 1MM URLs.

---Gets bonus match
begin try drop table #greendomain_bonus_match_url_title end try begin catch end catch;
select a.URL_title_Key, a.domain_name_key,common_supercategory,common_category, common_Vertical, a.url_title_gra, min(bonus_pattern_id) as best_bonus_pattern
into #greendomain_bonus_match_url_title
from #greendomain_all_url_titles a 
join ref.bonus_pattern_table b
on a.url_title_gra like b.bonus_pattern
group by a.URL_title_Key, a.domain_name_key,common_supercategory,common_category, common_Vertical, a.url_title_gra;
--(XX rows affected)

-- select top 100 * from  #greendomain_best_bonus_match_url_title;
begin try drop table #greendomain_best_bonus_match_url_title end try begin catch end catch;
select a.URL_title_key, a.domain_name_key, common_supercategory,common_category, common_Vertical,a.url_title_gra, b.bonus_lemma as bonus_word, b.bonus_priority_group
into #greendomain_best_bonus_match_url_title
from #greendomain_bonus_match_url_title  a 
join ref.bonus_pattern_table b
on a.best_bonus_pattern = b.bonus_pattern_id
group by a.URL_title_key, a.domain_name_key, common_supercategory,common_category, common_Vertical, a.url_title_gra, b.bonus_lemma, b.bonus_priority_group
;
--(XX rows affected)


begin try drop table #greendomain_url_title_best_pattern end try begin catch end catch;
select a.URL_title_key, a.domain_name_key, common_supercategory,common_category, common_Vertical, min(pattern_id) as best_pattern
into #greendomain_url_title_best_pattern
from #greendomain_all_url_titles a
inner join ref.pattern_table b
	on a.url_title_gra like b.pattern
	and (b.run_on_green=1 or b.semantic_priority_group = 'Remove')
group by a.URL_title_key, a.domain_name_key, common_supercategory,common_category, common_Vertical;
--(XX rows affected)

-- select top 100 * from #greendomain_url_title_semantic_match_v1;
-- select * from ref.url;

begin try drop table #greendomain_url_title_semantic_match_v1 end try begin catch end catch;
select a.record_id, a.URL_title_key, a.URL_title, a.url_title_gra,a.domain_name_key, domain_name,
c.semantic_lemma, c.semantic_priority_group,  c.pattern_id,
d.bonus_word, d.bonus_priority_group, 
 b.common_supercategory,b.common_category, b.common_Vertical
into #greendomain_url_title_semantic_match_v1
from #ref_url_title a
inner join #greendomain_url_title_best_pattern b
	on a.URL_title_key=b.URL_title_key
	and a.domain_name_key = b.domain_name_key
inner join ref.pattern_table c
	on b.best_pattern=c.pattern_id
	and c.semantic_priority_group <> 'Remove' -- excludes false postives
left join #greendomain_best_bonus_match_url_title d
	on a.URL_title_key = d.URL_title_key
	and a.domain_name_key = b.domain_name_key
group by a.record_id, a.URL_title_key, a.URL_title,a.url_title_gra, a.domain_name_key, domain_name,
c.semantic_lemma, c.semantic_priority_group,  c.pattern_id,
d.bonus_word, d.bonus_priority_group, 
 b.common_supercategory,b.common_category, b.common_Vertical
;
--(XX rows affected)



--------For words with a bonus word, it runs the ambiguous words
begin try drop table #greendomain_url_title_best_pattern_ambiguous end try begin catch end catch;
select a.url_title_key, a.domain_name_key, common_supercategory,common_category, common_Vertical,bonus_word,bonus_priority_group,min(pattern_id) as best_pattern
into #greendomain_url_title_best_pattern_ambiguous
from #greendomain_best_bonus_match_url_title  a
inner join ref.pattern_table b
	on a.url_title_gra like b.pattern
	and (b.run_on_green=0 or b.semantic_priority_group = 'Remove')
group by a.url_title_key, a.domain_name_key, common_supercategory,common_category, common_Vertical,bonus_word,bonus_priority_group;
--(XX rows affected)




-------Similar to with the unambiguous words
-- select * from #greendomain_url_semantic_match_ambiguous ;
begin try drop table #greendomain_url_title_semantic_match_ambiguous end try begin catch end catch;
select a.record_id, a.URL_title_key, a.URL_title,a.url_title_gra, a.domain_name_key, domain_name,
c.semantic_lemma, c.semantic_priority_group, c.pattern_id,
b.bonus_word, b.bonus_priority_group, 
 b.common_supercategory,b.common_category, b.common_Vertical
into #greendomain_url_title_semantic_match_ambiguous
from #ref_url_title a
inner join #greendomain_url_title_best_pattern_ambiguous b
	on a.URL_title_key=b.URL_title_key
	and a.domain_name_key = b.domain_name_key
inner join ref.pattern_table c
	on b.best_pattern=c.pattern_id
	and c.semantic_priority_group <> 'Remove' -- excludes false postives
group by a.record_id, a.URL_title_key, a.URL_title,a.url_title_gra, a.domain_name_key, domain_name,
c.semantic_lemma, c.semantic_priority_group, c.pattern_id,
b.bonus_word, b.bonus_priority_group, 
 b.common_supercategory,b.common_category, b.common_Vertical
;
--(XX rows affected)



-----This inserts in the matches from the ambiguous one
--It creates dupes if the url matched on both ambiguous and unambiguous words, so next step will choose the lower priority word
insert into #greendomain_url_title_semantic_match_v1 
select * from #greendomain_url_title_semantic_match_ambiguous;
--(XX rows affected)


begin try drop table #true_best_pattern_url_title end try begin catch end catch;
select url_title_key,domain_name_key, min(pattern_id) as true_best_pattern
into #true_best_pattern_url_title
from #greendomain_url_title_semantic_match_v1 
group by url_title_key,domain_name_key;
--(XX rows affected)


begin try drop table #greendomain_url_title_semantic_match_v2 end try begin catch end catch;
select a.*
into #greendomain_url_title_semantic_match_v2
from #greendomain_url_title_semantic_match_v1 a
join #true_best_pattern_url_title b on a.URL_title_key = b.URL_title_key and a.domain_name_key = b.domain_name_key and a.pattern_id = b.true_best_pattern;
--(XX rows affected)

-- select top 100 * from #greendomain_url_title_semantic_match_v2;
------Final repass
begin try drop table #greendomain_url_title_best_pattern_repass end try begin catch end catch;
select a.url_title_key, a.domain_name_key, a.common_supercategory,a.common_category, a.common_Vertical,a.bonus_word,a.bonus_priority_group,min(b.pattern_id) as best_pattern
into #greendomain_url_title_best_pattern_repass
from #greendomain_url_title_semantic_match_v2  a
inner join ref.pattern_table b
	on a.url_title_gra like b.pattern
group by a.url_title_key, a.domain_name_key,a.common_supercategory,a.common_category, a.common_Vertical,a.bonus_word,a.bonus_priority_group;
--(XX rows affected)


begin try drop table #greendomain_url_title_semantic_match end try begin catch end catch;
select a.*, c.semantic_lemma, c.semantic_priority_group,
d.bonus_word, d.bonus_priority_group, b.common_supercategory,b.common_category, b.common_Vertical
into #greendomain_url_title_semantic_match 
from #ref_url_title a
inner join #greendomain_url_title_best_pattern_repass b
	on a.URL_title_key=b.URL_title_key
	and a.domain_name_key = b.domain_name_key
inner join ref.pattern_table c
	on b.best_pattern=c.pattern_id
	and c.semantic_priority_group <> 'Remove' -- excludes false postives
left join #greendomain_best_bonus_match_url_title d
	on a.URL_title_key = d.URL_title_Key
	and a.domain_name_key = d.domain_name_key
--where e.URL_Key is null 
;
--(XX rows affected)


-- ##################################################################
-- EXPORT: GREEN URL TITLES FOR REVIEW
-- ##################################################################
-- drop table nestle2019.dbo.greendomain_url_semantic_match;

select record_id,URL_title_key, URL_title, url_title_gra,
domain_name_key, domain_name,  '' as domain_cat, '' as domain_subcat,
semantic_lemma, semantic_priority_group,
bonus_word, bonus_priority_group, common_supercategory,common_category, common_vertical, '' as acccuracy, '' as note
from #greendomain_url_title_semantic_match
where semantic_priority_group <> 'remove'
order by domain_name, semantic_priority_group, semantic_lemma,  url_title;
-- XX rows affected


----------------------------------------------------------------
-- URL Title Classifications Pull
----------------------------------------------------------------

-- select * from #greendomain_url_records order by Panelist_Key,Record_ID;
-- select count(distinct panelist_key) as distinct_panelist_count from #greendomain_url_records; 
-- select top 100 * from #greendomain_url_semantic_match

begin try drop table #greendomain_url_title_records end try begin catch end catch;

select b.panelist_key,
	a.Record_ID, 
	a.domain_name_key,
	a.domain_name, 
	a.common_supercategory,
	a.common_category,
	a.common_vertical,
	a.semantic_lemma,
	a.semantic_priority_group,
	b.digital_type_id,
	1 as ct_record
into #greendomain_url_title_records
from #greendomain_url_title_semantic_match a
join rps.sequence_event b
	on a.record_id= b.record_id
	;
-- (XX rows affected)

-- select * from #whitedomain_url_title_records;
begin try drop table #whitedomain_url_title_records end try begin catch end catch;

select b.panelist_key,
	a.Record_ID, 
	a.domain_name_key,
	a.domain_name, 
	a.common_supercategory,
	a.common_category,
	a.common_vertical,
	a.semantic_lemma,
	a.semantic_priority_group,
	b.digital_type_id,
	1 as ct_record
into #whitedomain_url_title_records
from #whitedomain_url_title_content_detail a
join rps.sequence_event b
	on a.record_id= b.record_id
	;
---- (XX rows affected)
-- select * from #greendomain_url_records;

----------------------------------------------------------------
-- 3E) Green domains count reach
----------------------------------------------------------------
-- by domain name 
-- select * from #greendomain_overall_counts order by panelist_count desc;
-- select * from #greendomain_url_records;

begin try drop table #greendomain_overall_counts_url_title end try begin catch end catch;

select a.domain_name_key, a.domain_name, 
	count(distinct panelist_key) as panelist_count, 
	sum(ct_record) as record_count
into #greendomain_overall_counts_url_title
from  #greendomain_url_title_records a
left join #greendomain_overall_counts e on a.domain_name_key = e.domain_name_key-- dont want to classify if already classified in normal green
where e.domain_name_key is null
group by a.domain_name_key, a.domain_name
;
--(XX rows affected)

begin try drop table #whitedomain_overall_counts_url_title end try begin catch end catch;

select a.domain_name_key, a.domain_name, 
	count(distinct panelist_key) as panelist_count, 
	sum(ct_record) as record_count
into #whitedomain_overall_counts_url_title
from  #whitedomain_url_title_records a
left join #whitedomain_overall_counts e on a.domain_name_key = e.domain_name_key-- dont want to classify if already classified in normal white (that pull only pulls domain if there are url matches)
where e.domain_name_key is null
group by a.domain_name_key, a.domain_name
;
--(XX rows affected)

begin try drop table #url_title_domain_classification end try begin catch end catch;
select 'white' as domain_color, a.domain_name_key, a.domain_name, b.request_flag, panelist_count, record_count,
'' as domain_category, '' as domain_subcategory, 
b.step_assigned,
c.common_supercategory, c.common_category, c.common_subcategory,c.common_vertical, c.project_m_reach,c.property_hash,c.rps_flag, c.rpt_flag, c.vetted_flag
into  #url_title_domain_classification
from #whitedomain_overall_counts_url_title a
inner join ref.domain_color b
	on a.domain_name=b.domain_name
left outer join Core.ref.taxonomy_Common_properties c
	on a.domain_name=c.property_name
	and c.digital_type_id = '235'
where c.common_supercategory <> '[remove]'
;
----(XX rows affected)

insert into  #url_title_domain_classification
select 'green' as domain_color, a.domain_name_key, a.domain_name, b.request_flag, panelist_count, record_count,
'' as domain_category, '' as domain_subcategory, 
b.step_assigned,
c.common_supercategory, c.common_category, c.common_subcategory,c.common_vertical, c.project_m_reach,c.property_hash,c.rps_flag, c.rpt_flag, c.vetted_flag
from #greendomain_overall_counts_url_title a
inner join ref.domain_color b
	on a.domain_name=b.domain_name
left outer join Core.ref.taxonomy_Common_properties c
	on a.domain_name=c.property_name
	and c.digital_type_id = '235'
where c.common_supercategory <> '[remove]'
;
------(XX rows affected)


begin try drop table #url_title_brand_retailer_v1 end try begin catch end catch;
select a.domain_name_key, min(c.pattern_id) as best_pattern
into #url_title_brand_retailer_v1 
from #url_title_domain_classification a
join ref.domain_color b on a.domain_name_key = b.domain_name_key
inner join ref.pattern_table c
	on b.domain_name_gra like c.pattern -- no requirements about ambiguity since want actual lowest
	and (b.semantic_priority_group like '%brand%' OR b.semantic_priority_group like '%retailer%')
;

begin try drop table #url_title_brand_retailer end try begin catch end catch;
select  domain_color, a.domain_name_key, a.domain_name,
isnull(c.pattern,'') as brand_retailer_pattern, isnull(c.semantic_lemma,'') as brand_retailer_lemma, isnull(c.semantic_priority_group,'') as brand_retailer_group,
 a.request_flag, panelist_count, record_count,
domain_category, domain_subcategory, 
a.step_assigned,
case when eco.property_name is not null then eco.ecosystem_category when eco.property_name is null and ctry.property_name is not null then ctry.ecosystem_category else '' end as prev_eco_category,
 case when eco.property_name is not null then eco.ecosystem_subcategory when eco.property_name is null and ctry.property_name is not null then ctry.ecosystem_subcategory else '' end as prev_eco_subcategory,
 case when eco.property_name is not null then eco.categorization_source when eco.property_name is null and ctry.property_name is not null then ctry.categorization_source else '' end as prev_eco_source,
 case when eco.property_name is not null then 'Exact Match' when eco.property_name is null and ctry.property_name is not null then 'Ctry Code Removed' else '' end as prev_eco_match_type,
a.common_supercategory, a.common_category, common_subcategory,a.common_vertical, project_m_reach,A.property_hash,rps_flag, rpt_flag, a.vetted_flag
into #url_title_brand_retailer
from #url_title_domain_classification a
join #url_title_brand_retailer_v1 b on a.domain_name_key = b.domain_name_key
join ref.pattern_table c on b.best_pattern = c.pattern_id
left join core.ref.Ecosystem_Classifications eco
	on a.domain_name = eco.property_name
	and eco.digital_type_id = 235
left join core.ref.Ecosystem_Classifications ctry
	on replace(REPLACE(a.domain_name,'.COUNTRYCODE',''),'.COUNTRYCODE','')  = ctry.property_name 
	and ctry.digital_type_id = 235
	--- IF FOR MEXICO AND domains are amazon.com.mx, remove .mx and matches on US version
	;

-- ##################################################################
-- 3F) EXPORT: url title domains tab
-- ##################################################################

select *
from #url_title_brand_retailer
order by domain_color desc,panelist_count desc, record_count desc;






------------QA to get keywords that dont have any matches in either the white or green list to determine if the pattern needs to change
---Note that joins are based on keywords and not patterns so if there are multiple patterns for the same keyword, all will be the same

begin try drop table #white_keywords end try begin catch end catch;
select semantic_lemma, count(*) as white_count
into #white_keywords
from #whitedomain_url_records
group by semantic_lemma;
--(57 rows affected)


begin try drop table #green_keywords end try begin catch end catch;
select semantic_lemma, count(*) as green_count
into #green_keywords
from #greendomain_url_records
group by semantic_lemma;
--(356 rows affected)

----- for keywords
begin try drop table #pattern_join end try begin catch end catch;
select a.pattern, a.semantic_lemma, a.semantic_priority_id, b.white_count, c.green_count, 
isnull(b.white_count,0)+isnull(c.green_count,0) as total_count
into #pattern_join
from ref.pattern_table a
left join #white_keywords b 
	on a.semantic_lemma = b.semantic_lemma
left join #green_keywords c 
	on a.semantic_lemma = c.semantic_lemma;
--(1022 rows affected)

/*
------ for brands
insert into #pattern_join
select a.pattern, a.semantic_lemma, a.semantic_priority_id, b.white_count, c.green_count,
	isnull(b.white_count,0)+isnull(c.green_count,0) as total_count
from ref.pattern_table_electronics_KP_brands
left join #white_keywords b
	on a.semantic_lemma=b.semantic_lemma
left join #green_keywords c
	on a.semantic_lemma=c.semantic_lemma
;*/


select * from #pattern_join order by total_count desc;

/*
---------Same thing for bonus words
begin try drop table #white_bonus end try begin catch end catch;
select bonus_word, count(*) as white_count
into #white_bonus
from #whitedomain_url_records
group by bonus_word;

begin try drop table #green_bonus end try begin catch end catch;
select bonus_word, count(*) as green_count
into #green_bonus
from #greendomain_url_records
group by bonus_word;
--(6 rows affected)

begin try drop table #bonus_join end try begin catch end catch;
select a.bonus_pattern, a.bonus_lemma, a.bonus_priority_group, b.white_count, c.green_count, 
isnull(b.white_count,0)+isnull(c.green_count,0) as total_count
into #bonus_join
from ref.bonus_pattern_GoogleSearchIntent2020 a
left join #white_bonus b on a.bonus_lemma = b.bonus_word
left join #green_bonus c on a.bonus_lemma = c.bonus_word;
--(9 rows affected)

select * from #bonus_join order by total_count;

*/
