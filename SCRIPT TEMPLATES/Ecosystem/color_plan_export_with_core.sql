-- ###########################################################################################
-- EXPORT: CORE AND SIMILAR WEB CATEGORY FOR COLOR PLANNING
-- ###########################################################################################
use Core;
--------------Core Prep
begin try drop table #core_prep_1 end try begin catch end catch;
select common_supercategory, common_category, common_vertical, max(project_m_reach) as max_property_reach, count(*) as num_properties
into #core_prep_1
from Core.ref.Taxonomy_Common_2021
group by common_supercategory, common_category, common_vertical;
--(522 rows affected)

begin try drop table #core_groups end try begin catch end catch;
select a.common_supercategory, a.common_category, a.common_vertical, max(b.property_name) as property_name, 
a.max_property_reach as property_reach, a.num_properties
into #core_groups
from #core_prep_1 a join core.ref.Taxonomy_Common_2021 b on a.common_supercategory = b.common_supercategory and a.common_category = b.common_category and a.common_vertical = b.common_vertical
and a.max_property_reach = b.project_m_reach
group by a.common_supercategory, a.common_category, a.common_vertical, a.max_property_reach, a.num_properties
;
--(522 rows affected)
--select * from #core_prep_2 order by common_supercategory, common_category, common_vertical;

--------------Android Prep: Since many categories have no apps with reviews fills in id with alphabetically first
--select top 100 * from core.ref.app_google_play_details;
begin try drop table #android_prep_1 end try begin catch end catch;
select category, max(reviews) as max_reviews, count(*) as num_properties
into #android_prep_1
from core.ref.app_google_play_details
group by category;
--(78 rows affected)

begin try drop table #android_prep_2 end try begin catch end catch;
select category, min(app_id) as app_first_alphabetically
into #android_prep_2
from core.ref.app_google_play_details
group by category;


begin try drop table #android_groups end try begin catch end catch;
select a.category, max(b.app_id) as app_id, a.max_reviews, a.num_properties
into #android_groups
from #android_prep_1 a left join core.ref.app_google_play_details b on a.category = b.category and a.max_reviews = b.reviews
group by a.category, a.num_properties, max_reviews;
--(78 rows affected)

update #android_groups set app_id = b.app_first_alphabetically
from #android_groups a join #android_prep_2 b on a.category = b.category and a.app_id is null;
--(28 rows affected)

--------------Itunes Prep
--select top 100 * from core.ref.app_apple_itunes_details;
begin try drop table #itunes_prep_1 end try begin catch end catch;
select category, max(userRatingCount) as max_reviews, count(*) as num_properties
into #itunes_prep_1
from core.ref.app_apple_itunes_details
group by category;
--(26 rows affected)

begin try drop table #itunes_groups end try begin catch end catch;
select a.category, max(b.Bundleid) as app_id, a.max_reviews, a.num_properties
into #itunes_groups
from #itunes_prep_1 a left join core.ref.app_apple_itunes_details b on a.category = b.category and a.max_reviews = b.userRatingCount
group by a.category, a.num_properties, max_reviews;
--(26 rows affected)


-------Bring Them All Together
begin try drop table #joint_categorization_export end try begin catch end catch;
select cast('core' as varchar(100)) as categorization_source,cast(common_supercategory as varchar(300)) as 'supercategory', cast(common_category as varchar(300)) as 'category',
cast('' as varchar(300)) as subcategory, cast(common_vertical as varchar(300)) as 'vertical', 
cast(property_name as varchar(300)) as example_property, num_properties
into #joint_categorization_export
from  #core_groups;
--(522 rows affected)


insert into #joint_categorization_export
select 'google play' as categorization_source,'' as 'supercategory', category as 'category','' as subcategory, '' as 'vertical', 
app_id as example_property, num_properties
from #android_groups
where category is not null
;
--(77 rows affected)

insert into #joint_categorization_export
select 'itunes' as categorization_source,'' as 'supercategory', category as 'category','' as subcategory, '' as 'vertical', 
app_id as example_property, num_properties
from #itunes_groups
where category is not null
;
--(25 rows affected)

--------------
--EXPORTS
--------------
-- select * from #sw_groups;

select *, '' as color
from #joint_categorization_export 
order by categorization_source, supercategory, category, subcategory, vertical;
