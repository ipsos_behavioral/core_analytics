---------------------
----Digital Signatures Segmentation
----------------------

use PROJECTNAME;

------Gets category names
begin try drop table #taxonomy_mapping end try begin catch end catch;
select cat_id, category
into #taxonomy_mapping
FROM [PROJECTNAME].[Ref].[Taxonomy_Ecosystem]
group by cat_id, category;
--(8 rows affected)


--------------Pulls the all devices, all digital, all content level of ecosystem std metrics
begin try drop table #daily_person_ecosystem_pull end try begin catch end catch;
select panelist_key, Date_ID, cat_hash, SubCat_Hash, Property_Key, raw_visits, raw_minutes
into #daily_person_ecosystem_pull
FROM [PROJECTNAME].[Rpt].[Person_Ecosystem_Category]
where platform_id = 0 and digital_type_id = 0 and report_level = 0 and content_cat_id = 0;
--(4263 rows affected)
--select top 100 * from #daily_person_ecosystem_pull order by panelist_key;

/* Ensures properties are in only 1 subcat
select SubCat_Hash,Property_Key
into #temp2
FROM [PROJECTNAME].[Rpt].[Person_Ecosystem_Category]
where platform_id = 0 and digital_type_id = 0 and report_level = 0 and content_cat_id = 0
group by SubCat_Hash,Property_Key;

select property_key, count(*) as count from #temp2 group by Property_Key order by count desc;
*/

-------Since visits and mins are summable, sums across date, subcat, property simultaneously
begin try drop table #person_category_metrics end try begin catch end catch;
select panelist_key, cat_hash, category,sum(raw_visits) as eco_cat_visits, sum(raw_minutes) as eco_cat_mins
into #person_category_metrics
FROM #daily_person_ecosystem_pull a
left join #taxonomy_mapping b on a.cat_hash = b.cat_id
group by panelist_key, cat_hash, category
;
--(1876 rows affected)

--select top 100 * from #person_category_metrics;

---------Sums across all categories to get total ecosystem metrics
begin try drop table #person_total_eco_metrics end try begin catch end catch;
select panelist_key, sum(eco_cat_visits) as eco_total_visits, sum(eco_cat_mins) as eco_total_mins
into #person_total_eco_metrics
from #person_category_metrics
group by panelist_key;
--(1103 rows affected)

-----------Deletes people below threshold of 1 minute
delete from #person_total_eco_metrics where eco_total_mins < 1; --(272 rows affected)

--select top 100 * from #person_total_eco_metrics;

----------Divides each category metrics for each person by their total metrics
begin try drop table #person_eco_share end try begin catch end catch;
select a.*, case when eco_total_visits = 0 then cast(0 as float) 
else cast(eco_cat_visits as float)/cast(eco_total_visits as float) end as visits_share,
case when eco_total_mins = 0 then cast(0 as float) 
else cast(eco_cat_mins as float)/cast(eco_total_mins as float) end as minutes_share
into #person_eco_share
from #person_category_metrics a join #person_total_eco_metrics b on a.panelist_key = b.panelist_key
;
--(1563 rows affected)


--select top 100 * from #person_eco_share where panelist_key = 6;
---Gets the avg visits and mins share per category
begin try drop table #average_eco_share end try begin catch end catch;
select cat_hash, category, AVG(visits_share) as avg_visits_share, avg(minutes_share) as avg_minutes_share
into #average_eco_share
from #person_eco_share
group by cat_hash, category
;
--(8 rows affected)


--select * from #average_eco_share;

--------Creates an index for each person for their category share v avg
begin try drop table #index end try begin catch end catch;
select a.*, cast(100 * (visits_share/avg_visits_share) as int) as visits_index, 
cast(100 * (minutes_share/avg_minutes_share) as int) as minutes_index
into #index
from #person_eco_share a join #average_eco_share b on a.cat_hash = b.cat_hash
;
--(1563 rows affected)


--select top 100 * from #index;

-------Gets max index for each person
begin try drop table #max_index end try begin catch end catch;
select panelist_key, MAX(visits_index) as max_visits_index, max(minutes_index) as max_minutes_index
into #max_index
from #index
group by panelist_key
;
--(831 rows affected)


--select top 100 * from #max_index;

--------Matches that max index to the category to assign a grouping
begin try drop table #panelist_group end try begin catch end catch;
select a.panelist_key, b.visits_index,b.category as panelist_visits_segment,
c.minutes_index,c.category as panelist_mins_segment
into #panelist_group
from #max_index a 
join #index b on a.panelist_key = b.panelist_key and a.max_visits_index= b.visits_index
join #index c on a.panelist_key = c.panelist_key and a.max_minutes_index = c.minutes_index
;
--(833 rows affected)

----------Deletes people with a minutes index less than 105
delete from #panelist_group where minutes_index < 105; --(82 rows affected)


--select top 100 * from #panelist_group;

-----------Since there is a chance for a panelist to have mult cat with max index, check and choose
--should be very rare
--if one version is consistent with other index, keep that one? 
--either that or to the segment which is lower in count
select panelist_key, COUNT(*) as count
 from #panelist_group
 group by panelist_key
order by count desc;

--select * from #panelist_group where panelist_key in (473,1579);

--delete from #panelist_group where panelist_key = 254 and panelist_visits_segment = 'Search Engine';
--delete from #panelist_group where panelist_key = 775 and panelist_visits_segment = 'General Retailer';
--delete from #panelist_group where panelist_key = 1579; --(4 rows affected)

------Gets number of panelists per index per cat -- decide which to keep
/*
select panelist_visits_segment, count(*) as count, avg(visits_index) as avg_index
from #panelist_group
group by panelist_visits_segment
order by count desc;
*/
select panelist_mins_segment, count(*) as count, avg(minutes_index) as avg_index
from #panelist_group
group by panelist_mins_segment
order by count desc;

---For now proceeding as though will only base on mins share
--For low count cats, re-assign those panelists to their top index included as long as 105
begin try drop table  #mins_rearrange_prep end try begin catch end catch;
select a.panelist_key, max(a.minutes_index) as max_mins_index
into #mins_rearrange_prep
from #index a join #panelist_group b on a.panelist_key = b.panelist_key
where b.panelist_mins_segment in ('XXX','XXX')
and a.category not in  ('XXX','XXX')
and a.minutes_index >= 105
group by a.panelist_key
;
--(0 rows affected)
/*

----------Matches to new group
begin try drop table #regrouping end try begin catch end catch;
select a.panelist_key, b.mins_index as new_mins_index,b.category as new_panelist_mins_segment
into #regrouping
from #mins_rearrange_prep a 
join #index b on a.panelist_key = b.panelist_key and a.max_mins_index= b.mins_index
;
--(2 rows affected)

---------Check for any duped
select panelist_key, COUNT(*) as count
 from #regrouping
 group by panelist_key
order by count desc;

------Update original groupings to reflect new changes
update #panelist_group
set mins_index = new_mins_index, panelist_mins_segment = new_panelist_mins_segment
from #regrouping a
join #panelist_group b on a.panelist_key = b.panelist_key;
--(2 rows affected)
*/
--------For all panelists still in old mins group, delete them
delete from #panelist_group where panelist_mins_segment in ('XXX','XXX'); --(40 rows affected)


--------Output
begin try drop table ref.panelist_digital_segmentation end try begin catch end catch;
select panelist_key, panelist_mins_segment as segmentation_name, minutes_index
into ref.panelist_digital_segmentation
from #panelist_group;
--(711 rows affected)