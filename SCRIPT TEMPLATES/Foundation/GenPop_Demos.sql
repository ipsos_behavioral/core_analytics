/****** Script for SelectTopNRows command from SSMS  ******/
Use GoogleSearchIntent2020;

-- select top 100 * from #survey_data_embee;
begin try drop table #survey_data_embee end try begin catch end catch;
select a.*, c.Vendor_Panelist_ID, c.Panelist_Key, REPLACE(postal_code, '.0' ,'') AS postal_code_join
into #survey_data_embee
FROM [GoogleSearchIntent2020].[Raw].[embee_demo_data] a
join [GoogleSearchIntent2020].ref.panelist c 
on a.panelist_id = c.Vendor_Panelist_ID 
;
--(998 rows affected)

update #survey_data_embee set postal_code_join = '0' + postal_code_join where len(postal_code_join) = 4;
--select distinct len(postal_code_join) from #survey_data_embee; --all are blank or 5 now

--gender 
--ethnicity
--hispanic
--income
-- age (generation?)
-- select top 100 * from [GoogleSearchIntent2020].raw.disqo_panelist_mapping;
-- select top 100 * from raw.disqo_demos;
-- select top 100 * from #survey_data_disqo_prep;
begin try drop table #survey_data_disqo_prep end try begin catch end catch;
select a.user_id as hashed_id, b.user_id,a.age,a.gender, cast(a.postal_code as varchar(20)) as postal_code, c.Panelist_Key
into #survey_data_disqo_prep
from [GoogleSearchIntent2020].raw.disqo_demos a 
join [GoogleSearchIntent2020].raw.disqo_panelist_mapping b on a.user_id = b.hashed_id 
join [GoogleSearchIntent2020].ref.panelist c on b.hashed_id = c.Vendor_Panelist_ID
;
--(987 rows affected)


update #survey_data_disqo_prep set postal_code = '0' + postal_code where len(postal_code) = 4;
--select distinct len(postal_code) from #survey_data_disqo_prep; --all are blank or 5 now

-- select top 100 * from raw.GSI_Demo_full;
-- select top 100 * from raw.new_disqo_mapping
begin try drop table #survey_data_disqo end try begin catch end catch;
select a.*, c.age_generation,REPLACE(ethnicity, '"' ,'') AS ethnicity, REPLACE(hispanicorigin, '"' ,'') AS hispanicorigin, 
REPLACE(householdincome, '"' ,'') AS householdincome
into  #survey_data_disqo
from #survey_data_disqo_prep a
join raw.new_disqo_mapping b on a.user_id  = b.user_id
join raw.GSI_Demo_full c on b.hashed_user_id = c.hashed_user_id
;
--(987 rows affected)

-- select top 100 * from #survey_data_embee;
-- select top 100 * from #survey_data_disqo;
-- select distinct hh_income  from #survey_data_embee;
-- select distinct householdincome  from #survey_data_disqo;
begin try drop table #survey_data end try begin catch end catch;
select panelist_id,Panelist_Key,age, 
case when age <= 25 then '<=25' when (age > 25 and age <= 35) then '26-35' 
when (age > 35 and age <= 45) then '36-45' 
when age > 45 then '46+' 
when (age is null or age in ('')) then 'Decline to answer' end as age_group,
case when gender = 'MALE' then 'Male' when gender = 'FEMALE' then 'Female' when (gender is null or gender in ('')) then 'Decline to answer' end as gender,
case when ethnicity in ('White','Blanco','White / Caucasian') then 'White' 
when ethnicity in ('Black / African American','Black or African American') then 'Black'
when (ethnicity = 'Middle Eastern'  or ethnicity like '%Asian%' or ethnicity like '%Pacific Islander%') then 'Asian/Pacific Islander'
when ethnicity in ('American Indian / Alaskan Native', 'Native American or Alaskan native','Nativo americano o nativo de Alaska') then 'Native American/Alaskan Native'
when ethnicity in ('Mixed / Other Race','Otra raza','Mixed racial background', 'Origen racial mixto','Other race') then 'Mixed/Other Race'
when ethnicity in ('Prefer Not to Say','','Decline to answer') then 'Decline to answer'
else ethnicity end as ethnicity,
case when is_hispanic in ('Yes, of Hispanic origin', 'S�, de origen hisp�nico') then 'Yes' when is_hispanic = 'No, not of Hispanic origin' then 'No' else 'Decline to answer' end as Is_Hispanic,
case when hh_income in ('Less than $5,000', '$5,000 - $9,999', '$0 - $14,999 USD') then '$0 - $14,999' 
when hh_income in ('$10,000 - $14,999','$15,000 - $19,999','$20,000 - $24,999','$15,000 - $24,999 USD') then '$15,000 - $24,999'
when hh_income in ('$25,000 - $29,999','$30,000 - $34,999','$35,000 - $39,999','$40,000 - $44,999','$45,000 - $49,999','$25,000 - $49,999 USD') then '$25,000 - $49,999'
when hh_income in ('$50,000 - $54,999','$55,000 - $59,999','$60,000 - $64,999','$65,000 - $69,999','$70,000 - $74,999','$75,000 - $79,999','$50,000 - $79,999 USD') then '$50,000 - $79,999'
when hh_income in ('$80,000 - $84,999','$85,000 - $89,999','$90,000 - $94,999','$95,000 - $99,999','$80,000 - $99,999 USD') then '$80,000 - $99,999'
when hh_income in ('$100,000 - $124,999','$125,000 - $149,999','$100,000 - $149,999 USD') then '$100,000 - $149,999'
when hh_income in ('$150,000 - $174,999','$175,000 - $199,999','$150,000 - $199,999 USD') then '$150,000 - $199,999'
when hh_income in ('$200,000 - $249,999', '$250,000 and above', '$200,000 USD or more') then '$200,000 or more'
when hh_income in ('Prefer Not to Say','') then 'Decline to answer'
end as hh_income,
postal_code_join as postal_code,
carrier as mobile_carrier,
handset_make,
'Mobile' as device_type
into #survey_data 
from #survey_data_embee;
--(998 rows affected)

insert into #survey_data
select hashed_id as panelist_id,Panelist_Key,age, 
case when age <= 25 then '<=25' when (age > 25 and age <= 35) then '26-35' 
when (age > 35 and age <= 45) then '36-45' 
when age > 45 then '46+' 
when (age is null or age in ('')) then 'Decline to answer' end as age_group,
case when gender = 'MALE' then 'Male' when gender = 'FEMALE' then 'Female' when (gender is null or gender in ('')) then 'Decline to answer' end as gender,
case when ethnicity in ('White','Blanco','White / Caucasian') then 'White' 
when ethnicity in ('Black / African American','Black or African American') then 'Black'
when (ethnicity = 'Middle Eastern'  or ethnicity like '%Asian%' or ethnicity like '%Pacific Islander%') then 'Asian/Pacific Islander'
when ethnicity in ('American Indian / Alaskan Native', 'Native American or Alaskan native','Nativo americano o nativo de Alaska') then 'Native American/Alaskan Native'
when ethnicity in ('Mixed / Other Race','Otra raza','Mixed racial background', 'Origen racial mixto','Other race') then 'Mixed/Other Race'
when ethnicity in ('Prefer Not to Say','','Decline to answer') then 'Decline to answer'
else ethnicity end as ethnicity,
case when hispanicorigin like'Yes%' then 'Yes' when hispanicorigin = 'No' then 'No' else 'Decline to answer' end as Is_Hispanic,
case when householdincome in ('Less than $5,000', '$5,000 - $9,999', '$0 - $14,999 USD') then '$0 - $14,999' 
when householdincome in ('$10,000 - $14,999','$15,000 - $19,999','$20,000 - $24,999','$15,000 - $24,999 USD') then '$15,000 - $24,999'
when householdincome in ('$25,000 - $29,999','$30,000 - $34,999','$35,000 - $39,999','$40,000 - $44,999','$45,000 - $49,999','$25,000 - $49,999 USD') then '$25,000 - $49,999'
when householdincome in ('$50,000 - $54,999','$55,000 - $59,999','$60,000 - $64,999','$65,000 - $69,999','$70,000 - $74,999','$75,000 - $79,999','$50,000 - $79,999 USD') then '$50,000 - $79,999'
when householdincome in ('$80,000 - $84,999','$85,000 - $89,999','$90,000 - $94,999','$95,000 - $99,999','$80,000 - $99,999 USD') then '$80,000 - $99,999'
when householdincome in ('$100,000 - $124,999','$125,000 - $149,999','$100,000 - $149,999 USD') then '$100,000 - $149,999'
when householdincome in ('$150,000 - $174,999','$175,000 - $199,999','$150,000 - $199,999 USD') then '$150,000 - $199,999'
when householdincome in ('$200,000 - $249,999', '$250,000 and above', '$200,000 USD or more') then '$200,000 or more'
when householdincome in ('Prefer Not to Say','') then 'Decline to answer'
end as hh_income,
postal_code as postal_code,
'' as mobile_carrier,
'' as handset_make,
'PC' as device_type
from #survey_data_disqo;
--(987 rows affected)


begin try drop table Core.ref.Panelist_Demos_GenPop end try begin catch end catch;
select 'GenPop' as panel_group,panelist_id, a.Panelist_Key, age_group, gender, ethnicity, Is_Hispanic, hh_income, device_type, postal_code, mobile_carrier, handset_make
into Core.ref.Panelist_Demos_GenPop
from #survey_data a
group by panelist_id, a.Panelist_Key, age_group, gender, ethnicity, Is_Hispanic, hh_income, device_type, postal_code, mobile_carrier, handset_make;
--(1985 rows affected)
