---------------------------------------
--HML Segmentation Template
--------------------------------------

use [PROJECTNAME];

-------------Qualification portion
begin try drop table #person_qualification end try begin catch end catch;
select a.panelist_key, Date_ID, week_id, month_id,
	weekly_qualified, monthly_qualified,
	platform_id_mobile as device_type_id_mobile, 
	platform_id_pc as device_type_id_pc
into #person_qualification
from rps.panelist_platform_qual_day a
group by 
	a.panelist_key, date_id, week_id, month_id,
	weekly_qualified, monthly_qualified,
	platform_id_mobile, 
	platform_id_pc
;
--(4571 rows affected)

create index person_qualification1 on #person_qualification(panelist_key);
create index person_qualification2 on #person_qualification(date_id);


------------------------------------------
--DP QUAL DAYS REGARDLESS OF DEVICE TYPE
------------------------------------------
-- drop table #person_qual_day_count;
begin try drop table #person_qual_day_count end try begin catch end catch;
select 0 as device_type_id, panelist_key, count(distinct Date_ID) as dp_qualdays
into #person_qual_day_count
from #person_qualification
where (device_type_id_mobile is not null or device_type_id_pc is not null)
group by panelist_key;
--(272 rows affected)

begin try drop table #limit_sequence_qual_days end try begin catch end catch;
SELECT
	a.*,
	d.dp_qualdays
INTO #limit_sequence_qual_days
FROM rps.v_Sequence_Event_Ecosystem a
JOIN #person_qualification c
	on a.panelist_key = c.panelist_key
	and a.Date_ID = c.Date_ID
	and (a.Platform_ID = c.device_type_id_mobile or a.Platform_ID = c.device_type_id_pc)
JOIN #person_qual_day_count d
	ON a.panelist_key = d.panelist_key;

create index lim_seq_panelist on #limit_sequence_qual_days (panelist_key);

begin try drop table #panelist_total_seconds end try begin catch end catch;
SELECT
	panelist_key,
	sum(dur_seconds) as total_dur_seconds,
	sum(dur_seconds/dp_qualdays) as seconds_per_qualday
INTO #panelist_total_seconds
FROM #limit_sequence_qual_days
GROUP BY 
	panelist_key;

begin try drop table #temp_rank end try begin catch end catch;
SELECT * 
INTO #temp_rank
FROM #panelist_total_seconds
ORDER BY seconds_per_qualday DESC;

begin try drop table #temp_rank2 end try begin catch end catch;
SELECT 
	row_number() OVER(ORDER BY seconds_per_qualday DESC) as rank, 
	a.*
into #temp_rank2 
from #temp_rank a
ORDER BY seconds_per_qualday DESC;

begin try drop table #temp_totals end try begin catch end catch;
SELECT
	a.*,
	(select count(distinct panelist_key) from #temp_rank2) as total_panelist_count,
	(select sum(total_dur_seconds) from #temp_rank2) as all_dur_seconds
INTO #temp_totals
FROM #temp_rank2 a;

begin try drop table #running_total end try begin catch end catch;
SELECT
	rank,
	total_dur_seconds,
	seconds_per_qualday,
	total_panelist_count,
	all_dur_seconds,
	sum(seconds_per_qualday) over (order by rank) as running_total
	--total_active_seconds/all_active_seconds as shareofseconds
INTO #running_total
FROM #temp_totals
ORDER BY rank;

begin try drop table #share_of_seconds end try begin catch end catch;
SELECT
	a.*,
	cast(rank*1.0/total_panelist_count as numeric(25,5)) as share_of_panelists
INTO #share_of_seconds
FROM #running_total a;

begin try drop table #hml end try begin catch end catch;
SELECT
	b.panelist_key,
	CASE WHEN share_of_panelists BETWEEN 0 AND .2 THEN 1			-- 1: Heavy (Top 20%)
            WHEN share_of_panelists BETWEEN .20 AND .50 THEN 2		-- 2: Medium (Next 30%)
            ELSE 3 END as hml_break,							-- 3: Light (Last 50%)
        CASE WHEN share_of_panelists BETWEEN 0 AND .2 THEN 'Heavy'
            WHEN share_of_panelists BETWEEN .20 AND .50 THEN 'Medium'
            ELSE 'Light' END as hml_break_name
INTO #hml
FROM #share_of_seconds a
JOIN #temp_totals b
	ON a.rank = b.rank;

begin try drop table #hml_mpu end try begin catch end catch;
SELECT
	a.panelist_key,
	hml_break,
	hml_break_name,
	total_dur_seconds,
	seconds_per_qualday
INTO #hml_mpu
FROM #hml a
JOIN #temp_totals b
	ON a.panelist_key = b.panelist_key;

begin try drop table rps.Panelist_HML end try begin catch end catch;
select *
into rps.Panelist_HML
from #hml_mpu;