-- name
-- date


USE [PROJECTNAME];

-- TERMINOLGY WE USE
------------------------------------------------------------------
-- Dimensions: Ways the data will be cut (e.g. device type, digital, population segments, time segments)
-- Classifications: Taxonomy mapping, often detailed, that describes scattered datapoints into a meaningful way. (e.g. domain, property, category)
-- Measurements: Sometimes called metrics. Numeric values only. These may be additive across dimensions (e.g. minutes, visits, searches) or deduplicated (unique visitors, usage days)

-- select min([date_id]) from [PROJECTNAME].[run].[user_qualification] where daily_qualified=1;
-- select max([date_id]) from [PROJECTNAME].[run].[user_qualification] where daily_qualified=1;
-- check to make sure that run.user_qualification dates are matching run.userbyday_total dates

-- Define Global Variables
declare @global_first_date date = 'XXXX-XX-XX'; -- first legitimate date of study
declare @global_last_date  date = 'XXXX-XX-XX'; -- today's date (or last date of study)
declare @global_max_date   date = 'XXXX-XX-XX';
declare @global_min_qual_days   int = 7;
declare @global_min_span_days	int = 14;

begin try drop table #tax_version_lookup end try begin catch end catch;
select cast(0 as int) as tax_version_id, cast('Common' as varchar(20)) as tax_version
into #tax_version_lookup;
insert into #tax_version_lookup select 1, 'Custom';

create index tax_version_lookup1 on #tax_version_lookup(tax_version_id);

-- ### USE THESE VARIABLE EDITS IN FUTURE PROJECTS ###
-- select * from #variable_names order by var_id, val_id;
-- drop table #variable_names;
begin try drop table  #variable_names end try begin catch end catch;
select var_id, var_name, val_id, val_name
into #variable_names
from [Ref].[Variable_Names];

--update #variable_names set val_id=10 where var_name='platform_id' and val_name='Mobile';
--update #variable_names set val_id=20 where var_name='platform_id' and val_name='PC';
insert into #variable_names select 1 as var_id, 'platform_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 2 as var_id, 'device_type_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 4 as var_id, 'digital_type_id' as var_name, 0 as val_id, 'All Digital' as val_name; -- ### Use 1 in the future

create index variable_names1 on #variable_names(val_id);

---------------------------------------------------------------------------------------------------
-- STEP 1) CREATE QUALIFICATION AGGREGATES
---------------------------------------------------------------------------------------------------

begin try drop table #vendor_panelist_device_event end try begin catch end catch;

select [PanelistId] as panelist_id_vendor,
	parentclientID as device_id_vendor,
	cast('Web' as varchar(300)) as digital_type,	
	case when OSName like 'Android%' THEN 'Android'
	   when OSName like 'iOS%' THEN 'iOS'
	   when OSName like 'OSX%' THEN 'Mac OS'
	   when OSName like 'Windows%' THEN 'Windows'
	   else NULL end as os_name,
	case when devicetype like 'Laptop/Desktop' THEN cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'iOS%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'Android%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'OSX%' then cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'Windows%' then cast('PC' as varchar(300))
		when DeviceType = 'tablet' then cast('Tablet' as varchar(300)) 
		else cast('Mobile' as varchar(300)) end as platform_type,
	cast(date as date) as data_date
into #vendor_panelist_device_event
from Raw.RealLifeWebV5;
--(1563368 rows affected)

insert into #vendor_panelist_device_event
select [PanelistId] as panelist_id_vendor,
	parentclientID as device_id_vendor,
	cast('App' as varchar(300)) as digital_type,	
	case when OSName like 'Android%' THEN 'Android'
	   when OSName like 'iOS%' THEN 'iOS'
	   when OSName like 'OSX%' THEN 'Mac OS'
	   when OSName like 'Windows%' THEN 'Windows'
	   else NULL end as os_name,
	case when devicetype like 'Laptop/Desktop' THEN cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'iOS%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'Android%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'OSX%' then cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'Windows%' then cast('PC' as varchar(300))
		when DeviceType = 'tablet' then cast('Tablet' as varchar(300))
		else cast('Mobile' as varchar(300)) end as platform_type,
	cast(startdate as date) as data_date
from raw.RealLifeAppV2;
--(2769052 rows affected)
--declare @global_last_date  date = '2020-01-16'; declare @global_first_date date = '2019-10-31';

begin try drop table #vendor_panelist_device_date end try begin catch end catch;
-- keep only valid dates and aggregate

select panelist_id_vendor, device_id_vendor, digital_type, os_name, platform_type, data_date,
	case when platform_type = 'PC' then 1 else 0 end as flag_pc,
	case when platform_type = 'Mobile' then 1 else 0 end as flag_mobile,
	case when platform_type = 'Tablet' then 1 else 0 end as flag_tablet,
	count(*) as ct_records
into #vendor_panelist_device_date
from #vendor_panelist_device_event
where data_date >= @global_first_date and data_date <= @global_last_date
group by panelist_id_vendor, device_id_vendor, digital_type, os_name, platform_type, data_date;
--(19699 rows affected)
--select top 100 * from #vendor_panelist_device_date;

begin try drop table #vendor_panelist_platform_date end try begin catch end catch;

select panelist_id_vendor, device_id_vendor, digital_type, platform_type, data_date, flag_pc, flag_mobile,flag_tablet,
	sum(ct_records) as ct_records
into #vendor_panelist_platform_date
from #vendor_panelist_device_date
group by panelist_id_vendor, device_id_vendor, digital_type, os_name, platform_type, data_date, flag_pc, flag_mobile, flag_tablet;
--(19699 rows affected)

-- select top 100 * from #vendor_panelist_device_date;
-- select top 100 * from #vendor_panelist_platform_date where platform_type = 'Tablet';

-- FORMAT FOR PARTICIPATION FUNNEL EXCEL
---------------------------------------------------------------------------------------------------
-- select top 100 * from #participation_prep order by panelist_key;
-- declare @global_max_date   date = '2017-12-19';

begin try drop table #participation_prep end try begin catch end catch;

select panelist_id_vendor,
	b.panelist_key, 
	cast('All' as varchar(300)) as platform_type,
	count(distinct platform_type) as ct_platforms,
	count(distinct device_id_vendor) as ct_devices,
	max(flag_pc) AS pc_flag,
	max(flag_mobile) AS mobile_flag,
	max(flag_tablet) as tablet_flag, 
	min(data_date) as min_date,
	max(data_date) as max_date,
	sum(ct_records) as ct_records,
	count(distinct data_date) as ct_dates,
	datediff(day, min(data_date), max(data_date))+1 as span_dates,
	datediff(day, max(data_date), @global_max_date)+1 as days_since_last,
	cast(count(distinct data_date) as decimal(6,3))/(datediff(day, min(data_date), max(data_date))+1) as utilization
into #participation_prep
from #vendor_panelist_device_date a
join [Ref].[Panelist] b -- appends any variables needed (such a country, or segment)
	on a.panelist_id_vendor = b.[Vendor_Panelist_ID]
group by panelist_id_vendor, panelist_key;
--(889 rows affected)

insert into #participation_prep
select panelist_id_vendor,
	b.panelist_key, 
	a.platform_type,
	1 as ct_platforms,
	count(distinct device_id_vendor) as ct_devices,
	max(flag_pc) AS pc_flag,
	max(flag_mobile) AS mobile_flag,
	max(flag_tablet) as tablet_flag, 
	min(data_date) as min_date,
	max(data_date) as max_date,
	sum(ct_records) as ct_records,
	count(distinct data_date) as ct_dates,
	datediff(day, min(data_date), max(data_date))+1 as span_dates,
	datediff(day, max(data_date), @global_max_date)+1 AS days_since_last,
	cast(count(distinct data_date) as decimal(6,3))/(datediff(day, min(data_date), max(data_date))+1) as utilization
from #vendor_panelist_device_date a
join [Ref].[Panelist] b
	on a.panelist_id_vendor = b.[Vendor_Panelist_ID]
group by panelist_id_vendor, panelist_key, platform_type;
--(1106 rows affected)
create index participation_prep1 on #participation_prep (panelist_key);

-- select * from rps.participation_summary order by panelist_key;
--declare @global_min_qual_days   int = 9;
begin try drop table rps.participation_summary end try begin catch end catch;

select a.panelist_id_vendor,
	a.panelist_key, 
	1 as ct_panelists,
	a.ct_platforms,
	a.ct_devices,
	isnull(m.ct_devices,0) as ct_mobile,
	isnull(p.ct_devices,0) as ct_pc,
	isnull(t.ct_devices,0) as ct_tablet,
	a.mobile_flag,
	a.pc_flag,
	a.tablet_flag,
	case when a.ct_platforms = 2 then 1 else 0 end as dual_flag,
	case when (a.ct_platforms = 1 and a.pc_flag = 1 ) then 1 else 0 end as pc_only_flag,
	case when (a.ct_platforms = 1 and a.mobile_flag = 1) then 1 else 0 end as mobile_only_flag,
	case when (a.ct_platforms = 1 and a.tablet_flag =1) then 1 else 0 end as tablet_only_flag,
	a.ct_dates as ct_dates_all,
	isnull(m.ct_dates,0) as ct_dates_mobile,
	isnull(p.ct_dates,0) as ct_dates_pc,
	isnull(t.ct_dates,0) as ct_dates_tablet,
	a.min_date as min_dates_all,
	-- m.min_date as min_dates_mobile,
	-- p.min_date as min_dates_pc,
	a.max_date as max_dates_all,
	-- m.max_date as max_dates_mobile,
	-- p.max_date as max_dates_pc,
	a.ct_records,
	a.span_dates,
	a.days_since_last,
	case when m.ct_dates >= @global_min_qual_days or p.ct_dates >= @global_min_qual_days or t.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_dual, -- OR logic
	case when m.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_mobile,
	case when p.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_pc,
	case when t.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_tablet,
	cast(a.utilization as numeric(10,2)) as util_all,
	isnull(cast(m.utilization as numeric(10,2)),0) as util_mobile,
	isnull(cast(p.utilization as numeric(10,2)),0) as util_pc,
	isnull(cast(t.utilization as numeric(10,2)),0) as util_tablet
INTO rps.participation_summary
FROM #participation_prep a
left outer join #participation_prep m
	on a.panelist_key=m.panelist_key
	and m.platform_type = 'Mobile'
left outer join #participation_prep p
	on a.panelist_key=p.panelist_key
	and p.platform_type = 'PC'
left outer join #participation_prep t
	on a.panelist_key=t.panelist_key
	and t.platform_type = 'Tablet'
where a.platform_type='All';
--(889 rows affected)
create index participation_summary1 on rps.participation_summary (panelist_key);
--select distinct platform_id from rps.[UserByDay_Total]

--select distinct platform_id from run.userbyday_total;

-- Additional pass of diagnostics that looks at usage, as well as latest UserByDay scrubbed for outliers.
-- Join to Panelist_Exclusion to eliminate any panelists we want removed (outlier, false positives, etc)
begin try drop table #usage_prep end try begin catch end catch;

select a.panelist_key,
	a.platform_id,
	case when a.platform_id=1 then cast('Mobile' as varchar(10))
		else case when a.platform_id=2 then cast('PC' as varchar(10))
		else '???' end end as platform_type, ---NOTE: still need to incorporate tablet
	min(Date_ID) as min_date, max(Date_ID) as max_date, count(distinct Date_ID) as ct_dates,
	(datediff(day, min(Date_ID), max(Date_ID))+1) as span_dates,
	cast(count(distinct Date_ID) as decimal(6,3))/(datediff(day, min(Date_ID), max(Date_ID))+1) as utilization
into #usage_prep
from rps.[UserByDay_Total] a
left outer join ref.Panelist_Exclusion e
	on a.panelist_key = e.panelist_key 	
where e.panelist_key is null and a.Date_ID is not null
group by a.panelist_key, a.platform_id;
--(863 rows affected)

insert into #usage_prep
select a.panelist_key,
	0 as platform_id,
	'All' as platform_type,
	min(Date_ID) as min_date, max(Date_ID) as max_date, count(distinct Date_ID) as ct_dates,
	(datediff(day, min(Date_ID), max(Date_ID))+1) as span_dates,
	cast(count(distinct Date_ID) as decimal(6,3))/(datediff(day, min(Date_ID), max(Date_ID))+1) as utilization
from rps.[UserByDay_Total] a
left outer join ref.Panelist_Exclusion e
	on a.panelist_key = e.panelist_key 	
where e.panelist_key is null and a.Date_ID is not null
group by a.panelist_key;
--(737 rows affected)

create index usage_prep1 on #usage_prep (panelist_key);
create index usage_prep2 on #usage_prep (platform_type);
create index usage_prep3 on #usage_prep (platform_id);


--declare @global_min_qual_days   int = 9; declare @global_min_span_days	int = 16;
begin try drop table rps.usage_summary end try begin catch end catch; --NEED TO ADD TABLET QUAL

-- select top 1000 * from rps.participation_summary;

--add in mobile only, pc only, dual flags
select a.panelist_id_vendor,
	a.panelist_key,
	a.ct_platforms,
	a.ct_devices,
	a.pc_flag,
	a.pc_only_flag,
	a.mobile_flag,
	a.mobile_only_flag,
	a.dual_flag,
	d.min_date,
	d.max_date,
	d.ct_dates,
	d.utilization,
	case when (m.ct_dates >= (@global_min_qual_days-2) and m.span_dates >= (@global_min_span_days-2)) OR (p.ct_dates >= (@global_min_qual_days-2) AND p.span_dates >= (@global_min_span_days-2)) then 1 else 0 end as qual_dual, --OR logic. also 2 less days uses when userbyday
	case when (m.ct_dates >= (@global_min_qual_days-2) and m.span_dates >= (@global_min_span_days-2)) then 1 else 0 end as qual_mobile,
	case when (p.ct_dates >= (@global_min_qual_days-2) and p.span_dates >= (@global_min_span_days-2)) then 1 else 0 end as qual_pc,
	cast(d.utilization as numeric(10,2)) as util_all,
	isnull(cast(m.utilization as numeric(10,2)),0) as util_mobile,
	isnull(cast(p.utilization as numeric(10,2)),0) as util_pc
into rps.usage_summary --make this a perm table
from rps.participation_summary a
inner join #usage_prep d
	on a.panelist_key=d.panelist_key
	and d.platform_type = 'All'
left outer join #usage_prep m
	on a.panelist_key=m.panelist_key
	and m.platform_type = 'Mobile'
left outer join #usage_prep p
	on a.panelist_key=p.panelist_key
	and p.platform_type = 'PC';
--(737 rows affected)


---------------------------------------------------------------------------------------------------
-- PROCESS FOR STRICTER PLATFORM BASED QUALIFICATION
---------------------------------------------------------------------------------------------------

-- select top 100 * from #user_platform_qual_day order by 1, 2;
begin try drop table #user_platform_qual_day end try begin catch end catch;

select a.panelist_key, a.Date_ID, a.week_id, a.month_id, a.weekly_qualified, a.monthly_qualified,
	case when b.qual_dual = 1 then 0 else null end as platform_id_dual,
	case when b.qual_mobile = 1 then 1 else null end as platform_id_mobile,
	case when b.qual_pc = 1 then 2 else null end as platform_id_pc
into #user_platform_qual_day
from rps.User_Qualification a
inner join rps.usage_summary b 
	on a.panelist_key=b.panelist_key
	and a.daily_qualified=1
left outer join ref.Panelist_Exclusion e -- remove 64 panelists
	on a.panelist_key = e.panelist_key 	
    and e.panelist_key is null 
;
--(7992 rows affected)

delete from #user_platform_qual_day where (platform_id_dual is null and platform_id_mobile is null and platform_id_pc is null);
--(3421 rows affected)

create index user_platform_qual_day1 on #user_platform_qual_day(panelist_key);
create index user_platform_qual_day2 on #user_platform_qual_day(date_id);
create index user_platform_qual_day3 on #user_platform_qual_day(platform_id_dual);
create index user_platform_qual_day4 on #user_platform_qual_day(platform_id_mobile);
create index user_platform_qual_day5 on #user_platform_qual_day(platform_id_pc);


-- ################################################################################################
-- A. PERMANENT TABLE CREATED HERE -- permanent table for #user_platform_qual_day
-- ################################################################################################
-- this is the same table as rpt.User_Platform_Qualifications from previous scripts 

begin try drop table rps.panelist_platform_qual_day end try begin catch end catch;
select *
into rps.panelist_platform_qual_day
from #user_platform_qual_day;
--(4571 rows affected)
create index var1 on rps.panelist_platform_qual_day (panelist_key);
create index var2 on rps.panelist_platform_qual_day (date_id);
create index var3 on rps.panelist_platform_qual_day (week_id);
create index var4 on rps.panelist_platform_qual_day (month_id);
create index var5 on rps.panelist_platform_qual_day (platform_id_dual);
create index var6 on rps.panelist_platform_qual_day (platform_id_mobile);
create index var7 on rps.panelist_platform_qual_day (platform_id_pc);
-- ################################################################################################


-- Create permanent tables for panelist platform qual WEEK and MONTH to be used in future standard metrics scripts 
-- begin try drop table rps.panelist_platform_qual_week end try begin catch end catch;
-- begin try drop table rps.panelist_platform_qual_month end try begin catch end catch;
begin try drop table rps.panelist_platform_qual_week end try begin catch end catch;
select panelist_key, week_id, platform_id_dual, platform_id_mobile, platform_id_pc
into rps.panelist_platform_qual_week
from #user_platform_qual_day a
where weekly_qualified=1
group by panelist_key, week_id, platform_id_dual, platform_id_mobile, platform_id_pc;
--(692 rows affected)
delete from rps.panelist_platform_qual_week where (platform_id_mobile is null and platform_id_pc is null); -- in case week level non-quals exist where days did not
create index user_platform_qual_week1 on rps.panelist_platform_qual_week(panelist_key);
create index user_platform_qual_week2 on rps.panelist_platform_qual_week(week_id);

begin try drop table rps.panelist_platform_qual_month end try begin catch end catch;
select panelist_key, month_id, platform_id_dual, platform_id_mobile, platform_id_pc
into rps.panelist_platform_qual_month
from #user_platform_qual_day a
where monthly_qualified=1
group by panelist_key, month_id, platform_id_dual, platform_id_mobile, platform_id_pc;

delete from rps.panelist_platform_qual_month where (platform_id_mobile is null and platform_id_pc is null);
create index user_platform_qual_month1 on rps.panelist_platform_qual_month(panelist_key);
create index user_platform_qual_month2 on rps.panelist_platform_qual_month(month_id);


-- QUAL DAYS
----------------------
-- create #user_platform_qual_day_ct to get #device_topline_qual_days;
begin try drop table #user_platform_qual_day_ct end try begin catch end catch;

select platform_id_dual as platform_id, panelist_key, count(distinct date_id) as dp_qualdays
into #user_platform_qual_day_ct
from #user_platform_qual_day
where platform_id_dual is not null
group by panelist_key, platform_id_dual;

insert into #user_platform_qual_day_ct
select platform_id_mobile as platform_id, panelist_key, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_mobile is not null
group by panelist_key, platform_id_mobile;

insert into #user_platform_qual_day_ct
select platform_id_pc as platform_id, panelist_key, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_pc is not null
group by panelist_key, platform_id_pc;

-- drop table #device_topline_qual_days;
select platform_id, count(distinct panelist_key) as dp, sum(dp_qualdays) as dp_qualdays
into #device_topline_qual_days
from #user_platform_qual_day_ct
group by platform_id;
-- select * from #device_topline_qual_days;

-- QUAL WEEKS
----------------------
-- create #user_platform_qual_week_ct to get #device_topline_qual_weeks;
-- select * from #user_platform_qual_day_ct;
begin try drop table #user_platform_qual_week_ct end try begin catch end catch;
select platform_id_dual as platform_id, panelist_key, count(distinct week_id) as wp_qualweeks
into #user_platform_qual_week_ct
from rps.panelist_platform_qual_week
where platform_id_dual is not null
group by panelist_key, platform_id_dual;

insert into #user_platform_qual_week_ct
select platform_id_mobile as platform_id, panelist_key, count(distinct week_id) as wp_qualweeks
from rps.panelist_platform_qual_week
where platform_id_mobile is not null
group by panelist_key, platform_id_mobile;

insert into #user_platform_qual_week_ct
select platform_id_pc as platform_id, panelist_key, count(distinct week_id) as wp_qualweeks
from rps.panelist_platform_qual_week
where platform_id_pc is not null
group by panelist_key, platform_id_pc;

-- drop table #device_topline_qual_weeks;
select platform_id, count(distinct panelist_key) as wp, sum(wp_qualweeks) as wp_qualweeks
into #device_topline_qual_weeks
from #user_platform_qual_week_ct
group by platform_id;

-- QUAL MONTHS
----------------------
-- create #user_platform_qual_month_ct to get #device_topline_qual_months;
-- select * from #user_platform_qual_month_ct;
begin try drop table #user_platform_qual_month_ct end try begin catch end catch

select platform_id_dual as platform_id, panelist_key, count(distinct month_id) as mp_qualmonths
into #user_platform_qual_month_ct
from rps.panelist_platform_qual_month
where platform_id_dual is not null
group by panelist_key, platform_id_dual;

insert into #user_platform_qual_month_ct
select platform_id_mobile as platform_id, panelist_key, count(distinct month_id) as mp_qualmonths
from rps.panelist_platform_qual_month
where platform_id_mobile is not null
group by panelist_key, platform_id_mobile;

insert into #user_platform_qual_month_ct
select platform_id_pc as platform_id, panelist_key, count(distinct month_id) as mp_qualmonths
from rps.panelist_platform_qual_month
where platform_id_pc is not null
group by panelist_key, platform_id_pc;

-- drop table #device_topline_qual_months;
select platform_id, count(distinct panelist_key) as mp, sum(mp_qualmonths) as mp_qualmonths
into #device_topline_qual_months
from #user_platform_qual_month_ct
group by platform_id;


-- ################################################################################################
-- B. PERMANENT TABLE CREATED HERE -- COMBINE TOPLINE FOR DAY, WEEK, AND MONTH
-- ################################################################################################
-- drop table rps.device_topline_qual;
begin try drop table rps.device_topline_qual end try begin catch end catch
select d.platform_id, dp, dp_qualdays, wp, wp_qualweeks, mp, mp_qualmonths
into rps.device_topline_qual
from #device_topline_qual_days d
inner join #device_topline_qual_weeks w
	on d.platform_id=w.platform_id
inner join #device_topline_qual_months m
	on d.platform_id=m.platform_id;

create index device_topline_qual1 on rps.device_topline_qual(platform_id);


-- END OF FILE --