use Colgate2020; 

--1) Starts with journey which includes buy
-- select top 100 * from #journey_session_pull order by panelist_key, session_id, start_time;
-- select top 100 * from dbo.Output_Journey;
-- select * from #journey_session_pull where panelist_key = 42;
-- select top 100 * from rpt.Journey_Sessions_NonPurchasers;
begin try drop table #journey_session_pull end try begin catch end catch;

select case when a.report_vertical=1 then 'All Panelists'
	when a.report_vertical=21 then 'LED'
	when a.report_vertical=22 then 'Pen'
	when a.report_vertical=23 then 'Whitestrip'
	when a.report_vertical=100 then 'Total Specialty'
	when a.report_vertical=24 then 'LED Purchase/Add to Cart'
	when a.report_vertical=25 then 'Whitestrip Purchase/Add to Cart'
	when a.report_vertical=26 then 'Non-Purchaser'
	else '' end as report_vertical_name,
	a.*
into #journey_session_pull
from rpt.Journey_Sessions_NonPurchasers a
; 
--(3307 rows affected)


-- select top 1000 * from #partitioned_points_ordered where panelist_key = 270 order by report_vertical, panelist_key, start_time desc;
-- select * from #partitioned_points_ordered where row_num = 1 and prev_cluster != '[Start]';
-- select * from #partitioned_points_ordered where panelist_key = 402;
begin try drop table #partitioned_points_ordered end try begin catch end catch;
select row_number() over (partition by report_vertical, panelist_key order by report_vertical, panelist_key, start_time ) as row_num, -- in countdown its time desc but want in order of start time now
	a.*
into #partitioned_points_ordered
from #journey_session_pull a
;
--(3307 rows affected)

-- select * from #panelist_number_steps order by panelist_key, report_vertical_name;
begin try drop table #panelist_number_steps end try begin catch end catch;
select report_vertical_name, panelist_key, max(row_num)-1 as num_steps
into #panelist_number_steps
from #partitioned_points_ordered
group by report_vertical_name, panelist_key
;
--(631 rows affected)

-- select * from #booya order by panelist_key, report_vertical_name;
begin try drop table #booya end try begin catch end catch;
select a.*, b.cluster as first_cluster, c.cluster as second_cluster, d.cluster as third_cluster
into #booya
from #panelist_number_steps a
left join #partitioned_points_ordered b on a.panelist_key = b.panelist_key and a.report_vertical_name= b.report_vertical_name and b.row_num = 1
left join #partitioned_points_ordered c on a.panelist_key = c.panelist_key and a.report_vertical_name= c.report_vertical_name and c.row_num = 2
left join #partitioned_points_ordered d on a.panelist_key = d.panelist_key and a.report_vertical_name= d.report_vertical_name and d.row_num = 3
;
--(631 rows affected)

---------For people with only one touchpoint, the third cluster will be null (since the second one is [End]), updates this to blank
update #booya set third_cluster = '' where third_cluster is null; --(124 rows affected)

begin try drop table #First_Three_Touchpoints end try begin catch end catch;
select *
into #First_Three_Touchpoints
from #booya
;
--(631 rows affected)
-- select * from #First_Three_Touchpoints order by report_vertical_name, panelist_key;


-- create panelist% calculation tables 
begin try drop table #third_cluster_count end try begin catch end catch;

select report_vertical_name, first_cluster, second_cluster, third_cluster, 
count(distinct panelist_key) as third_cluster_count
into #third_cluster_count
from #First_Three_Touchpoints
group by report_vertical_name,first_cluster, second_cluster, third_cluster
order by report_vertical_name,first_cluster, second_cluster, third_cluster;
--(209 row(s) affected)

begin try drop table #third_cluster_total end try begin catch end catch;

select report_vertical_name, first_cluster, second_cluster, 
sum(third_cluster_count) as third_cluster_total
into #third_cluster_total
from #third_cluster_count
group by report_vertical_name, first_cluster, second_cluster 
order by report_vertical_name, first_cluster, second_cluster;
-- select * from #third_cluster_total order by report_vertical_name;

-- select * from #third_cluster_count order by first_cluster, second_cluster, third_cluster;

begin try drop table #second_cluster_count end try begin catch end catch;

select report_vertical_name,first_cluster, second_cluster,  
sum(third_cluster_count) as second_cluster_count
into #second_cluster_count
from #third_cluster_count
group by report_vertical_name,first_cluster, second_cluster
order by report_vertical_name,first_cluster, second_cluster;
--(92 row(s) affected)
-- select * from #second_cluster_count order by first_cluster, second_cluster;

begin try drop table #second_cluster_total end try begin catch end catch;

select report_vertical_name, first_cluster,
sum(third_cluster_count) as second_cluster_total
into #second_cluster_total
from #third_cluster_count
group by report_vertical_name, first_cluster 
order by report_vertical_name, first_cluster;
-- select * from #second_cluster_total order by report_vertical_name;


begin try drop table #first_cluster_count end try begin catch end catch;

select report_vertical_name,first_cluster,   
sum(third_cluster_count) as first_cluster_count 
into #first_cluster_count
from #third_cluster_count
group by report_vertical_name,first_cluster
order by report_vertical_name,first_cluster;
--(30 row(s) affected)

-- select * from #first_cluster_count order by first_cluster;

begin try drop table #first_cluster_total end try begin catch end catch;

select report_vertical_name,   
sum(third_cluster_count) as first_cluster_total
into #first_cluster_total
from #third_cluster_count
group by report_vertical_name
order by report_vertical_name;
-- select * from #first_cluster_total order by report_vertical_name;


-- combine cluster counts
begin try drop table #cluster_count_combined_v1 end try begin catch end catch;

select a.report_vertical_name, a.first_cluster, a.second_cluster, a.third_cluster,
cast(c.first_cluster_count as numeric) as first_cluster_count,
cast(b.second_cluster_count as numeric) as second_cluster_count,
cast(a.third_cluster_count as numeric) as third_cluster_count,
case when c.first_cluster_count >=4 then 1 else 0 end as mrs_flag_1st,
case when b.second_cluster_count >=4 then 1 else 0 end as mrs_flag_2nd,
case when a.third_cluster_count >=4 then 1 else 0 end as mrs_flag_3rd
into #cluster_count_combined_v1
from #third_cluster_count a
left join #second_cluster_count b
on a.report_vertical_name = b.report_vertical_name
and a.first_cluster = b.first_cluster 
and a.second_cluster = b.second_cluster
left join #first_cluster_count c
on a.report_vertical_name = c.report_vertical_name 
and a.first_cluster = c.first_cluster; 

--***********
--EXPORT DATA
--***********
select a.report_vertical_name, a.first_cluster, a.second_cluster, a.third_cluster,
a.first_cluster_count,
cast(a.first_cluster_count/c1.first_cluster_total as DECIMAL(10,3)) as first_panelists_percent,
a.second_cluster_count, 
cast(a.second_cluster_count/c1.first_cluster_total as DECIMAL(10,3)) as second_panelists_percent,
cast(a.second_cluster_count/a.first_cluster_count as DECIMAL(10,3)) as second_percent_previous,
a.third_cluster_count,
cast(a.third_cluster_count/c1.first_cluster_total as DECIMAL(10,3)) as third_panelists_percent,
cast(a.third_cluster_count/a.second_cluster_count as DECIMAL(10,3)) as third_percent_previous,
a.mrs_flag_1st,
a.mrs_flag_2nd,
a.mrs_flag_3rd
from #cluster_count_combined_v1 a
left join #first_cluster_total c1
on a.report_vertical_name = c1.report_vertical_name; 


