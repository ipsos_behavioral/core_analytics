------------------------
----Journey Highlights
------------------------

Use PROJECTNAME;

------------------------
--1) Creates device session ID for grouping: ONLY RUN THIS FIRST SECTION ONCE
------------------------
/*
-- select top 100 * from rps.Sequence_Event;
begin try drop table #lag_ordering end try begin catch end catch;
SELECT [record_id], [Panelist_Key], Date_ID, Start_time, End_Time_Local,
LAG(End_Time_Local) OVER (PARTITION BY panelist_key, 
date_id ORDER BY Start_time asc) as PrevEndTime,
LAG([record_id]) OVER (PARTITION BY panelist_key, 
date_id ORDER BY Start_time asc) as PrevID
  into #lag_ordering
  FROM rps.Sequence_Event
  ;
--(XX rows affected)
 

-- select top 100 * from #tempnew  order by userID, deviceID, appNameHash, [startTime] asc

DECLARE @ValidTimeDiffnew INT = 30; -------- New session if inactive for 30 mins

-- select top 100 * from #apptempnew order by panelist_key, curstarttime;
begin try drop table #time_differential end try begin catch end catch;
select a.[record_id], a.[Panelist_Key], 
a.start_time as CurStartTime, a.End_Time_Local as CurEndTime,PrevEndTime, a.date_id, datediff(s, PrevEndTime, a.[start_time]) as seconds_diff,
 case when datediff(s, PrevEndTime, a.[start_time]) is null then 1 
	when datediff(s, PrevEndTime, a.[start_time]) > (@ValidTimeDiffnew * 60) then 1 ELSE 0 END as event_boundary
into #time_differential
FROM #lag_ordering a 
;
--(XX rows affected)

-- select top 100 * from #device_session;
begin try drop table #device_session end try begin catch end catch;
SELECT record_id, panelist_key, CurStartTime, CurEndTime,PrevEndTime, date_id, seconds_diff, event_boundary,
   SUM(event_boundary) OVER (ORDER BY panelist_key, date_id, CurStartTime ) AS device_session_id
   into #device_session
   from #time_differential 
;
--(XX rows affected)


begin try drop table #session_number_prep end try begin catch end catch;
select Panelist_Key, Date_ID, device_session_id, min(CurStartTime) as session_start_time, max(CurEndTime) as session_end_time
into #session_number_prep
from #device_session a
group by Panelist_Key, Date_ID, device_session_id
;
--(XX rows affected)

-- select top 100 * from #session_number order by panelist_key, session_start_time;
begin try drop table #session_number end try begin catch end catch;
select *, row_number() over (partition by panelist_key order by panelist_key, date_id, session_start_time asc) as panelist_session_num
into #session_number
from #session_number_prep
;
--(XX rows affected)

-- select top 100 * from ref.Device_Session_ID order by panelist_key, session_start_time, record_id;
begin try drop table ref.Device_Session_ID end try begin catch end catch;
select a.Record_ID, a.Panelist_Key, a.Date_ID, a.device_session_id, b.session_start_time,b.session_end_time, b.panelist_session_num
into ref.Device_Session_ID
from #device_session a
join #session_number b on a.Panelist_Key = b.Panelist_Key and a.device_session_id = b.device_session_id
;
----(XX rows affected)
*/
------------------------
--2) Pulls in ecosystem records
------------------------
-- select count(*) from rps.v_Report_Sequence_Events_P2P  where Ecosystem_Cat is not null;
--select top 100 * from rps.v_Report_Sequence_Events_P2P where Search_Term is not null and bdg_display not like '%Search%';
-- select top 100 * from #temp_sequence_ecosystem_v1;
begin try drop table #temp_sequence_ecosystem end try begin catch end catch;

select a.Record_ID,b.panelist_key, platform, digital_type, a.date_id, d.device_session_id,a.session_id as property_session_id,
	start_time,End_Time_Local as end_time,
	dur_seconds, 
	a.bdg_display as notes, case when bdg_display like '%Search%' then '' else a.Value end as value, -- dont want google url info, still want as 1 search multiple pages if URL changes
	 a.Search_Term,
	Ecosystem_Cat, Ecosytem_Subcategory,
	content_category_1, content_category_2, content_category_3
into #temp_sequence_ecosystem
from rps.v_Report_Sequence_Events_P2P a
join ref.Panelist b on a.Panelist_ID = b.Vendor_Panelist_ID
join ref.device_session_id d on a.record_id = d.record_id
where Ecosystem_Cat is not null
;
--(XX rows affected)

/* IF ANY REGROUPINGS FOR CLUSTER IN JOURNEY, MATCH THEM HERE
update #temp_sequence_ecosystem, set Ecosystem_Cat = 'XX', Ecosystem_Subcategory = 'XX'
where Ecosystem_Cat  = 'XX'
*/

------Process to update end time to parent for app searches
begin try drop table #raw_search_app_events end try begin catch end catch;
select vendor_session_id, record_id as child_record_id
into #raw_search_app_events
from raw.unified_app_events 
where search_term is not null
group by  vendor_session_id, record_id;
--(XX rows affected)

begin try drop table #search_parent_info end try begin catch end catch;
select a.*, b.record_id as parent_record_id, b.end_time_local as parent_end_time
into #search_parent_info
from #raw_search_app_events a
join raw.unified_app_events b 
on a.vendor_session_id = b.vendor_session_id 
and b.source = 'embee' -- source is embee for parent search and embgs for child
;
--(XX rows affected)

update #temp_sequence_ecosystem set end_time = parent_end_time
from #temp_sequence_ecosystem a join #search_parent_info b on a.Record_ID = b.child_record_id;
--(XX rows affected)

update #temp_sequence_ecosystem set dur_Seconds = DATEDIFF(ss, Start_Time,end_time) 
from #temp_sequence_ecosystem a join #search_parent_info b on a.Record_ID = b.child_record_id;
--(XX rows affected)


--------Final aggregation for multiple identical searches/URLs within same property session
begin try drop table #sequence_ecosystem end try begin catch end catch;
select min(Record_ID) as min_record_id,panelist_key, platform, digital_type, date_id,device_session_id,property_session_id,
	min(start_time) as start_time, max(end_time) as end_time, sum(dur_seconds) as dur_seconds,
	notes,Value,  isnull(Search_Term,'') as search_term,
	Ecosystem_Cat, Ecosytem_Subcategory,
	content_category_1, content_category_2, content_category_3
into #sequence_ecosystem
from #temp_sequence_ecosystem
group by panelist_key, platform, digital_type, date_id,device_session_id,property_session_id,
	notes,Value,  Search_Term,
	Ecosystem_Cat, Ecosytem_Subcategory,
	content_category_1, content_category_2, content_category_3
;
----(XX rows affected)

---------------Inserts in Purchase (as long as they have other ecosystem activity)
---------CONSIDER TAKING A MAX FOR SIMPLICITY OF JUST 1 BUY, OTHERWISE ALL DATA IS DUPLICATED
-- select top 100 * from #purchases;
begin try drop table #purchases end try begin catch end catch;
select a.panelist_key,
	max(a.purchase_date) as purchase_date
into #purchases
from ref.purchase_date a 
join #sequence_ecosystem b on a.Panelist_Key = b.Panelist_Key -- ensures they already have ecosystem activity
where a.purchase_date is not null
group by a.panelist_key
;
--(XX rows affected)

/*
-- Pulls the last ecosystem session id on date of purchase
-- select top 100 * from #last_eco_session;
begin try drop table #last_eco_session end try begin catch end catch;
select a.Panelist_Key, a.purchase_date, max(device_session_id) as last_session, max(end_time) as last_end_time
into #last_eco_session
from #purchases a
left join #temp_sequence_ecosystem b on a.Panelist_Key = b.Panelist_Key and a.purchase_date = b.Date_ID
group by a.Panelist_Key, a.purchase_date;


--If no ecosystem session on day just makes last session in day
begin try drop table #last_overall_session end try begin catch end catch;
select b.Panelist_Key, b.purchase_date, max(device_session_id) as last_session, max(session_end_time) as last_end_time
into #last_overall_session
from ref.Device_Session_ID a
join #purchases b on a.Panelist_Key = b.Panelist_Key and a.Date_ID = b.purchase_date
group by b.Panelist_Key, b.purchase_date
;

update #last_eco_session set last_session = b.last_session, last_end_time = b.last_end_time
from #last_eco_session a
join #last_overall_session b on a.panelist_key = b.panelist_key and a.purchase_date = b.purchase_date and a.last_session is null
;
-- select * from #last_eco_session;
*/

-- select top 100 * from #sequence_ecosystem;
insert into #sequence_ecosystem
select 0 as min_record_id,
	 a.panelist_key, 
	'All Devices' as platform,
	'All Digital' as digital_type,
	cast(purchase_date as date) as date_id,
	0 as session_id,
	0 as property_session_id,
	purchase_date AS start_time,
	purchase_date as end_time,
	0 as dur_seconds,
	'[Buy]' as notes,
	'[Buy]' as value,
	'' as search_term,
	'[Buy]' as Ecosystem_cat,
	'[Buy]' as Ecosystem_Subcategory,
	'[Buy]' as content_category_1,
	'[Buy]' as content_category_2,
	'[Buy]' as content_category_3
from #purchases a;

------------set to next day then two seconds earlier to get last min of day
update #sequence_ecosystem set start_time = DATEADD(DAY,1,cast(date_id as date)), end_time = DATEADD(DAY,1,cast(date_id as date))  where notes = '[Buy]';
update #sequence_ecosystem set start_time = DATEADD(SECOND,-2,start_time), end_time= DATEADD(SECOND,-2,end_time)   where notes = '[Buy]';


-----Deletes buy if it is the first event
-- select top 100 * from  #touch_num_prep order by panelist_key, panelist_touch_num;
begin try drop table #touch_num_prep end try begin catch end catch;
select a.*, row_number() over (partition by panelist_key order by  panelist_key, start_time) as panelist_touch_num
into #touch_num_prep
from #sequence_ecosystem a
;

delete from #sequence_ecosystem
from #sequence_ecosystem a
join #touch_num_prep b on a.Panelist_Key = b.Panelist_Key and a.min_record_id = b.min_record_id and a.start_time = b.start_time
where b.panelist_touch_num = 1 and b.notes = '[Buy]'
;

------------------------
--3) Countdown Values
------------------------
--select top 100 * from #panelist_touch_num order by panelist_key, panelist_touch_num;
begin try drop table #panelist_touch_num end try begin catch end catch;
select a.*, row_number() over (partition by panelist_key order by  panelist_key, start_time) as panelist_touch_num,
row_number() over (partition by panelist_key,device_session_id order by  panelist_key, start_time) as session_touch_num
into #panelist_touch_num
from #sequence_ecosystem a
;


-------If they bought multiple times, we count them as two separte journeys
-- select top 100 * from #buy_events;
begin try drop table #buy_events end try begin catch end catch;
select a.*,ROW_NUMBER() over (partition by panelist_key order by panelist_key, start_time desc) as buy_event
into #buy_events
from #panelist_touch_num a
where a.notes = '[Buy]';
--(1044 rows affected)

-- select top 100 * from #relative_to_buy order by panelist_key, panelist_touch_num;
begin try drop table #relative_to_buy end try begin catch end catch;
select a.*,
case when a.panelist_touch_num = b.panelist_touch_num  then 0
	when a.panelist_touch_num< b.panelist_touch_num then a.panelist_touch_num - b.panelist_touch_num 
	end as buy_countdown,
case when a.panelist_touch_num = b.panelist_touch_num  then 0
	when a.panelist_touch_num< b.panelist_touch_num then datediff(day,b.date_id,a.Date_ID)
	end as days_til_buy
into #relative_to_buy
from #panelist_touch_num a
left join #buy_events b
on a.panelist_key=b.panelist_key
	--and b.buy_event = 1   --gets all things including previous buy events relative to last buy event (since buy event number is desc)
	and  a.start_time <= b.start_time
;

------------------------
--4) Groups into 3 stages
------------------------
-- select top 100 * from #session_ordering order by panelist_key, session_start_time;
begin try drop table #session_ordering end try begin catch end catch;
select a.Panelist_Key, a.device_session_id, a.session_start_time, 
row_number() over (partition by a.Panelist_Key order by  a.Panelist_Key, session_start_time) as panelist_session_number
into #session_ordering
from ref.Device_Session_ID a
join #relative_to_buy b on a.device_session_id = b.device_session_id and a.Panelist_Key = b.Panelist_Key
where buy_countdown is not null -------only data leading up to buy
group by a.Panelist_Key, a.device_session_id, a.session_start_time
;

--------Splits the sessions into 3 groups
-- select top 100 * from #panelist_session_num order by panelist_key, device_session_id;
begin try drop table #panelist_session_num end try begin catch end catch;
select Panelist_Key,device_session_id,panelist_session_number,
ntile(3) over(partition by panelist_key order by panelist_session_number desc)journey_session_group_prep
into #panelist_session_num
from #session_ordering
;


-- select top 100 * from #stage_grouping order by panelist_key, panelist_touch_num ;
begin try drop table #stage_grouping end try begin catch end catch;
select a.*,case when notes = '[Buy]' then 0 else panelist_session_number end as panelist_session_number,
case when a.device_session_id = 0 then 0
when journey_session_group_prep = 3 then -3
when journey_session_group_prep = 2 then -2
when journey_session_group_prep = 1 then -1
end as journey_session_group
into #stage_grouping
from #relative_to_buy a
left join #panelist_session_num b on a.Panelist_Key = b.Panelist_Key and a.device_session_id = b.device_session_id
--where a.buy_countdown is not null -------only data leading up to buy (need to do again since buy can be in middle of session)
;

-----------For people that only have 1 or 2 sessions it uses touchpoints (based on property session id)
-- select top 100 * from #panelist_number_sessions order by panelist_key;
begin try drop table #panelist_number_sessions end try begin catch end catch;
select Panelist_Key, max(panelist_session_number) as panelist_number_sessions
into #panelist_number_sessions
from #panelist_session_num
group by Panelist_Key;

begin try drop table #repull end try begin catch end catch;
select a.Panelist_Key,a.property_session_id, min(a.start_time) as property_start_time
into #repull
from #stage_grouping a join #panelist_number_sessions b on a.Panelist_Key = b.Panelist_Key and b.panelist_number_sessions in (1,2) 
and a.property_session_id ! = 0 and buy_countdown is not null -------only data leading up to buy
group by a.Panelist_Key,a.property_session_id;

-- select top 100 * from #property_session_ordering order by panelist_key, property_start_time;
begin try drop table #property_session_ordering end try begin catch end catch;
select *,
row_number() over (partition by Panelist_Key order by  Panelist_Key, property_start_time) as property_session_number
into #property_session_ordering
from #repull
;

--------Splits the sessions into 3 groups
-- select top 100 * from #property_session_num order by panelist_key, property_session_number;
begin try drop table #property_session_num end try begin catch end catch;
select *,
ntile(3) over(partition by panelist_key order by property_session_number desc)journey_session_group_prep
into #property_session_num 
from #property_session_ordering
;

-- select top 100 * from #update_info order by panelist_key, property_session_number;
begin try drop table #update_info end try begin catch end catch;
select *,case 
when journey_session_group_prep = 3 then -3
when journey_session_group_prep = 2 then -2
when journey_session_group_prep = 1 then -1
end as journey_session_group
into #update_info
from #property_session_num;

update #stage_grouping set journey_session_group = b.journey_session_group
from #stage_grouping a join #update_info b on a.Panelist_Key = b.Panelist_Key and a.property_session_id = b.property_session_id;
------------------------
--5) Semantic Match for Brand Count
------------------------

-- select top 100 *
begin try drop table #url_gra end try begin catch end catch;
select min_record_id,value,Search_Term, core.dbo.alphanum(value+Search_Term) as gra_match
into #url_gra
from #stage_grouping
where notes ! = '[Buy]'
group by min_record_id,value,Search_Term;


-- select * from #search_term_gra_best_pattern;
begin try drop table #url_gra_best_pattern end try begin catch end catch;

select min_record_id, min(pattern_id) as best_pattern_id
into #url_gra_best_pattern
from #url_gra a
inner join ref.pattern_table b
	on a.gra_match like b.pattern
	and semantic_priority_group like '%brand%'
group by min_record_id;
-- (1091 rows affected)

-- select * from #semantic_match;
begin try drop table #semantic_match end try begin catch end catch;

select a.min_record_id, value, Search_Term,
	c.semantic_lemma as brand
into #semantic_match
from #url_gra a
inner join #url_gra_best_pattern b
	on a.min_record_id=b.min_record_id
inner join ref.pattern_table c
	on b.best_pattern_id=c.pattern_id
;
--(1091 rows affected)

------------------------
--5) Final Journey Highlights Table
------------------------
 -----------JOIN IN ALL SEGMENTATIONS (AS LONG AS THEY ARE MUTUALLY EXCLUSIVE, IF NOT use standard journey format where we add on additional rows)
begin try drop table rpt.Journey_Highlights end try begin catch end catch; 
select a.min_record_id, a.Panelist_Key, c.segment_name as 'SEGMENTATION_NAME_1', d.segment_name as 'SEGMENTATION_NAME_2', -- repeat as necessary
Platform, Digital_Type, Date_ID, device_session_id, property_session_id, start_time, cast(dur_seconds/60.0 as numeric(20,5)) as dur_minutes, notes, a.value, a.search_term,
Ecosystem_Cat,Ecosytem_Subcategory, content_category_1, content_category_2, content_category_3,
isnull(b.brand,'') as brand, case when b.brand is null then '' else CONCAT(a.Panelist_Key,'.',b.brand) end as user_by_brand,
panelist_touch_num,session_touch_num, buy_countdown, days_til_buy, panelist_session_number,journey_session_group 
into rpt.Journey_Highlights
from #stage_grouping a
left join #semantic_match b on a.min_record_id = b.min_record_id
left join ref.Panelist_Segmentation c on a.Panelist_Key = c.Panelist_key and c.segmentation_id = 'XX'
left join ref.Panelist_Segmentation d on a.Panelist_Key = d.Panelist_key and d.segmentation_id = 'XX'
;

-- select top 100 * from rpt.Journey_Highlights;

------------------------
--FINAL EXPORT
------------------------
select *
from rpt.Journey_Highlights 
order by Panelist_Key, panelist_touch_num;