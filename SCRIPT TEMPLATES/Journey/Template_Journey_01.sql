---------------------------------------------------------------------------------------------------
--Journey: PROJECT NAME
--Owner
--Date
---------------------------------------------------------------------------------------------------

-- 0) DEFINE GLOBAL VARIABLE
---------------------------------------------------------------------------------------------------
use [PROJECTNAME];


-- 1) MAKE SESSION LEVEL VIEW OF SEQUENCE EVENT
---------------------------------------------------------------------------------------------------
-- Remove entries designated as not relevant to journey.
-- select * from run.v_Sequence_Event_Ecosystem; -- 764
-- select * from #temp_sequence_ecosystem order by tax_id; 
-- select top 100 * from ref.taxonomy_ecosystem;
-- select * from #ecosystem_lookup;
-- select top 100 * from #cluster_id;
begin try drop table #cluster_id end try begin catch end catch;
select category, cat_id
into #cluster_id
from ref.Taxonomy_Ecosystem
group by category, cat_id;

begin try drop table #temp_sequence_ecosystem end try begin catch end catch;

select record_id,panelist_key, platform, digital_type, date_id, session_id,
	start_time, cast(dur_seconds/60.0 as numeric(20,5)) as dur_minutes, 
	a.notes, a.value,c.cat_id+2000 as cluster_id,Ecosystem_Cat as cluster, Ecosytem_Subcategory as subcategory
into #temp_sequence_ecosystem
from rps.v_Report_Sequence_Events_P2P a
join ref.Panelist b on a.Panelist_ID = b.Vendor_Panelist_ID
join #cluster_id c on a.Ecosystem_Cat = c.category
where Ecosystem_Cat is not null
;

---------------IF WE WANT TO RECLUSTER AT ALL, UPDATE STATEMENTS HERE
-- select cluster, subcategory, count(*) as count from #temp_sequence_ecosystem group by cluster, subcategory order by cluster, subcategory;
--update #temp_sequence_ecosystem set cluster = 'XX', cluster_id = XX, where subcategory = 'XX'


-- select top 100 * from  #temp_sequence_session_ecosystem 
-- select * from #temp_sequence_ecosystem where panelist_key = 190 and session_id = 66327;
begin try drop table #temp_sequence_session_ecosystem end try begin catch end catch;

select panelist_key, platform, digital_type, date_id, session_id, 
	min(start_time) as start_time, sum(dur_minutes) as dur_minutes, count(*) as ct_events 
into #temp_sequence_session_ecosystem
from #temp_sequence_ecosystem a
group by panelist_key, platform, digital_type, date_id, session_id;


create index temp_sequence_session_ecosystem1 on #temp_sequence_session_ecosystem (panelist_key);
create index temp_sequence_session_ecosystem2 on #temp_sequence_session_ecosystem (session_id);
create index temp_sequence_session_ecosystem3 on #temp_sequence_session_ecosystem (start_time);


-- particle detail.... but duplicates removed for cases where session timestamp is identical
---------------------------------------------------------------------------------------------

---Cant take multiple mins at once so takes min record id and then maps back for info
begin try drop table #temp_representative_ids_that_millisecond end try begin catch end catch;

select panelist_key, session_id, start_time as start_time,min(Record_ID) as min_record_id,--min(notes) as notes, min(value) as value, min(cluster) as cluster, min(subcategory) as subcategory,
	count(*) as simultaneous_pages
into #temp_representative_ids_that_millisecond
from #temp_sequence_ecosystem a
group by panelist_key, session_id, start_time;


create index temp_representative_ids_that_millisecond1 on #temp_representative_ids_that_millisecond (panelist_key);
create index temp_representative_ids_that_millisecond2 on #temp_representative_ids_that_millisecond (session_id);
create index temp_representative_ids_that_millisecond3 on #temp_representative_ids_that_millisecond (start_time);


begin try drop table #temp_sequence_session_ecosystem2 end try begin catch end catch;

select a.panelist_key, a.platform, a.digital_type, a.date_id, a.session_id, a.start_time, a.dur_minutes,
	cast(ct_events as numeric(20,3)) as ct_events,min_record_id,
	c.notes, c.value,c.cluster_id, c.cluster, c.subcategory
into #temp_sequence_session_ecosystem2
from #temp_sequence_session_ecosystem a
inner join #temp_representative_ids_that_millisecond b
	on a.panelist_key=b.panelist_key
	and a.session_id = b.session_id
	and a.start_time = b.start_time
inner join #temp_sequence_ecosystem c on b.min_record_id = c.Record_ID
;

-- select min_record_id, count(*) as count from #temp_sequence_session_ecosystem2 group by min_record_id order by count desc;

----------------------------------------------------------------------------------------
-- 2) Insert stated purchase dates
-----------------------------------------------------------------------------------------
-- select top 100 * from #temp_sequence_session_ecosystem2;
-- select * from #ecosystem_lookup where cluster_id<=999;
-- select * from ref.respondent_data;
-- select distinct subcategory, internal_group from #ecosystem_lookup


------Pulls purchase date and joins to get panelist key. Make sure you get the purchases from whatever purchase table is created for this project

begin try drop table #purchases end try begin catch end catch;
select a.panelist_key,
	a.purchase_date
into #purchases
from ref.purchase_date a 
join #temp_sequence_session_ecosystem2 b on a.Panelist_Key = b.Panelist_Key -- ensures they already have ecosystem activity
where a.purchase_date is not null
group by a.panelist_key,
	a.purchase_date;

-- select top 5 * from #temp_sequence_session_ecosystem2;
insert into #temp_sequence_session_ecosystem2
select a.panelist_key, 
	'All Devices' as platform,
	'All Digital' as digital_type,
	purchase_date as date_id,
	0 as session_id,
	purchase_date AS start_time, -- chose last minute of buy date
	0 as dur_minutes,
	1 as ct_events,
	NULL as min_record_id,
	'[Buy]' as notes,
	'[Buy]' as value,
	1 as cluster_id,
	'[Buy]' as cluster,
	'[Buy]' as subcategory
from #purchases a;



------------set to next day then two seconds earlier to get last min of day
update #temp_sequence_session_ecosystem2 set start_time = DATEADD(DAY,1,start_time) where cluster_id = 1 ;
update #temp_sequence_session_ecosystem2 set start_time = DATEADD(SECOND,-2,start_time) where cluster_id = 1 ;


---------------------------------------------------------------------------------------------------------------------------
-- 3) Insert end touchpoint session
-- Note: This should run after the stated purchases are inserted, because we want opportunity for purchase to be end event.
---------------------------------------------------------------------------------------------------------------------------
insert into #temp_sequence_session_ecosystem2
select a.panelist_key,
	'All Devices' as platform,
	'All Digital' as digital_type, max(a.Date_ID) as date_id, 0 as session_id,
	dateadd(SECOND,1,max(a.start_time)) AS start_time, -- choose time right after last event
	0 as dur_minutes, 0 as ct_events,NULL as min_record_id, '[End]' as notes,
	'[End]' as value,
	0 as cluster_id,
	'[End]' as cluster,
	'[End]' as subcategory
from #temp_sequence_session_ecosystem2 a
group by a.Panelist_Key;




/* ---if country included in study
begin try drop table #panelist_country end try begin catch end catch;
select a.*, b.Country
into #panelist_country
from #temp_sequence_session_ecosystem2 a
join ref.Respondent_Data b
on a.Panelist_Key = b.panelist_key;
*/


-- 4) APPLY SEGMENT
---------------------------------------------------------------------------------------------------
-- drop table #panelist_touchpoints;
/**
select segmentation_id, segmentation_name, segment_id, segment_name, count(*) as ct_records
from ref.Panelist_Segmentation
group by segmentation_id, segmentation_name, segment_id, segment_name
;
***/
/**
select a.segmentation_id, a.segmentation_name, a.segment_id, a.segment_name, count(distinct b.panelist_key) as ct_records
from ref.Panelist_Segmentation a join #temp_sequence_session_ecosystem2 b
	on a.Panelist_Key=b.panelist_key
group by a.segmentation_id, a.segmentation_name, a.segment_id, a.segment_name
order by segment_id
;
***/

----------------Adds in panelist country
begin try drop table #panelist_country end try begin catch end catch;
select a.*, 'XX' as country
into #panelist_country
from #temp_sequence_session_ecosystem2 a
;

begin try drop table #panelist_touchpoints end try begin catch end catch;

select segment_name, a.panelist_key
into #panelist_touchpoints
from #panelist_country a
inner join PROJECTNAME.ref.Panelist_Segmentation b
	on a.Panelist_Key=b.panelist_key
	and segmentation_name in ('*Total Survey*','INSERT ALL RELEVANT SEGMENTATIONS')
group by country, a.Panelist_Key,segment_name;

create index panelist_touchpoints1 on #panelist_touchpoints(panelist_key);


begin try drop table #temp_sequence_session_ecosystem3 end try begin catch end catch;
select b.segment_name, a.*
into #temp_sequence_session_ecosystem3
from #panelist_country a
inner join #panelist_touchpoints b
	on a.panelist_key=b.panelist_key;

------------------------------------------------------------------------------------------
-- 5) Remove panelists with only 1 touchpoint in ecosystem (therefore no path, no journey)
------------------------------------------------------------------------------------------------------------

begin try drop table #temp_content_panelist_group_counts end try begin catch end catch;
select segment_name,country, panelist_key, count(distinct subcategory) as ct_groups
into #temp_content_panelist_group_counts
from #temp_sequence_session_ecosystem3
where cluster_id ! = 0-- endpoints do not count
group by segment_name,country, panelist_key;


create index temp_content_panelist_group_counts1 on #temp_content_panelist_group_counts (panelist_key);
create index temp_content_panelist_group_counts2 on #temp_content_panelist_group_counts (segment_name);



-- QA... Sample size check to see how many panelist make it to this point. Should not deviate too much from Standard Metrics Ecosystem. If anything could be higher.
--select count(*) from #temp_content_panelist_group_counts where segment_name=1;				
--select count(*) from #temp_content_panelist_group_counts where segment_name=1 and ct_groups>1;	


------------------------------------------------------------------------------------------
-- 6) PARTITION BY PERSON. AND ALSO VERTICAL IF SUB-ECOSYSTEMS EXIST (e.g. Verticals within Shopping Retail)
------------------------------------------------------------------------------------------------------------
-- select top 100 * from #temp_sequence_session_ecosystem3;
-- select top 100 * from #partitioned_points_ordered where panelist_key = 155;
begin try drop table #partitioned_points_ordered end try begin catch end catch;
select row_number() over (partition by a.segment_name, a.panelist_key order by a.segment_name, a.panelist_key, a.date_id, a.start_time asc) as row_num,
	a.segment_name, a.country, a.panelist_key, a.platform, a.digital_type, a.date_id, a.session_id, a.start_time,a.cluster_id,a.cluster, a.subcategory, a.Notes as property, a.min_record_id
into #partitioned_points_ordered
from #temp_sequence_session_ecosystem3 a 
inner join #temp_content_panelist_group_counts b
	on a.Panelist_Key=b.Panelist_Key
	and a.segment_name=b.segment_name
	and b.ct_groups>1
;


begin try drop table #agg_paths end try begin catch end catch;
select a.segment_name,a.Country, a.panelist_key, a.session_id, a.platform, a.digital_type, 
	a.date_id, a.start_time,a.cluster_id,a.cluster, a.subcategory, a.property, a.min_record_id,
	isnull(b.platform, a.platform) as prev_platform,
	isnull(b.digital_type, a.digital_type) as prev_digital_type,
	isnull(b.date_id, a.date_id) as prev_day_id,
	isnull(b.start_time, dateadd(minute,-1,a.start_time)) as prev_start_time,
	isnull(b.cluster_id,0) as prev_cluster_id,
	isnull(b.cluster, '') as prev_cluster,
	isnull(b.subcategory, '') as prev_subcategory,
	isnull(b.property,'') as prev_property
into #agg_paths
from #partitioned_points_ordered a
left join #partitioned_points_ordered b
	on a.segment_name=b.segment_name
	and a.panelist_key=b.panelist_key
	and a.row_num=b.row_num+1;



/*** CHECK JOURNEY PATH DISTRIBUTION ***
	select segment_name, cluster, subcategory, count(*) as ct_visit
	from #agg_paths
	group by segment_name,cluster, subcategory
	order by 1,2,3;
 ***/


-- 7) JOIN TO LOOKUPS AND EXPORT
------------------------------------------------------------------------------------------------------------
-- select top 100 * from #journey_report order by segment_name, panelist_key, start_time;
-- select top 100 * from #agg_paths where panelist_key=82 order by panelist_key, start_time;
-- select top 100 * from #temp_sequence_session_ecosystem3 order by content_vertical_id, panelist_key, start_time;
-- select top 100 * from #temp_sequence_session_ecosystem2 order by panelist_key, start_time;
-- select * from #agg_path2 where panelist_key = 155 order by segment_name, start_time;

begin try drop table #agg_path2 end try begin catch end catch;
select segment_name, a.country, a.panelist_key, Platform, start_time,
	cluster_id,cluster,subcategory, 
	case when property = '' then cluster else property end as property, min_record_id,
	prev_start_time, prev_platform,
	case when prev_cluster_id = '' then 0 else prev_cluster_id end as prev_cluster_id,
	case when prev_cluster = '' then '[Start]' else prev_cluster end as prev_cluster,
	case when prev_subcategory = '' then '[Start]' else prev_subcategory end as prev_subcategory,
	case when prev_property = '' and prev_cluster = '' then '[Start]' 
		when  prev_property = '' then prev_cluster
		else prev_property end as prev_property
into #agg_path2
from #agg_paths a
where (a.subcategory <> a.subcategory) 
or ((a.prev_day_id <> a.date_id) and a.cluster_id != 1) -- remove internals
;

-- identify first event
begin try drop table #panelist_first_event_time_v1 end try begin catch end catch;
select segment_name, Panelist_Key, min(start_time) as first_event_time
into #panelist_first_event_time_v1
from #agg_path2
group by segment_name, Panelist_Key;


create index panelist_first_event_time1 on #panelist_first_event_time_v1 (panelist_key);
create index panelist_first_event_time2 on #panelist_first_event_time_v1 (segment_name);
create index panelist_first_event_time3 on #panelist_first_event_time_v1 (first_event_time);



-- remove records where buy is first event;

begin try drop table #agg_path3 end try begin catch end catch;
select a.*
into #agg_path3
from #agg_path2 a
left outer join #panelist_first_event_time_v1 b
	on a.segment_name=b.segment_name
	and a.Panelist_Key=b.Panelist_Key
	and a.start_time=b.first_event_time
	and a.cluster_id=1 -- buy records only
where b.panelist_key is null
;


create index temp_sequence_session_ecosystem1 on #agg_path3 (panelist_key);
create index temp_sequence_session_ecosystem2 on #agg_path3 (segment_name);


-- identify first event

begin try drop table #panelist_first_event_time_v2 end try begin catch end catch;
select segment_name,country, Panelist_Key, min(start_time) as first_event_time
into #panelist_first_event_time_v2
from #agg_path3
group by segment_name,country, Panelist_Key;


create index panelist_first_event_time1 on #panelist_first_event_time_v2 (panelist_key);
create index panelist_first_event_time2 on #panelist_first_event_time_v2 (segment_name);
create index panelist_first_event_time3 on #panelist_first_event_time_v2 (first_event_time);


begin try drop table #journey_report end try begin catch end catch;
select a.segment_name,a.Country, a.Panelist_Key, a.platform, a.start_time, a.cluster, a.cluster_id, a.subcategory, a.property,a.min_record_id, a.prev_start_time, a.prev_platform,
case when (b.Panelist_Key is not NULL and a.prev_cluster = '[Buy]') then '[Start]' 
else a.prev_cluster end as prev_cluster,
case when (b.Panelist_Key is not NULL and a.prev_cluster = '[Buy]') then 0 
else a.prev_cluster_id end as prev_cluster_id,
case when (b.Panelist_Key is not NULL and a.prev_cluster = '[Buy]') then '[Start]' 
else a.prev_subcategory end as prev_subcategory,
case when (b.Panelist_Key is not NULL and a.prev_cluster = '[Buy]') then '[Start]' 
else a.prev_property end as prev_property
into #journey_report
from #agg_path3 a
left outer join #panelist_first_event_time_v2 b
	on a.segment_name=b.segment_name
	and a.Panelist_Key=b.Panelist_Key
	and a.start_time=b.first_event_time
;

create index temp_sequence_session_ecosystem1 on #journey_report (panelist_key);
create index temp_sequence_session_ecosystem2 on #journey_report(segment_name);

-----------------------------------------------------------------------------
-- Export
-----------------------------------------------------------------------------
-- Identify panelists who only had 1 activity after removing buy event (if buy was the first activity).

begin try drop table #event_count end try begin catch end catch;
select Panelist_Key,count(*) as event_count
into #event_count
from #journey_report
where cluster_id <> 0 -- end point doesn't count
group by Panelist_Key
;


/**
select panelist_key 
from #event_count
where event_count <=1
**/


begin try drop table rpt.Journey_Sessions end try begin catch end catch;
select 
	segment_name,
	a.Country,
	a.panelist_key,
	platform,
	start_time,
	cluster,
	cluster_id,
	subcategory,
	property,
	min_record_id,
	prev_start_time,
	prev_platform,
	prev_cluster,
	prev_cluster_id,
	prev_subcategory,
	prev_property
into rpt.Journey_Sessions
from #journey_report a
join ref.Panelist b
	on a.panelist_key = b.Panelist_Key
inner join #event_count c
	on a.Panelist_Key = c.Panelist_Key
	and c.event_count > 1
;

------------------------------------------------------
-- FINAL TABLE
------------------------------------------------------
select segment_name,
	Country,
	panelist_key,
	platform,
	start_time,
	cluster,
	cluster_id,
	subcategory,
	property,
	prev_start_time,
	prev_platform,
	prev_cluster,
	prev_cluster_id,
	prev_subcategory,
	prev_property
from rpt.Journey_Sessions
order by segment_name,Panelist_Key, start_time;
------------------------------------------------------



---------------------------------------------------------------------------------
-- ***QA for panelist counts: compare # of panelists in journey VS ecosystem.***
---------------------------------------------------------------------------------
-- 1. # Panelists in journey:
	/**
	select segment_name, cluster, count(*) as ct_records, count(distinct panelist_key) as ct_panelists
	from #journey_report
	group by segment_name, cluster
	order by segment_name;
	**/

-- Copy journey panelist_key list to excel:
	-- select distinct Panelist_Key from rpt.journey_sessions;

-- 2. # Panelists in ecosystem standard metrics
-- Use * ecosystem standard metrics * script
-- Copy ecosystem panelist_key list to excel:
	-- select distinct Panelist_Key from rpt.person_ecosystem_category;

-- 3. Copy lists of panelists to Excel, vlookup & check missing panelists

-- 4. QA for missing panelist in journey:
-- check if the panelist is in sequence_event/sequence_event_ecosystem.
	-- select * from [run].[Sequence_Event] where Panelist_Key = 1198;
	-- select * from [run].[v_Sequence_Event_Ecosystem] where Panelist_Key = 1198;

-- check where did we lose the panelist:
	-- select * from #temp_sequence_session_ecosystem4 where panelist_key = 38;
	-- select * from #temp_content_panelist_group_counts where panelist_key = 38;
	-- select * from #temp_sequence_session_ecosystem5 where panelist_key = 38; 

-- 5. Find instances of valid event:
-- Check if the panelist is qualified
	-- select * from  [run].[User_Qualification] where Panelist_Key = 1198 and Daily_Qualified = 1

-- Find instances of activity records
	-- select * from [wrk].[Taxonomy_Ecosystem_Mapping] where digital_type = 235 and Record_ID = '1474028'

-- Find the vendor panelist ID associated with panelist key
/**
	select [Panelist_Key],[Vendor_Panelist_ID],[Panelist_Hash],[Exclusion_Flag]
	FROM ref.Panelist
	where Panelist_Key = 638;
**/

-- Get search domains and urls to see if there are relevant activities (original code in P2P)
	-- select * from #p2p_search;
	-- drop table #p2p_search;
/**
	select
		record_id, 
		DATEPART(dw,date_id) as dow,
		panelist_id, 
		digital_type, 
		platform,
		notes as domain_or_app, 
		value as page, 
		search_term, 
		date_id as date, 
		start_time, 
		end_time_local, 
		dur_seconds,
		content_category,
		Ecosystem_Cat, 
		Ecosytem_Subcategory,
		bdg_display as ecosystem_record
	into #p2p_search
	from [run].[v_report_sequence_events_p2p]
		--where 
		--ecosystem_cat is not null 
		order by start_time;
**/

	-- select * from  #p2p_search where Panelist_ID = '41449' order by date;
/*
---Additional QA. Check if missing from journey are from those with only 1 touchpoint
	select distinct a.Panelist_Key
	from #temp_sequence_session_ecosystem3 a
	left join #temp_sequence_session_ecosystem5 b
	on a.Panelist_Key = b.Panelist_Key
	where b.Panelist_Key is null;

--Check if those missing from ecosystem are just buyers
 select panelist_key
 from ref.Respondent_Data 
 where purchase_date is not null
 order by panelist_key;

 -- check what journey activity is for those missing
 select * from #journey_report where Panelist_Key = 936 order by segment_name, start_time;

 --Get people who are in data but not in segmentation
 select distinct a.Panelist_Key
 from #journey_report a
 left join ref.panelist_segmentation b
 on a.Panelist_Key = b.panelist_key
 where b.panelist_key is null;

 -- Make sure dif betw eco and journey is just qual
 select distinct a.Panelist_Key 
 into #temp
 from rpt.Journey_Sessions a
 left join Samsung2019.Rpt.person_ecosystem_category b
 on a.Panelist_Key = b.panelist_key
 where b.panelist_key is null;
 --(34 rows affected)

 select a.*
 from #temp a
 left join wrk.panelist_platform_qual_day b
 on a.Panelist_Key = b.panelist_key
 where b.panelist_key is not null;
 
 select * from ref.Respondent_Data where panelist_key = 38;
 
 ---------Dif betwen userbyday and journey
 --Those in userbyday but not journey
 -- drop table #temp1
 select distinct a.panelist_key
 into #temp1
 from run.UserByDay_Total_Ecosystem a
 left join rpt.Journey_Sessions b
 on a.panelist_key = b.Panelist_Key 
 where b.Panelist_Key is null
 and a.Panelist_Key is not null;
 --(221 rows affected)

 --People removed for one touchpoint
 -- drop table #temp2
select distinct a.Panelist_Key
into #temp2
from #temp_sequence_session_ecosystem3 a
left join #temp_sequence_session_ecosystem5 b
on a.Panelist_Key = b.Panelist_Key
where b.Panelist_Key is null;
--(277 rows affected)

--Those missing who are not from one touchpoint
select a.*
from #temp1 a
left join #temp2 b
on a.Panelist_Key = b.Panelist_Key
where b.Panelist_Key is null;

--Removed but also not in eco
select a.Panelist_Key, a.Date_ID
into #temp3
from #temp_sequence_session_ecosystem3 a
left join #temp_sequence_session_ecosystem5 b
on a.Panelist_Key = b.Panelist_Key
where b.Panelist_Key is null
group by a.Panelist_Key, a.Date_ID;
--(401 rows affected)

select a.*
from #temp3 a
left join wrk.panelist_platform_qual_day b
on a.Panelist_Key = b.panelist_key and a.Date_ID = b.Date_ID
where (b.panelist_key is null and b.Date_ID is null);

select a.*
from #temp2 a
left join #temp1 b
on a.Panelist_Key = b.Panelist_Key
where b.Panelist_Key is null;

*/
-- END OF FILE --