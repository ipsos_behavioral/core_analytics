---------------------------------------------------------------------------------------------------
-- OWNER NAME
-- DATE
---------------------------------------------------------------------------------------------------

use [PROJECTNAME];
declare @table_journey varchar(300)		 = '[PROJECTNAME].[rpt].[Journey_Sessions]';
declare @table_country varchar(300)		 = '[PROJECTNAME].[ref].[Panelist]';
declare @sql varchar(3000);

begin try drop table #nodes end try begin catch end catch;
begin try drop table #edges_preppy end try begin catch end catch;
begin try drop table #edges_total end try begin catch end catch;
begin try drop table #edges_final end try begin catch end catch;

-- Note: Starts included

-- #temp_table represents local temp tables.
-- ##temp_table represents global temp tables.
-- one option is to use ## inside dynamic script, but it can cause collision. insert into temp tables instead.

------------------------------------------------------------------------------------------------------------
-- 0) CREATE TEMP VERSIONS OF DYANAMIC TABLES
------------------------------------------------------------------------------------------------------------
-- select * from rpt.Journey_Sessions where prev_cluster = '[Start]';
-- select * from rpt.Journey_Sessions where prev_cluster_id = 1;


-- select top 100 * from #table_journey;
begin try drop table #table_journey end try begin catch end catch;
create table #table_journey(
	report_vertical	int,
	country varchar(300),
	panelist_key	int,
	platform_name	varchar(300),
	start_time		datetime,
	cluster			varchar(300),
	cluster_id		int,
	subcategory		varchar(300),
	property		varchar(300),
	prev_start_time	datetime,
	prev_platform_name	varchar(300),
	prev_cluster		varchar(300),
	prev_cluster_id		int,
	prev_subcategory	varchar(300),
	prev_property	varchar(300)	
	);


set @sql = 'select * from '+@table_journey+' ';

insert into #table_journey execute(@sql);

create index table_journey1 on #table_journey (panelist_key);
create index table_journey2 on #table_journey (country);
create index table_journey2 on #table_journey (cluster_id);
create index table_journey3 on #table_journey (subcategory);
create index table_journey4 on #table_journey (prev_cluster_id);
create index table_journey5 on #table_journey (prev_subcategory);




/*** Sizing
	select report_vertical, count(*) as ct_records
	from #table_journey
	group by report_vertical
	order by report_vertical;
***/

------------------------------------------------------------------------------------------------------------
-- 1) GEPHI REPORT VIEWS - CONTENT VERTICALS
------------------------------------------------------------------------------------------------------------

-- IDENTIFY POPULATION IN REPORT VERTICAL

begin try drop table #nodes_vertical end try begin catch end catch;
select report_vertical,
	cluster_id as id,
	cluster_id as cluster_color, -- numeric value to color nodes on
	cluster as label,
	count(*) as paths
into #nodes_vertical
from #table_journey a
where -- report_vertical=1 and 
	cluster_id!=0 -- exclude end touchpoint
	and subcategory <> prev_subcategory -- exclude self loop
group by report_vertical, cluster_id, cluster;


-- 2) GEPHI CHART NODES
------------------------------------------------------------------------------------------------------------

-- About Gephi Charts Notes
---------------------------
-- nodes with end removed. start is optional on project by project basis.
-- nodes should have various segments as columns (not rows), because it's easiest to update Gephi and preserve color.

-- This query helps you decide the segment
------------------------------------------

-------------------
-- ALL PANELISTS
-------------------

begin try drop table #nodes_all end try begin catch end catch;
select a.country,
	a.prev_cluster_id as ID,
	cast('All Panelists' as nvarchar(300)) as segment,
	a.prev_cluster_id as cluster_color, -- numeric value to color nodes on
	a.prev_cluster as label,
	count(distinct a.panelist_key) as [sample],
	0 as [weight],
	count(*) as weight_all_panelists
into #nodes_all
from #table_journey a	
 where report_vertical=1 
	and
	--prev_cluster_id!=0 and			-- comment out this line if you want start included
	subcategory <> prev_subcategory -- exclude self loop
group by country, prev_cluster, prev_cluster_id;


--------------------------------
-- INSERT SEGMENTATION BELOW -- 
--------------------------------

----------------------
-- SEGMENT NAME 1 -- 
----------------------
begin try drop table #nodes_SEGMENTNAME1 end try begin catch end catch;

select a.country,
	a.prev_cluster_id as ID,
	cast('SEGMENT NAME 1' as nvarchar(300)) as segment,
	a.prev_cluster_id as cluster_color,
	a.prev_cluster as label,
	count(distinct a.panelist_key) as [sample],
	0 as [weight],
	count(*) as weight_SEGMENTNAME1
into #nodes_SEGMENTNAME1
from #table_journey a
inner join [PROJECTNAME].ref.Panelist_Segmentation b
	on a.panelist_key = b.panelist_key
	and b.segment_id = ''
where report_vertical =1
	and subcategory <> prev_subcategory -- exclude self loop
group by country, prev_cluster, prev_cluster_id;


---------------------
-- SEGMENT NAME 2 -- 
---------------------
begin try drop table #nodes_SEGMENTNAME2 end try begin catch end catch;

select a.country,
	a.prev_cluster_id as ID,
	cast('SEGMENT NAME 2' as nvarchar(300)) as segment,
	a.prev_cluster_id as cluster_color,
	a.prev_cluster as label,
	count(distinct a.panelist_key) as [sample],
	0 as [weight],
	count(*) as weight_SEGMENTNAME2
into #nodes_SEGMENTNAME2
from #table_journey a
inner join [PROJECTNAME].ref.Panelist_Segmentation b
	on a.panelist_key = b.panelist_key
	and b.segment_id = ''
where report_vertical =1
	and subcategory <> prev_subcategory -- exclude self loop
group by country, prev_cluster, prev_cluster_id;



begin try drop table #nodes end try begin catch end catch;

select a.country, a.id, a.cluster_color, a.label, a.sample, a.weight, 
	a.weight_all_panelists,
	b.weight_SEGMENTNAME1,
	c.weight_SEGMENTNAME2
into #nodes
from #nodes_all a
left outer join #nodes_SEGMENTNAME1 b
	on a.id=b.id
	and a.cluster_color=b.cluster_color
	and a.label=b.label
	and b.segment='SEGMENT NAME 1'
left outer join #nodes_SEGMENTNAME2 c
	on a.id=c.id
	and a.cluster_color=c.cluster_color
	and a.label=c.label
	and c.segment='SEGMENT NAME 2';



-----------
-- EDGES 
----------- 

begin try drop table #edges_preppy end try begin catch end catch;

select a.country,
	a.report_vertical, 
	prev_cluster_id as [source],
	prev_cluster, 
	cluster_id as target,
	cluster,
	'directed' as type,
	case when a.report_vertical = 1 then 'All'
		when a.report_vertical=21 then 'SEGMENT NAME 1'
		when a.report_vertical=22 then 'SEGMENT NAME 2'
		else '???' end as segment,
	count(distinct a.panelist_key) as sample,
	cast(count(*) as numeric(20,3)) as paths
into #edges_preppy
from #table_journey a
where cluster <> prev_cluster -- exclude self loop
	-- and  prev_cluster_id!=0 -- comment this out if you want to include start 
	--	and cluster_id!=0 -- disconnects after buy	
group by country, report_vertical, prev_cluster_id, prev_cluster, cluster_id, cluster;



-- ## QA check ###
-- select * from #edges_preppy;
-- select * from #edges_source_total;

begin try drop table #edges_source_total end try begin catch end catch;
select country,
	report_vertical,
	prev_cluster,
	[source],
	cast(sum(paths) as decimal (10,2)) as total_source_paths
into #edges_source_total
from #edges_preppy
group by country, report_vertical, prev_cluster, source;
-- 21 rows affected


begin try drop table #edges_target_total end try begin catch end catch;
select country,
	report_vertical,
	[cluster],
	[target],
	cast(sum(paths) as decimal (10,2)) as total_target_paths
into #edges_target_total
from #edges_preppy
group by country, report_vertical, [cluster], [target];
-- 18 rows affected

begin try drop table #edges end try begin catch end catch;

select 	e.country,
	case when e.report_vertical = 1 then 'All'
	when e.report_vertical=21 then 'SEGMENT NAME 1 '
	when e.report_vertical=22 then 'SEGMENT NAME 2'
			else '???' end as report_vertical,
	e.[source], e.prev_cluster,
	e.[target], e.cluster,
	e.[type],
	segment as segment_name,
	cast(e.paths as int) as weight, -- option 1 as weight: path count
	-- cast((e.paths/s.total_source_paths)*100 as numeric(20,1)) as weight, -- option 2 as weight: out percentage
	e.[sample],
	cast(e.paths/s.total_source_paths as numeric(20,3)) as out_percent,
	cast(e.paths/s.total_source_paths as numeric(20,3)) as out_path_label,
	cast(e.paths/t.total_target_paths as numeric(20,3)) as in_percent,
	cast(e.paths/t.total_target_paths as numeric(20,3)) as in_path_label
into #edges
from #edges_preppy e
left join #edges_source_total s
	on e.report_vertical=s.report_vertical
	and e.source=s.source
left join #edges_target_total t
	on e.report_vertical=t.report_vertical
	and e.target=t.target;



-- QA check ###
-- outpercent > 1


-- 3) BUSINESS RULES TO FILTER
------------------------------------------------------------------------------------------------------------

declare @node_count	int  = 6; -- N = Number of touchpoints not counting start (clusters)
declare @min_sample	int  = 10; -- N = min sample # of panelists (people related)

/***
-- Effectively 3 ways path gets included in chart
-------------------------------------------------
(1) One Top Exit Rule.			A->B is included if B is the single largest path exiting A
(2) Two Way Significant Rule.	A->B is included if	B has higher than average exit (>= 100%/@node_count) and average exit (>= 50%/@node_count).
(3) Three Top Big Exits Rule.	A->B is included if	B is among top 3 paths, and has higher than average exit (>= 100%/@node_count)
(4) Strong Flow Rule.			A->B is included if	B is among top 3 paths, and the different between exit% and entry% is more than double.

-- Exclusions
-------------------------------------------------
(0) Self Loops Exclusion. Self loops are never counted in the above 3 selection rules.
(0) Low Sample Exclusion. Paths (and nodes) are excluded if @min_sample is not met.
 ***/



begin try drop table #partitioned_nonself_edges end try begin catch end catch;
select row_number() over (partition by report_vertical, segment_name, source order by report_vertical, segment_name, source, weight desc) as row_num,
	report_vertical, country, segment_name, source, prev_cluster, target, cluster, weight
into #partitioned_nonself_edges
from #edges a
where source<>target -- exclude self loop
order by report_vertical, country, segment_name, source, weight;


begin try drop table #edges_flagged end try begin catch end catch;

select report_vertical, country, source, prev_cluster, target, cluster, segment_name, type, concat(cast(round(out_path_label,2)*100 as int),'%') as label,
	weight, sample, out_percent, concat(cast(round(out_path_label,2)*100 as int),'%') as out_path_label,
	in_percent, concat(cast(round(in_path_label,2)*100 as int),'%') as in_path_label,
	0 as include_flag
into #edges_flagged
from #edges
order by report_vertical, country, source, segment_name, target;



-- UPDATE (3) Three Top Big Exits Rule.	A->B is included if	B is among top 3 paths, and has higher than average exit (>= 100%/@node_count)
update #edges_flagged
set include_flag = 3
from #edges_flagged a
inner join #partitioned_nonself_edges b
	on a.report_vertical=b.report_vertical
	and a.segment_name=b.segment_name
	-- and a.country=b.country
	and a.source=b.source
	and a.target=b.target
	and b.row_num <=3
	and a.out_percent >= (1.00/@node_count);

-- UPDATE (4) Strong Flow Rule. A->B is included if	B has higher than average exit (>= 100%/@node_count) and the different between exit% and entry% is more than double.
update #edges_flagged
set include_flag = 4
from #edges_flagged a
inner join #partitioned_nonself_edges b
	on a.report_vertical=b.report_vertical
	and a.segment_name=b.segment_name
	-- and a.country=b.country
	and a.source=b.source
	and a.target=b.target
	and b.row_num <=2
	and a.out_percent >= 2*a.in_percent;

-- UPDATE (2) Two Way Significant Rule.	A->B is included if	B has higher than average exit (>= 100%/@node_count) and average exit (>= 50%/@node_count).
update #edges_flagged
set include_flag = 2
from #edges_flagged a
where a.out_percent >= (1.00/@node_count) and a.in_percent >= (0.50/@node_count) and source<>target;


-- UPDATE (1) One Top Exit Rule. A->B is included if B is the single largest path exiting A
update #edges_flagged
set include_flag = 1
from #edges_flagged a
inner join #partitioned_nonself_edges b
	on a.report_vertical=b.report_vertical
	and a.segment_name=b.segment_name
	-- and a.country=b.country
	and a.source=b.source
	and a.target=b.target
	and b.row_num=1;



-- Exclusions
-------------------------------------------------
-- (0) Self Loops Exclusion. Self loops are never counted in the above 3 selection rules.
-- (0) Low Sample Exclusion. Paths (and nodes) are excluded if @min_sample is not met.
update #edges_flagged set include_flag = 0 from #edges_flagged where source=target;
update #edges_flagged set include_flag = 0 from #edges_flagged where weight<@min_sample;

-------Removes anything coming out of buy
update #edges_flagged set include_flag = 0 from #edges_flagged where prev_cluster = '[Buy]'; --(57 rows affected)

---------Adds in things going into buy with >5%
update #edges_flagged set include_flag = 5 from #edges_flagged where cluster = '[Buy]' and out_percent > = 0.045; --(38 rows affected)

select report_vertical, country, source, prev_cluster, target, cluster, segment_name, type, out_path_label as label, weight, sample, 
out_percent, out_path_label, in_percent, in_path_label, include_flag
from #edges_flagged order by report_vertical, segment_name, source, target;

select *, '1' as include_flag_node from #nodes_output order by country, id;


-- END OF FILE --