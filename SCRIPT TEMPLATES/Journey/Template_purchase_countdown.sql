--------------------------
--Purchase Countdown--
--OWNER
--------------------------

use GoogleAuto2019; 

--1) Starts with journey which includes buy
-- select top 100 * from #journey_session_pull order by panelist_key, session_id, start_time;
-- select top 100 * from dbo.Output_Journey;
-- select * from #journey_session_pull where panelist_key = 42;
begin try drop table #journey_session_pull end try begin catch end catch;

select report_vertical, panelist_key, start_time,
cluster, subcategory
into #journey_session_pull
from Googleauto2019.Rpt.journey_sessions
where cluster != '[End]';
--(32625 rows affected)

-----------Change report vertical to name
begin try drop table #journey_session_name end try begin catch end catch;
select 
case when a.report_vertical=1 then '*All Panelists*'
	when a.report_vertical=21 then 'Buyer'
	when a.report_vertical=22 then 'Non-Buyer'
	when a.report_vertical=31 then 'E-car Considerer'
	when a.report_vertical=32 then 'Not an E-car Considerer'
	else '' end as report_vertical,
panelist_key, start_time, cluster, subcategory
into #journey_session_name
from #journey_session_pull a;
--(32625 rows affected)

-- select top 100 * from #partitioned_points_ordered order by report_vertical, panelist_key, start_time desc;
-- select * from #partitioned_points_ordered where panelist_key = 42;
begin try drop table #partitioned_points_ordered end try begin catch end catch;
select row_number() over (partition by report_vertical, panelist_key order by report_vertical, panelist_key, start_time desc) as row_num,
	report_vertical, panelist_key, start_time, cluster, subcategory 
into #partitioned_points_ordered
from #journey_session_name a
order by report_vertical, panelist_key, start_time desc;
--(32625 rows affected)

-- select * from #buy_events  where panelist_key = 42;
-- select * from #partitioned_points_ordered where panelist_key = 42;
-- select top 100 * from #buy_events order by report_vertical, panelist_key, start_time desc;
begin try drop table #buy_events end try begin catch end catch;
select a.*,ROW_NUMBER() over (partition by panelist_key, report_vertical order by panelist_key, report_vertical, start_time desc) as buy_event
into #buy_events
from #partitioned_points_ordered a
where a.cluster = '[Buy]';
--(426 rows affected)


-- select top 100 * from #relative_to_buy order by report_vertical, panelist_key, start_time desc;
-- select top 100 * from #relative_to_buy where panelist_key = 3 and report_vertical = 1 order by start_time desc;
begin try drop table #relative_to_buy end try begin catch end catch;
select a.report_vertical, a.panelist_key, a. start_time, a.cluster, a.subcategory, a.row_num, b.buy_event,
concat(b.panelist_key, b.start_time) as session_id,
case when a.row_num = b.row_num  then 0
	when a.row_num > b.row_num then b.row_num - a.row_num
	end as position
into #relative_to_buy
from #partitioned_points_ordered a
left join #buy_events b
on a.report_vertical=b.report_vertical
	and a.panelist_key=b.panelist_key
	--and b.buy_event = 1   --gets all things including previous buy events relative to last buy event
where a.start_time <= b.start_time
;
--(7934 rows affected)

-- select top 100 * from #earliest_buy order by report_vertical, panelist_key, start_time desc;
-- select top 100 * from #earliest_buy where panelist_key = 2736 order by report_vertical, start_time desc;
begin try drop table #earliest_buy end try begin catch end catch;
select report_vertical, panelist_key, start_time, cluster, subcategory, max(position) as position
into #earliest_buy
from #relative_to_buy
group by report_vertical, panelist_key, start_time, cluster, subcategory
;
--(7934 rows affected)

-- select top 100 * from #position_buckets order by report_vertical, panelist_key, start_time desc;
begin try drop table #position_buckets end try begin catch end catch;
select a.*, case 
	when position = 0 then '0'
	when position = -1 then '-1'
	when position = -2 then '-2'
	when (-3 >= position and position >= -5 ) then '-3 to -5'
	when (-6 >= position and position >= -10 ) then '-6 to -10'
	when (-11 >= position and position >= -20 ) then '-11 to -20'
	when (-21 >= position and position >= -40 ) then '-21 to -40'
	when -41 >= position then '-41 and Beyond'
	end as position_bucket
into #position_buckets
from #earliest_buy a;
--(7934 rows affected)

--###########################
--Output for individual step data
--###########################
select * from #position_buckets order by report_vertical, panelist_key, start_time desc;

--------------Analysis to get purchase duration and steps

-- select top 100 * from #purchase_session order by report_vertical, panelist_key, start_time desc;
-- 5
begin try drop table #purchase_session end try begin catch end catch;
select a.report_vertical, a.panelist_key, a.start_time, a.cluster, a.subcategory, a.position, b.session_id
into #purchase_session
from #earliest_buy a
join #relative_to_buy b
on a.report_vertical = b.report_vertical
and a.panelist_key = b.panelist_key
and a.start_time = b.start_time
and a.position = b.position
--and a.report_vertical != 1
;
--(7934 rows affected)

-- select * from #step_collapse;
-- select * from #step_collapse where report_vertical = 300 and session_id = '1114Nov 25 2017  7:05AM'
-- select * from #purchase_session where report_vertical = 300 and session_id = '1114Nov 25 2017  7:05AM';
begin try drop table #step_collapse end try begin catch end catch;
select report_vertical,panelist_key, session_id, count(*)-1 as num_steps, cast(max(start_time)-min(start_time)+1 as int) as num_days ------------Added in the -1 for steps to not include buy
into #step_collapse 
from #purchase_session a
group by report_vertical, panelist_key, session_id;
--(426 rows affected)


--###########################
--Output for purchase session
--###########################
select * 
from #step_collapse
order by report_vertical,panelist_key, session_id;

--------------------End of Script---------------