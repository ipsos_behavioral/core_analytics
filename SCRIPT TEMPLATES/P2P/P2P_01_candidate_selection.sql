---- NAME OF OWNER
---- DATE

use [PROJECTNAME];

/*Counts all ecosystem related data by category/subcategory and by panelist. Will help determine who could be have a "good" P2P. 
If there are multiple content categories, make sure all are included*/
begin try drop table #panelist_ecosystem_activity end try begin catch end catch;

select panelist_id,panelist_key, count(distinct[Ecosystem_Cat]) as eco_cat_count, 
	count(distinct[Ecosytem_Subcategory]) as eco_subcat_count,
	count(distinct[content_category_1]) as eco_content_count,
	count(*) as total_ecosystem_touches
into #panelist_ecosystem_activity
from [PROJECTNAME].[run].[v_report_sequence_events_p2p] a --can also use v_report_sequence_event_ecosystem but stephen/sami should have P2P script updated
join ref.panelist b on a.panelist_id = b.panelist_key
where ecosystem_cat is not null
group by panelist_id,panelist_key;


---If there are any other relevant data points to pull (ie values from other deliveries, other counts), do these pulls first and then also left join to these data points
----Left join to all relevant segmentations
-- Will need another method if a desired segmentation is not mutually exclusive and someone can be in multiple segments 
begin try drop table #candidate_info end try begin catch end catch;
select a.*, b.segment_name as 'Segmentation_Name_B', c.segment_name as 'Segmentation_Name_B', d.relevant_data_point
into candidate_info
from #panelist_ecosystem_activity a
left join ref.panelist_segmentation b on 
	a.panelist_key = b.panelist_key 
	and b.segmentation_id = ''
left join ref.panelist_segmentation c on 
	a.panelist_key = c.panelist_key 
	and c.segmentation_id = ''
left join #relevant_data_point d on 
	a.panelist_key = b.panelist_key 
;

/*Joins the final segmentation table to the ecosystem activity table. Put this output into an excel sheet and sort in various ways to see if
someone is a good candidate.*/
select a.*
from #candidate_info a  
order by eco_cat_count;

/*If in the above output it seems like someone has the characteristis of a good candidate, run this code to get the panelist's ecosystem data to see if good story*/ 
select
	record_id, 
	panelist_id, 
	digital_type, 
	platform,
	notes as domain_or_app, 
	value as page, 
	search_term, 
	content_category,
	category, 
	subcategory
	bdg_display as ecosystem_record, 
	date_id as date, 
	start_time, 
	end_time_local, 
	dur_seconds
from [PROJECTNAME].[run].[v_report_sequence_events_p2p]
	where panelist_id = ''--INSERT IN PANELIST_ID YOU'RE SELECTING
	and ecosystem_cat is not null 
	order by start_time;

/*If the above output shows a good storyline then put that person into 02 script pull */


-------------------------------------
--End of Script--
-------------------------------------


