---------------------------------------------------------------------------------------------------
-- INDIVIDUAL P2P STORIES
---------------------------------------------------------------------------------------------------
---- NAME OF OWNER
---- DATE
---------------------------------------------------------------------------------------------------
-- Path to purchase.
-- Goal. Design script for P2P Indiv Story output that is easier to review / QA.
-- A) All ecosystem records should remain expanded at timestamp event level.
-- B) Potential relevant properties (such as news/info/social) should be left expanded to help QA.
-- C) Potentail non-relevant properties (such as adult/pets) should be collapsed making easier to read.
-- D) Section of script should exist so that properties/records0 can easily toggle to expand or collapse.

use PROJECTNAME;

-- select top 100 * from run.v_Report_Sequence_Events_P2P;

-- select * from #candidates;
-- drop table #candidates;
select [Panelist_ID], count(distinct[Ecosystem_Cat]) as eco_cat_count,
	count(distinct[Ecosytem_Subcategory]) as eco_subcat_count,
	count(distinct date_id) as metered_days
into #candidates
FROM run.v_Report_Sequence_Events_P2P
group by panelist_id
order by eco_cat_count desc ;

-- TBD: ### panliest_id in v_Report_Sequence_Events_P2P should be panelist_key ###

select panelist_key
into #selections
from ref.Panelist
where panelist_key in
(###, -- vendor panelist id, description of panelist

 );


-- 1) SUBSET TO CANDIDATE FILES
---------------------------------------------------------------------------------------------------
-- select top 100 * from run.v_Report_Sequence_Events_P2P where panelist_id in (511, 383, 313);

-- select * from #indiv_story_expand_all;
-- drop table #indiv_story_expand_all;
select panelist_id, digital_type, session_id, record_id,
	cast(Panelist_ID as varchar(30))+'.'+cast(Session_ID+200000000 as varchar(30)) as storyline_id,	 
	notes as property, value as property_detail, isnull(Search_Term,' ') as search_term,
	Date_ID, cast(start_time as time) as start_time, cast(a.dur_Seconds as numeric(30,5)) as dur_seconds,
	std_Cat as sw_cat, std_Subcategory as sw_subcat, Ecosystem_Cat as eco_cat, Ecosytem_Subcategory as eco_subcat,	bdg_display,
	case when Ecosytem_Subcategory is null then 0 else 1 end as eco_flag,
	case when Ecosytem_Subcategory is null then 0 else 1 end as expand_toggle
into #indiv_story_expand_all
from rps.v_Report_Sequence_Events_P2P a
inner join #selections b
	on a.Panelist_ID=b.Panelist_Key;
-- (33081 rows affected)

-- select * from #indiv_story_property_qa;
-- drop table #indiv_story_property_qa;
begin try drop table #indiv_story_property_qa end try begin catch end catch;
select property, a.digital_type, sw_cat, sw_subcat, b.common_category, b.common_supercategory, b.rps_flag, b.rpt_flag, max(eco_flag) as has_eco_record_flag, max(expand_toggle) as expand_toggle,
	sum(dur_seconds) as storyline_dur_seconds, count(*) as ct_records, count(distinct panelist_id) as ct_panelists
into #indiv_story_property_qa
from #indiv_story_expand_all a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
group by property, a.digital_type, sw_cat, sw_subcat, b.common_category, b.common_supercategory, b.rps_flag, b.rpt_flag;

begin try drop table #full_eco_property_list end try begin catch end catch;
select a.notes as property, b.common_supercategory, b.common_subcategory
into #full_eco_property_list
from run.v_Report_Sequence_Events_P2P a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.notes=b.property_name
where a.Digital_Type='Web' and Ecosystem_Cat is not null
group by a.notes, b.common_supercategory, b.common_subcategory;


-- apps go not need to be expanded
update #indiv_story_expand_all set expand_toggle=0 where digital_type = 'App';

-- get all search term detail - regardless of ecosystem
update #indiv_story_expand_all set expand_toggle=1 where digital_type = 'Web' and property like '%google%';
update #indiv_story_expand_all set expand_toggle=1 where digital_type = 'Web' and property_detail like '%search.yahoo.%';
update #indiv_story_expand_all set expand_toggle=1 where digital_type = 'Web' and property like '%bing%';

-- expand common mass retailers
update #indiv_story_expand_all set expand_toggle=1 where digital_type = 'Web' and property like '%amazon%';
update #indiv_story_expand_all set expand_toggle=1 where digital_type = 'Web' and property like '%ebay%';
update #indiv_story_expand_all set expand_toggle=1 where digital_type = 'Web' and property like '%walmart%';
update #indiv_story_expand_all set expand_toggle=1 where digital_type = 'Web' and property like '%target%';
update #indiv_story_expand_all set expand_toggle=1 where digital_type = 'Web' and property like '%bestbuy%';

-- mask all adult sites before handing off to internal team
update #indiv_story_expand_all set property='', property_detail='' where digital_type = 'Web' and sw_cat like '%adult%';

-- expand media/news/info
-- anything that already has ecosystem match, show all other ecosystem match
update #indiv_story_expand_all set expand_toggle=1 
from #indiv_story_expand_all a
inner join #full_eco_property_list b
	on a.property=b.property
	and (a.sw_cat like '%news%' or b.common_supercategory like '%news%');
-- and property like 'wikipedia%';

-- ## TBD QA ###
-- ap.org --> New

-- anything that already has ecosystem match, show all other ecosystem match
update #indiv_story_expand_all set expand_toggle=1 
from #indiv_story_expand_all a
inner join #indiv_story_property_qa b
	on a.property=b.property
	and b.expand_toggle=1;

select count(*) from #indiv_story_expand_all;
-- 33081;

-- records retained for expansion
select count(*) from #indiv_story_expand_all where expand_toggle=1;
-- 5216


-- select top 100 * from #indiv_story_expand_all;

--3)If the storyline has an expand flag in it, adds an expand flag for all entries.
begin try drop table #max_expand end try begin catch end catch;
select storyline_id, max(expand_toggle) as max_toggle
into #max_expand
from #indiv_story_expand_all
group by storyline_id;
--(8743 rows affected)

update #indiv_story_expand_all set expand_toggle=1 
from #indiv_story_expand_all a
inner join #max_expand b
	on a.storyline_id = b.storyline_id
	and b.max_toggle=1;
--(10131 rows affected)
 
 
--4) Aggregates data by storyline when the expand toggle is not 1
begin try drop table #indiv_story_storyline_id_first_record end try begin catch end catch;
select storyline_id, digital_type, date_id,
	min(start_time) as storyline_start_time_first_record,
	max(start_time) as storyline_start_time_last_record,
	count(*) as ct_records,
	sum(dur_seconds) as storyline_dur_seconds
into #indiv_story_storyline_id_first_record
from #indiv_story_expand_all
where expand_toggle = 0
group by storyline_id, digital_type, date_id
;
-- (6071 rows affected)
begin try drop table #indiv_story_storyline_id_first_record_v2 end try begin catch end catch;
select b.storyline_id, b.digital_type, b.date_id, b.storyline_dur_seconds,
	b.storyline_start_time_first_record as storyline_start_time,
	min(record_id) as storyline_first_record_id
into #indiv_story_storyline_id_first_record_v2
from #indiv_story_expand_all a
inner join #indiv_story_storyline_id_first_record b
	on a.storyline_id=b.storyline_id
	and a.digital_type=b.digital_type
	and a.date_id=b.date_id
	and a.start_time = b.storyline_start_time_first_record
group by b.storyline_id, b.digital_type, b.date_id, b.storyline_start_time_first_record, b.storyline_dur_seconds;
-- (6071 rows affected)

-- drop table #indiv_story_agg_output;
begin try drop table #indiv_story_agg_output end try begin catch end catch;

select a.Panelist_ID, a.Digital_Type, a.storyline_id, 'aggregate' as story_type, property, property_detail, search_term, a.date_id,
	b.storyline_start_time as start_time, b.storyline_dur_seconds/60.0 as dur_minutes,
	a.sw_cat, a.sw_subcat,
	isnull(a.eco_cat,' ') as eco_cat,
	isnull(a.eco_subcat,' ') as eco_subcat,
	ISNULL(a.content_category_1, '') as eco_content_1,
	ISNULL(a.content_category_2, '') as eco_content_2,
	eco_flag as story_flag,
	'' as story_notes
into #indiv_story_agg_output
from #indiv_story_expand_all a
inner join #indiv_story_storyline_id_first_record_v2 b
	on a.storyline_id=b.storyline_id
	and a.digital_type=b.digital_type
	and a.date_id=b.date_id
	and a.Record_ID=b.storyline_first_record_id
	and a.start_time = b.storyline_start_time;
-- (6071 rows affected)

--5) Makes similar version with expanded entries
begin try drop table #indiv_story_expanded_output end try begin catch end catch;

select a.Panelist_ID, a.Digital_Type, a.storyline_id,'expanded' as story_type, property, property_detail, search_term, a.date_id,
	a.start_time, a.dur_seconds/60.0 as dur_minutes,
	a.sw_cat, a.sw_subcat,
	isnull(a.eco_cat,' ') as eco_cat,
	isnull(a.eco_subcat,' ') as eco_subcat,
	ISNULL(a.content_category_1, '') as eco_content_1,
	ISNULL(a.content_category_2, '') as eco_content_2,
	eco_flag as story_flag,
	'' as story_notes
into #indiv_story_expanded_output
from #indiv_story_expand_all a
where a.expand_toggle = 1;
--(10131 rows affected)

--6) Joins them together
begin try drop table #indiv_story_combined end try begin catch end catch;
select *
into #indiv_story_combined
from #indiv_story_agg_output;
--(6071 rows affected)

insert into #indiv_story_combined
select *
from #indiv_story_expanded_output;
--(10131 rows affected)

-- 7) Pulls in purchase date table to create relative to buy column
begin try drop table #date_buy end try begin catch end catch;
select b.Vendor_Panelist_ID, a.final_purchase_date_time, cast(a.final_purchase_date_time as date) as buy_date
into #date_buy
from ref.Purchase_Date a
join ref.Panelist b on a.panelist_key = b.Panelist_Key;

--8) Creates final output
begin try drop table #indiv_story_output end try begin catch end catch;
select a.Panelist_ID, a.Digital_Type, a.storyline_id, a.story_type, a.property, a.property_detail, a.search_term, a.Date_ID, a.start_time,
a.dur_minutes, a.sw_cat, a.sw_subcat, a.eco_cat, a.eco_subcat, a.eco_content_1, a.eco_content_2,
case when datediff(day,Date_ID,buy_date) = 0 then 'Day 0' when datediff(day,Date_ID,buy_date) = 1 then 'Day -1' when datediff(day,Date_ID,buy_date) = 2 then 'Day -2' when datediff(day,Date_ID,buy_date) = 3 then 'Day -3' else '' end as Days_to_buy,
a.story_flag, a.story_notes
into #indiv_story_output
from #indiv_story_combined a left join #date_buy b on a.Panelist_ID = b.Vendor_Panelist_ID;
--(16202 rows affected)

-- 7) SELECT A PANELIST TO OUTPUT
---------------------------------------------------------------------------------------------------
select * from #indiv_story_output where Panelist_ID='' order by Date_ID, start_time;


-- END OF FILE -- 
