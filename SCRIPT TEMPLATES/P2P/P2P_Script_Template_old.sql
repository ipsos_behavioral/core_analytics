---- NAME OF OWNER
---- DATE

--this portion of the script will count all ecosystem related data by category/subcategory and by panelist. Will help determine who could be have a "good" P2P.
select [panelist_id], count(distinct[Category]) as eco_cat_count, 
	count(distinct[subcategory]) as eco_subcat_count,
	count(distinct[content_category]) as eco_content_count
	into #TABLENAME
from [PROJECTNAME].[run].[v_report_sequence_events_p2p] --can also use v_report_sequence_event_ecosystem but stephen/sami should have P2P script updated
where panelist_id in
	( --LIST PANELIST_IDS
	)
group by panelist_id, 
order by eco_cat_count desc ;

select * from #TABLENAME
order by eco_cat_count desc;

--below portion of script will allow you to quickly grab all ecosystem related data for a given panelist to see if they have a good p2p
select
	record_id, 
	panelist_id, 
	digital_type, 
	platform,
	notes as domain_or_app, 
	value as page, 
	search_term, 
	content_category,
	category, 
	subcategory
	bdg_display as ecosystem_record, 
	date_id as date, 
	start_time, 
	end_time_local, 
	dur_seconds
from [PROJECTNAME].[run].[v_report_sequence_events_p2p]
	where panelist_id = --INSERT IN PANELIST_ID YOU'RE SELECTING
	and ecosystem_cat is not null 
	order by start_time;

