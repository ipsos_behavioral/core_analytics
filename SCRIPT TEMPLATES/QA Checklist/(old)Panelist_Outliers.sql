--App and Domain Outliers
-- PROJECTNAME
-- Project Owner
-- Date
----------------------------------

use PROJECTNAME;

-------------------------------App and Domain Portion

------------Starts by pulling data from run.userbyday_total at property_level per day
-- select top 100 * from #digital_platform_panelist_property_date order by panelist_key,date_id;
-- select top 100 * from #digital_platform_panelist_property_date order by property_key, panelist_key, platform_id;
-- select * from #digital_platform_panelist_property_date where panelist_key = 1 and property_key = 238;
-- select * from #digital_platform_panelist_property_date order by dur_minutes desc;
begin try drop table #digital_platform_panelist_property_date end try begin catch end catch;

select digital_type_id, Platform_ID, Panelist_Key, Property_Key, Date_ID, cast(Num_Uses as float) as Num_Uses, dur_minutes
into #digital_platform_panelist_property_date
from PROJECTNAME.run.UserByDay_Total
where Report_Level = 0 and dur_minutes <> 0.00;
--(291293 rows affected)

/*
select digital_type_id, panelist_key, property_key, date_id, count(*)
from #digital_platform_panelist_property_date
group by digital_type_id, panelist_key, property_key, date_id
order by count(*) desc;
*/

------------Collapses across platform
-- select * from #digital_panelist_property_date where digital_type_id = 100 and panelist_key = 834 and property_key = 5966 and date_id = '2019-08-31';
-- select * from #digital_platform_panelist_property_date where digital_type_id = 100 and panelist_key = 834 and property_key = 5966 and date_id = '2019-08-31';
-- select * from #digital_panelist_property_date where digital_type_id = 235 and panelist_key = 1068 and property_key = 37640 and date_id = '2019-09-20';
-- select * from #digital_platform_panelist_property_date where digital_type_id = 235 and panelist_key = 1068 and property_key = 37640 and date_id = '2019-09-20'
-- select * from #digital_panelist_property_date where daily_qualified <> 1;
begin try drop table #digital_panelist_property_date end try begin catch end catch;

select a.Digital_Type_ID, a.Panelist_Key, a.Property_Key, a.Date_ID, b.Daily_Qualified, sum(num_uses) as Num_Uses, sum(dur_minutes) as dur_minutes
into #digital_panelist_property_date
from #digital_platform_panelist_property_date a join run.User_Qualification b
on a.Panelist_Key = b.Panelist_Key and a.Date_ID = b.Date_ID and b.Daily_Qualified = 1
group by a.digital_type_id, a.Panelist_Key, a.Property_Key, a.Date_ID, b.Daily_Qualified;
--(147267 rows affected)

---------Collapses data across days since analysis is of a panelist's overall effect on the data.
-- select top 100 * from #digital_panelist_property order by panelist_key;
-- select * from #digital_platform_panelist_property where panelist_key = 1 and property_key = 238;
-- select * from #digital_panelist_property_date where property_key = 76431 and digital_type_id = 235 and panelist_key = 1;
begin try drop table #digital_panelist_property end try begin catch end catch;

select digital_type_id, Panelist_Key, Property_Key, sum(Num_Uses) as panelist_uses, sum(dur_minutes) as panelist_mins, STDEVP(dur_minutes) as prop_mins_stddev
into #digital_panelist_property
from #digital_panelist_property_date
group by digital_type_id, Panelist_Key, Property_Key;
--(45197 rows affected)

-------Gets the names of the properties since core taxonomy table doesnt have property key. From here, script is broken into digital type
--App first
-- select top 100 * from #app_with_name order by app_name;
begin try drop table #app_with_name end try begin catch end catch;

select a.*,b.App_Name
into #app_with_name
from #digital_panelist_property a join PROJECTNAME.ref.App_Name b on a.Property_Key = b.App_Name_Key
where Digital_Type_ID = 100;
--(12502 rows affected)

--Domain 
-- select top 100 * from #domain_with_name;
begin try drop table #domain_with_name end try begin catch end catch;

select a.*,b.Domain_Name
into #domain_with_name
from #digital_panelist_property a join PROJECTNAME.ref.Domain_Name b on a.Property_Key = b.Domain_Name_Key
where Digital_Type_ID = 235;
--(32695 rows affected)

------------Joins with core taxonomy table to get the supercategories
--App
-- select top 100 * from #app_with_supercat where app_name = '.Tuenti';
-- select top 100 * from #app_with_supercat order by app_name;
begin try drop table #app_with_supercat end try begin catch end catch;

select a.*, b.common_supercategory, b.rpt_flag
into #app_with_supercat
from #app_with_name a left join Core.ref.Taxonomy_Common_Properties b on a.App_Name = b.property_name
where a.digital_type_id = 100
and b.rps_flag = 1; --if adding this changes # rows from app_with_name table then userbyday hasn't applied rps flag yet
--(8981 rows affected)

--Domain
-- select top 100 * from #domain_with_supercat;
begin try drop table #domain_with_supercat end try begin catch end catch;

select a.*, b.common_supercategory, b.rpt_flag
into #domain_with_supercat
from #domain_with_name a left join Core.ref.Taxonomy_Common_Properties b on a.domain_name = b.property_name
where a.digital_type_id = 235
and b.rps_flag = 1;
--(26663 rows affected)

-------------------Collapse across supercat and panelist to get supercat avg stddev

-- select * from #app_supercat_avgstddev_temp1;
begin try drop table #app_supercat_avgstddev_temp1 end try begin catch end catch;

select common_supercategory, avg(prop_mins_stddev) as avg_supercat_mins_stddev
into #app_supercat_avgstddev_temp1
from #app_with_supercat
group by common_supercategory;
--(13 rows affected)

---- select top 100 * from #app_supercat_avgstddev;
begin try drop table #app_supercat_avgstddev end try begin catch end catch;

select a.*, a.prop_mins_stddev/b.avg_supercat_mins_stddev as stddev_supercat_comparison
into #app_supercat_avgstddev
from #app_with_supercat a left join #app_supercat_avgstddev_temp1 b
on a.common_supercategory = b.common_supercategory;
--(8981 rows affected)

-- select * from #domain_supercat_avgstddev_temp1;
begin try drop table #domain_supercat_avgstddev_temp1 end try begin catch end catch;

select common_supercategory, avg(prop_mins_stddev) as avg_supercat_mins_stddev
into #domain_supercat_avgstddev_temp1
from #domain_with_supercat
group by common_supercategory;
--(11 rows affected)

---- select top 100 * from #domain_supercat_avgstddev;
begin try drop table #domain_supercat_avgstddev end try begin catch end catch;

select a.*, a.prop_mins_stddev/b.avg_supercat_mins_stddev as stddev_supercat_comparison
into #domain_supercat_avgstddev
from #domain_with_supercat a left join #app_supercat_avgstddev_temp1 b
on a.common_supercategory = b.common_supercategory;
--(26663 rows affected)

---------Collapses across supercategories
--App
-- select * from #app_with_supercat where panelist_key = 169 and common_supercategory = 'Shopping';
-- select * from #app_no_property where panelist_key = 169 and common_supercategory = 'Shopping';
begin try drop table #app_no_property end try begin catch end catch;

select panelist_key,common_supercategory, count(*) as num_properties, sum(panelist_uses) as supercat_visits, sum(panelist_mins) as supercat_mins
into #app_no_property
from #app_with_supercat 
group by panelist_key,common_supercategory;
--(2825 rows affected)

--Domain
-- select top 100 * from #domain_no_property where common_supercategory = 'Shopping' order by panelist_key;
begin try drop table #domain_no_property end try begin catch end catch;

select panelist_key,common_supercategory, count(*) as num_properties, sum(panelist_uses) as supercat_visits, sum(panelist_mins) as supercat_mins
into #domain_no_property
from #domain_with_supercat 
group by panelist_key,common_supercategory;
--(2994 rows affected)

-------Gets supercategory averages, standard deviations, and totals
--App
-- select * from #app_supercat_averages;
-- select * from #app_no_property where common_supercategory = 'Shopping'
begin try drop table #app_supercat_averages end try begin catch end catch;

select common_supercategory, avg(supercat_visits) as supercat_avg_visits, STDEVP(supercat_visits) as supercat_stddev_visits, sum(supercat_visits) as supercat_total_visits,
 avg(supercat_mins) as supercat_avg_mins, STDEVP(supercat_mins) as supercat_stddev_mins, sum(supercat_mins) as supercat_total_mins
into #app_supercat_averages
from #app_no_property
group by common_supercategory;
--(11 rows affected)

--Domain
-- select * from #domain_supercat_averages;
begin try drop table #domain_supercat_averages end try begin catch end catch;

select common_supercategory, avg(supercat_visits) as supercat_avg_visits, STDEVP(supercat_visits) as supercat_stddev_visits,sum(supercat_visits) as supercat_total_visits,
 avg(supercat_mins) as supercat_avg_mins, STDEVP(supercat_mins) as supercat_stddev_mins, sum(supercat_mins) as supercat_total_mins
into #domain_supercat_averages
from #domain_no_property
group by common_supercategory;
--(11 rows affected)

----------------Pulls from run.user_qualification to get the number of qualified days
-- select top 100 * from #qualification_1 order by panelist_key;
begin try drop table #qualification_1 end try begin catch end catch;

select Panelist_Key, date_id,Daily_Qualified
into #qualification_1
from run.User_Qualification;
--(27010 rows affected)


---------------- Collapses to panelist level to get total number of qualified days
-- select top 100 * from #qualification_2 order by panelist_key;
begin try drop table #qualification_2 end try begin catch end catch;

select panelist_key, sum(daily_qualified) as qualified_days
into #qualification_2
from #qualification_1
group by Panelist_Key;
--(1423 rows affected)

--------Joins to get how many stand devs they are from avg and what percentage of total they make up
--App
-- select * from #app_comparisons order by mins_stddevs desc;
-- select * from #app_no_property where panelist_key = 462
begin try drop table #app_comparisons end try begin catch end catch;

select a.*, (a.supercat_visits-b.supercat_avg_visits)/b.supercat_stddev_visits as visit_stddevs, (a.supercat_visits/b.supercat_total_visits) as panelist_share_of_supercat_visits,
(a.supercat_mins-b.supercat_avg_mins)/b.supercat_stddev_mins as mins_stddevs,  a.supercat_mins/b.supercat_total_mins as panelist_share_of_supercat_mins, 
c.qualified_days, a.supercat_mins/c.qualified_days as supercat_mins_per_qualday
into #app_comparisons
from #app_no_property a join #app_supercat_averages b
on a.common_supercategory = b.common_supercategory
join #qualification_2 c on a.Panelist_Key = c.Panelist_Key;
--(2825 rows affected)

--Domain
-- select * from #domain_comparisons order by mins_stddevs desc;
-- select * from #domain_no_property where common_supercategory = 'Utility/Other';
begin try drop table #domain_comparisons end try begin catch end catch;

select a.*, (a.supercat_visits-b.supercat_avg_visits)/b.supercat_stddev_visits as visit_stddevs, a.supercat_visits/b.supercat_total_visits as panelist_share_of_supercat_visits,
(a.supercat_mins-b.supercat_avg_mins)/b.supercat_stddev_mins as mins_stddevs,  a.supercat_mins/b.supercat_total_mins as panelist_share_of_supercat_mins,
c.qualified_days, a.supercat_mins/c.qualified_days as supercat_mins_per_qualday
into #domain_comparisons
from #domain_no_property a join #domain_supercat_averages b
on a.common_supercategory = b.common_supercategory
join #qualification_2 c on a.Panelist_Key = c.Panelist_Key;
--(2994 rows affected)

-- select * from #problem_apps;
begin try drop table #problem_apps end try begin catch end catch;
select * 
into #problem_apps
from #app_comparisons 
where mins_stddevs >= 6 and panelist_share_of_supercat_mins >= 0.05;
--(13 rows affected)

begin try drop table #problem_domains end try begin catch end catch;
select * 
into #problem_domains
from #domain_comparisons 
where mins_stddevs >= 6 and panelist_share_of_supercat_mins >= 0.05;
-- (13 rows affected)

------------Add on individual properties. If it is a few properties causing issues, remove those issues. If many properties, potentially the person.
-- select top 100 * from #app_supercat_avgstddev;
-- select * from #problem_apps_with_detail;
begin try drop table #problem_apps_with_detail end try begin catch end catch;
select a.Panelist_Key, a.common_supercategory, b.Property_Key, b.panelist_uses as property_uses, b.panelist_mins as property_mins, b.App_Name, b.rpt_flag, b.panelist_mins/a.supercat_mins as prop_share_supercat_mins,
a.num_properties as num_supercat_properties, a.supercat_visits, a.supercat_mins, a.mins_stddevs, a.panelist_share_of_supercat_mins, a.qualified_days, a.supercat_mins_per_qualday
into #problem_apps_with_detail
from #problem_apps a join #app_supercat_avgstddev b
on a.Panelist_Key = b.Panelist_Key
and a.common_supercategory = b.common_supercategory;
--(52 rows affected)

-- select * from #problem_domains_with_detail order by prop_share_supercat_mins desc;
-- select distinct panelist_key from #problem_domains_with_detail;
begin try drop table #problem_domains_with_detail end try begin catch end catch;
select  a.Panelist_Key, a.common_supercategory, b.Property_Key, b.panelist_uses as property_uses, b.panelist_mins as property_mins, b.domain_Name, b.rpt_flag, b.panelist_mins/a.supercat_mins as prop_share_supercat_mins,
a.num_properties as num_supercat_properties, a.supercat_visits, a.supercat_mins, a.mins_stddevs, a.panelist_share_of_supercat_mins, a.qualified_days, a.supercat_mins_per_qualday
into #problem_domains_with_detail
from #problem_domains a join #domain_supercat_avgstddev b
on a.Panelist_Key = b.Panelist_Key
and a.common_supercategory = b.common_supercategory;
--(580 rows affected)


----------Pulls out individual property issues where that property alone is 30%+ of the time in the problematic supercat. Remove these individual combinations.
begin try drop table #individual_prop_app_issues end try begin catch end catch;
select *
into #individual_prop_app_issues
from #problem_apps_with_detail
where prop_share_supercat_mins > = 0.3;
--(13 rows affected)

begin try drop table #individual_prop_domain_issues end try begin catch end catch;
select *
into #individual_prop_domain_issues
from #problem_domains_with_detail
where prop_share_supercat_mins > = 0.3;
--(11 rows affected)

----------Gets general panelist details if they do not have a single prop with more than 30% reach
begin try drop table #overall_panelist_app_issues end try begin catch end catch;
select a.* 
into #overall_panelist_app_issues
from #problem_apps a join #individual_prop_app_issues b
on a.Panelist_Key = b.Panelist_Key
and a.common_supercategory = b.common_supercategory
where (b.Panelist_Key is null and b.common_supercategory is null);
--(0 rows affected)

begin try drop table #overall_panelist_domain_issues end try begin catch end catch;
select a.* 
into #overall_panelist_domain_issues
from #problem_domains a join #individual_prop_domain_issues b
on a.Panelist_Key = b.Panelist_Key
and a.common_supercategory = b.common_supercategory
where (b.Panelist_Key is null and b.common_supercategory is null);
--(0 rows affected)

-------------------------
--App and Domain Separate Output--
--Use this to determine generally which panelist/supercat combinations might be problematic and then follow-up
-------------------------
--select * from #app_comparisons order by mins_stddevs desc;
--select * from #domain_comparisons order by mins_stddevs desc;
-- select * from #domain_comparisons where panelist_key = 156 order by common_supercategory;
-- select * from #app_comparisons where panelist_key = 156 order by common_supercategory;

---Individual property/panelist removals
select * from #individual_prop_app_issues order by supercat_mins_per_qualday, panelist_key, prop_share_supercat_mins desc;
select * from #individual_prop_domain_issues order by supercat_mins_per_qualday, panelist_key, prop_share_supercat_mins desc;

--Consider panelist overall removal
select * from #overall_panelist_app_issues order by supercat_mins_per_qualday, Panelist_Key;
select * from #overall_panelist_domain_issues order by supercat_mins_per_qualday, Panelist_Key;



--------------------------------Analysis to get overall high or low usage
----------Collapses across supercategory and digital type since not about how they spend their time but about if they in general are not active participants
begin try drop table #panelist_usage end try begin catch end catch;
select *
into #panelist_usage
from #app_comparisons;
--(4183 rows affected)

insert into #panelist_usage
select *
from #domain_comparisons;
--(4598 rows affected)

begin try drop table #panelist_usage_collapsed end try begin catch end catch;
select Panelist_Key, qualified_days, sum(num_properties) as panelist_num_properties, sum(supercat_visits) as panelist_total_visits, sum(supercat_mins) as panelist_total_mins
into #panelist_usage_collapsed
from #panelist_usage
group by Panelist_Key, qualified_days;
--(550 rows affected)

--select top 100 * from #panelist_usage_standardized order by panelist_key;
begin try drop table #panelist_usage_standardized end try begin catch end catch;
select Panelist_Key, qualified_days, panelist_num_properties, panelist_total_visits/qualified_days as visits_per_qualday, panelist_total_mins/qualified_days as mins_per_qualday
into #panelist_usage_standardized
from #panelist_usage_collapsed;
--(550 rows affected)

-- select top 100 * from #panelist_comparison order by panelist_key;
-- select top 100 * from #panelist_comparison order by abs(mins_stddev) desc;
begin try drop table #panelist_comparison end try begin catch end catch;
select a.*, ((a.visits_per_qualday-(avg(visits_per_qualday) over ()))/(STDEVP(visits_per_qualday) over ())) as visits_stddev,
((a.mins_per_qualday-(avg(mins_per_qualday) over ()))/(STDEVP(mins_per_qualday) over ())) as mins_stddev
into #panelist_comparison
from #panelist_usage_standardized a;
--(550 rows affected)

begin try drop table #problem_panelists end try begin catch end catch;
select *
into #problem_panelists
from #panelist_comparison
where (visits_stddev >= abs(6) or mins_stddev >= abs(6));

--##############
--Overall Usage Outliers
--##############
select *
from #problem_panelists;


--------------------------------END OF SCRIPT--