-- GLOBAL VARIABLE CHECK
-- Want to make sure that the global min, max, and last day match in run.user_qualification and run.userbyday_total
-- SCRIPTS AFFECTED: panelist_platform_qual, common SM, eco SM, unified SM (common & ecosystem)

-- user qualification table
select min([date_id]) from [ProjectName].[run].[user_qualification] where daily_qualified=1; 
select max([date_id]) from [ProjectName].[run].[user_qualification] where daily_qualified=1; 

-- user by day table
select min([date_id]) from [ProjectName].[run].[userbyday_total]; 
select max([date_id]) from [ProjectName].[run].[userbyday_total]; 