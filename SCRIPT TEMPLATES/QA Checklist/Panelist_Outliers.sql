-- Outliers Template for Earlier Processing
-- OWNER
-- DATE
----------------------------------

use PROJECTNAME;


------------Starts by pulling data from run.processed_events tables
-- select top 100 * from #processed_app_events order by session_id, record_id;
begin try drop table #processed_app_events end try begin catch end catch;
select digital_type_id, Panelist_Key, App_Name_Key, Date_ID, dur_minutes, Session_ID, Record_ID
into #processed_app_events
from run.App_Events;
--(2969316 rows affected)

begin try drop table #processed_web_events end try begin catch end catch;
select digital_type_id, Panelist_Key, Domain_Name_Key, URL_Key, Date_ID, dur_minutes, Session_ID, Record_ID
into #processed_web_events
from run.Web_Events;
--(3825022 affected)

------------ Collapses across record ID and url key to get sessions
-- select top 100 * from #app_record_collapse order by session_id;
begin try drop table #app_record_collapse end try begin catch end catch;
select digital_type_id, Panelist_Key, App_Name_Key, Date_ID,sum(dur_minutes) as dur_minutes, Session_ID
into #app_record_collapse
from #processed_app_events
group by digital_type_id, Panelist_Key, App_Name_Key, Date_ID, Session_ID;
--(1072878 rows affected)

begin try drop table #web_record_collapse end try begin catch end catch;
select digital_type_id, Panelist_Key, domain_Name_Key, URL_Key, Date_ID,sum(dur_minutes) as dur_minutes, Session_ID
into #web_record_collapse
from #processed_web_events
group by digital_type_id, Panelist_Key, domain_Name_Key, URL_Key, Date_ID, Session_ID;
--(2558501 rows affected)

begin try drop table #web_url_collapse end try begin catch end catch;
select digital_type_id, Panelist_Key, domain_Name_Key, Date_ID, sum(dur_minutes) as dur_minutes, Session_ID
into #web_url_collapse
from #web_record_collapse
group by digital_type_id, Panelist_Key, domain_Name_Key, Date_ID, Session_ID;
--(720646 rows affected)

----------------Collapses across sessions
begin try drop table #app_session_collapse end try begin catch end catch;
select digital_type_id, Panelist_Key, App_Name_Key, Date_ID,sum(dur_minutes) as dur_minutes, count(*) as num_sessions
into #app_session_collapse
from #app_record_collapse
group by digital_type_id, Panelist_Key, App_Name_Key, Date_ID;
--(379656 rows affected)

-- select top 100 * from #web_record_collapse order by panelist_key, date_id, domain_name_key, session_id;
-- select top 100 * from #web_url_collapse order by panelist_key, date_id, domain_name_key;
-- select top 100 * from #web_session_collapse order by panelist_key, date_id, domain_name_key;
begin try drop table #web_session_collapse end try begin catch end catch;
select digital_type_id, Panelist_Key, domain_Name_Key, Date_ID, sum(dur_minutes) as dur_minutes, count(*) as num_sessions
into #web_session_collapse
from #web_url_collapse
group by digital_type_id, Panelist_Key, domain_Name_Key, Date_ID;
--(478639 rows affected)

-----------------Collapses across days
-- select top 100 * from #app_session_collapse order by panelist_key, app_name_key;
-- select top 100 * from #app_day_collapse order by panelist_key, app_name_key;
begin try drop table #app_day_collapse end try begin catch end catch;
select digital_type_id, Panelist_Key, App_Name_Key, sum(dur_minutes) as dur_minutes, sum (num_sessions) as num_sessions, count(*) as num_days
into #app_day_collapse
from #app_session_collapse
group by digital_type_id, Panelist_Key, App_Name_Key;
--(52528 rows affected)

-- select top 100 * from #web_session_collapse order by panelist_key, domain_name_key;
-- select top 100 * from #web_day_collapse order by panelist_key, domain_name_key;
begin try drop table #web_day_collapse end try begin catch end catch;
select digital_type_id, Panelist_Key, domain_Name_Key, sum(dur_minutes) as dur_minutes, sum (num_sessions) as num_sessions, count(*) as num_days
into #web_day_collapse
from #web_session_collapse
group by digital_type_id, Panelist_Key, Domain_Name_Key;
--(196698 rows affected)


-------Gets the names of the properties since core taxonomy table doesnt have property key. From here, script is broken into digital type
--App first
-- select top 100 * from #app_with_name order by app_name;
begin try drop table #app_with_name end try begin catch end catch;
select a.*,b.App_Name, App_Name_Hash
into #app_with_name
from #app_day_collapse a join ref.App_Name b on a.App_Name_Key = b.App_Name_Key
;
--(52528 rows affected) ---ROW COUNT SHOULD MATCH DAY COLLAPSE


--Domain 
-- select top 100 * from #domain_with_name;
-- select * from #domain_with_name where panelist_key = 523 and property_key = 9520;
begin try drop table #domain_with_name end try begin catch end catch;
select a.*,b.Domain_Name, Domain_Name_Hash
into #domain_with_name
from #web_day_collapse a join ref.Domain_Name b on a.Domain_Name_Key = b.Domain_Name_Key;
--(196698 rows affected)  ---ROW COUNT SHOULD MATCH DAY COLLAPSE

------------Joins with core taxonomy table to get the supercategories
--App
-- select top 100 * from #app_with_supercat;
-- select top 100 * from #app_with_supercat order by app_name;

begin try drop table #app_with_supercat end try begin catch end catch;

select a.*, b.common_supercategory, b.rpt_flag
into #app_with_supercat
from #app_with_name a left join Core.ref.Taxonomy_Common_2021 b on a.App_Name_Hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.rps_flag = 1 or b.rps_flag is null)
;
--(47923 rows affected)

--Domain
-- select top 100 * from #domain_with_supercat;
-- select * from #domain_with_supercat where panelist_key = 523 and property_key = 9520;
begin try drop table #domain_with_supercat end try begin catch end catch;

select a.*, b.common_supercategory, b.rpt_flag
into #domain_with_supercat
from #domain_with_name a left join Core.ref.Taxonomy_Common_2021 b on a.Domain_Name_Hash = b.property_hash and a.digital_type_id = b.digital_type_id
where (b.rps_flag = 1 or b.rps_flag is null)
;
--(183356 rows affected)


-------------Collapses across supercategory
-- select top 100 * from #app_with_supercat order by panelist_key, common_supercategory;
-- select top 100 * from #app_property_collapse order by panelist_key, common_supercategory;
begin try drop table #app_property_collapse end try begin catch end catch;
select digital_type_id, common_supercategory, Panelist_Key, sum(dur_minutes) as supercat_mins, cast(sum(num_sessions) as float) as supercat_visits, count(*) as num_properties
into #app_property_collapse
from #app_with_supercat
group by digital_type_id, common_supercategory, Panelist_Key;
--(10920 rows affected)

begin try drop table #domain_property_collapse end try begin catch end catch;
select digital_type_id, common_supercategory, Panelist_Key, sum(dur_minutes) as supercat_mins, cast(sum(num_sessions) as float) as supercat_visits, count(*) as num_properties
into #domain_property_collapse
from #domain_with_supercat
group by digital_type_id, common_supercategory, Panelist_Key;
--(12685 rows affected)

-- select top 100 * from #app_comparisons order by share_supercat_visits desc;
begin try drop table #app_comparisons end try begin catch end catch;
select a.*, ((supercat_visits-(avg(supercat_visits) over (partition by common_supercategory)))/(STDEVP(supercat_visits) over (partition by common_supercategory))) as visit_stddevs, 
(a.supercat_visits/ (sum(supercat_visits) over(partition by common_supercategory))) as share_supercat_visits,
((supercat_mins-(avg(supercat_mins) over (partition by common_supercategory)))/(STDEVP(supercat_mins) over (partition by common_supercategory))) as mins_stddevs, 
(a.supercat_mins/ (sum(supercat_mins) over(partition by common_supercategory))) as share_supercat_mins,
((num_properties-(avg(num_properties) over (partition by common_supercategory)))/(STDEVP(num_properties) over (partition by common_supercategory))) as properties_stddevs
into #app_comparisons
from #app_property_collapse a;
--(10920 rows affected)

-- select top 100 * from #domain_comparisons order by share_supercat_visits desc;
begin try drop table #domain_comparisons end try begin catch end catch;
select a.*, ((supercat_visits-(avg(supercat_visits) over (partition by common_supercategory)))/(STDEVP(supercat_visits) over (partition by common_supercategory))) as visit_stddevs, 
(a.supercat_visits/ (sum(supercat_visits) over(partition by common_supercategory))) as share_supercat_visits,
((supercat_mins-(avg(supercat_mins) over (partition by common_supercategory)))/(STDEVP(supercat_mins) over (partition by common_supercategory))) as mins_stddevs, 
(a.supercat_mins/ (sum(supercat_mins) over(partition by common_supercategory))) as share_supercat_mins,
((num_properties-(avg(num_properties) over (partition by common_supercategory)))/(STDEVP(num_properties) over (partition by common_supercategory))) as properties_stddevs
into #domain_comparisons
from #domain_property_collapse a;
--(12685 rows affected)

-- select * from #problem_apps;
begin try drop table #problem_apps end try begin catch end catch;
select * 
into #problem_apps
from #app_comparisons 
where (mins_stddevs >= 6 and share_supercat_mins >= 0.1) 
or (visit_stddevs >= 6 and share_supercat_visits >= 0.1);
--(1 rows affected)

-- select * from #problem_domains;
begin try drop table #problem_domains end try begin catch end catch;
select * 
into #problem_domains
from #domain_comparisons 
where (mins_stddevs >= 6 and share_supercat_mins >= 0.1)
or (visit_stddevs >= 6 and share_supercat_visits >= 0.1);
-- (4 rows affected)

------------Add on individual properties. If it is a few properties causing issues, remove those issues. If many properties, potentially the person.
-- select top 100 * from #app_supercat_avgstddev;
-- select * from #problem_apps_with_detail;
begin try drop table #problem_apps_with_detail end try begin catch end catch;
select a.Panelist_Key, a.common_supercategory, b.App_Name_Key, b.App_Name, b.rpt_flag, b.num_sessions as property_uses, b.num_days,
 b.dur_minutes as property_mins, b.dur_minutes/a.supercat_mins as prop_share_supercat_mins, b.num_sessions/a.supercat_visits as prop_share_supercat_visits,
a.num_properties as num_supercat_properties, a.supercat_mins, a.mins_stddevs, a.share_supercat_mins, a.supercat_visits, a.visit_stddevs, a.share_supercat_visits
into #problem_apps_with_detail
from #problem_apps a join #app_with_supercat b
on a.Panelist_Key = b.Panelist_Key
and a.common_supercategory = b.common_supercategory;
--(1 rows affected)

-- select * from #problem_domains_with_detail order by prop_share_supercat_mins desc;
-- select distinct panelist_key from #problem_domains_with_detail;
begin try drop table #problem_domains_with_detail end try begin catch end catch;
select a.Panelist_Key, a.common_supercategory, b.Domain_Name_Key, b.Domain_Name, b.rpt_flag, b.num_sessions as property_uses, b.num_days,
 b.dur_minutes as property_mins, b.dur_minutes/a.supercat_mins as prop_share_supercat_mins, b.num_sessions/a.supercat_visits as prop_share_supercat_visits,
a.num_properties as num_supercat_properties, a.supercat_mins, a.mins_stddevs, a.share_supercat_mins, a.supercat_visits, a.visit_stddevs, a.share_supercat_visits
into #problem_domains_with_detail
from #problem_domains a join #domain_with_supercat b
on a.Panelist_Key = b.Panelist_Key
and a.common_supercategory = b.common_supercategory;
--(74 rows affected)


----------Pulls out individual property issues where that property alone is 30%+ of the time in the problematic supercat. Remove these individual combinations.
begin try drop table #individual_prop_app_issues end try begin catch end catch;
select *
into #individual_prop_app_issues
from #problem_apps_with_detail
where prop_share_supercat_mins > = 0.3 or prop_share_supercat_visits > = 0.3;
--(1 rows affected)

begin try drop table #individual_prop_domain_issues end try begin catch end catch;
select *
into #individual_prop_domain_issues
from #problem_domains_with_detail
where prop_share_supercat_mins > = 0.3 or prop_share_supercat_visits > = 0.3;
--(6 rows affected)

----------Gets general panelist details if they do not have a single prop with more than 30% reach
begin try drop table #overall_panelist_app_issues end try begin catch end catch;
select a.* 
into #overall_panelist_app_issues
from #problem_apps a left join #individual_prop_app_issues b
on a.Panelist_Key = b.Panelist_Key
and a.common_supercategory = b.common_supercategory
where (b.Panelist_Key is null and b.common_supercategory is null);
--(0 rows affected)

begin try drop table #overall_panelist_domain_issues end try begin catch end catch;
select a.* 
into #overall_panelist_domain_issues
from #problem_domains a left join #individual_prop_domain_issues b
on a.Panelist_Key = b.Panelist_Key
and a.common_supercategory = b.common_supercategory
where (b.Panelist_Key is null and b.common_supercategory is null);
--(0 rows affected)

--####################
--App and Domain Export--
--Use this to determine generally which panelist/supercat combinations might be problematic and then follow-up
--#####################

---Individual property/panelist removals
select * from #individual_prop_app_issues order by share_supercat_mins, panelist_key, prop_share_supercat_mins desc;
select * from #individual_prop_domain_issues order by share_supercat_mins, panelist_key, prop_share_supercat_mins desc;

--Consider panelist overall removal
select * from #overall_panelist_app_issues order by share_supercat_mins, Panelist_Key;
select * from #overall_panelist_domain_issues order by share_supercat_mins, Panelist_Key;


--------------------------------Analysis to get overall high or low usage
----------Collapses across supercategory and digital type since not about how they spend their time but about if they in general are not active participants
begin try drop table #panelist_usage end try begin catch end catch;
select *
into #panelist_usage
from #app_comparisons;
--(10920 rows affected)

insert into #panelist_usage
select *
from #domain_comparisons;
--(12685 rows affected)

-- select top 100 * from #panelist_usage_collapsed order by panelist_total_mins desc;
begin try drop table #panelist_usage_collapsed end try begin catch end catch;
select Panelist_Key, sum(num_properties) as panelist_num_properties, sum(supercat_visits) as panelist_total_visits, sum(supercat_mins) as panelist_total_mins
into #panelist_usage_collapsed
from #panelist_usage
group by Panelist_Key;
--(1395 rows affected)


-- select top 100 * from #panelist_comparison order by panelist_key;
-- select top 100 * from #panelist_comparison order by abs(mins_stddev) desc;
begin try drop table #panelist_comparison end try begin catch end catch;
select a.*, ((a.panelist_num_properties-(avg(panelist_num_properties) over ()))/(STDEVP(panelist_num_properties) over ())) as properties_stddev, 
((a.panelist_total_visits-(avg(panelist_total_visits) over ()))/(STDEVP(panelist_total_visits) over ())) as visits_stddev,
((a.panelist_total_mins-(avg(panelist_total_mins) over ()))/(STDEVP(panelist_total_mins) over ())) as mins_stddev, 
(a.panelist_total_visits/(sum(panelist_total_visits) over ())) as panelist_share_total_visits,
(a.panelist_total_mins/(sum(panelist_total_mins) over ())) as panelist_share_total_mins
into #panelist_comparison
from #panelist_usage_collapsed a;
--(1395 rows affected)

begin try drop table #problem_panelists end try begin catch end catch;
select *
into #problem_panelists
from #panelist_comparison
where (visits_stddev >= abs(6) or mins_stddev >= abs(6) or panelist_share_total_visits >= 0.05 or panelist_share_total_mins >= 0.05);
--(2 row affected)

--##############
--Overall Usage Outliers
--##############
select *
from #problem_panelists
order by panelist_share_total_mins;

----------------------End of Script------------------