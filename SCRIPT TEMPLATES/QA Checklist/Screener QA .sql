-- Screener/Pulse/Passive Panelist QA
-- Shirley 05/30/2019

USE MayoClinic;
--QA on Following Tables:
--Screener
--	Raw.dp_screener
--Passive:
--	Run.processed_App_Events
--	Run.processed_Web_Events
--	Run.UserByDay_Total
--	Run.v_Report_Sequence_Event
--	Ref.panelist
--	wrk.panelist_platform_qual_day 
--Pulse Survey
--  Raw.dp_pulse_Wave(X)_data

-- Section I:
-- Duplicate Check (will be on project basis, change/add column names for dup check based on project needs): 
/**
-- Screener
-- select top 100 * from raw.dp_screener;
select numericalId, CodPanelista, count(*) as dup_check 
from raw.dp_screener
group by numericalId, CodPanelista
having count(*) > 1;

select numericalId, count(distinct CodPanelista) as dup_check   
from raw.dp_screener
group by numericalId
having count(distinct CodPanelista) > 1;

--check all columns that will be included in segmentation creation or future reporting.   
select numericalId, count(distinct SERIOUS_AND_COMPLEX) as dup_check   
from raw.dp_screener
group by numericalId
having count(distinct SERIOUS_AND_COMPLEX) > 1;

-- ref.panelist:
-- select top 100 * from ref.panelist;
select *, count(*) as dup_check  
from ref.panelist
group by Panelist_Key,Vendor_Panelist_ID,Panelist_Hash,Exclusion_Flag
having count(*) > 1; 

select Panelist_Key, count(distinct Vendor_Panelist_ID) as dup_check  
from ref.panelist
group by Panelist_Key
having count(distinct Vendor_Panelist_ID) > 1; 

select Panelist_Key, count(distinct Panelist_Hash) as dup_check  
from ref.panelist
group by Panelist_Key
having count(distinct Panelist_Hash) > 1; 

select Panelist_Key, count(distinct Exclusion_Flag) as dup_check  
from ref.panelist
group by Panelist_Key
having count(distinct Exclusion_Flag) > 1; 

-- Pulse
-- select top 100 * from raw.dp_pulse_Wave1_data;
select numericalId, CodPanelista, count(*) as dup_check   
from raw.dp_pulse_Wave1_data
group by numericalId, CodPanelista
having count(*) > 1;

select CodPanelista, count(distinct numericalId) as dup_check   
from raw.dp_pulse_Wave1_data
group by CodPanelista
having count(distinct numericalId) > 1;

-- select top 100 * from raw.dp_pulse_Wave2_data;
select numericalId, CodPanelista, sex,ageRecode, count(*) as dup_check   
from raw.dp_pulse_Wave2_data
group by numericalId, CodPanelista, sex,ageRecode
having count(*) > 1;
-- select top 100 * from raw.dp_pulse_Wave3_data;
select numericalId, CodPanelista, sex,ageRecode, count(*) as dup_check   
from raw.dp_pulse_Wave3_data
group by numericalId, CodPanelista, sex,ageRecode
having count(*) > 1;
-- select top 100 * from raw.dp_pulse_Wave4_data;
select numericalId, CodPanelista, sex,ageRecode, count(*) as dup_check   
from raw.dp_pulse_Wave4_data
group by numericalId, CodPanelista, sex,ageRecode
having count(*) > 1;

--check all columns that will be included in segmentation creation or future reporting.
-- select * from #all_pulse_data;
-- drop table #all_pulse_data;
select *, 'pulse 1' as survey into #all_pulse_data 
from raw.dp_pulse_Wave1_data
union
select *,'pulse 2' as survey from raw.dp_pulse_Wave2_data
union
select *,'pulse 3' as survey from raw.dp_pulse_Wave3_data
union
select *,'pulse 4' as survey from raw.dp_pulse_Wave4_data;

select CodPanelista, count(distinct sex) as dup_check   
from #all_pulse_data
group by CodPanelista
having count(distinct sex) > 1;

select CodPanelista, count(distinct ageRecode) as dup_check   
from #all_pulse_data
group by CodPanelista
having count(distinct ageRecode) > 1;
**/

-- Section II: Check if panelist records match throughout screener > passive data > pulse process
-- For example, panelists can't have passive data without having taken the screener 

-- Initial Check:
-- is everyone in pulse also in screener? -- yes
/**
select count (distinct a.CodPanelista) from raw.dp_pulse_Wave1_data a 
where not exists (select b.CodPanelista from raw.dp_screener b where a.CodPanelista = b.CodPanelista) 
**/

-- Table 1: Counts of distinct panelists for each table:
-- select * from #Panelist_Counts;
-- drop table #Panelist_Counts;
Create table #Panelist_Counts(
Table_Name varchar(255),
Table_Panelist_Count float,
);
insert into #Panelist_Counts(Table_Name) select 'raw.dp_screener';
insert into #Panelist_Counts(Table_Name) select 'ref.panelist';
insert into #Panelist_Counts(Table_Name) select 'run.v_Report_Sequence_Event';
insert into #Panelist_Counts(Table_Name) select 'run.Processed_App_Events';
insert into #Panelist_Counts(Table_Name) select 'run.Processed_Web_Events';
insert into #Panelist_Counts(Table_Name) select 'run.UserByDay_Total';
insert into #Panelist_Counts(Table_Name) select 'wrk.panelist_platform_qual_day';
insert into #Panelist_Counts(Table_Name) select 'raw.dp_pulse_Wave1_data';
insert into #Panelist_Counts(Table_Name) select 'raw.dp_pulse_Wave2_data';
insert into #Panelist_Counts(Table_Name) select 'raw.dp_pulse_Wave3_data';
insert into #Panelist_Counts(Table_Name) select 'raw.dp_pulse_Wave4_data';

update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct CodPanelista) from raw.dp_screener)
where Table_Name = 'raw.dp_screener';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct Panelist_Key) from ref.panelist)
where Table_Name = 'ref.panelist';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct Panelist_ID) from run.v_Report_Sequence_Event)
where Table_Name = 'run.v_Report_Sequence_Event';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct Panelist_Key) from run.Processed_App_Events)
where Table_Name = 'run.Processed_App_Events';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct Panelist_Key) from run.Processed_Web_Events)
where Table_Name = 'run.Processed_Web_Events';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct Panelist_Key) from run.UserByDay_Total)
where Table_Name = 'run.UserByDay_Total';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct Panelist_Key) from wrk.panelist_platform_qual_day)
where Table_Name = 'wrk.panelist_platform_qual_day';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct CodPanelista) from raw.dp_pulse_Wave1_data)
where Table_Name = 'raw.dp_pulse_Wave1_data';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct CodPanelista) from raw.dp_pulse_Wave2_data)
where Table_Name = 'raw.dp_pulse_Wave2_data';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct CodPanelista) from raw.dp_pulse_Wave3_data)
where Table_Name = 'raw.dp_pulse_Wave3_data';
update #Panelist_Counts set
Table_Panelist_Count = (select count(distinct CodPanelista) from raw.dp_pulse_Wave4_data)
where Table_Name = 'raw.dp_pulse_Wave4_data';


-- Table 2: QA flags
-- select * from #Screener_QA;
-- drop table #Screener_QA;
Create table #Screener_QA(
PassiveTable_Name varchar(255),
Passive_Panelist_NotIn_Screener_Flag float,
Passive_Panelist_NotIn_Ref_Panelist_Flag float,
Pulse1_Panelist_NotIn_Passive_Flag float,
Pulse2_Panelist_NotIn_Passive_Flag float,
Pulse3_Panelist_NotIn_Passive_Flag float,
Pulse4_Panelist_NotIn_Passive_Flag float
);


-- Table 3: Counts of # differences if flag = 1
-- select * from #QA_Counts;
-- drop table #QA_Counts;
Create table #QA_Counts(
PassiveTable_Name varchar(255),
Passive_Panelist_NotIn_Screener_Panelist_Count float,
Passive_Panelist_NotIn_Ref_Panelist_Count float,
Pulse1_NotIn_Passive_Count float,
Pulse2_NotIn_Passive_Count float,
Pulse3_NotIn_Passive_Count float,
Pulse4_NotIn_Passive_Count float
);


-- 1. Screener vs ref.panelist
insert into #Screener_QA(PassiveTable_Name) select 'ref.panelist';

-- select * from #Screener_QA;
update #Screener_QA set
Passive_Panelist_NotIn_Screener_Flag = 
case when 
(select count(distinct Vendor_Panelist_ID) from ref.panelist b left join raw.dp_screener a
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null) > 0 
then '1' else '0' end,
Passive_Panelist_NotIn_Ref_Panelist_Flag = 0,
Pulse1_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.CodPanelista) from raw.dp_pulse_Wave1_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null) > 0 
then '1' else '0' end,
Pulse2_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.CodPanelista) from raw.dp_pulse_Wave2_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null) > 0 
then '1' else '0' end,
Pulse3_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.CodPanelista) from raw.dp_pulse_Wave3_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null)> 0 
then '1' else '0' end,
Pulse4_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.CodPanelista) from raw.dp_pulse_Wave4_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null)> 0 
then '1' else '0' end
where PassiveTable_Name = 'ref.panelist';

-- select * from #Screener_QA;
-- select * from #QA_Counts;
insert into #QA_Counts(PassiveTable_Name) select 'ref.panelist';
update #QA_Counts set
Passive_Panelist_NotIn_Screener_Panelist_Count = 
case when 
(select Passive_Panelist_NotIn_Screener_Flag from #Screener_QA where PassiveTable_Name = 'ref.panelist') = 1
then
(select count(distinct Vendor_Panelist_ID) from ref.panelist b left join raw.dp_screener a
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null)
else '0' end,
Passive_Panelist_NotIn_Ref_Panelist_Count  = '0',
Pulse1_NotIn_Passive_Count = 
case when 
(select Pulse1_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'ref.panelist') = 1
then
(select count(distinct CodPanelista) from raw.dp_pulse_Wave1_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null)
else '0' end,
Pulse2_NotIn_Passive_Count = 
case when 
(select Pulse2_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'ref.panelist') = 1
then
(select count(distinct CodPanelista) from raw.dp_pulse_Wave2_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null)
else '0' end,
Pulse3_NotIn_Passive_Count = 
case when 
(select Pulse3_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'ref.panelist') = 1
then
(select count(distinct CodPanelista) from raw.dp_pulse_Wave3_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null)
else '0' end,
Pulse4_NotIn_Passive_Count = 
case when 
(select Pulse4_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'ref.panelist') = 1
then
(select count(distinct CodPanelista) from raw.dp_pulse_Wave4_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null)
else '0' end
where PassiveTable_Name = 'ref.panelist';


-- find panelists that are in pulse but not in ref.panelist:
/**
select * from raw.dp_pulse_Wave1_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null order by CodPanelista;

select * from raw.dp_pulse_Wave2_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null order by CodPanelista;

select * from raw.dp_pulse_Wave3_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null order by CodPanelista;

select * from raw.dp_pulse_Wave4_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null order by CodPanelista;
**/
/**
select distinct a.CodPanelista from raw.dp_pulse_Wave1_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null order by CodPanelista;

select distinct a.CodPanelista from raw.dp_pulse_Wave2_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null order by CodPanelista;

select distinct a.CodPanelista from raw.dp_pulse_Wave3_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null order by CodPanelista;

select distinct a.CodPanelista from raw.dp_pulse_Wave4_data a left join ref.Panelist b 
on a.CodPanelista = b.Vendor_Panelist_ID where b.Vendor_Panelist_ID is null order by CodPanelista;
**/



-- 2. ref.panelist vs. run.v_Report_Sequence_Event
insert into #Screener_QA(PassiveTable_Name) select 'run.v_Report_Sequence_Event';
-- select top 100 * from run.v_Report_Sequence_Event;
-- select top 100 * from ref.panelist;
-- select top 100 * from raw.dp_screener;
-- select top 100 * from raw.dp_pulse_Wave1_data;
-- create panelist_key for screener table
-- select * from #screener_panelist;
-- drop table #screener_panelist;
select b.Panelist_Key, a.*
into #screener_panelist
from raw.dp_screener a
inner join
ref.panelist b
on a.CodPanelista = b.Vendor_Panelist_ID;
-- 985 rows

-- select * from #pulse_panelist1;
-- drop table #pulse_panelist1;
select b.Panelist_Key, a.*
into #pulse_panelist1
from raw.dp_pulse_Wave1_data a
inner join
ref.panelist b
on a.CodPanelista = b.Vendor_Panelist_ID;
-- 387

-- select * from #pulse_panelist2;
-- drop table #pulse_panelist2;
select b.Panelist_Key, a.*
into #pulse_panelist2
from raw.dp_pulse_Wave2_data a
inner join
ref.panelist b
on a.CodPanelista = b.Vendor_Panelist_ID;
--(783 row(s) affected)

-- select * from #pulse_panelist3;
-- drop table #pulse_panelist3;
select b.Panelist_Key, a.*
into #pulse_panelist3
from raw.dp_pulse_Wave3_data a
inner join
ref.panelist b
on a.CodPanelista = b.Vendor_Panelist_ID;
--(1175 row(s) affected)

-- select * from #pulse_panelist4;
-- drop table #pulse_panelist4;
select b.Panelist_Key, a.*
into #pulse_panelist4
from raw.dp_pulse_Wave4_data a
inner join
ref.panelist b
on a.CodPanelista = b.Vendor_Panelist_ID;
--(1568 row(s) affected)

-- select * from #Screener_QA;
update #Screener_QA set
Passive_Panelist_NotIn_Screener_Flag  = NULL,
Passive_Panelist_NotIn_Ref_Panelist_Flag = 
case when 
(select count(distinct Panelist_ID) from run.v_Report_Sequence_Event b left join ref.panelist a
on a.Panelist_Key = b.Panelist_ID where b.Panelist_ID is null) > 0 
then '1' else '0' end,
Pulse1_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct Panelist_Key) from #pulse_panelist1 b left join run.v_Report_Sequence_Event a
on a.Panelist_ID = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse2_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct Panelist_Key) from #pulse_panelist2 b left join run.v_Report_Sequence_Event a
on a.Panelist_ID = b.Panelist_Key where b.Panelist_Key is null)> 0 
then '1' else '0' end,
Pulse3_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct Panelist_Key) from #pulse_panelist3 b left join run.v_Report_Sequence_Event a
on a.Panelist_ID = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse4_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct Panelist_Key) from #pulse_panelist4 b left join run.v_Report_Sequence_Event a
on a.Panelist_ID = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end
where PassiveTable_Name = 'run.v_Report_Sequence_Event';

-- select * from #Screener_QA;
-- select * from #QA_Counts;
insert into #QA_Counts(PassiveTable_Name) select 'run.v_Report_Sequence_Event';
update #QA_Counts set
Passive_Panelist_NotIn_Screener_Panelist_Count = 
'0',
Passive_Panelist_NotIn_Ref_Panelist_Count  = 
case when 
(select Passive_Panelist_NotIn_Ref_Panelist_Flag from #Screener_QA where PassiveTable_Name = 'run.v_Report_Sequence_Event') = 1
then (select count(distinct Panelist_ID) from run.v_Report_Sequence_Event b left join ref.panelist a
on a.Panelist_Key = b.Panelist_ID where b.Panelist_ID is null)
else '0' end,
Pulse1_NotIn_Passive_Count = 
case when 
(select Pulse1_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.v_Report_Sequence_Event') = 1
then
(select count(distinct Panelist_Key) from #pulse_panelist1 b left join run.v_Report_Sequence_Event a
on a.Panelist_ID = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse2_NotIn_Passive_Count = 
case when 
(select Pulse2_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.v_Report_Sequence_Event') = 1
then
(select count(distinct Panelist_Key) from #pulse_panelist2 b left join run.v_Report_Sequence_Event a
on a.Panelist_ID = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse3_NotIn_Passive_Count = 
case when 
(select Pulse3_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.v_Report_Sequence_Event') = 1
then
(select count(distinct Panelist_Key) from #pulse_panelist3 b left join run.v_Report_Sequence_Event a
on a.Panelist_ID = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse4_NotIn_Passive_Count = 
case when 
(select Pulse4_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.v_Report_Sequence_Event') = 1
then
(select count(distinct Panelist_Key) from #pulse_panelist4 b left join run.v_Report_Sequence_Event a
on a.Panelist_ID = b.Panelist_Key where b.Panelist_Key is null)
else '0' end
where PassiveTable_Name = 'run.v_Report_Sequence_Event';


-- 3. ref.panelist vs. run.Processed_App_Events
-- select top 100 * from run.Processed_App_Events;
insert into #Screener_QA(PassiveTable_Name) select 'run.Processed_App_Events';
-- select * from #Screener_QA;
update #Screener_QA set
Passive_Panelist_NotIn_Screener_Flag = NULL,
Passive_Panelist_NotIn_Ref_Panelist_Flag  = 
case when 
(select count(distinct b.Panelist_Key) from run.Processed_App_Events b left join ref.panelist a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse1_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist1 b left join run.Processed_App_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse2_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist2 b left join run.Processed_App_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse3_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join run.Processed_App_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)  > 0 
then '1' else '0' end,
Pulse4_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join run.Processed_App_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)  > 0 
then '1' else '0' end
where PassiveTable_Name = 'run.Processed_App_Events';


-- select * from #Screener_QA;
-- select * from #QA_Counts;
insert into #QA_Counts(PassiveTable_Name) select 'run.Processed_App_Events';
update #QA_Counts set
Passive_Panelist_NotIn_Screener_Panelist_Count = 
'0',
Passive_Panelist_NotIn_Ref_Panelist_Count  = 
case when 
(select Passive_Panelist_NotIn_Ref_Panelist_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_App_Events') = 1
then (select count(distinct Panelist_ID) from run.v_Report_Sequence_Event b left join ref.panelist a
on a.Panelist_Key = b.Panelist_ID where b.Panelist_ID is null)
else '0' end,
Pulse1_NotIn_Passive_Count = 
case when 
(select Pulse1_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_App_Events') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist1 b left join run.Processed_App_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse2_NotIn_Passive_Count = 
case when 
(select Pulse2_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_App_Events') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist2 b left join run.Processed_App_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse3_NotIn_Passive_Count = 
case when 
(select Pulse3_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_App_Events') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join run.Processed_App_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse4_NotIn_Passive_Count = 
case when 
(select Pulse4_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_App_Events') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist4 b left join run.Processed_App_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end
where PassiveTable_Name = 'run.Processed_App_Events';

--4. ref.panelist vs. run.Processed_Web_Events
-- select top 100 * from run.Processed_Web_Events;
insert into #Screener_QA(PassiveTable_Name) select 'run.Processed_Web_Events';
-- select * from #Screener_QA;
update #Screener_QA set
Passive_Panelist_NotIn_Screener_Flag = NULL,
Passive_Panelist_NotIn_Ref_Panelist_Flag = 
case when 
(select count(distinct b.Panelist_Key) from run.Processed_Web_Events b left join ref.panelist a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse1_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist1 b left join run.Processed_Web_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse2_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist2 b left join run.Processed_Web_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse3_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join run.Processed_Web_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)  > 0 
then '1' else '0' end,
Pulse4_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join run.Processed_Web_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)  > 0 
then '1' else '0' end
where PassiveTable_Name = 'run.Processed_Web_Events';


-- select * from #Screener_QA;
-- select * from #QA_Counts;
insert into #QA_Counts(PassiveTable_Name) select 'run.Processed_Web_Events';
update #QA_Counts set
Passive_Panelist_NotIn_Screener_Panelist_Count = 
'0',
Passive_Panelist_NotIn_Ref_Panelist_Count  = 
case when 
(select Passive_Panelist_NotIn_Ref_Panelist_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_Web_Events') = 1
then (select count(distinct b.Panelist_Key) from run.Processed_Web_Events b left join ref.panelist a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse1_NotIn_Passive_Count = 
case when 
(select Pulse1_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_Web_Events') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist1 b left join run.Processed_Web_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse2_NotIn_Passive_Count = 
case when 
(select Pulse2_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_Web_Events') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist2 b left join run.Processed_Web_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse3_NotIn_Passive_Count = 
case when 
(select Pulse3_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_Web_Events') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join run.Processed_Web_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse4_NotIn_Passive_Count = 
case when 
(select Pulse4_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.Processed_Web_Events') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist4 b left join run.Processed_Web_Events a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end
where PassiveTable_Name = 'run.Processed_Web_Events';

-- 5. ref.panelist vs. run.UserByDay_Total
-- select top 100 * from run.UserByDay_Total;
insert into #Screener_QA(PassiveTable_Name) select 'run.UserByDay_Total';
-- select * from #Screener_QA;
update #Screener_QA set
Passive_Panelist_NotIn_Screener_Flag = NULL,
Passive_Panelist_NotIn_Ref_Panelist_Flag = 
case when 
(select count(distinct b.Panelist_Key) from run.UserByDay_Total b left join ref.panelist a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse1_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist1 b left join run.UserByDay_Total a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse2_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist2 b left join run.UserByDay_Total a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse3_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join run.UserByDay_Total a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)  > 0 
then '1' else '0' end,
Pulse4_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join run.UserByDay_Total a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)  > 0 
then '1' else '0' end
where PassiveTable_Name = 'run.UserByDay_Total';


-- select * from #Screener_QA;
-- select * from #QA_Counts;
insert into #QA_Counts(PassiveTable_Name) select 'run.UserByDay_Total';
update #QA_Counts set
Passive_Panelist_NotIn_Screener_Panelist_Count = 
'0',
Passive_Panelist_NotIn_Ref_Panelist_Count  = 
case when 
(select Passive_Panelist_NotIn_Ref_Panelist_Flag from #Screener_QA where PassiveTable_Name = 'run.UserByDay_Total') = 1
then (select count(distinct b.Panelist_Key) from run.UserByDay_Total b left join ref.panelist a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse1_NotIn_Passive_Count = 
case when 
(select Pulse1_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.UserByDay_Total') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist1 b left join run.UserByDay_Total a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse2_NotIn_Passive_Count = 
case when 
(select Pulse2_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.UserByDay_Total') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist2 b left join run.UserByDay_Total a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse3_NotIn_Passive_Count = 
case when 
(select Pulse3_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.UserByDay_Total') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join run.UserByDay_Total a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse4_NotIn_Passive_Count = 
case when 
(select Pulse4_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'run.UserByDay_Total') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist4 b left join run.UserByDay_Total a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end
where PassiveTable_Name = 'run.UserByDay_Total';


-- 6. ref.panelist vs. wrk.panelist_platform_qual_day
-- select top 100 * from wrk.panelist_platform_qual_day;
insert into #Screener_QA(PassiveTable_Name) select 'wrk.panelist_platform_qual_day';
-- select * from #Screener_QA;
update #Screener_QA set
Passive_Panelist_NotIn_Screener_Flag = NULL,
Passive_Panelist_NotIn_Ref_Panelist_Flag = 
case when 
(select count(distinct b.Panelist_Key) from wrk.panelist_platform_qual_day b left join ref.panelist a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse1_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist1 b left join wrk.panelist_platform_qual_day a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse2_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist2 b left join wrk.panelist_platform_qual_day a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null) > 0 
then '1' else '0' end,
Pulse3_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join wrk.panelist_platform_qual_day a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)  > 0 
then '1' else '0' end,
Pulse4_Panelist_NotIn_Passive_Flag = 
case when
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join wrk.panelist_platform_qual_day a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)  > 0 
then '1' else '0' end
where PassiveTable_Name = 'wrk.panelist_platform_qual_day';

-- select * from #Screener_QA;
-- select * from #QA_Counts;
insert into #QA_Counts(PassiveTable_Name) select 'wrk.panelist_platform_qual_day';
update #QA_Counts set
Passive_Panelist_NotIn_Screener_Panelist_Count = 
'0',
Passive_Panelist_NotIn_Ref_Panelist_Count  = 
case when 
(select Passive_Panelist_NotIn_Ref_Panelist_Flag from #Screener_QA where PassiveTable_Name = 'wrk.panelist_platform_qual_day') = 1
then (select count(distinct b.Panelist_Key) from wrk.panelist_platform_qual_day b left join ref.panelist a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse1_NotIn_Passive_Count = 
case when 
(select Pulse1_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'wrk.panelist_platform_qual_day') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist1 b left join wrk.panelist_platform_qual_day a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse2_NotIn_Passive_Count = 
case when 
(select Pulse2_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'wrk.panelist_platform_qual_day') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist2 b left join wrk.panelist_platform_qual_day a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse3_NotIn_Passive_Count = 
case when 
(select Pulse3_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'wrk.panelist_platform_qual_day') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist3 b left join wrk.panelist_platform_qual_day a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end,
Pulse4_NotIn_Passive_Count = 
case when 
(select Pulse4_Panelist_NotIn_Passive_Flag from #Screener_QA where PassiveTable_Name = 'wrk.panelist_platform_qual_day') = 1
then
(select count(distinct a.Panelist_Key) from #pulse_panelist4 b left join wrk.panelist_platform_qual_day a
on a.Panelist_Key = b.Panelist_Key where b.Panelist_Key is null)
else '0' end
where PassiveTable_Name = 'wrk.panelist_platform_qual_day';


-- qualification for pulse survey
-- select * from run.User_Qualification where panelist_key = 227;
-- select * from #pulse_panelist1 where panelist_key = 227;
-- select * from run.User_Qualification order by panelist_key;
select a.Panelist_Key, max(b.Daily_Qualified) as Daily_QA from #pulse_panelist1 a
inner join run.User_Qualification b
on a.Panelist_Key = b.Panelist_Key
group by a.Panelist_Key
order by Daily_QA;

-- select * from #pulse_panelist2;
-- select * from run.User_Qualification order by panelist_key;
select a.Panelist_Key, max(b.Daily_Qualified) as Daily_QA from #pulse_panelist2 a
inner join run.User_Qualification b
on a.Panelist_Key = b.Panelist_Key
group by a.Panelist_Key
order by Daily_QA;

-- select * from #pulse_panelist3;
-- select * from run.User_Qualification order by panelist_key;
select a.Panelist_Key, max(b.Daily_Qualified) as Daily_QA from #pulse_panelist3 a
inner join run.User_Qualification b
on a.Panelist_Key = b.Panelist_Key
group by a.Panelist_Key
order by Daily_QA;

-- select * from #pulse_panelist4;
-- select * from run.User_Qualification order by panelist_key;
select a.Panelist_Key, max(b.Daily_Qualified) as Daily_QA from #pulse_panelist4 a
inner join run.User_Qualification b
on a.Panelist_Key = b.Panelist_Key
group by a.Panelist_Key
order by Daily_QA;


-- select * from run.v_Report_Sequence_Event;
-- select * from run.User_Qualification order by panelist_key;
select a.Panelist_ID, max(b.Daily_Qualified) as Daily_QA from run.v_Report_Sequence_Event a
inner join run.User_Qualification b
on a.Panelist_ID = b.Panelist_Key
group by a.Panelist_ID
order by Daily_QA;

---------------------------
--Output
---------------------------
-- Table 1: Counts of distinct panelists for each table:
select * from #Panelist_Counts;

-- Table 2: QA flags
select * from #Screener_QA;

-- Table 3: Counts of # differences if flag = 1
select * from #QA_Counts;
---------------------------------------------------------------------------------------------------------
-- Detail tables for further investigation:
-- find panelists that are in screener but not in pulse:
/**
select * from raw.dp_screener a left join raw.dp_pulse_Wave1_data b on a.CodPanelista = b.CodPanelista 
where b.CodPanelista is null order by a.CodPanelista;

select * from raw.dp_screener a left join raw.dp_pulse_Wave2_data b on a.CodPanelista = b.CodPanelista 
where b.CodPanelista is null order by a.CodPanelista;

select * from raw.dp_screener a left join raw.dp_pulse_Wave3_data b on a.CodPanelista = b.CodPanelista 
where b.CodPanelista is null order by a.CodPanelista;

select * from raw.dp_screener a left join raw.dp_pulse_Wave4_data b on a.CodPanelista = b.CodPanelista 
where b.CodPanelista is null order by a.CodPanelista;

-- distinct panelist list
select distinct a.CodPanelista from raw.dp_screener a left join raw.dp_pulse_Wave1_data b on a.CodPanelista = b.CodPanelista 
where b.CodPanelista is null order by a.CodPanelista;

select distinct a.CodPanelista from raw.dp_screener a left join raw.dp_pulse_Wave2_data b on a.CodPanelista = b.CodPanelista 
where b.CodPanelista is null order by a.CodPanelista;

select distinct a.CodPanelista from raw.dp_screener a left join raw.dp_pulse_Wave3_data b on a.CodPanelista = b.CodPanelista 
where b.CodPanelista is null order by a.CodPanelista;

select distinct a.CodPanelista from raw.dp_screener a left join raw.dp_pulse_Wave4_data b on a.CodPanelista = b.CodPanelista 
where b.CodPanelista is null order by a.CodPanelista;
**/