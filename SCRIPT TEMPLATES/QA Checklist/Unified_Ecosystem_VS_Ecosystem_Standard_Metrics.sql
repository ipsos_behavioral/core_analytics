---------------------------------------------------------------------------------------------------
-- OWNER
---------------------------------------------------------------------------------------------------
USE [PROJECTNAME];
--------------------------------------------------------------------------------------------------

-- select * from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report where mp_qualmonths < mu_usemonths;
-- select * from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report where dp_qualdays < du_usedays;
-- select * from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report where wp_qualweeks < wu_useweeks;

-- select * from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report where category = '* Total Ecosystem *';
-- select distinct(report_level) from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report;

------------------------------------------------
-- Check if unified output match with ecosystem 
------------------------------------------------
/**
-- 1. Create prep tables:
-- select * from #unified_total order by 1,2,3,4; 
begin try drop table #unified_total end try begin catch end catch;
select device_type, digital_type, report_level, content_category_name, dp, du, dp_qualdays,
du_visits,du_mins,du_mins_ti,wp_qualweeks,wu_useweeks,mp_qualmonths,mu_usemonths
into #unified_total
from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report 
where category = '* Total Ecosystem *' and subcategory = '[ Total Category ]' 
and segmentation_name = 'Total' and segment_name = 'Total'
--and device_type in ('Mobile', 'PC')
order by 1,2,3,4;

-- select * from #ecosystem_total order by 1,2,3,4;
begin try drop table #ecosystem_total end try begin catch end catch;
select device_type, digital_type, report_level, content_category_name, dp, du, dp_qualdays,
du_visits,du_mins,du_mins_ti,wp_qualweeks,wu_useweeks,mp_qualmonths,mu_usemonths
into #ecosystem_total
from [PROJECTNAME].rpt.Ecosystem_Standard_Metrics_Report
where category = '* Total Ecosystem *' and subcategory = '* Total Ecosystem *' 
order by 1,2,3,4;

-- 2. Create QA_Flags by content_category_name 
-- Note: have to use UNION for each content_category_name value because joins on content_category_name didn't work for last project. There are extra spaces in Ecosystem_Standard_Metrics_Report file.
-- For future projects make sure there's no extra space and we might be able to simplify the code by joining content_category_name directly. 

begin try drop table #QA_Flags end try begin catch end catch;
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
into #QA_Flags
from #unified_total a
inner join #ecosystem_total b 
on a.content_category_name = b.content_category_name
and a.device_type = b.device_type and a.digital_type = b. digital_type 
where a.content_category_name = ' Total Baby ' 
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'All Baby Hygiene ' and b.content_category_name = ' All Baby Hygiene'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'All Diaper ' and b.content_category_name = ' All Diaper'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'Baby Hygiene ' and b.content_category_name = ' Baby Hygiene'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'Diaper ' and b.content_category_name = ' Diaper'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'Diaper Rash ' and b.content_category_name = ' Diaper Rash'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'General Baby ' and b.content_category_name = ' General Baby';

-- 3. QA Output (0 if match; otherwise will show difference):
select * from #QA_Flags order by 1,2,3,4;
**/

-- END OF FILE --
