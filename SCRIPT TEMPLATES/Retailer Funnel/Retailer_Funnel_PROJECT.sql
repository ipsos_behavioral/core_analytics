-- NAME
-- Purchase Funnel Analysis
-- DATE
---------------------------------------------------------------------------------------------------
USE [PROJECTNAME];  --replace this and every instance of PROEJCTNAME in the script with your project schema (CTRL+F for PROJECTNAME and replace)

-- WHAT IS THIS REPORT
------------------------------------------------------------------
-- Purchase funnel analysis using shopper data and event type scrapings
-- 1. Identify retailers we want to analyze and if they are available in the shopper data: e.g. Walmart, Amazon, ...
-- 2. Data sources: e.g. Embee, Disqo 
-- Note: firstly check what retailers and shop event are available in the shopper data.
-- might need additional scrapings/joins to find search terms, and shopper events of retailers that are not available in the shopper data.

-- Tables to use:
-- shopper events:
-- [Raw].[unified_shop_events]
-- [Ref].[Taxonomy_Ecosystem] 
-- Shopping: Product Title, App: Search Record, Web: Search Record, (Web: Search Inferred)
-- retailer scraping: 
-- [Run].[v_Report_Web_Events]
-- core.ref.retailer_actions

------------------------------------------------------------------------------
-- A. PREP SHOPPER DATA:
------------------------------------------------------------------------------
-- A1. USE [Raw].[unified_shop_events]
-- Note: firstly check if the what retailers are available in the shopper event, 
-- and if they are web/app/or combined 
-- For dev: would be helpful to include app vs web flags in shopper table
------------------------------------------------------------------------------
-- 1. Shopper event product names matched on ecosystem
-- select top 1000 * from [Raw].[unified_shop_events];
-- select top 1000 * from ref.panelist;
-- select distinct retailer from [Raw].[unified_shop_events];
-- select distinct panelist_id from #shopper_product_name where shop_event = 'purchased_cart_item' ;
-- select distinct shop_event from [PROJECTNAME].[Raw].[unified_shop_events];
-- select distinct source_name from [PROJECTNAME].[Ref].[Taxonomy_Ecosystem]; 
-- select * from [PROJECTNAME].[Ref].[Taxonomy_Ecosystem] where source_name = 'Shopping: Product Title';
-- select * from #shopper_product_name order by record_id;

begin try drop table #shopper_product_name end try begin catch end catch;

select a.record_id, p.panelist_key as panelist_id, 'App' as digital_type,
case when retailer like '%Amazon%' then 'Amazon' else '' end as retailer_name, -- double check to make sure retailer names match in each table 
a.shopper_product_name as product_name, 
'' as search_term, -- double check if there're search terms in the shopper data 
case when a.shop_event = 'cart_item' then 'add to cart'
when a.shop_event = 'purchased_cart_item' then 'checkout'
else a.shop_event end as shop_event, 
a.start_time_local as start_time
into #shopper_product_name
from [Raw].[unified_shop_events] a
inner join ref.panelist p
on a.panelist_id = p.vendor_panelist_id
inner join [Ref].[shopper_product] b
on a.shopper_product_hash = b.shopper_product_hash
inner join [Ref].[Taxonomy_Ecosystem] c
on b.shopper_product_key = c.source_particle_key
and c.source_name = 'Shopping: Product Title'
where a.is_valid = 1
;
--(200 row(s) affected)


-- 2. Search terms matched on ecosystem (if shopper data doesn't have search events, use a different table such as sequence_event)
-- select * from #shopper_search order by record_id;
-- select distinct panelist_key from #shopper_search;
-- select top 100 * from [Run].[App_Events];
-- select top 100 * from [Run].[v_Report_App_Events];
-- select distinct source_name from [Ref].[Taxonomy_Ecosystem];
-- select * from [Ref].[Taxonomy_Ecosystem] where source_name = 'App: Search Record';
-- select * from [Ref].[Taxonomy_Ecosystem] where source_name = 'Web: Search Record';
-- select * from [Ref].[Taxonomy_Ecosystem] where source_name = 'Web: Search Inferred';

-- Run THIS PART WHEN SEARCH EVENT IS AVAILABLE IN SHOPPER DATA
/**
begin try drop table #shopper_search end try begin catch end catch;
select a.* 
into #shopper_search
from [Raw].[unified_shop_events] a
inner join [Ref].[Taxonomy_Ecosystem] b
on a.Record_ID = b.source_particle_key
and b.source_name = 'Web: Search Record' -- OR 'App: Search Record' OR BOTH
and b.category = 'Retailer'
where a.retailer in ('') 
and shop_event = 'productSearch'
and a.is_valid = 1;
**/

-- Run THIS PART WHEN SEARCH EVENT IS *NOT* AVAILABLE IN SHOPPER DATA
-- select top 1000 * from [Run].[v_Report_App_Events];
-- select top 1000 * from [rps].[v_Report_Sequence_Event];
-- select * from #shopper_search order by record_id;
begin try drop table #shopper_search end try begin catch end catch;
select a.record_id, a.panelist_id, 
case when notes like '%Amazon%' then 'Amazon' else '' end as retailer_name, -- double check to make sure retailer names match in each table 
a.digital_type, 
'' as product_name, a.search_term, 'search' as shop_event, a.start_time 
into #shopper_search
from [rps].[v_Report_Sequence_Event] a
inner join [Ref].[Taxonomy_Ecosystem] b
on a.Record_ID = b.source_particle_key
and b.source_name in ('App: Search Record', 'Web: Search Record') 
and b.category = 'Retailer'
and notes like '%Amazon%'
where a.digital_type = 'App' -- comment out if want all digital types 
;
--(79 row(s) affected)


-- Combine events
-- select * from #shopper_event_combined order by panelist_id, start_time;
-- select distinct shop_event from #shopper_event_combined;
begin try drop table #shopper_event_combined end try begin catch end catch;
(select record_id, panelist_id, retailer_name, digital_type, product_name, search_term, shop_event, start_time
into #shopper_event_combined
from #shopper_product_name)
union
(select record_id, panelist_id, retailer_name, digital_type, product_name, search_term, shop_event, start_time
from #shopper_search);
--(279 row(s) affected)



------------------------------------------------------------------------------
-- A2. SHOPPER EVENT SCRAPING FOR ADDITIONAL RETAILERS 
------------------------------------------------------------------------------
-- select * from core.ref.retailer_actions;
-- select distinct retailer_action from core.ref.retailer_actions;
-- select top 100 * from [rps].[v_Report_Sequence_Event_Ecosystem];
-- select distinct shop_event from #shopper_event_scraping;
-- select * from #shopper_event_scraping;
begin try drop table #shopper_event_scraping end try begin catch end catch;

 select a.record_id, a.session_id, p.panelist_key as panelist_id, a.notes, 
 case when a.notes = 'walmart.com' then 'Walmart' else '' end as retailer_name, 
 a.value as URL, a.digital_type,
 '' as product_name, a.search_term, 
 case when b.retailer_action in ('page view', 'product view') then 'view product detail'
 else b.retailer_action end as shop_event,
 a.start_time
 into #shopper_event_scraping
 FROM [rps].[v_Report_Sequence_Event_Ecosystem] a
 inner join ref.panelist p
 on a.panelist_id = p.vendor_panelist_id
 left outer join core.ref.retailer_actions b
 on a.Notes = b.retailer_domain and a.value like b.action_identifier 
 where (a.notes in ('walmart.com')) 
 and b.retailer_action in ('search', 'page view', 'product view','add to cart','checkout')
 ;
 -- 70

 -- QA FOR GREENLIST RETAILERS: include add to cart and checkout events in the same sessions
 begin try drop table #shopper_session_lookup end try begin catch end catch;

 select distinct panelist_id, session_id, notes 
 into #shopper_session_lookup
 from #shopper_event_scraping;

 -- select * from #shopper_event_scraping_QA;
 begin try drop table #shopper_event_scraping_QA end try begin catch end catch;

 select a.record_id, a.session_id, a.panelist_id, a.notes,
 case when a.notes = 'walmart.com' then 'Walmart' else '' end as retailer_name, 
 a.value as URL, a.digital_type,
 '' as product_name, a.search_term, c.retailer_action as shop_event,
 a.start_time
 into #shopper_event_scraping_QA
 from [rps].[v_Report_Sequence_Event] a
 inner join #shopper_session_lookup b
 on a.Panelist_ID = b.panelist_id and a.Session_ID = b.Session_ID and a.Notes = b.Notes
 left outer join core.ref.retailer_actions c
 on a.Notes = c.retailer_domain and a.value like c.action_identifier 
 where (a.notes in ('walmart.com')) 
 and c.retailer_action in ('add to cart','checkout');

-- Combine events
-- select * from #shopper_event_scraping_combined order by panelist_id, start_time;
-- select distinct shop_event from #shopper_event_scraping_combined;
begin try drop table #shopper_event_scraping_combined end try begin catch end catch;
(select record_id, panelist_id, retailer_name, digital_type, product_name, search_term, shop_event, start_time
into #shopper_event_scraping_combined
from #shopper_event_scraping)
union
(select record_id, panelist_id, retailer_name, digital_type, product_name, search_term, shop_event, start_time
from #shopper_event_scraping_QA);


-- Create final shopper event table with all retailers combined 
-- select retailer, shop_event, count(distinct panelist_key) as count from #walmart_instacart_kroger group by retailer, shop_event order by retailer, shop_event;
-- select * from #shopper_event_final;
begin try drop table #shopper_event_final end try begin catch end catch;

(select record_id, panelist_id, retailer_name, digital_type, product_name, search_term, shop_event, start_time
into #shopper_event_final
from #shopper_event_combined a)
union
(select record_id, panelist_id, retailer_name, digital_type, product_name, search_term, shop_event, start_time
from #shopper_event_scraping_combined);
--(49 row(s) affected)


----------------------------------------
-- B. CREATE PURCHASE FUNNEL TABLE:
----------------------------------------

-- TOTAL LEVEL PURCHASE FUNNEL
---------------------------------------------------------------------------------------------------
-- SHOPPER COUNTS BY EVENT TYPES
-- select distinct retailer, shop_event from [Raw].[unified_shop_events];
-- select distinct retailer_name, digital_type, shop_event from #shopper_event_final order by 1,2;
-- select * from #shopper_total order by 1,2,3;

-- Total Counts
/**
begin try drop table #shopper_total end try begin catch end catch;

select retailer, 'Total' as purchase_funnel, '0' as funnel_number,
count(distinct panelist_key) as panelist_count
into #shopper_total
from #retailer_event_total
group by retailer;
**/

-- Consideration
begin try drop table #shopper_total end try begin catch end catch;

select retailer_name, digital_type, cast('Consideration'as varchar(50)) as purchase_funnel, '1' as funnel_number,
count(distinct panelist_id) as panelist_count
into #shopper_total
from #shopper_event_final
where shop_event in ('search','view product detail','add to cart','checkout')
group by retailer_name,digital_type;
-- select * from #shopper_total;

-- Evaluation
insert into #shopper_total
select retailer_name, digital_type, cast('Evaluation'as varchar(50)) as purchase_funnel, '2' as funnel_number,
count(distinct panelist_id) as panelist_count
from #shopper_event_final
where shop_event in ('view product detail','add to cart','checkout')
group by retailer_name,digital_type;
-- select * from #shopper_total;

-- Purchase Intent
insert into #shopper_total
select retailer_name, digital_type, cast('Purchase Intent'as varchar(50)) as purchase_funnel, '3' as funnel_number,
count(distinct panelist_id) as panelist_count
from #shopper_event_final
where shop_event in ('add to cart','checkout')
group by retailer_name,digital_type;
-- select * from #shopper_total;

-- Online Purchase
insert into #shopper_total
select retailer_name, digital_type, cast('Online Purchase'as varchar(50)) as purchase_funnel, '4' as funnel_number,
count(distinct panelist_id) as panelist_count
from #shopper_event_final
where shop_event in ('checkout')
group by retailer_name,digital_type;
-- select * from #shopper_total order by 1,4;

--###########################################
-- CREATE PERMANENT TABLE FOR FUNNAL CHARTS
--###########################################
-- select * from rpt.shopper_event_funnel order by 1,4;

begin try drop table rpt.shopper_event_funnel end try begin catch end catch;

select * 
into rpt.shopper_event_funnel
from #shopper_total;