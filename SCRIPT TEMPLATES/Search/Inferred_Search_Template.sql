---------------------------------------------------------------------------------------------------
-- Inferred Ecosystem Search
---------------------------------------------------------------------------------------------------
-- OWNER
-- DATE
---------------------------------------------------------------------------------------------------


USE [PROJECTNAME];

-- Context: After the close-to-final Ecosystem Consolidation file is made, we can perform one last
-- check for potentially missed searches, by identifying all the proposed ecosystem domain/URL
-- and looking at 10 minutes prior to that event for searches that preceded it.

-- ================================================================================================
-- 1) Pull existing ecosystem data
-- ================================================================================================

-- WHITE DOMAINS
-----------------
--- White domains 
begin try drop table #ecosystem_sessions end try begin catch end catch;
select b.source_name as eco_source_type, min(c.Record_ID) as eco_session_1st_record_id, source_particle_key, source_particle,
source_particle_key as session_property_key, source_particle as session_property_name,
d.panelist_key, d.session_id, d.date_id as session_date_id, min(d.start_time) as session_start_time, d.digital_type_id
into #ecosystem_sessions
from ref.Taxonomy_Ecosystem b 
join ref.Taxonomy_Ecosystem_Mapping c on b.Taxonomy_Ecosystem_Key = c.Taxonomy_Ecosystem_Key
join rps.sequence_event d on c.Record_ID = d.Record_ID
where b.source_id = 23530 --Web: Domain
group by b.source_name, source_particle_key, source_particle, Panelist_Key, Session_ID, Date_ID, d.digital_type_ID
;
--(XX rows affected)
-- select * from #ecosystem_sessions;


-- GREEN URLS
--------------
insert into #ecosystem_sessions
select b.source_name as eco_source_type, min(c.Record_ID) as eco_session_1st_record_id, min(source_particle_key) as source_particle_key,min(source_particle) as source_particle,
f.Domain_Name_Key as session_property_key, f.Domain_Name as session_property_name,
d.panelist_key, d.session_id, d.date_id as session_date_id, min(d.start_time) as session_start_time, d.digital_type_id
from ref.Taxonomy_Ecosystem B  
join ref.Taxonomy_Ecosystem_Mapping c on b.Taxonomy_Ecosystem_Key = c.Taxonomy_Ecosystem_Key
join rps.sequence_event d on c.Record_ID = d.Record_ID
join ref.URL e on b.source_particle_key = e.URL_Key
join ref.Domain_Name f on e.Domain_Name_Hash = f.Domain_Name_Hash
where b.source_id = 23570 -- Web: URL
group by b.source_name, Panelist_Key, Session_ID, Date_ID, d.digital_type_ID, Domain_Name_Key, f.Domain_Name
;
--(XX rows affected)

insert into #ecosystem_sessions
select b.source_name as eco_source_type, min(c.Record_ID) as eco_session_1st_record_id, min(source_particle_key) as source_particle_key,min(source_particle) as source_particle,
e.Domain_Name_Key as session_property_key, e.Domain_Name as session_property_name,
d.panelist_key, d.session_id, d.date_id as session_date_id, min(d.start_time) as session_start_time, d.digital_type_id
from ref.Taxonomy_Ecosystem B 
join ref.Taxonomy_Ecosystem_Mapping c on b.Taxonomy_Ecosystem_Key = c.Taxonomy_Ecosystem_Key
join rps.sequence_event d on c.Record_ID = d.Record_ID
join run.v_rpt_url_title_mapping e on c.Record_ID = e.record_id
where b.source_id = 23574 -- Web: URL Record ID
group by b.source_name, Panelist_Key, Session_ID, Date_ID, d.digital_type_ID,e.domain_name_key, e.domain_name
;
--(XX rows affected)


insert into #ecosystem_sessions
select  b.source_name as eco_source_type, min(c.Record_ID) as eco_session_1st_record_id, source_particle_key, source_particle,
source_particle_key as session_property_key, source_particle as session_property_name,
d.panelist_key, d.session_id, d.date_id as session_date_id, min(d.start_time) as session_start_time, d.digital_type_id
from ref.Taxonomy_Ecosystem B 
join ref.Taxonomy_Ecosystem_Mapping c on b.Taxonomy_Ecosystem_Key = c.Taxonomy_Ecosystem_Key
join rps.sequence_event d on c.Record_ID = d.Record_ID
where b.source_id = 10060 -- App: Name
group by b.source_name, source_particle_key, source_particle, Panelist_Key, Session_ID, Date_ID, d.digital_type_ID
;
--(XX rows affected)


insert into #ecosystem_sessions
select b.source_name as eco_source_type, min(c.Record_ID) as eco_session_1st_record_id, min(source_particle_key) as source_particle_key,min(source_particle) as source_particle,
f.Domain_Name_Key as session_property_key, e.retailer as session_property_name,
d.panelist_key, d.session_id, d.date_id as session_date_id, min(d.start_time) as session_start_time, d.digital_type_id
from ref.Taxonomy_Ecosystem B 
join ref.Taxonomy_Ecosystem_Mapping c on b.Taxonomy_Ecosystem_Key = c.Taxonomy_Ecosystem_Key
join rps.sequence_event d on c.Record_ID = d.Record_ID
join raw.unified_shop_events e on c.Record_ID = e.record_id
join ref.Domain_Name f on e.retailer = f.Domain_Name
where b.source_id = 26010 -- Web: Shopping: Product Title
group by b.source_name, Panelist_Key, Session_ID, Date_ID, d.digital_type_ID,f.domain_name_key, e.retailer
;
--(XX rows affected)

-- ================================================================================================
--2) Pre-existing searches
-- ================================================================================================


-- BLUE SEARCH KEYS (FOUND PRIOR TO THIS EXERCISE)
------------------------------------------------
-- NEW VERSION SINCE WE ONLY HAVE RECORD_ID in TAXONOMY ECOSYSTEM
begin try drop table #blue_searches_preexisting end try begin catch end catch;
select a.search_term_key, b.source_particle as search_term, record_id
into #blue_searches_preexisting
from rps.sequence_event a
join ref.taxonomy_ecosystem b
	on a.record_id=b.source_particle_key
	and b.source_id = 23524--Web: Search Record 
;
--(XX rows affected)
-- select distinct(source_id) from googlesearchintent2020.ref.taxonomy_Ecosystem where vertical_id=530


------Process to update end time to parent
begin try drop table #raw_search_app_events end try begin catch end catch;
select vendor_session_id, record_id as child_record_id
into #raw_search_app_events
from raw.unified_app_events 
where search_term is not null
group by  vendor_session_id, record_id;
--(XX rows affected)

begin try drop table #search_parent_info end try begin catch end catch;
select a.*, b.record_id as parent_record_id, b.end_time_local as parent_end_time
into #search_parent_info
from #raw_search_app_events a
join raw.unified_app_events b 
on a.vendor_session_id = b.vendor_session_id 
and b.source = 'embee' -- source is embee for parent search and embgs for child
;
--(XX rows affected)


-- select count(*) from #preexisting_search_events where digital_type_id = 100;
-- select top 5 * from #preexisting_search_events;
-- Adding in pre-exisitng into web and media
begin try drop table #preexisting_search_events end try begin catch end catch;
select a.*
into #preexisting_search_events
from rps.sequence_event a
inner join #blue_searches_preexisting b
	on a.record_id = b.record_id
--	on a.Search_Term_Key=b.Search_Term_Key
;
--(XX rows affected)

update #preexisting_search_events 
set End_Time_Local = parent_end_time
from #preexisting_search_events a 
join #search_parent_info b 
	on a.Record_ID = b.child_record_id;
----(XX rows affected)
-- select * from #preexisting_search_events where record_id=9730595;
-- select * from #search_parent_info;


-- ================================================================================================
--3) All Possible Searches
-- ================================================================================================


-- BLUE DOMAINS & APPS
-- Starts with core search, narrows down to those in project, and then adds in bluelist from most recent project in case its missing from core.
--------------------------------------------------
-- select * from #blue_domains order by domain_name_key
-- select * from #blue_properties;
-- select top 100 * from ref.domain_name;
begin try drop table #blue_properties end try begin catch end catch;
select property_name, Domain_Name_Key as property_key, property_hash, digital_type_id
into #blue_properties
from ref.Domain_Name a join core.ref.Taxonomy_Common_Properties b on  a.Domain_Name_Hash = b.property_hash
and b.digital_type_id = 235
where common_supercategory like '%search%'
;
----(XX rows affected)

-- select * from #blue_apps;
insert into #blue_properties
select property_name,app_Name_Key as property_key, property_hash, digital_type_id
from ref.app_Name a
 join core.ref.Taxonomy_Common_Properties b on a.App_Name_Hash = b.property_hash
and b.digital_type_id = 100
where common_supercategory like '%search%';
--(XX rows affected)

-- select top 100 * from #all_search_events_v1 where search_term_key is null;
--select top 100 * from #preexisting_search_events ;
begin try drop table #all_search_events end try begin catch end catch;

select a.Record_ID, a. Panelist_Key, a.digital_type_ID, e.property_name, a.Value as search_url_key, a.Search_Term_Key, e.property_key, 
a.Start_Time, a.End_Time_Local as end_time,  a.dur_seconds/60 as dur_minutes, a.Date_ID, a.Session_ID,
case when c.Record_ID is not null then 1 else 0 end as preexist_flag, d.URL as search_url
into #all_search_events
from rps.sequence_event a
inner join #blue_properties b
	on a.Notes=b.property_key 
	and a.digital_type_ID = b.digital_type_id
left outer join #preexisting_search_events c
	on a.Record_ID=c.Record_ID
left outer join ref.url d
	on c.Value=d.URL_Key
	and c.digital_type_id = 235
join #blue_properties e on a.Notes = e.property_key and a.digital_type_ID = e.digital_type_id
where a.Search_Term_Key is not null; ---COMMENT THIS OUT IF WE HAVE REASON TO BELIEVE VEDNOR COULD HAVE MISSED SEARCH TERMS
-- --(XX rows affected)

update #all_search_events set End_Time = parent_end_time
from #all_search_events a join #search_parent_info b on a.Record_ID = b.child_record_id;
--(65745 rows affected)


----- Less necessary since we limit down to where search term keys are present now but still worth the check:
delete from #all_search_events where search_url like '%mail.google%'; --(XX rows affected)
delete from #all_search_events where search_url like '%play.google%'; --(XX rows affected)
delete from #all_search_events where search_url like '%calendar.google%'; --(XX rows affected)
delete from #all_search_events where search_url like  '%drive.google%'; --(XX rows affected)
delete from #all_search_events where search_url like  '%docs.google%'; --(XX rows affected)
-- Adservice
delete from #all_search_events where search_url like '%adservice%'; --(XX rows affected)
--Google redirects
delete from #all_search_events where search_url like  '%google.com/url%' or search_url like '%google.__/url%' or search_url like '%google.co.__/url%'; --(XX rows affected)
--Maps
delete from #all_search_events where search_url like '%maps.google%' or search_url like '%google.com/maps%' 
or search_url like '%google.co.__/maps%' or search_url like '%google.__/maps%' ; --(XX rows affected)

--Recaptcha
delete from #all_search_events where search_url like '%recaptcha%'; --(XX rows affected)
-- exclude non-search yahoo and aol
delete from #all_search_events where search_url like '%yahoo%' and search_url not like '%search%'; --(XX rows affected)
delete from #all_search_events where search_url  like '%aol%' and search_url not like '%search%'; --(XX rows affected)

-- select top 100 * from #all_search_events where digital_type_id = 100;


-- ================================================================================================
--4) OVERLAP ANALYSIS
-- ================================================================================================
begin try drop table #pre_existing_match end try begin catch end catch;

select eco_source_type, a.panelist_key, session_property_key, session_property_name,  a.session_id, session_date_id, session_start_time,
	eco_session_1st_record_id, a.source_particle_key, a.source_particle,
	a.digital_type_id as eco_digital_type,
	max(s.preexist_flag) as search_preexist_flag -- in case when prexisting search already identified
into #pre_existing_match
from #ecosystem_sessions a
left outer join #all_search_events s
	on a.panelist_key=s.panelist_key
	and a.session_date_id=s.date_id
	and s.Start_time<=a.session_start_time						-- search occurs before session event
	and a.session_start_time<=dateadd(minute,5,s.End_time)
	and a.eco_session_1st_record_id <> s.Record_ID-- search isn't the record itself
group by eco_source_type, a.panelist_key,  session_property_key, session_property_name, a.session_id, session_date_id, session_start_time,
	eco_session_1st_record_id, a.source_particle_key,a.source_particle, a.digital_type_id;
----(XX rows affected)

begin try drop table #ecosystem_sessions_with_candidate_search_v1 end try begin catch end catch;

select eco_source_type, a.panelist_key, session_property_key, session_property_name,  a.session_id, session_date_id, session_start_time,
	eco_session_1st_record_id, a.source_particle_key, a.source_particle,
	a.digital_type_id as eco_digital_type,
	max(s.Start_time) as search_start_time_rep -- in case when multiple searches happen before event, we want last search
into #ecosystem_sessions_with_candidate_search_v1
from #ecosystem_sessions a
left outer join #all_search_events s
	on a.panelist_key=s.panelist_key
	and a.session_date_id=s.date_id
	and s.Start_time<=a.session_start_time						-- search occurs before session event
	and a.session_start_time<=dateadd(minute,5,s.End_time)
	and a.eco_session_1st_record_id <> s.Record_ID-- search isn't the record itself
group by eco_source_type, a.panelist_key,  session_property_key, session_property_name, a.session_id, session_date_id, session_start_time,
	eco_session_1st_record_id, a.source_particle_key,a.source_particle, a.digital_type_id;
--(XX rows affected)

------Need to rematch back and take max in case multiple records start at the same  time
begin try drop table #ecosystem_sessions_with_candidate_search_v2 end try begin catch end catch;
select eco_source_type, a.panelist_key, session_property_key, session_property_name,  a.session_id, session_date_id, session_start_time,
	eco_session_1st_record_id, a.source_particle_key, a.source_particle,
	eco_digital_type,
	max(s.Record_ID) as record_id_search_event_rep,
	search_start_time_rep
into #ecosystem_sessions_with_candidate_search_v2
from #ecosystem_sessions_with_candidate_search_v1 a
left join #all_search_events s
on a.search_start_time_rep = s.Start_time
  and a.panelist_key=s.panelist_key
group by eco_source_type, a.panelist_key, session_property_key, session_property_name,  a.session_id, session_date_id, session_start_time,
	eco_session_1st_record_id, a.source_particle_key, a.source_particle,
	eco_digital_type,
	search_start_time_rep;
--(XX rows affected)

begin try drop table #ecosystem_sessions_with_candidate_search end try begin catch end catch;

select a.eco_source_type, a.panelist_key, a.session_property_key, a.session_property_name,  a.session_id, a.session_date_id, a.session_start_time,
	a.eco_session_1st_record_id, a.source_particle_key, a.source_particle,
	a.eco_digital_type,
	record_id_search_event_rep,
	search_preexist_flag,
	search_start_time_rep,
	s.end_time as search_end_time_rep 
into #ecosystem_sessions_with_candidate_search
from #ecosystem_sessions_with_candidate_search_v2 a 
join  #pre_existing_match b
on a.panelist_key = b.panelist_key and a.eco_session_1st_record_id = b.eco_session_1st_record_id
left join #all_search_events s
on a.search_start_time_rep = Start_time
  and a.record_id_search_event_rep = s.Record_ID
;
--(XX rows affected)
--*********
--This table has every single ecosystem activity along with any relevant potential search information
--It is unique to the ecosystem activity, but when multiple activities point to the same search, it will have duplicate searches
--Tables below de-dupe to individual searches as well as QA them
--*********
begin try drop table #ecosystem_event_searches end try begin catch end catch;

select a.eco_source_type, a.panelist_key, session_property_key, session_property_name,a.session_id, session_date_id, session_start_time, eco_session_1st_record_id,
	a.source_particle_key, a.source_particle, a.eco_digital_type, c.property_key as search_property_key,
	isnull(c.digital_type_id,'') as search_digital_type,
	record_id_search_event_rep, search_start_time_rep, search_end_time_rep,
	isnull(c.preexist_flag,'') as search_preexist_flag,
	isnull(d.url,'') as search_url,
	isnull(e.property_name,'' ) as search_app,
	isnull(s.Search_Term_Key,0) as search_term_key,
	isnull(s.search_term,'') as search_term
into #ecosystem_event_searches
from #ecosystem_sessions_with_candidate_search a
--inner join #ecosystem_sessions b
--	on a.source_particle_key=b.Value
left outer join #all_search_events c
	on a.record_id_search_event_rep=c.Record_ID
left outer join ref.url d
	on c.search_url_key=d.URL_Key
	and c.digital_type_id = 235
left join #blue_properties e
	on c.property_key = e.property_key
	and c.digital_type_id = 100
left outer join ref.Search_Term s
	on c.Search_Term_Key=s.Search_Term_Key
;
--(XX rows affected)
-- select top 1000* from #all_search_events;


/*
-----To see if commonality if search term should be added
select	search_url, search_term, max(search_preexist_flag) as search_preexist_flag
from #ecosystem_event_searches
group by search_url, search_term
order by search_preexist_flag , search_term desc, search_url;
*/

---------For now, search can be connected to more than 1 ecosystem touch
----------De-dupes by going for first time first from url then from media then from domain. Also narrows to just new searches.
--select top 100* from #deduped order by record_id_search_event_rep;
begin try drop table #deduped end try begin catch end catch;
select record_id_search_event_rep, eco_source_type, eco_digital_type,min(session_start_time) as earliest_start
into #deduped
from #ecosystem_event_searches
where record_id_search_event_rep is not null and search_preexist_flag = 0
group by record_id_search_event_rep, eco_source_type, eco_digital_type;
--(XX rows affected)


begin try drop table #best_match end try begin catch end catch;
select *
into #best_match
from #deduped where eco_source_type = 'Web: URL';
--(XX rows affected)


insert into #best_match
select b.*
from #best_match a right join #deduped b 
on a.record_id_search_event_rep = b.record_id_search_event_rep
where a.record_id_search_event_rep is null and b.eco_source_type = 'Web: URL Record ID' ;
--(XX rows affected)


insert into #best_match
select b.*
from #best_match a right join #deduped b 
on a.record_id_search_event_rep = b.record_id_search_event_rep
where a.record_id_search_event_rep is null and b.eco_source_type = 'Shopping: Product Title' ;
--(XX rows affected)


insert into #best_match
select b.*
from #best_match a right join #deduped b 
on a.record_id_search_event_rep = b.record_id_search_event_rep
where a.record_id_search_event_rep is null and b.eco_source_type = 'Web: Domain' ;
--(XX rows affected)


insert into #best_match
select b.*
from #best_match a right join #deduped b 
on a.record_id_search_event_rep = b.record_id_search_event_rep
where a.record_id_search_event_rep is null and b.eco_source_type = 'App: Name' ;
--(XX rows affected)




-- select * from #best_match where record_id_search_event_rep = 5817891;
-- select * from #ecosystem_event_searches where record_id_search_event_rep = 5817891;
-- select top 100 * from #ecosystem_event_searches;
--On the very off chance that someone visited two of the exact same eco_types within a close enough time that sql server doesnt pick up the time difference, it pulls the min of the url/media key. (only 1 case in VW)
begin try drop table #duped_source_key end try begin catch end catch;
select record_id_search_event_rep, search_digital_type, Panelist_Key, search_start_time_rep, session_start_time, eco_source_type, eco_digital_type, min(source_particle_key) as min_key
into #duped_source_key
from #ecosystem_event_searches
group by record_id_search_event_rep, search_digital_type, Panelist_Key, search_start_time_rep, session_start_time, eco_source_type, eco_digital_type;
--(XX rows affected)

------------Creates de-duped table of new events;
begin try drop table #inferred_search_events end try begin catch end catch;
select a.panelist_key, a.record_id_search_event_rep as record_id_inferred_search_event, a.search_digital_type,d.Platform_ID as search_platform_id ,search_url+search_app as search_url_or_app, a.search_term_key, a.search_term, a.search_property_key,
	 a.search_start_time_rep, a.search_end_time_rep, 
	eco_session_1st_record_id, a.session_start_time as eco_session_start_time, source_particle as eco_source_particle, 
	source_particle_key as eco_source_particle_key, b.eco_source_type,b.eco_digital_type
into #inferred_search_events
from #ecosystem_event_searches a
join rps.sequence_event d on a.record_id_search_event_rep = d.Record_ID
join #best_match b on a.record_id_search_event_rep = b.record_id_search_event_rep and a.eco_source_type = b.eco_source_type and a.session_start_time = b.earliest_start
join #duped_source_key c on a.source_particle_key = c.min_key and a.record_id_search_event_rep = c.record_id_search_event_rep and a.eco_source_type = c.eco_source_type and b.earliest_start = c.session_start_time
;
--(XX rows affected)

-- select top 100* from #inferred_search_events;

-- select search_scenario, count(*) as count from #search_scenarios group by search_scenario order by search_scenario;
-- select count (*) from #search_scenarios where search_scenario is null;
-- select count (*) from #search_scenarios where search_scenario is null and search_term_key = 0;
-- select top 100 * from #search_scenarios where search_scenario is null;
-- select top 100 * from #search_scenarios where search_digital_type = 100;
--------------------------------QA

begin try drop table #search_scenarios end try begin catch end catch;
select a.*, cast(NULL as varchar(300)) as search_scenario
into #search_scenarios
from #inferred_search_events a;
--(XX rows affected)

update #search_scenarios
set search_scenario = 'a. search app'
from #search_scenarios where search_digital_type = 100;
--(XX rows affected))


update #search_scenarios
set search_scenario = 'b. vendor missed search:443'
from #search_scenarios where search_url_or_app like '%443' and search_term_key = 0 and search_scenario is null;
--(XX rows affected)

update #search_scenarios
set search_scenario = 'c. vendor missed search: %search%'
from #search_scenarios where search_url_or_app like '%search%' and search_term_key = 0 and search_scenario is null;
--(XX rows affected)


update #search_scenarios
set search_scenario = 'd. vendor missed search: google.com/amp'
from #search_scenarios where search_url_or_app like 'google.com/amp%' and search_term_key = 0 and search_scenario is null;
--(XX rows affected)

update #search_scenarios
set search_scenario = 'e. vendor missed search: google no info'
from #search_scenarios where (search_url_or_app like '%google.com' or search_url_or_app like 'google.__' or search_url_or_app like '%google.__/' 
 or search_url_or_app like '%google.com/' or search_url_or_app like '%google.co.__/' or search_url_or_app like '%google.co.__') and search_term_key = 0 and search_scenario is null;
--(XX rows affected)

update #search_scenarios
set search_scenario = 'f. vendor missed search: duckduckgo,yandex,bing no info'
from #search_scenarios where (search_url_or_app like '%duckduckgo.com/' or search_url_or_app like '%yandex.com/' or search_url_or_app = 'bing.com') and search_term_key = 0 and search_scenario is null;
--(XX rows affected)


update #search_scenarios
set search_scenario = 'g. maps'
from #search_scenarios where (search_url_or_app like '%maps%') and search_term_key = 0 and search_scenario is null;
--(XX rows affected)

--(search_url_or_app like '%maps.google%' or search_url_or_app like '%google.com/maps%' or search_url_or_app like '%google.co.__/maps%' or search_url_or_app like '%google.__/maps%' )); 

update #search_scenarios
set search_scenario = 'g. vendor missed search: other'
from #search_scenarios where search_term_key = 0 and search_scenario is null;
--(XX rows affected)

update #search_scenarios
set search_scenario = 'h. vendor classified, not in eco yet'
from #search_scenarios where search_scenario is null;
--(XX rows affected)

-------Takes the search url and runs the search process on it (but allows ambiguous words)
begin try drop table #search_url_gra end try begin catch end catch;
select record_id_inferred_search_event, search_url_or_app, core.dbo.alphanum(search_url_or_app) as search_url_gra
into #search_url_gra
from #inferred_search_events;
--(XX rows affected)

begin try drop table #search_url_gra_best_pattern end try begin catch end catch;
select record_id_inferred_search_event,search_url_or_app ,min(pattern_id) as best_pattern_id
into #search_url_gra_best_pattern
from #search_url_gra a
inner join ref.pattern_table b
	on a.search_url_gra like b.pattern
group by record_id_inferred_search_event,search_url_or_app
;
--(XX rows affected)


begin try drop table #search_scenarios_with_semantic end try begin catch end catch;
select a.*, isnull(c.semantic_lemma,'') as semantic_lemma , --c.semantic_lemma_english,
isnull(c.semantic_priority_group,'') as semantic_priority_group 
into #search_scenarios_with_semantic
from #inferred_search_events a
left join #search_url_gra_best_pattern b on a.record_id_inferred_search_event = b.record_id_inferred_search_event
left join ref.pattern_table c on b.best_pattern_id = c.pattern_id 
;
--(XX rows affected)

-------Pull in any relevant tax eco info useful for classification
begin try drop table #tax_eco_dedupe end try begin catch end catch;
select source_particle_key, source_name, category, subcategory
into #tax_eco_dedupe
from ref.Taxonomy_Ecosystem
group by source_particle_key, source_name, category, subcategory;
--(16647 rows affected) select count(*) from ref.Taxonomy_Ecosystem where vertical_id=510; --87306

-- select top 100 * from #subcat_info;
--Takes the relevent taxonomy information for what it is connected to and takes it on
begin try drop table #subcat_info end try begin catch end catch;
select b.*, a.category as target_cat, a.subcategory as target_subcat
--a.content_category_1, a.content_category_2, a.content_category_3
into #subcat_info
from #tax_eco_dedupe a join #search_scenarios_with_semantic b
on a.source_particle_key = b.eco_source_particle_key
and a.source_name =  b.eco_source_type 
;
--(XX rows affected)

 --*************
 --Output for Excel QA
 --*************
 select *,'' as search_subcat, '' as brand_flag, '' as retailer_flag ,'' as accuracy_flag -- include columns for anything necessary in project
 from #subcat_info;

 /*
----------------------------------------------------------------------------------------------------------
-- THESE NEXT STEPS DONE OFFLINE IN EXCEL
-- Review list, and identify
	-- A) Suggested Incremental Search Keywords
	-- B) Record ID (for sequence events / processed app / processed web) of timestamped search page event or app visit we want assigned as search.

-- Use a combination of the search scenario and any semantic info to filter for easier QA and fill in accuracy flag. Suggestion:

a. search app -- include all, in absence of explicitly knowing its not a match, the steps taken already infer that this is probably accurate
b. RM missed search:443 -- same as app
c. RM missed search: %search% -- use semantic match to help, but may require manual work. if remove group, probably remove but make sure. Perhaps going forward can look at how much the search url matches the ecosystem url
d. RM missed search: google.com/amp -- same as %search%
e. RM missed search: google no info -- same as app
f. RM missed search: duckduckgo,yandex,bing no info -- same as app
g. maps -- incorrect, probably will handle this differently going forward
h. RM missed search: other -- same as %search%
i. RM classified, not in eco yet -- same as %search%

*/
--------------First pulls records where no search term key
begin try drop table #qa_upload end try begin catch end catch;
create table #qa_upload (
record_id_inferred_search_event int,
search_subcat nvarchar(300),
brand_flag int,
retailer_flag int
)
;


begin try drop table #consolidation end try begin catch end catch;
select a.*, b.search_subcat, b.brand_flag, b.retailer_flag
into #consolidation
from #subcat_info a join #qa_upload b 
on a.record_id_inferred_search_event = b.record_id_inferred_search_event
--(42 rows affected)


--(42 rows affected)
-- select count(*) from dbo.inferred_search_consolidation;
-- select record_id_inferred_search_event, count(*) as count into #temp4 from dbo.inferred_search_consolidation group by record_id_inferred_search_event; select * into #temp5 from #temp4 where count !=1;
-- select * from #temp5 a join dbo.inferred_search_consolidation b on a.record_id_inferred_search_event = b.record_id_inferred_search_event order by a.record_id_inferred_search_event;
-- select top 100 * from dbo.inferred_search_consolidation;


begin try drop table rps.inferred_search_records end try begin catch end catch;
select *
into rps.inferred_search_records
from #consolidation;

-- select * from rps.inferred_search_records;

-- END OF FILE