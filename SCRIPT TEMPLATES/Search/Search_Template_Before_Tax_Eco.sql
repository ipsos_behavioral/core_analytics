---------------------------------------------------------------------------------------------------
-- Search
---------------------------------------------------------------------------------------------------
-- NAME OF OWNER
-- DATE
---------------------------------------------------------------------------------------------------

USE [PROJECTNAME];

-- FULL LIST OF DW TABLES USED
-- select top 100 * from run.processed_web_events;			
-- select top 100 * from ref.domain_name;
-- select top 100 * from ref.search_term;
-- select top 100 * from ref.device;
-- select top 100 * from ref.Variable_Names;

---------------------------------------------------------------------------------------------------
-- THIS FIRST PORTION SHOULD BE IDENTICAL TO SEMANTIC 03 SCIPT
---------------------------------------------------------------------------------------------------

-- select top 100 * from ref.search_term;

-- SET UP GRA TABLE
-- select top 100 * from #search_term_gra;
-- drop table #search_term_gra;

begin try drop table #search_term_gra end try begin catch end catch;

select search_term_key, search_term, core.dbo.alphanum(search_term) as search_term_gra
into #search_term_gra
from ref.search_term
group by search_term_key, search_term
;
-- (XX rows affected)


---Gets bonus match
begin try drop table #search_gra_bonus_match end try begin catch end catch;
select a.Search_Term_Key, min(bonus_pattern_id) as best_bonus_pattern
into #search_gra_bonus_match 
from #search_term_gra a 
join ref.bonus_pattern_table b
on a.search_term_gra like b.bonus_pattern
group by a.Search_Term_Key;
--(XX rows affected)

begin try drop table #search_gra_best_bonus_match end try begin catch end catch;
select a.Search_Term_Key, b.bonus_lemma as bonus_word, b.bonus_priority_group
into #search_gra_best_bonus_match 
from #search_gra_bonus_match a  
join ref.bonus_pattern_table b
on a.best_bonus_pattern = b.bonus_pattern_id
group by Search_Term_Key, bonus_lemma, bonus_priority_group
;
--(XX rows affected)

-- select top 100 * from #search_term_gra_best_pattern where search_term_key = '516214';

begin try drop table #search_term_gra_best_pattern end try begin catch end catch;

select search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern
from #search_term_gra a
inner join ref.pattern_table b
	on a.search_term_gra like b.pattern
	and (run_on_green=1 or semantic_priority_group = 'REMOVE')
group by search_term_key;
--(XX rows affected)


begin try drop table #search_term_gra_best_pattern_ambiguous end try begin catch end catch;

select a.Search_Term_Key, c.bonus_word, c.bonus_priority_group ,min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern_ambiguous
from #search_term_gra a
inner join ref.pattern_table b
	on a.search_term_gra like b.pattern
	and (run_on_green=0 or semantic_priority_group='Remove')
inner join #search_gra_best_bonus_match c on a.Search_Term_Key = c.Search_Term_Key
group by a.Search_Term_Key, c.bonus_word, c.bonus_priority_group;
--(XX rows affected)


begin try drop table #best_search_patterns_v1 end try begin catch end catch;
select a.search_term_key, a.best_pattern_id, b.bonus_word, b.bonus_priority_group
into #best_search_patterns_v1
from #search_term_gra_best_pattern a
left join #search_gra_best_bonus_match  b on a.Search_Term_Key = b.Search_Term_Key;
--(XX rows affected)


--It creates dupes if the search matched on both ambiguous and unambiguous words, so next step will choose the lower priority word
insert into #best_search_patterns_v1
select a.Search_Term_Key, a.best_pattern_id, a.bonus_word, a.bonus_priority_group
from #search_term_gra_best_pattern_ambiguous a
;
--(XX rows affected)


begin try drop table #true_best_pattern end try begin catch end catch;
select search_term_key, min(best_pattern_id) as true_best_pattern
into #true_best_pattern
from #best_search_patterns_v1
group by search_term_key;
--(XX rows affected)



begin try drop table #best_search_patterns end try begin catch end catch;
select a.search_term_key, best_pattern_id, bonus_word, bonus_priority_group
into #best_search_patterns
from #best_search_patterns_v1 a 
join #true_best_pattern b on a.search_term_key = b.search_term_key and a.best_pattern_id = b.true_best_pattern
group by a.search_term_key, best_pattern_id, bonus_word, bonus_priority_group
;
--(XX rows affected)

begin try drop table #search_semantic_match_v1 end try begin catch end catch;

select a.search_term_key, a.search_term, c.pattern_id as search_pattern_id, c.pattern as search_pattern,
	c.semantic_priority_group as search_semantic_group, 
	c.semantic_lemma as search_semantic_keyword
	--c.semantic_lemma_english as search_Semantic_keyword_english
into #search_semantic_match_v1
from #search_term_gra a
inner join #best_search_patterns b
	on a.search_term_key=b.search_term_key
inner join ref.pattern_table c
	on b.best_pattern_id=c.pattern_id
group by a.search_term_key, a.search_term, c.pattern_id, c.pattern,
	c.semantic_priority_group, 
	c.semantic_lemma;
;
--(XX rows affected)

delete from #search_semantic_match_v1 where search_semantic_group='Remove'; 
--(XX rows affected)


------------------Final repass with all terms if it made it in 

begin try drop table #search_term_gra_best_pattern_repass end try begin catch end catch;

select a.search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern_repass
from #search_term_gra a
inner join ref.pattern_table b
	on a.search_term_gra like b.pattern
inner join #search_semantic_match_v1 c on a.Search_Term_Key = c.Search_Term_Key
--	and flag_ambiguity=0
group by a.search_term_key;
--(XX rows affected)

begin try drop table #search_semantic_match end try begin catch end catch;
select a.search_term_key, a.search_term, a.search_term_gra,c.pattern_id as search_pattern_id, c.pattern as search_pattern,
	c.semantic_priority_group as search_semantic_group, 
	c.semantic_lemma as search_semantic_keyword
	--c.semantic_lemma_english as search_Semantic_keyword_english
into #search_semantic_match
from #search_term_gra a
inner join #search_term_gra_best_pattern_repass b
	on a.search_term_key=b.search_term_key
inner join ref.pattern_table c
	on b.best_pattern_id=c.pattern_id
group by a.search_term_key, a.search_term, a.search_term_gra,c.pattern_id, c.pattern,
	c.semantic_priority_group, 
	c.semantic_lemma
	;
--(XX rows affected)



-- add search_semantic_keyword_english column for foreign language projects
--select Search_Term_Key, Search_Term, search_pattern, search_semantic_keyword, search_semantic_group from #search_semantic_match where search_term_key = '516214';;
--select * from #search_semantic_match_final;


---------------------------------------------------------------------------------------------------
-- 1A) Search Terms from (Inclusion List)
---------------------------------------------------------------------------------------------------
-- select top 1000 * from run.web_events where search_term_key = '420425';
-- select top 1000 * from #temp_search_event where search_term_key = '420425';

-- select count(*) from run.web_events;

-- fetch panelist_id, domain_name, search_term, date, timestamp, usage_date
-- select top 1000 * from #temp_search_event where search_term_key = '516214' order by panelist_key, min_timestamp;
-- select * from #temp_search_event where panelist_key = 57 and min_timestamp = 'Feb  2 2019  3:31PM';
-- drop table #temp_search_event;
-- Similar to after tax eco version but pulls from search semantic match table and pulls in the record id at this stage
begin try drop table #full_search_event_info end try begin catch end catch;
select a.*,
p.Record_ID,
p.panelist_key,
p.Date_ID,
p.Device_Key,
p.Start_time,
d.domain_name as property_name,
v.val_name as device_type
into #full_search_event_info
from #search_semantic_match a
join run.web_events p on a.search_term_key = p.Search_Term_Key
join ref.domain_name d
	on p.domain_name_key = d.domain_name_key
join ref.device de
	on p.device_key = de.Device_Key
join ref.Variable_Names v
	on de.Platform_id = v.val_id
	and var_name = 'platform_id'
;
--(XX rows affected)


insert into #full_search_event_info
select 
a.*,
p.Record_ID,
p.panelist_key,
p.Date_ID,
p.Device_Key,
p.Start_time,
d.app_name as property_name,
v.val_name as device_type
from #search_semantic_match a
join run.app_events p on a.search_term_key = p.Search_Term_Key
inner join ref.app_name d
	on p.app_name_key = d.app_name_key
inner join ref.device de
	on p.device_key = de.Device_Key
inner join ref.Variable_Names v
	on de.Platform_id = v.val_id
	and var_name = 'platform_id'
--(XX rows affected)

------------------Identical to after tax eco version from this point out since we want to do pass throughs in the same way
-- select * from #full_search_event_info;
-- select top 100 * from #temp_search_event_prep;
begin try drop table #temp_search_event_prep end try begin catch end catch;

select
	panelist_key,
	Device_Key,
	device_type, 
	property_name,
	Search_Term_Key,
	search_term,
	search_term_gra,
	Date_ID,
	cast((count(*)) as varchar(350)) as pages,
	min(start_time) as min_timestamp,
	max(start_time) as max_timestamp
into #temp_search_event_prep
from #full_search_event_info
group by panelist_key,
	Device_Key,
	device_type, 
	property_name,
	Search_Term_Key,
	search_term,
	search_term_gra,
	Date_ID
;
--(XX rows affected)

-- select top 100 * from #full_search_event_info where search_term_key = 20165 and panelist_key = 179 and date_id = '2020-11-16'  order by start_time;
-- select top 100 * from #temp_search_event_prep where search_term_key = 20165 and panelist_key = 179 and date_id = '2020-11-16';
-- select top 100 * from #temp_search_event where search_term_key = 100890 and panelist_key = 37 and date_id = '2020-11-17';
-- select * from #temp_search_event where first_record_id = last_record_id and pages <>1;
-----Select top 100 * from #temp_search_event;
----------------PULLS IN FIRST AND LAST RECORD FOR JOINING 
begin try drop table #temp_search_event end try begin catch end catch;

select a.panelist_key,
	a.Device_Key,
	a.device_type, 
	a.property_name,
	a.Search_Term_Key,
	a.search_term,
	a.search_term_gra,
	a.Date_ID,
	a.pages,
	min_timestamp,
	max_timestamp,
min(b.record_id) as first_record_id, max(c.record_id) as last_record_id --- still need min and max just in case dupe records at same time
into #temp_search_event
from #temp_search_event_prep a
join #full_search_event_info b
on a.panelist_key = b.Panelist_Key --- Join on all columns that ID
	and a.Device_Key=b.Device_Key
	and a.device_type = b.device_type 
	and a.property_name= b.property_name
	and a.Search_Term_Key = b.Search_Term_Key
	and a.Date_ID = b.Date_ID
	and a.min_timestamp = b.start_time
join #full_search_event_info c
on a.panelist_key = c.Panelist_Key --- Join on all columns that ID
	and a.Device_Key=c.Device_Key
	and a.device_type = c.device_type 
	and a.property_name= c.property_name
	and a.Search_Term_Key = c.Search_Term_Key
	and a.Date_ID = c.Date_ID
	and a.max_timestamp = c.start_time
group by 	a.panelist_key,
	a.Device_Key,
	a.device_type, 
	a.property_name,
	a.Search_Term_Key,
	a.search_term,
	a.search_term_gra,
	a.Date_ID,
	a.pages,
	a.min_timestamp,
	a.max_timestamp,
	b.panelist_key,
	b.Device_Key,
	b.device_type, 
	b.property_name,
	b.Search_Term_Key,
	b.search_term,
	b.Date_ID,
	c.panelist_key,
	c.Device_Key,
	c.device_type, 
	c.property_name,
	c.Search_Term_Key,
	c.search_term,
	c.Date_ID
;
--(XX rows affected)

--------------------------------------------------------------------------------------------------
-- 1B) Search Output
---------------------------------------------------------------------------------------------------
begin try drop table #search_output end try begin catch end catch;

select 
	a.*,
	cast(concat(a.panelist_key,'_',date_id) as varchar(350)) as search_day,
	cast(concat(a.panelist_key,'_',date_id,a.property_name,a.Search_Term_Key) as varchar(350)) as search_id,
	case when a.property_name like '%google%' then 'google'
	when a.property_name like '%amazon%' then 'amazon'
	when a.property_name like 'yahoo%' then 'yahoo.com'
	when a.property_name like 'bing%' then 'bing.com'
	else a.property_name
	end as property_group,
	cast('' as nvarchar(300)) as semantic_keyword,
	cast('' as nvarchar(300)) as search_semantic_group,
	cast('' as nvarchar(300)) as brand,
	cast('' as nvarchar(300)) as retailer
into #search_output
from #temp_search_event a
;
--(XX rows affected)


--------- Non retailer or branded
begin try drop table #search_term_gra_best_pattern_no_brand_retailer end try begin catch end catch;

select a.search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern_no_brand_retailer
from #search_term_gra a
inner join ref.pattern_table b
	on a.search_term_gra like b.pattern
	and b.semantic_priority_group not in (
	'Brand'
	,'Category Retailer'
	)
inner join #search_output c on a.Search_Term_Key = c.Search_Term_Key
group by a.search_term_key;
--(XX rows affected)

-- select top 100 * from #best_search_patterns_no_brand_retailer;
begin try drop table #best_search_patterns_no_brand_retailer  end try begin catch end catch;
select a.search_term_key, a.best_pattern_id, b.semantic_lemma, b.semantic_priority_group
into #best_search_patterns_no_brand_retailer 
from #search_term_gra_best_pattern_no_brand_retailer a
join ref.pattern_table b 
	on a.best_pattern_id = b.pattern_id
group by a.search_term_key, a.best_pattern_id, b.semantic_lemma, b.semantic_priority_group;
--(XX rows affected)



update #search_output
set semantic_keyword = b.semantic_lemma, search_semantic_group = b.semantic_priority_group
from #search_output a
join #best_search_patterns_no_brand_retailer b on a.Search_Term_Key = b.Search_Term_Key;
--(XX rows affected)

--------- Brand
begin try drop table #search_term_gra_best_pattern_brand end try begin catch end catch;

select a.search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern_brand
from #search_term_gra a
inner join ref.pattern_table b
	on a.search_term_gra like b.pattern
	and b.semantic_priority_group = 'Brand'
inner join #search_output c on a.Search_Term_Key = c.Search_Term_Key
group by a.search_term_key;
--(XX rows affected)


begin try drop table #best_search_patterns_brand end try begin catch end catch;
select a.search_term_key, a.best_pattern_id, b.semantic_lemma, b.semantic_priority_group
into #best_search_patterns_brand
from #search_term_gra_best_pattern_brand a
join ref.pattern_table b on a.best_pattern_id = b.pattern_id
group by a.search_term_key, a.best_pattern_id, b.semantic_lemma, b.semantic_priority_group;
--(XX rows affected)



update #search_output
set brand = b.semantic_lemma
from #search_output a
join #best_search_patterns_brand b on a.Search_Term_Key = b.Search_Term_Key;
--(XX rows affected)


----Makes keyword brand if no other content
update #search_output
set semantic_keyword = b.semantic_lemma, search_semantic_group = semantic_priority_group
from #search_output a
join #best_search_patterns_brand b on a.Search_Term_Key = b.Search_Term_Key
where a.semantic_keyword = '';
--(XX rows affected)


--------- Retailer Pass
begin try drop table #retailer_pattern end try begin catch end catch;
select pattern_id, run_on_white, run_on_green, pattern, semantic_lemma, semantic_priority_id, semantic_priority_group
into #retailer_pattern 
from ref.pattern_table
where semantic_priority_group like '%retailer%';
--(XX rows affected)

set identity_insert #retailer_pattern on;
insert into #retailer_pattern  (pattern_id, run_on_white, run_on_green, pattern, semantic_lemma, semantic_priority_id, semantic_priority_group)
select pattern_id, run_on_white, run_on_green, pattern, semantic_lemma, semantic_priority_id, semantic_priority_group
from Core.ref.general_retailer_pattern;
--(XX rows affected)


begin try drop table #search_term_gra_best_pattern_retailer end try begin catch end catch;

select a.search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern_retailer
from #search_term_gra a
inner join #retailer_pattern b
	on a.search_term_gra like b.pattern
	and search_term not like '%Amazon.com:%' -- since amazon data sometimes comes in flagged
inner join #search_output c on a.Search_Term_Key = c.Search_Term_Key
group by a.search_term_key;
--(XX rows affected)

begin try drop table #best_search_patterns_retailer end try begin catch end catch;
select a.search_term_key, a.best_pattern_id, b.semantic_lemma, b.semantic_priority_group
into #best_search_patterns_retailer
from #search_term_gra_best_pattern_retailer a
join #retailer_pattern b on a.best_pattern_id = b.pattern_id
group by a.search_term_key, a.best_pattern_id, b.semantic_lemma, b.semantic_priority_group;
--(XX rows affected)


update #search_output
set retailer= b.semantic_lemma
from #search_output a
join #best_search_patterns_retailer b on a.Search_Term_Key = b.Search_Term_Key;
--(XX rows affected)



----Makes keyword retailer if no other content or brand 
update #search_output
set semantic_keyword = b.semantic_lemma, search_semantic_group = semantic_priority_group
from #search_output a
join #best_search_patterns_retailer b on a.Search_Term_Key = b.Search_Term_Key
where a.semantic_keyword = '';
--(XX rows affected)

begin try drop table rpt.Search_Delivery end try begin catch end catch;

select first_record_id,last_record_id, a.Panelist_Key, Search_Term,
device_type, property_name,property_group,  Search_Term_Key, Date_ID, pages,
min_timestamp,max_timestamp, search_day, search_id, semantic_keyword, search_semantic_group,
 brand, retailer,  case when brand <>'' then 'Brand' else 'Not Brand' end as brand_flag,
 case when retailer <>'' then 'Retailer' else 'Not Retailer' end as retailer_flag
into rpt.Search_Delivery
from #search_output a
;
--(XX rows affected)

-------Check this to make sure correct when its actually done
update rpt.Search_Delivery
set brand_flag = 'Brand'
from rpt.Search_Delivery  a
join rps.inferred_search_records b 
on a.first_record_id = b.record_id_inferred_search_event -- should be the correct join, but make sure
and b.brand_flag = 1
;

update rpt.Search_Delivery
set retailer_flag = 'Retailer'
from rpt.Search_Delivery  a
join rps.inferred_search_records b 
on a.first_record_id = b.record_id_inferred_search_event -- should be the correct join, but make sure
and b.retailer_flag = 1
;



--------------------------------
-- FINAL OUTPUT TABLE FOR EXCEL
--------------------------------
----HIDE FIRST AND LAST RECORDS and max timestamp IN EXCEL OUTPUT
select *
 from rpt.Search_Delivery
order by panelist_key,min_timestamp ,date_id;

-- END OF FILE

-- END OF FILE