---------------------------------------------------------------------------------------------------
-- Search with Motivation Indicators
---------------------------------------------------------------------------------------------------
-- Madison: 10/25/2019
-- Created based on Bayer_Search
---------------------------------------------------------------------------------------------------

USE MayoClinic;
-- FULL LIST OF DW TABLES USED
-- select top 100 * from run.processed_web_events;			
-- select top 100 * from ref.domain_name;
-- select top 100 * from ref.search_term;
-- select top 100 * from ref.device;
-- select top 100 * from ref.Variable_Names;
-- select top 100 * from Core.ref.search_indicators;

---------------------------------------------------------------------------------------------------
-- 0) Create #search_semantic_match Table.
---------------------------------------------------------------------------------------------------

-- select top 100 * from ref.search_term;

-- SET UP GRA TABLE
-- select top 100 * from #search_term_gra;
begin try drop table #search_term_gra end try begin catch end catch;
select search_term_key, search_term, core.dbo.alphanum(search_term) as search_term_gra
into #search_term_gra
from MayoClinic.ref.search_term
group by search_term_key, search_term;
-- (XX row(s) affected)

-- select * from [ref].[pattern_table_mayo];
-- drop table #search_term_gra_best_pattern;
begin try drop table #search_term_gra_best_pattern end try begin catch end catch;
select search_term_key, min(pattern_id) as best_pattern_id
into #search_term_gra_best_pattern
from #search_term_gra a
inner join [ref].[pattern_table_mayo] b
	on a.search_term_gra like b.pattern
	and (run_on_green=1 or semantic_priority_group = 'Remove')
--	and flag_ambiguity=0
group by search_term_key;
-- (548 row(s) affected)

--select top 100 * from [ref].[pattern_table_mayo] where semantic_lemma = 'bebe'

begin try drop table #search_semantic_match end try begin catch end catch;
select a.search_term_key, a.search_term, c.pattern_id as search_pattern_id, c.pattern as search_pattern,
	c.semantic_priority_group as search_semantic_group, c.semantic_lemma as search_semantic_lemma
into #search_semantic_match
from #search_term_gra a
inner join #search_term_gra_best_pattern b
	on a.search_term_key=b.search_term_key
inner join [ref].[pattern_table_mayo]  c
	on b.best_pattern_id=c.pattern_id;
-- (566 row(s) affected)

delete from #search_semantic_match where search_semantic_group='Remove'; 

select Search_Term_Key, Search_Term, search_pattern, search_semantic_lemma, search_semantic_group from #search_semantic_match;
---------------------------------------------------------------------------------------------------
-- 1A) Search Terms from (Inclusion List)
---------------------------------------------------------------------------------------------------
-- select top 1000 * from run.processed_web_events;
-- select count(*) from run.processed_web_events;

-- fetch panelist_id, domain_name, search_term, date, timestamp, usage_date
-- select top 1000* from #temp_search_event order by panelist_key, min_timestamp;
-- select * from #temp_search_event where panelist_key = 57 and min_timestamp = 'Feb  2 2019  3:31PM';
begin try drop table #temp_search_event end try begin catch end catch;
select p.panelist_key,
	p.Device_Key,
	v.val_name as device_type, 
	d.domain_name,
	s.search_term,
	p.Date_ID,
	cast((count(*)) as varchar(350)) as pages,
	cast((min(p.start_time)) as varchar(350)) as min_timestamp,
	s.search_term_key -- pass this along so tagged columns can be joined
into #temp_search_event
from [MayoClinic].run.processed_web_events p
inner join [MayoClinic].ref.domain_name d
	on p.domain_name_key = d.domain_name_key
inner join [MayoClinic].ref.search_term s
	on p.search_term_key = s.search_term_key
inner join [MayoClinic].ref.device de
	on p.device_key = de.Device_Key
inner join [MayoClinic].ref.Variable_Names v
	on de.Platform_id = v.val_id
	and var_name = 'platform_id'
group by p.panelist_key, p.device_key, v.val_name, d.domain_name, s.search_Term, p.date_id, s.search_term_key
order by panelist_key
;
--(22874 row(s) affected)

-- select * from ref.Panelist; 
-- select count(distinct Panelist_Key) from ref.Panelist; 
	-- 254
-- select count(distinct Panelist_Key) from #temp_search_event; 
	-- 193

--------------------------------------------------------------------------------------------------
-- 1B) Search Output
---------------------------------------------------------------------------------------------------
-- select count(*) from #temp_search_event; 
	-- 22874
-- select distinct domain_name from #search_cat;
-- select top 100 * from #temp_search_event;
-- select top 1000 * from [ref].[Taxonomy_Ecosystem];
-- select * from #search_cat order by panelist_key, date_id;
begin try drop table #seach_cat end try begin catch end catch;
select 
	a.Panelist_Key,
	a.Domain_Name,
	a.device_type,
	'' as search_term,
	a.Date_ID,
	a.min_timestamp as time_stamp,
	a.pages,
	cast(concat(a.panelist_key,'_',date_id) as varchar(350)) as search_day,
	cast(concat(a.panelist_key,'_',date_id,a.Domain_Name,a.Search_Term_Key) as varchar(350)) as search_id,
	case when a.Domain_Name like 'google%' then 'google.com'
	when a.Domain_Name like 'amazon%' then 'amazon.com'
	when a.Domain_Name like 'yahoo%' then 'yahoo.com'
	when a.Domain_Name like 'bing%' then 'bing.com'
	else a.Domain_Name
	end as domain_group,
	a.Search_Term_Key,
	t.subcategory, 
	t.content_category_1,
	t.content_category_2,
	s.search_semantic_lemma as semantic_lemma,
	a.Search_Term as search_term_vlookup
into #search_cat
from #temp_search_event a
inner join #search_semantic_match s 
	on a.Search_Term_Key = s.search_term_key
inner join [MayoClinic].[ref].[Taxonomy_Ecosystem] t
	on a.Search_Term_Key = t.source_particle_key
	and t.source_name = 'Web: Search Term'
;
-- (683 row(s) affected)
--select top 100 * from [BayerNappy].[ref].[Taxonomy_Ecosystem] where source_name = 'Web: Search Term'
--select top 100 * from [BayerNappy].[ref].[Taxonomy_Ecosystem] where content_category is null
--select distinct search_term_key, subcategory from #search_cat

select * from #search_cat 
-- where search_term_key not in (1725, 1726) -- exclude false positives 
order by panelist_key, date_id;

------------------------------------------------------
--Motivation Flags
------------------------------------------------------
-- select * from Core.ref.search_indicators;
-- select * from #motivation_flag1 order by panelist_key, search_term_key;
-- select * from #motivation_flag1 where search_term_key = 252803;
-- select * from #motivation_flag1 where indicator is not null;
begin try drop table  #motivation_flag1 end try begin catch end catch;
select a.*, case when b.sensitivity_indicator = 'quality' then 1 else 0 end as quality_flag, 
case when b.sensitivity_indicator = 'price' then 1 else 0 end as price_flag,
case when b.sensitivity_indicator = 'location' then 1 else 0 end as location_flag,
case when b.sensitivity_indicator = 'novelty' then 1 else 0 end as novelty_flag,
b.lemma_english as indicator
into #motivation_flag1
from #search_cat a left join Core.ref.search_indicators b
on a.search_term_vlookup like b.pattern;

---- select * from #motivation_flag2 where search_term_key = 252803;
begin try drop table  #motivation_flag2 end try begin catch end catch;
select Panelist_Key, Domain_Name, device_type, search_term, Date_ID, time_stamp, pages, search_day, search_id,
domain_group, Search_Term_Key, subcategory, content_category_1, content_category_2, semantic_lemma, search_term_vlookup,
sum(quality_flag) as quality_flag, sum(price_flag) as price_flag, sum(location_flag) as location_flag, 
sum(ambiguity_flag) as ambiguity_flag, min(indicator) as indicator
into #motivation_flag2
from #motivation_flag1
group by Panelist_Key, Domain_Name, device_type, search_term, Date_ID, time_stamp, pages, search_day, search_id,
domain_group, Search_Term_Key, subcategory, content_category_1, content_category_2, semantic_lemma, search_term_vlookup;
------------------------------------------------------
-- Unified Search:
------------------------------------------------------
begin try drop table  #unified_search_output end try begin catch end catch;
select a.*, b.segmentation_name, b.segment_name 
into #unified_search_output
from #motivation_flag2 a
inner join ref.Panelist_Segmentation b
on a.Panelist_Key = b.panelist_key
order by panelist_key;

-- output:
select * from #unified_search_output 
--where segmentation_name in ('Purchase Category', 'Age Group') 
--where ambiguity_flag is not null
order by panelist_key, date_id;

-------------------------------------------------------
-- QA: 
-------------------------------------------------------
-- Overall:
-- Check on search term coverage
-- select count(distinct search_term_key) from #search_semantic_match; -- 368
-- select count(distinct Search_Term_Key) from #search_cat; -- 356

/**
select Search_Term
from #search_semantic_match a
where search_term not in (select search_term from #search_cat);
**/

-- Excel Output:
/**
select content_category, count(distinct Panelist_Key) as distinct_panelists, 
count(distinct search_day) as search_days,
count(distinct search_id) as searches,
count(Domain_Name) as search_pages,
cast(count(distinct search_id) as decimal(6,1))/cast(count(distinct Panelist_Key) as decimal(6,1)) as searches_per_searcher,
cast(count(distinct search_id) as decimal(6,1))/cast(count(distinct search_day) as decimal(6,1)) as searches_per_search_day
from #search_cat group by content_category
order by distinct_panelists desc;

select Domain_Name, count(distinct Panelist_Key) as distinct_panelists, 
count(distinct search_day) as search_days,
count(distinct search_id) as searches,
count(Domain_Name) as search_pages,
cast(count(distinct search_id) as decimal(6,1))/cast(count(distinct Panelist_Key) as decimal(6,1)) as searches_per_searcher,
cast(count(distinct search_id) as decimal(6,1))/cast(count(distinct search_day) as decimal(6,1)) as searches_per_search_day
from #search_cat group by Domain_Name
order by distinct_panelists desc;
**/

-- For Unified:
-- select * from #unified_search_output where Panelist_Key = 169;
-- select count(distinct Panelist_Key) from #unified_search_output --62
-- select count(distinct Panelist_Key) from #search_cat; -- 62

/**
select * from #unified_search_output 
where segmentation_name is null;
select * from #unified_search_output 
where segment_name is null;
-- Segmentation count
select segmentation_name, segment_name, count(distinct Panelist_Key) as distinct_panelist from #unified_search_output 
group by segmentation_name, segment_name
order by segmentation_name, segment_name;
**/

-- END OF FILE