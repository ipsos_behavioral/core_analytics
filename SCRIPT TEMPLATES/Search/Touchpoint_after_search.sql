----------------
---Touchpoint After Search
----------------

use PROJECTNAME;

------------Pulls the start time for the first record following a search within 5 mins of the search end time (okay if end time isnt corrected for app searches)
-- select * from rpt.search_delivery where retailer_flag = 1;
begin try drop table #following_start_time end try begin catch end catch;
select s.last_record_id as search_last_record_id, s.panelist_key,p.Vendor_Panelist_ID as Panelist_ID, s.search_term, s.device_type,s.property_name, s.property_group,
s.search_term_key, s.date_id, s.max_timestamp as search_max_timestamp, s.search_day, s.search_id, s.semantic_keyword, s.search_semantic_group, 
 brand_flag  as search_brand_flag,retailer_flag as search_retailer_flag,
 min(b.Start_time) as start_time_next_touchpoint
into #following_start_time
from rpt.search_delivery s
join ref.Panelist p on s.panelist_key = p.Panelist_Key
join rps.sequence_event l on s.last_record_id = l.Record_ID
left join
 rps.v_Report_Sequence_Events_P2P b
 on p.Vendor_Panelist_ID = b.Panelist_ID
 and l.End_Time_Local < = b.Start_Time -- search occurs before session event
 and b.Start_Time<=dateadd(minute,5,l.End_Time_Local) -- start of next event is <= 5 mins of the end time of search
 and b.Record_ID <> l.Record_ID -- search isn't the record itself
group by s.last_record_id, s.panelist_key,p.Vendor_Panelist_ID,s.search_term, s.device_type, s.property_name, s.property_group,
s.search_term_key, s.date_id, s.max_timestamp, s.search_day, s.search_id, s.semantic_keyword, s.search_semantic_group, brand_flag , retailer_flag
;
--(XX rows affected)

-- select top 100 * from #following_record_id;
---Pulls associated record id (need group bys for multiple records at same time)
begin try drop table #following_record_id end try begin catch end catch;
select search_last_record_id, a.panelist_key,b.Panelist_ID, a.search_term, a.device_type, a.property_name, a.property_group,
a.search_term_key, a.date_id, a.search_max_timestamp, a.search_day, a.search_id, a.semantic_keyword, a.search_semantic_group, 
search_brand_flag,search_retailer_flag,
start_time_next_touchpoint,
min(b.record_id) as next_touchpoint_record
into #following_record_id
from #following_start_time a
left join rps.v_Report_Sequence_Events_P2P b
	on a.Panelist_ID = b.Panelist_ID 
	and a.start_time_next_touchpoint = b.Start_Time
group by search_last_record_id, a.panelist_key,b.Panelist_ID, a.search_term, a.device_type, a.property_name, a.property_group,
a.search_term_key, a.date_id, a.search_max_timestamp, a.search_day, a.search_id, a.semantic_keyword, a.search_semantic_group, search_brand_flag,search_retailer_flag,
start_time_next_touchpoint
;
--(XX rows affected)

-- select top 100 * from #touchpoint_following;
-- select search_last_record_id, count(*) as count from  #touchpoint_following group by search_last_record_id order by count desc
-- select * from #touchpoint_following where search_last_record_id = 9758159;
-- select * from rps.v_Report_Sequence_Events_P2P where Record_id = 9758159; 
--CHECK WHEN THIS IS RUN, BUT SEEMS LIKE DUPES ARE JUST DUE TO VERTICAL ISSUES WITH GSI
begin try drop table #touchpoint_following end try begin catch end catch;
select a.*, b.Digital_Type as next_TP_digital_type, b.Notes as next_TP_notes, b.Value as next_TP_value, b.Search_Term as next_TP_search_term,
b.Ecosystem_Cat as next_TP_eco_cat, b.Ecosytem_Subcategory as next_TP_eco_subcat, 
b.content_category_1 as next_TP_content_1,-- b.content_category_2 as next_TP_content_2, b.content_category_3 as next_TP_content_3
c.common_supercategory as next_TP_common_supercategory, c.common_category as next_TP_common_category,
--case when b.Ecosystem_Cat like '%brand%' OR common_category = 'Brand' then 'Brand' else 'Not Brand' end as next_TP_brand_flag, -- check if this/retailer below pulls everything as needed depending on ecosystem
--case when b.Ecosystem_Cat like '%retailer%' or common_category in ('Category Retailer','General Retailer','Marketplace','Pharmacy') then 'Retailer' else 'Not Retailer' end as next_TP_retailer_flag,
case when next_touchpoint_record is null then 'No records' else 'Record' end as next_TP_5_mins, 
case when next_touchpoint_record is null then 'No records' 
when next_touchpoint_record is not null and Ecosystem_Cat is null then 'Non-ecosystem'
when next_touchpoint_record is not null and Ecosystem_Cat is not null then 'Ecosystem'
end as next_TP_ecosystem_flag
into #touchpoint_following
from #following_record_id a
left join rps.v_Report_Sequence_Events_P2P b
on a.next_touchpoint_record = b.Record_ID
left join core.ref.Taxonomy_Common_Properties c 
on b.Notes = c.property_name
and ( (b.Digital_Type = 'App' and c.digital_type_id = 100)
OR (b.Digital_Type = 'Web' and c.digital_type_id = 235)
OR (b.Digital_Type = 'Shopping' and c.digital_type_id = 100)
)
group by search_last_record_id, a.panelist_key,a.Panelist_ID, a.search_term, a.device_type, a.property_name, a.property_group,
a.search_term_key, a.date_id, a.search_max_timestamp, a.search_day, a.search_id, a.semantic_keyword, a.search_semantic_group,search_brand_flag, 
search_retailer_flag,
start_time_next_touchpoint,
next_touchpoint_record,
b.Digital_Type, b.Notes, b.Value, b.Search_Term,
b.Ecosystem_Cat, b.Ecosytem_Subcategory, 
b.content_category_1,-- b.content_category_2, b.content_category_3
c.common_supercategory, c.common_category
;
--(XX rows affected)


begin try drop table rpt.Touchpoint_after_Search end try begin catch end catch;
select search_last_record_id, Panelist_Key,device_type,date_id,property_group as search_property,search_max_timestamp, search_term, search_brand_flag, search_retailer_flag,
ISNULL(next_touchpoint_record,'') as record_id_next_TP,ISNULL(start_time_next_touchpoint,'') as start_time_next_TP,
 ISNULL(next_TP_digital_type,'') as next_TP_digital_type,
ISNULL(next_TP_notes,'') as next_TP_notes, ISNULL(next_TP_value,'') as next_TP_value, ISNULL(next_TP_search_term,'') as next_TP_search_term,
isnull(next_TP_eco_cat,'') as next_TP_eco_cat, ISNULL(next_TP_eco_subcat,'') as next_TP_eco_subcat, 
ISNULL(next_TP_content_1,'') as next_TP_content_1, --ISNULL(next_TP_content_2,'') as next_TP_content_2, ISNULL(next_TP_content_3,'') as next_TP_content_3,
ISNULL(next_TP_common_supercategory,'') as next_TP_common_supercategory, ISNULL(next_TP_common_category,'') as next_TP_common_category,
--next_TP_brand_flag, next_TP_retailer_flag,
next_TP_5_mins, next_TP_ecosystem_flag
into rpt.Touchpoint_after_Search
from #touchpoint_following
;

select * from rpt.Touchpoint_after_Search 
order by Panelist_Key, search_max_timestamp;