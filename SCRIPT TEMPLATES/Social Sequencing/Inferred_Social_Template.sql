---------------------------------------------------------------------------------------------------
-- Inferred Social
---------------------------------------------------------------------------------------------------
-- NAME
-- DATE
---------------------------------------------------------------------------------------------------

USE [PROJECTNAME];
/**
What's this script for: 
Leverage social visit and brand/retailer visits after that within 5 mins timeframe to infer social ad exposure.
Mostly infer social app events but also double check web events that were not picked up by the semantic match (not in the ecosystem). 
**/

-- ================================================================================================
-- 1) IDENTIFY ECOSYSTEM SESSIONS
-- ================================================================================================

-- IDENTIFY ALL SESSIONS THAT HAVE ECOSYSTEM ENTRY
----------------------------------------------------------------------
-- select top 100 * from [rps].[v_Report_Sequence_Events_P2P] order by session_id;
-- select distinct source_name from ref.taxonomy_ecosystem;
-- select distinct cat_id, category, subcat_id, subcategory, vertical_id, vertical from [Ref].[Taxonomy_Ecosystem] order by 2,4;
begin try drop table #ecosystem_sessions_v1 end try begin catch end catch;

select b.panelist_key, a.* 
into #ecosystem_sessions_v1
from [rps].[v_Report_Sequence_Events_P2P] a
inner join ref.panelist b
on a.panelist_id = b.vendor_panelist_id
where Ecosystem_Cat in ('Retailer','Brand') or ecosytem_subcategory in ('Ratings and Reviews');


begin try drop table #ecosystem_sessions_v2 end try begin catch end catch;

select cast(0 as bigint) as eco_session_1st_record_id,
panelist_key, panelist_id, digital_type, platform,
notes as session_property_name, cast('' as Varchar(max)) as rep_url,
session_id, date_id as session_date_id, min(start_time) as session_start_time,  
ecosystem_cat, ecosytem_subcategory as ecosystem_subcat
into #ecosystem_sessions_v2
from #ecosystem_sessions_v1 
group by panelist_key, panelist_id, digital_type, platform, notes, session_id, date_id, ecosystem_cat, ecosytem_subcategory
;

-- select top 1000 * from #ecosystem_sessions_v2 order by session_start_time;

update #ecosystem_sessions_v2
set eco_session_1st_record_id=b.record_id
from #ecosystem_sessions_v2 a 
inner join #ecosystem_sessions_v1 b
	on a.session_id=b.session_id
	and a.panelist_key=b.panelist_key
	and a.session_date_id=b.Date_ID
	and a.session_property_name=b.Notes
	and a.digital_type = b.digital_type
	and a.session_start_time=b.Start_time;

update #ecosystem_sessions_v2
set rep_url=b.value
from #ecosystem_sessions_v2 a 
inner join #ecosystem_sessions_v1 b
	on a.session_id=b.session_id
	and a.panelist_key=b.panelist_key
	and a.session_date_id=b.Date_ID
	and a.session_property_name=b.Notes
	and a.digital_type = b.digital_type
	and a.session_start_time=b.Start_time;


begin try drop table #session_end_eco end try begin catch end catch;
select session_id, max(End_Time_Local) as session_end_time
into #session_end_eco
from #ecosystem_sessions_v1
group by Session_ID;


---Gets end time for ultimate session chosen
-- select top 1000 * from #ecosystem_sessions order by panelist_key, session_start_time;
-- select top 1000 * from #ecosystem_sessions where session_id = '26993' 
-- select count(distinct session_id) from #ecosystem_sessions;
begin try drop table #ecosystem_sessions end try begin catch end catch;
select eco_session_1st_record_id,
panelist_key, panelist_id, digital_type, platform,
session_property_name, rep_url,
a.session_id, session_date_id,session_start_time, b.session_end_time,
ecosystem_cat, ecosystem_subcat
into #ecosystem_sessions
from #ecosystem_sessions_v2 a 
join #session_end_eco b on a.Session_ID = b.Session_ID;


-- ================================================================================================
-- 2) IDENTIFY ALL RELEVANT SOCIAL EVENTS
-- ================================================================================================
-- select distinct cat_id, category, subcat_id, subcategory, vertical_id, vertical from [Ref].[Taxonomy_Ecosystem] order by 2,4;
-- select distinct notes from [rps].[v_Report_Sequence_Events_P2P] where Digital_Type = 'app' and notes like '%pinterest%';
-- select distinct notes from [rps].[v_Report_Sequence_Events_P2P] where Digital_Type = 'web' and notes like '%facebook%';
-- select top 1000 * from [rps].[v_Report_Sequence_Events_P2P] where notes = 'facebook.co';

begin try drop table #social_events end try begin catch end catch;

select b.panelist_key, a.*,
case when Ecosystem_Cat is null then 0 else 1 end as preexist_social_flag 
into #social_events
from [rps].[v_Report_Sequence_Events_P2P] a
inner join ref.panelist b
on a.panelist_id = b.vendor_panelist_id
where 
ecosytem_subcategory in ('Social: Facebook (Web)','Social: Pinterest (Web)','Social: Twitter (Web)')
or (ecosytem_subcategory = 'Social: All Other (Web)' and notes like '%instagram%')
-- for inferred QA, only need social events that are not picked up by the ecosystem
or (Digital_Type = 'web' and notes in ('instagram.com','facebook.com','twitter.com','pinterest.com')) 
or (Digital_Type = 'app' and notes in ('instagram','facebook','Facebook Lite','twitter','pinterest'));

-- select top 1000 * from #social_events order by start_time;
-- select top 1000 * from #social_events where session_id = 26703 order by start_time


-- ================================================================================================
-- 3) IDENTIFY SOCIAL TOUCHPOINTS THAT LEAD TO ECOSYSTEM SESSIONS 
-- ================================================================================================
-- FIND SOCIAL EVENTS 5 MINS PRIOR TO ECOSYSTEM EVENTS
----------------------------------------------------------------------
-- select top 100 * from #social_events;
-- select top 100 * from #ecosystem_sessions;
-- select top 1000 * from #ecosystem_sessions_with_candidate_social_v1 order by panelist_key, social_start_time_rep;
begin try drop table #ecosystem_sessions_with_candidate_social_v1 end try begin catch end catch;

select a.panelist_key, a.session_property_name as eco_session_property_name, a.rep_url as eco_rep_url,
	a.session_id as eco_session_id, a.session_date_id as eco_session_date_id, 
	a.session_start_time as eco_session_start_time, a.session_end_time as eco_session_end_time,
	a.eco_session_1st_record_id, a.digital_type as eco_digital_type, 
	a.ecosystem_cat as target_cat, a.ecosystem_subcat as target_subcat,
	max(s.start_time) as social_start_time_rep -- in case when multiple social happen before event, we want last social
into #ecosystem_sessions_with_candidate_social_v1
from #ecosystem_sessions a
left outer join #social_events s
	on a.panelist_key=s.panelist_key
	and a.session_date_id=s.date_id
	and a.session_start_time > s.start_time						-- ecosystem occurs after social session event
	and a.session_start_time<=dateadd(minute,5,s.end_time_local)   -- social sessions withn 5 minutes prior to ecosystem touchpoints.
	and a.eco_session_1st_record_id <> s.record_id              -- not the record itself
group by a.panelist_key, a.session_property_name, a.rep_url, a.session_id , a.session_date_id , 
	a.session_start_time, a.session_end_time, a.eco_session_1st_record_id, a.digital_type,a.ecosystem_cat,a.ecosystem_subcat;


begin try drop table #ecosystem_sessions_with_candidate_social_v2 end try begin catch end catch;

select a.panelist_key, 
eco_session_property_name, eco_rep_url,
eco_session_id, eco_session_date_id, 
eco_session_start_time, eco_session_end_time,
a.eco_session_1st_record_id, eco_digital_type,
target_cat, target_subcat,
max(b.record_id) as record_id_social_event_rep, -- take max record_id in case multiple social records start at the same time
social_start_time_rep
into #ecosystem_sessions_with_candidate_social_v2
from #ecosystem_sessions_with_candidate_social_v1 a
left join #social_events b
on a.panelist_key = b.Panelist_Key
and a.social_start_time_rep = b.Start_Time 
group by a.panelist_key, eco_session_property_name, eco_rep_url,eco_session_id, eco_session_date_id, 
eco_session_start_time, eco_session_end_time,a.eco_session_1st_record_id, eco_digital_type,target_cat, target_subcat,social_start_time_rep;

-- join back social to get event details 
-- select top 100 * from rps.sequence_event;
begin try drop table #ecosystem_sessions_with_candidate_social end try begin catch end catch;

select a.panelist_key, 
eco_session_property_name, eco_rep_url,
eco_session_id, eco_session_date_id, 
eco_session_start_time, eco_session_end_time,
eco_session_1st_record_id, eco_digital_type,target_cat, target_subcat,
record_id_social_event_rep, 
b.digital_type as social_digital_type, c.digital_type_id as social_digital_type_id,
b.Platform as social_platform, c.platform_id as social_platform_id,
b.notes as social_property_name, b.Value as social_rep_url,
social_start_time_rep, b.End_Time_Local as social_end_time,
b.Session_ID as social_session_id, 
case when b.preexist_social_flag !=1 then 'Social: Inferred' else b.Ecosystem_Cat end as social_Ecosystem_Cat,
case when b.preexist_social_flag !=1 then concat('Social: Inferred ',b.notes) else b.Ecosytem_Subcategory end as social_Ecosystem_Subcat,
b.preexist_social_flag
into #ecosystem_sessions_with_candidate_social
from #ecosystem_sessions_with_candidate_social_v2 a
left join #social_events b
on a.panelist_key = b.Panelist_Key
and a.record_id_social_event_rep = b.record_id
left join rps.sequence_event c -- get digital_type_id and platform_id for QA
on a.panelist_key = c.panelist_key
and a.record_id_social_event_rep = c.record_id
where record_id_social_event_rep is not null; -- only keep events with a social 

-- select * from #ecosystem_sessions_with_candidate_social order by panelist_key, eco_session_start_time;
-- select * from #ecosystem_sessions_with_candidate_social where social_session_id = 26703

-- dedup when same social event matched with multiple eco events, we want the first eco event
begin try drop table #ecosystem_sessions_with_candidate_social_dedup_v1 end try begin catch end catch;

select 
panelist_key, 
min(eco_session_start_time) as first_eco_session_start_time,
record_id_social_event_rep, 
social_digital_type, social_digital_type_id,
social_platform, social_platform_id,
social_property_name, social_rep_url,
social_start_time_rep, social_end_time,
social_session_id, social_Ecosystem_Cat, social_Ecosystem_Subcat,
preexist_social_flag
into #ecosystem_sessions_with_candidate_social_dedup_v1
from #ecosystem_sessions_with_candidate_social 
group by panelist_key, record_id_social_event_rep, social_digital_type, social_digital_type_id,
social_platform, social_platform_id,social_property_name, social_rep_url,
social_start_time_rep, social_end_time,social_session_id, social_Ecosystem_Cat, social_Ecosystem_Subcat,preexist_social_flag;

-- select * from #ecosystem_sessions_with_candidate_social_dedup_v1 order by panelist_key, first_eco_session_start_time;

-- join original table to get the earliest eco event details 
begin try drop table #ecosystem_sessions_with_candidate_social_dedup_v2 end try begin catch end catch;

select 
a.panelist_key, min(eco_session_1st_record_id) as eco_session_1st_record_id,-- in case multiple records at same time, pick min record 
a.first_eco_session_start_time as eco_session_start_time,
a.record_id_social_event_rep, 
a.social_digital_type, a.social_digital_type_id,
a.social_platform, a.social_platform_id,
a.social_property_name, a.social_rep_url,
a.social_start_time_rep, a.social_end_time,
a.social_session_id, a.social_Ecosystem_Cat, a.social_Ecosystem_Subcat,
a.preexist_social_flag
into #ecosystem_sessions_with_candidate_social_dedup_v2
from #ecosystem_sessions_with_candidate_social_dedup_v1 a
join #ecosystem_sessions_with_candidate_social b
on a.Panelist_Key = b.Panelist_Key
and a.first_eco_session_start_time = b.eco_session_start_time
group by a.panelist_key, a.first_eco_session_start_time,a.record_id_social_event_rep, a.social_digital_type, a.social_digital_type_id,
a.social_platform, a.social_platform_id,a.social_property_name, a.social_rep_url,a.social_start_time_rep, a.social_end_time,
a.social_session_id, a.social_Ecosystem_Cat, a.social_Ecosystem_Subcat,a.preexist_social_flag;

-- select * from #ecosystem_sessions_with_candidate_social_dedup_v2 order by panelist_key, eco_session_start_time;

-- select top 1000 * from #ecosystem_sessions_with_candidate_social;
begin try drop table #ecosystem_sessions_with_candidate_social_dedup end try begin catch end catch;

select 
a.panelist_key, 
b.eco_session_property_name, b.eco_rep_url, b.eco_session_id,
b.eco_session_date_id, b.eco_session_start_time, b.eco_session_end_time,
a.eco_session_1st_record_id, b.eco_digital_type,
b.target_cat, b.target_subcat,
a.record_id_social_event_rep, 
a.social_digital_type, a.social_digital_type_id,
a.social_platform, a.social_platform_id,
a.social_property_name, a.social_rep_url,
a.social_start_time_rep, a.social_end_time,
a.social_session_id, a.social_Ecosystem_Cat, a.social_Ecosystem_Subcat,
a.preexist_social_flag
into #ecosystem_sessions_with_candidate_social_dedup
from #ecosystem_sessions_with_candidate_social_dedup_v2 a
join #ecosystem_sessions_with_candidate_social b
on a.Panelist_Key = b.Panelist_Key
and a.eco_session_1st_record_id = b.eco_session_1st_record_id;

-- select * from #ecosystem_sessions_with_candidate_social_dedup order by panelist_key, eco_session_start_time;

-- QA for dups
-- select eco_session_1st_record_id, count(*) as count from #ecosystem_sessions_with_candidate_social_dedup group by eco_session_1st_record_id order by count(*) desc;
-- select * from #ecosystem_sessions_with_candidate_social_dedup where eco_session_1st_record_id = '17276615';

-------Takes the web social url and runs the search process on it (but allows ambiguous words)
begin try drop table #social_url_gra end try begin catch end catch;
select record_id_social_event_rep, social_rep_url, core.dbo.alphanum(social_rep_url) as social_url_gra
into #social_url_gra
from #ecosystem_sessions_with_candidate_social_dedup
where social_digital_type = 'web' and preexist_social_flag = 0;

-- select top 1000 * from #social_url_gra;

-- select top 1000 * from ref.pattern_table_apparel;
begin try drop table #social_url_gra_best_pattern end try begin catch end catch;
select record_id_social_event_rep,social_rep_url ,min(pattern_id) as best_pattern_id
into #social_url_gra_best_pattern
from #social_url_gra a
inner join ref.pattern_table b
	on a.social_url_gra like b.pattern
group by record_id_social_event_rep,social_rep_url
;
--(23 row(s) affected)
-- select top 1000 * from #social_url_gra_best_pattern;

begin try drop table #social_scenarios_with_semantic end try begin catch end catch;
select a.*, isnull(c.semantic_lemma,'') as semantic_lemma , --c.semantic_lemma_english,
isnull(c.semantic_priority_group,'') as semantic_priority_group 
into #social_scenarios_with_semantic
from #ecosystem_sessions_with_candidate_social_dedup a
left join #social_url_gra_best_pattern b on a.record_id_social_event_rep = b.record_id_social_event_rep
left join ref.pattern_table c on b.best_pattern_id = c.pattern_id 
;
--(4487 row(s) affected)
-- select * from #social_scenarios_with_semantic where semantic_lemma != '';
-- select top 1000 * from [rps].[v_Report_Sequence_Events_P2P] where record_id = '19984299'
-- select * from ref.pattern_table_apparel where semantic_lemma = 'Anthropologie';

-- add flag to for FP social web events exclusion 
begin try drop table #social_scenarios_with_semantic_final end try begin catch end catch;

select *, 
case when (social_digital_type_id = '235' and (semantic_lemma = '' or semantic_priority_group = 'REMOVE')) then 0 else 1 end as include_flag
into #social_scenarios_with_semantic_final
from #social_scenarios_with_semantic;
--(4487 row(s) affected)

-- select * from #social_scenarios_with_semantic_final where semantic_priority_group = 'REMOVE';


--###################################################
-- Create Permanent Table for Inferred Social QA
--###################################################

begin try drop table rpt.inferred_social_events_apparel_test end try begin catch end catch;

select panelist_key, 
record_id_social_event_rep as record_id_inferred_social_event, 
social_digital_type_id, social_platform_id,
social_property_name, social_rep_url,
social_start_time_rep, social_end_time,
social_session_id, social_Ecosystem_Cat as inferred_social_ecosystem_cat, 
social_Ecosystem_Subcat as inferred_social_ecosystem_subcat,
semantic_lemma as social_semantic_lemma, semantic_priority_group as social_semantic_priority_group,
eco_session_1st_record_id, eco_session_id, eco_digital_type,
eco_session_date_id, eco_session_start_time,
eco_session_property_name, eco_rep_url, 
target_cat, target_subcat
into rpt.inferred_social_events_apparel_test
from #social_scenarios_with_semantic_final 
where preexist_social_flag != 1 and include_flag = 1 order by panelist_key, social_start_time_rep;
--(2998 row(s) affected)

-------------------------
-- OUTPUT TO EXCEL
-------------------------
select * from rpt.inferred_social_events_apparel_test order by panelist_key, social_start_time_rep;

-- END OF SCRIPT --



