---------------------------------------------------------------------------------------------------
-- Social Sequencing 
---------------------------------------------------------------------------------------------------
-- NAME
-- DATE
---------------------------------------------------------------------------------------------------

USE [PROJECTNAME];
/**
NOTE: RUN THIS SCRIPT AFTER INFERRED SOCIAL IS APPLIED TO THE ECOSYSTEM

What's this script for: 
Leverage social visit and brand/retailer visits after that within 5 mins timeframe to infer ad exposure and identify "shoppy" type of activity.

Steps:
1. Identify all social app touchpoints or social web touchpoints within the ecosystem
2. Look for the 1st brand/retailer/rating and reviews touchpoints within 5 mins after the social event.
3. Identify all search events within 5 mins after the social event. 

Touch points: 
A. Social web events (in ecosystem) & all social apps listed below: 
-- Instagram, Facebook, Twitter, Pinterest 

B. Search events 
-- roll up same searches in one session

C. Brand/retailer/(media review) in ecosystem

Analysis:
Among all social visits, 
1. How many of the sessions never left social (within 5 mins)
2. How many of the sessions have brand/retailer/rating and reviews session after social (within 5 mins)
3. How many of the sessions have search (within 5 mins after social, either in between social and ecosystem touchpoints, or after ecosystem touchpoints)  

**/

-- ================================================================================================
-- 1) IDENTIFY ECOSYSTEM/SOCIAL/SEARCH EVENTS
-- ================================================================================================

-- IDENTIFY ALL ECOSYSTEM EVENTS OF INTEREST
----------------------------------------------------------------------
-- select top 100 * from [rps].[v_Report_Sequence_Events_P2P] order by session_id;
-- select distinct cat_id, category, subcat_id, subcategory, vertical_id, vertical from [Ref].[Taxonomy_Ecosystem] where vertical_id = 500 order by 2,4;
begin try drop table #ecosystem_sessions end try begin catch end catch;

select b.panelist_key, a.* 
into #ecosystem_sessions
from [rps].[v_Report_Sequence_Events_P2P] a
inner join ref.panelist b
on a.panelist_id = b.vendor_panelist_id
where vertical_id = '500' and -- remove if no vertical_id
(Ecosystem_Cat in ('Retailer','Brand') or ecosytem_subcategory in ('Ratings and Reviews'));
--(142885 row(s) affected)
-- select top 100 * from #ecosystem_sessions order by session_id, start_time;


-- IDENTIFY SOCIAL EVENT SESSIONS
----------------------------------------------------------------------
-- select distinct cat_id, category, subcat_id, subcategory, vertical_id, vertical from [Ref].[Taxonomy_Ecosystem] where vertical_id = 500 order by 2,4;
-- select top 1000 * from [rps].[v_Report_Sequence_Events_P2P] where ecosytem_subcategory = 'Social: All Other (Web)' and notes like '%instagram%';
-- select distinct notes from [rps].[v_Report_Sequence_Events_P2P] where Digital_Type = 'app' and notes like '%pinterest%';
begin try drop table #social_events_v1 end try begin catch end catch;

select b.panelist_key, a.* 
into #social_events_v1
from [rps].[v_Report_Sequence_Events_P2P] a
inner join ref.panelist b
on a.panelist_id = b.vendor_panelist_id
where (vertical_id = '500' and -- remove if no vertical_id
(ecosytem_subcategory in ('Social: Facebook (Web)','Social: Pinterest (Web)','Social: Twitter (Web)')
or (ecosytem_subcategory = 'Social: All Other (Web)' and notes like '%instagram%')))
or (Digital_Type = 'app' and notes in ('instagram','facebook','Facebook Lite','twitter','pinterest')); -- for future replace with the inferred events 
--(543443 row(s) affected)
-- select top 1000 * from #social_events_v1 order by start_time;

-- Roll up social events to session level
begin try drop table #social_events_v2 end try begin catch end catch;

select cast(0 as bigint) as social_session_last_record_id,
panelist_key, panelist_id, digital_type, platform,
notes as session_property_name, cast('' as Varchar(max)) as rep_url,
session_id, date_id as session_date_id, min(start_time) as session_start_time, 
max(start_time) as last_record_start_time, -- want to show the last social record 
ecosystem_cat, ecosytem_subcategory as ecosystem_subcat
into #social_events_v2
from #social_events_v1 
group by panelist_key, panelist_id, digital_type, platform, notes, session_id, date_id, ecosystem_cat, ecosytem_subcategory
;
--(122123 row(s) affected)
-- select top 1000 * from #social_events_v2 order by session_start_time;

update #social_events_v2
set social_session_last_record_id=b.record_id
from #social_events_v2 a 
inner join #social_events_v1 b
	on a.session_id=b.session_id
	and a.panelist_key=b.panelist_key
	and a.session_date_id=b.Date_ID
	and a.session_property_name=b.Notes
	and a.digital_type = b.digital_type
	and a.last_record_start_time=b.Start_time;
-- (122060 row(s) affected)

update #social_events_v2
set rep_url=b.value
from #social_events_v2 a 
inner join #social_events_v1 b
	on a.session_id=b.session_id
	and a.panelist_key=b.panelist_key
	and a.session_date_id=b.Date_ID
	and a.session_property_name=b.Notes
	and a.digital_type = b.digital_type
	and a.last_record_start_time=b.Start_time;
-- (122060 row(s) affected)

begin try drop table #session_end_social end try begin catch end catch;
select session_id, max(End_Time_Local) as session_end_time
into #session_end_social
from #social_events_v1
group by Session_ID;
--(122060 row(s) affected)

---Gets end time for ultimate session chosen
begin try drop table #social_events end try begin catch end catch;
select social_session_last_record_id,
panelist_key, panelist_id, digital_type, platform,
session_property_name, rep_url,
a.session_id, session_date_id,session_start_time, b.session_end_time,
ecosystem_cat, ecosystem_subcat
into #social_events
from #social_events_v2 a 
join #session_end_social b on a.Session_ID = b.Session_ID;
--(122060 row(s) affected)
-- select top 1000 * from #social_events order by session_start_time;


-- IDENTIFY ALL SEARCH EVENT SESSIONS 
----------------------------------------------------------------------
-- select top 1000 * from rpt.Search_Delivery_Apparel_Accessories;
-- select top 1000 * from rps.v_Report_Sequence_Events_P2P where Ecosystem_Cat = 'search engine';
begin try drop table #search_events_v1 end try begin catch end catch;

select b.panelist_key, a.* 
into #search_events_v1
from [rps].[v_Report_Sequence_Events_P2P] a
inner join ref.panelist b
on a.panelist_id = b.vendor_panelist_id
where vertical_id = '500' -- remove if no vertical_id
and Ecosystem_Cat = 'search engine';
--(11810 row(s) affected)

-- select top 100 * from #search_events_v1;

-- roll up same searches in the same session
begin try drop table #search_events_v2 end try begin catch end catch;

select cast(0 as bigint) as search_session_1st_record_id,
panelist_key, panelist_id, digital_type, platform,
notes as session_property_name, cast('' as Varchar(max)) as rep_url, search_term,
session_id, date_id as session_date_id, min(start_time) as session_start_time, 
ecosystem_cat, ecosytem_subcategory as ecosystem_subcat
into #search_events_v2
from #search_events_v1 
group by panelist_key, panelist_id, digital_type, platform, notes, search_term, session_id, date_id, ecosystem_cat, ecosytem_subcategory
;
--(7420 row(s) affected)

-- select top 100 * from #ecosystem_sessions_v2;

update #search_events_v2
set search_session_1st_record_id=b.record_id
from #search_events_v2 a 
inner join #search_events_v1 b
	on a.session_id=b.session_id
	and a.panelist_key=b.panelist_key
	and a.session_date_id=b.Date_ID
	and a.session_property_name=b.Notes
	and a.digital_type = b.digital_type
	and a.session_start_time=b.Start_time;
--(7420 row(s) affected)

update #search_events_v2
set rep_url=b.value
from #search_events_v2 a 
inner join #search_events_v1 b
	on a.session_id=b.session_id
	and a.panelist_key=b.panelist_key
	and a.session_date_id=b.Date_ID
	and a.session_property_name=b.Notes
	and a.digital_type = b.digital_type
	and a.session_start_time=b.Start_time;
-- (7420 row(s) affected)

begin try drop table #session_end_search end try begin catch end catch;
select session_id, max(End_Time_Local) as session_end_time
into #session_end_search
from #search_events_v1
group by Session_ID;
--(4309 row(s) affected)


---Gets end time for ultimate session chosen
-- select top 1000 * from #search_events order by panelist_key, session_start_time;
begin try drop table #search_events end try begin catch end catch;
select search_session_1st_record_id,
panelist_key, panelist_id, digital_type, platform,
session_property_name, rep_url, search_term,
a.session_id, session_date_id,session_start_time, b.session_end_time,
ecosystem_cat, ecosystem_subcat
into #search_events
from #search_events_v2 a 
join #session_end_search b on a.Session_ID = b.Session_ID;
--(7420 row(s) affected)


-- ================================================================================================
-- 2) FIND SOCIAL EVENTS 5 MINS PRIOR TO ECOSYSTEM EVENTS
-- ================================================================================================
-- select top 100 * from #social_events;
-- select top 100 * from #ecosystem_sessions;
-- select top 1000 * from #ecosystem_sessions_after_social_v1 order by panelist_key, social_session_start_time;
begin try drop table #ecosystem_sessions_after_social_v1 end try begin catch end catch;

select s.panelist_key, 
	s.social_session_last_record_id as social_last_record_id, 
	s.session_id as social_session_id,
	s.session_date_id as social_session_date,
	s.session_start_time as social_session_start_time,
	s.session_end_time as social_session_end_time,
	s.digital_type as social_digital_type,
	s.session_property_name as social_rep_property, 
	s.rep_url as social_rep_url,
	isnull(s.Ecosystem_Cat, 'Social') as ecosystem_cat_social, 
	isnull(s.ecosystem_subcat,'Social App') as ecosystem_subcat_social,
	--min(a.record_id) as eco_1st_record_id, 
	min(a.start_time) as eco_event_start_time -- in case when multiple eco events happen after same social event, we want the first one after social
into #ecosystem_sessions_after_social_v1
from #social_events s
left outer join #ecosystem_sessions a
	on a.panelist_key=s.panelist_key
	and a.date_id=s.session_date_id
	and s.session_start_time < a.start_time						-- ecosystem occurs after social session event
	and a.start_time<=dateadd(minute,5,s.session_end_time)   -- social sessions withn 5 minutes prior to ecosystem touchpoints.
	and a.record_id <> s.social_session_last_record_id              -- not the record itself
group by s.panelist_key, s.social_session_last_record_id,s.session_id,s.session_date_id,s.session_start_time,s.session_end_time,
s.digital_type,s.session_property_name,s.rep_url,s.Ecosystem_Cat,s.ecosystem_subcat;
--(122060 row(s) affected)
-- select top 1000 * from #ecosystem_sessions_after_social_v1 where social_last_record_id is not null order by panelist_key, eco_session_start_time;

begin try drop table #ecosystem_sessions_after_social_v2 end try begin catch end catch;

select a.panelist_key, a.social_last_record_id, a.social_session_id, a.social_session_date,
a.social_session_start_time, a.social_session_end_time,
a.social_digital_type, a.social_rep_property, a.social_rep_url,
a.ecosystem_cat_social, a.ecosystem_subcat_social,a.eco_event_start_time,
min(b.record_id) as eco_1st_record_id -- take min record_id in case multiple eco records start at the same time
into #ecosystem_sessions_after_social_v2
from #ecosystem_sessions_after_social_v1 a
left join #ecosystem_sessions b
on a.panelist_key = b.Panelist_Key
and a.eco_event_start_time = b.Start_Time
group by a.panelist_key, a.social_last_record_id, a.social_session_id, a.social_session_date,a.social_session_start_time, a.social_session_end_time,
a.social_digital_type, a.social_rep_property, a.social_rep_url,a.ecosystem_cat_social, a.ecosystem_subcat_social,a.eco_event_start_time;
--where a.eco_session_1st_record_id is not null;
--(122060 row(s) affected)
-- select top 1000 * from #ecosystem_sessions_after_social_v2 where social_last_record_id is not null order by panelist_key, eco_session_start_time;


begin try drop table #ecosystem_sessions_after_social end try begin catch end catch;

select a.panelist_key, a.social_last_record_id, a.social_session_id, a.social_session_date,
a.social_session_start_time, a.social_session_end_time,
a.social_digital_type, a.social_rep_property, a.social_rep_url,
a.ecosystem_cat_social, a.ecosystem_subcat_social,
a.eco_1st_record_id, 
b.session_id as eco_session_id, b.date_id as eco_event_date, 
a.eco_event_start_time, b.end_time_local as eco_event_end_time,
b.digital_type as eco_digital_type, b.notes as eco_event_property_name, 
b.value as eco_url, b.ecosystem_cat as ecosystem_cat, b.ecosytem_subcategory as ecosystem_subcat,
case when eco_1st_record_id is not null then 1 else 0 end as social_trigger_eco_flag
into #ecosystem_sessions_after_social
from #ecosystem_sessions_after_social_v2 a
left join #ecosystem_sessions b
on a.panelist_key = b.Panelist_Key
and a.eco_1st_record_id = b.record_id;
--(122060 row(s) affected)

-- select top 1000 * from #ecosystem_sessions_after_social order by panelist_key, social_session_start_time;

-- ================================================================================================
-- 3) FIND SEARCH EVENTS 5 MINS AFTER SOCIAL
-- ================================================================================================

-- PREP TABLE: CREATE ROW NUMBER AS EVENT NUMBER
begin try drop table #partitioned_ecosystem_sessions_after_social_v1 end try begin catch end catch;
select *, row_number() over (partition by panelist_key order by panelist_key, social_session_date, social_session_start_time asc) as event_num
into #partitioned_ecosystem_sessions_after_social_v1
from #ecosystem_sessions_after_social;
--(122060 row(s) affected)

-- Add event_id
-- select * from #partitioned_ecosystem_sessions_after_social order by panelist_key, social_session_start_time;
begin try drop table #partitioned_ecosystem_sessions_after_social end try begin catch end catch;

select *, concat(panelist_key,'_',event_num) as Event_ID
into #partitioned_ecosystem_sessions_after_social
from #partitioned_ecosystem_sessions_after_social_v1
--(122060 row(s) affected)

-- select top 100 * from #search_events;
-- select top 1000 * from #ecosystem_sessions_after_social_v1 order by panelist_key, session_start_time;
-- select top 1000 * from #search_sessions_v1 order by panelist_key, start_time;
begin try drop table #search_sessions_v1 end try begin catch end catch;

select 'search event' as touchpoint_type, a.event_num,
s.search_session_1st_record_id as record_id, digital_type, s.Panelist_Key, s.session_property_name as property_name, rep_url as url,
s.search_term, s.session_start_time as start_time, s.session_end_time as end_time, s.session_Date_ID as date_id, s.Session_ID,
s.ecosystem_cat, s.ecosystem_subcat, a.Event_ID
into #search_sessions_v1
from #search_events s
inner join #partitioned_ecosystem_sessions_after_social a
	on a.panelist_key=s.panelist_key
	and a.social_session_date=s.session_Date_ID
--	and s.session_start_time < a.eco_session_start_time						-- searches occurs between social and eco
--	and s.session_start_time > a.social_start_time                           -- note: should we use start time of end time?
	and a.social_session_start_time < s.session_start_time						-- search occurs after social session event
	and s.session_start_time<=dateadd(minute,5,a.social_session_end_time)   -- search sessions withn 5 minutes after social touchpoints.
	and a.social_last_record_id <> s.search_session_1st_record_id                  -- not the record itself
where a.eco_1st_record_id is not null; -- only find search for sessions with ecosystem visits 
;
--(425 row(s) affected)

-- ================================================================================================
-- 4) COMBINE EVENTS FOR OUTPUT
-- ================================================================================================
-- START TOUCHPOINT: SOCIAL EVENTS
-- select top 100 * from #partitioned_ecosystem_sessions_after_social;
-- select * from #partitioned_ecosystem_sessions_after_social where eco_1st_record_id is not null;
insert into #search_sessions_v1
select 'social event' as touchpoint_type, s.event_num,
s.social_last_record_id as Record_ID, s.social_digital_type as digital_type, s.Panelist_Key, 
s.social_rep_property as property_name, s.social_rep_url as url, '' as search_term, 
s.social_session_start_time as start_time, s.social_session_end_time as End_time, 
s.social_session_date as Date_ID, s.social_session_id as Session_ID,
s.ecosystem_cat_social, s.ecosystem_subcat_social, Event_ID
from #partitioned_ecosystem_sessions_after_social s
where social_trigger_eco_flag = 1; -- only include social sessions that trigger eco events
--(3433 row(s) affected)


-- END TOUCHPOINT: ECOSYSTEM BRAND/RETAIL EVENTS
insert into #search_sessions_v1
select 'ecosystem' as touchpoint_type, 
s.event_num,
s.eco_1st_record_id as Record_ID, 
s.eco_digital_type as digital_type, 
s.Panelist_Key, 
s.eco_event_property_name as property_name, 
s.eco_url as url, 
'' as search_term, 
s.eco_event_start_time as start_time, 
s.eco_event_end_time as End_time, 
s.eco_event_date as Date_ID, 
s.eco_session_id as Session_ID,
s.ecosystem_cat, s.ecosystem_subcat, Event_ID
from #partitioned_ecosystem_sessions_after_social s
where s.social_trigger_eco_flag = 1;
--(3433 row(s) affected)

-- select top 1000 * from #search_sessions_v1 order by panelist_key, start_time;


-- PARTITION BY PERSON TO CREAT BEFORE/AFTER EVENTS
-- OPTIONAL: ADD IN VERTICALS/SEGMENTATIONS
-----------------------------------------------------------------------
-- select top 1000 * from #partitioned_events_ordered order by panelist_key, start_time;
begin try drop table #partitioned_events_ordered end try begin catch end catch;
select a.event_num, row_number() over (partition by panelist_key,event_num order by panelist_key,event_num, date_id, start_time asc) as row_num_within_event,
a.touchpoint_type, a.Record_ID,a.digital_type,a.Panelist_Key,a.property_name, url, a.search_term,
a.start_time,a.End_time,Date_ID,a.Session_ID,
a.ecosystem_cat, a.ecosystem_subcat, Event_ID
into #partitioned_events_ordered 
from #search_sessions_v1 a
--(7291 row(s) affected)

begin try drop table #agg_paths end try begin catch end catch;
select a.Panelist_Key, a.event_num, 
isnull(b.touchpoint_type, '[Start]') as prev_touchpoint_type,
isnull(b.property_name, '[Start]') as prev_property_name,
isnull(b.ecosystem_cat, '[Start]') as prev_eco_cat,
isnull(b.ecosystem_subcat, '[Start]') as prev_eco_subcat,
isnull(b.start_time, dateadd(minute,-1,a.start_time)) as prev_start_time,
isnull(b.End_time, dateadd(minute,-1,a.start_time)) as prev_End_time,
--isnull(b.Session_ID, 0) as prev_Session_ID,
a.touchpoint_type, a.Record_ID,a.digital_type,a.property_name,
a.url,
a.ecosystem_cat, a.ecosystem_subcat,
a.search_term,a.start_time,a.End_time,a.Date_ID,a.Session_ID, a.Event_ID
into #agg_paths
from #partitioned_events_ordered a
left join #partitioned_events_ordered b
on a.event_num = b.event_num
and a.panelist_key = b.panelist_key
and a.row_num_within_event = b.row_num_within_event+1;
-- (7291 row(s) affected)
-- select top 1000 * from #agg_paths order by panelist_key, start_time;


--########################
-- OUTPUT TO EXCEL
--########################
-- select * from #partitioned_ecosystem_sessions_after_social order by panelist_key, social_session_start_time;
select panelist_key, event_num, prev_touchpoint_type, prev_property_name, prev_eco_cat, prev_eco_subcat, prev_start_time, prev_End_time,
touchpoint_type, Digital_Type, property_name, 
url, 
ecosystem_cat, ecosystem_subcat, Search_Term, Start_Time, end_time,
Date_ID, Session_ID, Event_ID, Record_ID
from #agg_paths order by Panelist_Key,event_num, start_time;


-- Numbers for Analysis
--------------------------
-- A. all social sessions
-- # panelists of all social sessions
select count(distinct panelist_key) from #partitioned_ecosystem_sessions_after_social --1011
-- Total event sessions with social events 
select count(distinct event_id) from #partitioned_ecosystem_sessions_after_social --122060
-- Social event session with no ecosystem touchpoint
select count(distinct event_id) from #partitioned_ecosystem_sessions_after_social where social_trigger_eco_flag = 0 --118627

-- B. social sessions triggered ecosystem touchpoints
-- # panelists of social sessions triggered ecosystem touchpoints  
select count(distinct panelist_key) from #agg_paths; --515
-- Social event sessions triggered ecosystem touchpoints within 5 mins
select count(distinct event_id) from #agg_paths --3433

-- B2. for search counts
-- create summary table 
begin try drop table #event_count end try begin catch end catch;
select Event_ID, touchpoint_type, count(*) as count_events 
into #event_count
from #agg_paths group by Event_ID, touchpoint_type;
--(3433 row(s) affected)

-----------------------------------
-- Output to excel 
-- for event summary pivot table
-----------------------------------
select * from #event_count order by Event_ID desc;

-- Social sessions with searches count 
-- Social event sessions triggered ecosystem touchpoints with 2+ searches within 5 mins
select count(distinct Event_ID) from #event_count where touchpoint_type = 'search event' and count_events > 1;
-- 80
-- Social event sessions triggered ecosystem touchpoints with 1 search within 5 mins
select count(distinct Event_ID) from #event_count where touchpoint_type = 'search event' and count_events = 1;
-- 208












