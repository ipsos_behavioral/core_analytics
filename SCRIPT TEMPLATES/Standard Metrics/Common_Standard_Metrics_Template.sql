-- NAME
-- Common Standard Metrics
-- DATE

-- TERMINOLGY WE USE
------------------------------------------------------------------
-- Dimensions: Ways the data will be cut (e.g. device type, digital, population segments, time segments)
-- Classifications: Taxonomy mapping, often detailed, that describes scattered datapoints into a meaningful way. (e.g. domain, property, category)
-- Measurements: Sometimes called metrics. Numeric values only. These may be additive across dimensions (e.g. minutes, visits, searches) or deduplicated (unique visitors, usage days)

-- STEP 1) DECLARE DB NAME -- or else you may be running on a DB from previous scripts....CRUCIAL

USE [projectname];


-- ### USE THESE VARIABLE EDITS IN FUTURE PROJECTS ###
-- select * from #variable_names order by var_id, val_id;
-- drop table #variable_names;
begin try drop table #variable_names end try begin catch end catch;
select var_id, var_name, val_id, val_name
into #variable_names
from [Ref].[Variable_Names];
--(36 rows affected)

--update #variable_names set val_id=10 where var_name='platform_id' and val_name='Mobile';
--update #variable_names set val_id=20 where var_name='platform_id' and val_name='PC';
insert into #variable_names select 1 as var_id, 'platform_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 2 as var_id, 'device_type_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 4 as var_id, 'digital_type_id' as var_name, 0 as val_id, 'All Digital' as val_name; -- ### Use 1 in the future

---------Redoes variable names table to allow for all levels of categorization
delete from #variable_names where var_id = 6;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 0 as val_id, 'Property' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 1 as val_id, 'Subcategory' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 2 as val_id, 'Category' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 3 as val_id, 'Supercategory' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 4 as val_id, 'Total Vertical' as val_name;
insert into #variable_names select 6 as var_id, 'ReportLevel' as var_name, 5 as val_id, 'Total Internet' as val_name;

create index variable_names1 on #variable_names(val_id);


---------------------------------------------------------------------------------------------------
-- STEP 2) CREATE DAILY RAW METRICS
---------------------------------------------------------------------------------------------------
--PROPERTY LEVEL PULL WITH SUPERCAT APPEND, DAILY, DOMAIN

begin try drop table #temp_person_daily_prop end try begin catch end catch;
select 
	'US' as country, 
	a.panelist_key, 
	platform_id,
	s.digital_type_id,
	a.report_level, 
	a.date_id,
	s.common_supercategory,
	s.common_category,
	s.common_subcategory,
	s.common_vertical,
	property_hash, 
	isnull(a.property_key,'') as property_key,
	count(distinct a.date_id) as raw_usage_days,
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
into #temp_person_daily_prop
from rps.UserByDay_Total a
inner join ref.Domain_Name d
	on a.Property_Key = d.Domain_Name_Key
inner join core.ref.Taxonomy_Common_Properties s
	on d.Domain_Name_Hash = s.property_hash
	and a.Digital_Type_ID = s.digital_type_id
	and s.digital_type_id = 235
inner join  rps.panelist_platform_qual_day  c
	on a.panelist_key = c.panelist_key
	and a.date_id = c.date_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    report_level= 0 -- property
	and a.date_id is not null
--	and s.common_supercategory not in ('[Remove]', '[Alert]', '') --,'[TBD]'
	and rps_flag = 1
group by a.panelist_key, platform_id, s.digital_type_id, a.report_level, a.date_id,s.common_supercategory,s.common_category,s.common_subcategory,s.common_vertical, a.property_key, property_hash;




--PROPERTY LEVEL PULL WITH SUPERCAT APPEND, DAILY, APP
----------------------------------------------------------------

INSERT INTO #temp_person_daily_prop
select 
	'US' as country, 
	a.panelist_key, 
	platform_id,
	s.digital_type_id,
	a.report_level, 
	a.date_id,
	s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical,	 
	property_hash, 
	isnull(a.property_key,'') as property_key,
	count(distinct a.date_id) as raw_usage_days,
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total a
inner join ref.App_Name d
	on a.Property_Key = d.App_Name_Key
inner join core.ref.Taxonomy_Common_Properties s
	on d.app_name_hash = s.property_hash
	and a.Digital_Type_ID = s.digital_type_id
	and s.digital_type_id = 100
inner join rps.panelist_platform_qual_day c
	on a.panelist_key = c.panelist_key
	and a.date_id = c.date_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    report_level= 0 -- property
	and a.date_id is not null
--	and s.common_supercategory not in ('[Remove]', '[Alert]', '') --,'[TBD]'
	and rps_flag = 1 
group by a.panelist_key, platform_id, s.digital_type_id, a.report_level, a.date_id, s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical,	 a.property_key, property_hash;



--PROPERTY LEVEL PULL WITH SUPERCAT APPEND, DAILY, TI
----------------------------------------------------------------
INSERT INTO #temp_person_daily_prop
select 
	'US' as country, 
	a.panelist_key, 
	platform_id,
	a.digital_type_id,
	5 as report_level, 
	a.date_id,
	'* Total Internet *' as common_supercategory, 
	'* Total Internet *' as common_category, 
	'* Total Internet *' as common_subcategory,
	'* Total Internet *' as common_vertical, 
	0 as property_hash, 
	isnull(a.property_key,'') as property_key,
	count(distinct a.date_id) as raw_usage_days,
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total a
inner join rps.panelist_platform_qual_day c
	on a.panelist_key = c.panelist_key
	and a.date_id = c.date_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    report_level= 3 -- total
	and a.date_id is not null
group by a.panelist_key, platform_id, a.digital_type_id, a.report_level, a.date_id, a.property_key;


begin try drop table #property_level_pull end try begin catch end catch;
select * 
into #property_level_pull
from #temp_person_daily_prop 
where Report_Level = 0;


--------Total Subcategory view
-- delete from #temp_person_daily_prop where report_level = 1;
insert into #temp_person_daily_prop 
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	1 as report_level,
	Date_ID,
	common_supercategory,
	common_category,
	common_subcategory,
	'* Total Subcategory *' as common_vertical,
	0 as property_hash, 
	0 as property_key, 
	count(distinct date_id) as raw_usage_days,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull
group by country, Panelist_Key, Platform_ID, digital_type_id,Date_ID,
common_supercategory,common_category, common_subcategory;


--------Total category view
-- delete from #temp_person_daily_prop where report_level = 2;
insert into #temp_person_daily_prop 
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	2 as report_level,
	Date_ID,
	common_supercategory,
	common_category,
	'* Total Category *' as common_subcategory,
	'* Total Category *' as common_vertical,
	0 as property_hash, 
	0 as property_key, 
	count(distinct date_id) as raw_usage_days,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull
group by country, Panelist_Key, Platform_ID, digital_type_id,Date_ID,
common_supercategory,common_category;


--------Total supercategory view
insert into #temp_person_daily_prop 
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	3 as report_level,
	Date_ID,
	common_supercategory,
	'* Total Supercategory *' as common_category,
	'* Total Supercategory *' as common_subcategory,
	'* Total Supercategory *' as common_vertical,
	0 as property_hash, 
	0 as property_key, 
	count(distinct date_id) as raw_usage_days,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull
group by country, Panelist_Key, Platform_ID, digital_type_id,Date_ID,common_supercategory;


-------Total Vertical View
insert into #temp_person_daily_prop 
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	4 as report_level,
	Date_ID,
	'* Total Vertical *' as common_supercategory,
	'* Total Vertical *' as common_category,
	'* Total Vertical *' as common_subcategory,
	common_vertical,
	0 as property_hash, 
	0 as property_key, 
	count(distinct date_id) as raw_usage_days,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull
group by country, Panelist_Key, Platform_ID, digital_type_id,Date_ID,common_vertical;



-----------------------


begin try drop table  #temp_person_agg_prop end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, date_id, common_supercategory as supercategory, 
common_category as category,common_subcategory as subcategory ,common_vertical as vertical,
property_hash, property_key,
	count(distinct date_id) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_person_agg_prop
from  #temp_person_daily_prop
group by country, panelist_key, platform_id, digital_type_id, report_level, date_id,
common_supercategory,common_category,common_subcategory,common_vertical,property_hash, property_key;


insert into #temp_person_agg_prop
select country, panelist_key, 0 as platform_id, 0 as digital_type_id, report_level, date_id,
common_supercategory as supercategory,common_category as category,common_subcategory as subcategory ,common_vertical as vertical, property_hash, property_key,
	count(distinct date_id) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from #temp_person_daily_prop
group by country, panelist_key, report_level, date_id,common_supercategory,common_category,
common_subcategory,common_vertical,property_hash, property_key;


insert into #temp_person_agg_prop
select country, panelist_key, 0 as platform_id, digital_type_id, report_level, date_id,
common_supercategory as supercategory, common_category as category,common_subcategory as subcategory ,common_vertical as vertical, property_hash, property_key,
	count(distinct date_id) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from #temp_person_daily_prop
group by country, panelist_key, digital_type_id, report_level,  date_id,
common_supercategory,common_category,common_subcategory,common_vertical,property_hash, property_key;


-- THIS SECTION: Breakout of devices for "Total Digital Types"... Does not make sense for anything other than total internet.
-- This is because properties current only get 1 digital type. (Google.com is not an app. "Angry Birds" is not a website) 
-- EXCLUDE FOR NOW. INCLUDE WHEN WE HAVE ALIGNED CATEGORIES ACROSS APP AND WEB (e.g. Social Media is same list across web and app)

insert into #temp_person_agg_prop
select country, panelist_key, platform_id, 0 as digital_type_id, report_level,date_id, 
common_supercategory as supercategory,common_category as category,common_subcategory as subcategory ,common_vertical as vertical, property_hash, property_key,
	count(distinct date_id) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from #temp_person_daily_prop
group by country, panelist_key, Platform_ID, report_level, date_id,
common_supercategory,common_category,common_subcategory,common_vertical,property_hash, property_key;


-- ################################################################################################
-- B. PERMANENT TABLE CREATED HERE
-- ################################################################################################
-- select top 100 * from rpt.Standard_Metrics_Common_DU where subcat_hash <> 0; 
-- select top 100 * from rpt.Standard_Metrics_Common_DU; 
--select top 100 * from rpt.Person_Standard_Metrics_Common_DU;
/*
select top 100  panelist_key, platform_id,digital_type_id,report_level,Date_ID,week_of_year,supercategory,property_hash,Property_Key,count(*) as count
from rpt.Person_Standard_Metrics_Common_DU 
group by panelist_key, platform_id,digital_type_id,report_level,Date_ID,week_of_year,supercategory,property_hash,Property_Key
order by count desc;
*/

--PERSON LEVEL COMMON DU -- permanent version of #temp_person_agg_prop
---------------------------------------------------------------

begin try drop table rpt.Person_Standard_Metrics_Common_DU end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, Date_ID,
supercategory,category,subcategory,vertical,property_hash, Property_Key,raw_usage_days, raw_visits, raw_minutes
into rpt.Person_Standard_Metrics_Common_DU
from #temp_person_agg_prop;



--FINAL DAILY PROPERTY LEVEL AGG
---------------------------------------
-- select top 100 * from #temp_person_agg_prop where report_level<>0;
begin try drop table #temp_panel_agg_prop end try begin catch end catch;

select country, platform_id, digital_type_id, report_level, supercategory, category,subcategory,vertical,property_hash, property_key,
	count(distinct panelist_key) as raw_persons,
	sum(raw_usage_days) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_panel_agg_prop
from  #temp_person_agg_prop
group by country, platform_id, digital_type_id, report_level, supercategory, category,subcategory,vertical,property_hash, property_key;


-- CHECK: confirm websites across all device types are represented. table with "all devices", should have digital split.
-- select top 100 * from #temp_panel_agg_subcat_prop where platform_id=0;
-- select top 100 * from #temp_panel_agg_subcat_prop where platform_id=0 and digital_type_id>0;
-- select top 100 * from #temp_panel_agg_subcat_prop where platform_id=0 and digital_type_id=0;



-- FINALIZE STANDARD METRICS DAILY USAGE, AGG LEVEL
-- Just permanent version of #temp_panel_agg_prop
-------------------------------------------------------
---#temp_panel_agg_prop
-- select top 1000 * from rpt.Standard_Metrics_Common_DU_KP order by platform_id, digital_type_id;
-- select * from rpt.standard_metrics_common_DU_KP where timeframe = 'Total Study';

begin try drop table [rpt].[Standard_Metrics_Common_DU] end try begin catch end catch;

select country, platform_id, digital_type_id, report_level, supercategory,category,subcategory, vertical,
property_hash, property_key,
	raw_persons as du, raw_usage_days as du_usedays, raw_visits as du_visits, raw_minutes as du_minutes
into [rpt].[Standard_Metrics_Common_DU]
from #temp_panel_agg_prop;



create index Standard_Metrics_Common_DU1 on rpt.Standard_Metrics_Common_DU(platform_id);
create index Standard_Metrics_Common_DU2 on rpt.Standard_Metrics_Common_DU(digital_type_id);
create index Standard_Metrics_Common_DU3 on rpt.Standard_Metrics_Common_DU(report_level);
create index Standard_Metrics_Common_DU4 on rpt.Standard_Metrics_Common_DU(supercategory);
create index Standard_Metrics_Common_DU6 on rpt.Standard_Metrics_Common_DU(property_key);
create index Standard_Metrics_Common_DU7 on rpt.Standard_Metrics_Common_DU(property_hash);
create index Standard_Metrics_Common_DU10 on rpt.Standard_Metrics_Common_DU(category);
create index Standard_Metrics_Common_DU11 on rpt.Standard_Metrics_Common_DU(vertical);
create index Standard_Metrics_Common_DU12 on rpt.Standard_Metrics_Common_DU(subcategory);


---------------------------------------------------------------------------------------------------
-- STEP 3) CREATE USAGE WEEKS AND USAGE MONTHS
---------------------------------------------------------------------------------------------------
-- select top 100 * from [Process].[UserByDay_Total];
-- select top 100 * from #person_qualified_week;
---------------------------------------------------------------------------------------------------
-- STEP 2) CREATE WEEKLY RAW METRICS
---------------------------------------------------------------------------------------------------
--PROPERTY LEVEL PULL WITH SUPERCAT APPEND, WEEKLY, DOMAIN

begin try drop table #temp_person_weekly_prop end try begin catch end catch;
select 
	'US' as country, 
	a.panelist_key, 
	platform_id,
	s.digital_type_id,
	a.report_level, 
	a.week_id,
	s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical, 
	property_hash, 
	isnull(a.property_key,'') as property_key,
	count(distinct a.week_id) as wu_useweeks,
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
into #temp_person_weekly_prop
from rps.UserByDay_Total a
inner join ref.Domain_Name d
	on a.Property_Key = d.Domain_Name_Key
inner join core.ref.Taxonomy_Common_Properties s
	on d.Domain_Name_Hash = s.property_hash
	and a.Digital_Type_ID = s.digital_type_id
	and s.digital_type_id = 235
inner join rps.panelist_platform_qual_week c
	on a.panelist_key = c.panelist_key
	and a.week_id = c.week_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    report_level= 0 -- property
	and a.date_id is not null
--	and s.common_supercategory not in ('[Remove]', '[Alert]', '') --,'[TBD]'
	and rps_flag = 1
group by a.panelist_key, platform_id, s.digital_type_id, a.report_level, a.week_id, s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical, property_hash, a.property_key;


--PROPERTY LEVEL PULL WITH SUPERCAT APPEND, weekly, APP
----------------------------------------------------------------
INSERT INTO #temp_person_weekly_prop
select 
	'US' as country, 
	a.panelist_key, 
	platform_id,
	s.digital_type_id,
	a.report_level, 
	a.week_id,
	s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical,
	property_hash,
	isnull(a.property_key,'') as property_key,
	count(distinct a.week_id) as wu_useweeks,
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total a
inner join ref.App_Name d
	on a.Property_Key = d.App_Name_Key
inner join core.ref.Taxonomy_Common_Properties s
	on d.app_name_hash = s.property_hash
	and a.Digital_Type_ID = s.digital_type_id
	and s.digital_type_id = 100
inner join rps.panelist_platform_qual_week c
	on a.panelist_key = c.panelist_key
	and a.week_id = c.week_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    report_level= 0 -- property
	and a.date_id is not null
--	and s.common_supercategory not in ('[Remove]', '[Alert]', '') --,'[TBD]'
	and rps_flag = 1
group by a.panelist_key, platform_id, s.digital_type_id, a.report_level, a.week_id, s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical, property_hash, a.property_key;


--PROPERTY LEVEL PULL WITH SUPERCAT APPEND, weekly, TI
----------------------------------------------------------------
INSERT INTO #temp_person_weekly_prop
select 
	'US' as country, 
	a.panelist_key, 
	platform_id,
	a.digital_type_id,
	5 as report_level, 
	a.week_id,
	'* Total Internet *' as common_supercategory,
	'* Total Internet *' as common_category,
	'* Total Internet *' as common_subcategory,
	'* Total Internet *' as common_vertical, 
	0 as property_hash, 
	isnull(a.property_key,'') as property_key,
	count(distinct a.week_id) as wu_useweeks,
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total a
inner join rps.panelist_platform_qual_week c
	on a.panelist_key = c.panelist_key
	and a.week_id = c.week_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    report_level= 3 -- total
	and a.date_id is not null
group by a.panelist_key, platform_id, a.digital_type_id, a.report_level, a.week_id, a.property_key;

----------------------------

begin try drop table #property_level_pull_weekly end try begin catch end catch;
select * 
into #property_level_pull_weekly
from #temp_person_weekly_prop 
where Report_Level = 0;


--------Total Subcategory view
insert into #temp_person_weekly_prop
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	1 as report_level,
	week_id,
	common_supercategory,
	common_category,
	common_subcategory,
	'* Total Subcategory *' as common_vertical,
	0 as property_hash,
	0 as property_key,
	count(distinct Week_ID) as wu_useweeks,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull_weekly
group by country,Panelist_Key,Platform_ID,digital_type_id,Week_ID,
common_supercategory, common_category,common_subcategory;



--------Total category view
insert into #temp_person_weekly_prop
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	2 as report_level,
	Week_ID,
	common_supercategory,
	common_category,
	'* Total Category *' as common_subcategory,
	'* Total Category *' as common_vertical,
	0 as property_hash,
	0 as property_key,
	count(distinct Week_ID) as wu_useweeks,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull_weekly
group by country,Panelist_Key,Platform_ID,digital_type_id,Week_ID,
common_supercategory, common_category;


--------Total supercategory view
insert into #temp_person_weekly_prop
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	3 as report_level,
	Week_ID,
	common_supercategory,
	'* Total Supercategory *' as common_category,
	'* Total Supercategory *' as common_subcategory,
	'* Total Supercategory *' as common_vertical,
	0 as property_hash,
	0 as property_key,
	count(distinct Week_ID) as wu_useweeks,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull_weekly
group by country,Panelist_Key,Platform_ID,digital_type_id,Week_ID,
common_supercategory;


-------Total Vertical View
insert into #temp_person_weekly_prop
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	4 as report_level,
	Week_ID,
	'* Total Vertical *' as common_supercategory,
	'* Total Vertical *' as common_category,
	'* Total Vertical *' as common_subcategory,
	common_vertical,
	0 as property_hash,
	0 as property_key,
	count(distinct Week_ID) as wu_useweeks,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull_weekly
group by country,Panelist_Key,Platform_ID,digital_type_id,Week_ID,
common_vertical;



begin try drop table  #temp_person_agg_prop_weekly end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, week_id, common_supercategory as supercategory, 
common_category as category,common_subcategory as subcategory ,common_vertical as vertical,property_hash, property_key,
	count(distinct week_id) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_person_agg_prop_weekly
from  #temp_person_weekly_prop
group by country, panelist_key, platform_id, digital_type_id, report_level, week_id, common_supercategory,common_category,
common_subcategory,common_vertical, property_hash,property_key;


insert into #temp_person_agg_prop_weekly
select country, panelist_key, 0 as platform_id, 0 as digital_type_id, report_level, week_id,common_supercategory as supercategory, 
common_category as category,common_subcategory as subcategory ,common_vertical as vertical,property_hash,property_key,
	count(distinct week_id) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from #temp_person_weekly_prop
group by country, panelist_key, report_level, week_id,common_supercategory,common_category,
common_subcategory,common_vertical, property_hash,property_key;


insert into #temp_person_agg_prop_weekly
select country, panelist_key, 0 as platform_id, digital_type_id, report_level, week_id,  common_supercategory,common_category,
common_subcategory,common_vertical, property_hash,property_key,
	count(distinct week_id) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from #temp_person_weekly_prop
group by country, panelist_key, digital_type_id, report_level, week_id,  common_supercategory,common_category,
common_subcategory,common_vertical,property_hash, property_key;


-- THIS SECTION: Breakout of devices for "Total Digital Types"... Does not make sense for anything other than total internet.
-- This is because properties current only get 1 digital type. (Google.com is not an app. "Angry Birds" is not a website) 
-- EXCLUDE FOR NOW. INCLUDE WHEN WE HAVE ALIGNED CATEGORIES ACROSS APP AND WEB (e.g. Social Media is same list across web and app)

insert into #temp_person_agg_prop_weekly
select country, panelist_key, platform_id, 0 as digital_type_id, report_level, week_id,  common_supercategory,common_category,
common_subcategory,common_vertical,property_hash, property_key,
	count(distinct week_id) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from #temp_person_weekly_prop
group by country, panelist_key, platform_id, report_level, week_id,  common_supercategory,common_category,
common_subcategory,common_vertical, property_hash,property_key;


--FINAL weekly PROPERTY LEVEL AGG
---------------------------------------
-- select top 100 * from #temp_panel_agg_prop_weekly where report_level<>0;
begin try drop table #temp_panel_agg_prop_weekly end try begin catch end catch;

select country, platform_id, digital_type_id, report_level,supercategory, category,subcategory,vertical, property_hash, property_key,
	count(distinct panelist_key) as raw_persons,
	sum(wu_useweeks) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_panel_agg_prop_weekly
from  #temp_person_agg_prop_weekly
group by country, platform_id, digital_type_id, report_level, supercategory, category,subcategory,vertical, property_hash, property_key;


-- CHECK: confirm websites across all device types are represented. table with "all devices", should have digital split.
-- select top 100 * from #temp_panel_agg_subcat_prop where platform_id=0;
-- select top 100 * from #temp_panel_agg_subcat_prop where platform_id=0 and digital_type_id>0;
-- select top 100 * from #temp_panel_agg_subcat_prop where platform_id=0 and digital_type_id=0;


-- ################################################################################################
-- B. PERMANENT TABLE CREATED HERE
-- ################################################################################################
--STAGE A PERSON LEVEL VERSION usage weeks OF ECOSYSTEM CATEGORY FOR UNIFIED PULL
-- select top 100 * from rpt.Person_Category_Usage_Weeks_v2 where subcat_hash <> 0; 
-- select platform_id, digital_type_id from rpt.Person_Category_Usage_Weeks_v2 group by platform_id, digital_type_id ; 

--PERSON LEVEL COMMON WU, HAS BOTH PROPERTY AND CATEGORY LEVEL AT WEEKLY LEVEL
---------------------------------------------------------------

begin try drop table rpt.Person_Category_Usage_Weeks end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, week_id, supercategory,category,subcategory,vertical,
property_hash, Property_Key, wu_useweeks, raw_visits, raw_minutes
into rpt.Person_Category_Usage_Weeks
from #temp_person_agg_prop_weekly;


-- FINALIZE STANDARD METRICS WEEKLY USAGE, AGG LEVEL
-------------------------------------------------------
begin try drop table #standard_metrics_wu_useweeks end try begin catch end catch;

select country, platform_id, digital_type_id, report_level, supercategory,category, subcategory,vertical,
property_hash, property_key, raw_persons as wu, wu_useweeks as wu_useweeks, raw_visits as wu_visits, raw_minutes as wu_minutes
into #standard_metrics_wu_useweeks
from #temp_panel_agg_prop_weekly;
--(1736172 rows affected)


-------------------------------------------------------------------------------
-- BE SURE WEEK AND MONTH USAGE ARE INDEXED
-- We really should just use tax_id... as the map, so we don't have to index on all 4 levels of taxonomy/reporting structure.
create index standard_metrics_wu_useweeks1 on #standard_metrics_wu_useweeks(platform_id);
create index standard_metrics_wu_useweeks2 on #standard_metrics_wu_useweeks(digital_type_id);
create index standard_metrics_wu_useweeks3 on #standard_metrics_wu_useweeks(supercategory);
create index standard_metrics_wu_useweeks5 on #standard_metrics_wu_useweeks(property_key);
create index standard_metrics_wu_useweeks7 on #standard_metrics_wu_useweeks(property_hash);
create index standard_metrics_wu_useweeks6 on #standard_metrics_wu_useweeks(report_level);
create index standard_metrics_wu_useweeks8 on #standard_metrics_wu_useweeks(category);
create index standard_metrics_wu_useweeks9 on #standard_metrics_wu_useweeks(subcategory);
create index standard_metrics_wu_useweeks10 on #standard_metrics_wu_useweeks(vertical);
---------------------------------------------------------------------------------------------------
-- STEP 2) CREATE monthLY RAW METRICS
---------------------------------------------------------------------------------------------------
--PROPERTY LEVEL PULL WITH SUPERCAT APPEND, monthLY, DOMAIN

begin try drop table #temp_person_monthly_prop end try begin catch end catch;
select 
	'US' as country, 
	a.panelist_key, 
	platform_id,
	s.digital_type_id,
	a.report_level, 
	a.month_id,
	s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical,
	property_hash, 
	isnull(a.property_key,'') as property_key,
	count(distinct a.month_id) as mu_usemonths,
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
into #temp_person_monthly_prop
from rps.UserByDay_Total a
inner join ref.Domain_Name d
	on a.Property_Key = d.Domain_Name_Key
inner join core.ref.Taxonomy_Common_Properties s
	on d.Domain_Name_Hash = s.property_hash
	and a.Digital_Type_ID = s.digital_type_id
	and s.digital_type_id = 235
inner join rps.panelist_platform_qual_month c
	on a.panelist_key = c.panelist_key
	and a.month_id = c.month_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    report_level= 0 -- property
	and a.date_id is not null
--	and s.common_supercategory not in ('[Remove]', '[Alert]', '') --,'[TBD]'
	and rps_flag = 1
group by a.panelist_key, platform_id, s.digital_type_id, a.report_level, a.month_id, s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical, property_hash, a.property_key;



--PROPERTY LEVEL PULL WITH SUPERCAT APPEND, monthly, APP
----------------------------------------------------------------
INSERT INTO #temp_person_monthly_prop
select 
	'US' as country, 
	a.panelist_key, 
	platform_id,
	s.digital_type_id,
	a.report_level, 
	a.month_id,
	s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical,
	property_hash, 
	isnull(a.property_key,'') as property_key,
	count(distinct a.month_id) as mu_usemonths,
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total a
inner join ref.App_Name d
	on a.Property_Key = d.App_Name_Key
inner join core.ref.Taxonomy_Common_Properties s
	on d.app_name_hash = s.property_hash
	and a.Digital_Type_ID = s.digital_type_id
	and s.digital_type_id = 100
inner join rps.panelist_platform_qual_month c
	on a.panelist_key = c.panelist_key
	and a.month_id = c.month_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    report_level= 0 -- property
	and a.date_id is not null
--	and s.common_supercategory not in ('[Remove]', '[Alert]', '') --,'[TBD]'
	and rps_flag = 1
group by a.panelist_key, platform_id, s.digital_type_id, a.report_level, a.month_id,s.common_supercategory, 
	s.common_category,
	s.common_subcategory,
	s.common_vertical, property_hash, a.property_key;


--PROPERTY LEVEL PULL WITH SUPERCAT APPEND, monthly, TI
----------------------------------------------------------------
INSERT INTO #temp_person_monthly_prop
select 
	'US' as country, 
	a.panelist_key, 
	platform_id,
	a.digital_type_id,
	5 as report_level, 
	a.month_id,
	'* Total Internet *' as common_supercategory,
	'* Total Internet *' as common_category,
	'* Total Internet *' as common_subcategory,
	'* Total Internet *' as common_vertical, 
	0 as property_hash, 
	isnull(a.property_key,'') as property_key,
	count(distinct a.month_id) as mu_usemonths,
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total a
inner join rps.panelist_platform_qual_month c
	on a.panelist_key = c.panelist_key
	and a.month_id = c.month_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    report_level= 3 -- total
	and a.date_id is not null
group by a.panelist_key, platform_id, a.digital_type_id, a.report_level, a.month_id, a.property_key;


begin try drop table #property_level_pull_monthly end try begin catch end catch;
select * 
into #property_level_pull_monthly
from #temp_person_monthly_prop 
where Report_Level = 0;
--(467266 rows affected)

--------Total Subcategory view
insert into #temp_person_monthly_prop
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	1 as report_level,
	Month_ID,
	common_supercategory,
	common_category,
	common_subcategory,
	'* Total Subcategory *' as common_vertical,
	0 as property_hash,
	0 as property_key,
	count(distinct Month_ID) as mu_usemonths,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull_monthly
group by country,Panelist_Key,Platform_ID,digital_type_id,Month_ID,
common_supercategory, common_category,common_subcategory;



--------Total category view
insert into #temp_person_monthly_prop
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	2 as report_level,
	Month_ID,
	common_supercategory,
	common_category,
	'* Total Category *' as common_subcategory,
	'* Total Category *' as common_vertical,
	0 as property_hash,
	0 as property_key,
	count(distinct Month_ID) as mu_usemonths,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull_monthly
group by country,Panelist_Key,Platform_ID,digital_type_id,Month_ID,
common_supercategory, common_category;



--------Total supercategory view
insert into #temp_person_monthly_prop
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	3 as report_level,
	Month_ID,
	common_supercategory,
	'* Total Supercategory *' as common_category,
	'* Total Supercategory *' as common_subcategory,
	'* Total Supercategory *' as common_vertical,
	0 as property_hash,
	0 as property_key,
	count(distinct Month_ID) as mu_usemonths,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull_monthly
group by country,Panelist_Key,Platform_ID,digital_type_id,month_id,
common_supercategory;

-------Total Vertical View
insert into #temp_person_monthly_prop
select 
	country,
	Panelist_Key,
	Platform_ID,
	digital_type_id,
	4 as report_level,
	Month_ID,
	'* Total Vertical *' as common_supercategory,
	'* Total Vertical *' as common_category,
	'* Total Vertical *' as common_subcategory,
	common_vertical,
	0 as property_hash,
	0 as property_key,
	count(distinct Month_ID) as mu_usemonths,
	sum(raw_visits) as raw_visits, 
	sum(raw_minutes) as raw_minutes
from #property_level_pull_monthly
group by country,Panelist_Key,Platform_ID,digital_type_id,Month_ID,
common_vertical;
--(46122 rows affected)

-----------------------------------
-- select top 100 * from #temp_person_monthly_prop;

-- drop table #temp_person_agg_subcat_prop;
begin try drop table  #temp_person_agg_prop_monthly end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, month_id, common_supercategory as supercategory, 
common_category as category,common_subcategory as subcategory ,common_vertical as vertical, property_hash, property_key,
	count(distinct month_id) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_person_agg_prop_monthly
from  #temp_person_monthly_prop
group by country, panelist_key, platform_id, digital_type_id, report_level, month_id, common_supercategory,common_category,
common_subcategory,common_vertical, property_hash, property_key;


insert into #temp_person_agg_prop_monthly
select country, panelist_key, 0 as platform_id, 0 as digital_type_id, report_level, month_id, common_supercategory,common_category,
common_subcategory,common_vertical, property_hash, property_key,
	count(distinct month_id) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from #temp_person_monthly_prop
group by country, panelist_key, report_level, month_id, common_supercategory,common_category,
common_subcategory,common_vertical, property_hash, property_key;

insert into #temp_person_agg_prop_monthly
select country, panelist_key, 0 as platform_id, digital_type_id, report_level, month_id, common_supercategory,common_category,
common_subcategory,common_vertical, property_hash, property_key,
	count(distinct month_id) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from #temp_person_monthly_prop
group by country, panelist_key, digital_type_id, report_level, month_id, common_supercategory,common_category,
common_subcategory,common_vertical, property_hash, property_key;


-- THIS SECTION: Breakout of devices for "Total Digital Types"... Does not make sense for anything other than total internet.
-- This is because properties current only get 1 digital type. (Google.com is not an app. "Angry Birds" is not a website) 
-- EXCLUDE FOR NOW. INCLUDE WHEN WE HAVE ALIGNED CATEGORIES ACROSS APP AND WEB (e.g. Social Media is same list across web and app)

insert into #temp_person_agg_prop_monthly
select country, panelist_key, platform_id, 0 as digital_type_id, report_level, month_id, common_supercategory,common_category,
common_subcategory,common_vertical, property_hash, property_key,
	count(distinct month_id) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from #temp_person_monthly_prop
group by country, panelist_key, platform_id, report_level, month_id, common_supercategory,common_category,
common_subcategory,common_vertical, property_hash, property_key;


--FINAL monthly PROPERTY LEVEL AGG
---------------------------------------
-- select top 100 * from #temp_panel_agg_subcat_prop where report_level<>0;
begin try drop table #temp_panel_agg_prop_monthly end try begin catch end catch;

select country, platform_id, digital_type_id, report_level,  supercategory, category,subcategory,vertical, property_hash, property_key,
	count(distinct panelist_key) as raw_persons,
	sum(mu_usemonths) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_panel_agg_prop_monthly
from  #temp_person_agg_prop_monthly
group by country, platform_id, digital_type_id, report_level,  supercategory, category,subcategory,vertical,property_hash, property_key;



-- CHECK: confirm websites across all device types are represented. table with "all devices", should have digital split.
-- select top 100 * from #temp_panel_agg_subcat_prop where platform_id=0;
-- select top 100 * from #temp_panel_agg_subcat_prop where platform_id=0 and digital_type_id>0;
-- select top 100 * from #temp_panel_agg_subcat_prop where platform_id=0 and digital_type_id=0;

-- ################################################################################################
-- B. PERMANENT TABLE CREATED HERE
-- ################################################################################################
--STAGE A PERSON LEVEL VERSION usage months OF ECOSYSTEM CATEGORY FOR UNIFIED PULL
-- select top 100 * from rpt.Person_Category_Usage_months_v2 where subcat_hash <> 0; 
-- select platform_id, digital_type_id from rpt.Person_Category_Usage_months_v2 group by platform_id, digital_type_id ; 

--PERSON LEVEL COMMON WU, HAS BOTH PROPERTY AND CATEGORY LEVEL AT monthLY LEVEL
---------------------------------------------------------------

begin try drop table rpt.Person_Category_Usage_months end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, month_id,supercategory,category,subcategory,vertical,
 property_hash, Property_Key, mu_usemonths, raw_visits, raw_minutes
into rpt.Person_Category_Usage_months
from #temp_person_agg_prop_monthly;


-- FINALIZE STANDARD METRICS MONTHLY USAGE, AGG LEVEL
-------------------------------------------------------

begin try drop table #standard_metrics_mu_usemonths end try begin catch end catch;


select country, platform_id, digital_type_id, report_level, supercategory,category, subcategory,vertical, property_hash, property_key,
	raw_persons as mu, mu_usemonths as mu_usemonths, raw_visits as mu_visits, raw_minutes as mu_minutes
into #standard_metrics_mu_usemonths
from #temp_panel_agg_prop_monthly;



-------------------------------------------------------------------------------
-- BE SURE WEEK AND MONTH USAGE ARE INDEXED
-- We really should just use tax_id... as the map, so we don't have to index on all 4 levels of taxonomy/reporting structure.
create index standard_metrics_mu_usemonths1 on #standard_metrics_mu_usemonths(platform_id);
create index standard_metrics_mu_usemonths2 on #standard_metrics_mu_usemonths(digital_type_id);
create index standard_metrics_mu_usemonths4 on #standard_metrics_mu_usemonths(supercategory);
create index standard_metrics_mu_usemonths5 on #standard_metrics_mu_usemonths(property_key);
create index standard_metrics_mu_usemonths7 on #standard_metrics_mu_usemonths(property_hash);
create index standard_metrics_mu_usemonths6 on #standard_metrics_mu_usemonths(report_level);


---------------------------------------------------------------------------------------------------
-- STEP 4) INTEGRATE EVERYTHING... FO SHO
---------------------------------------------------------------------------------------------------

-- select top 100 * from #temp_total_internet_minutes where platform_id = 1;
-- select * from #temp_total_internet_minutes order by 1,2,3;
begin try drop table #temp_total_internet_minutes end try begin catch end catch;
select country, platform_id, digital_type_id, du_minutes
into #temp_total_internet_minutes
from rpt.Standard_Metrics_Common_DU
where report_level=5;


create index temp_total_internet_minutes1 on #temp_total_internet_minutes(platform_id);
create index temp_total_internet_minutes2 on #temp_total_internet_minutes(digital_type_id);


-- select top 100 * from #temp_standard_metrics_booya;
-- when you get here, and you have a table output, say "boo ya!"
begin try drop table #temp_standard_metrics_booya_prep1 end try begin catch end catch;
select 
	a.country,
	a.platform_id, 
	a.digital_type_id, 
	a.report_level, 
	a.supercategory,
	a.category,
	a.subcategory,
	a.vertical,
	a.property_hash,
	a.property_key,
	p.dp, 
	a.du, 
	p.dp_qualdays, 
	a.du_usedays, 
	a.du_visits,
	cast(a.du_minutes as numeric(30,2)) as du_mins,
	cast(ti.du_minutes as numeric(30,2)) as du_mins_ti,
	p.wp_qualweeks,
	p.mp_qualmonths 
into #temp_standard_metrics_booya_prep1
from rpt.Standard_Metrics_Common_DU a
inner join rps.device_topline_qual p
	on a.platform_id=p.platform_id
--	and a.country = p.country
inner join #temp_total_internet_minutes ti
	on a.platform_id=ti.platform_id
	and a.digital_type_id=ti.digital_type_id
	and a.country = ti.country
;


-- CHECK: confirm websites across all device types are represented. table with "all devices", should have digital split.
-- select top 100 * from #temp_standard_metrics_booya where platform_id=0;
-- select top 100 * from #temp_standard_metrics_booya where platform_id=0 and digital_type_id>0;

begin try drop table #temp_standard_metrics_booya_prep2 end try begin catch end catch;
select 
	a.*,
	wu.wu_useweeks
into #temp_standard_metrics_booya_prep2
from #temp_standard_metrics_booya_prep1 a
inner join #standard_metrics_wu_useweeks wu
	on a.platform_id=wu.platform_id
	and a.digital_type_id=wu.digital_type_id
	and a.report_level=wu.report_level
	and a.supercategory = wu.supercategory
	and a.category = wu.category
	and a.subcategory = wu.subcategory
	and a.vertical = wu.vertical
	and a.property_key=wu.property_key
	and a.property_hash = wu.property_hash
	and a.country = wu.country
;


begin try drop table #temp_standard_metrics_booya end try begin catch end catch;
select a.*,
	mu.mu_usemonths
into #temp_standard_metrics_booya
from #temp_standard_metrics_booya_prep2 a
inner join #standard_metrics_mu_usemonths mu
	on a.platform_id=mu.platform_id
	and a.digital_type_id=mu.digital_type_id
	and a.report_level=mu.report_level
	and a.supercategory = mu.supercategory
	and a.category = mu.category
	and a.subcategory = mu.subcategory
	and a.vertical = mu.vertical
	and a.property_key=mu.property_key
	and a.property_hash = mu.property_hash
	and a.country = mu.country
;


-- ################################################################################################
-- C. PERMANENT TABLE CREATED HERE
-- ################################################################################################
-- select top 100 * from #temp_standard_metrics_booya;
-- select top 100 * from #temp_standard_metrics_booya where subcat_hash <>0;
-- select top 100 * from #variable_names order by var_id, val_id;
-- select top 100 * from #cat_subcat_lookup;
-- select top 100 * from #tax_version_lookup;


begin try drop table [rpt].[Standard_Metrics_Common_Report] end try begin catch end catch;

select 
	a.country, 
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	e.val_name as report_level, 
	a.supercategory,
	a.category,
	a.subcategory,
	a.vertical, 
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
into [rpt].[Standard_Metrics_Common_Report]
from #temp_standard_metrics_booya a
left outer join #variable_names c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
left outer join #variable_names e
	on a.Report_Level=e.val_id
	and e.var_name = 'ReportLevel'
where Report_Level <> 0 --- does not include property level
;


-- ################################################################################################
-- PERMANENT TABLE CREATED HERE
-- ################################################################################################
-- select top 100 * from rpt.Standard_Metrics_Property;
-- select top 100 * from rpt.Standard_Metrics_Property where device_type='All Devices';
-- select count(*) from rpt.Standard_Metrics_Property;

begin try drop table [rpt].[Standard_Metrics_Property] end try begin catch end catch;

select
	country, 															
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	e.val_name as report_level,
	supercategory,
	category,
	subcategory,
	vertical,
	s.property_name,	
--	0 as whitelist_flag,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
into [rpt].[Standard_Metrics_Property]
from #temp_standard_metrics_booya a
inner join core.ref.Taxonomy_Common_Properties s
	on a.property_hash = s.property_hash
	and a.digital_type_id=s.digital_type_id
	and s.rpt_flag = 1					--limiting to reportable sites only
left outer join #variable_names c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
left outer join #variable_names e
	on a.Report_Level=e.val_id
	and e.var_name = 'ReportLevel'
where a.Report_Level = 0
;

--TI INSERT
insert into [rpt].[Standard_Metrics_Property]
select 
	country, 
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	e.val_name as report_level,
	'* Total Internet * ' as supercategory, 
	'* Total Internet * ' as category, 
	'* Total Internet * ' as subcategory, 
	'* Total Internet * ' as vertical, 
	'* Total Internet * ' as property,
--	0 as whitelist_flag,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
from #temp_standard_metrics_booya a
left outer join #variable_names c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join  #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
inner join  #variable_names g
	on a.report_level=g.val_id
	and g.var_name = 'reportlevel'
left outer join #variable_names e
	on a.Report_Level=e.val_id
	and e.var_name = 'ReportLevel'
WHERE a.report_level = 5 --TI
;


---------------------------------------------------------------------------------------------------
-- STEP 5) Creates Share of Digital
---------------------------------------------------------------------------------------------------
-- select * from rpt.Share_of_Digital_Property_Level;
-- select top 1000 * from [rpt].[Standard_Metrics_Property];
begin try drop table rpt.Share_of_Digital_Property_Level end try begin catch end catch;
select device_type, digital_type, property_name, supercategory, du_mins, du_visits, du as daily_users
into rpt.Share_of_Digital_Property_Level
from [rpt].[Standard_Metrics_Property]
where report_level = 'Property' 
and du >=15
and device_type in ('All Devices','Mobile','PC')
;

---------------------------------------------------------------------------------------------------
-- STEP 6) EXPORT STANDARD METRICS... GET IT OUT
---------------------------------------------------------------------------------------------------

-- EXPORT 1
-- This selection exports TI, SUPERCAT
select 
	country,
	report_level, 
	device_type, 
	digital_type, 
    supercategory,
	category,
--	subcategory,
--	vertical,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
from [rpt].[Standard_Metrics_Common_Report] 
where report_level ! = 'Property'
	and supercategory not in ('[Alert]','[TBD]')
	and report_level in ('Total Internet', 'Supercategory', 'Category')
order by 
	country, 
	report_level,
	device_type, 
	digital_type,  
	supercategory, 
	category,
	subcategory,
	vertical,
	du desc;

---------------------------------------------------------
-- EXPORT 2
-- This selection exports TI, Property
select 
	country, 
	device_type, 
	digital_type, 
--	whitelist_flag,
	supercategory, 
	category,
--	subcategory,
--	vertical,
	property_name,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
from [rpt].[Standard_Metrics_Property]
where (whitelist_flag = 1 or
	du >=15)
order by 
	country,  
	device_type, 
	digital_type, 
	supercategory,
	category,
	subcategory,
	vertical, 
	property_name, 
	du desc
;
/*
-----------
--Export 3: Share of Digital
select *
from rpt.Share_of_Digital
order by device_type, digital_type,report_level, supercategory, category, subcategory, vertical;
*/

-- ############################
-- SUMMARY OF PERMANENT TABLES
-- ############################
-- select top 100 * from wrk.panelist_platform_qual_day;

-- select top 100 * from Participation;
-- permanent table for #participation_summary

-- select top 100 * from rpt.Standard_Metrics_DU;
-- select top 100 * from rpt.Standard_Metrics_Report;
-- select top 100 * from rpt.Standard_Metrics_Property;

-- END OF FILE --