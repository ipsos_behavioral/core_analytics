-- OWNER NAME
-- Ecosystem Standard Metrics
-- DATE
---------------------------------------------------------------------------------------------------

USE [PROJECTNAME];  --replace this and every instance of PROEJCTNAME in the script with your project schema (CTRL+F for PROJECTNAME and replace)

-- TERMINOLGY WE USE
------------------------------------------------------------------
-- Dimensions: Ways the data will be cut (e.g. device type, digital, population segments, time segments)
-- Classifications: Taxonomy mapping, often detailed, that describes scattered datapoints into a meaningful way. (e.g. domain, property, category)
-- Measurements: Sometimes called metrics. Numeric values only. These may be additive across dimensions (e.g. minutes, visits, searches) or deduplicated (unique visitors, usage days)


--COULD REMOVE THIS SECTION IF WE UPDATED THE VARIABLE NAMES DB TABLE
-- select * from #tax_version_lookup;
begin try drop table #tax_version_lookup end try begin catch end catch;

select cast(0 as int) as tax_version_id, cast('Common' as varchar(20)) as tax_version
into #tax_version_lookup;
insert into #tax_version_lookup select 1, 'Custom';

create index tax_version_lookup1 on #tax_version_lookup(tax_version_id);

-- ### USE THESE VARIABLE EDITS IN FUTURE PROJECTS ###

begin try drop table #variable_names end try begin catch end catch;

select var_id, var_name, val_id, val_name
into #variable_names
from [Ref].[Variable_Names];


--update #variable_names set val_id=10 where var_name='platform_id' and val_name='Mobile';
--update #variable_names set val_id=20 where var_name='platform_id' and val_name='PC';
insert into #variable_names select 1 as var_id, 'platform_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 2 as var_id, 'device_type_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 4 as var_id, 'digital_type_id' as var_name, 0 as val_id, 'All Digital' as val_name; -- ### Use 1 in the future

create index variable_names1 on #variable_names(val_id);




--- SET UP PROPERTY_HASH LOOKUP FOR TAXONOMY_COMMON_PROPERTIES TABLE
begin try drop table #property_hash_lookup end try begin catch end catch;

select cast(domain_name_key as int) as property_key, domain_name as property_name,  domain_name_hash as property_hash, cast(235 as int) as digital_Type_id
into #property_hash_lookup
from ref.domain_name;


-- set identity_insert #property_hash_lookup on;
insert into #property_hash_lookup
select  cast(App_Name_Key as int) as property_key, APP_NAME as property_name,  App_Name_Hash as property_hash, cast(100 as int) as digital_Type_id
from ref.App_Name;


-- JOIN TO TAXONOMY_COMMON_PROPERTIES FOR RPT_FLAG
begin try drop table #taxonomy_common_prop_lookup end try begin catch end catch;

select a.*, b.rpt_flag, b.rps_flag
into #taxonomy_common_prop_lookup
from #property_hash_lookup a
join core.ref.Taxonomy_Common_Properties b -------------CHANGE TO LOCKED DOWN PROJECT SPECIFIC VERSION
	on a.property_hash=b.property_hash
	and a.digital_Type_id=b.digital_type_id;


-- select * from #device_topline_qual;
---------------------------------------------------------------------------------------------------
-- 2) Taxonomy Lookup Tables, PROJECT SPECIFIC UPDATES TO THE ECO TAXONOMY FOR ALTS
---------------------------------------------------------------------------------------------------
-- select * from ref.taxonomy_Ecosystem where domain_name is null;

begin try drop table #source_id_map end try begin catch end catch;

select source_id, source_name
into #source_id_map
from Ref.Taxonomy_Ecosystem
group by source_id, source_name; 

begin try drop table #cat_id_map_prep end try begin catch end catch;
select cat_id, category
into #cat_id_map_prep
from Ref.Taxonomy_Ecosystem
group by cat_id, category;

insert into #cat_id_map_prep select 2010, '* Total XX Ecosystem *';   --INSERT IN A ALT CAT LINE



begin try drop table #cat_id_map end try begin catch end catch;
select cat_id, category
into #cat_id_map
from #cat_id_map_prep
group by cat_id, category;

begin try drop table #subcat_id_map end try begin catch end catch;

select subcat_id, subcategory
into #subcat_id_map
from Ref.Taxonomy_Ecosystem
group by  subcat_id, subcategory;

insert into #subcat_id_map select 0, '[ Total Category ]';



-- select * from ref.Taxonomy_Ecosystem;

-- select * from #subcat_id_map;

----------------------------------
--- CREATE CONTENT CATEGORY LOOKUP
----------------------------------
-------Repeat this for however many content category groups there are (can also just do inserts but this is quicker)
begin try drop table #content_cat_lookup end try begin catch end catch;
select content_category_code_1 as content_cat_id, content_category_1 as content_category_name
into #content_cat_lookup 
from Ref.Taxonomy_Ecosystem
where content_cat_id is not null
group by content_category_code_1, content_category_1
;
--(XX rows affected)

insert into #content_cat_lookup
select content_category_code_2 as content_cat_id, content_category_2 as content_category_name
from  Ref.Taxonomy_Ecosystem
where content_cat_id is not null
group by content_category_code_2, content_category_2;
--(XX rows affected)

insert into #content_cat_lookup
select content_category_code_3 as content_cat_id, content_category_3 as content_category_name
from  Ref.Taxonomy_Ecosystem
where content_cat_id is not null
group by content_category_code_3, content_category_3;
--(XX rows affected)

--the below content_cat_lookup is for the Total Ecosystem Line. Always "select 0 as content_cat_id" and rename to whatever your ecosystem is representing (ex: For Newell Baby: "Total Baby")
insert into #content_cat_lookup select 0 as content_cat_id, '* Total XX *' as content_category_name;


--- insert in new cat ids for any roll-up alts you want to be creating
insert into #content_cat_lookup select xx as content_cat_id, ' xx ' as content_category_name; 


------------------------------
--------------------------------------------------------------------------
/*
-- CATEGORY/SUBCATEGORY DUPE CHECKS!!!!!!!!!!!!!!!!!
-- select source_id, count(*) from #source_id_map group by source_id order by 2 desc; -- OK. No dupes.
-- select cat_id, count(*) from #cat_id_map group by cat_id order by 2 desc; -- OK. No dupes.
-- select subcat_id, count(*) from #subcat_id_map group by subcat_id order by 2 desc; -- OK. No dupes.
-- select * from #subcat_id_map;
*/
--------------------------------------------------------------------------------------------------------

--PROPERTY MAPPING

-- can pull from BDG_display table instead of ref.taxonomy_ecosystem

begin try drop table #property_map end try begin catch end catch;

select bdg_display_key, bdg_display
into #property_map
from Ref.Taxonomy_Ecosystem
group by  bdg_display_key, bdg_display;

create index source_id_map1 on #source_id_map (source_id);
create index cat_id_map1 on #cat_id_map (cat_id);
create index subcat_id_map1 on #subcat_id_map (subcat_id);
--create index property_map1 on #property_map (bdg_id);
-- 1727 rows affected

-- select top 1000 * from ref.Taxonomy_Ecosystem;



-- select * from #content_cat_Stacked order by source_particle_key;

---------------------------------------------------------------------------------------------------
-- STEP 3) CREATE DAILY RAW METRICS AT PROPERTY, SUBCATEGORY, CATEGORY LEVEL
---------------------------------------------------------------------------------------------------
-- PERSON LEVEL AGG
-- select top 100 * from [PROJECTNAME].UserByDay_Total_Ecosystem

-----------------------------------------
--TOPLINE DAILY PULL FROM STANDARD USERBYDAY, THIS IS REPEATED AT WEEKLY AND MONTHLY LEVEL 
-----------------------------------------
begin try drop table #temp_person_daily_cat_total end try begin catch end catch;

select 
	c.country,
	a.panelist_key, 
	platform_id, 
	digital_type_id,
	cast(1 as int) as tax_version_id, 
	report_level,
	0 as content_cat_id, -- total line
	a.Date_ID,
	isnull(cat_hash,'') as cat_hash, 
	isnull(SubCat_Hash,'') as SubCat_Hash, 
	isnull(property_key,'') as property_key,
	count(distinct a.Date_ID) as raw_usage_days, --count(*) as raw_usage_days_check, -- check can be commented out if script works
	sum(Num_Uses) as raw_visits, 
	cast(sum(Dur_Minutes) as numeric(30,5)) as raw_minutes
into #temp_person_daily_cat_total
from rps.UserByDay_Total_Ecosystem a
inner join rps.panelist_platform_qual_day c
	on a.panelist_key = c.panelist_key
	and a.Date_ID = c.Date_ID
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where
    (report_level= 2  --category
	or report_level = 1 --subcat
	or report_level= 3 --total
	or report_level = 0) --property
	and a.Date_ID is not null
group by c.country, a.panelist_key, platform_id, digital_type_id, a.report_level, a.Date_ID, cat_hash, SubCat_Hash, property_key;


----------------------------------------------
--- CONTENT PULLS
----------------------------------------------

insert into #temp_person_daily_cat_total
select 
	c.country,	
	a.panelist_key, 
	platform_id, 
	digital_type_id, 
	cast(1 as int) as tax_version_id, 
	report_level,
	a.content_cat_id, 
	a.date_id,
	isnull(cat_hash,'') as cat_hash,
	isnull(subcat_hash,'') as subcat_hash,
	isnull(property_key,'') as property_key,
	count(distinct a.date_id) as raw_usage_days, 
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total_Ecosystem_Content a
inner join rps.panelist_platform_qual_day c
	on a.panelist_key = c.panelist_key
	and a.date_id = c.date_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where a.date_id is not null
	and (report_level = 2  --category
	or report_level = 1  --subcat
	or report_level = 3 --total
	or report_level = 8 --total content
	or report_level = 0) --property
	and a.date_id is not null
group by c.country, a.content_cat_id, a.panelist_key, platform_id, digital_type_id, a.date_id, cat_hash, subcat_hash, property_key, report_level ;

----------------------------------------------
---use the below for ALTs. If you have more than one ALT, copy paste below, and change content_cat_id to respective content_cat_id from #content_cat_lookup
----------------------------------------------

insert into #temp_person_daily_cat_total
select 
	c.country,	
	a.panelist_key, 
	platform_id, 
	digital_type_id, 
	cast(1 as int) as tax_version_id, 
	report_level,
	XXXX as content_cat_id, --- insert in Alt content cat id  
	a.date_id,
	isnull(cat_hash,'') as cat_hash,
	isnull(subcat_hash,'') as subcat_hash,
	isnull(property_key,'') as property_key,
	count(distinct a.date_id) as raw_usage_days, 
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total_Ecosystem_Content a
inner join rps.panelist_platform_qual_day c
	on a.panelist_key = c.panelist_key
	and a.date_id = c.date_id
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where a.date_id is not null
	and (report_level = 2  --category
	or report_level = 1  --subcat
	or report_level = 3 --total
	or report_level = 8 --total content
	or report_level = 0) --property
	and a.content_cat_id in ()   -- INSERT IN CONTENT_CAT_ID for the contents you are rolling into the new alt
	and a.date_id is not null
group by c.country, a.panelist_key, platform_id, digital_type_id, a.date_id, cat_hash, subcat_hash, property_key, report_level ;

---------------------------------------------------------------------------------------------------------------
--AGGREGATION OF DAILY USAGE, ADDING IN TOTAL DIGITAL AND ALL DEVICES LINES
---------------------------------------------------------------------------------------------------------------
-- check that cats sum to total line
-- Create 0-0 group for cat and total

-- START HERE IF NO ALTS FOR PROJECT (skips steps in rows 545 - 600);

begin try drop table #temp_person_agg_cat_total end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level,content_cat_id, Date_ID, cat_hash, SubCat_Hash, property_key,
	count(distinct Date_ID) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_person_agg_cat_total
from  #temp_person_daily_cat_total
group by country, panelist_key, platform_id, digital_type_id, report_level, content_cat_id, Date_ID, cat_hash, SubCat_Hash, property_key;


--ALL DEVICES
insert into #temp_person_agg_cat_total
select country, panelist_key, 0 as platform_id, digital_type_id, report_level, content_cat_id, Date_ID, cat_hash, SubCat_Hash, property_key,
	count(distinct Date_ID) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from  #temp_person_daily_cat_total
group by country, panelist_key, digital_type_id, report_level, content_cat_id, Date_ID, cat_hash, SubCat_Hash, property_key;


--ALL DIGITAL TYPE
insert into #temp_person_agg_cat_total
select country, panelist_key, platform_id, 0 as digital_type_id, report_level, content_cat_id, Date_ID, cat_hash, SubCat_Hash, property_key,
	count(distinct Date_ID) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from  #temp_person_daily_cat_total
group by  country, panelist_key, platform_id, report_level, content_cat_id, Date_ID, cat_hash, SubCat_Hash, property_key;


--ALL DEVICES, ALL DIGITAL
insert into #temp_person_agg_cat_total
select country, panelist_key, 0 as platform_id, 0 as digital_type_id, report_level, content_cat_id, Date_ID, cat_hash, SubCat_Hash, property_key,
	count(distinct Date_ID) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from  #temp_person_daily_cat_total
group by country, panelist_key, report_level, content_cat_id, Date_ID, cat_hash, SubCat_Hash, property_key;




-- ############################
-- PERMANENT TABLE CREATED HERE
-- ############################
-- STEP 4B) STAGE A PERSON LEVEL VERSION OF ECOSYSTEM CAT/SUBCAT DAILY DATA FOR UNIFIED PULL
---------------------------------------------------------------
begin try drop table rpt.Person_Ecosystem_Category end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, content_cat_id, Date_ID, cat_hash, SubCat_Hash, Property_Key,raw_usage_days, raw_visits, raw_minutes
into rpt.Person_Ecosystem_Category
from #temp_person_agg_cat_total;



---------------------------------------------------------------
--AGGREGATE TO PROPERTY, SUBCATEGORY, CATEGORY LEVEL 
---------------------------------------------------------------
begin try drop table #temp_panel_agg_cat_total end try begin catch end catch;

select country, a.platform_id, a.Digital_Type_ID, report_level, content_cat_id, cat_hash, SubCat_Hash, property_key,
	count(distinct a.panelist_key) as raw_persons,
	sum(raw_usage_days) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_panel_agg_cat_total
from  #temp_person_agg_cat_total a
group by country, a.platform_id, a.digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, property_key;
--select top 100 * from #temp_panel_agg_cat_total where platform_id = 1 and digital_type_id = 0 and property_key = 0 and cat_hash = 0 and content_cat_id = 3011
-- select * from #temp_panel_agg_cat_total order by country, platform_id, digital_Type_id, report_level, content_Cat_id;
-- (60665 rows affected)


-- ############################
-- PERMANENT TABLE CREATED HERE
-- ############################

-- FINALIZE STANDARD METRICS DAILY USAGE

begin try drop table rpt.Ecosystem_Standard_Metrics_DU end try begin catch end catch;

select country, platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, property_key,
	raw_persons as du, raw_usage_days as du_usedays, raw_visits as du_visits, raw_minutes as du_minutes
into rpt.Ecosystem_Standard_Metrics_DU
from #temp_panel_agg_cat_total;
-- (60665 rows affected)



create index Standard_Metrics_DU1 on rpt.Ecosystem_Standard_Metrics_DU(platform_id);
create index Standard_Metrics_DU2 on rpt.Ecosystem_Standard_Metrics_DU(digital_type_id);
create index Standard_Metrics_DU3 on rpt.Ecosystem_Standard_Metrics_DU(report_level);
create index Standard_Metrics_DU4 on rpt.Ecosystem_Standard_Metrics_DU(cat_hash);
create index Standard_Metrics_DU5 on rpt.Ecosystem_Standard_Metrics_DU(SubCat_Hash);
create index Standard_Metrics_DU6 on rpt.Ecosystem_Standard_Metrics_DU(property_key);




---------------------------------------------------------------------------------------------------
-- STEP 3) CREATE USAGE WEEKS
---------------------------------------------------------------------------------------------------
-- select top 100 * from rps.UserByDay_Total_Ecosystem;
-- select top 100 * from #person_qualified_week;  --USES A WEEKLY QUAL TABLE INSTEAD OF DAILY


-----------------------------------------
--TOPLINE 
-----------------------------------------
begin try drop table #temp_person_qual_week_used end try begin catch end catch;

select 
	country, 
	a.panelist_key, 
	platform_id, 
	digital_type_id,
	cast(1 as int) as tax_version_id, 
	report_level,
	0 as content_Cat_id,
	a.week_id, 
	isnull(cat_hash,'') as cat_hash, 
	isnull(SubCat_Hash,'') as SubCat_Hash, 
	isnull(property_key,'') as property_key,
	1 as wu_useweeks, sum(num_uses) as raw_visits, 
	cast(sum(Dur_Minutes) as numeric(30,5)) as raw_minutes
into #temp_person_qual_week_used
from rps.UserByDay_Total_Ecosystem a
inner join rps.panelist_platform_qual_week c
	on a.panelist_key=c.panelist_key
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
	and a.week_id = c.week_id
WHERE Date_ID is not null
group by country, a.panelist_key, a.week_id, platform_id, digital_type_id, a.report_level, cat_hash, SubCat_Hash, property_key;
-- select * from #temp_person_qual_week_used;
-- (27359 rows affected)



-----------------------------------------
--- CONTENT PULLS
-----------------------------------------

insert into #temp_person_qual_week_used
select 
	country,
	a.panelist_key, 
	platform_id, 
	digital_type_id, 
	cast(1 as int) as tax_version_id, 
	report_level,
	a.content_cat_id, 
	a.Week_ID,
	isnull(cat_hash,'') as cat_hash,
	isnull(subcat_hash,'') as subcat_hash,
	isnull(property_key,'') as property_key,
	count(distinct a.date_id) as raw_usage_days, 
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total_Ecosystem_Content a
inner join rps.panelist_platform_qual_week c
	on a.panelist_key = c.panelist_key
	and a.Week_ID = c.Week_ID
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where a.date_id is not null
	and (report_level = 2  --category
	or report_level = 1  --subcat
	or report_level = 3 --total
	or report_level = 8 --total content
	or report_level = 0) --property
group by country, a.content_cat_id, a.Week_ID, a.panelist_key, platform_id, digital_type_id, cat_hash, subcat_hash, property_key, report_level ;


--------------------------
--- ALTS ---
--------------------------------------
insert into #temp_person_qual_week_used
select 
	country,
	a.panelist_key, 
	platform_id, 
	digital_type_id, 
	cast(1 as int) as tax_version_id, 
	report_level,
	XXXX as content_cat_id, --- insert in Alt content cat id  
	a.Week_ID,
	isnull(cat_hash,'') as cat_hash,
	isnull(subcat_hash,'') as subcat_hash,
	isnull(property_key,'') as property_key,
	count(distinct a.date_id) as raw_usage_days, 
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total_Ecosystem_Content a
inner join rps.panelist_platform_qual_week c
	on a.panelist_key = c.panelist_key
	and a.Week_ID = c.Week_ID
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where a.date_id is not null
	and (report_level = 2  --category
	or report_level = 1  --subcat
	or report_level = 3 --total
	or report_level = 8 --total content
	or report_level = 0) --property
	and a.content_cat_id in ()   -- INSERT IN CONTENT_CAT_ID for the contents you are rolling into the new alt
group by country, a.Week_ID, a.panelist_key, platform_id, digital_type_id, cat_hash, subcat_hash, property_key, report_level ;

----------------------------------------------------------------------------
--PANELIST WEEKLY AGGREGATION ADDING IN ALL DIGITAL AND ALL DEVICE LINES
----------------------------------------------------------------------------

begin try drop table #temp_person_week_agg_cat_total end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, content_cat_id, week_id, cat_hash, SubCat_Hash, property_key,
	count(distinct week_id) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_person_week_agg_cat_total
from  #temp_person_qual_week_used
group by country, panelist_key, platform_id, digital_type_id, report_level, content_cat_id, week_id, cat_hash, SubCat_Hash, property_key;


--all devices
insert into #temp_person_week_agg_cat_total
select country, panelist_key, 0 as platform_id, digital_type_id, report_level, content_cat_id, week_id, cat_hash, SubCat_Hash, property_key,
	count(distinct week_id) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from  #temp_person_qual_week_used
group by country, panelist_key, digital_type_id, report_level, content_cat_id, week_id, cat_hash, SubCat_Hash, property_key;


--all digital type
insert into #temp_person_week_agg_cat_total
select country, panelist_key, platform_id, 0 as digital_type_id, report_level, content_cat_id, week_id, cat_hash, SubCat_Hash, property_key,
	count(distinct week_id) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from  #temp_person_qual_week_used
group by country, panelist_key, platform_id, report_level, content_cat_id, week_id, cat_hash, SubCat_Hash, property_key;


--ti, all digital type, all devices
insert into #temp_person_week_agg_cat_total
select country, panelist_key, 0 as platform_id, 0 as digital_type_id, report_level, content_cat_id, week_id, cat_hash, SubCat_Hash, property_key,
	count(distinct week_id) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from  #temp_person_qual_week_used
group by country, panelist_key, report_level, content_cat_id, week_id, cat_hash, SubCat_Hash, property_key;



--STAGE A PERSON LEVEL VERSION usage weeks OF ECOSYSTEM CATEGORY FOR UNIFIED PULL
---------------------------------------------------------------

begin try drop table rpt.Person_Ecosystem_Category_usage_weeks end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, content_cat_id, week_id, 
	cat_hash, SubCat_Hash, Property_Key, wu_useweeks, raw_visits, raw_minutes
into rpt.Person_Ecosystem_Category_Usage_Weeks
from #temp_person_week_agg_cat_total;


---------------------------------------
--PANEL AGGREGATE FOR WEEKLY USAGE
---------------------------------------
-- drop table #temp_panel_week_agg_cat_total;
begin try drop table #temp_panel_week_agg_cat_total end try begin catch end catch;

select country, a.platform_id, a.digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, property_key,
	count(distinct a.panelist_key) as wu,
	sum(wu_useweeks) as wu_useweeks,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_panel_week_agg_cat_total
from  #temp_person_week_agg_cat_total a
group by country, a.platform_id, a.digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, property_key;

---------------------------------------

begin try drop table #standard_metrics_wu_useweeks end try begin catch end catch

select country, platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, property_key, wu_useweeks, wu
into #standard_metrics_wu_useweeks
from #temp_panel_week_agg_cat_total
group by country, platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, property_key, wu_useweeks, wu;


-------------------------------
-- USAGE MONTH
-------------------------------
-- select * from #temp_person_qual_month_used where month_id is null;
-- select count(*) from #temp_person_qual_month_used; 

-----------------------------------------
--TOPLINE 
-----------------------------------------
begin try drop table #temp_person_qual_month_used end try begin catch end catch;

select 
	country,
	a.panelist_key, 
	platform_id, 
	digital_type_id,
	cast(1 as int) as tax_version_id,  
	report_level,
	0 as content_cat_id,
	a.Month_ID,	
	isnull(cat_hash,'') as cat_hash, 
	isnull(SubCat_Hash,'') as SubCat_Hash, 
	isnull(property_key,'') as property_key,
	1 as mu_usemonths, 
	sum(num_uses) as raw_visits, 
	cast(sum(Dur_Minutes) as numeric(30,5)) as raw_minutes
into #temp_person_qual_month_used
from rps.UserByDay_Total_Ecosystem a
inner join rps.panelist_platform_qual_month c
	on a.panelist_key=c.panelist_key
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
	and a.month_id = c.month_id
left join Ref.Panelist p 
	on a.panelist_key=p.panelist_key
WHERE Date_ID is not null
group by country, a.panelist_key, a.month_id, platform_id, digital_type_id, report_level, cat_hash, SubCat_Hash, property_key;

-----------------------------------------
--- CONTENT PULLS
-----------------------------------------

insert into #temp_person_qual_month_used
select 
	country,
	a.panelist_key, 
	platform_id, 
	digital_type_id, 
	cast(1 as int) as tax_version_id, 
	report_level,
	a.content_cat_id, 
	a.Month_ID,
	isnull(cat_hash,'') as cat_hash,
	isnull(subcat_hash,'') as subcat_hash,
	isnull(property_key,'') as property_key,
	count(distinct a.date_id) as raw_usage_days, 
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total_Ecosystem_Content a
inner join rps.panelist_platform_qual_month c
	on a.panelist_key = c.panelist_key
	and a.Month_ID = c.Month_ID
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where a.date_id is not null
	and (report_level = 2  --category
	or report_level = 1  --subcat
	or report_level = 3 --total
	or report_level = 8 --total content
	or report_level = 0) --property
group by country, a.content_cat_id, a.Month_ID, a.panelist_key, platform_id, digital_type_id, cat_hash, subcat_hash, property_key, report_level ;


--------------------------
--- ALTS ---
--------------------------
insert into #temp_person_qual_month_used
select 
	country,
	a.panelist_key, 
	platform_id, 
	digital_type_id, 
	cast(1 as int) as tax_version_id, 
	report_level,
	XXXX as content_cat_id, --- insert in Alt content cat id
	a.Month_ID,
	isnull(cat_hash,'') as cat_hash,
	isnull(subcat_hash,'') as subcat_hash,
	isnull(property_key,'') as property_key,
	count(distinct a.date_id) as raw_usage_days, 
	sum(num_uses) as raw_visits, 
	cast(sum(dur_minutes) as numeric(30,5)) as raw_minutes
from rps.UserByDay_Total_Ecosystem_Content a
inner join rps.panelist_platform_qual_month c
	on a.panelist_key = c.panelist_key
	and a.Month_ID = c.Month_ID
	and (a.platform_id = c.platform_id_mobile or a.platform_id = c.platform_id_pc)
where a.date_id is not null
	and (report_level = 2  --category
	or report_level = 1  --subcat
	or report_level = 3 --total
	or report_level = 8 --total content
	or report_level = 0) --property
	and a.content_cat_id in ()   -- INSERT IN CONTENT_CAT_ID for the contents you are rolling into the new alt
group by country, a.Month_ID, a.panelist_key, platform_id, digital_type_id, cat_hash, subcat_hash, property_key, report_level ;


-------------------------------------------------------------
--PANELIST MONTHLY AGGREGATION FOR ALL DIGITAL ALL DEVICES
-------------------------------------------------------------

begin try drop table #temp_person_month_agg_cat_total end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, content_cat_id, month_id, cat_hash, SubCat_Hash, property_key,
	count(distinct month_id) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_person_month_agg_cat_total
from  #temp_person_qual_month_used
group by country, panelist_key, platform_id, digital_type_id, report_level, content_cat_id, month_id, cat_hash, SubCat_Hash, property_key;


--all devices
insert into #temp_person_month_agg_cat_total
select country,  panelist_key, 0 as platform_id, digital_type_id, report_level, content_cat_id, month_id, cat_hash, SubCat_Hash, property_key,
	count(distinct month_id) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from  #temp_person_qual_month_used
group by country, panelist_key, digital_type_id, report_level, content_cat_id, month_id, cat_hash, SubCat_Hash, property_key;


--all digital by platform_id
insert into #temp_person_month_agg_cat_total
select country, panelist_key, platform_id, 0 as digital_type_id, report_level, content_cat_id, month_id, cat_hash, SubCat_Hash, property_key,
	count(distinct month_id) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from  #temp_person_qual_month_used
group by country, panelist_key, platform_id, report_level, content_Cat_id, month_id, cat_hash, SubCat_Hash, property_key;


--ti, all digital type, all devices
insert into #temp_person_month_agg_cat_total
select country, panelist_key, 0 as platform_id, 0 as digital_type_id, report_level, content_cat_id, month_id, cat_hash, SubCat_Hash, property_key,
	count(distinct month_id) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from  #temp_person_qual_month_used
group by country, panelist_key, report_level, content_cat_id, month_id, cat_hash, SubCat_Hash, property_key;



-- STAGE A PERSON LEVEL VERSION USAGE MONTHS OF ECOSYSTEM CATEGORY FOR UNIFIED PULLS
---------------------------------------------------------------
begin try drop table rpt.Person_Ecosystem_Category_Usage_Months end try begin catch end catch;

select country, panelist_key, platform_id, digital_type_id, report_level, content_cat_id,
	month_id, cat_hash, SubCat_Hash, Property_Key, mu_usemonths, raw_visits, raw_minutes
into rpt.Person_Ecosystem_Category_Usage_Months
from #temp_person_month_agg_cat_total;



-------------------------------------------
--PANEL AGGREGATION FOR MONTHLY USAGE
-------------------------------------------
-- drop table #temp_panel_week_agg_cat_total;
-- select * from #temp_panel_month_agg_cat_total where mu_usemonths is null;
-- select * from #standard_metrics_mu_usemonths where mu_usemonths is null;

begin try drop table #temp_panel_month_agg_cat_total end try begin catch end catch;

select country, a.platform_id, a.digital_type_id, report_level, a.content_cat_id, cat_hash, SubCat_Hash, property_key,
	count(distinct a.panelist_key) as mu,
	sum(mu_usemonths) as mu_usemonths,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_panel_month_agg_cat_total
from  #temp_person_month_agg_cat_total a
group by country, a.platform_id, a.digital_type_id, report_level, a.content_cat_id, cat_hash, SubCat_Hash, property_key;

---------------------------------------

begin try drop table #standard_metrics_mu_usemonths end try begin catch end catch;

select country, platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, 
	property_key, mu_usemonths, mu
into #standard_metrics_mu_usemonths
from #temp_panel_month_agg_cat_total
group by  country, platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, property_key, mu_usemonths, mu;


-- BE SURE WEEK AND MONTH USAGE ARE INDEXED
create index standard_metrics_wu_useweeks1 on #standard_metrics_wu_useweeks(platform_id);
create index standard_metrics_wu_useweeks2 on #standard_metrics_wu_useweeks(digital_type_id);
create index standard_metrics_wu_useweeks3 on #standard_metrics_wu_useweeks(cat_hash);
create index standard_metrics_wu_useweeks4 on #standard_metrics_wu_useweeks(SubCat_Hash);
create index standard_metrics_wu_useweeks5 on #standard_metrics_wu_useweeks(property_key);
create index standard_metrics_wu_useweeks6 on #standard_metrics_wu_useweeks(report_level);

create index standard_metrics_mu_usemonths1 on #standard_metrics_mu_usemonths(platform_id);
create index standard_metrics_mu_usemonths2 on #standard_metrics_mu_usemonths(digital_type_id);
create index standard_metrics_mu_usemonths3 on #standard_metrics_mu_usemonths(cat_hash);
create index standard_metrics_mu_usemonths4 on #standard_metrics_mu_usemonths(SubCat_Hash);
create index standard_metrics_mu_usemonths5 on #standard_metrics_mu_usemonths(property_key);
create index standard_metrics_mu_usemonths6 on #standard_metrics_mu_usemonths(report_level);




---------------------------------------------------------------------------------------------------
-- STEP 4) INTEGRATE EVERYTHING... FO SHO
---------------------------------------------------------------------------------------------------

-- drop table #temp_total_internet_minutes
begin try drop table #temp_total_internet_minutes end try begin catch end catch
select country, platform_id, content_cat_id, digital_type_id, du_minutes, du_visits
into #temp_total_internet_minutes
from rpt.Ecosystem_Standard_Metrics_DU 
where report_level=3;


--NEED TO PULL IN TOPLINE TI NUMBERS FOR THE CONTENT CATEOGRIES, LEVEL 8  **CAN REMOVE IF NO ALTS
insert into #temp_total_internet_minutes
select country, platform_id, content_cat_id, digital_type_id, du_minutes, du_visits
from rpt.Ecosystem_Standard_Metrics_DU
where report_level=8; 


create index temp_total_internet_minutes1 on #temp_total_internet_minutes(platform_id);
create index temp_total_internet_minutes2 on #temp_total_internet_minutes(digital_type_id);

begin try drop table #temp_cat_du_visits end try begin catch end catch
select country, platform_id, content_cat_id, digital_type_id, sum(du_visits) as du_visits
into #temp_cat_du_visits
from rpt.Ecosystem_Standard_Metrics_DU 
where report_level=1
group by country, platform_id, content_cat_id, digital_type_id;



begin try drop table #temp_prop_du_visits end try begin catch end catch
select country, platform_id, content_cat_id, digital_type_id, sum(du_visits) as du_visits
into #temp_prop_du_visits
from rpt.Ecosystem_Standard_Metrics_DU 
where report_level=0
group by country, platform_id, content_cat_id, digital_type_id;



-- when you get here, and you have a table output, say "boo ya!"
begin try drop table #temp_standard_metrics_booya end try begin catch end catch

select 
	a.country,
	1 as tax_version_id, 
	a.platform_id, 
	a.digital_type_id, 
	a.report_level, 
	a.content_cat_id,
	a.cat_hash, 
	a.SubCat_Hash, 
	a.property_key,
	p.dp, 
	a.du, 
	p.dp_qualdays, 
	a.du_usedays, 
	a.du_visits,
	cast(a.du_minutes as numeric(30,2)) as du_mins,
	cast(ti.du_minutes as numeric(30,2)) as du_mins_ti,
	--cast(cat_ti.du_visits as numeric(30,2)) as du_visits_ti,
	case when a.cat_hash = 0 then cast(ti.du_visits as numeric(30,2)) else cast(cat_ti.du_visits as numeric(30,2)) end as du_visits_ti, 
	p.wp_qualweeks, 
	isnull(wu.wu_useweeks,0) as wu_useweeks,
	p.mp_qualmonths, 
	isnull(mu.mu_usemonths,0) as mu_usemonths
into #temp_standard_metrics_booya
from rpt.Ecosystem_Standard_Metrics_DU a    --DAILY NUMBERS
inner join rps.device_topline_qual p 			--PULLS TOPLINE QUAL NUMBERS
	on a.platform_id=p.platform_id
	and a.country=p.country
inner join #temp_total_internet_minutes ti  --PULLS TI MINUTES FOR DENOMINATOR OF SHARE CALC
	on a.platform_id=ti.platform_id
	and a.digital_type_id=ti.digital_type_id
	and a.content_cat_id = ti.content_cat_id
	and a.country=ti.country
inner join #temp_cat_du_visits cat_ti
	on a.platform_id=cat_ti.platform_id
	and a.digital_type_id=cat_ti.digital_type_id
	and a.content_cat_id = cat_ti.content_cat_id
	and a.country=cat_ti.country
left outer join #standard_metrics_wu_useweeks wu  --WEEKLY NUMBERS
	on a.platform_id=wu.platform_id
	and a.digital_type_id=wu.digital_type_id
	and a.report_level=wu.report_level
	and a.cat_hash=wu.cat_hash
	and a.SubCat_Hash=wu.SubCat_Hash
    and a.property_key = wu.property_key 
	and a.content_cat_id = wu.content_cat_id
	and a.country=wu.country
left outer join #standard_metrics_mu_usemonths mu  --MONTHLY NUMBERS
	on a.platform_id=mu.platform_id
	and a.digital_type_id=mu.digital_type_id
	and a.report_level=mu.report_level
	and a.cat_hash=mu.cat_hash
	and a.SubCat_Hash=mu.SubCat_Hash
	and a.property_key=mu.property_key
	and a.content_cat_id = mu.content_cat_id
	and a.country=mu.country
where a.report_level != 0; -- Excludes Property level from reporting



---------------------------------------------
-- ############################
-- PERMANENT TABLE CREATED HERE
-- ############################
-- MAKING THE DB ECOSYSTEM CATEGORY SUBCATEGORY TABLE
---------------------------------------------
--ADDING IN A TOTAL CONTENT REPORT LEVEL SINCE IT DOESN'T EXIST IN THE REF.VARIABLE_NAMES DB TABLE


insert into #variable_names select 6, 'ReportLevel', 8, 'Total Content';
-- select  * from #variable_names

begin try drop table rpt.Ecosystem_Standard_Metrics_Report end try begin catch end catch;
select
	a.country,
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	x.tax_version, 
	g.val_name as report_level,
	cc.content_category_name, 
	e.category, 
	f.subcategory,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	du_visits_ti, 
	wp_qualweeks, 
	isnull(wu_useweeks,0) as wu_useweeks, 
	mp_qualmonths, 
	isnull(mu_usemonths,0) as mu_usemonths
into rpt.Ecosystem_Standard_Metrics_Report
from #temp_standard_metrics_booya a
left outer join #variable_names c   
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
left outer join #cat_id_map e
	on a.cat_hash = e.cat_id
left outer join #subcat_id_map f
	on a.subcat_hash = f.subcat_id
inner join #variable_names g
	on a.report_level=g.val_id
	and g.var_name = 'reportlevel'
inner join #tax_version_lookup x
	on a.tax_version_id=x.tax_version_id
inner join #content_cat_lookup cc
	on a.content_cat_id = cc.content_cat_id
;


--INSERTING AN ALL DEVICES TI LINE
INSERT INTO rpt.Ecosystem_Standard_Metrics_Report
select 
	a.country,
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	x.tax_version, 
	g.val_name as report_level, 
	cc.content_category_name,
	'* Total Ecosystem * ' as category, 
	'* Total Ecosystem * ' as subcategory,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	du_visits_ti,
	wp_qualweeks, 
	isnull(wu_useweeks,0) as wu_useweeks, 
	mp_qualmonths, 
	isnull(mu_usemonths,0) as mu_usemonths
from #temp_standard_metrics_booya a
left outer join [Ref].[Variable_Names] c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
inner join #variable_names g
	on a.report_level=g.val_id
	and g.var_name = 'reportlevel'
inner join #tax_version_lookup x
	on a.tax_version_id=x.tax_version_id
inner join #content_Cat_lookup cc
	on a.content_cat_id = cc.content_cat_id
WHERE a.report_level in (3,8)
;



--------------------------------------------
--BEGIN BUILDING FOR PROPERTY SECTION
--COULD COLLAPSE THIS INTO LESS STEPS OR ADD INTO CAT/SUBCAT SECTION AND JUST EXPORT BASED ON REPORT LEVEL
--WAS SEPARATED BEFORE BC OF DOMAIN VS APP CLASSIFICATION BUT NOT NEEDED ANY MORE? 
--#temp_standard_metrics_hip IS BASICALLY THE SAME AS BOOYA
--------------------------------------------
-- select top 100 * from #temp_standard_metrics_hip;
-- select count(*) from #temp_standard_metrics_hip; 

begin try drop table #temp_standard_metrics_hip end try begin catch end catch;

select 
	a.country,
	1 as tax_version_id, 
	a.platform_id, 
	a.digital_type_id, 
	a.report_level, 
	a.content_cat_id,
	a.cat_hash, 
	a.SubCat_Hash, 
	a.property_key,
	p.dp, 
	a.du, 
	p.dp_qualdays, 
	a.du_usedays, 
	a.du_visits,
	cast(a.du_minutes as numeric(30,2)) as du_mins,
	cast(ti.du_minutes as numeric(30,2)) as du_mins_ti,
	--cast(prop_ti.du_visits as numeric(30,2)) as du_visits_ti,
	case when a.cat_hash = 0 then cast(ti.du_visits as numeric(30,2)) else cast(prop_ti.du_visits as numeric(30,2)) end as du_visits_ti, 
	p.wp_qualweeks, 
	isnull(wu.wu_useweeks,0) as wu_useweeks, 
	p.mp_qualmonths,
	isnull(mu.mu_usemonths,0) as mu_usemonths,
	rpt_flag
into #temp_standard_metrics_hip
from rpt.Ecosystem_Standard_Metrics_DU a    --DAILY 
inner join rps.device_topline_qual p --PULLS TOPLINE QUAL NUMBERS
	on a.platform_id=p.platform_id
	and a.country=p.country
inner join #temp_total_internet_minutes ti --PULLS TI MINUTES FOR DENOMINATOR OF SHARE CALC
	on a.platform_id=ti.platform_id
	and a.digital_type_id=ti.digital_type_id
	and a.content_cat_id = ti.content_cat_id
	and a.country=ti.country
inner join #temp_prop_du_visits prop_ti
	on a.platform_id=prop_ti.platform_id
	and a.digital_type_id=prop_ti.digital_type_id
	and a.content_cat_id = prop_ti.content_cat_id
	and a.country=prop_ti.country
left outer join #standard_metrics_wu_useweeks wu  --WEEKLY 
	on a.platform_id=wu.platform_id
	and a.digital_type_id=wu.digital_type_id
	and a.report_level=wu.report_level
	and a.cat_hash=wu.cat_hash
	and a.SubCat_Hash=wu.SubCat_Hash
	and a.property_key=wu.property_key
	and a.content_cat_id = wu.content_cat_id
	and a.country=wu.country
left outer join #standard_metrics_mu_usemonths mu  --MONTHLY
	on a.platform_id=mu.platform_id
	and a.digital_type_id=mu.digital_type_id
	and a.report_level=mu.report_level
	and a.cat_hash=mu.cat_hash
	and a.SubCat_Hash=mu.SubCat_Hash
	and a.property_key=mu.property_key
	and a.content_cat_id = mu.content_cat_id
	and a.country=mu.country
left outer join #taxonomy_common_prop_lookup cp
	on a.property_key=cp.property_key
	and a.Digital_Type_ID=cp.digital_Type_id
	and a.digital_type_id <> 0
	and rpt_flag=1
where a.report_level = 0 --property ONLY 
;



create index #temp_standard_metrics_hip1 on #temp_standard_metrics_hip(platform_id);
create index #temp_standard_metrics_hip2 on #temp_standard_metrics_hip(digital_type_id);
create index #temp_standard_metrics_hip3 on #temp_standard_metrics_hip(cat_hash);
create index #temp_standard_metrics_hip4 on #temp_standard_metrics_hip(SubCat_Hash);
create index #temp_standard_metrics_hip5 on #temp_standard_metrics_hip(property_key);
create index #temp_standard_metrics_hip6 on #temp_standard_metrics_hip(report_level);


begin try drop table #standard_metrics_prop_prep end try begin catch end catch;

SELECT
	a.country,
	a.digital_type_id,
	a.platform_id,
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	a.tax_version_id,
	cc.content_category_name,
	e.category, 
	f.subcategory,  
	g.bdg_display,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	du_visits_ti,
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
INTO #standard_metrics_prop_prep
FROM #temp_standard_metrics_hip a
left outer join #variable_names c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
left outer join #cat_id_map e
	on a.cat_hash = e.cat_id
left outer join #subcat_id_map f
	on a.subcat_hash = f.subcat_id
inner join #property_map g
	on a.Property_Key = g.bdg_display_key
inner join #content_cat_lookup cc
	on a.content_cat_id = cc.content_cat_id
;



-------------------------------------------------------
-- ############################
-- PERMANENT TABLE CREATED HERE
-- ############################
--CREATING DB PROPERTY TABLE
--COULD JOIN TO A WHITELIST DOMAIN TABLE TO UPDATE THE WHITELIST FLAG ACCORDINGLY IF EXISTS 
-------------------------------------------------------
-- select top 100 * from [PROJECTNAME].dbo.Standard_Metrics_Property;
-- select top 100 * from [PROJECTNAME].dbo.Standard_Metrics_Property where device_type='All Devices';
-- select count(*) from [PROJECTNAME].dbo.Standard_Metrics_Property;

begin try drop table rpt.Ecosystem_Standard_Metrics_Property end try begin catch end catch;

select  
	country,
	device_type, 
	digital_type, 
	x.tax_version, 
	content_category_name,
	category, 
	subcategory, 
	bdg_display as property,
	0 as whitelist_flag,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	du_visits_ti,
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
into rpt.Ecosystem_Standard_Metrics_Property
from #standard_metrics_prop_prep a
inner join #tax_version_lookup x
	on a.tax_version_id=x.tax_version_id;



 
INSERT INTO rpt.Ecosystem_Standard_Metrics_Property
select 
	a.country,
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	x.tax_version, 
	cc.content_category_name, 
	'* Total Ecosystem * ' as category, 
	'* Total Ecosystem * ' as subcategory,
	'* Total Ecosystem * ' as property,
	1 as whitelist_flag,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	du_visits_ti,
	wp_qualweeks, 
	isnull(wu_useweeks,0) as wu_useweeks, 
	mp_qualmonths, 
	isnull(mu_usemonths,0) as mu_usemonths
from #temp_standard_metrics_booya a
left outer join #variable_names c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
inner join #variable_names g
	on a.report_level=g.val_id
	and g.var_name = 'reportlevel'
inner join #tax_version_lookup x
	on a.tax_version_id=x.tax_version_id
inner join #content_cat_lookup cc
	on a.content_cat_id = cc.content_cat_id
WHERE a.report_level in (3,8)
;


---------------------------------------------------------------------------------------------------
-- STEP 5) EXPORT STANDARD METRICS... GET IT OUT
---------------------------------------------------------------------------------------------------
delete from rpt.Ecosystem_Standard_Metrics_Report where category is null;


-- EXPORT 1
-- This selection exports Total, Cat, & Subcat
select 
	country,
	device_type, 
	digital_type, 
	tax_version,
	case when report_level='Total ' 
			then 'Total' 
			else report_level 
		end as report_level,
	content_category_name,
	case when report_level='Total' 
			then '* Total Ecosystem * ' 
			else case when category='XXXAdult' 
				then 'XXX Adult' 
				else category 
		end end as category,
	subcategory, 
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	du_visits_ti,
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
from rpt.Ecosystem_Standard_Metrics_Report
where report_level in ('Category', 'Subcategory', 'Total', 'Total Content')
order by 
	tax_version, 
	device_type, 
	digital_type, 
	report_level, 
	content_category_name,
	category, 
	subcategory, 
	du desc;






-------------------------------------------
-- EXPORT 2
-- This selection exports Property
select 
	country,
	device_type, 
	digital_type, 
	tax_version, 
	content_category_name,
	category, 
	subcategory, 
	property,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	du_visits_ti,
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths,
	rpt_flag
from rpt.Ecosystem_Standard_Metrics_Property
where (du>2 or whitelist_flag=1)   --LIMITING TO ONLY PROPERTIES THAT HAVE MORE THAN 2 DAILY USERS OR WHITELISTED SITES
order by 
	tax_version, 
	device_type, 
	digital_type,
	content_category_name,
	category, 
	subcategory, 
	property, 
	du desc;

	-- select top 100 * from [PROJECTNAME].rpt.Ecosystem_Standard_metrics_property where property like '%otriven%';

-- ############################
-- SUMMARY OF PERMANENT TABLES
-- ############################
-- select top 100 * from [PROJECTNAME].dbo.Standard_Metrics_DU;
-- select top 100 * from [PROJECTNAME].dbo.Standard_Metrics_Report;
-- select top 100 * from [PROJECTNAME].dbo.Standard_Metrics_Property;

-- END OF FILE --