-----------------------
---Computes monthly reach for indexing
-----------------------

use PROJECTNAME;

---------Declare the segmentation name for the group indexing against (usually total pop or total survey)
declare @index_ref_pop varchar(300) = 'Total Survey';

--select top 100 * from rpt.Unified_Standard_Metrics_Property;
------------------Reference pop Column
-------First gets daily reach
-- declare @index_ref_pop varchar(300) = 'Total Survey'
begin try drop table #reference_group_daily_reach end try begin catch end catch;
select *, 
case when dp_qualdays = 0 then cast(0 as float) else cast(du_usedays as float)/cast(dp_qualdays as float) end as reference_group_daily_reach
into #reference_group_daily_reach
from rpt.Unified_Standard_Metrics_Property
where segmentation_name = @index_ref_pop
--and content_category_name !=''; Make sure to check for any dupes or blanks in original table
--(10098 rows affected)

--select top 100 * from #reference_group_daily_reach;

-------Gets inital version of weekly_reach
begin try drop table #reference_group_weekly_reach end try begin catch end catch;
select *, case when wp_qualweeks = 0 then cast(0 as float) else cast(wu_useweeks as float)/cast(wp_qualweeks as float) end as reference_group_weekly_reach
into #reference_group_weekly_reach 
from #reference_group_daily_reach
;
--(10098 rows affected)

--select top 100 * from #reference_group_weekly_reach;

---------Follows same excel logic and makes weekly reach equal to daily reach if daily reach is higher
update #reference_group_weekly_reach set reference_group_weekly_reach = reference_group_daily_reach
where reference_group_daily_reach > reference_group_weekly_reach
;
--(181 rows affected)

---Initial version of monthly reach
begin try drop table #reference_group_monthly_reach end try begin catch end catch;
select *, case when mp_qualmonths = 0 then cast(0 as float) else cast(mu_usemonths as float)/cast(mp_qualmonths as float) end as reference_group_monthly_reach
into #reference_group_monthly_reach
from #reference_group_weekly_reach
;
--(10098 rows affected)

----- Since weekly reach is already >= weekly reach based on previous apply, it only has to update based on weekly reach
update #reference_group_monthly_reach set reference_group_monthly_reach = reference_group_weekly_reach
where reference_group_weekly_reach > reference_group_monthly_reach
;
--(981 rows affected)

/*
--------Gets daily visits per 100 qualified qualified panelists
begin try drop table #reference_group_d_vpq100 end try begin catch end catch;
select *, case when dp_qualdays = 0 then cast(0 as float) 
else cast(100*(du_visits) as float)/cast(dp_qualdays as float) end as reference_group_d_vpq100
into #reference_group_d_vpq100
from #reference_group_monthly_reach
;
--(1224 rows affected)

--- Multiples daily visits per 100 *30 for monthly
begin try drop table #reference_group_m_vpq100 end try begin catch end catch;
select *,30*reference_group_d_vpq100 as reference_group_m_vpq100
into #reference_group_m_vpq100
from #reference_group_d_vpq100
;
--(1224 rows affected)

----------M_vpq100 is compared to the total content level (which includes in itself a total eco content)
begin try drop table #reference_group_visits_share_prep end try begin catch end catch;
select a.*, b.reference_group_m_vpq100 as reference_group_total_m_vpq100
into #reference_group_visits_share_prep
from #reference_group_m_vpq100 a left join #reference_group_m_vpq100 b on 
a.device_type = b.device_type
and a.digital_type = b.digital_type
and a.segmentation_name = b.segmentation_name -- technically not necessary for ref group but just in case
and a.segment_name = b.segment_name -- technically not necessary for ref group but just in case
and a.content_category_name = b.content_category_name
where b.category = '* Total Ecosystem *' --'* Total Internet *' --change depending on if eco or common
;
--(1224 rows affected)

begin try drop table #reference_group_visits_share end try begin catch end catch;
select a.*, case when reference_group_total_m_vpq100 = 0 then cast(0 as float)
else cast(reference_group_m_vpq100 as float)/cast(reference_group_total_m_vpq100 as float) 
end as reference_group_visits_share
into #reference_group_visits_share
from #reference_group_visits_share_prep a
;
--(1224 rows affected)

begin try drop table #reference_group_minutes_share end try begin catch end catch;
select *, case when du_mins_ti = 0 then cast(0 as float) 
else cast(du_mins as float)/cast(du_mins_ti as float) end as reference_group_minutes_share
into #reference_group_minutes_share
from #reference_group_visits_share
;
--(1224 rows affected)
*/

----------Gets for all data points
begin try drop table #all_daily_reach end try begin catch end catch;
select *, case when dp_qualdays = 0 then cast(0 as float) else cast(du_usedays as float)/cast(dp_qualdays as float) end as segment_daily_reach
into #all_daily_reach
from rpt.Unified_Standard_Metrics_Property
--where content_category_name != '' -- again check for any blanks or dupes
;
--(279629 rows affected)

begin try drop table #all_weekly_reach end try begin catch end catch;
select *, case when wp_qualweeks = 0 then cast(0 as float) else cast(wu_useweeks as float)/cast(wp_qualweeks as float) end as segment_weekly_reach
into  #all_weekly_reach
from #all_daily_reach
;
--(279629 rows affected)
--select top 100 * from #all_weekly_reach;

update #all_weekly_reach set segment_weekly_reach = segment_daily_reach
where segment_daily_reach > segment_weekly_reach
;
--(4888 rows affected)

begin try drop table #all_monthly_reach end try begin catch end catch;
select *, case when mp_qualmonths = 0 then cast(0 as float) else cast(mu_usemonths as float)/cast(mp_qualmonths as float) end as segment_monthly_reach
into  #all_monthly_reach
from #all_weekly_reach
;
--(279629 rows affected)

update #all_monthly_reach set segment_monthly_reach = segment_weekly_reach
where segment_weekly_reach > segment_monthly_reach 
;
--(27879 rows affected)

/*
--------Gets daily visits per 100 qualified qualified panelists
begin try drop table #all_d_vpq100 end try begin catch end catch;
select *, case when dp_qualdays = 0 then cast(0 as float) 
else cast(100*(du_visits) as float)/cast(dp_qualdays as float) end as segment_d_vpq100
into #all_d_vpq100
from #all_monthly_reach
;
--(57554 rows affected)

--- Multiples daily visits per 100 *30 for monthly
begin try drop table #all_m_vpq100 end try begin catch end catch;
select *,30*segment_d_vpq100 as segment_m_vpq100
into #all_m_vpq100
from #all_d_vpq100
;
--(57554 rows affected)

----------M_vpq100 is compared to the total content level (which includes in itself a total eco content)
begin try drop table #all_visits_share_prep end try begin catch end catch;
select a.*, b.segment_m_vpq100 as segment_total_m_vpq100
into #all_visits_share_prep
from #all_m_vpq100 a left join #all_m_vpq100 b on 
a.device_type = b.device_type
and a.digital_type = b.digital_type
and a.segmentation_name = b.segmentation_name -- technically not necessary for ref group but just in case
and a.segment_name = b.segment_name -- technically not necessary for ref group but just in case
and a.content_category_name = b.content_category_name
where b.category = '* Total Ecosystem *' --'* Total Internet *' --change depending on if eco or common
;
--(57554 rows affected)


begin try drop table #all_visits_share end try begin catch end catch;
select a.*, case when segment_total_m_vpq100 = 0 then cast(0 as float)
else cast(segment_m_vpq100 as float)/cast(segment_total_m_vpq100 as float) 
end as segment_visits_share
into #all_visits_share
from #all_visits_share_prep a
;
--(57554 rows affected)

begin try drop table #all_minutes_share end try begin catch end catch;
select *, case when du_mins_ti = 0 then cast(0 as float) 
else cast(du_mins as float)/cast(du_mins_ti as float) end as segment_minutes_share
into #all_minutes_share
from #all_visits_share
;
--(57554 rows affected)
*/

------Include all necessary, unique identifiers and relevant data points
--select top 100 * from #reference_group_monthly_reach;
begin try drop table #booya end try begin catch end catch;
select --a.country, 
a.device_type, a.digital_type, a.report_level,a.content_category_name,
a.segmentation_name, a.segment_name, 
a.category, a.subcategory,
a.du, 
a.segment_monthly_reach, b.reference_group_monthly_reach,
--a.segment_minutes_share, b.reference_group_minutes_share,
--a.segment_visits_share, b.reference_group_visits_share
into #booya
from #all_monthly_reach a join #reference_group_monthly_reach b
on --a.country = b.country
--and a.timeframe = b.timeframe
--and a.week_of_year = b.week_of_year
 a.device_type = b.device_type
and a.digital_type = b.digital_type
--and a.tax_version = b.tax_version -----Usually wont need this
and a.report_level = b.report_level
and a.content_category_name = b.content_category_name
--and a.supercategory = b.supercategory
--and a.property = b.property
and a.category = b.category
and a.subcategory = b.subcategory
--and a.vertical = b.vertical
--	and a.segmentation_name = c.segmentation_name ## if the reference group is the same panelists but based on timeframe
--	and a.segment_name = c.segment_name			  ## uncomment these and comment out joins on timeframe/week of year
;
--(57554 rows affected)


begin try drop table #final_index end try begin catch end catch;

begin try drop table #final_index end try begin catch end catch;
select *, case when du < 15 then NULL 
else cast ( 100 * (segment_monthly_reach/reference_group_monthly_reach) as int) end as monthly_reach_index
--case when du < 15 then NULL 
--else cast ( 100 * (segment_minutes_share/reference_group_minutes_share) as int) end as minutes_share_index,
--case when du < 15 then NULL 
--else cast ( 100 * (segment_visits_share/reference_group_visits_share) as int) end as visits_share_index
into #final_index
from #booya;
--(279629 rows affected)

--select top 100 * from #final_index order by monthly_reach_index desc;
begin try drop table rpt.unified_common_indexing end try begin catch end catch;
select * 
into rpt.unified_common_indexing
from #final_index;
----------
--EXPORT
---------
select * 
from  rpt.unified_ecosystem_indexing
--where monthly_reach_index is not null
order by --country, 
device_type, digital_type,
-- supercategory, property, 
 segmentation_name, segment_name,
 content_category_name,report_level,
 category, subcategory
;