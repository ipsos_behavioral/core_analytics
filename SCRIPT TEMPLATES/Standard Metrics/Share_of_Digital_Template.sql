-- OWNER
-- Share of Digital
-- DATE
---------------------------------------------------------------------------------------------------

USE [PROJECTNAME];  --replace this and every instance of PROEJCTNAME in the script with your project schema (CTRL+F for PROJECTNAME and replace)

-- WHAT IS THIS REPORT
------------------------------------------------------------------
-- For several projects now, clients have been looking for a singular view of minutes share and visits
-- share incorporates the ecosystem we build carved out from the rest of the panelists' digital activity.
-- Similar to "Share of Mind" or "Share of Voice", we create a single chart for "Share of "Digital"
-- that blends Ecosystem with the Common Taxonomy.

-- select top 1000 * from run.v_Report_Sequence_Event order by date_id;
-- select top 100 * from run.v_Report_Sequence_Event_Ecosystem;
-- select top 100 * from core.ref.Taxonomy_Common_Properties;
-- select * from core.ref.Taxonomy_Common_Properties where project_m_reach >0.05 order by project_m_reach desc;
-- select * from core.ref.Taxonomy_Common_Properties where property_name = 'Facebook.com';


-- COMBINE COMMON TAXONOMY AND ECOSYSTEM INTO INTEGRATED SEQUENCE
------------------------------------------------------------------
-- select * from run.v_Report_Sequence_Event where platform is null;
-- select * from run.v_Report_Sequence_Event where notes = 'Hidden Objects House Cleaning'
-- select distinct category from run.v_Report_Sequence_Event;
-- select * from run.v_Report_Sequence_Event where notes = 'southpark';
-- select top 1000 * from #sequence_integrated_tax where platform is null;
-- select top 1000 * from #sequence_integrated_tax where best_category is null;

begin try drop table #sequence_integrated_tax end try begin catch end catch;

select record_id, a.Panelist_ID as panelist_key, digital_type, platform, notes as property, 1 as reportable_flag,
	category as best_category, a.subcategory as best_subcategory, category as com_category, dur_seconds, session_id,
	cast('Common' as varchar(30)) as eco_type
into #sequence_integrated_tax
from run.v_Report_Sequence_Event a
where a.notes is not null and (digital_type='APP' or digital_type = 'Web');



create index var1 on #sequence_integrated_tax(record_id);


-- COMMENT OUT THE BELOW SECTION WHEN RUNNING FOR COMMON, BUT ADD BACK IN WHEN YOU WANT THE ECOSYSTEM CUT
/**
update #sequence_integrated_tax
set best_category = 'ECOSYSTEM NAME'  best_subcategory = '', eco_type = 'Ecosystem'
from #sequence_integrated_tax a
inner join  run.v_Report_Sequence_Event_Ecosystem b
	on a.record_id=b.record_id;
	**/

-- AGGREGATE AT PERSON + PROPERTY + SESSION LEVEL
------------------------------------------------------------------
-- ### Note: Error in Mayo Code... We need to split by eco_type, because sites like Amazon can have both eco and non-eco content. ###
-- select distinct best_category from #sequence_integrated_tax;
-- select top 1000 * from #share_session_agg where best_category is null;
-- select top 1000 * from #share_session_agg where property = 'southpark';
-- select distinct best_category from #share_session_agg;
-- select distinct best_category from #sequence_integrated_tax;

begin try drop table #share_session_agg end try begin catch end catch;
select platform, digital_type, panelist_key, eco_type, property, reportable_flag, best_category, best_subcategory, com_category, session_id, cast(sum(dur_seconds)/60.0 as numeric(10,5)) as  dur_minutes
into #share_session_agg
from #sequence_integrated_tax
group by platform, digital_type, panelist_key, eco_type, property, reportable_flag, best_category, best_subcategory, com_category, session_id;


-- AGGREGATE AT PERSON + PROPERTY + SESSION LEVEL
------------------------------------------------------------------
-- select top 100 * from #share_session_prop where platform is null;
-- select top 100 * from #share_session_prop where best_category = '';
-- select top 100 * from #share_session_prop where property = 'Builder Game';
-- drop table #share_session_prop;

begin try drop table #share_session_prop end try begin catch end catch;
select platform, digital_type, property, eco_type, reportable_flag, best_category, best_subcategory, com_category, count(distinct panelist_key) as total_panelists, 
	count(*) as total_visits, sum(dur_minutes) as dur_minutes
into #share_session_prop
from #share_session_agg
group by platform, digital_type, property, eco_type, reportable_flag, best_category, best_subcategory, com_category;



-- TESTING
/***
select * from #share_session_prop
 where com_category in ('Other')
order by total_panelists desc, total_visits desc;

select best_category, count(*) as ct_properties, sum(dur_minutes) as dur_minutes from #share_session_prop group by best_category order by 1;
****/

-- REMAPPING
------------------------------------------------------------------
-- REMAP ICATEGORIES TO EASIER TO UNDERSTAND GROUPINGS
-- ### Note: Error in Mayo Code... We need to split by eco_type, because sites like Amazon can have both eco and non-eco content. ###
-- NON-REPORTED
-- Traffic that is legit and representative, but we don't want it called out as a single line item.
------------------------------------------------------------------
-- update #share_session_prop	set best_category = 'Entertainment/Games', best_subcategory='XXX Adult', reportable_flag=0 from #share_session_prop where com_category in ('XXXAdult', 'XXX Adult');
-- Add in other non-reportable domains. 
-- update #share_session_prop set reportable_flag=0 from #share_session_prop where property in ( ) and eco_type='Common';


-- UPDATE WITH CORE COMMON TAXONOMY
-- select top 1000 * from core.ref.Taxonomy_Common_Properties order by project_m_reach desc;
-- select distinct common_supercategory from core.ref.Taxonomy_Common_Properties;
-- select * from #share_session_prop where best_category = 'utility/other'
-- select * from #share_session_prop where property = 'Hidden Objects House Cleaning';
update #share_session_prop
set best_category = b.common_supercategory, best_subcategory = b.common_category,
reportable_flag = isnull(a.reportable_flag*b.rpt_flag, 0) -- in case either source are non-reportable
from #share_session_prop a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name 
where eco_type='Common';
-- 40140 rows affected

-- Unclassified best category:
update #share_session_prop	set best_category = '[TBD]', best_subcategory = '[Alert: TBD]' from #share_session_prop where (best_category is null) and eco_type='Common';

/** QA CHECK
select a.*
from #share_session_prop a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where b.property_name is null and best_category not like '%Alert%' ; 

best_category not like '[%]' and eco_type='Common';
**/

--- FINAL AGG TO CATEGORY LEVEL
begin try drop table #share_best_subcategory end try begin catch end catch;

select platform, digital_type, best_category, best_subcategory, sum(total_visits) as total_visits, cast(sum(dur_minutes) as numeric(30,5)) as dur_minutes
into #share_best_subcategory
from #share_session_prop
group by platform, digital_type, best_category, best_subcategory;


begin try drop table #share_best_category end try begin catch end catch;

select platform, digital_type, best_category, sum(total_visits) as total_visits, cast(sum(dur_minutes) as numeric(30,5)) as dur_minutes
into #share_best_category
from #share_session_prop
group by platform, digital_type, best_category;



--OUTPUTS
--------------------------
--1. Output for the table
select *
from #share_best_category 
order by platform, digital_type, best_category;


--2. Output for property level table #share_session_prop
select a.*, a.dur_minutes/a.total_panelists as avg_minutes from #share_session_prop a
order by eco_type desc, platform, Digital_Type, dur_minutes desc;

-- select * from #share_session_prop where 
-- compare reach
--select top 100 * from #share_session_agg;
--select top 100 * from #share_session_prop;
--select top 1000 * from run.v_Report_Sequence_Event order by date_id;

/**
---- QA

select count(distinct a.panelist_key) 
from #share_session_agg a
inner join run.User_Qualification b
on a.panelist_key = b.Panelist_Key and Daily_Qualified > 0;

select *
from core.ref.Taxonomy_Common_Properties 
where project_m_reach >0.05
order by project_m_reach desc;

select a.*, a.dur_minutes/a.total_panelists as avg_minutes, cast(cast(a.total_panelists as float)/1251 as decimal(10,5)) as reach, b.project_m_reach 
from #share_session_prop a 
inner join core.ref.Taxonomy_Common_Properties b
on a.property = b.property_name
where b.project_m_reach >0.05 and a.Digital_Type = 'web'
order by project_m_reach desc;

select a.*, a.dur_minutes/a.total_panelists as avg_minutes, cast(cast(a.total_panelists as float)/1251 as decimal(10,5)) as reach
from #share_session_prop a 
order by reach desc;


/**
-- QA largest contributors
select * -- eco_type, reportable_flag, platform, Digital_Type, property, dur_minutes
from #share_session_prop
where dur_minutes>500 or eco_type like 'Eco%'
order by eco_type desc, platform, Digital_Type, dur_minutes desc;

-- QA check if any one panelist is creative ridiuculous skew
select top 100 * from #share_session_agg order by dur_minutes desc;


-- QA largest contributors
select * from #share_session_prop 
--where dur_minutes>100
--where property in ('wattpad.com', 'Wattpad Free Books')
order by eco_type desc, platform, Digital_Type, dur_minutes desc;

-- QA on alert/remove/tbd properties
select distinct best_category from #share_session_prop;
Select * from #share_session_prop where best_category = '[Alert]';
Select * from #share_session_prop where best_category = '[Remove]';
Select * from #share_session_prop where best_category = '[TBD]';
Select * from #share_session_prop where reportable_flag = 0;

**/

-- QA for core
/**
-- 1. make sure unclassified portion is under 3% of total minutes
-- pull list of unclassified properties
-- select sum(dur_minutes) from #share_session_prop; --265410
select a.*, case when a.Digital_Type = 'app' then 100 when a.Digital_Type = 'web' then 235 end as digital_type_id, 
b.dur_minutes as cat_dur_mins, a.dur_minutes/b.dur_minutes as cat_dur_min_share from #share_session_prop a
left join #share_best_category b
on a.Platform = b.Platform and a.Digital_Type = b.Digital_Type and a.best_category = b.best_category
where 
--a.Digital_Type = 'web'
a.best_category like '%TBD%' and (total_panelists>2 or a.dur_minutes/b.dur_minutes > 0.05)
--or a.dur_minutes > 30 
order by total_panelists desc;

-- unvetted
select a.*, b.dur_minutes as cat_dur_mins, a.dur_minutes/b.dur_minutes as cat_dur_min_share, c.vetted_flag 
from #share_session_prop a
left join #share_best_category b
on a.Platform = b.Platform and a.Digital_Type = b.Digital_Type and a.best_category = b.best_category
left join (select property_name, 
case when digital_type_id = 100 then 'App' when digital_type_id = 235 then 'Web' end as digital_type, 
vetted_flag from core.ref.Taxonomy_Common_2020) c
on a.property = c.property_name and a.Digital_Type = c.digital_type
where a.best_category not like '%Alert%' and a.best_category not like '%TBD%' and a.best_category not like '%Remove%' and vetted_flag = 0
and a.dur_minutes/b.dur_minutes > 0.01
order by 1, cat_dur_min_share desc;

-- select * from core.ref.Taxonomy_Common_2020 where property_name = 'letgo- Sell & Buy Used Stuff';

-- high share of minute within category with mrs < 15 and cat min share > 10%
select a.*, b.dur_minutes as cat_dur_mins, a.dur_minutes/b.dur_minutes as cat_dur_min_share, c.vetted_flag 
from #share_session_prop a
left join #share_best_category b
on a.Platform = b.Platform and a.Digital_Type = b.Digital_Type and a.best_category = b.best_category
left join (select property_name, 
case when digital_type_id = 100 then 'App' when digital_type_id = 235 then 'Web' end as digital_type, 
vetted_flag from core.ref.Taxonomy_Common_2020) c
on a.property = c.property_name and a.Digital_Type = c.digital_type
where a.best_category not like '%Alert%' and a.best_category not like '%TBD%' and a.best_category not like '%Remove%' and total_panelists < 15
and a.dur_minutes/b.dur_minutes > 0.05
order by 1, cat_dur_min_share desc;
**/

-- END OF FILE --