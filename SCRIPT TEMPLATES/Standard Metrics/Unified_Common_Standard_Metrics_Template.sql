----------------------------------------------------------------------------------------------
-- OWNER
-- Unified Standard Metrics 
-- DATE
---------------------------------------------------------------------------------------------------

USE [PROJECTNAME]
-- FULL LIST OF DW TABLES USED
-- #UserByDay_Total_Ecosystem_daily_qual
-- Process.User_Qualification

-- TERMINOLGY WE USE
------------------------------------------------------------------
-- Dimensions: Ways the data will be cut (e.g. device type, digital, population segments, time segments)
-- Classifications: Taxonomy mapping, often detailed, that describes scattered datapoints into a meaningful way. (e.g. domain, property, category)
-- Measurements: Sometimes called metrics. Numeric values only. These may be additive across dimensions (e.g. minutes, visits, searches) or deduplicated (unique visitors, usage days)
-- Define Global Variables

-- select min([date_id]) from [PROJECTNAME].[run].[user_qualification] where daily_qualified=1;
-- select max([date_id]) from [PROJECTNAME].[run].[user_qualification] where daily_qualified=1;
-- check to make sure that run.user_qualification dates are matching run.userbyday_total dates

declare @global_first_date date = 'XXXX-XX-XX'; -- first legitimate date of study
declare @global_last_date  date = 'XXXX-XX-XX'; -- today's date (or last date of study)
declare @global_max_date   date = 'XXXX-XX-XX';
declare @global_min_qual_days   int = 9; -- 7+2 (to make sure to capture full week)
declare @global_min_span_days   int = 16; -- 14+2 (to make sure to capture full two weeks)



-- ##### JIMMY QA: IN FUTURE, ENSURE THAT THESE TABLES HAVE INDEX BEFORE THIS SCRIPT RUNS #####
/***
create index indx_panelist on rpt.Person_Standard_Metrics_Common_DU (panelist_key);
create index indx_property on rpt.Person_Standard_Metrics_Common_DU (property_key);
create index indx_digital on rpt.Person_Standard_Metrics_Common_DU (digital_type_id);

create index indx_panelist on ref.Panelist_Segmentation (panelist_key);
create index indx_panelist on ref.Respondent_Data (panelist_key);

create index indx_panelist on rpt.Person_Category_Usage_Weeks (panelist_key);
create index indx_property on rpt.Person_Category_Usage_Weeks (property_key);
create index indx_digital on rpt.Person_Category_Usage_Weeks (digital_type_id);

create index indx_panelist on rpt.Person_Category_Usage_Months (panelist_key);
create index indx_property on rpt.Person_Category_Usage_Months (property_key);
create index indx_digital on rpt.Person_Category_Usage_Months (digital_type_id);
****/



-- select * from #tax_version_lookup;
-- drop table #tax_version_lookup;
begin try drop table #tax_version_lookup end try begin catch end catch;

select cast(0 as int) as tax_version_id, cast('Common' as varchar(20)) as tax_version
into #tax_version_lookup;
insert into #tax_version_lookup select 1, 'Custom';

create index tax_version_lookup1 on #tax_version_lookup(tax_version_id);

-- ### USE THESE VARIABLE EDITS IN FUTURE PROJECTS ###
-- select * from #variable_names order by var_id, val_id;
begin try drop table #variable_names end try begin catch end catch;

select var_id, var_name, val_id, val_name
into #variable_names
from [Ref].[Variable_Names];
-- (36 rows affected)

--update #variable_names set val_id=10 where var_name='platform_id' and val_name='Mobile';
--update #variable_names set val_id=20 where var_name='platform_id' and val_name='PC';
insert into #variable_names select 1 as var_id, 'platform_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 2 as var_id, 'device_type_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 4 as var_id, 'digital_type_id' as var_name, 0 as val_id, 'All Digital' as val_name; -- ### Use 1 in the future

create index variable_names1 on #variable_names(val_id);

/*** Current report level
ReportLevel	0	Property
ReportLevel	1	Subcategory
ReportLevel	2	Category
ReportLevel	3	Total
	-- future report level
ReportLevel	0	Particle (pattern, domain, host, app name, media hash, keyword, etc.)
ReportLevel	1	Total
ReportLevel 2	Category
ReportLevel	3	Subcategory
Reportlevel 4   Propety (For vendor this is the smallest until. Often either app name, domain, or host)
Reportlevel	10	Alt Roll-up
Reportlevel	11	Alt Level 1
***/

---------------------------------------------------------------------------------------------------
-- STEP 1) CREATE QUALIFICATION AGGREGATES
---------------------------------------------------------------------------------------------------
begin try drop table #vendor_panelist_device_event end try begin catch end catch;

select [PanelistId] as panelist_id_vendor,
	parentclientID as device_id_vendor,
	cast('Web' as varchar(300)) as digital_type,	
	case when OSName like 'Android%' THEN 'Android'
	   when OSName like 'iOS%' THEN 'iOS'
	   when OSName like 'OSX%' THEN 'Mac OS'
	   when OSName like 'Windows%' THEN 'Windows'
	   else NULL end as os_name,
	case when devicetype like 'Laptop/Desktop' THEN cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'iOS%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'Android%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'OSX%' then cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'Windows%' then cast('PC' as varchar(300))
		when DeviceType = 'tablet' and OSName like 'Windows%' then cast('PC' as varchar(300))
		else null end as platform_type,		
	cast(date as date) as data_date
into #vendor_panelist_device_event
from dbo.RealLifeWeb;


insert into #vendor_panelist_device_event
select [PanelistId] as panelist_id_vendor,
	parentclientID as device_id_vendor,
	cast('App' as varchar(300)) as digital_type,	
	case when OSName like 'Android%' THEN 'Android'
	   when OSName like 'iOS%' THEN 'iOS'
	   when OSName like 'OSX%' THEN 'Mac OS'
	   when OSName like 'Windows%' THEN 'Windows'
	   else NULL end as os_name,
	case when devicetype like 'Laptop/Desktop' THEN cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'iOS%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'Android%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'OSX%' then cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'Windows%' then cast('PC' as varchar(300))
		when DeviceType = 'tablet' and OSName like 'Windows%' then cast('PC' as varchar(300))
		else null end as platform_type,			
	cast(startdate as date) as data_date
from dbo.RealLifeApp;


begin try drop table #vendor_panelist_device_date end try begin catch end catch;
-- keep only valid dates and aggregate

select panelist_id_vendor, device_id_vendor, digital_type, os_name, platform_type, data_date,
	case when platform_type = 'PC' then 1 else 0 end as flag_pc,
	case when platform_type = 'Mobile' then 1 else 0 end as flag_mobile,
	count(*) as ct_records
into #vendor_panelist_device_date
from #vendor_panelist_device_event
where data_date >= @global_first_date and data_date <= @global_last_date 
group by panelist_id_vendor, device_id_vendor, digital_type, os_name, platform_type, data_date;


begin try drop table #vendor_panelist_platform_date end try begin catch end catch;

select panelist_id_vendor, device_id_vendor, digital_type, platform_type, data_date, flag_pc, flag_mobile,
	sum(ct_records) as ct_records
into #vendor_panelist_platform_date
from #vendor_panelist_device_date
group by panelist_id_vendor, device_id_vendor, digital_type, os_name, platform_type, data_date, flag_pc, flag_mobile;



-- FORMAT FOR PARTICIPATION FUNNEL EXCEL
---------------------------------------------------------------------------------------------------
-- select top 100 * from #participation_prep order by panelist_key;

begin try drop table #participation_prep end try begin catch end catch;

select panelist_id_vendor,
	b.panelist_key, 
	cast('All' as varchar(300)) as platform_type,
	count(distinct platform_type) as ct_platforms,
	count(distinct device_id_vendor) as ct_devices,
	max(flag_pc) AS pc_flag,
	max(flag_mobile) AS mobile_flag,
	min(data_date) as min_date,
	max(data_date) as max_date,
	sum(ct_records) as ct_records,
	count(distinct data_date) as ct_dates,
	datediff(day, min(data_date), max(data_date))+1 as span_dates,
	datediff(day, max(data_date), @global_max_date)+1 AS days_since_last,
	cast(count(distinct data_date) as decimal(6,3))/(datediff(day, min(data_date), max(data_date))+1) as utilization
into #participation_prep
from #vendor_panelist_device_date a
join [Ref].[Panelist] b -- append any variables needed (such a country, or segment)
	on a.panelist_id_vendor = b.[Vendor_Panelist_ID]
group by panelist_id_vendor, panelist_key;


insert into #participation_prep
select panelist_id_vendor,
	b.panelist_key, 
	a.platform_type,
	1 as ct_platforms,
	count(distinct device_id_vendor) as ct_devices,
	max(flag_pc) AS pc_flag,
	max(flag_mobile) AS mobile_flag,
	min(data_date) as min_date,
	max(data_date) as max_date,
	sum(ct_records) as ct_records,
	count(distinct data_date) as ct_dates,
	datediff(day, min(data_date), max(data_date))+1 as span_dates,
	datediff(day, max(data_date), @global_max_date)+1 AS days_since_last,
	cast(count(distinct data_date) as decimal(6,3))/(datediff(day, min(data_date), max(data_date))+1) as utilization
from #vendor_panelist_device_date a
join [Ref].[Panelist] b
	on a.panelist_id_vendor = b.[Vendor_Panelist_ID]
group by panelist_id_vendor, panelist_key, platform_type;


create index participation_prep1 on #participation_prep (panelist_key);



begin try drop table #participation_summary end try begin catch end catch;

select a.panelist_id_vendor,
	a.panelist_key, 
	1 as ct_panelists,
	a.ct_platforms,
	a.ct_devices,
	isnull(m.ct_devices,0) as ct_mobile,
	isnull(p.ct_devices,0) as ct_pc,
	a.mobile_flag,
	a.pc_flag,
	case when a.ct_platforms = 2 then 1 else 0 end as dual_flag,
	case when (a.ct_platforms = 1 and a.pc_flag = 1) then 1 else 0 end as pc_only_flag,
	case when (a.ct_platforms = 1 and a.mobile_flag = 1) then 1 else 0 end as mobile_only_flag,
	a.ct_dates as ct_dates_all,
	isnull(m.ct_dates,0) as ct_dates_mobile,
	isnull(p.ct_dates,0) as ct_dates_pc,
	a.min_date as min_dates_all,
	-- m.min_date as min_dates_mobile,
	-- p.min_date as min_dates_pc,
	a.max_date as max_dates_all,
	-- m.max_date as max_dates_mobile,
	-- p.max_date as max_dates_pc,
	a.ct_records,
	a.span_dates,
	a.days_since_last,
    case when m.ct_dates >= @global_min_qual_days or p.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_dual, -- OR logic
	case when m.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_mobile,
	case when p.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_pc,
	cast(a.utilization as numeric(10,2)) as util_all,
	isnull(cast(m.utilization as numeric(10,2)),0) as util_mobile,
	isnull(cast(p.utilization as numeric(10,2)),0) as util_pc
INTO #participation_summary
FROM #participation_prep a
left outer join #participation_prep m
	on a.panelist_key=m.panelist_key
	and m.platform_type = 'Mobile'
left outer join #participation_prep p
	on a.panelist_key=p.panelist_key
	and p.platform_type = 'PC'
where a.platform_type='All';


create index participation_summary1 on #participation_summary (panelist_key);

-- Additional pass of diagnostics that looks at usage, as well as latest UserByDay scrubbed for outliers.

begin try drop table #usage_prep end try begin catch end catch;

select a.panelist_key,
	a.platform_id,
	case when a.platform_id=1 then cast('Mobile' as varchar(10))
		else case when a.platform_id=2 then cast('PC' as varchar(10))
		else '???' end end as platform_type,
	min(Date_ID) as min_date, max(Date_ID) as max_date, count(distinct Date_ID) as ct_dates,
	(datediff(day, min(Date_ID), max(Date_ID))+1) as span_dates,
	cast(count(distinct Date_ID) as decimal(6,3))/(datediff(day, min(Date_ID), max(Date_ID))+1) as utilization
into #usage_prep
from run.[UserByDay_Total] a
left outer join dbo.Panelist_Exclusion e
	on a.panelist_key = e.panelist_key 	
where e.panelist_key is null and a.Date_ID is not null
group by a.panelist_key, a.platform_id;



insert into #usage_prep
select a.panelist_key,
	0 as platform_id,
	'All' as platform_type,
	min(Date_ID) as min_date, max(Date_ID) as max_date, count(distinct Date_ID) as ct_dates,
	(datediff(day, min(Date_ID), max(Date_ID))+1) as span_dates,
	cast(count(distinct Date_ID) as decimal(6,3))/(datediff(day, min(Date_ID), max(Date_ID))+1) as utilization
from run.[UserByDay_Total] a
left outer join dbo.Panelist_Exclusion e
	on a.panelist_key = e.panelist_key 	
where e.panelist_key is null and a.Date_ID is not null
group by a.panelist_key;


create index usage_prep1 on #usage_prep (panelist_key);
create index usage_prep2 on #usage_prep (platform_type);
create index usage_prep3 on #usage_prep (platform_id);



begin try drop table #usage_summary end try begin catch end catch;

select a.panelist_id_vendor,
	a.panelist_key,
	a.ct_platforms,
	a.ct_devices,
	a.pc_flag,
	a.mobile_flag,
	d.min_date,
	d.max_date,
	d.ct_dates,
	d.utilization,
	case when (m.ct_dates >= (@global_min_qual_days-2) and m.span_dates >= (@global_min_span_days-2)) OR (p.ct_dates >= (@global_min_qual_days-2) AND p.span_dates >= (@global_min_span_days-2)) then 1 else 0 end as qual_dual, --OR logic. also 2 less days uses when userbyday
	case when (m.ct_dates >= (@global_min_qual_days-2) and m.span_dates >= (@global_min_span_days-2)) then 1 else 0 end as qual_mobile,
	case when (p.ct_dates >= (@global_min_qual_days-2) and p.span_dates >= (@global_min_span_days-2)) then 1 else 0 end as qual_pc,
	cast(d.utilization as numeric(10,2)) as util_all,
	isnull(cast(m.utilization as numeric(10,2)),0) as util_mobile,
	isnull(cast(p.utilization as numeric(10,2)),0) as util_pc
into #usage_summary
from #participation_summary a
inner join #usage_prep d
	on a.panelist_key=d.panelist_key
	and d.platform_type = 'All'
left outer join #usage_prep m
	on a.panelist_key=m.panelist_key
	and m.platform_type = 'Mobile'
left outer join #usage_prep p
	on a.panelist_key=p.panelist_key
	and p.platform_type = 'PC';



-- PROCESS FOR STRICTER PLATFORM BASED QUALIFICATION
--**PRIOR TO RUNNING THE SCRIPT BELOW...NEED TO MAKE PANELIST_SEGMENTATION FILE
---------------------------------------------------------------------------------------------------


begin try drop table #user_platform_qual_day end try begin catch end catch;

select 'XX' as country, a.panelist_key, p.segmentation_name, p.segment_name, a.Date_ID, a.week_id, a.month_id, a.weekly_qualified, a.monthly_qualified,
	case when b.qual_dual = 1 then 0 else null end as platform_id_dual,
	case when b.qual_mobile = 1 then 1 else null end as platform_id_mobile,
	case when b.qual_pc = 1 then 2 else null end as platform_id_pc
into #user_platform_qual_day
from run.User_Qualification a
inner join #usage_summary b --- ultimately, use participation, instead of usage, because usage trims first and last day. --1439
	on a.panelist_key=b.panelist_key
inner join PROJECTNAME.ref.[Panelist_Segmentation] p
	on a.panelist_key = p.panelist_key
left outer join dbo.Panelist_Exclusion e -- remove XX panelists
	on a.panelist_key = e.panelist_key 	
	where e.panelist_key is null and a.daily_qualified=1
;
--(367675 rows affected)
-- select * from PROJECTNAME.run.user_qualification where panelist_key =589
-- select * from #usage_summary where panelist_key = 589
-- select * from #participation_summary where panelist_key = 589
-- select * from PROJECTNAME.ref.panelist_segmentation where panelist_key=589





delete from #user_platform_qual_day where (platform_id_dual is null and platform_id_mobile is null and platform_id_pc is null);
--(7672 rows affected)

create index user_platform_qual_day1 on #user_platform_qual_day(panelist_key);
create index user_platform_qual_day2 on #user_platform_qual_day(date_id);
create index user_platform_qual_day3 on #user_platform_qual_day(platform_id_dual);
create index user_platform_qual_day4 on #user_platform_qual_day(platform_id_mobile);
create index user_platform_qual_day5 on #user_platform_qual_day(platform_id_pc);


begin try drop table #user_platform_qual_week end try begin catch end catch;

select country, panelist_key, segmentation_name, segment_name, week_id, platform_id_dual, platform_id_mobile, platform_id_pc
into #user_platform_qual_week
from #user_platform_qual_day a
where weekly_qualified=1
group by country, panelist_key, segmentation_name, segment_name, week_id, platform_id_dual, platform_id_mobile, platform_id_pc;
-- (51284 row(s) affected)

delete from #user_platform_qual_week where (platform_id_mobile is null and platform_id_pc is null); -- in case week level non-quals exist where days did not

create index user_platform_qual_week1 on #user_platform_qual_week(panelist_key);
create index user_platform_qual_week2 on #user_platform_qual_week(week_id);

-- select * from #user_platform_qual_month; 
begin try drop table #user_platform_qual_month end try begin catch end catch;

select country, panelist_key, segmentation_name, segment_name, month_id, platform_id_dual, platform_id_mobile, platform_id_pc
into #user_platform_qual_month
from #user_platform_qual_day a
where monthly_qualified=1
group by country, panelist_key, segmentation_name, segment_name, month_id, platform_id_dual, platform_id_mobile, platform_id_pc;
-- (10106 row(s) affected)

delete from #user_platform_qual_month where (platform_id_mobile is null and platform_id_pc is null);
create index user_platform_qual_month1 on #user_platform_qual_month(panelist_key);
create index user_platform_qual_month2 on #user_platform_qual_month(month_id);



-- QUAL DAYS
----------------------
-- select * from #user_platform_qual_Day_Ct;
begin try drop table #user_platform_qual_day_ct end try begin catch end catch;

select country, platform_id_dual as platform_id, panelist_key, segmentation_name, segment_name, count(distinct date_id) as dp_qualdays
into #user_platform_qual_day_ct
from #user_platform_qual_day
where platform_id_dual is not null
group by country, panelist_key, segmentation_name, segment_name, platform_id_dual;
--(10615 rows affected)
/*
insert into #user_platform_qual_day_ct
select country, platform_id_dual as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_dual is not null
group by country, panelist_key, platform_id_dual;
--(953 rows affected)
*/

insert into #user_platform_qual_day_ct
select country, platform_id_mobile as platform_id, panelist_key, segmentation_name, segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_mobile is not null
group by country, panelist_key, segmentation_name, segment_name, platform_id_mobile;
--(8260 rows affected)

/*
insert into #user_platform_qual_day_ct
select country, platform_id_mobile as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_mobile is not null
group by country, panelist_key, platform_id_mobile;
--(733 rows affected)
*/

insert into #user_platform_qual_day_ct
select country, platform_id_pc as platform_id, panelist_key, segmentation_name, segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_pc is not null
group by country, panelist_key, segmentation_name, segment_name, platform_id_pc;
--(2853 rows affected)

/*
insert into #user_platform_qual_day_ct
select country, platform_id_pc as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_pc is not null
group by country, panelist_key, platform_id_pc;
--(291 rows affected)
*/

-- select * from #user_platform_qual_day_ct order by country, panelist_key, segmentation_name, segment_name; 30727 rows 

-- drop table #device_topline_qual_days;
begin try drop table #device_topline_qual_days end try begin catch end catch;

select country, platform_id, segmentation_name, segment_name, count(distinct panelist_key) as dp, sum(dp_qualdays) as dp_qualdays
into #device_topline_qual_days
from #user_platform_qual_day_ct
group by country, platform_id, segmentation_name, segment_name;
--(180 rows affected)
-- select * from #device_topline_qual_days where platform_id =2;
-- select * from #user_platform_qual_day where panelist_key = 1389;

-- QUAL WEEKS
----------------------
-- select * from #user_platform_qual_day_ct;
begin try drop table #user_platform_qual_week_ct end try begin catch end catch;

select country, platform_id_dual as platform_id, panelist_key, segmentation_name, segment_name, count(distinct week_id) as wp_qualweeks
into #user_platform_qual_week_ct
from #user_platform_qual_week
where platform_id_dual is not null
group by country, panelist_key, segmentation_name, segment_name, platform_id_dual;
--(10615 rows affected)

/*
insert into #user_platform_qual_week_ct
select country, platform_id_dual as platform_id, panelist_key,  'Total' as segmentation_name, 'Total' as segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_dual is not null
group by country, panelist_key, platform_id_dual;
--(953 rows affected)
*/

insert into #user_platform_qual_week_ct
select country, platform_id_mobile as platform_id, panelist_key, segmentation_name, segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_mobile is not null
group by country, panelist_key, segmentation_name, segment_name, platform_id_mobile;
--(8260 rows affected)

/*
insert into #user_platform_qual_week_ct
select country, platform_id_mobile as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_mobile is not null
group by country, panelist_key, platform_id_mobile;
--(733 rows affected)
*/
insert into #user_platform_qual_week_ct
select country, platform_id_pc as platform_id, panelist_key, segmentation_name, segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_pc is not null
group by country, panelist_key, segmentation_name, segment_name, platform_id_pc;
--(2853 rows affected)

/*
insert into #user_platform_qual_week_ct
select country, platform_id_pc as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_pc is not null
group by country, panelist_key, platform_id_pc;
--(291 rows affected)
-- select * from #user_platform_qual_day_ct; 30727 rows 
*/
-- select * from #device_topline_qual_weeks;
-- drop table #device_topline_qual_weeks;

begin try drop table #device_topline_qual_weeks end try begin catch end catch;

select country, platform_id, segmentation_name, segment_name, count(distinct panelist_key) as wp, sum(wp_qualweeks) as wp_qualweeks
into #device_topline_qual_weeks
from #user_platform_qual_week_ct
group by country, platform_id, segmentation_name, segment_name;
--(180 rows affected)


-- QUAL MONTHS
----------------------
-- select * from #user_platform_qual_month_ct;
-- select * from #user_platform_qual_month;
begin try drop table #user_platform_qual_month_ct end try begin catch end catch;

select country, platform_id_dual as platform_id, panelist_key, segmentation_name, segment_name, count(distinct month_id) as mp_qualmonths
into #user_platform_qual_month_ct
from #user_platform_qual_month
where platform_id_dual is not null
group by country, panelist_key, segmentation_name, segment_name, platform_id_dual;
--(10106 rows affected)

/*
insert into #user_platform_qual_month_ct
select country, platform_id_dual as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_dual is not null
group by country, panelist_key, platform_id_dual;
--(928 rows affected)
*/

insert into #user_platform_qual_month_ct
select country, platform_id_mobile as platform_id, panelist_key, segmentation_name, segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_mobile is not null
group by country, panelist_key, segmentation_name, segment_name, platform_id_mobile;
--(7881 rows affected)

/*
insert into #user_platform_qual_month_ct
select country, platform_id_mobile as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_mobile is not null
group by country, panelist_key, platform_id_mobile;
--(711 rows affected)
*/

insert into #user_platform_qual_month_ct
select country, platform_id_pc as platform_id, panelist_key, segmentation_name, segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_pc is not null
group by country, panelist_key, segmentation_name, segment_name, platform_id_pc;
--(2723 rows affected)

/*
insert into #user_platform_qual_month_ct
select country, platform_id_pc as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_pc is not null
group by country, panelist_key, platform_id_pc;
--(288 rows affected)
-- select * from #user_platform_qual_month; 3265 rows 
*/

-- select * from #device_topline_qual_months;
-- drop table #device_topline_qual_months;
begin try drop table #device_topline_qual_months end try begin catch end catch;

select country, platform_id, segmentation_name, segment_name, count(distinct panelist_key) as mp, sum(mp_qualmonths) as mp_qualmonths
into #device_topline_qual_months
from #user_platform_qual_month_ct
group by country, platform_id, segmentation_name, segment_name;
--(180 rows affected)

-- COMBINE TOPLINE FOR DAY, WEEK, AND MONTH

-- select * from #device_topline_qual where segmentation_name='total' order by platform_id, country
-- select * from #device_topline_qual_days where segmentation_name = 'Total'

begin try drop table #device_topline_qual end try begin catch end catch;

select d.country, d.platform_id, d.segmentation_name, d.segment_name, dp, dp_qualdays, wp, wp_qualweeks, mp, mp_qualmonths
into #device_topline_qual
from #device_topline_qual_days d
inner join #device_topline_qual_weeks w
	on d.platform_id=w.platform_id
	and d.segmentation_name = w.segmentation_name
	and d.segment_name = w.segment_name
	and d.country = w.country
inner join #device_topline_qual_months m
	on d.platform_id=m.platform_id
	and d.segmentation_name = m.segmentation_name
	and d.segment_name = m.segment_name
	and d.country = m.country
;
--(180 rows affected)
-- select * from #device_topline_qual;
-- select distinct(panelist_key) from #user_platform_qual_day; 
-- select * from #user_platform_qual_day where panelist_key =1389;
-- select distinct(panelist_key) from #user_platform_qual_month;
-- select * from #user_platform_qual_month where panelist_key = 589



create index device_topline_qual1 on #device_topline_qual(platform_id);
create index device_topline_qual2 on #device_topline_qual(country);
create index device_topline_qual3 on #device_topline_qual(segmentation_name);
create index device_topline_qual4 on #device_topline_qual(segment_name);

-- select * from #device_topline_qual;

---------------------------------------------------------------------------------------------------
-- STEP 3) CREATE DAILY RAW METRICS
---------------------------------------------------------------------------------------------------
-- PERSON LEVEL AGG
-- Create Daily table to analyze TOTAL & CAT & subcat usage days on
-----------------------------
-----------------------------
--PULL IN SEGMENTS
-----------------------------


begin try drop table #temp_person_agg_cat_total_segments end try begin catch end catch;

select a.*, b.segmentation_name, b.segment_name
into #temp_person_agg_cat_total_segments
from rpt.Person_Standard_Metrics_Common_DU  a
join ref.Panelist_Segmentation b
	on a.panelist_key = b.panelist_key
and supercategory not in ('Remove', '[Alert]', '[TBD]')
where platform_id=0 and (a.digital_type_id in (100,235) or property_key=0) --  ##### JIMMY QA: PATCH
;

create index #temp_person_agg_cat_total_segments1 on #temp_person_agg_cat_total_segments (panelist_key);



begin try drop table #temp_panel_agg_cat_total_segments end try begin catch end catch;
select country, segmentation_name, segment_name, platform_id, digital_type_id, report_level, supercategory, Property_Key, property_hash,
	count(distinct panelist_key) as raw_persons,
	count(date_id) as raw_usage_days, --count(distinct Date_ID) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_panel_agg_cat_total_segments
from  #temp_person_agg_cat_total_segments
group by country, segmentation_name, segment_name, platform_id, digital_type_id, report_level, supercategory, Property_Key, property_hash;


/*
insert into  #temp_panel_agg_cat_total_segments
select a.country, 'Total' as segmentation_name, 'Total' as segment_name, a.platform_id, a.digital_type_id, a.report_level, a.supercategory, a.Property_Key, property_hash,
	count(distinct a.panelist_key) as raw_persons,
	count(date_id) as raw_usage_days, 
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from PROJECTNAME.rpt.Person_Standard_Metrics_Common_DU a
	--where platform_id=0 and (a.digital_type_id in (100,235) or property_key=0) --  ##### JIMMY QA: PATCH
group by a.country, a.digital_type_id, platform_id, report_level, supercategory, a.Property_Key, property_hash;
--(121234 rows affected)
*/



begin try drop table #temp_panel_agg_cat_total_segments_weighted end try begin catch end catch;
select 
	country, segmentation_name, segment_name, platform_id, 
    digital_type_id, report_level, supercategory, Property_Key, property_hash,
	sum(raw_persons) as raw_persons,
	sum(raw_usage_days) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_panel_agg_cat_total_segments_weighted
from  #temp_panel_agg_cat_total_segments a
--where report_level in (0,3)
group by digital_type_id, platform_id, report_level, supercategory, Property_Key, property_hash,
	country, segmentation_name, segment_name;



---------------------------------------------------------------------------------------------------
-- STEP 5) CREATE USAGE WEEKS AND USAGE MONTHS
---------------------------------------------------------------------------------------------------
--select distinct device_type_id, digital_type_id, report_level from #temp_person_qual_week_used order by device_type_id, digital_type_id, report_level
--select distinct cat_hash, subcat_hash, device_type_id, digital_type_id, report_level from #temp_person_qual_week_used where cat_hash = 2011 and subcat_hash = 3011 and report_level = 1 order by device_type_id, digital_type_id, report_level 

-----------------------------------------------------------------------------------
-- USAGE WEEKS
-----------------------------------------------------------------------------------


--select top 100 * from #temp_person_qual_week_used_segments;
-- select * from rpt.Person_Category_Usage_weeks
begin try drop table #temp_person_qual_week_used_segments end try begin catch end catch;
select a.*, b.segmentation_name, segment_name--, r.country
into #temp_person_qual_week_used_segments
from rpt.Person_Category_Usage_weeks a
join ref.Panelist_Segmentation b
	on a.panelist_key = b.panelist_key
    and supercategory not in ('Remove', '[Alert]', '[TBD]');


begin try drop table #standard_metrics_wu_useweeks_weighted end try begin catch end catch;
select country, digital_type_id, platform_id, report_level, supercategory, Property_Key,property_hash,
	segmentation_name, segment_name,
	 sum(wu_useweeks) as wu_useweeks, count(distinct panelist_key) as wu
into #standard_metrics_wu_useweeks_weighted
from #temp_person_qual_week_used_segments
group by country, digital_type_id, platform_id, report_level, supercategory, Property_Key,property_hash,
    segmentation_name, segment_name
;


/*
insert into #standard_metrics_wu_useweeks_weighted
select a.country, digital_type_id, platform_id, report_level, supercategory, Property_Key,property_hash,
	'Total' as segmentation_name, 'Total' as segment_name,
	sum(wu_useweeks) as wu_useweeks, count(distinct a.panelist_key) as wu
from [PROJECTNAME].rpt.Person_Category_Usage_Weeks a
group by a.country, digital_type_id, platform_id, report_level, supercategory, Property_Key,property_hash;
--(120837 rows affected)
*/


--------------------------------------------------------------------------------------------------------------
-- USAGE MONTH
--------------------------------------------------------------------------------------------------------------

--select top 100 * from #temp_person_qual_month_used_segments;
begin try drop table #temp_person_qual_month_used_segments end try begin catch end catch;
select a.*, b.segmentation_name, segment_name--, r.country
into #temp_person_qual_month_used_segments
from rpt.Person_Category_Usage_Months a
join ref.Panelist_Segmentation b
	on a.panelist_key = b.panelist_key
	and supercategory not in ('Remove', '[Alert]', '[TBD]');


begin try drop table #unified_standard_metrics_mu_usemonths_weighted end try begin catch end catch;
select country, digital_type_id, platform_id, report_level, supercategory, Property_Key, property_hash,
	segmentation_name, segment_name,  
	sum(mu_usemonths) as mu_usemonths, count(distinct panelist_key) as mu
into #unified_standard_metrics_mu_usemonths_weighted
from #temp_person_qual_month_used_segments
group by country, digital_type_id, platform_id, report_level, supercategory, Property_Key,property_hash,
	segmentation_name, segment_name;

/*
insert into #unified_standard_metrics_mu_usemonths_weighted
select a.country, digital_type_id, platform_id, report_level, supercategory, Property_Key, property_hash,
	'Total' as segmentation_name, 'Total' as segment_name,
	sum(mu_usemonths) as mu_usemonths, count(distinct a.panelist_key) as mu
from [PROJECTNAME].rpt.Person_Category_Usage_Months a 
group by a.country, digital_type_id, platform_id, report_level, supercategory, Property_Key,property_hash
;

*/


-- BE SURE WEEK AND MONTH USAGE ARE INDEXED
-- We really should just use tax_id... as the map, so we don't have to index on all 4 levels of taxonomy/reporting structure.
create index standard_metrics_wu_useweeks1 on #standard_metrics_wu_useweeks_weighted(platform_id);
create index standard_metrics_wu_useweeks2 on #standard_metrics_wu_useweeks_weighted(digital_type_id);
create index standard_metrics_wu_useweeks5 on #standard_metrics_wu_useweeks_weighted(Property_Key);
create index standard_metrics_wu_useweeks6 on #standard_metrics_wu_useweeks_weighted(report_level);
create index standard_metrics_wu_useweeks8 on #standard_metrics_wu_useweeks_weighted(segmentation_name);
create index standard_metrics_wu_useweeks9 on #standard_metrics_wu_useweeks_weighted(segment_name);

create index standard_metrics_mu_usemonths1 on #unified_standard_metrics_mu_usemonths_weighted(platform_id);
create index standard_metrics_mu_usemonths2 on #unified_standard_metrics_mu_usemonths_weighted(digital_type_id);
create index standard_metrics_mu_usemonths5 on #unified_standard_metrics_mu_usemonths_weighted(Property_Key);
create index standard_metrics_mu_usemonths6 on #unified_standard_metrics_mu_usemonths_weighted(report_level);
create index standard_metrics_mu_usemonths8 on #unified_standard_metrics_mu_usemonths_weighted(segmentation_name);
create index standard_metrics_mu_usemonths9 on #unified_standard_metrics_mu_usemonths_weighted(segment_name);

---------------------------------------------------------------------------------------------------
-- STEP 6) INTEGRATE EVERYTHING... FO SHO
---------------------------------------------------------------------------------------------------


begin try drop table #temp_total_internet_minutes end try begin catch end catch;
select country, segmentation_name, segment_name, digital_type_id, platform_id, raw_minutes as du_minutes
into #temp_total_internet_minutes
from #temp_panel_agg_cat_total_segments_weighted 
where report_level= 3;


create index temp_total_internet_minutes2 on #temp_total_internet_minutes(digital_type_id);


-- NOTE: Split out the Booya, so that script runs faster

begin try drop table #temp_standard_metrics_booya1 end try begin catch end catch;
select 
	p.country, 
	p.platform_id,
	a.digital_type_id, 
	p.segmentation_name, 
	p.segment_name, 
	a.report_level,  
	a.supercategory,
	a.Property_Key,
	a.property_hash,
	p.dp, 
	a.raw_persons as du,    
	p.dp_qualdays, 
	raw_usage_days as du_usedays, 
	raw_visits as du_visits,
	cast(a.raw_minutes as numeric(30,2)) as du_mins,
	--cast(ti.du_minutes as numeric(30,2)) as du_mins_ti,
	p.wp_qualweeks, 
	--wu.wu_useweeks,
	p.mp_qualmonths --, 
	--mu.mu_usemonths
into #temp_standard_metrics_booya1
from  #temp_panel_agg_cat_total_segments_weighted a
inner join #device_topline_qual p -- CHECK -- select top 100 * from #device_topline_qual
	on p.country=a.country
	and p.platform_id = a.platform_id
	and p.segmentation_name=a.segmentation_name
	and p.segment_name=a.segment_name;


begin try drop table #temp_standard_metrics_booya2 end try begin catch end catch;
select 
	a.country, a.platform_id, a.digital_type_id, a.segmentation_name, a.segment_name, a.report_level,
	a.supercategory, a.Property_Key, a.property_hash,
	dp, du, dp_qualdays, du_usedays, du_visits, du_mins,
	--cast(ti.du_minutes as numeric(30,2)) as du_mins_ti,
	wp_qualweeks, 
	isnull(wu.wu_useweeks,0) as wu_useweeks,
	mp_qualmonths --, 
	--mu.mu_usemonths
into #temp_standard_metrics_booya2
from #temp_standard_metrics_booya1 a
left outer join #standard_metrics_wu_useweeks_weighted wu --select top 100 * from #standard_metrics_wu_useweeks_weighted
	on a.platform_id=wu.platform_id
	and a.digital_type_id=wu.digital_type_id
	and a.report_level=wu.report_level
	and a.supercategory=wu.supercategory
	and a.Property_Key = wu.Property_Key
	and a.property_hash = wu.property_hash
	and a.segmentation_name = wu.segmentation_name
	and a.segment_name = wu.segment_name
	and a.country = wu.country;


begin try drop table #temp_standard_metrics_booya3 end try begin catch end catch;
select 
	a.country, a.platform_id, a.digital_type_id, a.segmentation_name, a.segment_name, a.report_level,
	a.supercategory, a.Property_Key, a.property_hash,
	dp, du, dp_qualdays, du_usedays, du_visits, du_mins,
	--cast(ti.du_minutes as numeric(30,2)) as du_mins_ti,
	wp_qualweeks, 
	wu_useweeks,
	mp_qualmonths,
	isnull(mu.mu_usemonths,0) as mu_usemonths
into #temp_standard_metrics_booya3
from #temp_standard_metrics_booya2 a
left outer join #unified_standard_metrics_mu_usemonths_weighted mu --select top 100 * from #unified_standard_metrics_mu_usemonths_weighted
	on a.platform_id=mu.platform_id
	and a.digital_type_id=mu.digital_type_id
	and a.report_level=mu.report_level
	and a.supercategory=mu.supercategory
	and a.Property_Key = mu.Property_Key
	and a.property_hash = mu.property_hash
	and a.segmentation_name = mu.segmentation_name
	and a.segment_name = mu.segment_name
	and a.country = mu.country;


begin try drop table #temp_standard_metrics_booya4 end try begin catch end catch;
select 
	a.country, a.platform_id, a.digital_type_id, a.segmentation_name, a.segment_name, a.report_level,
	a.supercategory, a.Property_Key, isnull(c.rpt_flag, 1) as rpt_flag,
	dp, du, dp_qualdays, du_usedays, du_visits, du_mins,
	cast(isnull(ti.du_minutes,0) as numeric(30,2)) as du_mins_ti,
	wp_qualweeks, wu_useweeks, mp_qualmonths, mu_usemonths
into #temp_standard_metrics_booya4
from #temp_standard_metrics_booya3 a
left outer join #temp_total_internet_minutes ti -- should only have 8 rows -- CHECK -- select top 100 * from #temp_total_internet_minutes order by device_type_id, digital_type_id
	on a.platform_id=ti.platform_id
	and a.digital_type_id=ti.digital_type_id
	and a.segment_name = ti.segment_name
	and a.segmentation_name = ti.segmentation_name
	and a.country = ti.country
Left outer join [Core].[ref].[Taxonomy_Common_Properties] c
	on a.property_hash = c.property_hash 
	and a.digital_type_id = c.digital_type_id
;


-- select * from #temp_standard_metrics_booya4 
begin try drop table rpt.Unified_Common_Standard_Metrics_Report end try begin catch end catch;
select 
	country, 
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	case when g.val_name is null then 'Total' else g.val_name end as report_level, 
	segmentation_name, 
	segment_name, 
	supercategory,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths,
	rpt_flag
into rpt.Unified_Common_Standard_Metrics_Report
from #temp_standard_metrics_booya4 a
left outer join [Ref].[Variable_Names] c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join [Ref].[Variable_Names] d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
join [Ref].[Variable_Names] g
	on a.report_level=g.val_id
	and g.var_name = 'reportlevel'
	and a.report_level <> 0
;

/*
INSERT INTO rpt.Unified_Common_Standard_Metrics_Report
select
	country, 
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	case when g.val_name is null then 'Total' else g.val_name end as report_level,
	segmentation_name,
	segment_name,  
	'* Total Internet * ' as category, 
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
from #temp_standard_metrics_booya4 a
left outer join [Ref].[Variable_Names] c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join [Ref].[Variable_Names] d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
left outer join [Ref].[Variable_Names] g
	on a.report_level=g.val_id
	and g.var_name = 'reportlevel'
WHERE a.report_level = 3
;
*/


--------------------------------
--PROPERTY SECTION 
--------------------------------
-- ################################################################################################
-- PERMANENT TABLE CREATED HERE
-- ################################################################################################
-- select top 100 * from [rpt].[Unified_Standard_Metrics_Property];
-- select top 100 * from [rpt].[Unified_Standard_Metrics_Property] where device_type='All Devices';
-- select count(*) from [rpt].[Unified_Standard_Metrics_Property];

begin try drop table [rpt].[Unified_Standard_Metrics_Property] end try begin catch end catch;

select		
	country, 												-- add digital_type = app
	isnull(g.val_name,'All Devices') as device_type,
	d.val_name as digital_type, 
	'Common' as tax_version,
	a.segmentation_name, 
	a.segment_name,
	supercategory,    
	h.app_name as property,
	0 as whitelist_flag,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths,
	rpt_flag
into [rpt].[Unified_Standard_Metrics_Property]
from #temp_standard_metrics_booya4 a
inner join [Ref].[App_Name] h
	on a.property_key = h.app_name_key
	AND a.digital_type_id = 100
join  #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
left outer join #variable_names g
	on a.platform_id=g.val_id
	and g.var_name = 'platform_id'
;



create index indx_property_name on rpt.Unified_Standard_Metrics_Property (property);


insert into [rpt].[Unified_Standard_Metrics_Property]	-- add digital_type = web
select 
	country, 
	isnull(g.val_name,'All Devices') as device_type,
	d.val_name as digital_type, 
	'Common' as tax_version, 
	a.segmentation_name, 
	a.segment_name, 
	supercategory, 
	h.domain_name as property,
	0 as whitelist_flag,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths,
	rpt_flag
from #temp_standard_metrics_booya4 a
inner join [Ref].Domain_Name h
	on a.property_key = h.domain_name_key
	AND a.digital_type_id = 235
join  #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
left outer join #variable_names g
	on a.platform_id=g.val_id
	and g.var_name = 'platform_id'
;



insert into [rpt].[Unified_Standard_Metrics_Property]
select 
	country,
	isnull(c.val_name,'All Devices') as device_type, 
	d.val_name as digital_type, 
	'Common' as tax_version, 
	a.segmentation_name, 
	a.segment_name, 
	'* Total Internet * ' as supercategory,
	'* Total Internet * ' as property,
	0 as whitelist_flag,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths,
	rpt_flag
from #temp_standard_metrics_booya4 a
left outer join #variable_names c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join  #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
inner join  #variable_names g
	on a.report_level=g.val_id
	and g.var_name = 'reportlevel'
WHERE a.report_level = 3
;


--select distinct platform_id from #temp_standard_metrics_booya
---------------------------------------------------------------------------------------------------
-- STEP 7) EXPORT UNIFIED STANDARD METRICS... GET IT OUT
---------------------------------------------------------------------------------------------------

-- EXPORT 1
-- This selection exports Total, Cat, & Subcat

select 
	country, 
	device_type, 
	digital_type, 
	'Common' as tax_version,
	supercategory,
	segmentation_name,
	segment_name,
	dp, du, dp_qualdays, du_usedays, du_visits, du_mins, du_mins_ti, wp_qualweeks, wu_useweeks, mp_qualmonths, mu_usemonths
from [rpt].Unified_Common_Standard_Metrics_Report
order by 
	tax_version,  
	digital_type, 
	supercategory, 
	du desc
;


-- EXPORT 2
-- This selection exports Property
select 
--	country, 
	device_type, 
	digital_type, 
	tax_version, 
	whitelist_flag,
	supercategory,
	property,
	segmentation_name,
	segment_name,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths,
	mu_usemonths
from [rpt].[Unified_Standard_Metrics_Property] a
where tax_version = 'Common' 
    and (whitelist_flag=1 or du>=15)
	--and supercategory not in ('[Alert]', '[TBD]')
	and rpt_flag != 0
order by 
	tax_version,  
	digital_type, 
	supercategory,
	property, 
	du desc
;

---------------------------------------------------------------------------------------------------
-- Create Unified Share of Digital
---------------------------------------------------------------------------------------------------
-- property level
-- select top 100 * from [rpt].[Unified_Standard_Metrics_Property];
-- select distinct device_type from [rpt].[Unified_Standard_Metrics_Property];
-- select distinct digital_type from [rpt].[Unified_Standard_Metrics_Property];
-- select distinct segmentation_name, segment_name from rpt.Unified_Common_Standard_Metrics_Report order by 1,2;
-- select top 1000 * from rpt.unified_Share_of_Digital_Property_Level order by 1,2,3,4,5;

begin try drop table rpt.unified_Share_of_Digital_Property_Level end try begin catch end catch;
select device_type, digital_type, segmentation_name, segment_name, property, supercategory, du_mins, du as daily_users, du_visits
into rpt.unified_Share_of_Digital_Property_Level
from [rpt].[Unified_Standard_Metrics_Property]
where segmentation_name in ('')
and supercategory ! = '* Total Internet * '
and device_type in ('All Devices','Mobile','PC')
;


/*
select platform_id, digital_type_id, segmentation_name, segment_name
from #temp_panel_agg_cat_total_segments_weighted
where Property_Key <> 0
group by platform_id, digital_type_id, segmentation_name, segment_name
order by platform_id, digital_type_id, segmentation_name, segment_name;

select platform_id, digital_type_id, segmentation_name, segment_name
from #standard_metrics_wu_useweeks_weighted
where Property_Key <> 0
group by platform_id, digital_type_id, segmentation_name, segment_name
order by platform_id, digital_type_id, segmentation_name, segment_name;

select platform_id, digital_type_id, segmentation_name, segment_name
from #unified_standard_metrics_mu_usemonths_weighted
where Property_Key <> 0 --and digital_type_id = 0
group by platform_id, digital_type_id, segmentation_name, segment_name
order by platform_id, digital_type_id, segmentation_name, segment_name;

select platform_id, digital_type_id, segmentation_name, segment_name
from #temp_total_internet_minutes
group by platform_id, digital_type_id, segmentation_name, segment_name
order by platform_id, digital_type_id, segmentation_name, segment_name;

select platform_id, segmentation_name, segment_name
from #device_topline_qual
group by platform_id, segmentation_name, segment_name
order by platform_id, segmentation_name, segment_name;

select platform_id, digital_type_id, segmentation_name, segment_name
from #temp_standard_metrics_booya
where Property_Key <> 0
group by platform_id, digital_type_id, segmentation_name, segment_name
order by platform_id, digital_type_id, segmentation_name, segment_name;
*/
--select  * from #temp_standard_metrics_booya where property_key = 0 and subcat_hash <> 0 and segmentation_name = 'Total'

--select distinct segmentation_name from #temp_panel_agg_cat_total_segments_weighted;
--select distinct segmentation_name from #standard_metrics_wu_useweeks_weighted;
--select distinct segmentation_name from #unified_standard_metrics_mu_usemonths_weighted;
--select distinct segmentation_name from #temp_total_internet_minutes;

--select top 10000 * from #temp_standard_metrics_booya where subcat_hash = 0 and report_level = 2
--select top 100 * from [Ref].[Variable_Names]

--select top 1000 * from #cat_subcat_lookup
--select top 10000 * from #temp_standard_metrics_booya where subcat_hash = 0 and report_level = 2
--select top 10000 * from #temp_standard_metrics_booya where subcat_hash = 0 and report_level = 2
--select top 10000 * from #temp_standard_metrics_booya where subcat_hash = 0 and report_level = 2

--select top 100 * from rpt.Unified_Common_Standard_Metrics_Report
--select distinct segmentation_name from #temp_standard_metrics_booya;
--select distinct segmentation_name from ref.Panelist_Segmentation; 
--select top 100 * from #temp_panel_agg_cat_total_segments_weighted;

-----END OF FILE-----
