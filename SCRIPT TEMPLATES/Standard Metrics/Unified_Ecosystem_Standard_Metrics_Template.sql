---------------------------------------------------------------------------------------------------
-- OWNER
---------------------------------------------------------------------------------------------------

USE [PROJECTNAME];
-- FULL LIST OF DW TABLES USED
-- #UserByDay_Total_Ecosystem_daily_qual
-- Process.User_Qualification

-- TERMINOLGY WE USE
------------------------------------------------------------------
-- Dimensions: Ways the data will be cut (e.g. device type, digital, population segments, time segments)
-- Classifications: Taxonomy mapping, often detailed, that describes scattered datapoints into a meaningful way. (e.g. domain, property, category)
-- Measurements: Sometimes called metrics. Numeric values only. These may be additive across dimensions (e.g. minutes, visits, searches) or deduplicated (unique visitors, usage days)

-- Define Global Variables (make sure that these dates match the user_qualification max/min date_id and the userbyday_total max/min date_id)
declare @global_first_date date = 'XXXX-XX-XX'; -- first legitimate date of study (min(day_id) from user_qualification table
declare @global_last_date  date = 'XXXX-XX-XX'; -- today's date (or last date of study)
declare @global_max_date   date = 'XXXX-XX-XX'; -- max(day_id) from user_qualification table
declare @global_min_qual_days   int = 9;
declare @global_min_span_days   int = 16;

-- select * from #tax_version_lookup;
begin try drop table #tax_version_lookup end try begin catch end catch;

select cast(0 as int) as tax_version_id, cast('Common' as varchar(20)) as tax_version
into #tax_version_lookup;
insert into #tax_version_lookup select 1, 'Custom';

create index tax_version_lookup1 on #tax_version_lookup(tax_version_id);

-- ### USE THESE VARIABLE EDITS IN FUTURE PROJECTS ###
-- select * from #variable_names order by var_id, val_id;
begin try drop table #variable_names end try begin catch end catch;

select var_id, var_name, val_id, val_name
into #variable_names
from [Ref].[Variable_Names];

--update #variable_names set val_id=10 where var_name='platform_id' and val_name='Mobile';
--update #variable_names set val_id=20 where var_name='platform_id' and val_name='PC';
insert into #variable_names select 1 as var_id, 'platform_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 2 as var_id, 'device_type_id' as var_name, 0 as val_id, 'All Devices' as val_name; -- ### Use 1 in the future
insert into #variable_names select 4 as var_id, 'digital_type_id' as var_name, 0 as val_id, 'All Digital' as val_name; -- ### Use 1 in the future

create index variable_names1 on #variable_names(val_id);


/*** Current report level
ReportLevel	0	Property
ReportLevel	1	Subcategory
ReportLevel	2	Category
ReportLevel	3	Total
	-- future report level
ReportLevel	0	Particle (pattern, domain, host, app name, media hash, keyword, etc.)
ReportLevel	1	Total
ReportLevel 2	Category
ReportLevel	3	Subcategory
Reportlevel 4   Propety (For vendor this is the smallest until. Often either app name, domain, or host)
Reportlevel	10	Alt Roll-up
Reportlevel	11	Alt Level 1
***/

---------------------------------------------------------------------------------------------------
-- STEP 1) CREATE QUALIFICATION AGGREGATES
---------------------------------------------------------------------------------------------------

begin try drop table #vendor_panelist_device_event end try begin catch end catch;

select [PanelistId] as panelist_id_vendor,
	parentclientID as device_id_vendor,
	cast('Web' as varchar(300)) as digital_type,	
	case when os like 'Android%' THEN 'Android'
	   when os like 'iOS%' THEN 'iOS'
	   when os like 'OSX%' THEN 'Mac OS'
	   when os like 'Windows%' THEN 'Windows'
	   else NULL end as os_name,
	case when devicetype like 'Laptop/Desktop' THEN cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'iOS%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'Android%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'OSX%' then cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'Windows%' then cast('PC' as varchar(300))
		when DeviceType = 'tablet' and OSName like 'Windows%' then cast('PC' as varchar(300))
		else null end as platform_type,		
	cast(date as date) as data_date
into #vendor_panelist_device_event
from dbo.RealLifeWeb;


insert into #vendor_panelist_device_event
select [PanelistId] as panelist_id_vendor,
	parentclientID as device_id_vendor,
	cast('App' as varchar(300)) as digital_type,	
	case when os like 'Android%' THEN 'Android'
	   when os like 'iOS%' THEN 'iOS'
	   when os like 'OSX%' THEN 'Mac OS'
	   when os like 'Windows%' THEN 'Windows'
	   else NULL end as os_name,
	case when devicetype like 'Laptop/Desktop' THEN cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'iOS%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'Android%' then cast('Mobile' as varchar(300))
		when DeviceType is null and OSName like 'OSX%' then cast('PC' as varchar(300))
		when DeviceType is null and OSName like 'Windows%' then cast('PC' as varchar(300))
		when DeviceType = 'tablet' and OSName like 'Windows%' then cast('PC' as varchar(300))
		else null end as platform_type,		
	cast(startdate as date) as data_date
from dbo.RealLifeApp;

begin try drop table #vendor_panelist_device_date end try begin catch end catch;
-- keep only valid dates and aggregate

select panelist_id_vendor, device_id_vendor, digital_type, os_name, platform_type, data_date,
	case when platform_type = 'PC' then 1 else 0 end as flag_pc,
	case when platform_type = 'Mobile' then 1 else 0 end as flag_mobile,
	count(*) as ct_records
into #vendor_panelist_device_date
from #vendor_panelist_device_event
where data_date >= @global_first_date and data_date <= @global_last_date
group by panelist_id_vendor, device_id_vendor, digital_type, os_name, platform_type, data_date;

begin try drop table #vendor_panelist_platform_date end try begin catch end catch;

select panelist_id_vendor, device_id_vendor, digital_type, platform_type, data_date, flag_pc, flag_mobile,
	sum(ct_records) as ct_records
into #vendor_panelist_platform_date
from #vendor_panelist_device_date
group by panelist_id_vendor, device_id_vendor, digital_type, os_name, platform_type, data_date, flag_pc, flag_mobile;

--select top 100 * from #vendor_panelist_device_date
--select top 100 * from #vendor_panelist_platform_date

-- FORMAT FOR PARTICIPATION FUNNEL EXCEL
---------------------------------------------------------------------------------------------------
-- select top 100 * from #participation_prep order by panelist_key;

begin try drop table #participation_prep end try begin catch end catch;

select panelist_id_vendor,
	b.panelist_key, 
	cast('All' as varchar(300)) as platform_type,
	count(distinct platform_type) as ct_platforms,
	count(distinct device_id_vendor) as ct_devices,
	max(flag_pc) AS pc_flag,
	max(flag_mobile) AS mobile_flag,
	min(data_date) as min_date,
	max(data_date) as max_date,
	sum(ct_records) as ct_records,
	count(distinct data_date) as ct_dates,
	datediff(day, min(data_date), max(data_date))+1 as span_dates,
	datediff(day, max(data_date), @global_max_date)+1 AS days_since_last,
	cast(count(distinct data_date) as decimal(6,3))/(datediff(day, min(data_date), max(data_date))+1) as utilization
into #participation_prep
from #vendor_panelist_device_date a
join [PROJECTNAME].[Ref].[Panelist] b -- append any variables needed (such a country, or segment)
	on a.panelist_id_vendor = b.[Vendor_Panelist_ID]
group by panelist_id_vendor, panelist_key;

insert into #participation_prep
select panelist_id_vendor,
	b.panelist_key, 
	a.platform_type,
	1 as ct_platforms,
	count(distinct device_id_vendor) as ct_devices,
	max(flag_pc) AS pc_flag,
	max(flag_mobile) AS mobile_flag,
	min(data_date) as min_date,
	max(data_date) as max_date,
	sum(ct_records) as ct_records,
	count(distinct data_date) as ct_dates,
	datediff(day, min(data_date), max(data_date))+1 as span_dates,
	datediff(day, max(data_date), @global_max_date)+1 AS days_since_last,
	cast(count(distinct data_date) as decimal(6,3))/(datediff(day, min(data_date), max(data_date))+1) as utilization
from #vendor_panelist_device_date a
join [PROJECTNAME].[Ref].[Panelist] b
	on a.panelist_id_vendor = b.[Vendor_Panelist_ID]
group by panelist_id_vendor, panelist_key, platform_type;

create index participation_prep1 on #participation_prep (panelist_key);

-- select * from #participation_summary order by panelist_key;
-- declare @global_min_qual_days   int = 18;

begin try drop table #participation_summary end try begin catch end catch;

select a.panelist_id_vendor,
	a.panelist_key, 
	1 as ct_panelists,
	a.ct_platforms,
	a.ct_devices,
	isnull(m.ct_devices,0) as ct_mobile,
	isnull(p.ct_devices,0) as ct_pc,
	a.mobile_flag,
	a.pc_flag,
	a.ct_dates as ct_dates_all,
	isnull(m.ct_dates,0) as ct_dates_mobile,
	isnull(p.ct_dates,0) as ct_dates_pc,
	a.min_date as min_dates_all,
	-- m.min_date as min_dates_mobile,
	-- p.min_date as min_dates_pc,
	a.max_date as max_dates_all,
	-- m.max_date as max_dates_mobile,
	-- p.max_date as max_dates_pc,
	a.ct_records,
	a.span_dates,
	a.days_since_last,
	case when m.ct_dates >= @global_min_qual_days or p.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_dual, -- OR logic
	case when m.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_mobile,
	case when p.ct_dates >= @global_min_qual_days then 1 else 0 end as qual_pc,
	cast(a.utilization as numeric(10,2)) as util_all,
	isnull(cast(m.utilization as numeric(10,2)),0) as util_mobile,
	isnull(cast(p.utilization as numeric(10,2)),0) as util_pc
INTO #participation_summary
FROM #participation_prep a
left outer join #participation_prep m
	on a.panelist_key=m.panelist_key
	and m.platform_type = 'Mobile'
left outer join #participation_prep p
	on a.panelist_key=p.panelist_key
	and p.platform_type = 'PC'
where a.platform_type='All';

create index participation_summary1 on #participation_summary (panelist_key);

-- Additional pass of diagnostics that looks at usage, as well as latest UserByDay scrubbed for outliers.
-- drop table #usage_prep;
begin try drop table #usage_prep end try begin catch end catch;

select a.panelist_key,
	a.platform_id,
	case when a.platform_id=1 then cast('Mobile' as varchar(10))
		else case when a.platform_id=2 then cast('PC' as varchar(10))
		else '???' end end as platform_type,
	min(date_id) as min_date, max(date_id) as max_date, count(distinct date_id) as ct_dates,
	(datediff(day, min(Date_ID), max(Date_ID))+1) as span_dates,
	cast(count(distinct date_id) as decimal(6,3))/(datediff(day, min(date_id), max(date_id))+1) as utilization
into #usage_prep
from run.[UserByDay_Total] a
where a.date_id is not null
--left outer join [PROJECTNAME].dbo.Panelist_Exclusion e
	--on a.panelist_key = e.panelist_key 	
    --and e.panelist_key is null 
group by a.panelist_key, a.platform_id;
--select * from #usage_prep where   platform_type = 'All'

insert into #usage_prep
select a.panelist_key,
	0 as platform_id,
	'All' as platform_type,
	min(date_id) as min_date, max(date_id) as max_date, count(distinct date_id) as ct_dates,
	(datediff(day, min(Date_ID), max(Date_ID))+1) as span_dates,
	cast(count(distinct date_id) as decimal(6,3))/(datediff(day, min(date_id), max(date_id))+1) as utilization
from run.[UserByDay_Total] a
left outer join [PROJECTNAME].dbo.Panelist_Exclusion e
	on a.panelist_key = e.panelist_key 	
where e.panelist_key is null and a.date_id is not null
group by a.panelist_key;

create index usage_prep1 on #usage_prep (panelist_key);
create index usage_prep2 on #usage_prep (platform_type);
create index usage_prep3 on #usage_prep (platform_id);

--select min(date_id) from run.user_qualification where daily_qualified = 1 
-- declare @global_min_qual_days   int = 18; -- 16 instead of 18, because ends are trimmed.
begin try drop table #usage_summary end try begin catch end catch;

select a.panelist_id_vendor,
	a.panelist_key,
	a.ct_platforms,
	a.ct_devices,
	a.pc_flag,
	a.mobile_flag,
	d.min_date,
	d.max_date,
	d.ct_dates,
	d.utilization,
	case when (m.ct_dates >= (@global_min_qual_days-2) and m.span_dates >= (@global_min_span_days-2)) OR (p.ct_dates >= (@global_min_qual_days-2) AND p.span_dates >= (@global_min_span_days-2)) then 1 else 0 end as qual_dual, --OR logic. also 2 less days uses when userbyday
	case when (m.ct_dates >= (@global_min_qual_days-2) and m.span_dates >= (@global_min_span_days-2)) then 1 else 0 end as qual_mobile,
	case when (p.ct_dates >= (@global_min_qual_days-2) and p.span_dates >= (@global_min_span_days-2)) then 1 else 0 end as qual_pc,
	cast(d.utilization as numeric(10,2)) as util_all,
	isnull(cast(m.utilization as numeric(10,2)),0) as util_mobile,
	isnull(cast(p.utilization as numeric(10,2)),0) as util_pc
into #usage_summary
from #participation_summary a
inner join #usage_prep d
	on a.panelist_key=d.panelist_key
	and d.platform_type = 'All'
left outer join #usage_prep m
	on a.panelist_key=m.panelist_key
	and m.platform_type = 'Mobile'
left outer join #usage_prep p
	on a.panelist_key=p.panelist_key
	and p.platform_type = 'PC';

--select count(distinct panelist_key) from ref.panelist_segmentation -- 254
--select top 100 * from rpt.panelist_segmentation
--select distinct segmentation_name, segment_name from ref.panelist_segmentation

-- PROCESS FOR STRICTER PLATFORM BASED QUALIFICATION
--**PRIOR TO RUNNING THE SCRIPT BELOW...NEED TO MAKE PANELIST_SEGMENTATION FILE
---------------------------------------------------------------------------------------------------
-- select top 100 * from #user_platform_qual_day order by 1, 2;
--select count(distinct panelist_key) from #user_platform_qual_day; 14
--select count(distinct month_id) from #user_platform_qual_day; 92


-- drop table #user_platform_qual_day;
begin try drop table #user_platform_qual_day end try begin catch end catch;

select b.panelist_id_vendor, a.panelist_key, p.segmentation_name, p.segment_name, a.date_id, a.week_id, a.month_id, a.weekly_qualified, a.monthly_qualified,
	case when b.qual_dual = 1 then 0 else null end as platform_id_dual,
	case when b.qual_mobile = 1 then 1 else null end as platform_id_mobile,
	case when b.qual_pc = 1 then 2 else null end as platform_id_pc
into #user_platform_qual_day
from [PROJECTNAME].run.User_Qualification a
inner join #usage_summary b --- ultimately, use participation, instead of usage, because usage trims first and last day. --1439
	on a.panelist_key=b.panelist_key
	and a.daily_qualified=1
inner join [PROJECTNAME].ref.panelist_segmentation p 
	on a.panelist_key = p.panelist_key
--left outer join [PROJECTNAME].dbo.Panelist_Exclusion e 
	--on a.panelist_key = e.panelist_key 	
--where e.panelist_key is null 
;
--select min(date_id) from #user_platform_qual_day
--select max(date_id) from #user_platform_qual_day
---- (73408 row(s) affected)
---select top 100 * from  [PROJECTNAME].dbo.Panelist_Exclusion 

delete from #user_platform_qual_day where (platform_id_dual is null and platform_id_mobile is null and platform_id_pc is null);
create index user_platform_qual_day1 on #user_platform_qual_day(panelist_key);
create index user_platform_qual_day2 on #user_platform_qual_day(date_id);
create index user_platform_qual_day3 on #user_platform_qual_day(platform_id_dual);
create index user_platform_qual_day4 on #user_platform_qual_day(platform_id_mobile);
create index user_platform_qual_day5 on #user_platform_qual_day(platform_id_pc);
-------------------------------------------------------------------------------
-- select * from #user_platform_qual_day;
-- select * from #user_platform_qual_week;
begin try drop table #user_platform_qual_week end try begin catch end catch;
select panelist_key, segmentation_name, segment_name, week_id, platform_id_dual, platform_id_mobile, platform_id_pc
into #user_platform_qual_week
from #user_platform_qual_day a
where weekly_qualified=1
group by panelist_key, segmentation_name, segment_name, week_id, platform_id_dual, platform_id_mobile, platform_id_pc;
-- (10563 row(s) affected)
--select count(distinct week_id) from #user_platform_qual_week
--select distinct week_id from #user_platform_qual_week

delete from #user_platform_qual_week where (platform_id_mobile is null and platform_id_pc is null); -- in case week level non-quals exist where days did not
create index user_platform_qual_week1 on #user_platform_qual_week(panelist_key);
create index user_platform_qual_week2 on #user_platform_qual_week(week_id);

-- select * from #user_platform_qual_month; 
-- select * from #user_platform_qual_day where panelist_key = 576;
begin try drop table #user_platform_qual_month end try begin catch end catch;
select panelist_key, segmentation_name, segment_name, month_id, platform_id_dual, platform_id_mobile, platform_id_pc
into #user_platform_qual_month
from #user_platform_qual_day a
where monthly_qualified=1
group by panelist_key, segmentation_name, segment_name, month_id, platform_id_dual, platform_id_mobile, platform_id_pc;
-- (2350 row(s) affected)

delete from #user_platform_qual_month where (platform_id_mobile is null and platform_id_pc is null);
create index user_platform_qual_month1 on #user_platform_qual_month(panelist_key);
create index user_platform_qual_month2 on #user_platform_qual_month(month_id);

-- select * from #user_platform_qual_Day where panelist_key=2;
-- select * from #user_platform_qual_month order by panelist_key;

-- QUAL DAYS
----------------------
-- select * from #user_platform_qual_Day_Ct;
begin try drop table #user_platform_qual_day_ct end try begin catch end catch;
select platform_id_dual as platform_id, panelist_key, segmentation_name, segment_name, count(distinct date_id) as dp_qualdays
into #user_platform_qual_day_ct
from #user_platform_qual_day
where platform_id_dual is not null
group by panelist_key, segmentation_name, segment_name, platform_id_dual; 

/*
insert into #user_platform_qual_day_ct
select platform_id_dual as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_dual is not null
group by panelist_key, platform_id_dual; */

insert into #user_platform_qual_day_ct
select platform_id_mobile as platform_id, panelist_key, segmentation_name, segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_mobile is not null
group by panelist_key, segmentation_name, segment_name, platform_id_mobile;

/*
insert into #user_platform_qual_day_ct
select platform_id_mobile as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_mobile is not null
group by panelist_key, platform_id_mobile;
*/

insert into #user_platform_qual_day_ct
select platform_id_pc as platform_id, panelist_key, segmentation_name, segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_pc is not null
group by panelist_key, segmentation_name, segment_name, platform_id_pc;

/*
insert into #user_platform_qual_day_ct
select platform_id_pc as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct date_id) as dp_qualdays
from #user_platform_qual_day
where platform_id_pc is not null
group by panelist_key, platform_id_pc;
-- select * from #user_platform_qual_day_ct;
*/

-- drop table #device_topline_qual_days;
begin try drop table #device_topline_qual_days end try begin catch end catch;

select platform_id, segmentation_name, segment_name, count(distinct panelist_key) as dp, sum(dp_qualdays) as dp_qualdays
into #device_topline_qual_days
from #user_platform_qual_day_ct
group by platform_id, segmentation_name, segment_name;
-- select * from #device_topline_qual_days where segmentation_name = 'Total'

-- QUAL WEEKS
----------------------
-- select * from #user_platform_qual_day_ct;
begin try drop table #user_platform_qual_week_ct end try begin catch end catch;

select platform_id_dual as platform_id, panelist_key, segmentation_name, segment_name, count(distinct week_id) as wp_qualweeks
into #user_platform_qual_week_ct
from #user_platform_qual_week
where platform_id_dual is not null
group by panelist_key, segmentation_name, segment_name, platform_id_dual;

/*
insert into #user_platform_qual_week_ct
select platform_id_dual as platform_id, panelist_key,  'Total' as segmentation_name, 'Total' as segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_dual is not null
group by panelist_key, platform_id_dual;
*/

insert into #user_platform_qual_week_ct
select platform_id_mobile as platform_id, panelist_key, segmentation_name, segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_mobile is not null
group by panelist_key, segmentation_name, segment_name, platform_id_mobile;

/*
insert into #user_platform_qual_week_ct
select platform_id_mobile as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_mobile is not null
group by panelist_key, platform_id_mobile;
*/

insert into #user_platform_qual_week_ct
select platform_id_pc as platform_id, panelist_key, segmentation_name, segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_pc is not null
group by panelist_key, segmentation_name, segment_name, platform_id_pc;

/*
insert into #user_platform_qual_week_ct
select platform_id_pc as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct week_id) as wp_qualweeks
from #user_platform_qual_week
where platform_id_pc is not null
group by panelist_key, platform_id_pc;
*/


begin try drop table #device_topline_qual_weeks end try begin catch end catch;

select platform_id, segmentation_name, segment_name, count(distinct panelist_key) as wp, sum(wp_qualweeks) as wp_qualweeks
into #device_topline_qual_weeks
from #user_platform_qual_week_ct
group by platform_id, segmentation_name, segment_name;

-- QUAL MONTHS
----------------------
-- select * from #user_platform_qual_month_ct;
-- select * from #user_platform_qual_month;
begin try drop table #user_platform_qual_month_ct end try begin catch end catch;

select platform_id_dual as platform_id, panelist_key, segmentation_name, segment_name, count(distinct month_id) as mp_qualmonths
into #user_platform_qual_month_ct
from #user_platform_qual_month
where platform_id_dual is not null
group by panelist_key, segmentation_name, segment_name, platform_id_dual;

/*
insert into #user_platform_qual_month_ct
select platform_id_dual as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_dual is not null
group by panelist_key, platform_id_dual;
*/

insert into #user_platform_qual_month_ct
select platform_id_mobile as platform_id, panelist_key, segmentation_name, segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_mobile is not null
group by panelist_key, segmentation_name, segment_name, platform_id_mobile;

/*
insert into #user_platform_qual_month_ct
select platform_id_mobile as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_mobile is not null
group by panelist_key, platform_id_mobile;
*/

insert into #user_platform_qual_month_ct
select platform_id_pc as platform_id, panelist_key, segmentation_name, segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_pc is not null
group by panelist_key, segmentation_name, segment_name, platform_id_pc;

/*
insert into #user_platform_qual_month_ct
select platform_id_pc as platform_id, panelist_key, 'Total' as segmentation_name, 'Total' as segment_name, count(distinct month_id) as mp_qualmonths
from #user_platform_qual_month
where platform_id_pc is not null
group by panelist_key, platform_id_pc;
*/

-- select * from #device_topline_qual_months;
-- drop table #device_topline_qual_months;
begin try drop table #device_topline_qual_months end try begin catch end catch;

select platform_id, segmentation_name, segment_name, count(distinct panelist_key) as mp, sum(mp_qualmonths) as mp_qualmonths
into #device_topline_qual_months
from #user_platform_qual_month_ct
group by platform_id, segmentation_name, segment_name;


-- COMBINE TOPLINE FOR DAY, WEEK, AND MONTH

begin try drop table #device_topline_qual end try begin catch end catch;

select d.platform_id, d.segmentation_name, d.segment_name, dp, dp_qualdays, wp, wp_qualweeks, mp, mp_qualmonths
into #device_topline_qual
from #device_topline_qual_days d
inner join #device_topline_qual_weeks w
	on d.platform_id=w.platform_id
	and d.segmentation_name = w.segmentation_name
	and d.segment_name = w.segment_name
inner join #device_topline_qual_months m
	on d.platform_id=m.platform_id
	and d.segmentation_name = m.segmentation_name
	and d.segment_name = m.segment_name
;

create index device_topline_qual1 on #device_topline_qual(platform_id);
create index device_topline_qual3 on #device_topline_qual(segmentation_name);
create index device_topline_qual4 on #device_topline_qual(segment_name);

-- select * from #device_topline_qual where segmentation_name = 'Total'
---------------------------------------------------------------------------------------------------
-- 2) Taxonomy Lookup Tables
---------------------------------------------------------------------------------------------------

begin try drop table #source_id_map end try begin catch end catch;
select source_id, source_name
into #source_id_map
from [PROJECTNAME].Ref.Taxonomy_Ecosystem
group by source_id, source_name; 

-- select * from #cat_id_map where cat_id = 2200;
begin try drop table #cat_id_map_prep end try begin catch end catch;
select cat_id, category
into #cat_id_map_prep
from [PROJECTNAME].Ref.Taxonomy_Ecosystem
group by cat_id, category;

insert into #cat_id_map_prep select 2010, '* Total Ecosystem *'; -- name this the ecosystem that you are running (ex: Skincare for PGSkincare)

begin try drop table #cat_id_map end try begin catch end catch;
select cat_id, category
into #cat_id_map
from #cat_id_map_prep
group by cat_id, category;

begin try drop table #subcat_id_map end try begin catch end catch;
select subcat_id, subcategory
into #subcat_id_map
from [PROJECTNAME].Ref.Taxonomy_Ecosystem
group by subcat_id, subcategory;

insert into #subcat_id_map select 0, '[ Total Category ]';


----------------------------------
--- CREATE CONTENT CATEGORY LOOKUP
----------------------------------
begin try drop table #content_cat_lookup end try begin catch end catch;

-- may need to repeat if there are more than one content categories
select content_cat_id, content_category as content_category_name
into #content_cat_lookup 
from [PROJECTNAME].Ref.Taxonomy_Ecosystem
group by content_cat_id, content_category;

insert into #content_cat_lookup
select 0 as content_cat_id, ' Total Baby ' as content_category_name;

---------------------------------------------------------------
-- CATEGORY/SUBCATEGORY/CONTENTCAT DUPE CHECKS!!!!!!!!!!!!!!!!!
-- select source_id, count(*) from #source_id_map group by source_id order by 2 desc; -- OK. No dupes.
-- select cat_id, count(*) from #cat_id_map group by cat_id order by 2 desc; -- OK. No dupes.
-- select subcat_id, count(*) from #subcat_id_map group by subcat_id order by 2 desc; -- OK. No dupes.
-- select content_cat_id, count(*) from #content_cat_lookup group by content_cat_id order by 2 desc; -- OK. No dupes.
---------------------------------------------------------------
begin try drop table #property_map end try begin catch end catch;
select bdg_display_key, bdg_display
into #property_map
from [PROJECTNAME].Ref.Taxonomy_Ecosystem
group by bdg_display_key, bdg_display;

create index source_id_map1 on #source_id_map (source_id);
create index cat_id_map1 on #cat_id_map (cat_id);
create index subcat_id_map1 on #subcat_id_map (subcat_id);
create index content_cat_lookup1 on #content_cat_lookup (content_cat_id);


---------------------------------------------------------------------------------------------------
-- STEP 3) CREATE DAILY RAW METRICS
---------------------------------------------------------------------------------------------------
-- PERSON LEVEL AGG
-- Create Daily table to analyze TOTAL & CAT & subcat usage days on
-----------------------------
-----------------------------
--PULL IN SEGMENTS
-----------------------------
--select top 100 * from [PROJECTNAME].rpt.Person_Ecosystem_Category
--select top 100 * from [PROJECTNAME].rpt.Panelist_Segmentation 
--select top 100 * from #temp_person_agg_cat_total_segments

begin try drop table #temp_person_agg_cat_total_segments end try begin catch end catch;
select a.*, b.segmentation_name, b.segment_name
into #temp_person_agg_cat_total_segments
from [PROJECTNAME].rpt.Person_Ecosystem_Category a
join [PROJECTNAME].ref.Panelist_Segmentation b
	on a.panelist_key = b.panelist_key
	and report_level <> 0  --not at the property level
;
-- 844296 rows affected

-- select * from #temp_person_agg_Cat_Total_segments;
begin try drop table #temp_panel_agg_cat_total_segments end try begin catch end catch;
select segmentation_name, segment_name, platform_id, digital_type_id, report_level, content_Cat_id, cat_hash, SubCat_Hash, Property_Key,
	count(distinct panelist_key) as raw_persons,
	sum(raw_usage_days) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
into  #temp_panel_agg_cat_total_segments
from  #temp_person_agg_cat_total_segments
group by  segmentation_name, segment_name, platform_id, digital_type_id, report_level, content_Cat_id, cat_hash, SubCat_Hash, Property_Key;

/*
insert into  #temp_panel_agg_cat_total_segments
select 'Total' as segmentation_name, 'Total' as segment_name, platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, Property_Key,
	count(distinct panelist_key) as raw_persons,
	sum(raw_usage_days) as raw_usage_days,
	sum(raw_visits) as raw_visits,
	sum(raw_minutes) as raw_minutes
from [PROJECTNAME].rpt.Person_Ecosystem_Category 
where report_level <> 0
group by platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, Property_Key;
*/

--select top 100 * from #temp_panel_agg_cat_total_segments 
--select top 1000 * from #temp_panel_agg_cat_total_segments where segment_name = 'Total' and cat_hash <> 0

create index standard_metrics_daily1 on #temp_panel_agg_cat_total_segments(platform_id);
create index standard_metrics_daily2 on #temp_panel_agg_cat_total_segments(digital_type_id);
create index standard_metrics_daily3 on #temp_panel_agg_cat_total_segments(cat_hash);
create index standard_metrics_daily4 on #temp_panel_agg_cat_total_segments(SubCat_Hash);
create index standard_metrics_daily5 on #temp_panel_agg_cat_total_segments(Property_Key);
create index standard_metrics_daily6 on #temp_panel_agg_cat_total_segments(report_level);
create index standard_metrics_daily7 on #temp_panel_agg_cat_total_segments(segmentation_name);
create index standard_metrics_daily8 on #temp_panel_agg_cat_total_segments(segment_name);


---------------------------------------------------------------------------------------------------
-- STEP 5) CREATE USAGE WEEKS AND USAGE MONTHS
---------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
-- USAGE WEEKS
-----------------------------------------------------------------------------------

-- select * From #temp_person_qual_week_used_segments;
begin try drop table #temp_person_qual_week_used_segments end try begin catch end catch;
select a.*, b.segmentation_name, b.segment_name
into #temp_person_qual_week_used_segments
from [PROJECTNAME].rpt.Person_Ecosystem_Category_Usage_weeks a
join [PROJECTNAME].ref.panelist_segmentation b
	on a.panelist_key = b.panelist_key
	and report_level <> 0
;
--select top 100 * from #temp_person_qual_week_used_segments

begin try drop table #standard_metrics_wu_useweeks_weighted end try begin catch end catch;
select platform_id, digital_type_id, report_level, cat_hash, content_cat_id, SubCat_Hash, Property_Key,
	segmentation_name, segment_name,
	 sum(wu_useweeks) as wu_useweeks, count(distinct panelist_key) as wu
into #standard_metrics_wu_useweeks_weighted
from #temp_person_qual_week_used_segments
group by platform_id, digital_type_id, report_level, cat_hash, content_cat_id, subcat_hash, Property_Key,
    segmentation_name, segment_name
;
-- 13333 rows affected 

/*

insert into #standard_metrics_wu_useweeks_weighted
select platform_id, digital_type_id, report_level, cat_hash, content_cat_id, SubCat_Hash, Property_Key,
	'Total' as segmentation_name, 'Total' as segment_name,
	sum(wu_useweeks) as wu_useweeks, count(distinct panelist_key) as wu
from [PROJECTNAME].rpt.Person_Ecosystem_Category_Usage_weeks
where report_level <> 0
group by platform_id, digital_type_id, report_level, content_Cat_id, cat_hash, SubCat_Hash, Property_Key;
*/


-- select * from #standard_metrics_wu_useweeks_weighted;
-- select * from #standard_metrics_wu_useweeks_weighted where segmentation_name = 'total';
--------------------------------------------------------------------------------------------------------------
-- USAGE MONTH
--------------------------------------------------------------------------------------------------------------

-------------------------------

begin try drop table #temp_person_qual_month_used_segments end try begin catch end catch;
select a.*, b.segmentation_name, segment_name
into #temp_person_qual_month_used_segments
from [PROJECTNAME].rpt.Person_Ecosystem_Category_Usage_Months a
join [PROJECTNAME].ref.panelist_segmentation b
	on a.panelist_key = b.panelist_key
	and report_level <> 0
;


begin try drop table #unified_standard_metrics_mu_usemonths_weighted end try begin catch end catch;
select platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, Property_Key, 
	segmentation_name, segment_name,  
	sum(mu_usemonths) as mu_usemonths, count(distinct panelist_key) as mu
into #unified_standard_metrics_mu_usemonths_weighted 
from #temp_person_qual_month_used_segments 
group by platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, Property_Key,
	segmentation_name, segment_name
;

/*
insert into #unified_standard_metrics_mu_usemonths_weighted
select platform_id, digital_type_id, report_level, content_Cat_id, cat_hash, SubCat_Hash, Property_Key, 
	'Total' as segmentation_name, 'Total' as segment_name,
	sum(mu_usemonths) as mu_usemonths, count(distinct panelist_key) as mu
from [PROJECTNAME].rpt.Person_Ecosystem_Category_Usage_Months 
where report_level <> 0
group by platform_id, digital_type_id, report_level, content_cat_id, cat_hash, SubCat_Hash, Property_Key
;
*/

-- BE SURE WEEK AND MONTH USAGE ARE INDEXED
-- We really should just use tax_id... as the map, so we don't have to index on all 4 levels of taxonomy/reporting structure.
create index standard_metrics_wu_useweeks1 on #standard_metrics_wu_useweeks_weighted(platform_id);
create index standard_metrics_wu_useweeks2 on #standard_metrics_wu_useweeks_weighted(digital_type_id);
create index standard_metrics_wu_useweeks3 on #standard_metrics_wu_useweeks_weighted(cat_hash);
create index standard_metrics_wu_useweeks4 on #standard_metrics_wu_useweeks_weighted(SubCat_Hash);
create index standard_metrics_wu_useweeks5 on #standard_metrics_wu_useweeks_weighted(Property_Key);
create index standard_metrics_wu_useweeks6 on #standard_metrics_wu_useweeks_weighted(report_level);
create index standard_metrics_wu_useweeks8 on #standard_metrics_wu_useweeks_weighted(segmentation_name);
create index standard_metrics_wu_useweeks9 on #standard_metrics_wu_useweeks_weighted(segment_name);

create index standard_metrics_mu_usemonths1 on #unified_standard_metrics_mu_usemonths_weighted(platform_id);
create index standard_metrics_mu_usemonths2 on #unified_standard_metrics_mu_usemonths_weighted(digital_type_id);
create index standard_metrics_mu_usemonths3 on #unified_standard_metrics_mu_usemonths_weighted(cat_hash);
create index standard_metrics_mu_usemonths4 on #unified_standard_metrics_mu_usemonths_weighted(SubCat_Hash);
create index standard_metrics_mu_usemonths5 on #unified_standard_metrics_mu_usemonths_weighted(Property_Key);
create index standard_metrics_mu_usemonths6 on #unified_standard_metrics_mu_usemonths_weighted(report_level);
create index standard_metrics_mu_usemonths8 on #unified_standard_metrics_mu_usemonths_weighted(segmentation_name);
create index standard_metrics_mu_usemonths9 on #unified_standard_metrics_mu_usemonths_weighted(segment_name);



---------------------------------------------------------------------------------------------------
-- STEP 6) INTEGRATE EVERYTHING... FO SHO
---------------------------------------------------------------------------------------------------

begin try drop table #temp_total_internet_minutes end try begin catch end catch;
select segmentation_name, segment_name, platform_id, content_cat_id, digital_type_id, raw_minutes as du_minutes
into #temp_total_internet_minutes
from #temp_panel_agg_cat_total_segments 
where report_level= 3;


--need to double check this
insert into #temp_total_internet_minutes
select segmentation_name, segment_name, platform_id, content_cat_id, digital_type_id, raw_minutes as du_minutes
from #temp_panel_agg_cat_total_segments 
where report_level=8;   --total internet line for content categories **/

create index temp_total_internet_minutes1 on #temp_total_internet_minutes(platform_id);
create index temp_total_internet_minutes2 on #temp_total_internet_minutes(digital_type_id);
create index temp_total_internet_minutes3 on #temp_total_internet_minutes(segmentation_name);
create index temp_total_internet_minutes4 on #temp_total_internet_minutes(segment_name);
create index temp_total_internet_minutes5 on #temp_total_internet_minutes(content_cat_id);



begin try drop table #temp_standard_metrics_booya end try begin catch end catch;
select
	a.platform_id, 
	a.digital_type_id, 
	a.segmentation_name, 
	a.segment_name, 
	a.report_level,  
	a.cat_hash, 
	a.SubCat_Hash, 
	a.Property_Key,
	a.content_cat_id,
	p.dp, 
	a.raw_persons as du,    
	p.dp_qualdays, 
	raw_usage_days as du_usedays, 
	raw_visits as du_visits,
	cast(a.raw_minutes as numeric(30,2)) as du_mins,
	cast(ti.du_minutes as numeric(30,2)) as du_mins_ti,
	p.wp_qualweeks, 
	wu.wu_useweeks,
	p.mp_qualmonths, 
	mu.mu_usemonths
into #temp_standard_metrics_booya
from #temp_panel_agg_cat_total_segments a  ---daily table 
inner join #device_topline_qual p  --should only be 3 rows  --select top 1000 * from #device_topline_qual
	on a.platform_id=p.platform_id
	and a.segmentation_name = p.segmentation_name
	and a.segment_name = p.segment_name
inner join #standard_metrics_wu_useweeks_weighted wu  
	on a.platform_id=wu.platform_id
	and a.digital_type_id=wu.digital_type_id
	and a.report_level=wu.report_level
	and a.cat_hash=wu.cat_hash
	and a.SubCat_Hash=wu.SubCat_Hash
	and a.Property_Key = wu.Property_Key
	and a.segmentation_name = wu.segmentation_name
	and a.segment_name = wu.segment_name
	and a.content_cat_id = wu.content_cat_id
inner join #unified_standard_metrics_mu_usemonths_weighted mu  
	on a.platform_id=mu.platform_id
	and a.digital_type_id=mu.digital_type_id
	and a.report_level=mu.report_level
	and a.cat_hash=mu.cat_hash
	and a.SubCat_Hash=mu.SubCat_Hash
	and a.Property_Key = mu.Property_Key
	and a.segmentation_name = mu.segmentation_name
	and a.segment_name = mu.segment_name
	and a.content_cat_id = mu.content_cat_id
inner join #temp_total_internet_minutes ti -- should only have 8 rows --select top 100 * from #temp_total_internet_minutes order by  platform_id, digital_type_id, content_cat_id, segment_name, segmentation_name
	on a.platform_id=ti.platform_id
	and a.digital_type_id=ti.digital_type_id
	and a.segment_name = ti.segment_name
	and a.segmentation_name = ti.segmentation_name
	and a.content_cat_id = ti.content_cat_id
where a.report_level != 0;


----------------------------------------
insert into #variable_names select 6, 'ReportLevel', 8, 'Total Content';

--select top 100 * from #temp_standard_metrics_booya
-- select top 100 * from [PROJECTNAME].dbo.Ecosystem_Standard_Metrics_Report where subcategory like '%youtube%';
-- select * from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report;
begin try drop table [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report end try begin catch end catch;
select 
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type,  
	case when g.val_name is null then 'Total' else g.val_name end as report_level, 
	cc.content_category_name,
	segmentation_name, 
	segment_name, 
	category, 
	subcategory,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
into [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report
from #temp_standard_metrics_booya a
left outer join #variable_names c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join #variable_names d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
left outer join #cat_id_map e
	on a.cat_hash = e.cat_id
left outer join #subcat_id_map f
	on a.subcat_hash = f.subcat_id
left outer join #variable_names g
	on a.report_level=g.val_id
	and g.var_name = 'reportlevel'
inner join #content_cat_lookup cc
	on a.content_cat_id = cc.content_cat_id;
	-- 13207 rows affected


/*
INSERT INTO [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report
select
	isnull(c.val_name,'All Devices') as device_type, 
	isnull(d.val_name,'All Digital') as digital_type, 
	case when g.val_name is null then 'Total' else g.val_name end as report_level,
	cc.content_category_name,
	segmentation_name,
	segment_name,  
	'* Total Internet * ' as category, 
	'* Total Internet * ' as subcategory,
	dp, 
	du, 
	dp_qualdays, 
	du_usedays, 
	du_visits, 
	du_mins, 
	du_mins_ti, 
	wp_qualweeks, 
	wu_useweeks, 
	mp_qualmonths, 
	mu_usemonths
from #temp_standard_metrics_booya a
left outer join [PROJECTNAME].[Ref].[Variable_Names] c
	on a.platform_id=c.val_id
	and c.var_name = 'platform_id'
left outer join [PROJECTNAME].[Ref].[Variable_Names] d
	on a.digital_type_id=d.val_id
	and d.var_name = 'digital_type_id'
left outer join [PROJECTNAME].[Ref].[Variable_Names] g
	on a.report_level=g.val_id
	and g.var_name = 'reportlevel'
inner join #content_cat_lookup cc
	on a.content_cat_id = cc.content_cat_id
WHERE a.report_level in (3,8);
;
*/

update [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report set category = '* Total Ecosystem *' where category = '* Total Internet *';
update [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report set category = '* Total Ecosystem *' where category is null;  

--select distinct device_type, digital_type from [BayerNappy].dbo.Ecosystem_Standard_Metrics_Property
---------------------------------------------------------------------------------------------------
-- STEP 7) EXPORT UNIFIED STANDARD METRICS... GET IT OUT
---------------------------------------------------------------------------------------------------

-- EXPORT 1
-- This selection exports Total, Cat, & Subcat
-- drop table [PROJECTNAME].rpt.unified_ecosystem_standard_metrics_report ;

select 
	device_type, 
	digital_type, 
	case when report_level='Total' then '* Total *' else report_level end as report_level, 
	content_Category_name,
	case when report_level='Total' then '* Total Ecosystem *' else case when category='XXXAdult' then 'XXX Adult' else category end end as category,
	subcategory,
	--case when subcategory is null and report_level !='Total' then '[ Total Category ]' else subcategory end as subcategory, 
	dp, du, dp_qualdays, du_usedays, du_visits, du_mins, du_mins_ti, wp_qualweeks, wu_useweeks, mp_qualmonths, mu_usemonths,
	segmentation_name, segment_name
from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report
where subcategory <> '* Total Internet *'
order by device_type, digital_type, report_level, content_category_name, category, subcategory, segmentation_name, segment_name, du desc;


--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
-- QA
--------------------------------------------------------------------------------------------------
-- select * from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report where mp_qualmonths < mu_usemonths;
-- select * from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report where dp_qualdays < du_usedays;
-- select * from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report where wp_qualweeks < wu_useweeks;

-- select * from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report where category = '* Total Ecosystem *';
-- select distinct(report_level) from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report;

------------------------------------------------
-- Check if unified output match with ecosystem 
------------------------------------------------
/**
-- 1. Create prep tables:
-- select * from #unified_total order by 1,2,3,4; 
begin try drop table #unified_total end try begin catch end catch;
select device_type, digital_type, report_level, content_category_name, dp, du, dp_qualdays,
du_visits,du_mins,du_mins_ti,wp_qualweeks,wu_useweeks,mp_qualmonths,mu_usemonths
into #unified_total
from [PROJECTNAME].rpt.Unified_Ecosystem_Standard_Metrics_Report 
where category = '* Total Ecosystem *' and subcategory = '[ Total Category ]' 
and segmentation_name = 'Total' and segment_name = 'Total'
--and device_type in ('Mobile', 'PC')
order by 1,2,3,4;

-- select * from #ecosystem_total order by 1,2,3,4;
begin try drop table #ecosystem_total end try begin catch end catch;
select device_type, digital_type, report_level, content_category_name, dp, du, dp_qualdays,
du_visits,du_mins,du_mins_ti,wp_qualweeks,wu_useweeks,mp_qualmonths,mu_usemonths
into #ecosystem_total
from [PROJECTNAME].rpt.Ecosystem_Standard_Metrics_Report
where category = '* Total Ecosystem *' and subcategory = '* Total Ecosystem *' 
order by 1,2,3,4;

-- 2. Create QA_Flags by content_category_name (if joins not working - check if there're extra spaces in content_category_name)
begin try drop table #QA_Flags end try begin catch end catch;
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
into #QA_Flags
from #unified_total a
inner join #ecosystem_total b 
on a.content_category_name = b.content_category_name
and a.device_type = b.device_type and a.digital_type = b. digital_type 
where a.content_category_name = ' Total Baby ' 
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'All Baby Hygiene ' and b.content_category_name = ' All Baby Hygiene'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'All Diaper ' and b.content_category_name = ' All Diaper'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'Baby Hygiene ' and b.content_category_name = ' Baby Hygiene'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'Diaper ' and b.content_category_name = ' Diaper'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'Diaper Rash ' and b.content_category_name = ' Diaper Rash'
UNION
select a.device_type, a.digital_type, a.report_level, a.content_category_name, 
(a.dp-b.dp) as dp, 
(a.du-b.du) as du, 
(a.dp_qualdays - b.dp_qualdays) as dp_qualdays, 
(a.du_visits - b.du_visits) as du_visits,
(a.du_mins - b.du_mins) as du_mins, 
(a.wp_qualweeks - b.wp_qualweeks) as wp_qualweeks, 
(a.wu_useweeks - b.wu_useweeks) as wu_useweeks,
(a.mp_qualmonths - b.mp_qualmonths) as mp_qualmonths, 
(a.mu_usemonths - b.mu_usemonths) as mu_usemonths 
from #unified_total a
inner join #ecosystem_total b 
on a.device_type = b.device_type and a.digital_type = b. digital_type 
and a.content_category_name = 'General Baby ' and b.content_category_name = ' General Baby';

-- 3. QA Output (0 if match; otherwise will show difference):
select * from #QA_Flags order by 1,2,3,4;
**/

-- END OF FILE --
