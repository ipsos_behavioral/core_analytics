---------------------------------------------------------------------------------------------------
-- URL Splitter
---------------------------------------------------------------------------------------------------
-- Dylan	(2018/06/22)
-- Jimmy	(2018/06/25)
---------------------------------------------------------------------------------------------------

USE [GoogleAuto];

/****** Script for SelectTopNRows command from SSMS  ******/
begin try drop table #url_query_split end try begin catch end catch;

begin try drop table #url_protocol_split end try begin catch end catch;
begin try drop table #NoProtocolTable end try begin catch end catch;
begin try drop table #DomainSplitTable end try begin catch end catch;
begin try drop table #DirectorySplitTable end try begin catch end catch;
begin try drop table #PageSplitTable end try begin catch end catch;
begin try drop table #ProtocolSplitTable end try begin catch end catch;

-- select top 100 * from #url_query_split;
-- drop table #url_query_split;
select url_key, url,
	-- charindex('?',url) as index_num, charindex('#',url) as index_num2,
	case when charindex('?',url)>0 then left(lower(url), charindex('?',url)-1)
		else case when charindex('#',url)>0 then left(lower(url), charindex('#',url)-1)		
		else lower(url) end end as url_not_query,
		-- Note for Dylan. Want url, rather than reverse(url) for handling, because we want to exclude full query (starting with first instance of ?)
		-- Example: clicks.att.com/OCT/eTrac?EMAIL_ID=1831812124&finalURL=https://www.att.com/esupport/?source=EACQM0ftr0000000E&wtExtndSource=CPRU_X026EH
	case when charindex('?',url)>0 then right(lower(url), len(url)-charindex('?',url))
		else case when charindex('#',url)>0 then right(lower(url), len(url)-charindex('#',url))
		else '' end end as query
INTO #url_query_split
FROM ref.URL;
-- (3099812 row(s) affected)
-- 1:01

-- SELECT Protocol, Max(Place) As Place FROM #url_protocol_split GROUP BY Protocol ORDER BY Protocol

-- select top 100 * from #url_protocol_split;
-- drop table #url_protocol_split;
SELECT url_key, url,
	case when charindex('://', url_not_query)>0 then right(url_not_query, len(url_not_query)-charindex('://',url_not_query)-1)
		else url_not_query end as url_not_protocol,
	case when charindex('://',url_not_query)>0 then left(url_not_query, charindex('://',url_not_query)-1)
		else '' end as protocol,
		-- Note for Dylan. Script works fine for 1000 example records, but didn't work for larger dataset (~3MM... needs edit)
	query
INTO #url_protocol_split
FROM #url_query_split;

select *
into dbo.jk_url_protocol_split
from #url_protocol_split;


-- select * from #DomainSplitTable;
--SELECT * FROM #ProtocolSplitTable
SELECT
	[URL],
	Protocol,
	CASE WHEN  CHARINDEX('/', NoProtocolURl) = 0
		THEN NoProtocolURL
		ELSE SUBSTRING(NoProtocolURL, 1, CHARINDEX('/', NoProtocolURl) - 1) END AS Domain, 

	CASE WHEN CHARINDEX('/', NoProtocolURl) = 0
		THEN NULL
		ELSE SUBSTRING(NoProtocolURL, CHARINDEX('/', NoProtocolURL) + 1, 300) END AS NoDomainURL,

	Query
INTO #DomainSplitTable
FROM #ProtocolSplitTable;

-- select * from #DirectorySplitTable;
-- SELECT TOP(1000) * FROM #DomainSplitTable;
SELECT
	[URL],
	Protocol,
	Domain,
	NoDomainURL,

	CASE WHEN NoDomainURL IS NULL
		THEN 'No Directory'
		ELSE CASE WHEN CHARINDEX('/', LEFT(NoDomainURL, LEN(NoDomainURL) - CHARINDEX('/',REVERSE(NoDomainURL)))) = 0
			THEN 'No Directory'
			ELSE LEFT(NoDomainURL, LEN(NoDomainURL) - CHARINDEX('/',REVERSE(NoDomainURL))) END 
			END AS Directory,

	CASE WHEN NoDomainURL IS NULL
		THEN 'No Page'
		ELSE CASE WHEN CHARINDEX('/',REVERSE(NoDomainURL))-1 >= 0 
			THEN RIGHT(NoDomainURL, CHARINDEX('/',REVERSE(NoDomainURL))-1)
			ELSE NoDomainURL END 
		END AS [Page],

	Query

	INTO #DirectorySplitTable
FROM #DomainSplitTable

-- SELECT TOP(1000) [URL], [Protocol], [Domain], [Directory], [Page], [Query]  FROM #DirectorySplitTable;0

SELECT
	[URL],
	Domain,
	Directory,
	LEFT(PageAndQuery, LEN(PageAndQuery) - CHARINDEX('?',REVERSE(PageAndQuery))) AS [Page],
	CASE WHEN CHARINDEX('?',REVERSE(PageAndQuery))-1 >= 0
		THEN RIGHT(PageAndQuery, CHARINDEX('?',REVERSE(PageAndQuery))-1)
		ELSE PageAndQuery END AS Query
INTO #PageSplitTable
FROM #DirectorySplitTable;

--SELECT TOP(1000) * FROM #PageSplitTable
