
SELECT SSE.[Panelist_ID], SSE.[Datetime], SSE.[Notes] AS Domain, TEC.[eco_category1] AS category
INTO #retailSearchClickstream
FROM [YouTubeShopping].[dbo].[vw_SequenceSessionEcosystem] SSE
INNER JOIN [YouTubeShopping].[dbo].[TaxonomyEcosystemCorrected] TEC ON SSE.TaxID_Whitelist = TEC.taxid_whitelist
WHERE eco_category1 = 'Brand' OR eco_category1 = 'Category Retailer' OR eco_category1 = 'General Retailer' OR eco_category1 = 'Other: Online Marketplace'
OR eco_category1 = 'Search Engine: Branded' OR eco_category1 = 'Search Engine: Generic'

SELECT *  INTO #retailClickstream FROM #retailSearchClickstream
WHERE category = 'Brand' OR category = 'Category Retailer' OR category = 'General Retailer' OR category = 'Other: Online Marketplace'

SELECT * INTO #searchClickstream FROM #retailSearchClickstream
WHERE category = 'Search Engine: Branded' OR category = 'Search Engine: Generic'

SELECT RC.[Panelist_ID] AS ID, SC.[Datetime] AS searchTime, RC.[Datetime] AS retailTime, DATEDIFF(SECOND, SC.[Datetime], RC.[Datetime]) AS timeElapsed
INTO #conversionClickstream
FROM #retailClickstream RC
INNER JOIN #searchClickstream SC ON RC.Panelist_ID = SC.Panelist_ID
WHERE DATEDIFF(SECOND, SC.[Datetime], RC.[Datetime]) >= 0 AND  DATEDIFF(SECOND, SC.[Datetime], RC.[Datetime]) <= 599

SELECT COUNT(*) AS frequency, timeElapsed
INTO #lapseFrequency
FROM #conversionClickstream
GROUP BY timeElapsed

SELECT * FROM #lapseFrequency
ORDER BY timeElapsed