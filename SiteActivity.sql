begin try drop table #GoogleTicketingUsageTable end try begin catch end catch;

SELECT property,
       category, 
	   subcategory,
	   du AS 'panelistsVisited'
	INTO #GoogleTicketingUsageTable
	FROM GoogleTicketing.dbo.Standard_Metrics_Property
	WHERE device_type = 'All Devices';

ALTER TABLE #GoogleTicketingUsageTable
ADD TotalPanelists integer;

ALTER TABLE #GoogleTicketingUsageTable
ADD Project varchar(50);

BEGIN DECLARE @GTMaxPanelists int = (SELECT MAX(dp) FROM GoogleTicketing.dbo.Standard_Metrics_Common_Report) END

UPDATE #GoogleTicketingUsageTable
SET TotalPanelists = @GTMaxPanelists;

UPDATE #GoogleTicketingUsageTable
SET Project = 'GoogleTicketing';

--SELECT TOP(10) * FROM #GoogleTicketingUsageTable


begin try drop table #YouTubeShoppingUsageTable end try begin catch end catch;

SELECT property,
       category, 
	   subcategory,
	   du AS 'panelistsVisited'
	INTO #YouTubeShoppingUsageTable
	FROM YouTubeShopping.dbo.Standard_Metrics_Eco_Property
	WHERE device_type = 'All Devices';

ALTER TABLE #YouTubeShoppingUsageTable
ADD TotalPanelists integer;

ALTER TABLE #YouTubeShoppingUsageTable
ADD Project varchar(50);


BEGIN DECLARE @YTMaxPanelists int = (SELECT MAX(dp) FROM [YouTubeShopping].[dbo].[Standard_Metrics_Eco_Report]) END

UPDATE #YouTubeShoppingUsageTable
SET TotalPanelists = @YTMaxPanelists;

UPDATE #YouTubeShoppingUsageTable
SET Project = 'YouTubeShopping';

--SELECT TOP(10) * FROM #YouTubeShoppingUsageTable

begin try drop table #IISPilotUsageTable end try begin catch end catch;

SELECT property,
       category, 
	   subcategory,
	   du AS 'panelistsVisited'
	INTO #IISPilotUsageTable
	FROM IISTest.rpt.Ecosystem_Standard_Metrics_Property
	WHERE device_type = 'All Devices';

ALTER TABLE #IISPilotUsageTable
ADD TotalPanelists integer;

ALTER TABLE #IISPilotUsageTable
ADD Project varchar(50);


BEGIN DECLARE @IISMaxPanelists int = (SELECT MAX(dp) FROM [IISTest].[dbo].[Standard_Metrics_Common_Report]) END

UPDATE #IISPilotUsageTable
SET TotalPanelists = @IISMaxPanelists;

UPDATE #IISPilotUsageTable
SET Project = 'IISPilot';

--SELECT * FROM #IISPilotUsageTable;

begin try drop table #UnionTable end try begin catch end catch;
begin try drop table #PartitionedTable end try begin catch end catch;

SELECT * INTO #UnionTable FROM (SELECT * FROM #GoogleTicketingUsageTable 
UNION ALL 
SELECT * FROM #YouTubeShoppingUsageTable
UNION ALL
SELECT * FROM #IISPilotUsageTable) alias;


SELECT * , ROW_NUMBER() OVER( PARTITION BY [property]
							  ORDER BY [panelistsVisited] DESC, [TotalPanelists] DESC)
							  AS [row_Number]

INTO #PartitionedTable
FROM #UnionTable

SELECT [property], [category], [subcategory], [panelistsVisited], [TotalPanelists], [Project] FROM #PartitionedTable WHERE [row_Number] = 1