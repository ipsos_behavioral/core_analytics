---------------------------------------------------------------------------------------------------
-- C) IPSOS COMMON TAXONOMY: ADD NEW PROJECT PROPERTIES
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/12/12)
---------------------------------------------------------------------------------------------------

use core;

-- C0) PORT OVER 2019 PROPERTIES
---------------------------------------------------------------------------------------------------
-- Create shell for table. Combine both app and wec.
---------------------------------------------------------------------------------------------------
-- C0. port prior year properties

-- select top 100 * from core.ref.Taxonomy_Common_Properties order by digital_type_id;
-- select count(*) from core.ref.Taxonomy_Common_Properties;
-- 733108

-- select top 100 * from core.ref.Taxonomy_common_2020 order by digital_type_id;
-- select count(*) from core.ref.Taxonomy_common_2020; --703945
begin try drop table ref.Taxonomy_Common_2020 end try begin catch end catch;
create table ref.Taxonomy_Common_2020  (
	property_name			nvarchar(4000),
	property_key			bigint,
	property_hash			bigint,
	digital_type_id			int,
	project_country			nvarchar(300),
	project_sample_source	nvarchar(300),
	project_sample_size	bigint,
	property_sample_size	bigint,
	project_m_reach			numeric(30,5),
	omni_entity				nvarchar(300), -- intented to be parent company owner (e.g. Apple, Alphabet, Oath, etc.)
	common_supercategory	nvarchar(300),
	common_category			nvarchar(300),
	common_subcategory		nvarchar(300),
	common_vertical			nvarchar(300),
	common_subvertical		nvarchar(300),
	sw_category				nvarchar(300),
	sw_subcategory			nvarchar(300),
	iab_category			nvarchar(300),
	iab_subcategory			nvarchar(300),
	rps_flag				int,
	rpt_flag				int,
	vetted_flag				int,
	date_created			date,
	date_updated			date,
	step_assigned			nvarchar(300),
);

insert into ref.Taxonomy_Common_2020
select
	property_name,
	property_key,
	property_hash,
	digital_type_id,
	project_country,
    project_sample_source,
	project_sample_size,
	property_sample_size,
    project_m_reach,
    omni_entity,
    common_supercategory,
    common_category,
    common_subcategory,
    common_vertical,
    common_subvertical,
    sw_category,
    sw_subcategory,
    iab_category,
    iab_subcategory,
    rps_flag,
    rpt_flag,
	vetted_flag,
    date_created,
    date_updated,
    step_assigned
from ref.Taxonomy_common_2019;

--create index var1 on ref.Taxonomy_Common_2020 (property_name);
create index var1 on ref.Taxonomy_Common_2020 (property_hash);
create index var2 on ref.Taxonomy_Common_2020 (property_key);
create index var3 on ref.Taxonomy_Common_2020 (digital_type_id);

---------------------------------------------------------------------------------------------------
-- C1) 2020 NEW PROPERTIES
---------------------------------------------------------------------------------------------------
-- C1. new project properties

--------------------------------------------------------
-- UPDATE TABLE WITH PROPS FROM 2020 PROJECTS
--------------------------------------------------------
-- KnowledgePanel2020
insert into core.ref.Taxonomy_Common_2020
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2020 KnowledgePanel' as project_sample_source,
	'4227' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    KnowledgePanel2020_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
	a.date_created,
	'2020-10-28' as date_updated, 
	'c1. new project properties' as step_assigned
from KnowledgePanel2020.ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,KnowledgePanel2020_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 4/13:10870 new properties
-- 4/20: 5862 new properties 
-- 4/27: 8106 new properties
-- 5/4: 6649 new properties
-- 5/11: 6312 new properties
-- 6/9: 10771 new properties
-- 6/16: 6985
-- 7/7: 18839
-- 8/14: 34207
-- 8/25: 9190
-- 10/5: 30168
-- 10/28: 17009

-- Turkey Pilot 2020
insert into core.ref.Taxonomy_Common_2020
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'TU' as project_country,
    '2020 Turkey Pilot' as project_sample_source,
	'237' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    TurkeyPilot2020_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2020-10-15' as [date_created],
	'2020-11-17' as[date_updated], 
	'c1. new project properties' as step_assigned
from TurkeyPilot2020.ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,TurkeyPilot2020_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 

-- 9Now 2020
insert into core.ref.Taxonomy_Common_2020
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'AU' as project_country,
    '2020 9Now' as project_sample_source,
	'920' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    NineNow2020_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2020-11-04' as [date_created],
	--a.date_created,
	'2020-12-03' as[date_updated], 
	'c1. new project properties' as step_assigned
from [9Now2020].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,NineNow2020_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 

-- 9Now 2020 final
insert into core.ref.Taxonomy_Common_2020
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'AU' as project_country,
    '2020 9Now' as project_sample_source,
	'920' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    NineNow2020_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2020-11-04' as [date_created],
	--a.date_created,
	'2020-12-29' as[date_updated], 
	'c1. new project properties' as step_assigned
from [9Now2020].ref.taxonomy_properties_final a
left outer join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,NineNow2020_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 

-- ABI 2020
insert into core.ref.Taxonomy_Common_2020
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2020 ABI' as project_sample_source,
	'3123' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    ABI2020_m_reach as project_m_reach, -- custom panel size denominator for 
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2020-12-04' as [date_created],
	--a.date_created,
	'2020-12-04' as[date_updated], 'c1. new project properties' as step_assigned
from [ABI2020].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,ABI2020_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 

-- Colgate 2020
insert into core.ref.Taxonomy_Common_2020
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2020 Colgate' as project_sample_source,
	'413' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    Colgate2020_m_reach as project_m_reach, -- custom panel size denominator for 
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2020-12-10' as [date_created],
	--a.date_created,
	'2020-12-10' as[date_updated], 'c1. new project properties' as step_assigned
from [Colgate2020].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,Colgate2020_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 

-- Colgate 2020 final
insert into core.ref.Taxonomy_Common_2020
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2020 Colgate' as project_sample_source,
	'413' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    Colgate2020_m_reach as project_m_reach, -- custom panel size denominator for 
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2020-12-10' as [date_created],
	--a.date_created,
	'2020-12-29' as[date_updated], 'c1. new project properties' as step_assigned
from [Colgate2020].ref.taxonomy_properties_final a
left outer join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,Colgate2020_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 

-- Google Search Intent 2020
insert into core.ref.Taxonomy_Common_2020
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2020 Google Search Intent' as project_sample_source,
	'1982' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    GoogleSearchIntent2020_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-01-15' as [date_created],
	--a.date_created,
	'2021-01-15' as[date_updated], 'c1. new project properties' as step_assigned
from [GoogleSearchIntent2020].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,GoogleSearchIntent2020_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 


-- Sanofi Allergy 2021
insert into core.ref.Taxonomy_Common_2020
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2021 Sanofi Allergy' as project_sample_source,
	'500' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    SanofiAllergy2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-02-08' as [date_created],
	--a.date_created,
	'2021-02-08' as[date_updated], 'c1. new project properties' as step_assigned
from [SanofiAllergy2021].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,SanofiAllergy2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 


-- select top 100 * from core.ref.Taxonomy_Common_2020 order by digital_type_id;
-- select top 100 * from core.ref.Taxonomy_Common_2020 where project_sample_source like '%sanofi%allergy%'
-- select top 100 * from core.ref.Taxonomy_Common_2020 where property_name = 'sweetwineclub.com';
-- select count(*) from core.ref.Taxonomy_Common_2020;	--825274
-- select count(*) from core.ref.Taxonomy_common_2020;	--801708

-- select top 100 * from core.ref.Taxonomy_Common_Properties order by digital_type_id;

-- update sw categories using most updated version 
-- (will update this step after stephen migrate the global sw classifications to core table.)
update core.ref.Taxonomy_Common_2020 set 
sw_category = b.Category, 
sw_subcategory = b.SubCategory
from core.ref.Taxonomy_Common_2020 a
inner join [PassiveMaster].[Reference].[SimilarWeb_Domain_Classification] b
on a.property_name = b.Domain_Name and a.property_hash = b.Domain_Name_Hash and a.digital_type_id = 235
where a.sw_category = 'uncategorized' and b.category is not null
;

select top 100 * from core.ref.Taxonomy_Common_2020 where step_assigned like 'c1%';
select count(*) from core.ref.Taxonomy_Common_2020 where step_assigned like 'c1%';

-- END OF FILE --
