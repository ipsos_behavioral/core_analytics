﻿---------------------------------------------------------------------------------------------------
-- C) IPSOS COMMON TAXONOMY: VETTED RECLASSIFY COMMON
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/12/12)
---------------------------------------------------------------------------------------------------

use core;

-- C4) ONE OFF CHANGES TO PROPERTIES
---------------------------------------------------------------------------------------------------
-- C4. vetted reclassify common

-- C4. 510 EMAIL/CHAT
---------------------

-- C4. 520 ENTERTAINMENT/GAMES
------------------------------
update ref.Taxonomy_Common_2020 set common_category='Music and Audio',date_updated= '1/24/2020', 
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 
where common_supercategory = 'Entertainment/Games' and common_category in ('Music Streaming', 'Streaming Audio');

update ref.Taxonomy_Common_2020 set common_vertical ='Media/Entertainment',date_updated= '04/03/2020', 
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 
where common_supercategory = 'Entertainment/Games' and common_category in ('Movies', 'Music and Audio', 'TV and Video', 'Art/Museums') and common_vertical = '';

-- update mobile/video games categories
update ref.Taxonomy_Common_2020 set common_supercategory='Entertainment/Games', common_category='Games', date_updated= '03/26/2020', 
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 where common_supercategory = 'Entertainment/Games' and common_category in ('Mobile Games', 'Video Games');

update ref.Taxonomy_Common_2020 set common_vertical='Games',
date_updated= '05/15/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 where common_supercategory='Entertainment/Games' and common_category='Games' and common_vertical = '';

--change book distribution to e-Reading
-- select top 100 * from ref.Taxonomy_Common_2020 where common_category = 'Book Distribution';
update ref.Taxonomy_Common_2020 set common_category='e-Reading',
date_updated= '05/15/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 where common_supercategory = 'Entertainment/Games' and common_category = 'Book Distribution';

update ref.Taxonomy_Common_2020 set common_vertical='Books/Magazines',
date_updated= '05/15/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 where common_supercategory='Entertainment/Games' and common_category='e-Reading' and common_vertical = '';

--add sports category
update ref.Taxonomy_Common_2020 set common_supercategory='Entertainment/Games', common_category='Sports', common_vertical='Sports',
date_updated= '05/15/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 where common_supercategory = 'Entertainment/Games' and common_vertical = 'Sports' and vetted_flag = 1;

-- change comedy vertical to commn_category
update ref.Taxonomy_Common_2020 set common_supercategory='Entertainment/Games', common_category='Comedy', common_vertical='',
date_updated= '10/27/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 where common_vertical = 'Comedy';

-- C4. 530 NEWS/MEDIA/INFO
---------------------------
update ref.Taxonomy_Common_2020 set common_supercategory='News/Media/Info', common_category='General News',
date_updated= '05/15/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 
where common_supercategory = 'News/Media/Info' and common_category = 'News' and (common_vertical = '' or common_vertical = 'Politics');

update ref.Taxonomy_Common_2020 set common_supercategory='News/Media/Info', common_category='Category News',
date_updated= '05/15/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 
where common_supercategory = 'News/Media/Info' and common_category = 'News' and common_vertical != '' and common_vertical != 'Politics';

update ref.Taxonomy_Common_2020 set common_supercategory='News/Media/Info', common_category='General Info',
date_updated= '05/15/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 
where common_supercategory = 'News/Media/Info' and common_category = 'Info' and common_vertical = '';

update ref.Taxonomy_Common_2020 set common_supercategory='News/Media/Info', common_category='Category Info',
date_updated= '05/15/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 
where common_supercategory = 'News/Media/Info' and common_category = 'Info' and common_vertical != '';

-- change Dictionary/Encyclopedia vertical to commn_subcategory
update ref.Taxonomy_Common_2020 set common_supercategory='News/Media/Info', common_category='Category Info',
common_subcategory='Dictionary/Encyclopedia', common_vertical='',
date_updated= '10/27/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 where common_vertical = 'Dictionary/Encyclopedia';

-- change Price Comparison vertical to commn_subcategory
update ref.Taxonomy_Common_2020 set common_supercategory='News/Media/Info', common_category='Category Info',
common_subcategory='Price Comparison', common_vertical='',
date_updated= '10/27/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 where common_vertical = 'Price Comparison';

-- C4. 540 LIFESTYLE/LIVING
---------------------------
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_vertical='Food/Groceries', step_assigned='b4. vetted reclassify common' 
from ref.Taxonomy_Common_2020 where property_name like 'McDonald%s' or property_name like 'Domino%s Pizza USA' or property_name in (
	'dominos.com', 
	'pizzahut.com', 
	'starbucks.com', 
	'Starbucks'
	);

-- update health: insurance categories
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Lifestyle/Living', common_category='Health', common_subcategory='insurance', common_vertical='Health/Medicine',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Lifestyle/Living' and common_category = 'Health: Insurance';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Lifestyle/Living', common_category='Health', common_subcategory='Hospitals', common_vertical='Health/Medicine',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Lifestyle/Living' and common_category = 'Health: Hospitals';
update ref.Taxonomy_Common_2020 set common_vertical='Health/Medicine', date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Lifestyle/Living' and common_category = 'Health' and common_vertical = '';

--Change Lifestyle/Living>Food Delivery, Lifestyle/Living>Restaurants to Shopping>Food Delivery, Shopping>Restaurants
update ref.Taxonomy_Common_2020 set common_supercategory='Shopping', date_updated= '04/27/2020' from ref.Taxonomy_Common_2020 where common_supercategory = 'Lifestyle/Living' and common_category in ('Restaurants','Food Delivery');


-- C4. 550 SEARCH ENGINE
---------------------------

-- C4. 560 SHOPPING
---------------------------
-- 1/24 Update Shopping Categories and Verticals
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Apparel',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Apparel';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Beauty',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Beauty';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Computer/Electronics',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Computer Electronics';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Computer/Electronics',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Electronics';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Fashion Accessories',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Fashion Accessories';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Food/Groceries',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Food';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Food/Groceries',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Food/Groceries';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Home/Garden',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Home and Garden';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Home/Garden',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Home Furnishings';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Shoes/Footwear',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Shoes/Footwear';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Travel',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Travel';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Home/Garden',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Home Appliances';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Brand', common_vertical='Alcohol/Beverage',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Brand: Alcohol/Beverage';

update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Apparel',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Apparel';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Automotive',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Auto';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Beauty',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Beauty';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Books/Magazines',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Books/Magazines';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Family and Children',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Children';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Computer/Electronics',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Computer Electronics';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Computer/Electronics',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Electronics';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Food/Groceries',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Food/Grocery';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Food/Groceries',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Grocery';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Home/Garden',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Home and Garden';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Home/Garden',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Home Appliances';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Home/Garden',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Home Furnishings';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Home/Garden',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Home/Garden';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Live Events',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Live Events';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Health/Medicine',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Medicine';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Pets',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Pets';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Travel',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Travel';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Alcohol/Beverage',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Alcohol/Beverage';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Gift Cards',date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Gift Cards';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Shopping', common_category='Category Retailer', common_vertical='Fashion Accessories', date_updated= '12/12/2019', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020  where common_supercategory = 'Shopping' and common_category = 'Category Retailer: Fashion Accessories';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_category='', date_updated= '05/20/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020  where common_supercategory = 'Shopping' and common_category = 'Shopping: Automotive'; -- revisit those sites to determine category retailer vs brand.

update ref.Taxonomy_Common_2020 set common_category='Coupons', common_subcategory = '[Alert: Discretion Coupons]', date_updated= '05/20/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020  where common_supercategory = 'Shopping' and common_category = 'Discount/Rewards'; 
update ref.Taxonomy_Common_2020 set common_category='Restaurants', date_updated= '05/20/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020  where common_supercategory = 'Shopping' and common_category = 'Restaurants '; 
update ref.Taxonomy_Common_2020 set common_subcategory='Vineyard/Winery', date_updated= '12/09/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020  where common_supercategory = 'Shopping' and common_subcategory = 'Vineyard '; 


--retail vertical
update ref.Taxonomy_Common_2020 set common_vertical='', date_updated= '05/20/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020  where common_vertical = '[ Other Retail ]';

update ref.Taxonomy_Common_2020 set common_vertical='Food/Groceries', date_updated= '05/20/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020  where common_supercategory = 'Shopping' and common_category = 'Food Delivery';

-- change Gift Cards vertical to commn_subcategory
update ref.Taxonomy_Common_2020 set common_supercategory='Shopping', common_category='Category Retailer',
common_subcategory='Gift Cards', common_vertical='', rps_flag = 1, rpt_flag = 1,
date_updated= '10/27/2020', step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 where common_vertical = 'Gift Cards';

-- C4. 570 SOCIAL/PHOTOS
---------------------------
update ref.Taxonomy_Common_2020 set common_category='Blogs', date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Social/Photos' and common_category = 'Blog';

-- Change Social/Photos>Blogs to Lifestyle/Living>Blogs
update ref.Taxonomy_Common_2020 set common_supercategory = 'Lifestyle/Living', date_updated= '04/27/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Social/Photos' and common_category = 'Blogs';


-- C4. 580 FINANCE/WORK/GOV
---------------------------
-- 3/26 Update Finance Categories, subcat and verticals
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', date_updated= '1/24/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Shopping' and common_category = 'Shipping';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Investment/Investing', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_subcategory = 'investing';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Investment/Investing', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Finance: investing';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Accounting', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Finance: Accounting';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Banking', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Finance: Banking';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Credit', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Finance: Credit';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Loan', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Finance: Loan';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Investment/Investing', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Finance: Investment/Investing';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Insurance', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Finance: Insurance';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Payment', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Finance: Payment';
update ref.Taxonomy_Common_2020 set vetted_flag = 1, common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='Real Estate', common_vertical='Finance',date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Finance: Real Estate';
update ref.Taxonomy_Common_2020 set common_vertical='Education',date_updated= '05/20/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Education' and common_vertical = '';
update ref.Taxonomy_Common_2020 set common_vertical='Business',date_updated= '05/20/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Law' and common_vertical = '';


-- C4. 590 UTILITY/OTHER
---------------------------

-- C4. 600 Alert
---------------------------
update ref.Taxonomy_common_2020 set vetted_flag=1, common_supercategory='[Alert]', common_category='[Alert: Business Services]', common_subcategory='[Alert: Business Services]', rps_flag=1, rpt_flag=0,
	step_assigned='c4. vetted reclassify common' from ref.Taxonomy_common_2020 where property_name in (
	'SkypeBridge',
	'live.com',
	'wisepops.com',
	'adobe.com');

-- update missing/wrong common_category:
-- select * from ref.Taxonomy_common_2020 where common_subcategory = '[Alert: Business/Service]';
update ref.Taxonomy_common_2020
set step_assigned='c5. business rules', common_category='[Alert: Business Services]', common_subcategory = '[Alert: Business Services]'
where common_supercategory = '[Alert]' and (common_category='[Alert: Business/Service]' or common_subcategory = '[Alert: Business/Service]');

update ref.Taxonomy_common_2020
set step_assigned='c5. business rules', rpt_flag = 0
where common_subcategory = '[Alert: Discretion Guns/Weapons]';

-- C4. 999 REMOVALS
--------------------------
-- 11/14/2019 remove following apps
update ref.Taxonomy_common_2020 set vetted_flag=1, common_supercategory='[Remove]', common_category='[Remove: Research/Marketing]', common_subcategory='[Remove: Research/Marketing]', rps_flag=0, rpt_flag=0,
	step_assigned='c4. vetted reclassify common' from ref.Taxonomy_common_2020 where property_name in (
	'OpinionAPP',
	'opinionapp.mobi',
	'opinion-people.com',
	'opinionworld.com',
	'masqueopiniones.com',
	'everyopinion.fr',
	'efs-survey.com',
	'panelia.fr');

update ref.Taxonomy_common_2020 set vetted_flag=1, common_supercategory='[Remove]', common_category='[Remove: Blocked]', common_subcategory='[Remove: Blocked]', rps_flag=0, rpt_flag=0,
	step_assigned='c4. vetted reclassify common' from ref.Taxonomy_common_2020 where property_name in (
	'coreidvd',
	'nsurlsessiond',
	'com.apple.healthappd',
	'com.apple.gamed');

-- Doamin removals 
update ref.Taxonomy_common_2020 set vetted_flag=1, common_supercategory='[Remove]', common_category='[Remove: Blocked]', common_subcategory='[Remove: Blocked]', rps_flag=0, rpt_flag=0,
	step_assigned='c4. vetted reclassify common' from ref.Taxonomy_common_2020 where property_name in (
	'walmart.co',
	'kroger.co',
	'krogerkrazy.co',
	'target.co',
	'costco.ms',
	'target.com.co');


--Update vetted_flag
update ref.Taxonomy_Common_2020 set vetted_flag=1, step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_Common_2020 
where property_name in ('imdb.com',
'wikia.com',
'net.oneplus.weather',
'wayfair.com',
'net.oneplus.weather',
'instagc.com',
'airbnb.com',
'nicequest.com',
'acop.com',
'rottentomatoes.com',
'btttag.com',
'qz.com',
'digitaltrends.com',
'consumerreports.org',
'revenueuniverse.com',
'aytm.com',
'victoriassecret.com',
'trivago.com',
'disney.com',
'biotrust.com',
'aniview.com',
'nike.com',
'bgr.com',
'opinionsite.com',
'musical.ly',
'inside-graph.com',
'myworkdayjobs.com',
'hunker.com',
'tomsguide.com',
'alibaba.com',
'lithium.com',
'insticator.com',
'taleo.net',
'neuvoo.com',
'techcrunch.com'
);

-- UPDATE VERTICALS
update ref.Taxonomy_Common_2020 set common_vertical='Home/Garden',step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_vertical in ('Home Appliances','Home Furnishings');
update ref.Taxonomy_Common_2020 set common_vertical='Computer/Electronics', step_assigned='c4. category name update' from ref.Taxonomy_Common_2020 where common_vertical = 'Consumer Electronics';
update ref.Taxonomy_Common_2020 set common_vertical='Outdoors',step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_vertical in ('Outdoor');
update ref.Taxonomy_Common_2020 set common_subcategory='Auto Parts',common_vertical='Automotive', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_vertical='Auto Parts';
update ref.Taxonomy_Common_2020 set common_vertical='Family and Children', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_vertical='Children';


-- add travel and gov vertical
update ref.Taxonomy_Common_2020 set common_vertical='Travel', date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'News/Media/Info' and common_category = 'Transportation' and common_vertical = '';
update ref.Taxonomy_Common_2020 set common_vertical='Government', date_updated= '03/26/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020 where common_supercategory = 'Finance/Work/Gov' and common_category = 'Government' and common_vertical = '';

update ref.Taxonomy_Common_2020 set common_vertical='Automotive', date_updated= '05/20/2020', step_assigned= 'c4. category name update' from ref.Taxonomy_Common_2020  where common_vertical = 'Auto'; 

-- 8/5 update Family and Children verticals to following Newell properties (Nestle Gerber proposal)
update ref.Taxonomy_Common_2020 set common_vertical='Family and Children', date_updated= '08/05/2020', 
step_assigned= 'c4. category name update' 
from ref.Taxonomy_Common_2020 
where property_name in(
'babycenter.com',
'carters.com',
'whattoexpect.com',
'thebump.com',
'buybuybaby.com',
'babble.com',
'babylist.com',
'sheknows.com',
'gracobaby.com',
'albeebaby.com',
'summerinfant.com',
'carseatcanopy.com',
'evenflo.com',
'car-seat.org',
'chiccousa.com',
'babytrend.com',
'ergobaby.com',
'healthybabyhappyearth.com',
'oshkosh.com',
'parents.com',
'romper.com',
'safety1st.com',
'todaysparent.com',
'uppababy.com',
'urbinibaby.com',
'kidsii.ca',
'kidsii.con',
'kidsii.com',
'lovetoknow.com',
'lucieslist.com',
'maxicosi.com',
'mbeans.com',
'nuk-usa.com',
'chicco.com',
'everydayfamily.com',
'happiestbaby.com',
'blogspot.com',
'bundoo.com',
'babyshop.com',
'babyearth.com',
'csftl.org',
'deltachildren.com',
'easybabylife.com',
'carseatblog.com',
'cafemom.com');

--select * from core.ref.Taxonomy_Common_2020 where property_name = 'kidsii.con'
-- Insert ABI removed properties (revisit in future, need to update abi taxonomu_properties
insert into core.ref.Taxonomy_Common_2020
select a.* from abi2020.ref.Taxonomy_Common_Properties_common a
left join ref.Taxonomy_Common_2020 b
on a.property_hash = b.property_hash and a.digital_type_id = b.digital_type_id
where b.property_name is null and a.digital_type_id is not null and a.property_name is not null;

-- QA 
select top 100 * from core.ref.Taxonomy_Common_2020 where step_assigned like 'c4%';
select count(*) from core.ref.Taxonomy_Common_2020 where step_assigned like 'c4%';

-- ref.Taxonomy_Common_Propertiesselect top 100 * from core.ref.Taxonomy_common_properties where property_name in ('uber', 'uber.com', 'lyft', 'lyft.com', 'Lyft - Taxi & Bus App Alternative')

-- END OF FILE --
