---------------------------------------------------------------------------------------------------
-- C) IPSOS COMMON TAXONOMY: ADD NEW PROJECT PROPERTIES
---------------------------------------------------------------------------------------------------
-- Script by Jimmy
-- Modified by Shirley (2021/3/25)
---------------------------------------------------------------------------------------------------

use core;

-- C0) PORT OVER 2020 PROPERTIES
---------------------------------------------------------------------------------------------------
-- Create shell for table. Combine both app and wec.
---------------------------------------------------------------------------------------------------
-- C0. port prior year properties

-- select top 100 * from core.ref.Taxonomy_Common_Properties order by digital_type_id;
-- select count(*) from core.ref.Taxonomy_Common_Properties;
-- 733108

-- select top 100 * from core.ref.Taxonomy_Common_2021 order by digital_type_id;
-- select count(*) from core.ref.Taxonomy_Common_2021; --703945
begin try drop table ref.Taxonomy_Common_2021 end try begin catch end catch;
create table ref.Taxonomy_Common_2021  (
	property_name			nvarchar(4000),
	property_key			bigint,
	property_hash			bigint,
	digital_type_id			int,
	project_country			nvarchar(300),
	project_sample_source	nvarchar(300),
	project_sample_size	bigint,
	property_sample_size	bigint,
	project_m_reach			numeric(30,5),
	omni_entity				nvarchar(300), -- intented to be parent company owner (e.g. Apple, Alphabet, Oath, etc.)
	common_supercategory	nvarchar(300),
	common_category			nvarchar(300),
	common_subcategory		nvarchar(300),
	common_vertical			nvarchar(300),
	common_subvertical		nvarchar(300),
	sw_category				nvarchar(300),
	sw_subcategory			nvarchar(300),
	iab_category			nvarchar(300),
	iab_subcategory			nvarchar(300),
	rps_flag				int,
	rpt_flag				int,
	vetted_flag				int,
	date_created			date,
	date_updated			date,
	step_assigned			nvarchar(300),
);

insert into ref.Taxonomy_Common_2021
select
	property_name,
	property_key,
	property_hash,
	digital_type_id,
	project_country,
    project_sample_source,
	project_sample_size,
	property_sample_size,
    project_m_reach,
    omni_entity,
    common_supercategory,
    common_category,
    common_subcategory,
    common_vertical,
    common_subvertical,
    sw_category,
    sw_subcategory,
    iab_category,
    iab_subcategory,
    rps_flag,
    rpt_flag,
	vetted_flag,
    date_created,
    date_updated,
    step_assigned
from ref.Taxonomy_common_2020;

--create index var1 on ref.Taxonomy_Common_2021 (property_name);
create index var1 on ref.Taxonomy_Common_2021 (property_hash);
create index var2 on ref.Taxonomy_Common_2021 (property_key);
create index var3 on ref.Taxonomy_Common_2021 (digital_type_id);

---------------------------------------------------------------------------------------------------
-- C1) 2021 NEW PROPERTIES
---------------------------------------------------------------------------------------------------
-- C1. new project properties

--------------------------------------------------------
-- UPDATE TABLE WITH PROPS FROM 2021 PROJECTS
--------------------------------------------------------
-- Google Taiwan 2021 Interim
insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'CN' as project_country,
    '2021 Google Taiwan' as project_sample_source,
	'168' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    GoogleTaiwan2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-4-27' as [date_created],
	--a.date_created,
	'2021-4-27' as[date_updated], 'c1. new project properties' as step_assigned
from [GoogleTaiwan2021 ].ref.taxonomy_properties_interim a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,GoogleTaiwan2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
--(6300 row(s) affected)

-- Google Taiwan 2021 Final
insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'CN' as project_country,
    '2021 Google Taiwan' as project_sample_source,
	'168' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    GoogleTaiwan2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-5-5' as [date_created],
	--a.date_created,
	'2021-5-5' as[date_updated], 'c1. new project properties' as step_assigned
from [GoogleTaiwan2021 ].ref.taxonomy_properties_final a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,GoogleTaiwan2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
--(64 row(s) affected)

-- ColgateMexicoPDJ 2021
insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'MX' as project_country,
    '2021 ColgateMexicoPDJ' as project_sample_source,
	'275' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    ColgateMexicoPDJ2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-7-12' as [date_created],
	--a.date_created,
	'2021-7-12' as[date_updated], 'c1. new project properties' as step_assigned
from [ColgateMexicoPDJ2021].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,ColgateMexicoPDJ2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 8549

insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'MX' as project_country,
    '2021 ColgateMexicoPDJ' as project_sample_source,
	'275' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    ColgateMexicoPDJ2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-8-9' as [date_created],
	--a.date_created,
	'2021-8-9' as[date_updated], 'c1. new project properties' as step_assigned
from [ColgateMexicoPDJ2021].ref.taxonomy_properties_final a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,ColgateMexicoPDJ2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
----7473

-- ColgateBrazilPDJ 2021
insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'BR' as project_country,
    '2021 ColgateBrazilPDJ' as project_sample_source,
	'1954' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    ColgateBrazilPDJ2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-7-12' as [date_created],
	--a.date_created,
	'2021-7-12' as[date_updated], 'c1. new project properties' as step_assigned
from [ColgateBrazilPDJ2021].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,ColgateBrazilPDJ2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 64978

insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'BR' as project_country,
    '2021 ColgateBrazilPDJ' as project_sample_source,
	'1954' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    ColgateBrazilPDJ2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-8-5' as [date_created],
	--a.date_created,
	'2021-8-5' as[date_updated], 'c1. new project properties' as step_assigned
from [ColgateBrazilPDJ2021].ref.taxonomy_properties_final a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,ColgateBrazilPDJ2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 32840

insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'BR' as project_country,
    '2021 ColgateBrazilPDJ' as project_sample_source,
	'1954' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    ColgateBrazilPDJ2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-8-13' as [date_created],
	--a.date_created,
	'2021-8-13' as[date_updated], 'c1. new project properties' as step_assigned
from [ColgateBrazilPDJ2021].ref.taxonomy_properties_final2 a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,ColgateBrazilPDJ2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
--(10006 row(s) affected)

insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'BR' as project_country,
    '2021 ColgateBrazilPDJ' as project_sample_source,
	'1954' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    ColgateBrazilPDJ2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-8-16' as [date_created],
	--a.date_created,
	'2021-8-16' as[date_updated], 'c1. new project properties' as step_assigned
from [ColgateBrazilPDJ2021].ref.taxonomy_properties_final3 a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,ColgateBrazilPDJ2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 5523

-- Sanofi Gold Bond (Lotion)
insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2021 SanofiLotion' as project_sample_source,
	'1100' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    SanoLotion2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-9-2' as [date_created],
	--a.date_created,
	'2021-9-2' as[date_updated], 'c1. new project properties' as step_assigned
from [SanoLotion2021].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,SanoLotion2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 47096


-- Sanofi Pain
insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2021 Sanofi Pain' as project_sample_source,
	'1115' as project_sample_size,
	max(num_panelists) as property_sample_size, -- custom
    SanofiPain2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-9-7' as [date_created],
	--a.date_created,
	'2021-9-7' as[date_updated], 'c1. new project properties' as step_assigned
from [SanofiPain2021].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,SanofiPain2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 26506

-- Beam Suntory 
-- embee portion
insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2021 BeamSuntory' as project_sample_source, -- change to project name
	'1423' as project_sample_size, -- change to project N
	max(num_panelists) as property_sample_size, -- custom
    BeamSuntory2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-11-22' as [date_created], -- change date to today's date
	'2021-11-22' as[date_updated], 'c1. new project properties' as step_assigned -- same as date_created by defaule
from [BeamSuntory2021].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,BeamSuntory2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
--32924

-- disqo portion
insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2021 BeamSuntory' as project_sample_source, -- change to project name
	'1423' as project_sample_size, -- change to project N
	max(num_panelists) as property_sample_size, -- custom
    BeamSuntory2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2021-11-24' as [date_created], -- change date to today's date
	'2021-11-24' as[date_updated], 'c1. new project properties' as step_assigned -- same as date_created by defaule
from [BeamSuntory2021].ref.taxonomy_properties_v1 a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,BeamSuntory2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
--(23414 row(s) affected)

-- cox wireless
-- embee portion
insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2021 CoxWireless' as project_sample_source, -- change to project name
	'1004' as project_sample_size, -- change to project N
	max(num_panelists) as property_sample_size, -- custom
    CoxWireless2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2022-01-04' as [date_created], -- change date to today's date
	'2022-01-04' as[date_updated], 'c1. new project properties' as step_assigned -- same as date_created by defaule
from [CoxWireless2021].ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,CoxWireless2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 13278

insert into core.ref.Taxonomy_Common_2021
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2021 CoxWireless' as project_sample_source, -- change to project name
	'1004' as project_sample_size, -- change to project N
	max(num_panelists) as property_sample_size, -- custom
    CoxWireless2021_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2022-01-11' as [date_created], -- change date to today's date
	'2022-01-11' as[date_updated], 'c1. new project properties' as step_assigned -- same as date_created by defaule
from [CoxWireless2021].ref.taxonomy_properties_v1 a
left outer join core.ref.Taxonomy_Common_2021 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,CoxWireless2021_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- 11846


-----------------------------------------------
-- RUN THIS AFTER THE PROJECT INSERT SCRIPTS
-- update sw categories using most updated version 
-- (will update this step after stephen migrate the global sw classifications to core table.)
update core.ref.Taxonomy_Common_2021 set 
sw_category = b.Category, 
sw_subcategory = b.SubCategory
from core.ref.Taxonomy_Common_2021 a
inner join [PassiveMaster].[Reference].[SimilarWeb_Domain_Classification] b
on a.property_name = b.Domain_Name and a.property_hash = b.Domain_Name_Hash and a.digital_type_id = 235
where a.sw_category = 'uncategorized' and b.category is not null
;

-- QA
--select top 100 * from core.ref.Taxonomy_Common_2021 where step_assigned like 'c1%' and project_sample_source = '2021 CoxWireless';
--select count(*) from core.ref.Taxonomy_Common_2021 where step_assigned like 'c1%' and project_sample_source = '2021 BeamSuntory';
--select * from core.ref.Taxonomy_Common_2021 where step_assigned like 'c1%' and [date_created] = '2022-1-11';
--select count(*) from core.ref.Taxonomy_Common_2021 where step_assigned like 'c1%';

-- END OF FILE --
