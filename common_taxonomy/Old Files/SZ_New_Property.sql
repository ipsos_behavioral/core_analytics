----------------------------------
--Script for core taxonomy property update
--Modified from Madison's panelist outlier QA script
----------------------------------

use GoogleAuto2019;

-------------------------------App and Domain Portion

------------Starts by pulling data from run.userbyday_total at property_level per day
-- select top 100 * from #digital_platform_panelist_property_date order by panelist_key,date_id;
-- select top 100 * from #digital_platform_panelist_property_date order by property_key, panelist_key, platform_id;
-- select * from #digital_platform_panelist_property_date where panelist_key = 1 and property_key = 238;
-- select * from #digital_platform_panelist_property_date order by dur_minutes;
-- select count(distinct property_key) from GoogleAuto2019.run.UserByDay_Total where Report_Level = 0 and dur_minutes <> 0.00; -- 111626
-- select count(distinct property_key) from GoogleAuto2019.run.UserByDay_Total where digital_type_id = 235 and Report_Level = 0 and dur_minutes <> 0.00; -- 107196
-- select count(distinct property_key) from GoogleAuto2019.run.UserByDay_Total where digital_type_id = 100 and Report_Level = 0 and dur_minutes <> 0.00; -- 13929
-- select count(distinct property_key) from [GoogleAuto2019].[ref].[vw_taxonomy_properties] where digital_type_id = 235; -- 78628
-- select count(distinct property_key) from [GoogleAuto2019].[ref].[vw_taxonomy_properties] where digital_type_id = 100; -- 13929
-- select * from GoogleAuto2019.run.UserByDay_Total where property_key = '982' 
-- and digital_type_id = 100;

begin try drop table #digital_platform_panelist_property_date end try begin catch end catch;

select digital_type_id, Platform_ID, Panelist_Key, Property_Key, Date_ID, cat_hash, subcat_hash, cast(Num_Uses as float) as Num_Uses, dur_minutes
into #digital_platform_panelist_property_date
from GoogleAuto2019.run.UserByDay_Total
where Report_Level = 0; 
--and dur_minutes <> 0.00;
--(1664139 rows affected)

------------Collapses across platform
-- select * from #digital_panelist_property_date where digital_type_id = 100 and panelist_key = 834 and property_key = 5966 and date_id = '2019-08-31';
-- select * from #digital_platform_panelist_property_date where digital_type_id = 100 and panelist_key = 834 and property_key = 5966 and date_id = '2019-08-31';
-- select * from #digital_panelist_property_date where digital_type_id = 235 and panelist_key = 1068 and property_key = 37640 and date_id = '2019-09-20';
-- select * from #digital_platform_panelist_property_date where digital_type_id = 235 and panelist_key = 1068 and property_key = 37640 and date_id = '2019-09-20'
-- select * from #digital_panelist_property_date where daily_qualified <> 1;
begin try drop table #digital_panelist_property_date end try begin catch end catch;

select a.Digital_Type_ID, a.Panelist_Key, a.Property_Key, a.Date_ID, cat_hash, subcat_hash, b.Daily_Qualified, sum(num_uses) as Num_Uses, sum(dur_minutes) as dur_minutes
into #digital_panelist_property_date
from #digital_platform_panelist_property_date a join run.User_Qualification b
on a.Panelist_Key = b.Panelist_Key and a.Date_ID = b.Date_ID 
-- and b.Daily_Qualified = 1
group by a.digital_type_id, a.Panelist_Key, a.Property_Key, a.Date_ID, cat_hash, subcat_hash, b.Daily_Qualified;
--(1659190 rows affected)

---------Collapses data across days since analysis is of a panelist's overall effect on the data.
-- select top 100 * from #digital_panelist_property order by panelist_key;
-- select * from #digital_platform_panelist_property where panelist_key = 1 and property_key = 238;
-- select * from #digital_panelist_property_date where property_key = 76431 and digital_type_id = 235 and panelist_key = 1;
begin try drop table #digital_panelist_property end try begin catch end catch;

select digital_type_id, Panelist_Key, Property_Key, cat_hash, subcat_hash, sum(Num_Uses) as panelist_uses, sum(dur_minutes) as panelist_mins, STDEVP(dur_minutes) as prop_mins_stddev
into #digital_panelist_property
from #digital_panelist_property_date
group by digital_type_id, Panelist_Key, Property_Key,cat_hash, subcat_hash;
--(469339 rows affected)

-------Gets the names of the properties since core taxonomy table doesnt have property key. From here, script is broken into digital type
--App first
-- select top 100 * from #app_with_name order by app_name;
-- select count(*) from GoogleAuto2019.ref.App_Name; -- 14582
-- select top 100 * from GoogleAuto2019.ref.App_Name where app_name = 'Hidden Objects House Cleaning';
begin try drop table #app_with_name end try begin catch end catch;

select a.*,b.App_Name as property_name
into #app_with_name
from #digital_panelist_property a join GoogleAuto2019.ref.App_Name b on a.Property_Key = b.App_Name_Key
where Digital_Type_ID = 100;
--(56312 rows affected)

--Domain 
-- select count(distinct Domain_Name) from GoogleAuto2019.ref.domain_Name; -- 115768
-- select count(distinct property_name) from #domain_with_name;--107196
begin try drop table #domain_with_name end try begin catch end catch;

select a.*,b.Domain_Name as property_name
into #domain_with_name
from #digital_panelist_property a join GoogleAuto2019.ref.Domain_Name b on a.Property_Key = b.Domain_Name_Key
where Digital_Type_ID = 235;
--(338104 rows affected)

-- combine app & domain:
-- select * from #vw_all_properties;
-- select count(distinct property_name) from #vw_all_properties where digital_type_id = 235; -- 107196
begin try drop table #vw_all_properties end try begin catch end catch;

select x.* into #vw_all_properties
from
(select * from #app_with_name
union
select * from #domain_with_name) x;


-- create #cat_subcat_lookup
-- select * from #cat_subcat_lookup;
begin try drop table #cat_subcat_lookup end try begin catch end catch;
select digital_type_id, cat_Hash, subcat_Hash, category, subcategory
	into #cat_subcat_lookup
	from [Ref].[Global_Taxonomy]
	group by digital_type_id, cat_Hash, subcat_Hash, category, subcategory;
create index cat_subcat_lookup1 on #cat_subcat_lookup (digital_type_id);
create index cat_subcat_lookup2 on #cat_subcat_lookup (cat_Hash);
create index cat_subcat_lookup3 on #cat_subcat_lookup (subcat_Hash);

-- Final table:
-- select * from ref.taxonomy_properties;
-- select count(*) from ref.taxonomy_properties where digital_type_id =235; 
begin try drop table [GoogleAuto2019].[ref].[taxonomy_properties] end try begin catch end catch;

select a.digital_type_id, Property_Key, property_name, e.category, e.subcategory, sum(panelist_mins) as total_mins, count(*) as num_panelists
into [GoogleAuto2019].[ref].[taxonomy_properties]
from #vw_all_properties a
left outer join #cat_subcat_lookup e
	on a.cat_hash = e.cat_hash
	and a.subcat_hash = e.subcat_hash
	and a.digital_type_id = e.digital_type_id
group by a.digital_type_id, Property_Key, property_name, e.category, e.subcategory;
--(121125 row(s) affected)


/**
-----------Top unclassified apps
-- select * from #apps_to_classify order by num_panelists desc;
-- drop table #apps_to_classify;
-- select * from #app_with_supercat
-- select * from #apps_to_classify;
begin try drop table #apps_to_classify end try begin catch end catch;

select common_supercategory, app_name, Property_Key, sum(panelist_mins) as total_mins, count(*) as num_panelists
into #apps_to_classify
from #app_with_supercat
group by Property_Key, App_Name, common_supercategory;

-----------Top unclassified domains

-- select * from #domains_to_classify order by num_panelists desc;
-- drop table #domains_to_classify;
begin try drop table #domains_to_classify end try begin catch end catch;

select common_supercategory, domain_name, Property_Key, sum(panelist_mins) as total_mins, count(*) as num_panelists
into #domains_to_classify
from #domain_with_supercat
group by Property_Key, Domain_Name, common_supercategory;
**/
