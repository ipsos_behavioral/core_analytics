---------------------------------------------------------------------------------------------------
-- IPSOS COMMON TAXONOMY (AGGREATED ACROSS ALL PROJECTS)
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/11/08)
---------------------------------------------------------------------------------------------------
use core;

-- 1) COMBINE COMMON DOMAIN WITH COMMON APP FOR COMBINED
--------------------------------------------------------

-- select top 100 * from core.ref.Taxonomy_Common_Properties order by digital_type_id;
-- select count(*) from ref.Taxonomy_Common_Properties;
begin try drop table ref.Taxonomy_Common_Properties end try begin catch end catch;
create table ref.Taxonomy_Common_Properties  (
	property_name			nvarchar(4000),
	digital_type_id			int,
	project_country			nvarchar(300),
	project_sample_source	nvarchar(300),
	project_sample_size		bigint,
	project_m_reach			numeric(30,5),
	omni_entity				nvarchar(300), -- intented to be parent company owner (e.g. Apple, Alphabet, Oath, etc.)
	common_category_source	nvarchar(300),
	common_supercategory	nvarchar(300),
	common_category			nvarchar(300),
	common_subcategory		nvarchar(300),
	common_vertical			nvarchar(300),
	common_subvertical		nvarchar(300),
	sw_category				nvarchar(300),
	sw_subcategory			nvarchar(300),
	iab_category			nvarchar(300),
	iab_subcategory			nvarchar(300),
	rps_flag				int,
	rpt_flag				int,
	date_created			datetime,
	date_updated			datetime,
	update_reason			nvarchar(300),
	property_hash           bigint
);

insert into ref.Taxonomy_Common_Properties
select property_name,
	digital_type_id,
	'US' as project_country,
    project_sample_source,
	project_sample_size,
    project_m_reach,
    omni_entity,
    common_category_source,
    common_supercategory,
    common_category,
    common_subcategory,
    common_vertical,
    common_subvertical,
    sw_category,
    sw_subcategory,
    iab_category,
    iab_subcategory,
    rps_flag,
    rpt_flag,
    date_created,
    date_updated,
    update_reason,
	core.dbo.text_hash(property_name) as property_hash
from ref.Taxonomy_Common_2018;

create index var1 on ref.Taxonomy_Common_Properties (property_name);
create index var2 on ref.Taxonomy_Common_Properties (digital_type_id);



-----------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------
-- UPDATE TABLE WITH PROPS FROM  BIG Q1/Q2 2019 PROJECTS
--------------------------------------------------------
-- select top 100 * from ref.Taxonomy_Common_Properties order by digital_type_id;
-- select top 100 * from [MayoClinic].[rpt].[Standard_Metrics_Property];
-- MAYO
insert into ref.Taxonomy_Common_Properties
select a.property as property_name, 
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'US' as project_country,
    '2019 Mayo' as project_sample_source,
	max(a.du) as project_sample_size,
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as project_m_reach,
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from [MayoClinic].[rpt].[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type -- dedupe in case there are multiple entries per property
; 
-- (1403 rows affected)

-- GSK NASAL
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'DE' as project_country,
    '2019 GSK Nasal' as [project_sample_source],
	max(a.du) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from GSKNasal.[rpt].[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type -- dedupe in case there are multiple entries per property
;
-- (207 rows affected)

-- BAYER NAPPY
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'BR' as project_country,
    '2019 Bayer Nappy Rash' as [project_sample_source],
	max(a.du) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-29' as [date_created], '2019-08-29' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from BayerNappy.[rpt].[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.mp_qualmonths > 0 and a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type -- dedupe in case there are multiple entries per property


-- UPDATE TABLE WITH PROPS FROM 2018 PROJECTS
--------------------------------------------------------
-- L'OREAL CDJ
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'US' as project_country,
    '2018 LOreal' as [project_sample_source],
	max(a.du) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from Loreal2018.[rpt].[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.mp_qualmonths > 0 and a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type -- dedupe in case there are multiple entries per property
;
-- (2104 rows affected)

-- P&G
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'US' as project_country,
    '2018 PG Skincare' as [project_sample_source],
	max(a.du) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from PGSkincare.[rpt].[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.mp_qualmonths > 0 and a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type -- dedupe in case there are multiple entries per property
;
-- (214 rows affected)

-- Google Auto
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'US' as project_country,
    '2018 Google Auto' as [project_sample_source],
	max(a.du) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from GoogleAuto.[rpt].[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.mp_qualmonths > 0 and a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type -- dedupe in case there are multiple entries per property
;
-- (286 rows affected)

-- Google Ticketing
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'US' as project_country,
    '2018 Google Ticketing' as [project_sample_source],
	max(a.du) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from GoogleTicketing.dbo.[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.mp_qualmonths > 0 and a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type -- dedupe in case there are multiple entries per property
;
-- (877 rows affected)

-- Newell
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'US' as project_country,
    '2018 Newell' as [project_sample_source],
	max(a.du) as [project_sample_size],	
	max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from Newell.[rpt].[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.mp_qualmonths > 0 and a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type
; 
-- (89 rows affected)

-- Mars DMTM
-- select top 100 * from MarsDMTM.[rpt].[Standard_Metrics_Property];
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'US' as project_country,
    '2018 Mars DMTM' as [project_sample_source],
	max(a.du) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from MarsDMTM.[rpt].[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.mp_qualmonths > 0 and a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type
; 
-- (0 rows affected)
-- why no records added from mars? Believe this is because domains were pre-selected to be classified by SimilarWeb, which is input for
-- B script, run immediately before this step.

-- Mattel
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'UK' as project_country,
    '2018 Mattel' as [project_sample_source],
	max(a.du) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from Mattel.dbo.[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.mp_qualmonths > 0 and a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type
;
-- (532 rows affected)

-- Virginia Tourism
insert into ref.Taxonomy_Common_Properties
select a.property as property_name,
	case when digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'UK' as project_country,
    '2018 Virginia Tourism' as [project_sample_source],
	max(a.du) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-08-28' as [date_created], '2019-08-28' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property) as property_hash
from VirginiaTourism.rpt.[Standard_Metrics_Property] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property=b.property_name
where a.mp_qualmonths > 0 and a.property not like '%*%' -- removes '* Total Internet *'
	and digital_type in ('Web','App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type
;
-- (55 rows affected)

-- Projects without prop tables set up with standardized format.
-- 2019 Mars Gen Z
-- 2018 Google Gaming

-- UPDATE TABLE WITH CURRENT PROJECTS AFTER DATA CLOSE
--------------------------------------------------------
-- GoogleAuto2019
--1.Update core.ref.Taxonomy_Common_Properties with new properties from the project 
-- select top 100 * from core.ref.Taxonomy_Common_Properties order by digital_type_id;
insert into core.ref.Taxonomy_Common_Properties
select a.property_name, 
	a.digital_type_id,
	'INTL' as project_country,
    '2019 VW Google Auto' as project_sample_source,
	max(num_panelists) as project_sample_size,
    (max(1.0*num_panelists)/(1.0*1541)) as project_m_reach,
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-11-08' as [date_created], '2019-11-08' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property_name) as property_hash
from [GoogleAuto2019].[ref].[taxonomy_properties] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property_name=b.property_name
where b.property_name is null -- only add the entries not previously found
group by a.property_name, a.digital_type_id -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- (11572 row(s) affected)

--update from sequence event vw
insert into core.ref.Taxonomy_Common_Properties
select a.property_name, 
	a.digital_type_id,
	'INTL' as project_country,
    '2019 VW Google Auto' as project_sample_source,
	max(num_panelists) as project_sample_size,
    (max(1.0*num_panelists)/(1.0*1541)) as project_m_reach,
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.sw_category) as[sw_category], min(a.sw_subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-11-13' as [date_created], '2019-11-13' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property_name) as property_hash
from [GoogleAuto2019].[ref].[vw_taxonomy_properties_2] a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property_name=b.property_name
where b.property_name is null -- only add the entries not previously found
and a.digital_type_id is not null and a.property_name is not null
group by a.property_name, a.digital_type_id -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
--(552 row(s) affected)


--2.update reach of existing properties using VW numbers:
-- VW property table:
-- select * from #VW_property_reach where property_name like '%search%' and digital_type_id = 100 order by project_m_reach desc;
begin try drop table #VW_property_reach end try begin catch end catch;
select a.property_name, 
	a.digital_type_id,
	'INTL' as project_country,
    '2019 VW Google Auto' as project_sample_source,
	max(num_panelists) as project_sample_size,
    (max(1.0*num_panelists)/(1.0*1541)) as project_m_reach,
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-11-08' as [date_created], '2019-11-08' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property_name) as property_hash
into #VW_property_reach
from [GoogleAuto2019].[ref].[taxonomy_properties] a
group by a.property_name, a.digital_type_id 
order by project_m_reach desc
; 
--(121125 row(s) affected)

-- Append VW reach to TBD properties:
-- update core taxonomy table TBD properties whose reach = 0:
-- select * from core.ref.Taxonomy_Common_Properties where common_supercategory = '[TBD]' and project_m_reach >= 0.01 order by project_m_reach desc;
-- select * from core.ref.Taxonomy_Common_Properties where common_supercategory = '[TBD]' and project_m_reach >= 0.01 and project_sample_source = '2019 VW Google Auto' order by project_m_reach desc;
-- 1203 new properties from VW
update core.ref.Taxonomy_Common_Properties set 
project_country = b.project_country,
project_sample_source = b.project_sample_source,
project_sample_size = b.project_sample_size,
project_m_reach = b.project_m_reach,
date_created = b.date_created,
date_updated = '2019-11-12',
update_reason = 'Project: New reach'
from core.ref.Taxonomy_Common_Properties a 
inner join #VW_property_reach b 
on a.property_hash = b.property_hash and a.property_name = b.property_name and a.digital_type_id = b.digital_type_id 
and a.common_supercategory = '[TBD]' and a.project_m_reach = 0;
--(57956 row(s) affected)


-- Samsung2019
--1.Update core.ref.Taxonomy_Common_Properties with new properties from the project 
-- select count(distinct panelist_key) from [Samsung2019].[Run].[sequence_event]; -- 645
-- select * from core.ref.Taxonomy_Common_Properties where project_sample_source = '2019 Samsung';
insert into core.ref.Taxonomy_Common_Properties
select a.property_name, 
	a.digital_type_id,
	'US' as project_country,
    '2019 Samsung' as project_sample_source,
	max(num_panelists) as project_sample_size,
    (max(1.0*num_panelists)/(1.0*645)) as project_m_reach,
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-11-08' as [date_created], '2019-11-08' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property_name) as property_hash
from Samsung2019.ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.digital_type_id -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- (16190 row(s) affected)

--2. update reach of existing properties using Samsung numbers:
-- property table:
-- select * from #property_reach where property_name like '%search%' and digital_type_id = 100 order by project_m_reach desc;
begin try drop table #samsung_property_reach end try begin catch end catch;
select a.property_name, 
	a.digital_type_id,
	'US' as project_country,
    '2019 Samsung' as project_sample_source,
	max(num_panelists) as project_sample_size,
    (max(1.0*num_panelists)/(1.0*645)) as project_m_reach,
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-12-02' as [date_created], '2019-12-02' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property_name) as property_hash
into #samsung_property_reach
from [Samsung2019].[ref].[taxonomy_properties] a
group by a.property_name, a.digital_type_id 
order by project_m_reach desc
; 
--(34963 row(s) affected)

update core.ref.Taxonomy_Common_Properties set 
project_country = b.project_country,
project_sample_source = b.project_sample_source,
project_sample_size = b.project_sample_size,
project_m_reach = b.project_m_reach,
date_created = b.date_created,
date_updated = '2019-12-02',
update_reason = 'Project: New reach'
from core.ref.Taxonomy_Common_Properties a 
inner join #samsung_property_reach b 
on a.property_hash = b.property_hash and a.property_name = b.property_name and a.digital_type_id = b.digital_type_id 
and a.common_supercategory = '[TBD]' and a.project_m_reach = 0;
--(5266 row(s) affected)


--Nestle 2019 Update:
--1.Update core.ref.Taxonomy_Common_Properties with new properties from the project 
insert into core.ref.Taxonomy_Common_Properties
select a.property_name, 
	a.digital_type_id,
	'US' as project_country,
    '2019 Nestle' as project_sample_source,
	max(num_panelists) as project_sample_size,
    Nestle_m_reach as project_m_reach,
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-12-11' as [date_created], '2019-12-11' as[date_updated], 'Project: New properties' [update_reason],
	a.property_hash
from Nestle2019.ref.taxonomy_properties a
left outer join core.ref.Taxonomy_Common_Properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.digital_type_id,Nestle_m_reach,a.property_hash -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
-- (23566 row(s) affected)

--2. update reach of existing properties using Nestle numbers:
update core.ref.Taxonomy_Common_Properties set 
project_country = 'US',
project_sample_source = '2019 Nestle',
project_sample_size = max(b.num_panelists),
project_m_reach = b.Nestle_m_reach,
date_created = a.date_created,
date_updated = '2019-12-02',
update_reason = 'Project: New reach'
from core.ref.Taxonomy_Common_Properties a 
inner join Nestle2019.ref.taxonomy_properties b 
on a.property_hash = b.property_hash and a.property_name = b.property_name and a.digital_type_id = b.digital_type_id 
and a.common_supercategory = '[TBD]' and a.project_m_reach = 0;

-- UPDATE TABLE WITH REPRESENTIVITY AND REPORTABLITY LOGIC
----------------------------------------------------------

-- Browsers
update ref.Taxonomy_Common_Properties
set common_category_source = 'Scripts C1.2019',
	common_supercategory = '[Remove]',
	common_category = '[Remove: Browser]',
	common_subcategory = '[Remove: Browser]',
	rps_flag=0,
	rpt_flag=0,
	update_reason = 'Remove: Browser'
from ref.Taxonomy_Common_Properties
where property_name in ('Chrome', 'chrome.exe', 'Internet Explorer', 'iexplore.exe', 'Firefox', 'firefox.exe', 'Safari', 'safari.exe',
	'Microsoft Edge','Microsoft.MicrosoftEdge','Microsoft Edge Web Platform','Microsoft Edge Preview (Unreleased)', 'UC Browser',
	'Opera', 'opera.exe', 'Mozilla', 'mozilla.exe');

-- Research and Marketing Properties
update ref.Taxonomy_Common_Properties
set common_category = common_subcategory, rps_flag=0, rpt_flag=0, update_reason = 'Remove: Research/Marketing'
from ref.Taxonomy_Common_Properties where common_subcategory='[Remove: Research/Marketing]';

-- Blocked Properties
update ref.Taxonomy_Common_Properties
set common_category = common_subcategory, rps_flag=0, rpt_flag=0, update_reason = 'Remove: Blocked'
from ref.Taxonomy_Common_Properties where common_subcategory='[Remove: Blocked]';

-- Alert Businesses (e.g. adobe, oracle)
update ref.Taxonomy_Common_Properties
set common_category = common_subcategory, rps_flag=1, rpt_flag=0, update_reason = 'Alert: Business Services'
from ref.Taxonomy_Common_Properties where common_subcategory='[Alert: Business Services]';

-- Alert Skew
update ref.Taxonomy_Common_Properties
set common_category = common_subcategory, rps_flag=1, rpt_flag=0, update_reason = 'Alert: Business Services'
from ref.Taxonomy_Common_Properties where common_subcategory='[Alert: Bias/Skew]';

-- Add Hash
-- select top 100 * from core.ref.Taxonomy_Common_Properties;
-- select top 100 * from ref.Taxonomy_Common_Properties where property_hash is null;
--ALTER TABLE ref.Taxonomy_Common_Properties ADD property_hash bigint;
--update ref.Taxonomy_Common_Properties set property_hash = core.dbo.text_hash(property_name) from ref.Taxonomy_Common_Properties;

-- Add digital type text
-- select top 100 * from core.ref.Taxonomy_Common_Properties;
-- select top 100 * from ref.Taxonomy_Common_Properties where property_hash is null;
/**
ALTER TABLE core.ref.Taxonomy_Common_Properties ADD digital_type nvarchar(300);
update core.ref.Taxonomy_Common_Properties set 
digital_type = (case when digital_type_id = 100 then 'App' when digital_type_id = 235 then 'Web' end) 
from core.ref.Taxonomy_Common_Properties;
**/

-- Check at end of common cats so far
-------------------------------------
--select count(*) from ref.Taxonomy_Common_Properties;
--722184

select common_supercategory, common_category, common_subcategory, rps_flag, rpt_flag, count(*) as ct_properties
from ref.Taxonomy_Common_Properties
group by common_supercategory, common_category, common_subcategory, rps_flag, rpt_flag
order by 1,2,3 desc;

-- END OF FILE --
