---------------------------------------------------------------------------------------------------
-- C) IPSOS COMMON TAXONOMY: VENDOR AUTOCLASSIFY COMMON
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/12/12)
---------------------------------------------------------------------------------------------------

use core;

-- C2) AUTOMATICALLLY CLASSIFY PROPERTIES TO A COMMON-CATEGORY BASED ON SIMILAR-WEB-CATEGORY
---------------------------------------------------------------------------------------------------
-- c2. vendor autoclasssify common

-- Only do this if Common_Supercategory was not assigned in earlier B1 script.
-- Keep them on alert and non-reportable until they are vetted through QA
---------------------------------------------------------------------------------------------------
/***
select common_supercategory, common_category, common_subcategory, count(*) as ct_properties
from ref.Taxonomy_common_2020
group by common_supercategory, common_category, common_subcategory
order by 1,4 desc;
****/

update ref.Taxonomy_common_2020 set common_supercategory='[TBD]' from ref.Taxonomy_common_2020 where common_supercategory ='' and vetted_flag=0;
-- (735937 rows affected)


-- SimilarWeb Remap Categories
------------------------------
update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify remove', common_supercategory='[Remove]', common_category='[Remove: Blocked]', vetted_flag=1
	from ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in ('Blocked'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Shopping', common_subcategory='[Alert: TBD Shopping]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in ('Shopping'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Finance/Work/Gov', common_subcategory='[Alert: TBD Finance/Work/Gov]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in ('Law and Government', 'Career and Education'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Finance/Work/Gov', common_category='Finance', common_subcategory='[Alert: TBD Finance/Work/Gov]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in ('Finance', 'Personal Finance & Real Estate'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Lifestyle/Living', common_category='Health', common_subcategory='[Alert: TBD Health]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in ('Health'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Entertainment/Games', common_category='XXX Adult'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category like 'XXX%Adult'; -- and vetted_flag=0;

-- SimilarWeb Remap Subcategories
---------------------------------
update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Social/Photos', common_subcategory='[Alert: TBD Social/Photos]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_subcategory in ('Social Networking', 'Social Network'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Search', common_subcategory='[Alert: TBD Search]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and (sw_subcategory in ('Search Engine') or sw_category in ('Search & Portals')) and property_name not in ('Opera'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Email/Chat', common_subcategory='[Alert: TBD Email/Chat]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and (sw_category in ('Email', 'Telecommunications', 'Chats and Forums') or sw_category in ('Communication')); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='News/Media/Info', common_subcategory='[Alert: TBD News/Media/Info]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category='Internet and Telecom' and sw_subcategory='General'; -- and vetted_flag=0; -- most sites remaining are aol.com, blogspot.com 

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='News/Media/Info', common_subcategory='[Alert: TBD News/Media/Info]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in ('News and Media', 'Reference'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Utility/Other', common_subcategory='[Alert: TBD Utility/Other]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category='Internet and Telecom' and sw_subcategory in ('Web Hosting', 'Mobile Developers', 'Domain Names and Register', 'File Sharing', 'Web Design'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Entertainment/Games', common_subcategory='[Alert: TBD Entertainment/Games]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in ('Arts and Entertainment', 'Games', 'Sports', 'Media Content', 'Sports & Gaming'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Shopping', common_subcategory='[Alert: TBD Shopping]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in ('Shopping', 'Computer and Electronics', 'General Commerce', 'Technology Commerce'); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='[Alert]', common_subcategory='[Alert: Business Services]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category='Business and Industry'; -- and vetted_flag=0; -- these are prone to misclassification bias based on prior QA;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Lifestyle/Living', common_subcategory='[Alert: TBD Lifestyle/Living]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in (
	'Food and Drink',
	'People and Society',
	'Travel',
	'Gambling',
	'General Interest',
	'Travel',
	'Beauty and Fitness',
	'Autos and Vehicles',
	'Home and Garden',
	'Books and Literature',
 	'Science',
	'Pets and Animals',
	'Recreation and Hobbies'
); -- and vetted_flag=0;

update ref.Taxonomy_common_2020 set step_assigned='c2. vendor autoclasssify common', common_supercategory='Utility/Other', common_subcategory='[Alert: TBD Utility/Other]'
	from  ref.Taxonomy_common_2020 where common_supercategory='[TBD]' and sw_category in ('Other'); -- and vetted_flag=0;


-- UPDATE TABLE WITH REPRESENTIVITY AND REPORTABLITY LOGIC
----------------------------------------------------------
-- Browsers
-- N/A in future years. All should be addressed by 2020. Add if new browsers come online.

-- Research and Marketing Properties
update ref.Taxonomy_common_2020 set common_category = common_subcategory, rps_flag=0, rpt_flag=0, step_assigned='c2. vendor autoclasssify remove'
from ref.Taxonomy_common_2020 where common_subcategory='[Remove: Research/Marketing]' and vetted_flag=0;

-- Blocked Properties
update ref.Taxonomy_common_2020 set common_category = common_subcategory, rps_flag=0, rpt_flag=0, step_assigned='c2. vendor autoclasssify remove'
from ref.Taxonomy_common_2020 where common_subcategory='[Remove: Blocked]' and vetted_flag=0;

-- Alert Businesses (e.g. adobe, oracle)
update ref.Taxonomy_common_2020 set common_category = common_subcategory, rps_flag=1, rpt_flag=0, step_assigned='c2. vendor autoclasssify alert'
from ref.Taxonomy_common_2020 where common_subcategory='[Alert: Business Services]' and vetted_flag=0;

-- Alert Skew
update ref.Taxonomy_common_2020 set common_category = common_subcategory, rps_flag=1, rpt_flag=0, step_assigned='c2. vendor autoclasssify alert'
from ref.Taxonomy_common_2020 where common_subcategory='[Alert: Bias/Skew]' and vetted_flag=0;


-- select top 100 * from core.ref.Taxonomy_Common_2020 order by digital_type_id;

-- select count(*) from core.ref.Taxonomy_Common_2020;	--825274
-- select count(*) from core.ref.Taxonomy_common_2020;	--801708

-- select top 100 * from core.ref.Taxonomy_Common_Properties order by digital_type_id;

select top 100 * from core.ref.Taxonomy_Common_2020 where step_assigned like 'c2%';
select count(*) from core.ref.Taxonomy_Common_2020 where step_assigned like 'c2%';

-- END OF FILE --
