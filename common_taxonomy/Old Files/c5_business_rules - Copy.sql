---------------------------------------------------------------------------------------------------
-- C) IPSOS COMMON TAXONOMY: AUTOMATED BUSINESS RULES
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/12/12)
---------------------------------------------------------------------------------------------------

use core;

-----------------------------------------------------------------
-- C5. APPLY BUSINESS RULES
-----------------------------------------------------------------
-- C5. CATEGORY ASSIGNMENT BUSINESS RULES
-- select * from ref.Taxonomy_common_2020 where common_category = '[Alert: Business Services]' order by project_m_reach desc;
/**
update ref.Taxonomy_common_2020
set step_assigned='c5. business rules', common_subcategory='[Remove: Blocked]'
where common_supercategory = '[Remove]' and common_category = '[Remove: Blocked]' and vetted_flag=0;

update ref.Taxonomy_common_2020
set step_assigned='c5. business rules', common_supercategory = '[TBD]', common_category='[Alert: TBD]', common_subcategory='[Alert: TBD]'	
where common_supercategory = '[TBD]' and vetted_flag=0;
**/

update ref.Taxonomy_common_2020
set step_assigned='c5. business rules', common_supercategory = '[TBD]', common_category='[Alert: TBD Research/Marketing]', common_subcategory='[Alert: TBD Research/Marketing]'	
where common_supercategory = '[TBD]' and vetted_flag=0 
and (property_name like '%survey%' or property_name like '%opinion%' or property_name like '%panel%' or property_name like '%research%' or property_name like '%nielsen%')
;


-- update missing/wrong common_category:
-- select * from ref.Taxonomy_common_2020 where common_subcategory = '[Alert: Business/Service]';
update ref.Taxonomy_common_2020
set step_assigned='c5. business rules', common_category='[Alert: Business Services]', common_subcategory = '[Alert: Business Services]'
where common_supercategory = '[Alert]' and (common_category='[Alert: Business/Service]' or common_subcategory = '[Alert: Business/Service]') and vetted_flag=0;

/**
-- C5. SUBCATEGORY ASSIGNMENT BUSINESS RULES

-- alert adult properties so they are not reported
update ref.Taxonomy_common_2020
set common_subcategory='[Alert: Discretion Adult]'
where common_category like ('%XXX%Adult%');

-- coupons
update ref.Taxonomy_common_2020
set common_subcategory='[Alert: Discretion Coupons]'
where common_supercategory = 'Shopping' and common_category = 'Coupons';

update ref.Taxonomy_common_2020
set common_subcategory='[Remove: Offline Utility]'
where common_supercategory = '[Remove]' and common_category = '[Remove: Offline Utility]';

update ref.Taxonomy_common_2020
set common_subcategory='[Remove: Research/Marketing]'
where common_supercategory = '[Remove]' and common_category = '[Remove: Research/Marketing]';
**/

-----------------------------------------------------------------
-- C5. APPLY REPRESENTITY AND REPORTABILITY LOGIC BASED ON SUPERCAT
-----------------------------------------------------------------
update ref.Taxonomy_common_2020 set rps_flag=0, rpt_flag=0 from ref.Taxonomy_common_2020 where common_subcategory like '%Remove%';
update ref.Taxonomy_common_2020 set rpt_flag=0 from ref.Taxonomy_common_2020 where common_subcategory like '%Alert%' and vetted_flag=0;


select top 100 * from core.ref.Taxonomy_Common_2020 where step_assigned like 'c5%';
select count(*) from core.ref.Taxonomy_Common_2020 where step_assigned like 'c5%';


-- END OF FILE --
