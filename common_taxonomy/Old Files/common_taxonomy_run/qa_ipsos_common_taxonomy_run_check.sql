---------------------------------------------------------------------------------------------------
-- IPSOS COMMON TAXONOMY CHECKS
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/11/08)
---------------------------------------------------------------------------------------------------
-- Used in production steps to check data and updates once data collection starts, at 50% mark,
-- and at close of tracking period.
use core;

-- select count(*) from ref.Taxonomy_Common_Properties_run;
-- 733108 by 12/4

-- 0. Update reach of TBD properties using current project reach
-- GoogleAuto2019
--2.update reach of existing properties using VW numbers:
-- VW property table:
-- select * from #VW_property_reach where property_name like '%search%' and digital_type_id = 100 order by project_m_reach desc;
begin try drop table #VW_property_reach end try begin catch end catch;
select a.property_name, 
	a.digital_type_id,
	'INTL' as project_country,
    '2019 VW Google Auto' as project_sample_source,
	max(num_panelists) as project_sample_size,
    (max(1.0*num_panelists)/(1.0*1541)) as project_m_reach,
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-11-08' as [date_created], '2019-11-08' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property_name) as property_hash
into #VW_property_reach
from [GoogleAuto2019].[ref].[taxonomy_properties] a
group by a.property_name, a.digital_type_id 
order by project_m_reach desc
; 
--(121125 row(s) affected)

-- Append VW reach to TBD properties:
-- update core taxonomy table TBD properties whose reach = 0:
-- select * from core.ref.Taxonomy_Common_Properties_run where common_supercategory = '[TBD]' and project_m_reach >= 0.01 order by project_m_reach desc;
-- select * from core.ref.Taxonomy_Common_Properties_run where common_supercategory = '[TBD]' and project_m_reach >= 0.01 and project_sample_source = '2019 VW Google Auto' order by project_m_reach desc;
-- 1203 new properties from VW
update core.ref.Taxonomy_Common_Properties_run set 
project_country = b.project_country,
project_sample_source = b.project_sample_source,
project_sample_size = b.project_sample_size,
project_m_reach = b.project_m_reach,
date_created = b.date_created,
date_updated = '2019-11-12',
update_reason = 'Project: New reach'
from core.ref.Taxonomy_Common_Properties_run a 
inner join #VW_property_reach b 
on a.property_hash = b.property_hash and a.property_name = b.property_name and a.digital_type_id = b.digital_type_id 
and a.common_supercategory = '[TBD]' and a.project_m_reach = 0;
--(57956 row(s) affected)

--Samsung2019
--2. update reach of existing properties using Samsung numbers:
-- property table:
-- select * from #property_reach where property_name like '%search%' and digital_type_id = 100 order by project_m_reach desc;
begin try drop table #samsung_property_reach end try begin catch end catch;
select a.property_name, 
	a.digital_type_id,
	'US' as project_country,
    '2019 Samsung' as project_sample_source,
	max(num_panelists) as project_sample_size,
    (max(1.0*num_panelists)/(1.0*645)) as project_m_reach,
	'' as omni_entity,
    '' as common_category_source,    
	'' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
    1 as rps_flag, 1 as rpt_flag,
    '2019-12-02' as [date_created], '2019-12-02' as[date_updated], 'Project: New properties' [update_reason],
	core.dbo.text_hash(a.property_name) as property_hash
into #samsung_property_reach
from [Samsung2019].[ref].[taxonomy_properties] a
group by a.property_name, a.digital_type_id 
order by project_m_reach desc
; 
--(34963 row(s) affected)

update core.ref.Taxonomy_Common_Properties_run set 
project_country = b.project_country,
project_sample_source = b.project_sample_source,
project_sample_size = b.project_sample_size,
project_m_reach = b.project_m_reach,
date_created = b.date_created,
date_updated = '2019-12-02',
update_reason = 'Project: New reach'
from core.ref.Taxonomy_Common_Properties_run a 
inner join #samsung_property_reach b 
on a.property_hash = b.property_hash and a.property_name = b.property_name and a.digital_type_id = b.digital_type_id 
and a.common_supercategory = '[TBD]' and a.project_m_reach = 0;
--(3274 row(s) affected)

--Nestle 2019
--2. update reach of existing properties using Nestle numbers:
-- select top 100 * from core.ref.Taxonomy_Common_Properties_run
update core.ref.Taxonomy_Common_Properties_run set 
project_country = 'US',
project_sample_source = '2019 Nestle',
--project_sample_size = b.num_panelists,
project_m_reach = b.Nestle_m_reach,
date_created = a.date_created,
date_updated = '2019-12-11',
update_reason = 'Project: New reach'
from core.ref.Taxonomy_Common_Properties_run a 
inner join Nestle2019.ref.taxonomy_properties b 
on a.property_hash = b.property_hash and a.property_name = b.property_name and a.digital_type_id = b.digital_type_id 
and a.common_supercategory = '[TBD]' and a.project_m_reach = 0;
--(4866 row(s) affected)

-- select * from core.ref.Taxonomy_Common_Properties_run where project_sample_source = '2019 Nestle'and update_reason = 'Project: New reach'
select top 100 * from core.ref.Taxonomy_Common_Properties

-- 1. All properties:
-- See how SimilarWeb Classifies
-------------------------------------
select sw_category, sw_subcategory, rps_flag, rpt_flag, count(*) as ct_properties
from ref.Taxonomy_Common_Properties_run
group by sw_category, sw_subcategory, rps_flag, rpt_flag 
order by 1,2,3 desc;


-- Check at end of common cats so far
-------------------------------------
select common_supercategory, common_category, common_subcategory, rps_flag, rpt_flag, count(*) as ct_properties
from ref.Taxonomy_Common_Properties_run
group by common_supercategory, common_category, common_subcategory, rps_flag, rpt_flag
order by 1,2,3 desc;

--select * from ref.Taxonomy_Common_Properties_run where common_category = 'General Retailer' and rpt_flag = 0;
-- select * from core.ref.Taxonomy_Common_Properties where property_name like '%driver%'


-- 2. Project output: all new properties from current project
select
	property_hash, 
	property_name,
	digital_type_id,
	project_sample_size,
    project_m_reach,
	sw_category,
    sw_subcategory,
    common_supercategory,
    common_category,
    common_subcategory,
    rps_flag,
    rpt_flag
from core.ref.Taxonomy_Common_Properties_run
where project_sample_source = '2019 Nestle' -- project name
and rps_flag = 0
--and common_subcategory like '%tbd%'
order by project_m_reach desc;

--Project QA:
-- TBD properties n core table that are not from VW:
select *
from core.ref.Taxonomy_Common_Properties_run
where common_supercategory = '[TBD]' and project_sample_source !='2019 Nestle';

-- 1. See how SimilarWeb Classifies
-------------------------------------
select sw_category, sw_subcategory, rps_flag, rpt_flag, count(*) as ct_properties
from core.ref.Taxonomy_Common_Properties_run
where common_supercategory = '[TBD]' and project_sample_source ='2019 Nestle'
group by sw_category, sw_subcategory, rps_flag, rpt_flag 
order by 1,2,3 desc;

-- 2. Check at end of common cats so far
-------------------------------------
select common_supercategory, common_category, common_subcategory, rps_flag, rpt_flag, count(*) as ct_properties
from core.ref.Taxonomy_Common_Properties_run
where common_supercategory = '[TBD]' and project_sample_source ='2019 Nestle'
group by common_supercategory, common_category, common_subcategory, rps_flag, rpt_flag
order by 1,2,3 desc;

select common_supercategory, common_category, common_subcategory, rps_flag, rpt_flag, count(*) as ct_properties
from core.ref.Taxonomy_Common_Properties_run
where project_sample_source ='2019 Nestle'
group by common_supercategory, common_category, common_subcategory, rps_flag, rpt_flag
order by 1,2,3 desc;


-- END OF FILE --
