---------------------------------------------------------------------------------------------------
-- IPSOS COMMON TAXONOMY CHECKS
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/11/08)
---------------------------------------------------------------------------------------------------
-- Used in production steps to check data and updates once data collection starts, at 50% mark,
-- and at close of tracking period.
use core;

--------------------------------------
-- 1. Functional Super-Categories
-------------------------------------
-- 0. ad hoc checks:
select * from ref.Taxonomy_Common_2020 
where  common_supercategory not in ('[TBD]', '[Alert]', '[Remove]') 
and common_subcategory not like '%Alert%' and rpt_flag = 0

select * from ref.Taxonomy_Common_2020 
where  common_supercategory not in ('[TBD]', '[Alert]', '[Remove]') 
and common_subcategory not like '%Alert%TBD%' and common_subcategory like '%Alert%' and rpt_flag = 1
-- should be coupons only



-- 1. Supercat/category/subcat/vertical checks:
	-- 1. check dup/typos in names
	-- 2. check rps/rpt flags
	---- a. all REMOVE should be rps = 0 rpt = 0
	---- b. all TBD supercat should be vetted = 0
	---- c. all ALERT cat should be rps = 1 rpt = 0 (for coupons, ecosystem related ones will be rpt = 1)
	---- d. for all others, # rps = # rpt = # properties
	-- 3. check max reach

-- 1. Supercat breakdown
select common_supercategory, count(*) as ct_properties, sum(vetted_flag) as vetted_properties,
	sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties, max(project_m_reach) as max_reach,
	avg(project_m_reach) as avg_reach 
--	sum(project_m_reach)/(select sum(project_m_reach) from core.ref.Taxonomy_Common_2020) as reach_share
from ref.Taxonomy_Common_2020
group by common_supercategory
order by 1,2,3,4,5 desc;

-- 2. By category
Select vetted_flag,
common_supercategory,
common_category,
count(distinct property_name) as property_count,
sum(project_m_reach) as sum_reach,
max(project_m_reach) as max_reach,
min(project_m_reach) as min_reach
from core.ref.Taxonomy_Common_2020 
group by vetted_flag, common_supercategory,common_category
order by 1 desc,2,3 desc, 4 desc
; 

-- 3. Supercat, cat, subcat breakdown
select a.common_supercategory,a.common_category,a.common_subcategory, count(*) as ct_properties, sum(vetted_flag) as vetted_properties,
	sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties, max(a.project_m_reach) as max_reach, avg(a.project_m_reach) as avg_reach 
--	sum(a.project_m_reach)/(select sum(project_m_reach) from core.ref.Taxonomy_Common_2020) as reach_share
from ref.Taxonomy_Common_2020 a
group by a.common_supercategory,a.common_category,a.common_subcategory
order by 1,2,3,4 desc;
/**
select common_supercategory,common_category,common_subcategory, count(*) as ct_properties, sum(vetted_flag) as vetted_properties,
sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties, max(project_m_reach) as max_reach, avg(project_m_reach) as avg_reach, 
--sum(project_m_reach)/(select sum(project_m_reach) from core.ref.Taxonomy_Common_2020) as reach_share,
property_name as max_property, rn
from (select *,
 row_number() over(partition by a.common_category,a.common_subcategory order by a.project_m_reach desc) as rn
from ref.Taxonomy_Common_2020 as a) as a
group by common_supercategory,common_category,common_subcategory,property_name
order by 1,2,3,4,5;

-- Overall category breakdown + top 3 properties
-- select * from #category_QA order by 1,2,3,4 desc;
begin try drop table #category_QA end try begin catch end catch
select a.common_supercategory,a.common_category,a.common_subcategory, count(*) as ct_properties, sum(vetted_flag) as vetted_properties,
	sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties, max(a.project_m_reach) as max_reach, avg(a.project_m_reach) as avg_reach 
--	sum(a.project_m_reach)/(select sum(project_m_reach) from core.ref.Taxonomy_Common_2020) as reach_share
into #category_QA
from ref.Taxonomy_Common_2020 a
group by a.common_supercategory,a.common_category,a.common_subcategory
order by 1,2,3,4 desc;

begin try drop table #category_QA_1 end try begin catch end catch
select a.*, b.property_name as max_property_1 
into #category_QA_1
from #category_QA a
left outer join #top_properties b
on a.common_supercategory = b.common_supercategory and a.common_category=b.common_category and a.common_subcategory = b.common_subcategory
where b.rn=1
order by 1,2,3,4 desc;

begin try drop table #category_QA_2 end try begin catch end catch
select a.*, b.property_name as max_property_2
into #category_QA_2
from #category_QA_1 a
left join #top_properties b
on a.common_supercategory = b.common_supercategory and a.common_category=b.common_category and a.common_subcategory = b.common_subcategory
where b.rn=2
order by 1,2,3,4 desc;

begin try drop table #category_QA_3 end try begin catch end catch
select a.*, b.property_name as max_property_3
into #category_QA_3
from #category_QA_2 a
left join #top_properties b
on a.common_supercategory = b.common_supercategory and a.common_category=b.common_category and a.common_subcategory = b.common_subcategory
where b.rn=3
order by 1,2,3,4 desc;

select * from #category_QA_3 order by 1,2,3,4 desc;
**/

-- 4. By vertical
Select vetted_flag,
common_vertical,
count(distinct property_name) as property_count,
sum(project_m_reach) as sum_reach,
max(project_m_reach) as max_reach,
min(project_m_reach) as min_reach
from core.ref.Taxonomy_Common_2020 
group by vetted_flag, common_vertical
order by 1 desc,2,3 desc, 4 desc
; 

/***
select common_supercategory, common_vertical, common_subvertical, count(*) as ct_properties, sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties 
from ref.Taxonomy_Common_2020
--where common_supercategory = 'Shopping'
group by common_supercategory, common_vertical, common_subvertical
order by 1,2,3 desc;
***/

/***
select common_supercategory, common_category, common_vertical, common_subvertical, count(*) as ct_properties, sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties 
from ref.Taxonomy_Common_2020
--where common_supercategory = 'Shopping'
group by common_supercategory,common_category, common_vertical, common_subvertical
order by 1,2,3 desc;
***/

/**
-- Shopping properties that don't have vertical:
select * from core.ref.Taxonomy_Common_2020 
where common_supercategory = 'Shopping' and common_vertical = ''
and common_category != 'Coupons'
order by project_m_reach desc;
**/


-- 5. check sw category 
select distinct sw_category from core.ref.Taxonomy_Common_2020;

select a.property_name, b.Domain_Name, a.sw_category, b.Category, a.sw_subcategory, b.SubCategory
from core.ref.Taxonomy_Common_2020 a
inner join [PassiveMaster].[Reference].[SimilarWeb_Domain_Classification] b
on a.property_name = b.Domain_Name and a.property_hash = b.Domain_Name_Hash and a.digital_type_id = 235
where a.sw_category = 'uncategorized' 
and b.category is not null
;
select a.property_name, b.Domain_Name, a.sw_category, b.Category, a.sw_subcategory, b.SubCategory
from core.ref.Taxonomy_Common_Properties a
inner join [PassiveMaster].[Reference].[SimilarWeb_Domain_Classification] b
on a.property_name = b.Domain_Name and a.property_hash = b.Domain_Name_Hash and a.digital_type_id = 235
where a.sw_category = 'uncategorized' 
and b.category is not null
;
select a.property_name, b.Domain_Name, a.sw_category, b.Category, a.sw_subcategory, b.SubCategory
from core.ref.Taxonomy_Common_Properties a
inner join [GooglePoland2019].[Wrk].[Domain_Name_Categorized] b
on a.property_name = b.Domain_Name and a.property_hash = b.Domain_Name_Hash and a.digital_type_id = 235
where 
--Domain_Name = 'hustlersites.com'
a.sw_category = 'uncategorized' 
and b.category != ''
;



--------------------------------------
-- 2. See where steps are assigned
-------------------------------------

select step_assigned, count(*) as ct_properties, sum(vetted_flag) as vetted_properties, sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties
from ref.Taxonomy_Common_2020
group by step_assigned
order by 1,2 desc;

--------------------------------------
-- 3. Join to Project Volume and Check
--------------------------------------
-- Nestle QA
-------------
select isnull(b.common_supercategory,'') as common_supercategory, sum(a.total_mins*rps_flag) as rps_minutes,
	max(num_panelists) as max_prop_sample,  max(Nestle_m_reach) as max_prop_reach, count(*) as ct_properties,
	sum(vetted_flag) as vetted_properties, sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties
from Nestle2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
group by b.common_supercategory
order by 1;

-- Nestle Highest Minutes Outliers (That still have rps_flag=1)
----------------------------------------------------------------
-- drop table #Nestle_QA_with_dupes
select top 100 isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,
	a.property_name, a.digital_type_id, Nestle_m_reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
into #Nestle_QA_with_dupes
from Nestle2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.common_supercategory is null or ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
order by a.total_mins desc;

-- Insert Nestle Outliers that have 5% reach (That still have rps_flag=1)
----------------------------------------------------------------
insert into #Nestle_QA_with_dupes
select isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,
	a.property_name, a.digital_type_id, Nestle_m_reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
from Nestle2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where a.Nestle_m_reach>=.05 and ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
order by a.Nestle_m_reach desc;

select common_supercategory, common_category, property_name, digital_type_id, Nestle_m_reach, total_mins, rps_flag, rpt_flag, vetted_flag
from #Nestle_QA_with_dupes
group by common_supercategory, common_category, property_name, digital_type_id, Nestle_m_reach, total_mins, rps_flag, rpt_flag, vetted_flag
order by Nestle_m_reach desc, total_mins desc;


-- Samsung QA
-------------
-- select count(*) from Samsung2019.ref.panelist

select isnull(b.common_supercategory,'') as common_supercategory, sum(a.total_mins*rps_flag) as rps_minutes,
	max(num_panelists) as max_prop_sample,  max(a.num_panelists/653.0) as max_prop_reach, count(*) as ct_properties,
	sum(vetted_flag) as vetted_properties, sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties
from Samsung2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
group by b.common_supercategory
order by 1;

-- Samsung Highest Minutes Outliers (That still have rps_flag=1)
----------------------------------------------------------------
-- drop table #Samsung_QA_with_dupes;
select top 100 isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,
	a.property_name, a.digital_type_id, num_panelists/653.0 as reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
into #Samsung_QA_with_dupes
from Samsung2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.common_supercategory is null or ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
	and a.property_name not in (select property_name from #Nestle_QA_with_dupes)
order by a.total_mins desc;

-- Insert Samsung Outliers that have 5% reach (That still have rps_flag=1)
----------------------------------------------------------------
insert into #Samsung_QA_with_dupes
select isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,
	a.property_name, a.digital_type_id, num_panelists/653.0 as reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
from Samsung2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where (num_panelists/653.0)>=.05 and ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
	and a.property_name not in (select property_name from #Nestle_QA_with_dupes)
order by num_panelists desc;

select common_supercategory, common_category, property_name, digital_type_id, reach, total_mins, rps_flag, rpt_flag, vetted_flag
from #Samsung_QA_with_dupes
group by common_supercategory, common_category, property_name, digital_type_id, reach, total_mins, rps_flag, rpt_flag, vetted_flag
order by reach desc, total_mins desc;

--select * from core.ref.Taxonomy_Common_2020 where property_sample_size > 500 order by project_m_reach desc

-- Google Poland QA
-------------
select
	property_hash, 
	property_name,
	digital_type_id,
	property_sample_size,
    project_m_reach,
	sw_category,
    sw_subcategory,
    common_supercategory,
    common_category,
    common_subcategory,
    rps_flag,
    rpt_flag
from core.ref.Taxonomy_Common_2020
where project_sample_source = '2019 GooglePoland' -- project name
order by project_m_reach desc;

-- select count(*) from Samsung2019.ref.panelist

select isnull(b.common_supercategory,'') as common_supercategory, sum(a.total_mins*rps_flag) as rps_minutes,
	max(num_panelists) as max_prop_sample,  max(GooglePoland2019_m_reach) as max_prop_reach, count(*) as ct_properties,
	sum(vetted_flag) as vetted_properties, sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties
from GooglePoland2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
group by b.common_supercategory
order by 1;

-- Google Poland Highest Minutes Outliers (That still have rps_flag=1)
----------------------------------------------------------------
-- drop table #GooglePoland_QA_with_dupes;
select top 100 isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,
	a.property_name, a.digital_type_id, b.project_m_reach as reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
into #GooglePoland_QA_with_dupes
from GooglePoland2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.common_supercategory is null or ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
--	and a.property_name not in (select property_name from #GooglePoland_QA_with_dupes)
order by a.total_mins desc;

-- Insert Google Poland Outliers that have 5% reach (That still have rps_flag=1)
----------------------------------------------------------------
insert into #GooglePoland_QA_with_dupes
select isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,
	a.property_name, a.digital_type_id, b.project_m_reach as reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
from GooglePoland2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where GooglePoland2019_m_reach>=.05 and ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
and a.property_name not in (select property_name from #GooglePoland_QA_with_dupes)
order by num_panelists desc;

select common_supercategory, common_category, property_name, digital_type_id, reach, total_mins, rps_flag, rpt_flag, vetted_flag
from #GooglePoland_QA_with_dupes
group by common_supercategory, common_category, property_name, digital_type_id, reach, total_mins, rps_flag, rpt_flag, vetted_flag
order by reach desc, total_mins desc;

-- GP Vetted vs non-vetted share of minutes & reach:
-- 1. Topline
Select a.vetted_flag,
count(distinct a.property_name) as property_count,
sum(b.total_mins) as sum_mins,
sum(b.total_mins)/(select sum(a.total_mins) from GooglePoland2019.ref.taxonomy_properties a inner join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name and a.digital_type_id = b.digital_type_id) as mins_share,
max(b.GooglePoland2019_m_reach) as max_reach,
min(b.GooglePoland2019_m_reach) as min_reach,
max(b.total_mins) as max_min,
min(b.total_mins) as min_min
from ref.Taxonomy_Common_2020 a
inner join GooglePoland2019.ref.taxonomy_properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
--where project_sample_source = '2019 GooglePoland'
group by a.vetted_flag
order by 1 desc
; 

-- 2. By supercat
Select a.vetted_flag,
a.common_supercategory,
count(distinct a.property_name) as property_count,
sum(b.total_mins) as sum_mins,
sum(b.total_mins)/(select sum(a.total_mins) from GooglePoland2019.ref.taxonomy_properties a inner join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name and a.digital_type_id = b.digital_type_id) as mins_share,
max(b.GooglePoland2019_m_reach) as max_reach,
min(b.GooglePoland2019_m_reach) as min_reach,
max(b.total_mins) as max_min,
min(b.total_mins) as min_min
from core.ref.Taxonomy_Common_2020 a
inner join GooglePoland2019.ref.taxonomy_properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
--where project_sample_source = '2019 GooglePoland'
group by a.vetted_flag, a.common_supercategory
order by 1 desc
; 
--(34963 row(s) affected)

-- 2. By vertical
Select a.vetted_flag,
a.common_vertical,
count(distinct a.property_name) as property_count,
sum(b.total_mins) as sum_mins,
sum(b.total_mins)/(select sum(a.total_mins) from GooglePoland2019.ref.taxonomy_properties a inner join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name and a.digital_type_id = b.digital_type_id) as mins_share,
max(b.GooglePoland2019_m_reach) as max_reach,
min(b.GooglePoland2019_m_reach) as min_reach,
max(b.total_mins) as max_min,
min(b.total_mins) as min_min
from core.ref.Taxonomy_Common_2020 a
inner join GooglePoland2019.ref.taxonomy_properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
--where project_sample_source = '2019 GooglePoland'
group by a.vetted_flag, a.common_vertical
order by 1 desc,3 desc
; 

-- PMI QA
-------------
select
	property_hash, 
	property_name,
	digital_type_id,
	property_sample_size,
    project_m_reach,
	sw_category,
    sw_subcategory,
    common_supercategory,
    common_category,
    common_subcategory,
    rps_flag,
    rpt_flag
from core.ref.Taxonomy_Common_2020
where project_sample_source = '2020 PhilipMorris' -- project name
order by project_m_reach desc;

-- select count(*) from PhilipMorris2020.ref.panelist; -- 14

select isnull(b.common_supercategory,'') as common_supercategory, sum(a.total_mins*rps_flag) as rps_minutes,
	max(num_panelists) as max_prop_sample,  max(PhilipMorris2020_m_reach) as max_prop_reach, count(*) as ct_properties,
	sum(vetted_flag) as vetted_properties, sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties
from PhilipMorris2020.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
group by b.common_supercategory
order by 1;

-- PMI Highest Minutes Outliers (That still have rps_flag=1)
----------------------------------------------------------------
-- drop table #PhilipMorris2020_QA_with_dupes;
select top 100 isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,
	a.property_name, a.digital_type_id, b.project_m_reach as reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
into #PhilipMorris2020_QA_with_dupes
from GooglePoland2019.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.common_supercategory is null or ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
--	and a.property_name not in (select property_name from #GooglePoland_QA_with_dupes)
order by a.total_mins desc;

-- Insert PhilipMorris2020 Outliers that have 5% reach (That still have rps_flag=1)
----------------------------------------------------------------
insert into #PhilipMorris2020_QA_with_dupes
select isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,
	a.property_name, a.digital_type_id, b.project_m_reach as reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
from PhilipMorris2020.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where PhilipMorris2020_m_reach>=.05 and ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
and a.property_name not in (select property_name from #GooglePoland_QA_with_dupes)
order by num_panelists desc;

select common_supercategory, common_category, property_name, digital_type_id, reach, total_mins, rps_flag, rpt_flag, vetted_flag
from #PhilipMorris2020_QA_with_dupes
group by common_supercategory, common_category, property_name, digital_type_id, reach, total_mins, rps_flag, rpt_flag, vetted_flag
order by reach desc, total_mins desc;

-- PMI Vetted vs non-vetted share of minutes & reach:
-- 1. Topline
Select a.vetted_flag,
count(distinct a.property_name) as property_count,
sum(b.total_mins) as sum_mins,
sum(b.total_mins)/(select sum(a.total_mins) 
from PhilipMorris2020.ref.taxonomy_properties a 
inner join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name and a.digital_type_id = b.digital_type_id) as mins_share,
max(b.PhilipMorris2020_m_reach) as max_reach,
min(b.PhilipMorris2020_m_reach) as min_reach,
max(b.total_mins) as max_min,
min(b.total_mins) as min_min
from ref.Taxonomy_Common_2020 a
inner join PhilipMorris2020.ref.taxonomy_properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
group by a.vetted_flag
order by 1 desc
; 

-- 2. By supercat
Select a.vetted_flag,
a.common_supercategory,
count(distinct a.property_name) as property_count,
sum(b.total_mins) as sum_mins,
sum(b.total_mins)/(select sum(a.total_mins) 
from PhilipMorris2020.ref.taxonomy_properties a 
inner join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name and a.digital_type_id = b.digital_type_id) as mins_share,
max(b.PhilipMorris2020_m_reach) as max_reach,
min(b.PhilipMorris2020_m_reach) as min_reach,
max(b.total_mins) as max_min,
min(b.total_mins) as min_min
from core.ref.Taxonomy_Common_2020 a
inner join PhilipMorris2020.ref.taxonomy_properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
group by a.vetted_flag, a.common_supercategory
order by 1 desc
; 
--(34963 row(s) affected)

-- 2. By vertical
Select a.vetted_flag,
a.common_vertical,
count(distinct a.property_name) as property_count,
sum(b.total_mins) as sum_mins,
sum(b.total_mins)/(select sum(a.total_mins) 
from PhilipMorris2020.ref.taxonomy_properties a 
inner join core.ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name and a.digital_type_id = b.digital_type_id) as mins_share,
max(b.PhilipMorris2020_m_reach) as max_reach,
min(b.PhilipMorris2020_m_reach) as min_reach,
max(b.total_mins) as max_min,
min(b.total_mins) as min_min
from core.ref.Taxonomy_Common_2020 a
inner join PhilipMorris2020.ref.taxonomy_properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
--where project_sample_source = '2019 GooglePoland'
group by a.vetted_flag, a.common_vertical
order by 1 desc,3 desc
; 

-- KnowledgePanel QA
-------------
select
	property_hash, 
	property_name,
	digital_type_id,
	property_sample_size,
    project_m_reach,
	sw_category,
    sw_subcategory,
    common_supercategory,
    common_category,
    common_subcategory,
    rps_flag,
    rpt_flag
from core.ref.Taxonomy_Common_2020
where project_sample_source = '2020 KnowledgePanel' -- project name
order by project_m_reach desc;

-- check high reach ad/marketing sites for KP
select top 2000 b.KnowledgePanel2020_m_reach, a.* from ref.Taxonomy_Common_Properties a
inner join KnowledgePanel2020.ref.taxonomy_properties b
on a.property_hash = b.property_hash
where vetted_flag = 0 and rpt_flag = 1
--common_category = '[Remove: Research/Marketing]' 
order by KnowledgePanel2020_m_reach desc;

select top 100 * from ref.Taxonomy_Common_Properties a
inner join KnowledgePanel2020.ref.taxonomy_properties b
on a.property_hash = b.property_hash
where sw_subcategory in ('Marketing and Advertising','Market Research & Points Rewards') and rps_flag = 1 order by project_m_reach desc;

-- select count(*) from Samsung2019.ref.panelist

select isnull(b.common_supercategory,'') as common_supercategory, sum(a.total_mins*rps_flag) as rps_minutes,
	max(num_panelists) as max_prop_sample,  max(KnowledgePanel2020_m_reach) as max_prop_reach, count(*) as ct_properties,
	sum(vetted_flag) as vetted_properties, sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties
from KnowledgePanel2020.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_2020 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
group by b.common_supercategory
order by 1;

-- kp Highest Minutes Outliers (That still have rps_flag=1)
----------------------------------------------------------------
-- drop table #KnowledgePanel2020_QA_with_dupes;
select top 100 isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,sw_subcategory,
	a.property_name, a.digital_type_id, b.project_m_reach as reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
into #KnowledgePanel2020_QA_with_dupes
from KnowledgePanel2020.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_Properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.common_supercategory is null or sw_subcategory in ('Marketing and Advertising','Market Research & Points Rewards') or ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
--	and a.property_name not in (select property_name from #GooglePoland_QA_with_dupes)
order by a.total_mins desc;

-- Insert Outliers that have 5% reach (That still have rps_flag=1)
----------------------------------------------------------------
insert into #KnowledgePanel2020_QA_with_dupes
select isnull(b.common_supercategory,'') as common_supercategory,  isnull(b.common_supercategory,'') as common_category,sw_subcategory,
	a.property_name, a.digital_type_id, b.project_m_reach as reach, a.total_mins, rps_flag, rpt_flag, vetted_flag
from KnowledgePanel2020.ref.taxonomy_properties a
left outer join ref.Taxonomy_Common_Properties b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where KnowledgePanel2020_m_reach>=.05 and ((common_supercategory like '%Alert%' or common_supercategory like '%Remove%' or common_supercategory like '%TBD%') and b.rps_flag=1)
and a.property_name not in (select property_name from #KnowledgePanel2020_QA_with_dupes)
order by num_panelists desc;

select common_supercategory, common_category, property_name, sw_subcategory, digital_type_id, reach, total_mins, rps_flag, rpt_flag, vetted_flag
from #KnowledgePanel2020_QA_with_dupes
group by common_supercategory, common_category, sw_subcategory,property_name, digital_type_id, reach, total_mins, rps_flag, rpt_flag, vetted_flag
order by reach desc, total_mins desc;


--------------------------------------
-- 4. Check Sample Size 
--------------------------------------
select distinct project_sample_source, project_country, project_sample_size, max(property_sample_size) as max_property_size, count(distinct property_name) as count_property
from ref.Taxonomy_Common_2020
group by project_sample_source, project_country, project_sample_size
order by project_sample_source, project_sample_size desc;

select top 1000 * from ref.Taxonomy_Common_2020 where project_sample_size < 100 and project_sample_source != '';

select count(*) from ref.Taxonomy_Common_2020
;
-- END OF FILE --
