---------------------------------------------------------------------------------------------------
-- IPSOS COMMON TAXONOMY RELEASE
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/11/12)
---------------------------------------------------------------------------------------------------
-- One the production/update scripts pass QA, officially release the latest version of script.
use [9Now2020];


begin try drop table ref.Taxonomy_Common_Properties_final end try begin catch end catch;
create table ref.Taxonomy_Common_Properties_final  (
	property_name			nvarchar(4000),
	property_key			bigint,
	property_hash			bigint,
	digital_type_id			int,
	project_country			nvarchar(300),
	project_sample_source	nvarchar(300),
	project_sample_size	bigint,
	property_sample_size	bigint,
	project_m_reach			numeric(30,5),
	omni_entity				nvarchar(300), -- intented to be parent company owner (e.g. Apple, Alphabet, Oath, etc.)
	common_supercategory	nvarchar(300),
	common_category			nvarchar(300),
	common_subcategory		nvarchar(300),
	common_vertical			nvarchar(300),
	common_subvertical		nvarchar(300),
	sw_category				nvarchar(300),
	sw_subcategory			nvarchar(300),
	iab_category			nvarchar(300),
	iab_subcategory			nvarchar(300),
	rps_flag				int,
	rpt_flag				int,
	vetted_flag				int,
	date_created			date,
	date_updated			date,
	step_assigned			nvarchar(300),
);


begin try drop table ref.Taxonomy_Common_Properties_final end try begin catch end catch;

select a.*
into ref.Taxonomy_Common_Properties_final
from core.ref.Taxonomy_Common_Properties a;


update [9Now2020].ref.Taxonomy_Common_Properties_final set vetted_flag = 1, common_supercategory='[Remove]', common_category='[Remove: Research/Marketing]', common_subcategory='[Remove: Research/Marketing]', common_vertical='', rps_flag=0, rpt_flag=0, date_updated= '01/09/2021', step_assigned= 'c3. ecosystem reclassify common' from [9Now2020].ref.Taxonomy_Common_Properties_final where property_name = 'LightSpeed';
update [9Now2020].ref.Taxonomy_Common_Properties_final set vetted_flag = 1, common_supercategory='[Remove]', common_category='[Remove: Research/Marketing]', common_subcategory='[Remove: Research/Marketing]', common_vertical='', rps_flag=0, rpt_flag=0, date_updated= '01/09/2021', step_assigned= 'c3. ecosystem reclassify common' from [9Now2020].ref.Taxonomy_Common_Properties_final where property_name = 'lightspeedresearch.com';

--select top 1000 * from ref.Taxonomy_Common_Properties_KP_Oct
-- select * from [9Now2020].ref.Taxonomy_Common_Properties_final where property_name = 'LightSpeed';
--QA
/**
select common_supercategory, count(*) as ct_properties, sum(vetted_flag) as vetted_properties,
	sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties, max(project_m_reach) as max_reach,
	avg(project_m_reach) as avg_reach 
--	sum(project_m_reach)/(select sum(project_m_reach) from core.ref.Taxonomy_Common_2020) as reach_share
from core.ref.Taxonomy_Common_Properties
group by common_supercategory
order by 1,2,3,4,5 desc;

select common_supercategory, count(*) as ct_properties, sum(vetted_flag) as vetted_properties,
	sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties, max(project_m_reach) as max_reach,
	avg(project_m_reach) as avg_reach 
--	sum(project_m_reach)/(select sum(project_m_reach) from core.ref.Taxonomy_Common_2020) as reach_share
from [9Now2020].ref.Taxonomy_Common_Properties_final
group by common_supercategory
order by 1,2,3,4,5 desc;
**/

-- END OF FILE --
