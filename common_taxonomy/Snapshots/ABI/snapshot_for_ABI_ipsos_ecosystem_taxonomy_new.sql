---------------------------------------------------------------------------------------------------
-- IPSOS COMMON TAXONOMY RELEASE
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/11/12)
---------------------------------------------------------------------------------------------------
-- One the production/update scripts pass QA, officially release the latest version of script.
use [ABI2020];


begin try drop table ref.Taxonomy_Common_Properties_Ecosystem_New end try begin catch end catch;
create table ref.Taxonomy_Common_Properties_Ecosystem_New  (
	property_name			nvarchar(4000),
	property_key			bigint,
	property_hash			bigint,
	digital_type_id			int,
	project_country			nvarchar(300),
	project_sample_source	nvarchar(300),
	project_sample_size	bigint,
	property_sample_size	bigint,
	project_m_reach			numeric(30,5),
	omni_entity				nvarchar(300), -- intented to be parent company owner (e.g. Apple, Alphabet, Oath, etc.)
	common_supercategory	nvarchar(300),
	common_category			nvarchar(300),
	common_subcategory		nvarchar(300),
	common_vertical			nvarchar(300),
	common_subvertical		nvarchar(300),
	sw_category				nvarchar(300),
	sw_subcategory			nvarchar(300),
	iab_category			nvarchar(300),
	iab_subcategory			nvarchar(300),
	rps_flag				int,
	rpt_flag				int,
	vetted_flag				int,
	date_created			date,
	date_updated			date,
	step_assigned			nvarchar(300),
);


begin try drop table ref.Taxonomy_Common_Properties_Ecosystem_New end try begin catch end catch;

select a.*
into ref.Taxonomy_Common_Properties_Ecosystem_New
from core.ref.Taxonomy_Common_Properties a;

-- select top 1000 * from ref.Taxonomy_Common_Properties_Ecosystem_New where project_sample_source like '%ABI%' and vetted_flag = 1
-- select top 1000 * from ref.Taxonomy_Common_Properties_Ecosystem_New where date_updated = '12/17/2020';
-- select top 1000 * from ref.Taxonomy_Common_Properties_Ecosystem where property_name = 'daisurvey.com'


-- END OF FILE --
