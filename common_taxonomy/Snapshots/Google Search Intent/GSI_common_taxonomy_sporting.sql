---------------------------------------------------------------------------------------------------
-- IPSOS COMMON TAXONOMY RELEASE
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/11/12)
---------------------------------------------------------------------------------------------------
-- One the production/update scripts pass QA, officially release the latest version of script.
use [GoogleSearchIntent2020];


begin try drop table ref.Taxonomy_Common_Properties_Sporting_Goods end try begin catch end catch;
create table ref.Taxonomy_Common_Properties_Sporting_Goods  (
	property_name			nvarchar(4000),
	property_key			bigint,
	property_hash			bigint,
	digital_type_id			int,
	project_country			nvarchar(300),
	project_sample_source	nvarchar(300),
	project_sample_size	bigint,
	property_sample_size	bigint,
	project_m_reach			numeric(30,5),
	omni_entity				nvarchar(300), -- intented to be parent company owner (e.g. Apple, Alphabet, Oath, etc.)
	common_supercategory	nvarchar(300),
	common_category			nvarchar(300),
	common_subcategory		nvarchar(300),
	common_vertical			nvarchar(300),
	common_subvertical		nvarchar(300),
	sw_category				nvarchar(300),
	sw_subcategory			nvarchar(300),
	iab_category			nvarchar(300),
	iab_subcategory			nvarchar(300),
	rps_flag				int,
	rpt_flag				int,
	vetted_flag				int,
	date_created			date,
	date_updated			date,
	step_assigned			nvarchar(300),
);


begin try drop table ref.Taxonomy_Common_Properties_Sporting_Goods end try begin catch end catch;

select a.*
into ref.Taxonomy_Common_Properties_Sporting_Goods
from core.ref.Taxonomy_Common_Properties a;

--update ref.Taxonomy_Common_Properties_Electronics set vetted_flag = 1, common_supercategory='Shopping', common_category='General Retailer', common_subcategory='', common_vertical='General Retail', rps_flag=1, rpt_flag=1, date_updated= '01/29/2021', step_assigned= 'c3. ecosystem reclassify common' from ref.Taxonomy_Common_Properties_Electronics where property_name = 'Amazon Shopping - Search, Find, Ship, and Save';
-- select property_name from ref.Taxonomy_Common_Properties_Sporting_Goods where date_updated = '03/11/2021'


--select top 1000 * from ref.Taxonomy_Common_Properties_KP_Oct
-- select * from ref.Taxonomy_Common_Properties_Sporting_Goods where property_name = 'homegoods.com';
--QA
/**
select common_supercategory, count(*) as ct_properties, sum(vetted_flag) as vetted_properties,
	sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties, max(project_m_reach) as max_reach,
	avg(project_m_reach) as avg_reach 
--	sum(project_m_reach)/(select sum(project_m_reach) from core.ref.Taxonomy_Common_2020) as reach_share
from core.ref.Taxonomy_Common_Properties
group by common_supercategory
order by 1,2,3,4,5 desc;

select common_supercategory, count(*) as ct_properties, sum(vetted_flag) as vetted_properties,
	sum(rps_flag) as rps_properties,  sum(rpt_flag) as rpt_properties, max(project_m_reach) as max_reach,
	avg(project_m_reach) as avg_reach 
--	sum(project_m_reach)/(select sum(project_m_reach) from core.ref.Taxonomy_Common_2020) as reach_share
from [googlesearchintent2020].ref.Taxonomy_Common_Properties_Electronics
group by common_supercategory
order by 1,2,3,4,5 desc;
**/

-- END OF FILE --
