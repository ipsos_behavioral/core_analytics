---------------------------------------------------------------------------------------------------
-- IPSOS COMMON TAXONOMY RELEASE
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/11/12)
---------------------------------------------------------------------------------------------------
use core;

-- snapshot table for Nestle

begin try drop table ref.Taxonomy_Common_Properties_Nestle end try begin catch end catch;
select a.*
into ref.Taxonomy_Common_Properties_Nestle
from ref.Taxonomy_Common_2020 a;


-- END OF FILE --
