---------------------------------------------------------------------------------------------------
-- B) IPSOS COMMON TAXONOMY: 2018 (ARCHIVES)
---------------------------------------------------------------------------------------------------
-- Jimmy	(2019/11/02)
---------------------------------------------------------------------------------------------------
use core;

-- 1) CREATE COMMON TAXONOMY. START WITH BASELINE DOMAINS FROM 2018 THIRD PARTY DATA
-------------------------------------------------------------------------------------

-- select * from raw.Input_3PcS_201805_Domain_Metrics order by domain_name;
begin try drop table ref.Taxonomy_Common_2018 end try begin catch end catch;
create table ref.Taxonomy_Common_2018  (
	property_name			varchar(300),
	digital_type_id			int,
	project_country			varchar(30),
	project_sample_source	nvarchar(300),
	project_sample_size		bigint,
	property_sample_size	bigint,
	project_m_reach			numeric(30,5),
	omni_entity				nvarchar(300), -- intented to be parent company owner (e.g. Apple, Alphabet, Oath, etc.)
	common_category_source	nvarchar(300),
	common_supercategory	nvarchar(300),
	common_category			nvarchar(300),
	common_subcategory		nvarchar(300),
	common_vertical			nvarchar(300),
	common_subvertical		nvarchar(300),
	sw_category				nvarchar(300),
	sw_subcategory			nvarchar(300),
	iab_category			nvarchar(300),
	iab_subcategory			nvarchar(300),
	rps_flag				int, -- 1 means property is representative of online behavior. 0 means we remove it (exclude/blacklist) from mid-stage production.
	rpt_flag				int, -- 1 means property is reportable. 0 means we leave it out of reports for the client due to discretion.
	date_created			datetime,
	date_updated			datetime,
	update_reason			nvarchar(300)
);

insert into ref.Taxonomy_Common_2018
select 
	domain_name as property_name,
	235 as digital_type_id,
	'' as project_country,
	'' as project_sample_source,
	1 as project_sample_size,
	1 as property_sample_size,
	0 as project_m_reach,
	'' as omni_entity,
	'' as common_category_source,
	'' as common_supercategory,
	'' as common_category,
	'' as common_subcategory,
	'' as common_vertical,
	'' as common_subvertical,
	isnull(replace(category,'XAd','X Ad'),'') as sw_category,
	isnull(subcategory,'') as sw_subcategory,
	'' as iab_category,
	'' as iab_subcategory,
	1 as rps_flag,
	1 as rpt_flag,
	date_created,
	date_updated,
	isnull(update_reason,'') as update_reason
from PassiveMaster.reference.SimilarWeb_Domain_Classification where date_created < '2020-02-03';
-- (355531 row(s) affected)
-- select * from PassiveMaster.reference.SimilarWeb_Domain_Classification where date_created > '2019-01-01'
-- ####################################
-- Insert all entities in YouTube Shopper Study that wasn't previously classifies as domain
-- select * from YouTubeShopping.[dbo].[Standard_Metrics_Property] order by dp;
insert into ref.Taxonomy_Common_2018
select a.property as property_name,
	case when a.digital_type='Web' then 235 when digital_type='App' then 100 else null end as digital_type_id,
	'US' as project_country, 
    '2018 Google YT Shopper' as [project_sample_source],
	max(a.du) as [property_sample_size],
	max(a.dp) as [project_sample_size],
    max((1.0*a.mu_usemonths)/(1.0*a.mp_qualmonths)) as [project_m_reach],
	'' as [omni_entity],
    '' as [common_category_source],    
	'' as [common_supercategory], '' as [common_category], '' as [common_subcategory], '' as [common_vertical], '' as [common_subvertical],
    min(a.category) as[sw_category], min(a.subcategory) as [sw_subcategory],
    '' as [iab_category], '' as [iab_subcategory],
	1 as rps_flag,
	1 as rpt_flag,
    '2019-06-28' as [date_created], '2019-06-28' as[date_updated], 'Project: New properties' [update_reason]
from YouTubeShopping.[dbo].[Standard_Metrics_Property] a
left outer join ref.Taxonomy_Common_2018 b
	on a.property=b.property_name
where digital_type in ('Web', 'App') and du>1 and b.property_name is null -- only add the entries not previously found
group by property, digital_type;

create index var1 on ref.Taxonomy_Common_2018(property_name);

/**
-- update sw categories using most updated version 
update core.ref.Taxonomy_Common_2018 set 
sw_category = b.Category, 
sw_subcategory = b.SubCategory
from core.ref.Taxonomy_Common_2018 a
inner join [PassiveMaster].[Reference].[SimilarWeb_Domain_Classification] b
on a.property_name = b.Domain_Name and a.digital_type_id = 235
where a.sw_category = 'uncategorized' and b.category is not null
;
**/

-- QA check
-- Confirm domain and hash combinations are unique
-- select property_name, count(*) as ct_records from ref.Taxonomy_Common_2018 group by property_name order by 2 desc;
-- select sw_category, count(*) as ct_properties from core.ref.Taxonomy_Common_2018 group by sw_category order by 1;

-- 1B) BLACKLIST
----------------
-- select * from GoogleTicketing.[Ref].[Blacklist] where blacklist_flag=1 and property like '%.%';
-- select * from PassiveMaster.ref.Taxonomy_Common_2018 where domain_name in ('ge.com', 'groceryoutlet.com', 'emporium.com', 'smartandfinal.com');

update ref.Taxonomy_Common_2018
set common_category_source='2017 Google Ticketing',
	rps_flag=0,
	rpt_flag=0,
	common_supercategory='[Remove]',
	common_subcategory='[Remove: Blocked]'
from ref.Taxonomy_Common_2018 a
inner join GoogleTicketing.[Ref].[Blacklist] b
	on a.property_name=b.Property
	and property not in ('answers.com') -- outlier for that project but not true blacklist
	and b.blacklist_flag=1;

update ref.Taxonomy_Common_2018
set common_category_source='2018 Similar Web',
	rps_flag=0,
	rpt_flag=0,
	common_supercategory='[Remove]',
	common_subcategory='[Remove: Research/Marketing]'
from ref.Taxonomy_Common_2018
where sw_subcategory='Marketing and Advertising';

update ref.Taxonomy_Common_2018
set common_category_source='2018 07 Update',
	rps_flag=0,
	rpt_flag=0,
	common_supercategory='[Remove]',
	common_subcategory='[Remove: Research/Marketing]'
where property_name in (
	'foresee.com',
	'dsp.io',
	'vidible.tv',
	'placeiq.com',
	'pch.com',
	'kampyle.com',
	'pushcrew.com',
	'uservoice.com',
	'mybuys.com',
	'bluecore.com',
	'yotpo.com',
	'po.st',
	'trustwave.com',
	'jwplayer.com',
	'privy.com',
	'connatix.com',
	'productreportcard.com',
	'shareaholic.com',
	'mobify.net',
	'checkout51.com',
	'salecycle.com'
	);


-- 1C) GREYLIST
----------------
-- sites undisclosed for discretion. sites that index higher or may skew are often in this section.

update ref.Taxonomy_Common_2018
set common_category_source='2018 07 Update',
	rpt_flag=0, -- remove from reportability
	common_supercategory='[Alert]',
	common_subcategory='[Alert: Business Services]'
where  sw_category='Business and Industry' and sw_subcategory in ('Business Services', 'General')
	and property_name not in ('eventbrite.com', 'offerup.com', 'bbb.org', 'salesforce.com', 'nordstromrack.com', 'angieslist.com', 'hbonow.com', 'amc.com', 'ge.com', 'groceryoutlet.com', 'emporium.com', 'smartandfinal.com');

update ref.Taxonomy_Common_2018
set common_category_source='2018 07 Update',
	rpt_flag=0, -- remove from reportability
	common_supercategory='[Alert]',
	common_subcategory='[Alert: Bias/Skew]'
where property_name in (
	'ibotta.com',
	'lowermybills.com',
	'shoprunner.com'
	)

-- 2A) POPULATE COMMON TAXONOMY WITH YOUTUBE SHOPPER (2017 GYTS)
----------------------------------------------------------------

-- Note: Unfortunately popular sites like Amazon, Gap, etc are not picked up this way because our reports have it at the true "Property" level and not "domain"
-- Fix this to some degree by populating Taxnomy from GYTS.

select source_id, source from YouTubeShopping.dbo.TaxonomyEcosystemCorrected group by source_id, source;

-- select * from #taxonomy_update_2017gyts order by dp desc;
begin try drop table #taxonomy_update_2017gyts end try begin catch end catch;
select 
	case when source='Web: Domain' then 235 when source='App' then 100 else null end as digital_type_id,
cast(source_unique_key as nvarchar(300)) as property_name,
	a.eco_category1 as category_channel,
	a.retail_category as category_retail,
	bdg_display as omni_entity,
	cast(1 as bigint) as dp,
	cast(1 as bigint) as du,
	cast(1 as bigint) as mp_qualmonths,
	cast(0 as bigint) as mu_usemonths,
	cast(0 as bigint) as mu_visits,
	cast(0 as bigint) as mu_mins
into #taxonomy_update_2017gyts
from YouTubeShopping.dbo.TaxonomyEcosystemCorrected a
where source in ('Web: Domain', 'App') and
	eco_category1 != 'Social Retail'; -- don't count pinterest.

update #taxonomy_update_2017gyts
set dp=b.dp, du=b.du, mp_qualmonths=b.mp_qualmonths, mu_usemonths=b.mu_usemonths,
	mu_visits=(b.mp_qualmonths*b.du_visits)/b.dp_qualdays,
	mu_mins=(b.mp_qualmonths*b.du_mins)/b.dp_qualdays
from #taxonomy_update_2017gyts a
inner join YouTubeShopping.dbo.Standard_Metrics_Eco_Property b
	on a.property_name=b.property
where b.device_type='All Devices' and b.category <> '* Total Retail *' and b.report_level = 'Property';

-- QA: check if brands can be all consolidated. Yes then can.
select * from #taxonomy_update_2017gyts where category_channel like '%brand%' order by mu_usemonths desc, mu_visits desc;
update #taxonomy_update_2017gyts set category_channel='Brand' where category_channel like '%brand%';

-- Standardize Naming of Categories
update #taxonomy_update_2017gyts set category_channel='Discount/Rewards' where category_channel like '%Discount/Rewards%';
update #taxonomy_update_2017gyts set category_channel='Marketplace' where category_channel like '%Marketplace%';
update #taxonomy_update_2017gyts set category_channel='Resources' where category_channel like '%Resource%';
update #taxonomy_update_2017gyts set category_channel='Reviews/Info' where category_channel like '%Reviews/Info%';

-- Standardize Naming of Content
update #taxonomy_update_2017gyts set category_retail='Auto' where category_retail='Automobiles';
update #taxonomy_update_2017gyts set category_retail='Auto Parts' where category_retail='Automobiles Parts or Accessories';
update #taxonomy_update_2017gyts set category_retail='Beauty' where category_retail='Beauty, Skincare and Cosmetics';
update #taxonomy_update_2017gyts set category_retail='Books/Magazines' where category_retail='Books or Magazines';
update #taxonomy_update_2017gyts set category_retail='Apparel' where category_retail='Clothing or Apparel';
update #taxonomy_update_2017gyts set category_retail='Food/Groceries' where category_retail='Food and Groceries';

update #taxonomy_update_2017gyts set category_retail='Food/Groceries' where category_retail='Food and Groceries';
update #taxonomy_update_2017gyts set category_retail='Media/Entertainment' where category_retail='Media and Entertainment';
update #taxonomy_update_2017gyts set category_retail='Travel' where category_retail='Personal or Vacation Travel';

update #taxonomy_update_2017gyts set category_retail='Pets' where category_retail='Pet Supplies';
update #taxonomy_update_2017gyts set category_retail='Shoes/Footwear' where category_retail='Shoes or Footwear';
update #taxonomy_update_2017gyts set category_retail='Sporting/Fitness' where category_retail='Sporting or Fitness Goods';
update #taxonomy_update_2017gyts set category_retail='Alcohol/Beverage' where category_retail='Wine, Beer, Spirits or Beverage';

-- Standarize Naming of Omni Entity
update #taxonomy_update_2017gyts set omni_entity='Google' where (property_name like 'google.__' or property_name like 'google.___' or property_name like 'google.co.*');
update #taxonomy_update_2017gyts set omni_entity='Amazon' where (property_name like 'amazon.__' or property_name like 'amazon.___' or property_name like 'amazon.com.*');
update #taxonomy_update_2017gyts set omni_entity='eBay' where (property_name like 'ebay.__' or property_name like 'ebay.___' or property_name like 'ebay.com.*');
update #taxonomy_update_2017gyts set omni_entity='Expedia' where (property_name like 'expedia.__' or property_name like 'expedia.___' or property_name like 'expedia.com.*'  or property_name like 'expedia.co.*');

create index #taxonomy_update_2017gyts on #taxonomy_update_2017gyts(property_name);

select category_channel, sum(mu_visits) as mu_visits
from #taxonomy_update_2017gyts
group by category_channel
order by 1;

select category_retail, sum(mu_visits) as mu_visits
from #taxonomy_update_2017gyts
group by category_retail
order by 1;

-- UPATE
-- select * from #taxonomy_update_2017gyts
update ref.Taxonomy_Common_2018
set project_country='US',
	project_sample_source='2018 Google YT Shopper',
	omni_entity=b.omni_entity,
	common_category_source='2018 Google YT Shopper',
	common_supercategory='Shopping',
	common_category=category_channel,
	common_subcategory='', -- This field is commonly used as alerts. Alerts of Businesses from SimilarWeb were reviewed. All sampled checks cleared, so removing alert.
	common_vertical=category_retail,
	date_updated='2018-06-28 12:00:00.000',
	update_reason='Retail category baseline'
from ref.Taxonomy_Common_2018 a
inner join #taxonomy_update_2017gyts b
	on a.property_name=b.property_name
	and a.rps_flag=1; -- do not update exclusions
-- (8715 row(s) affected)

-- Not all "Shopping" from YT study are shopping site. Reassign them for now.
update ref.Taxonomy_Common_2018
set common_supercategory='News/Media/Info',
	common_category='',
	common_subcategory='', -- This field is commonly used as alerts. Alerts of Businesses from SimilarWeb were reviewed. All sampled checks cleared, so removing alert.
	update_reason='GYTS: News/Media/Info'
from ref.Taxonomy_Common_2018 where common_supercategory='Shopping' and common_category='Reviews/Info';

update ref.Taxonomy_Common_2018
set common_supercategory='[Alert]',
	common_category='[Alert: TBD]',
	common_subcategory='[Alert: TBD]',
	update_reason='GYTS: Resources'
from ref.Taxonomy_Common_2018 where common_supercategory='Shopping' and common_category='Resources';


update ref.Taxonomy_Common_2018
set property_sample_size = du, project_sample_size = dp, project_m_reach = (b.mu_usemonths*1.0)/(b.mp_qualmonths*1.0),
	rps_flag=1, rpt_flag=1
from ref.Taxonomy_Common_2018 a
inner join YouTubeShopping.[dbo].[Standard_Metrics_Property] b
	on a.property_name = b.property
	and a.project_sample_source ='2018 Google YT Shopper'

-- select * from ref.Taxonomy_Common_2018 where project_sample_source ='2018 Google YT Shopper' order by project_sample_size;
-- 11687 counts

-- ### Ad hoc edits ###
-----------------------
update ref.Taxonomy_Common_2018 set common_subvertical='Children' from ref.Taxonomy_Common_2018 a where property_name='disneybaby.com';


-- 2B) POPULATE COMMON TAXONOMY 3RD PARTY SOURCE (3PCS 2018 05)
---------------------------------------------------------------
-- select top 100 * from raw.Input_3PcS_201805_Domain_Metrics order by domain_name;

update ref.Taxonomy_Common_2018
set project_sample_source='2018 3PCS 05',
	property_sample_size = (b.project_mu_usemonths*1.0)/(b.project_mp_qualmonths*1.0)*b.project_sample_size,
	project_sample_size=b.project_sample_size,
	project_m_reach=(b.project_mu_usemonths*1.0)/(b.project_mp_qualmonths*1.0), -- this helps convert to decimal, otherwise returns integer 0 or 1
	date_updated='2018-07-02 03:19:20.000',
	update_reason='Gen pop baseline'
from ref.Taxonomy_Common_2018 a
inner join raw.Input_3PcS_201805_Domain_Metrics b
	on a.property_name=b.domain_name;


-- 3A) ADHOC QA EDITS
------------------------------------------------------------
-- select top 100 * from ref.Taxonomy_Common_2018 where property_name='nyc.gov';
update ref.Taxonomy_Common_2018 set omni_entity='New York Government', common_supercategory='Finance/Work/Gov', common_category='Government', update_reason='Adhoc 2017-07-10' where property_name='nyc.gov';

update ref.Taxonomy_Common_2018 set common_supercategory='[Remove]', common_category='[Remove: Research/Marketing]', common_subcategory='[Remove: Research/Marketing]',
	rps_flag=0, rpt_flag=0, update_reason='Remove: Research/Marketing'
	from ref.Taxonomy_Common_2018 where property_name='shoprunner.com';



-- 3) CHECKS ON RESULTS
------------------------------------------------------------

-- test resulting category map

select top 100 common_vertical, common_subvertical
from ref.Taxonomy_Common_2018
where project_sample_source <> ''
group by common_vertical, common_subvertical
order by 1,2;

select top 100 common_supercategory, common_category, common_subcategory, min(project_m_reach) as monthly_proxy_reach
from ref.Taxonomy_Common_2018
where project_sample_source <> ''
group by common_supercategory, common_category, common_subcategory
order by 1,2;

-- Sanity check. Highest contributing sites to visit
select top 100 property_name, count(*) as ct_record, min(project_m_reach) as monthly_reach
from ref.Taxonomy_Common_2018
group by property_name
order by 3 desc;

-- QA: Confirms apps as well as domains get inserted into properties table.
-- select top 100 * from ref.Taxonomy_Common_2018 where property_name like 'cvs%';

-- confirm if total internet is in taxonomy
-- select top 100 * from ref.Taxonomy_Common_2018 where property_name like '%*%';

-- QA: Check if alerts overrule assignment from GYTS study
-- select top 100 * from core.ref.Taxonomy_Common_2018 where common_subcategory like '%alert%bus%' and common_supercategory not like '%alert%';

-- check uniqueness
select property_name, count(*) as ct_records
from core.ref.Taxonomy_Common_2018
group by property_name
order by 2 desc;

-- END OF FILE --
