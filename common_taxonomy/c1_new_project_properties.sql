---------------------------------------------------------------------------------------------------
-- C) IPSOS COMMON TAXONOMY: ADD NEW PROJECT PROPERTIES
---------------------------------------------------------------------------------------------------
-- Script by Jimmy
-- Modified by Shirley (2021/3/25)
---------------------------------------------------------------------------------------------------

use core;

-- C0) PORT OVER 2021 PROPERTIES
---------------------------------------------------------------------------------------------------
-- Create shell for table. Combine both app and wec.
---------------------------------------------------------------------------------------------------
-- C0. port prior year properties

-- select top 100 * from core.ref.Taxonomy_Common_Properties order by digital_type_id;
-- select count(*) from core.ref.Taxonomy_Common_Properties;
-- 733108
/*
-- select top 100 * from core.ref.Taxonomy_Common_2021 order by digital_type_id;
-- select count(*) from core.ref.Taxonomy_Common_2021; --703945
begin try drop table ref.Taxonomy_Common_2022 end try begin catch end catch;
create table ref.Taxonomy_Common_2022  (
	property_name			nvarchar(4000),
	property_key			bigint,
	property_hash			bigint,
	digital_type_id			int,
	project_country			nvarchar(300),
	project_sample_source	nvarchar(300),
	project_sample_size	bigint,
	property_sample_size	bigint,
	project_m_reach			numeric(30,5),
	omni_entity				nvarchar(300), -- intented to be parent company owner (e.g. Apple, Alphabet, Oath, etc.)
	common_supercategory	nvarchar(300),
	common_category			nvarchar(300),
	common_subcategory		nvarchar(300),
	common_vertical			nvarchar(300),
	common_subvertical		nvarchar(300),
	sw_category				nvarchar(300),
	sw_subcategory			nvarchar(300),
	iab_category			nvarchar(300),
	iab_subcategory			nvarchar(300),
	rps_flag				int,
	rpt_flag				int,
	vetted_flag				int,
	date_created			date,
	date_updated			date,
	step_assigned			nvarchar(300),
);

insert into ref.Taxonomy_Common_2022
select
	property_name,
	property_key,
	property_hash,
	digital_type_id,
	project_country,
    project_sample_source,
	project_sample_size,
	property_sample_size,
    project_m_reach,
    omni_entity,
    common_supercategory,
    common_category,
    common_subcategory,
    common_vertical,
    common_subvertical,
    sw_category,
    sw_subcategory,
    iab_category,
    iab_subcategory,
    rps_flag,
    rpt_flag,
	vetted_flag,
    date_created,
    date_updated,
    step_assigned
from ref.Taxonomy_common_2021;

--create index var1 on ref.Taxonomy_Common_2022 (property_name);
create index var1 on ref.Taxonomy_Common_2022 (property_hash);
create index var2 on ref.Taxonomy_Common_2022 (property_key);
create index var3 on ref.Taxonomy_Common_2022 (digital_type_id);
*/
---------------------------------------------------------------------------------------------------
-- C1) 2021 NEW PROPERTIES
---------------------------------------------------------------------------------------------------
-- C1. new project properties

--------------------------------------------------------
-- UPDATE TABLE WITH PROPS FROM 2022 PROJECTS
--------------------------------------------------------
/*
insert into core.ref.Taxonomy_Common_2022
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2022 BrownForman' as project_sample_source, -- change to project name
	'1016' as project_sample_size, -- change to project N
	max(num_panelists) as property_sample_size, -- custom
    BrownForman2022_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2022-04-01' as [date_created], -- change date to today's date
	'2022-04-01' as[date_updated], 'c1. new project properties' as step_assigned -- same as date_created by defaule
from [BrownForman2022].ref.taxonomy_properties_v1 a
left outer join core.ref.Taxonomy_Common_2022 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235  or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,BrownForman2022_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 
*/

insert into core.ref.Taxonomy_Common_2022
select
	a.property_name, 0 as property_key, a.property_hash,
	a.digital_type_id,
	'US' as project_country,
    '2022 MarsDinner' as project_sample_source, -- change to project name
	'800' as project_sample_size, -- change to project N
	max(num_panelists) as property_sample_size, -- custom
    Mars2022_m_reach as project_m_reach, -- custom panel size denominator for samsung
	'' as omni_entity, '' as common_supercategory, '' as common_category, '' as common_subcategory, '' as common_vertical, '' as common_subvertical,
    min(a.category) as sw_category, min(a.subcategory) as sw_subcategory,
    '' as iab_category, '' as iab_subcategory,
    1 as rps_flag, 1 as rpt_flag, 0 as vetted_flag,
    '2022-05-03' as [date_created], -- change date to today's date
	'2022-05-03' as[date_updated], 'c1. new project properties' as step_assigned -- same as date_created by defaule
from [Mars2022].ref.taxonomy_properties_v1 a
left outer join core.ref.Taxonomy_Common_2022 b
	on a.property_name=b.property_name
	and a.digital_type_id = b.digital_type_id
where b.property_name is null -- only add the entries not previously found
and (a.digital_type_id = 235 or a.digital_type_id = 100)
group by a.property_name, a.property_hash, a.digital_type_id,Mars2022_m_reach, a.date_created -- dedupe in case there are multiple entries per property 
order by project_m_reach desc
; 






-----------------------------------------------
-- RUN THIS AFTER THE PROJECT INSERT SCRIPTS
-- update sw categories using most updated version 
-- (will update this step after stephen migrate the global sw classifications to core table.)
update core.ref.Taxonomy_Common_2022 set 
sw_category = b.Category, 
sw_subcategory = b.SubCategory
from core.ref.Taxonomy_Common_2022 a
inner join [PassiveMaster].[Reference].[SimilarWeb_Domain_Classification] b
on a.property_name = b.Domain_Name and a.property_hash = b.Domain_Name_Hash and a.digital_type_id = 235
where a.sw_category = 'uncategorized' and b.category is not null
;

-- QA
--select top 100 * from core.ref.Taxonomy_Common_2022 where step_assigned like 'c1%' and project_sample_source = '2022 MarsDinner';
--select count(*) from core.ref.Taxonomy_Common_2022 where step_assigned like 'c1%' and project_sample_source = '2021 BeamSuntory';
--select * from core.ref.Taxonomy_Common_2022 where step_assigned like 'c1%' and [date_created] = '2022-1-11';
--select count(*) from core.ref.Taxonomy_Common_2022 where step_assigned like 'c1%';

-- END OF FILE --