﻿---------------------------------------------------------------------------------------------------
-- C) IPSOS COMMON TAXONOMY: ECOSYSTEM RECLASSIFY COMMON
---------------------------------------------------------------------------------------------------
-- Script by Jimmy
-- Modified by Shirley (2021/3/25)
---------------------------------------------------------------------------------------------------

use core;

-- C3) RECLASSIFY PROPERTIES THAT HAVE VETTING DONE ALREADY IN ECOSYSTEM BUILD PROCESS
---------------------------------------------------------------------------------------------------
-- C3. ecosystem reclassify common






-- QA
-- select * from  Core.ref.Taxonomy_common_2022 where property_name = 'mandrillapp.com';
-- select * from  ref.Taxonomy_Common_2019 where property_name like '%hollister.com%'
-- select * from  ref.Taxonomy_Common_2018 where property_name like '%flickr.com%'
--  select * from core.ref.Taxonomy_common_2022 where date_updated = '1/11/2022' and vetted_flag = 1
-- select * from  ref.Taxonomy_common_2022 where property_name like '%wechat%'

-- select * from  ref.Taxonomy_common_2022 where property_hash = '-924497237983793299'
-- select property_name from core.ref.Taxonomy_common_2022 where (date_updated = '01/07/2021' or date_updated = '01/08/2021') and rps_flag = 0
/**
select property_name, digital_type_id, project_m_reach, common_supercategory, common_category, common_subcategory, common_vertical,
rps_flag,rpt_flag,vetted_flag
from ref.Taxonomy_Common_properties where 
property_name like '%7%eleven%' 
and project_country = 'US' order by project_m_reach desc;
**/
--select * from ref.Taxonomy_Common_properties where property_name = 'app'
-- select * from core.ref.Taxonomy_common_2022 where property_name = 'Me@Walmart';
select count(*) from core.ref.Taxonomy_common_2022 where step_assigned like 'c3%'

-- END OF FILE --
