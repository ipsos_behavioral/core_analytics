﻿---------------------------------------------------------------------------------------------------
-- C) IPSOS COMMON TAXONOMY: VETTED RECLASSIFY COMMON
---------------------------------------------------------------------------------------------------
-- Script by Jimmy
-- Modified by Shirley (2021/3/25)
---------------------------------------------------------------------------------------------------

use core;

-- C4) ONE OFF CHANGES TO PROPERTIES
---------------------------------------------------------------------------------------------------
-- C4. vetted reclassify common

-- C4. 510 EMAIL/CHAT
---------------------

-- C4. 520 ENTERTAINMENT/GAMES
------------------------------
-- Auto Assign Verticals (For properties that don't have a vertical)
update ref.Taxonomy_common_2022 set common_vertical='Books/Magazines',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Entertainment/Games' 
and common_category='e-Reading' and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical ='Media/Entertainment', 
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 
where common_supercategory = 'Entertainment/Games' and common_category 
in ('Entertainment', 'Movies', 'Music and Audio', 'TV and Video', 'Art/Museums', 'Comedy') and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Sports',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Entertainment/Games' 
and common_category='Sports' and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Games',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Entertainment/Games' 
and common_category='Games' and common_vertical = '';


-- C4. 530 NEWS/MEDIA/INFO
---------------------------
-- Auto Assign Verticals (For properties that don't have a vertical)
update ref.Taxonomy_common_2022 set common_vertical='Travel',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='News/Media/Info' 
and common_category in ('Maps and Navigation','Transportation') and common_vertical = '';


-- C4. 540 LIFESTYLE/LIVING
---------------------------
-- Auto Assign Verticals (For properties that don't have a vertical)
update ref.Taxonomy_common_2022 set common_vertical='Food/Groceries',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Lifestyle/Living' 
and common_category='Cooking' and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Health/Medicine',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Lifestyle/Living' 
and common_category='Health' and common_vertical = '';


-- C4. 550 SEARCH ENGINE
---------------------------


-- C4. 560 SHOPPING
---------------------------
-- Auto Assign Verticals (For properties that don't have a vertical)
update ref.Taxonomy_common_2022 set common_vertical='Food/Groceries',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Shopping' 
and common_category in ('Food Delivery','Restaurants') and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Food/Groceries',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Shopping' 
and common_subcategory in ('Grocery Store','Grocery Delivery') and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='General Retail',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Shopping' 
and common_category in ('General Retail') and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Automotive',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Shopping' 
and common_subcategory in ('Auto Parts','Car Dealership') and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Travel',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Shopping' 
and common_subcategory in ('Airlines','Hotels','Car Rental') and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Alcohol/Beverage',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Shopping' 
and common_subcategory in ('Vineyard/Winery','Distillery','Brewery') and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Health/Medicine',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Shopping' 
and common_category in ('Pharmacy') and common_vertical = '';

-- C4. 570 SOCIAL/PHOTOS
---------------------------


-- C4. 580 FINANCE/WORK/GOV
---------------------------
-- Auto Assign Verticals (For properties that don't have a vertical)
update ref.Taxonomy_common_2022 set common_vertical='Business',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Finance/Work/Gov' 
and common_category='Business Services' and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Education',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Finance/Work/Gov' 
and common_category='Education' and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Finance',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Finance/Work/Gov' 
and common_category = 'Finance' and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Government',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Finance/Work/Gov' 
and common_category = 'Government' and common_vertical = '';

update ref.Taxonomy_common_2022 set common_vertical='Business',
step_assigned= 'c4. vetted reclassify common' from ref.Taxonomy_common_2022 where common_supercategory='Finance/Work/Gov' 
and common_category = 'Law' and common_vertical = '';

-- C4. 590 UTILITY/OTHER
---------------------------

-- C4. 600 Alert
---------------------------



-- C4. 999 REMOVALS
--------------------------



-- QA 
select top 100 * from core.ref.Taxonomy_common_2022 where step_assigned like 'c4%';
select count(*) from core.ref.Taxonomy_common_2022 where step_assigned like 'c4%';

-- ref.Taxonomy_Common_Propertiesselect top 100 * from core.ref.Taxonomy_common_properties where property_name in ('uber', 'uber.com', 'lyft', 'lyft.com', 'Lyft - Taxi & Bus App Alternative')

-- END OF FILE --
