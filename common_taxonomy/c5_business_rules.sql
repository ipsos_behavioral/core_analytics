---------------------------------------------------------------------------------------------------
-- C) IPSOS COMMON TAXONOMY: AUTOMATED BUSINESS RULES
---------------------------------------------------------------------------------------------------
-- Script by Jimmy
-- Modified by Shirley (2021/3/25)
---------------------------------------------------------------------------------------------------

use core;

-----------------------------------------------------------------
-- C5. APPLY BUSINESS RULES
-----------------------------------------------------------------
-- C5. CATEGORY ASSIGNMENT BUSINESS RULES
update ref.Taxonomy_common_2022
set step_assigned='c5. business rules', common_supercategory = '[TBD]', common_category='[Alert: TBD]', common_subcategory='[Alert: TBD]'	
where common_supercategory = '[TBD]' and vetted_flag=0;

update ref.Taxonomy_common_2022
set step_assigned='c5. business rules', common_supercategory = '[TBD]'	
where common_category='[Alert: TBD]' and common_subcategory='[Alert: TBD]' and vetted_flag=0;

-- C5. SUBCATEGORY ASSIGNMENT BUSINESS RULES
update ref.Taxonomy_common_2022
set step_assigned='c5. business rules',common_category = '[Remove: Blocked]', common_subcategory='[Remove: Blocked]'
where common_supercategory = '[Remove]' and common_category in ('[Remove: Blocked]','[Remove: Duplicate]');


-- alert following properties so they are not reported
update ref.Taxonomy_common_2022
set step_assigned='c5. business rules', common_subcategory='[Alert: Discretion Adult]'
where common_category like ('%XXX%Adult%');

update ref.Taxonomy_common_2022
set step_assigned='c5. business rules',common_subcategory='[Alert: Business Services]'
where common_supercategory ='[Alert]' and common_category = '[Alert: Business Services]';
-- coupons
update ref.Taxonomy_common_2022
set step_assigned='c5. business rules',common_subcategory='[Alert: Discretion Coupons]'
where common_supercategory = 'Shopping' and common_category = 'Coupons';

update ref.Taxonomy_common_2022
set step_assigned='c5. business rules',common_subcategory='[Remove: Offline Utility]'
where common_supercategory = '[Remove]' and common_category = '[Remove: Offline Utility]';

update ref.Taxonomy_common_2022
set step_assigned='c5. business rules',common_subcategory='[Remove: Research/Marketing]'
where common_supercategory = '[Remove]' and common_category = '[Remove: Research/Marketing]';

--semantic matching 
-- research/marketing
update ref.Taxonomy_common_2022
set step_assigned='c5. business rules', common_supercategory = '[Alert]', common_category='[Alert: TBD Research/Marketing]', common_subcategory='[Alert: TBD Research/Marketing]'	
where vetted_flag=0 
and common_supercategory not like '%Remove%'
and (property_name like '%survey%' or property_name like '%opinion%' or property_name like '%panel%' or property_name like '%research%' 
or property_name like '%nielsen%' or property_name like '%ipsos%' or property_name like '%doubleclick%' or property_name like '%reward%'
or property_name like '%imrworldwide%')
;

-- VPN
update ref.Taxonomy_common_2022
set step_assigned='c5. business rules', common_supercategory = '[Alert]', common_category='[Alert: TBD Offline Utility]', common_subcategory='[Alert: TBD Offline Utility]'
where vetted_flag=0 
and common_supercategory not like '%Remove%'
and (property_name like '%VPN%')
;

-- XXXAdult
update ref.Taxonomy_common_2022
set step_assigned='c5. business rules', common_supercategory = 'Entertainment/Games', common_category='XXX Adult', common_subcategory='[Alert: Discretion Adult]'
where vetted_flag=0 
and common_supercategory not like '%Remove%'
and (property_name like '%XXX%' or property_name like '%porn%')
;
--select top 1000 * from ref.Taxonomy_common_2022 where vetted_flag=0 and (property_name like '%XXX%' or property_name like '%porn%')

-- delete row where property_name is empty
-- select top 100 * from core.ref.Taxonomy_Common_Properties where property_name = ''
-- DELETE FROM ref.Taxonomy_common_2022 WHERE property_name = '';


--update ref.Taxonomy_common_2022 set rps_flag=0, rpt_flag=0, step_assigned='c2. vendor autoclasssify common', common_supercategory = '[Remove]', common_category = '[Remove: Blocked]', common_subcategory = '[Remove: Blocked]', common_vertical = '' from ref.Taxonomy_common_2022 where vetted_flag=0 and common_supercategory='[Alert]' or common_subcategory = '[Alert: Discretion Coupons]' and sw_category = 'Blocked' and sw_subcategory = 'General';
--update ref.Taxonomy_common_2022 set rps_flag=0, rpt_flag=0, step_assigned='c2. vendor autoclasssify common', common_supercategory = '[Remove]', common_category = '[Remove: Research/Marketing]', common_subcategory = '[Remove: Research/Marketing]', common_vertical = '' from ref.Taxonomy_common_2022 where vetted_flag=0 and common_supercategory='[Alert]' or common_subcategory = '[Alert: Discretion Coupons]' and sw_category = 'Business and Consumer Services' and sw_subcategory = 'Marketing and Advertising';
--update ref.Taxonomy_common_2022 set rps_flag=0, rpt_flag=0, step_assigned='c2. vendor autoclasssify common', common_supercategory = '[Remove]', common_category = '[Remove: Research/Marketing]', common_subcategory = '[Remove: Research/Marketing]', common_vertical = '' from ref.Taxonomy_common_2022 where vetted_flag=0 and common_supercategory='[Alert]' or common_subcategory = '[Alert: Discretion Coupons]' and sw_category = 'Business and Industry' and sw_subcategory = 'Marketing and Advertising';
--update ref.Taxonomy_common_2022 set rps_flag=0, rpt_flag=0, step_assigned='c2. vendor autoclasssify common', common_supercategory = '[Remove]', common_category = '[Remove: Research/Marketing]', common_subcategory = '[Remove: Research/Marketing]', common_vertical = '' from ref.Taxonomy_common_2022 where vetted_flag=0 and common_supercategory='[Alert]' or common_subcategory = '[Alert: Discretion Coupons]' and sw_category = 'Other' and sw_subcategory = 'Market Research & Points Rewards';

--QA
/**
select * from ref.Taxonomy_common_2022
where 
vetted_flag=0
--vetted_flag=1
and rpt_flag = 1
and (property_name like '%survey%' or property_name like '%opinion%' or property_name like '%panel%' or property_name like '%research%' 
or property_name like '%nielsen%' or property_name like '%ipsos%')
order by project_m_reach desc
;

select * from ref.Taxonomy_common_2022
where 
vetted_flag=0
--vetted_flag=1
and (property_name like '%reward%')
order by project_m_reach desc
;
**/
-----------------------------------------------------------------
-- C5. APPLY REPRESENTITY AND REPORTABILITY LOGIC BASED ON SUPERCAT
-----------------------------------------------------------------
update ref.Taxonomy_common_2022 set rps_flag=1, rpt_flag=0 from ref.Taxonomy_common_2022 where common_supercategory like '%TBD%' and vetted_flag=0;
update ref.Taxonomy_common_2022 set rps_flag=0, rpt_flag=0 from ref.Taxonomy_common_2022 where common_subcategory like '%Remove%';
update ref.Taxonomy_common_2022 set rps_flag=1, rpt_flag=0 from ref.Taxonomy_common_2022 where common_supercategory like '%Alert%' 
or common_subcategory in ('[Alert: TBD Blocked New Sites]','[Alert: Discretion Adult]',
'[Alert: Discretion Sites]','[Alert: Discretion Guns/Weapons]');

update ref.Taxonomy_common_2022 set rps_flag=1, rpt_flag=0 from ref.Taxonomy_common_2022 where 
vetted_flag=0 and common_subcategory in ('[Alert: Discretion Coupons]');

select top 1000 * from core.ref.Taxonomy_common_2022 where step_assigned like 'c5%';
select count(*) from core.ref.Taxonomy_common_2022 where step_assigned like 'c5%';
-- select project_sample_source, count(*) from core.ref.Taxonomy_common_2022 where digital_type_id is null group by  project_sample_source;
-- END OF FILE --
