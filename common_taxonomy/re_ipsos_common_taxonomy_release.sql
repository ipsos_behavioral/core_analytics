---------------------------------------------------------------------------------------------------
-- IPSOS COMMON TAXONOMY RELEASE
---------------------------------------------------------------------------------------------------
-- Script by Jimmy
-- Modified by Shirley (2021/3/25)
---------------------------------------------------------------------------------------------------
-- One the production/update scripts pass QA, officially release the latest version of script.
use core;

begin try drop table ref.Taxonomy_Common_Properties end try begin catch end catch;

select a.*
into ref.Taxonomy_Common_Properties
from ref.Taxonomy_Common_2022 a;


-- select count(*) from ref.Taxonomy_Common_Properties; --1287258
-- select property_name, common_supercategory from core.ref.Taxonomy_Common_Properties where date_updated = '07/27/2021'

-- select * from ref.Taxonomy_Common_Properties where property_name = 'bulbagarden.net';
-- END OF FILE --
