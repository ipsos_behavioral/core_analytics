-------------
--General Retailer Load
--Use for pattern matching in search
------------


use Core;


-----------------------General Retailer
begin try drop table #import end try begin catch end catch;
create table #import(
	run_on_white int,
	run_on_green int,
	pattern			nvarchar(300),
	semantic_lemma	nvarchar(300),
	semantic_priority_id	int,
	semantic_priority_group	nvarchar(300)
);
insert into #import select 1,1,'%costco%','Costco',9001,'Club Store';
insert into #import select 1,1,'%samsclub%','Sams Club',9001,'Club Store';
insert into #import select 1,1,'%sams club%','Sams Club',9001,'Club Store';
insert into #import select 1,1,'%sam s club%','Sams Club',9001,'Club Store';
insert into #import select 1,1,'%dollartree%','Dollar Tree',9002,'Dollar Store';
insert into #import select 1,1,'%dollar tree%','Dollar Tree',9002,'Dollar Store';
insert into #import select 1,1,'%familydollar%','Family Dollar',9002,'Dollar Store';
insert into #import select 1,1,'%family dollar%','Family Dollar',9002,'Dollar Store';
insert into #import select 1,1,'%dollarGeneral%','Dollar General',9002,'Dollar Store';
insert into #import select 1,1,'%dollar General%','Dollar General',9002,'Dollar Store';
insert into #import select 1,1,'%Walgreens%','Walgreens',9003,'Drug Store';
insert into #import select 1,1,'%CVS%','CVS',9003,'Drug Store';
insert into #import select 1,1,'%riteaid%','Rite Aid',9003,'Drug Store';
insert into #import select 1,1,'%rite aid%','Rite Aid',9003,'Drug Store';
insert into #import select 1,1,'%market basket%','Market Basket',9006,'Grocery Store';
insert into #import select 1,1,'%kroger%','Kroger',9006,'Grocery Store';
insert into #import select 1,1,'%albertson%','Albertsons',9006,'Grocery Store';
insert into #import select 1,1,'%Meijer%','Meijer',9006,'Grocery Store';
insert into #import select 1,1,'%Publix%','Publix',9006,'Grocery Store';
insert into #import select 1,1,'%Safeway%','Safeway',9006,'Grocery Store';
insert into #import select 1,1,'%Cub Foods%','Cub Foods',9006,'Grocery Store';
insert into #import select 1,1,'%Dillons%','Dillons',9006,'Grocery Store';
insert into #import select 1,1,'%Food Lion%','Food Lion',9006,'Grocery Store';
insert into #import select 1,1,'%Food 4 Less%','Food 4 Less',9006,'Grocery Store';
insert into #import select 1,1,'%Frys Food Stores%','Frys Food Stores',9006,'Grocery Store';
insert into #import select 1,1,'%Fry s Food Stores%','Frys Food Stores',9006,'Grocery Store';
insert into #import select 1,1,'%Fred Meyer%','Fred Meyer',9006,'Grocery Store';
insert into #import select 1,1,'%Hannaford%','Hannaford',9006,'Grocery Store';
insert into #import select 1,1,'%H E B%','H-E-B',9006,'Grocery Store';
insert into #import select 1,1,'% HEB %','H-E-B',9006,'Grocery Store';
insert into #import select 1,1,'%King Soopers%','King Soopers',9006,'Grocery Store';
insert into #import select 1,1,'%Ralphs%','Ralphs',9006,'Grocery Store';
insert into #import select 1,1,'% QFC %','QFC (Quality Food Centers)',9006,'Grocery Store';
insert into #import select 1,1,'%Quality Food Centers%','QFC (Quality Food Centers)',9006,'Grocery Store';
insert into #import select 1,1,'%Wegman%','Wegmans',9006,'Grocery Store';
insert into #import select 1,1,'%Schnuck%','Schnucks',9006,'Grocery Store';
insert into #import select 1,1,'%Jewel Osco%','Jewel Osco',9006,'Grocery Store';
insert into #import select 1,1,'%Harris Teeter%','Harris Teeter',9006,'Grocery Store';
insert into #import select 1,1,'%ShopRite%','ShopRite',9006,'Grocery Store';
insert into #import select 1,1,'%PriceRite%','PriceRite',9006,'Grocery Store';
insert into #import select 1,1,'% Ingles %','Ingles',9006,'Grocery Store';
insert into #import select 1,1,'%Winn Dixie%','Winn Dixie',9006,'Grocery Store';
insert into #import select 1,1,'%Hy Vee%','Hy-Vee',9006,'Grocery Store';
insert into #import select 1,1,'%Stop   Shop%','Stop & Shop',9006,'Grocery Store';
insert into #import select 1,1,'%Shop n Save%','Shop n Save',9006,'Grocery Store';
insert into #import select 1,1,'%Whole Foods%','Whole Foods',9006,'Grocery Store';
insert into #import select 1,1,'%Trader Joe%','Trader Joes',9006,'Grocery Store';
insert into #import select 1,1,'%Aldi%','Aldi',9006,'Grocery Store';
insert into #import select 1,1,'%ebay%','Ebay',9007,'Online Retailer';
insert into #import select 1,1,'%etsy%','Etsy',9007,'Online Retailer';
insert into #import select 1,1,'%Gopuff%','GoPuff',9007,'Online Retailer';
insert into #import select 1,1,'%Marshalls%','Marshalls',9008,'Mass Merchandiser';
insert into #import select 1,1,'%target%','Target',9008,'Mass Merchandiser';
insert into #import select 1,1,'%walmart%','Walmart',9008,'Mass Merchandiser';
insert into #import select 1,1,'%Kohl%','Kohls',9008,'Mass Merchandiser';
insert into #import select 1,1,'%Bed%Bath%Beyond%','Bed Bath and Beyond',9008,'Mass Merchandiser';
insert into #import select 1,1,'%Bloomingdale%','Bloomingdales',9008,'Mass Merchandiser';
insert into #import select 1,1,'%Nordstrom%','Nordstrom',9008,'Mass Merchandiser';
insert into #import select 1,1,'%five below%','Five Below',9009,'General Retailer';
insert into #import select 1,1,'%BigLots%','Big Lots',9009,'General Retailer';
insert into #import select 1,1,'%Big Lots%','Big Lots',9009,'General Retailer';
insert into #import select 1,1,'%JCPenney%','JCPenney',9009,'General Retailer';
insert into #import select 1,1,'%JCPenny%','JCPenney',9009,'General Retailer';
insert into #import select 1,1,'%JC Penney%','JCPenney',9009,'General Retailer';
insert into #import select 1,1,'%JC Penny%','JCPenney',9009,'General Retailer';
insert into #import select 1,1,'%J C Penney%','JCPenney',9009,'General Retailer';
insert into #import select 1,1,'%J C Penny%','JCPenney',9009,'General Retailer';
insert into #import select 1,1,'%J C  Penney%','JCPenney',9009,'General Retailer';
insert into #import select 1,1,'%J C  Penny%','JCPenney',9009,'General Retailer';
insert into #import select 1,1,'%morningsave%','Morningsave',9009,'General Retailer';
insert into #import select 1,1,'%sears%','Sears',9009,'General Retailer';
insert into #import select 1,1,'%amazon%','Amazon',9999,'Amazon';






---General Retailer
begin try drop table ref.general_retailer_pattern end try begin catch end catch;
create table ref.general_retailer_pattern (
	pattern_id bigint identity,
	run_on_white int,
	run_on_green int,
	pattern			nvarchar(300),
	semantic_lemma	nvarchar(300),
	semantic_priority_id	int,
	semantic_priority_group	nvarchar(300)
);
-- First 4 digits of pattern_id is the semantic_priority_id. Last 7 digits is hash of pattern
SET IDENTITY_INSERT ref.general_retailer_pattern ON;

insert into ref.general_retailer_pattern (
	pattern_id, 	
	run_on_white,	
	run_on_green,	
	pattern	,		
	semantic_lemma,
	semantic_priority_id,
	semantic_priority_group
)
	select convert(bigint,semantic_priority_id)*20000000+abs(convert(bigint,hashbytes('SHA1',pattern)))%20000000 as pattern_id,	-----------note multiplied by 20000000 instead of 10000000 so always higher than the project specific retailers
	run_on_white,	
	run_on_green,	
	pattern	,		
	semantic_lemma,
	semantic_priority_id,
	semantic_priority_group
from #import
;
--(31 rows affected)

create index general_retailer_pattern_table1 on ref.general_retailer_pattern (pattern_id);
create index general_retailer_pattern_table2 on ref.general_retailer_pattern (semantic_lemma);
create index general_retailer_pattern_table4 on ref.general_retailer_pattern (pattern);
create index general_retailer_pattern_table5 on ref.general_retailer_pattern (semantic_priority_id);

