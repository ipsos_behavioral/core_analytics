----------------------------------------------------------------------------------------------------
-- This script performs inventory counts/measurements of existing data we have from priord projects.
-- The intent is to leverage this historic data as resource for feasiblity checks, and potential
-- follow-on project work, so long as they are compliant by the MSAs of the corresponding clients.
-- Jimmy Kong (2019-06-10)
----------------------------------------------------------------------------------------------------


create table #project_data_inventory (
	project_sample_source varchar(300),
	project_sample_source_name varchar(300),
	project_sample_database varchar(300),
	project_sample_size int,
	project_sample_pc int,
	project_sample_mobile int,
	project_sample_digital_and_survey int,
	project_count_installed int,
	project_start_date date,
	project_end_date date,
	project_weeks_tracked int
	);


-- 2017 SAMSUNG
----------------------------------------------------------------------------------------------------
-- select top 100 * from [Samsung].[dbo].UserByDay_AppMobile;
-- drop table #userbyday_samsung;
select userid as panelist_id, dayid as date_id, weekid,
	1 as platform_id, -- mobile
	100 as digital_type_id -- app
into #userbyday_samsung
from [Samsung].[dbo].UserByDay_AppMobile
group by userid, dayid, weekid;

insert into #userbyday_samsung
select userid as panelist_id, dayid as date_id, weekid,
	1 as platform_id, -- mobile
	235 as digital_type_id -- web
from [Samsung].[dbo].UserByDay_WebMobile
group by userid, dayid, weekid;

insert into #userbyday_samsung
select userid as panelist_id, dayid as date_id, weekid,
	2 as platform_id, -- pc
	235 as digital_type_id -- web
from [Samsung].[dbo].UserByDay_WebPC
group by userid, dayid, weekid;

insert into #project_data_inventory
select '2017 SamsungTV ' as project_sample_source,
	'2017 Samsung - Televisions' as project_sample_source_name,
	'[Samsung]' as project_sample_database,
	count(distinct panelist_id) as project_sample_size,
	null as project_sample_pc,
	null as project_sample_mobile,
	null as project_sample_digital_and_survey,
	null as project_count_installed,
	min(a.date_id) as project_start_date,
	max(a.date_id) as project_end_date,
	count(distinct a.WeekID) as project_weeks_tracked
from #userbyday_samsung a;

update #project_data_inventory
set project_sample_pc = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_id) as ct_sample from #userbyday_samsung where platform_id=2) b
	on project_sample_database='[Samsung]';

update #project_data_inventory
set project_sample_mobile = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_id) as ct_sample from #userbyday_samsung where platform_id=1) b
	on project_sample_database='[Samsung]';

update #project_data_inventory
set project_sample_digital_and_survey = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct uuid) as ct_sample from [Samsung].[dbo].[Screener_Pulse_Followup]) b
	on project_sample_database='[Samsung]';

update #project_data_inventory
set project_count_installed = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct Panelist_ID) as ct_sample from [Samsung].[dbo].Panelist_Device_Diagnostic) b
	on project_sample_database='[Samsung]';


-- 2017 YOUTUBE
----------------------------------------------------------------------------------------------------
-- select top 100 * from [Samsung].[dbo].UserByDay_AppMobile;
-- drop table #userbyday_youtube;
select userid as panelist_id, dayid as date_id, weekid,
	1 as platform_id, -- mobile
	100 as digital_type_id -- app
into #userbyday_youtube
from [YouTubeShopping].[dbo].UserByDay_App_Mobile
group by userid, dayid, weekid;

insert into #userbyday_youtube
select userid as panelist_id, dayid as date_id, weekid,
	1 as platform_id, -- mobile
	235 as digital_type_id -- web
from [YouTubeShopping].[dbo].UserByDay_Web_Mobile
group by userid, dayid, weekid;

insert into #userbyday_youtube
select userid as panelist_id, dayid as date_id, weekid,
	2 as platform_id, -- pc
	235 as digital_type_id -- web
from [YouTubeShopping].[dbo].UserByDay_Web_PC
group by userid, dayid, weekid;

insert into #project_data_inventory
select '2017 GYTS ' as project_sample_source,
	'2017 Google - YouTube Shopping' as project_sample_source_name,
	'[YouTubeShopping]' as project_sample_database,
	count(distinct panelist_id) as project_sample_size,
	null as project_sample_pc,
	null as project_sample_mobile,
	null as project_sample_digital_and_survey,
	null as project_count_installed,
	min(a.date_id) as project_start_date,
	max(a.date_id) as project_end_date,
	count(distinct a.WeekID) as project_weeks_tracked
from #userbyday_youtube a;

update #project_data_inventory
set project_sample_pc = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_id) as ct_sample from #userbyday_youtube where platform_id=2) b
	on project_sample_database='[YouTubeShopping]';

update #project_data_inventory
set project_sample_mobile = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_id) as ct_sample from #userbyday_youtube where platform_id=1) b
	on project_sample_database='[YouTubeShopping]';

update #project_data_inventory
set project_sample_digital_and_survey = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct [Respondent_Serial]) as ct_sample from YouTubeShopping.[dbo].[ShopAllSurveyData]) b
	on project_sample_database='[YouTubeShopping]';

update #project_data_inventory
set project_count_installed = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct Panelist_Key) as ct_sample from YouTubeShopping.ref.Panelist) b
	on project_sample_database='[YouTubeShopping]';


-- 2017 GOOGLE GAMING
----------------------------------------------------------------------------------------------------
-- drop table #userbyday_googlegaming;
select panelist_hash as panelist_id, day_id as date_id, week_id, platform_id, digital_type_id -- platform_id: 10=mobile, 20=app	
into #userbyday_googlegaming
from [GoogleGaming].[dbo].UserByDay_Total
group by panelist_hash, day_id, week_id, platform_id, digital_type_id;

insert into #project_data_inventory
select '2017 GoogleGaming ' as project_sample_source,
	'2017 Google - Gaming' as project_sample_source_name,
	'[GoogleGaming]' as project_sample_database,
	count(distinct panelist_id) as project_sample_size,
	null as project_sample_pc,
	null as project_sample_mobile,
	null as project_sample_digital_and_survey,
	null as project_count_installed,
	min(a.date_id) as project_start_date,
	max(a.date_id) as project_end_date,
	count(distinct a.week_id) as project_weeks_tracked
from #userbyday_googlegaming a;

update #project_data_inventory
set project_sample_pc = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_id) as ct_sample from #userbyday_googlegaming where platform_id=20) b
	on project_sample_database='[GoogleGaming]';

update #project_data_inventory
set project_sample_mobile = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_id) as ct_sample from #userbyday_googlegaming where platform_id=10) b
	on project_sample_database='[GoogleGaming]';

update #project_data_inventory
set project_sample_digital_and_survey = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct id) as ct_sample from GoogleGaming.[dbo].[Survey_Checkin]) b
	on project_sample_database='[GoogleGaming]';

update #project_data_inventory
set project_count_installed = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct Panelist_Key) as ct_sample from GoogleGaming.Reference.Panelist) b
	on project_sample_database='[GoogleGaming]';


-- 2017 MATTEL
----------------------------------------------------------------------------------------------------
-- drop table #userbyday_mattel;
select panelist_key, day_id as date_id, week_id, platform_id, digital_type_id -- platform_id: 10=mobile, 20=app	
into #userbyday_mattel
from [Mattel].Process.UserByDay_Total
group by panelist_key, day_id, week_id, platform_id, digital_type_id;

insert into #project_data_inventory
select '2017 Mattel ' as project_sample_source,
	'2017 Mattel' as project_sample_source_name,
	'[Mattel]' as project_sample_database,
	count(distinct panelist_key) as project_sample_size,
	null as project_sample_pc,
	null as project_sample_mobile,
	null as project_sample_digital_and_survey,
	null as project_count_installed,
	min(a.date_id) as project_start_date,
	max(a.date_id) as project_end_date,
	count(distinct a.week_id) as project_weeks_tracked
from #userbyday_mattel a;

update #project_data_inventory
set project_sample_pc = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from #userbyday_mattel where platform_id=20) b
	on project_sample_database='[Mattel]';

update #project_data_inventory
set project_sample_mobile = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from #userbyday_mattel where platform_id=10) b
	on project_sample_database='[Mattel]';

update #project_data_inventory
set project_count_installed = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from Mattel.Ref.Panelist) b
	on project_sample_database='[Mattel]';

update #project_data_inventory
set project_sample_digital_and_survey = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct [Respondent_Serial]) as ct_sample from (select [Respondent_Serial] from Mattel.[dbo].Survey_Popup1 union select [Respondent_Serial] from Mattel.[dbo].Survey_Popup2) c ) b
	on project_sample_database='[Mattel]';


-- 2017 GOOGLE TICKETING
----------------------------------------------------------------------------------------------------
-- drop table #userbyday_googleticketing;
select panelist_key, day_id as date_id, week_id, platform_id, digital_type_id
into #userbyday_googleticketing
from [GoogleTicketing].Process.UserByDay_Total
group by panelist_key, day_id, week_id, platform_id, digital_type_id;

insert into #project_data_inventory
select '2017 GoogleTicketing ' as project_sample_source,
	'2017 Google - Ticketing' as project_sample_source_name,
	'[GoogleTicketing]' as project_sample_database,
	count(distinct panelist_key) as project_sample_size,
	null as project_sample_pc,
	null as project_sample_mobile,
	null as project_sample_digital_and_survey,
	null as project_count_installed,
	min(a.date_id) as project_start_date,
	max(a.date_id) as project_end_date,
	count(distinct a.week_id) as project_weeks_tracked
from #userbyday_googleticketing a;

update #project_data_inventory
set project_sample_pc = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from #userbyday_googleticketing where platform_id=20) b
	on project_sample_database='[GoogleTicketing]';

update #project_data_inventory
set project_sample_mobile = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from #userbyday_googleticketing where platform_id=10) b
	on project_sample_database='[GoogleTicketing]';

update #project_data_inventory
set project_count_installed = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from GoogleTicketing.Ref.Panelist) b
	on project_sample_database='[GoogleTicketing]';

update #project_data_inventory
set project_sample_digital_and_survey = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct [Respondent_ID]) as ct_sample from (select [Respondent_ID] from GoogleTicketing.[dbo].Trigger_Survey union select [Respondent_ID] from GoogleTicketing.[dbo].Final_Survey) c ) b
	on project_sample_database='[GoogleTicketing]';


-- 2018 L'OREAL CONSUMER DIGITAL JOURNEY
----------------------------------------------------------------------------------------------------
-- drop table #userbyday_loreal;
select panelist_key, day_id as date_id, week_id, platform_id, digital_type_id
into #userbyday_loreal2018
from [Loreal2018].Run.UserByDay_Total
group by panelist_key, day_id, week_id, platform_id, digital_type_id;

insert into #project_data_inventory
select '2018 LorealCDG ' as project_sample_source,
	'2018 Loreal - Consumer Digital Journey' as project_sample_source_name,
	'[Loreal2018]' as project_sample_database,
	count(distinct panelist_key) as project_sample_size,
	null as project_sample_pc,
	null as project_sample_mobile,
	null as project_sample_digital_and_survey,
	null as project_count_installed,
	min(a.date_id) as project_start_date,
	max(a.date_id) as project_end_date,
	count(distinct a.week_id) as project_weeks_tracked
from #userbyday_loreal2018 a;

update #project_data_inventory
set project_sample_pc = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from #userbyday_loreal2018 where platform_id=2) b
	on project_sample_database='[Loreal2018]';

update #project_data_inventory
set project_sample_mobile = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from #userbyday_loreal2018 where platform_id=1) b
	on project_sample_database='[Loreal2018]';

update #project_data_inventory
set project_count_installed = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from Loreal2018.Ref.Panelist) b
	on project_sample_database='[Loreal2018]';

update #project_data_inventory
set project_sample_digital_and_survey = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct Respondent_ID) as ct_sample from Loreal2018.dbo.Survey_Consolidated c ) b
	on project_sample_database='[Loreal2018]';


-- 2018 GOOGLE AUTO
----------------------------------------------------------------------------------------------------
-- drop table #userbyday_googleauto;
select panelist_key, day_id as date_id, week_id, platform_id, digital_type_id
into #userbyday_googleauto
from GoogleAuto.Run.UserByDay_Total
group by panelist_key, day_id, week_id, platform_id, digital_type_id;

insert into #project_data_inventory
select '2018 GoogleAuto' as project_sample_source,
	'2018 Google - Auto' as project_sample_source_name,
	'[GoogleAuto]' as project_sample_database,
	count(distinct panelist_key) as project_sample_size,
	null as project_sample_pc,
	null as project_sample_mobile,
	null as project_sample_digital_and_survey,
	null as project_count_installed,
	min(a.date_id) as project_start_date,
	max(a.date_id) as project_end_date,
	count(distinct a.week_id) as project_weeks_tracked
from #userbyday_googleauto a;

update #project_data_inventory
set project_sample_pc = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from #userbyday_loreal2018 where platform_id=2) b
	on project_sample_database='[GoogleAuto]';

update #project_data_inventory
set project_sample_mobile = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from #userbyday_loreal2018 where platform_id=1) b
	on project_sample_database='[Loreal2018]';

update #project_data_inventory
set project_count_installed = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct panelist_key) as ct_sample from Loreal2018.Ref.Panelist) b
	on project_sample_database='[Loreal2018]';

update #project_data_inventory
set project_sample_digital_and_survey = b.ct_sample
from #project_data_inventory a
inner join (select count(distinct Respondent_ID) as ct_sample from Loreal2018.dbo.Survey_Consolidated c ) b
	on project_sample_database='[Loreal2018]';





-- TEST QUERY
select * from #project_data_inventory order by project_start_date, project_end_date;




create table #project_data_inventory (
	project_sample_source varchar(300),
	project_sample_source_name varchar(300),
	project_sample_database varchar(300),
	project_sample_size int,
	project_sample_pc int,
	project_sample_mobile int,
	project_sample_digital_and_survey int,
	project_count_installed int,
	project_start_date date,
	project_end_date date,
	project_weeks_tracked int
	);

/***
	project_sample_source	project_sample_size	common_category_source
	1	2017 Google Ticketing
	1	2018 07 Update
	1	2018 Similar Web
2018 3PCS 05	255585	
2018 3PCS 05	255585	2017 Google Ticketing
2018 3PCS 05	255585	2018 07 Update
2018 3PCS 05	255585	2018 Similar Web
2018 GYTS	1	
 ***/

