/****** Script for SelectTopNRows command from SSMS  ******/

-- select * from #variable_names;
-- drop table #variable_names;
create table #variable_names (
	variable_name_key int,
	var_id int,
	var_name varchar(300),
	val_id int,
	val_name varchar(300));

insert into #variable_names select 1,	1,	'platform_id',	1,	'Mobile';
insert into #variable_names select 2,	1,	'platform_id',	2,	'PC';

insert into #variable_names select 3,	2,	'device_type_id',	10,	'Mobile';
insert into #variable_names select 4,	2,	'device_type_id',	11,	'Smartphone';
insert into #variable_names select 5,	2,	'device_type_id',	12,	'Tablet';
insert into #variable_names select 6,	2,	'device_type_id',	20,	'PC';
insert into #variable_names select 7,	2,	'device_type_id',	21,	'Desktop';
insert into #variable_names select 8,	2,	'device_type_id',	22,	'Laptop';

insert into #variable_names select 9,	3,	'os_id',	163,	'Android';
insert into #variable_names select 10,	3,	'os_id',	892,	'iOS';
insert into #variable_names select 11,	3,	'os_id',	286,	'Windows';
insert into #variable_names select 12,	3,	'os_id',	364,	'Chrome OS';
insert into #variable_names select 13,	3,	'os_id',	713,	'Mac OS';
insert into #variable_names select 14,	3,	'os_id',	986,	'Linux';

insert into #variable_names select 15,	4,	'digital_type_id',	100,	'App';
insert into #variable_names select 16,	4,	'digital_type_id',	235,	'Web';
insert into #variable_names select 17,	4,	'digital_type_id',	483,	'Vid';
insert into #variable_names select 18,	4,	'digital_type_id',	539,	'Geo';

insert into #variable_names select 19,	5,	'source_id',	10000,	'App: Package Name';
insert into #variable_names select 20,	5,	'source_id',	10020,	'App: Search';
insert into #variable_names select 21,	5,	'source_id',	10028,	'App: Search Inferred';
insert into #variable_names select 22,	5,	'source_id',	10040,	'App: Video';
insert into #variable_names select 23,	5,	'source_id',	23520,	'Web: Search';
insert into #variable_names select 24,	5,	'source_id',	23528,	'Web: Search Inferred';
insert into #variable_names select 25,	5,	'source_id',	23530,	'Web: Domain';
insert into #variable_names select 26,	5,	'source_id',	23540,	'Web: Video';
insert into #variable_names select 27,	5,	'source_id',	23560,	'Web: Host';
insert into #variable_names select 28,	5,	'source_id',	23570,	'Web: URL';
insert into #variable_names select 29,	5,	'source_id',	48310,	'Vid: ACR';
insert into #variable_names select 30,	5,	'source_id',	53950,	'Geo: GPS';

insert into #variable_names select 32,	6,	'reportlevel',	0,	'Property';
insert into #variable_names select 33,	6,	'reportlevel',	1,	'Subcategory';
insert into #variable_names select 34,	6,	'reportlevel',	2,	'Category';
insert into #variable_names select 35,	6,	'reportlevel',	3,	'Total';

insert into #variable_names select 36,	7,	'level_id',	0,	'[No Aggregation]';
insert into #variable_names select 37,	7,	'level_id',	1,	'Total';
insert into #variable_names select 38,	7,	'level_id',	2,	'Category';
insert into #variable_names select 39,	7,	'level_id',	3,	'Subcategory';
insert into #variable_names select 39,	7,	'level_id',	9,	'Property';

-- UPDATE

drop table core.ref.variable_names;

select a.*
into core.ref.variable_names
from #variable_names a;

create index indx1 on core.ref.variable_names(variable_name_key);
create index indx2 on core.ref.variable_names(var_id);
create index indx3 on core.ref.variable_names(val_id);

/***
-- couldn't figure this out. TBD, check with Stephen to do the right way.
-- truncate table core.ref.variable_names;
SET IDENTITY_INSERT core.ref.variable_names ON;

insert into core.ref.variable_names (--variable_name_key,
	var_name, val_id, val_name)
select --row_number() over (partition by var_id order by val_id desc) ,
	a.var_name, a.val_id, a.val_name
from #variable_names a;
***/

select * from core.ref.variable_names;

