---------------------------------------------------------------------------------------------------
-- Online Journey (Just a small town girl. Livin' in a lonely world)
-- Jimmy
-- 2018/07/17
---------------------------------------------------------------------------------------------------
-- GOOGLE X PLATFORM USED FOR DEMO
-- MULTIPLE CONTENT (GENERAL, AUTO, ELECTRONICS, TRAVEL)
---------------------------------------------------------------------------------------------------

-- 0) DEFINE GLOBAL VARIABLE
---------------------------------------------------------------------------------------------------
use YouTubeShopping;

declare @sql varchar(3000);
-- declare @table_survey1	varchar(300) = 'VirginiaTourism.dbo.Survey_Popup1';
-- declare @table_survey2	varchar(300) = 'VirginiaTourism.dbo.Survey_Popup2';
-- declare @table_panelist	varchar(300) = 'VirginiaTourism.ref.Panelist';
-- declare @survey_var_buy	varchar(300) = 'q11b'; -- data column for purchase info

--select top 100 * from gxp.SequenceSesssionGXP_jk;
--select top 100 * from gxp.TaxonomyEcosystemGXP;

-- 0) CREATE PANELIST SEGMENT BASED ON IF THEY BELONG TO GIVEN RETAIL JOURNEY
-----------------------------------------------------------------------------
-- select top 100 * from gxp.Panelist_Retail_Segments;
-- drop table gxp.Panelist_Retail_Segments;
begin try drop table gxp.Panelist_Retail_Segments end try begin catch end catch;
select left(panelist_id,8) as panelist_key, panelist_id, b.retail_cat_id, b.retail_category, count(*) as ct_visits, count(distinct a.property) as ct_properties, sum(a.session_minutes) as raw_minutes
into gxp.Panelist_Retail_Segments
from gxp.SequenceSesssionGXP_jk a
inner join gxp.TaxonomyEcosystemGXP b
	on a.session_first_taxid_gxp=b.taxid_whitelist
group by left(panelist_id,8), panelist_id, b.retail_cat_id, b.retail_category;
-- (6643 row(s) affected)


-- 1) CUTSOMIZED PER PROJECT. SELF STATED PURCHASE EVENT (FROM POP UP SURVEY)
-- NOTE: THIS DOES NOT APPLY TO GOOGLE X PLATFORM
---------------------------------------------------------------------------------------------------

-- select * from #survey_purchase_date_prep1;
/***
begin try drop table #survey_purchase_date_prep1 end try begin catch end catch;
select respondent_id,
	1 as survey_type_id, -- Pop Up Survey 1 
	convert(datetime, Q5, 103) as purchase_date, 
	DataCollection_StartTime as survey_datetime,
	case 
		when Q6_NEW = 1						then 'Buy: Fisher Price'
		else case when Q6_NEW = 2			then 'Buy: Barbie'
		else case when Q6_NEW = 3			then 'Buy: Hot Wheels'
		else 'Buy: None of the above' 
		 end end end end end end end end end as brand_purchase,
	case when GRID_Q7_NEW_1_Q7 = '3' then 'Buy: Toys R Us'
		else case when GRID_Q7_NEW_1_Q7 = '9' then 'Buy: Smyths'
		else case when GRID_Q7_NEW_1_Q7 = '10' then 'Buy: Argos'
		else case when GRID_Q7_NEW_1_Q7 = '25' then 'Buy: myToys'
		else case when GRID_Q7_NEW_2_Q7 is not null then 'Buy: Offline'
		else 'Buy: Other Place'
			end end end end end end end end end end as purchase_location,
		case when GRID_Q7_NEW_2_Q7 <> '' AND GRID_Q7_NEW_1_Q7 = '' then 'Buy: Offline'
		else case when GRID_Q7_NEW_1_Q7 <> '' then 'Buy: Online'
		end end as purchase_onoffline
into #survey_purchase_date_prep1
from Mattel.dbo.Survey_Popup1;

----------------------
--POP UP SURVEY 2
----------------------
insert into #survey_purchase_date_prep1
select respondent_id,
	2 as survey_type_id, -- Pop Up Survey 2
	convert(datetime, Q5, 103) as purchase_date, 
	DataCollection_StartTime as survey_datetime,
	case 
		when Q6_NEW = 1						then 'Buy: Fisher Price'
		else case when Q6_NEW = 2			then 'Buy: Barbie'
		else case when Q6_NEW = 3			then 'Buy: Hot Wheels'
		else 'Buy: None of the above' 
			end end end end end end end end end as brand_purchase, 
case when GRID_Q7_NEW_1_Q7 = '3' then 'Buy: Toys R Us'
		else case when GRID_Q7_NEW_1_Q7 = '9' then 'Buy: Smyths'
		else case when GRID_Q7_NEW_1_Q7 = '10' then 'Buy: Argos'
		else case when GRID_Q7_NEW_1_Q7 = '25' then 'Buy: myToys'
		else case when GRID_Q7_NEW_2_Q7 is not null then 'Buy: Offline'
		else 'Buy: Other Place'
			end end end end end end end end end end as purchase_location,
		case when GRID_Q7_NEW_2_Q7 <> '' AND GRID_Q7_NEW_1_Q7 = '' then 'Buy: Offline'
		else case when GRID_Q7_NEW_1_Q7 <> '' then 'Buy: Online'
		end end as purchase_onoffline
from Mattel.dbo.Survey_Popup2;

-- select * from #survey_purchase_date_prep5;
begin try drop table #survey_purchase_date_prep5 end try begin catch end catch;
select 
	s.respondent_id,
	p.panelist_key,
	s.survey_type_id,
	s.purchase_location,
	--s.purchase_detail,
	s.purchase_onoffline,
	purchase_date,
	cast(survey_datetime as date) as purchase_date_alternative
into #survey_purchase_date_prep5
from #survey_purchase_date_prep1 s
left outer join ref.panelist p
	on p.Vendor_Panelist_ID=s.Respondent_ID;
-- (151 row(s) affected)

-- ################################################################################################
-- PERMANENT TABLE CREATED HERE
-- ################################################################################################

-- select * from dbo.Survey_Respondent_Purchase_Dates order by panelist_key, purchase_date; 
	begin try drop table dbo.Survey_Respondent_Purchase_Dates end try begin catch end catch;
	select *
	into dbo.Survey_Respondent_Purchase_Dates
	from  #survey_purchase_date_prep5;

	create index Survey_Respondent_Purchase_Dates1 on dbo.Survey_Respondent_Purchase_Dates(panelist_key);
	create index Survey_Respondent_Purchase_Dates2 on dbo.Survey_Respondent_Purchase_Dates(respondent_id);
***/


-- 2) CREATE LOOKUPS 
---------------------------------------------------------------------------------------------------

/***
-- select * from #platform_lookup;
select val_id as platform_id, val_name as platform_name
into #platform_lookup
from ref.variable_names a
where var_name='device_type_id' and val_id in (10,20); --- device_id used as plaform ID
insert into #platform_lookup select 0, 'All Platforms';

create index platform_lookup1 on #platform_lookup (platform_id);

insert into #platform_lookup select 90, 'Other';

-- select * from #digital_type_lookup;
select val_id as digital_type_id, val_name as digital_type
into #digital_type_lookup
from ref.variable_names a
where var_name='digital_type_id';
insert into #digital_type_lookup select 0, 'All Digital';

create index digital_type_lookup1 on #digital_type_lookup (digital_type_id);

insert into #digital_type_lookup select 999, '...';
***/

-- DEFINE CLUSTERS HERE
------------------------
-- Check size per subcat to identify best clustering
-- select * from #tmp_ecosystem_subcat_sizing order by cat_id, subcat_id;
-- select * from #tmp_ecosystem_subcat_sizing order by retail_category, eco_cat1_id, eco_category1;
-- drop table #tmp_ecosystem_subcat_sizing;
select retail_cat_id, retail_category, a.eco_cat1_id, a.eco_category1,
	eco_cat1_id as cluster_id,
	eco_category1 as cluster,
	count(distinct b.panelist_id) as ct_panelists,
	count(distinct b.session_hash) as ct_sessions, sum(session_minutes) as raw_minutes
into #tmp_ecosystem_subcat_sizing
from gxp.TaxonomyEcosystemGXP a
inner join gxp.SequenceSesssionGXP_jk b
	on a.taxid_whitelist=b.session_first_taxid_gxp
group by retail_cat_id, retail_category, a.eco_cat1_id, a.eco_category1;

-- UPDATE ECOSYSTEM WITH CHOSEN CLUSTER HERE
/** 
update #taxonomy_ecosystem_patched set cat_id=2500, category='Reviews/Info', subcat_id=3501, subcategory='Travel Review: TripAdvisor'
from #taxonomy_ecosystem_patched where taxonomy_ecosystem_key in (1, 57, 87, 147, 171);

update #taxonomy_ecosystem_patched set cluster_id=101, cluster ='Accom Luxury' from #taxonomy_ecosystem_patched where subcat_id=3101;
update #taxonomy_ecosystem_patched set cluster_id=102, cluster ='Accom Other' from #taxonomy_ecosystem_patched where cat_id=2100 and subcat_id<>3101;
***/

-- CHECK ASSIGNED CLUSTERS HERE
-------------------------------
-- select * from #tmp_ecosystem_cluster_sizing
/***
select cluster_id, cluster, count(distinct b.panelist_key) as ct_panelists,
	count(distinct session_id) as ct_sessions, count(*) as ct_journey_pages
into #tmp_ecosystem_cluster_sizing
from #taxonomy_ecosystem_patched a
inner join run.v_sequence_event_ecosystem b
	on a.taxonomy_ecosystem_key=b.tax_id
group by  cluster_id, cluster;
***/

-- 3) MAKE SESSION LEVEL VIEW OF SEQUENCE EVENT (THIS IS DONE FOR GXP IN OTHER SCRIPT)
---------------------------------------------------------------------------------------------------

-- 4 Report Verticals. 801=General Retail
-- select top 100 * from #temp_sequence_session_ecosystem
-- drop table #temp_sequence_session_ecosystem; 
select 801 as report_vertical, left(panelist_id,8) as panelist_key, device_type, digital_type, retail_cat_id, eco_cat1_id, property,
	session_date, session_hash, session_date_time, session_minutes, session_first_taxid_gxp
into #temp_sequence_session_ecosystem
from gxp.SequenceSesssionGXP_jk a
inner join gxp.TaxonomyEcosystemGXP b
	on a.session_first_taxid_gxp=b.taxid_whitelist
group by left(panelist_id,8), device_type, digital_type, retail_cat_id, eco_cat1_id, property,
	session_date, session_hash, session_date_time, session_minutes, session_first_taxid_gxp;
-- (343880 row(s) affected)

-- 802=Auto
insert into #temp_sequence_session_ecosystem
select 802 as report_vertical, left(panelist_id,8) as panelist_key, device_type, digital_type, retail_cat_id, eco_cat1_id, property,
	session_date, session_hash, session_date_time, session_minutes, session_first_taxid_gxp
from gxp.SequenceSesssionGXP_jk a
inner join gxp.TaxonomyEcosystemGXP b
	on a.session_first_taxid_gxp=b.taxid_whitelist
	and b.retail_cat_id=802 -- Auto Only
group by left(panelist_id,8), device_type, digital_type, retail_cat_id, eco_cat1_id, property,
	session_date, session_hash, session_date_time, session_minutes, session_first_taxid_gxp;
-- (6718 row(s) affected)

-- 803=Electronics
-- delete from #temp_sequence_session_ecosystem where report_vertical=803;
insert into #temp_sequence_session_ecosystem
select 803 as report_vertical, left(a.panelist_id,8) as panelist_key, device_type, digital_type, b.retail_cat_id, eco_cat1_id, property,
	session_date, session_hash, session_date_time, session_minutes, session_first_taxid_gxp
from gxp.SequenceSesssionGXP_jk a
inner join gxp.TaxonomyEcosystemGXP b
	on a.session_first_taxid_gxp=b.taxid_whitelist
	and b.retail_cat_id in (801,803) -- Electronics includes General Retail visits (e.g. visits to Target)
inner join gxp.Panelist_Retail_Segments p
	on a.panelist_id=p.panelist_id -- Filter on only people segmented in electronics
	and p.retail_cat_id=803
group by left(a.panelist_id,8), device_type, digital_type, b.retail_cat_id, eco_cat1_id, property,
	session_date, session_hash, session_date_time, session_minutes, session_first_taxid_gxp;
-- (129247 row(s) affected)

-- 804=Travel
insert into #temp_sequence_session_ecosystem
select 804 as report_vertical, left(panelist_id,8) as panelist_key, device_type, digital_type, retail_cat_id, eco_cat1_id, property,
	session_date, session_hash, session_date_time, session_minutes, session_first_taxid_gxp
from gxp.SequenceSesssionGXP_jk a
inner join gxp.TaxonomyEcosystemGXP b
	on a.session_first_taxid_gxp=b.taxid_whitelist
	and b.retail_cat_id=804 -- Travel Only
group by left(panelist_id,8), device_type, digital_type, retail_cat_id, eco_cat1_id, property,
	session_date, session_hash, session_date_time, session_minutes, session_first_taxid_gxp;
-- (30240 row(s) affected)

-- Insert end touchpoint session
-- Note: This should run after the stated purchases are inserted, because we want opportunity for purchase to be end event.
---------------------------------------------------------------------------------------------------------------------------
insert into #temp_sequence_session_ecosystem
select report_vertical, panelist_key, '' as device_type, '' as digital_type, 0 as retail_cat_id, 0 as eco_cat1_id, '[END]' as property,
	max(session_date) as session_date, 0 as session_hash,
	dateadd(minute,1,max(session_date_time)) AS session_date_time, -- choose time right after last event
	0 as session_minutes, 0 as session_first_taxid_gxp
from #temp_sequence_session_ecosystem
group by report_vertical,  panelist_key;
-- (5120 row(s) affected)


-- Remove panelists only 1 touchpoint (therefore no path)
---------------------------------------------------------
-- Identify panelists that only had 1 activity. Remove them from journy analysis.
-- select * from #panelist_touchpoints where ct_properties <=2;
-- select * from #panelist_touchpoints where panelist_key=2;
-- select * from #temp_sequence_session_ecosystem2 where panelist_key=2 order by start_time
-- drop table #panelist_touchpoints;
select report_vertical, panelist_key, count(distinct property) as ct_properties
into #panelist_touchpoints
from #temp_sequence_session_ecosystem
group by report_vertical, panelist_key;
-- (5120 row(s) affected)

-- select count(*) from #temp_sequence_session_ecosystem; --515205

-- select top 100 * from #temp_sequence_session_ecosystem2;
-- drop table #temp_sequence_session_ecosystem2;
select a.*
into #temp_sequence_session_ecosystem2
from #temp_sequence_session_ecosystem a
inner join #panelist_touchpoints b
	on a.report_vertical=b.report_vertical
	and a.panelist_key=b.panelist_key
	and b.ct_properties >= 3;
-- (513480 row(s) affected)

create index panelist_touchpoints0 on #panelist_touchpoints (report_vertical);
create index panelist_touchpoints1 on #panelist_touchpoints (panelist_key);


-- 4) PARTITION BY PERSON. AND ALSO VERTICAL IF SUB-ECOSYSTEMS EXIST (e.g. Verticals within Shopping Retail)
------------------------------------------------------------------------------------------------------------

-- select top 100 * from #temp_sequence_session_ecosystem2;

-- select top 100 * from #partitioned_points_ordered;
begin try drop table #partitioned_points_ordered end try begin catch end catch;
select row_number() over (partition by report_vertical, panelist_key order by report_vertical, panelist_key, session_date, session_date_time asc) as row_num,
	report_vertical, panelist_key, device_type, digital_type, session_date, session_hash, session_date_time, retail_cat_id, eco_cat1_id, property, session_first_taxid_gxp
into #partitioned_points_ordered
from #temp_sequence_session_ecosystem2 a
order by report_vertical, panelist_key, session_date, session_date_time;
-- (513480 row(s) affected)

-- select top 100 * from #agg_paths;
begin try drop table #agg_paths end try begin catch end catch;
select a.report_vertical, a.panelist_key, a.session_hash, a.device_type, a.digital_type,
	a.session_date as date_id, a.session_date_time as start_time,
	a.retail_cat_id, a.eco_cat1_id, a.property,
	isnull(b.device_type, a.device_type) as prev_device_type,
	isnull(b.digital_type, a.digital_type) as prev_digital_type,
	isnull(b.session_date, a.session_date) as prev_day_id,
	isnull(b.session_date_time, dateadd(minute,-1,a.session_date_time)) as prev_start_time,
	isnull(b.retail_cat_id, 0) as prev_retail_cat_id,
	isnull(b.eco_cat1_id,0) as prev_eco_cat1_id,
	isnull(b.property, '') as prev_property
into #agg_paths
from #partitioned_points_ordered a
left join #partitioned_points_ordered b
	on a.report_vertical=b.report_vertical
	and a.panelist_key=b.panelist_key
	and a.row_num=b.row_num+1;
-- (513480 row(s) affected)

/*** CHECK JOURNEY PATH DISTRIBUTION ***

	select top 100 * from #agg_paths order by report_vertical, panelist_key, start_time;

	select top 100 * from #agg_paths where report_vertical=802 order by report_vertical, panelist_key, start_time;
	select top 100 * from #agg_paths where report_vertical=803 order by report_vertical, panelist_key, start_time;
	select top 100 * from #agg_paths where report_vertical=804 order by report_vertical, panelist_key, start_time;
	select top 100 * from #agg_paths where property like 'booking.%';
	select top 100 * from #agg_paths where property like 'orbitz.%';
	
	select count(*) from #agg_paths where report_vertical=803;
	
 ***/

-- 5) JOIN TO LOOKUPS AND EXPORT
------------------------------------------------------------------------------------------------------------
-- select top 100 * from #journey_report order by report_vertical, panelist_key, start_time;
-- select top 100 * from #agg_paths where panelist_key=326 order by panelist_key, start_time;
-- select top 100 * from #temp_sequence_session_ecosystem3 order by content_vertical_id, panelist_key, start_time;
-- select top 100 * from #temp_sequence_session_ecosystem2 order by panelist_key, start_time;
/***
-- select count(*) from dbo.rpt_Online_Journey;
begin try drop table dbo.rpt_Online_Journey end try begin catch end catch;
select report_vertical, a.panelist_key, cluster_variety, p.platform_name, start_time,
	cluster, cluster_id, subcategory,
	prev_start_time, isnull(p2.platform_name,'') as prev_platform_name,
	case when prev_cluster = '' then '[Start]' else prev_cluster end as prev_cluster,
	case when prev_cluster_id = '' then 0 else prev_cluster_id end as prev_cluster_id,
	case when prev_subcategory = '' then '[Start]' else prev_subcategory end as prev_subcategory
into dbo.rpt_Online_Journey
from #agg_paths a
left outer join #platform_lookup p
	on a.platform_id=p.platform_id
left outer join #platform_lookup p2
	on a.prev_platform_id=p2.platform_id
where (prev_cluster_id <> cluster_id or prev_day_id <> day_id) -- remove same day back-to-back internals site visits.
	and cluster_variety > 1
;
-- (1677 row(s) affected)


-- select TOP 100 * from dbo.Output_Web_Journey where panelist_key=149 order by report_vertical, start_time;

-- Export
begin try drop table #journey_report end try begin catch end catch;
select 
	report_vertical,
	a.panelist_key,
	cluster_variety,
	--'' as seg_buyer,
	--'' as seg_buyer_channel,
	start_time,
	prev_cluster,
	prev_subcategory,
	cluster,
	subcategory	
into #journey_report
from dbo.rpt_Online_Journey a
;
-- (1497 rows affected)

select panelist_key, cluster_variety, cast(start_time as date) as day_id, start_time, prev_subcategory, prev_cluster, subcategory, cluster,
	case when prev_subcategory = subcategory then 1 else 0 end as self_loop
from #journey_report
order by panelist_key, start_time;
***/
-- END OF FILE --