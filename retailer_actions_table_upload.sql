--------------
--Retailer Action Table
--------------

create table core.ref.retailer_actions (
retailer_domain nvarchar(300),
retailer_action nvarchar(300),
action_identifier nvarchar(300)
)
;
insert into core.ref.retailer_actions select 'drizly.com', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'drizly.com', 'checkout', '%/order/%';
insert into core.ref.retailer_actions select 'drizly.com', 'page view', '%/p####%';
insert into core.ref.retailer_actions select 'drizly.com', 'page view', '%/p#####%';
insert into core.ref.retailer_actions select 'drizly.com', 'page view', '%/p######%';
insert into core.ref.retailer_actions select 'drizly.com', 'search', '%/search?q=%';
insert into core.ref.retailer_actions select 'instacart.com', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'instacart.com', 'category view', '%/departments/%';
insert into core.ref.retailer_actions select 'instacart.com', 'checkout', '%checkout%';
insert into core.ref.retailer_actions select 'instacart.com', 'product view', '%/items/%';
insert into core.ref.retailer_actions select 'instacart.com', 'search', '%/search_v3/%';
insert into core.ref.retailer_actions select 'kroger.com', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'kroger.com', 'category view', '%/pl/%';
insert into core.ref.retailer_actions select 'kroger.com', 'checkout', '%checkout%';
insert into core.ref.retailer_actions select 'kroger.com', 'product view', '%/p/%';
insert into core.ref.retailer_actions select 'kroger.com', 'search', '%searchType=%';
insert into core.ref.retailer_actions select 'kroger.com', 'search', '%/search?query=%';
insert into core.ref.retailer_actions select 'totalwine.com', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'totalwine.com', 'checkout', '%/checkout/%';
insert into core.ref.retailer_actions select 'totalwine.com', 'product view', '%/p/%';
insert into core.ref.retailer_actions select 'totalwine.com', 'search', '%/search/%';
insert into core.ref.retailer_actions select 'walmart.com', 'add to cart', '%/pac?id=/%';
insert into core.ref.retailer_actions select 'walmart.com', 'checkout', '%/checkout/%';
insert into core.ref.retailer_actions select 'walmart.com', 'page view', '%/ip/%';
insert into core.ref.retailer_actions select 'walmart.com', 'search', '%/search/%';
insert into core.ref.retailer_actions select 'tesco.com', 'search', '%/search?query=%';
insert into core.ref.retailer_actions select 'tesco.com', 'product view', '%/products/%';
insert into core.ref.retailer_actions select 'tesco.com', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'tesco.com', 'checkout', '%checkout/confirmation%';
insert into core.ref.retailer_actions select 'ocado.com', 'search', '%/search?entry=%';
insert into core.ref.retailer_actions select 'ocado.com', 'product view', '%/products/%';
insert into core.ref.retailer_actions select 'ocado.com', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'ocado.com', 'checkout', '%checkout/confirmation%';
insert into core.ref.retailer_actions select 'carrefour.fr', 'search', '%/s?q=%';
insert into core.ref.retailer_actions select 'carrefour.fr', 'product view', '%/p/%';
insert into core.ref.retailer_actions select 'carrefour.fr', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'carrefour.fr', 'checkout', '%/cart/confirmation?data=%';
insert into core.ref.retailer_actions select 'e-leclerc.com', 'search', '%/recherche?q=%';
insert into core.ref.retailer_actions select 'e-leclerc.com', 'search', '%recherche.aspx?TexteRecherche=%';
insert into core.ref.retailer_actions select 'e-leclerc.com', 'product view', '%/fiche-produits-%';
insert into core.ref.retailer_actions select 'e-leclerc.com', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'e-leclerc.com', 'checkout', 'NEED HISTORIC';
insert into core.ref.retailer_actions select 'ah.nl', 'search', '%/zoeken?query =%';
insert into core.ref.retailer_actions select 'ah.nl', 'product view', '%/producten/product/%';
insert into core.ref.retailer_actions select 'ah.nl', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'ah.nl', 'checkout', 'NEED HISTORIC';
insert into core.ref.retailer_actions select 'banabi.com.tr', 'search', '%/?s=PRODUCT%post_type=%';
insert into core.ref.retailer_actions select 'banabi.com.tr', 'product view', '%/urun/%';
insert into core.ref.retailer_actions select 'banabi.com.tr', 'add to cart', 'UNAVAILABLE FROM URL';
insert into core.ref.retailer_actions select 'banabi.com.tr', 'checkout', 'NEED HISTORIC';


--delete from core.ref.retailer_actions where retailer_domain in ('ulta.com', 'macys.com','sephora.com')