---------------------------------------------------------------------------------------------------
-- Search
---------------------------------------------------------------------------------------------------
-- Jimmy	
-- 2018/07/17
---------------------------------------------------------------------------------------------------
-- P&G SKINCARE AS EXAMPLE

USE PGSkincare;
-- FULL LIST OF DW TABLES USED
-- select top 100 * from [GoogleAuto].dbo.search_term_inclusion;
-- select top 100 * from [GoogleAuto].process.web_events;		-- internal process
-- select top 100 * from [GoogleAuto].dbo.v_Report_Web_Events;			-- readable report
-- select top 100 * from [GoogleAuto].ref.domain_name;
-- select top 100 * from [GoogleAuto].ref.search_term;


---------------------------------------------------------------------------------------------------
-- 1) Search Terms from (Inclusion List)
---------------------------------------------------------------------------------------------------

-- select * from dbo.search_term_inclusion where search_term_hash = '6632642189061046844';
--delete from dbo.search_term_inclusion where search_term_hash = '6632642189061046844'
--insert into dbo.search_term_inclusion select '6632642189061046844'


-- fetch panelist_id, domain_name, search_term, date, timestamp, usage_date

-- select top 100 * from #temp_search_event;
-- drop table #temp_search_event;
select p.panelist_key,
	d.domain_name,
	s.search_term,
	PassiveMaster.dbo.alphanum(concat(' ',s.search_term,' ')) as search_gra,
	p.day_id,
	p.start_time_local as time_stamp,
	cast(concat(p.panelist_key,'_',day_id) as varchar(350)) as usage_date,
	--s.search_term_hash 
	s.search_term_key -- pass this along so tagged columns can be joined
into #temp_search_event
from PGSkincare.run.processed_web_events p
inner join PGSkincare.ref.domain_name d
	on p.domain_name_key = d.domain_name_key
inner join PGSkincare.ref.search_term s
	on p.search_term_key = s.search_term_key;

select a.*, b.green_tea_id, b.green_pattern, b.retail_content
into #temp_search_event_skincare
from #temp_search_event a
inner join wrk.green_tea b
	on a.search_gra like b.green_pattern;
--(1379 row(s) affected)

select panelist_key, domain_name, search_term, day_id, time_stamp, usage_date, min(green_tea_id) as best_green_tea_id
into #temp_search_event_skincare_best
from #temp_search_event_skincare a
group by panelist_key, domain_name, search_term, day_id, time_stamp, usage_date;
-- (1253 row(s) affected)

-- create schema rpt;
-- drop table rpt.Search_Wave1;
select panelist_key, domain_name, search_term, day_id, time_stamp, usage_date as search_day,
	abs( convert(bigint,hashbytes('SHA1',concat(usage_date,domain_name,search_term))) % 10000000000) as search_id,
	green_keyword as match_keyword, retail_content_group as match_content_group, retail_content as match_content
into rpt.Search_Wave1
from #temp_search_event_skincare_best a
inner join wrk.green_tea b
	on a.best_green_tea_id=b.green_tea_id;
-- (1253 row(s) affected)

-- select * from rpt.Search_Wave1;


-- END OF FILE