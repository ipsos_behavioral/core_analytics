-------------
--Search Motivation Pattern
--------------

use [Core];

begin try drop table #import end try begin catch end catch;
create table #import(
	lemma	nvarchar(300),
	lemma_english	nvarchar(300),
	ambiguity_flag int,
	pattern	nvarchar(300),
	sensitivity_indicator	nvarchar(300),
)
insert into #import select 'best', 'best', 0, '%best%', 'quality';
insert into #import select 'review', 'review', 1, '%review%', 'quality';
insert into #import select 'sale', 'sale', 0, '%sale%', 'price';
insert into #import select 'cheap', 'cheap', 0, '%cheap%', 'price';
insert into #import select 'recommend', 'recommend', 0, '%recommend%', 'quality';
insert into #import select 'price', 'price', 0, '% price%', 'price';
insert into #import select 'pricing', 'pricing', 0, '%pricing%', 'price';
insert into #import select 'cost', 'cost', 0, '% cost %', 'price';
insert into #import select 'quality', 'quality', 0, '%quality%', 'quality';
insert into #import select 'rating', 'rating', 0, '%rating%', 'quality';
insert into #import select 'top', 'top', 1, '% top %', 'quality';
insert into #import select 'promotion', 'promotion', 0, '%promotion%', 'price';
insert into #import select 'deal', 'deal', 1, '%deal%', 'price';
insert into #import select 'location', 'location', 0, '%location%', 'location';
insert into #import select 'under $', 'under $', 0, '%under $%', 'price';
insert into #import select 'financ', 'financ', 1, '% financ%', 'price';
insert into #import select 'value', 'value', 0, '%value%', 'price';
insert into #import select 'promo code', 'promo code', 0, '% promo %', 'price';
insert into #import select 'coupon', 'coupon', 0, '%coupon%', 'price';
insert into #import select 'discount', 'discount', 0, '%discount%', 'price';
insert into #import select 'free', 'free', 0, '%free%', 'price';
insert into #import select 'clearance', 'clearance', 0, '%clearance%', 'price';
insert into #import select 'below %', 'below $', 0, '%below $%', 'price';
insert into #import select 'reward', 'reward', 1, '%reward%', 'price';
insert into #import select 'compare', 'compare', 0, '%compare%', 'quality';
insert into #import select 'comparison', 'comparison', 0, '%comparison%', 'quality';
insert into #import select 'ingredient', 'ingredient', 1, '%ingredient%', 'quality';
insert into #import select 'near', 'near', 0, '%near%', 'location';
insert into #import select 'new', 'new', 1, '%new%', 'novelty';
insert into #import select 'locator', 'locator', 0, '%locator%', 'location';
insert into #import select 'rebate', 'rebate', 0, '%rebate%', 'price';




-- insert in output from pattern table excel spreadsheet


begin try drop table ref.search_indicators end try begin catch end catch;
create table ref.search_indicators(
	lemma	nvarchar(300),
	lemma_english	nvarchar(300),
	ambiguity_flag int,
	pattern	nvarchar(300),
	sensitivity_indicator	nvarchar(300),
);

-- First 4 digits of pattern_id is the semantic_priority_id. Last 7 digits is hash of pattern

insert into ref.search_indicators (
	lemma, lemma_english, ambiguity_flag, pattern, sensitivity_indicator
)
select lemma, lemma_english, ambiguity_flag, pattern, sensitivity_indicator
from #import;
--(31 rows affected)