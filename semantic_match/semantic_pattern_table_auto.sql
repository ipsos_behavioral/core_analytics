---------------------------------------------------------------------------------------------------
-- SEMANTIC PATTERN TABLE: AUTO RETAIL
---------------------------------------------------------------------------------------------------
-- Jimmy	(2018/08/02)
-- Informed by Google Auto Stugy
---------------------------------------------------------------------------------------------------
use core;

-- CREATE PATTERN TABLE
-----------------------

-- List of keywords and patterns for TEXT ECOSYSTEM ASSIGNMENT
begin try drop table #import end try begin catch end catch;
create table #import(
	white	int,
	green	int,
	pattern nvarchar(300),
	keyword	nvarchar(300),
	filter_id	int,
	filter_name	nvarchar(300),
	label		nvarchar(300),
	sublabel	nvarchar(300)
);
insert into #import select 1 ,1, '% bmw %', 'bmw', 30, 'Brand', 'bmw', 'bmw';
insert into #import select 1 ,1, '% minicooper %', 'mini cooper', 30, 'Brand', 'bmw', 'mini';
insert into #import select 1 ,1, '% mini cooper %', 'mini cooper', 30, 'Brand', 'bmw', 'mini';
insert into #import select 1 ,1, '% mercedesbenz %', 'benz', 30, 'Brand', 'daimler', 'mercedes-benz';
insert into #import select 1 ,1, '% mercedes benz %', 'benz', 30, 'Brand', 'daimler', 'mercedes-benz';
insert into #import select 1 ,1, '% benz %', 'benz', 30, 'Brand', 'daimler', 'mercedes-benz';
insert into #import select 1 ,1, '% smartcar %', 'smart car', 30, 'Brand', 'diamler', 'smart';
insert into #import select 1 ,1, '% smart car %', 'smart car', 30, 'Brand', 'diamler', 'smart';
insert into #import select 1 ,1, '% alfa romeo %', 'alfa romeo', 30, 'Brand', 'fca', 'alfa romeo';
insert into #import select 1 ,1, '% chrysler %', 'chrysler', 30, 'Brand', 'fca', 'chrysler';
insert into #import select 1 ,0, '% dodge %', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% dodge% truck%', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% dodge% car %', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% car% dodge %', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% auto% dodge %', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% truck% dodge %', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% dodge% charger %', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% dodge% challenger %', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% dodge% durango %', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% dodge% journey %', 'dodge', 30, 'Brand', 'fca', 'dodge';
insert into #import select 1 ,1, '% ferrari %', 'ferrari', 30, 'Brand', 'fca', 'ferrari';
insert into #import select 1 ,1, '% fiat %', 'fiat', 30, 'Brand', 'fca', 'fiat';
insert into #import select 1 ,1, '% jeep %', 'jeep', 30, 'Brand', 'fca', 'jeep';
insert into #import select 1 ,1, '% maserati %', 'maserati', 30, 'Brand', 'fca', 'maserati';
insert into #import select 1 ,0, '% ram %', 'ram', 30, 'Brand', 'fca', 'ram';
insert into #import select 1 ,1, '% ram_truck%', 'ram', 30, 'Brand', 'fca', 'ram';
insert into #import select 1 ,1, '% ram % truck%', 'ram', 30, 'Brand', 'fca', 'ram';
insert into #import select 1 ,1, '% car % ram %', 'ram', 30, 'Brand', 'fca', 'ram';
insert into #import select 1 ,1, '% auto% ram %', 'ram', 30, 'Brand', 'fca', 'ram';
insert into #import select 1 ,0, '% ford %', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,1, '% ford% truck%', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,1, '% ford% car %', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,1, '% car % ford %', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,1, '% auto% ford %', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,1, '% ford% explorer %', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,1, '% ford% escape %', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,1, '% ford% raptor %', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,1, '% ford% bronco %', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,1, '% ford% mustang %', 'ford', 30, 'Brand', 'ford', 'ford';
insert into #import select 1 ,0, '% lincoln %', 'lincoln', 30, 'Brand', 'ford', 'lincoln';
insert into #import select 1 ,1, '% lincoln% car %', 'lincoln', 30, 'Brand', 'ford', 'lincoln';
insert into #import select 1 ,1, '% lincoln% truck%', 'lincoln', 30, 'Brand', 'ford', 'lincoln';
insert into #import select 1 ,1, '% car % lincoln %', 'lincoln', 30, 'Brand', 'ford', 'lincoln';
insert into #import select 1 ,1, '% auto% lincoln %', 'lincoln', 30, 'Brand', 'ford', 'lincoln';
insert into #import select 1 ,1, '% lincoln% continental %', 'lincoln', 30, 'Brand', 'ford', 'lincoln';
insert into #import select 1 ,1, '% lincoln% mk_ %', 'lincoln', 30, 'Brand', 'ford', 'lincoln';
insert into #import select 1 ,1, '% lincoln% navigator %', 'lincoln', 30, 'Brand', 'ford', 'lincoln';
insert into #import select 1 ,1, '% volvo %', 'volvo', 30, 'Brand', 'geely automobile hld', 'volvo';
insert into #import select 1 ,1, '% buick %', 'buick', 30, 'Brand', 'general motors', 'buick';
insert into #import select 1 ,1, '% cadillac %', 'cadillac', 30, 'Brand', 'general motors', 'cadillac';
insert into #import select 1 ,1, '% chevrolet %', 'chevrolet', 30, 'Brand', 'general motors', 'chevrolet';
insert into #import select 1 ,1, '% gmc %', 'gmc', 30, 'Brand', 'general motors', 'gmc';
insert into #import select 1 ,1, '% pontiac %', 'pontiac', 30, 'Brand', 'general motors', 'pontiac';
insert into #import select 1 ,1, '% acura %', 'acura', 30, 'Brand', 'honda', 'acura';
insert into #import select 1 ,0, '% honda %', 'honda', 30, 'Brand', 'honda', 'honda';
insert into #import select 1 ,1, '% honda % car%', 'honda', 30, 'Brand', 'honda', 'honda';
insert into #import select 1 ,1, '% honda % auto%', 'honda', 30, 'Brand', 'honda', 'honda';
insert into #import select 1 ,1, '% car% honda %', 'honda', 30, 'Brand', 'honda', 'honda';
insert into #import select 1 ,1, '% auto% honda %', 'honda', 30, 'Brand', 'honda', 'honda';
insert into #import select 1 ,1, '% hyundai %', 'hyundai', 30, 'Brand', 'hyundai', 'hyundai';
insert into #import select 1 ,1, '% kia %', 'kia', 30, 'Brand', 'hyundai', 'kia';
insert into #import select 1 ,1, '% mazda %', 'mazda', 30, 'Brand', 'mazda motors', 'mazda';
insert into #import select 1 ,1, '% mitsubishi %', 'mitsubishi', 30, 'Brand', 'mitsubishi motors', 'mitsubishi';
insert into #import select 1 ,1, '% infiniti %', 'infiniti', 30, 'Brand', 'nissan', 'infiniti';
insert into #import select 1 ,1, '% nissan %', 'nissan', 30, 'Brand', 'nissan', 'nissan';
insert into #import select 1 ,1, '% subaru %', 'subaru', 30, 'Brand', 'subaru', 'subaru';
insert into #import select 1 ,0, '% jaguar %', 'jaguar', 30, 'Brand', 'tata', 'jaguar';
insert into #import select 1 ,1, '% jaguar % car%', 'jaguar', 30, 'Brand', 'tata', 'jaguar';
insert into #import select 1 ,1, '% jaguar % auto%', 'jaguar', 30, 'Brand', 'tata', 'jaguar';
insert into #import select 1 ,1, '% car% jaguar %', 'jaguar', 30, 'Brand', 'tata', 'jaguar';
insert into #import select 1 ,1, '% auto% jaguar %', 'jaguar', 30, 'Brand', 'tata', 'jaguar';
insert into #import select 1 ,1, '% land rover %', 'land rover', 30, 'Brand', 'tata', 'land rover';
insert into #import select 1 ,1, '% tesla %', 'tesla', 30, 'Brand', 'tesla motors', 'tesla';
insert into #import select 1 ,1, '% lexus %', 'lexus', 30, 'Brand', 'toyota', 'lexus';
insert into #import select 1 ,1, '% scion %', 'scion', 30, 'Brand', 'toyota', 'scion';
insert into #import select 1 ,1, '% toyota %', 'toyota', 30, 'Brand', 'toyota', 'toyota';
insert into #import select 1 ,1, '% audi %', 'audi', 30, 'Brand', 'volkswagen', 'audi';
insert into #import select 1 ,1, '% bentley %', 'bentley', 30, 'Brand', 'volkswagen', 'bentley';
insert into #import select 1 ,1, '% lamborghini %', 'lamborghini', 30, 'Brand', 'volkswagen', 'lamborghini';
insert into #import select 1 ,1, '% porsche %', 'porsche', 30, 'Brand', 'volkswagen', 'porsche';
insert into #import select 1 ,1, '% volkswagen %', 'volkswagen', 30, 'Brand', 'volkswagen', 'volkswagen';
insert into #import select 0 ,1, '% carmax %', 'carmax', 31, 'Retail', 'marketplaces', 'carmax';
insert into #import select 0 ,1, '% carfax %', 'carfax', 31, 'Retail', 'marketplaces', 'carfax';
insert into #import select 0 ,1, '% truecar %', 'truecar', 31, 'Retail', 'marketplaces', 'truecar';
insert into #import select 0 ,1, '% carvana %', 'carvana', 31, 'Retail', 'marketplaces', 'carvana';
insert into #import select 0 ,1, '% ebay motors %', 'ebay motors', 31, 'Retail', 'marketplaces', 'ebay motors';
insert into #import select 0 ,1, '% craigslist car %', 'craigslist car', 31, 'Retail', 'marketplaces', 'craigslist car';
insert into #import select 0 ,1, '% amazon garage %', 'amazon garage', 31, 'Retail', 'endemics', 'amazon garage';
insert into #import select 0 ,1, '% autotrader %', 'autotrader', 31, 'Retail', 'endemics', 'autotrader';
insert into #import select 0 ,1, '% cargurus %', 'cargurus', 31, 'Retail', 'endemics', 'cargurus';
insert into #import select 0 ,1, '% cars_com %', 'cars.com', 31, 'Retail', 'endemics', 'cars.com';
insert into #import select 0 ,1, '% edmunds %', 'edmunds', 31, 'Retail', 'endemics', 'edmunds';
insert into #import select 0 ,1, '% kbb %', 'kbb', 31, 'Retail', 'endemics', 'kbb';
insert into #import select 0 ,1, '% car and driver %', 'car and driver', 34, 'News/Info', 'news and info', 'car and driver';
insert into #import select 0 ,1, '% autonews %', 'autonews', 34, 'News/Info', 'news and info', 'autonews';
insert into #import select 0 ,1, '% motortrend%', 'motortrend', 34, 'News/Info', 'news and info', 'motortrend';
insert into #import select 0 ,1, '% accident%', '[accident]', 0, '[Remove]', '[remove]', '[accident]';
insert into #import select 0 ,1, '% self%driving %', '[self driving]', 0, '[Remove]', '[remove]', '[self driving]';
insert into #import select 0 ,1, '% toy car%', '[toy car]', 0, '[Remove]', '[remove]', '[toy car]';
insert into #import select 0 ,1, '% cable%car %', '[cable car]', 0, '[Remove]', '[remove]', '[cable car]';
insert into #import select 0 ,1, '% car%pool %', '[car pool]', 0, '[Remove]', '[remove]', '[car pool]';
insert into #import select 0 ,1, '% car% movie %', '[car movie]', 0, '[Remove]', '[remove]', '[car movie]';
insert into #import select 0 ,1, '% car% rent%', '[car rental]', 0, '[Remove]', '[remove]', '[car rental]';
insert into #import select 0 ,1, '% car% gam%', '[car game]', 0, '[Remove]', '[remove]', '[car game]';
insert into #import select 0 ,1, '% car% rac%', '[car racing]', 0, '[Remove]', '[remove]', '[car racing]';
insert into #import select 0 ,1, '% car% news %', '[car news]', 0, '[Remove]', '[remove]', '[car news]';
insert into #import select 0 ,1, '% car% wash %', '[car news]', 0, '[Remove]', '[remove]', '[car wash]';
insert into #import select 0 ,1, '% race car%', '[race car]', 0, '[Remove]', '[remove]', '[race car]';
insert into #import select 0 ,1, '% daytona %', '[daytona]', 0, '[Remove]', '[remove]', '[daytona]';
insert into #import select 0 ,1, '% ind% 500 %', '[indy 5000]', 0, '[Remove]', '[remove]', '[indy 5000]';
insert into #import select 0 ,1, '% formula 1 %', '[formula 1]', 0, '[Remove]', '[remove]', '[formula 1]';
insert into #import select 1 ,0, '% pick%up %', 'truck', 90, 'Topic', 'segment', 'truck';
insert into #import select 1 ,0, '% truck %', 'truck', 90, 'Topic', 'segment', 'truck';
insert into #import select 1 ,0, '% trucks %', 'truck', 90, 'Topic', 'segment', 'truck';
insert into #import select 1 ,0, '% van %', 'van', 90, 'Topic', 'segment', 'van';
insert into #import select 1 ,0, '% vans %', 'van', 90, 'Topic', 'segment', 'van';
insert into #import select 1 ,0, '%wagon %', 'wagon', 90, 'Topic', 'segment', 'wagon';
insert into #import select 1 ,0, '%wagons %', 'wagon', 90, 'Topic', 'segment', 'wagon';
insert into #import select 1 ,1, '% sedan %', 'sedan', 90, 'Topic', 'segment', 'sedan';
insert into #import select 1 ,1, '% sedans %', 'sedan', 90, 'Topic', 'segment', 'sedan';
insert into #import select 1 ,1, '% coupe %', 'coupe', 90, 'Topic', 'segment', 'coupe';
insert into #import select 1 ,0, '% crossover %', 'crossover', 90, 'Topic', 'segment', 'crossover';
insert into #import select 1 ,0, '% camper %', 'camper', 90, 'Topic', 'segment', 'camper';
insert into #import select 1 ,0, '% electric %', 'electric car', 90, 'Topic', 'segment', 'electric car';
insert into #import select 1 ,1, '% electric car%', 'electric car', 90, 'Topic', 'segment', 'electric car';
insert into #import select 1 ,1, '% electric vehicle%', 'electric car', 90, 'Topic', 'segment', 'electric car';
insert into #import select 1 ,0, '% hybrid %', 'hybrid', 90, 'Topic', 'segment', 'hybrid';
insert into #import select 1 ,1, '% hybrid car%', 'hybrid', 90, 'Topic', 'segment', 'hybrid';
insert into #import select 1 ,1, '% hybrid vehicle%', 'hybrid', 90, 'Topic', 'segment', 'hybrid';
insert into #import select 1 ,1, '% hatchback %', 'hatchback', 90, 'Topic', 'segment', 'hatchback';
insert into #import select 1 ,1, '% mini%van %', 'minivan', 90, 'Topic', 'segment', 'minivan';
insert into #import select 1 ,1, '% roadster %', 'roadster', 90, 'Topic', 'segment', 'roadster';
insert into #import select 1 ,1, '% suv %', 'suv', 90, 'Topic', 'segment', 'suv';
insert into #import select 1 ,1, '% crv %', 'crv', 90, 'Topic', 'segment', 'crv';
insert into #import select 0 ,1, '% car %review %', 'car review', 90, 'Topic', 'general', 'car review';
insert into #import select 0 ,0, '% cars %', 'cars', 90, 'Topic', '[exclude: false +]', 'cars';
insert into #import select 0 ,0, '% auto %', 'auto', 90, 'Topic', '[exclude: false +]', 'auto';
insert into #import select 0 ,1, '% autos %', 'auto', 90, 'Topic', 'general', 'auto';



-- drop table wrk.all_tea_v2;
begin try drop table ref.pattern_table_auto end try begin catch end catch;
create table ref.pattern_table_auto (
	pattern_id	int identity,
	white int,
	green int,
	pattern nvarchar(300),
	keyword	nvarchar(300),
	filter_id	int,
	filter_name	nvarchar(300),
	label_id	int,
	label		nvarchar(300),
	sublabel_id	int,
	sublabel	nvarchar(300)
);

-- First 2 digits of pattern_id is the filter_id. Last 7 digits is hash of pattern
SET IDENTITY_INSERT ref.pattern_table_auto ON;

insert into ref.pattern_table_auto (
	pattern_id, white, green, pattern, keyword, filter_id, filter_name,
	label_id, label, sublabel_id, sublabel
)
select (filter_id*10000000)+abs(convert(bigint,hashbytes('SHA1',pattern))%10000000) as pattern_id,
	white, green, pattern, keyword,
	filter_id, filter_name,	
	abs(convert(bigint,hashbytes('SHA1',label)))%1000000000 as label_id, label,
	abs(convert(bigint,hashbytes('SHA1',sublabel)))%1000000000 as sublabel_id, sublabel
from #import;

create index all_tea_v2a on ref.pattern_table_auto (pattern_id);
create index all_tea_v2b on ref.pattern_table_auto (keyword);
create index all_tea_v2c on ref.pattern_table_auto (pattern);
create index all_tea_v2d on ref.pattern_table_auto (filter_id);

select * from core.ref.pattern_table_auto order by filter_id, label, sublabel, pattern_id;

-- END OF FILE --