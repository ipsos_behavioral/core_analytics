---------------------------------------------------------------------------------------------------
-- SEMANTIC PATTERN TABLE: ELECTRONICS RETAIL
---------------------------------------------------------------------------------------------------
-- Jimmy	(2018/08/03)
-- Informed by Google YT Shopper Study
---------------------------------------------------------------------------------------------------
use core;

-- CREATE PATTERN TABLE
-----------------------

-- List of keywords and patterns for TEXT ECOSYSTEM ASSIGNMENT

begin try drop table #import end try begin catch end catch;
create table #import(
	white	int,
	green	int,
	pattern nvarchar(300),
	keyword	nvarchar(300),
	filter_id	int,
	filter_name	nvarchar(300),
	label		nvarchar(300),
	sublabel	nvarchar(300)
);

insert into #import select 0 ,1, '% sony pictures %', '[sony pictures]', 0, '[Remove]', '[remove]', '[sony pictures]';
insert into #import select 1 ,0, '% dell %', 'dell', 30, 'Brand', 'computer', 'dell';
insert into #import select 1 ,1, '% lenovo %', 'lenovo', 30, 'Brand', 'computer', 'lenovo';
insert into #import select 1 ,1, '% macbook %', 'macbook', 30, 'Brand', 'computer', 'macbook';
insert into #import select 1 ,1, '% surface laptop %', 'surface laptop', 30, 'Brand', 'computer', 'surface laptop';
insert into #import select 1 ,1, '% surface pro %', 'surface pro', 30, 'Brand', 'computer', 'surface pro';
insert into #import select 1 ,1, '% amazon % echo %', 'amazon echo', 30, 'Brand', 'device', 'amazon echo';
insert into #import select 1 ,1, '% amazon% fire%stick %', 'amazon fire stick', 30, 'Brand', 'device', 'amazon fire stick';
insert into #import select 1 ,1, '% echo dot %', 'echo dot', 30, 'Brand', 'device', 'echo dot';
insert into #import select 1 ,1, '% google home %', 'google home', 30, 'Brand', 'device', 'google home';
insert into #import select 1 ,1, '% barnes % nook %', 'nook', 30, 'Brand', 'device', 'barnes noble nook';
insert into #import select 1 ,1, '% roku %', 'roku', 30, 'Brand', 'device', 'roku';
insert into #import select 1 ,1, '% texas instrument%', 'texas instrument', 30, 'Brand', 'device', 'texas instrument';
insert into #import select 1 ,1, '% ti 84 %', 'ti-84', 30, 'Brand', 'device', 'ti-84';
insert into #import select 1 ,1, '% lg %', 'lg', 30, 'Brand', 'general electronics', 'lg';
insert into #import select 1 ,0, '% microsoft %', 'microsoft', 30, 'Brand', 'general electronics', 'microsoft';
insert into #import select 1 ,1, '% samsung %', 'samsung', 30, 'Brand', 'general electronics', 'samsung';
insert into #import select 1 ,1, '% sony %', 'sony', 30, 'Brand', 'general electronics', 'sony';
insert into #import select 1 ,0, '% android %', 'android', 30, 'Brand', 'phone', 'android';
insert into #import select 1 ,1, '% galaxy% note %', 'galaxy note', 30, 'Brand', 'phone', 'galaxy note';
insert into #import select 1 ,1, '% iphone %', 'iphone', 30, 'Brand', 'phone', 'iphone';
insert into #import select 1 ,1, '% moto g5 %', 'moto g5', 30, 'Brand', 'phone', 'moto g5';
insert into #import select 1 ,1, '% ipad %', 'ipad', 30, 'Brand', 'tablet', 'ipad';
insert into #import select 1 ,1, '% kindle %', 'kindle', 30, 'Brand', 'tablet', 'kindle';
insert into #import select 1 ,1, '% panosonic %', 'panosonic', 30, 'Brand', 'television', 'panosonic';
insert into #import select 1 ,0, '% pioneer %', 'pioneer', 30, 'Brand', 'television', 'pioneer';
insert into #import select 1 ,1, '% toshiba %', 'toshiba', 30, 'Brand', 'television', 'toshiba';
insert into #import select 1 ,1, '% vizio %', 'vizio', 30, 'Brand', 'television', 'vizio';
insert into #import select 1 ,1, '% apple%watch %', 'applewatch', 30, 'Brand', 'wearable', 'applewatch';
insert into #import select 1 ,1, '% fitbit %', 'fitbit', 30, 'Brand', 'wearable', 'fitbit';
insert into #import select 1 ,1, '% garmin %', 'garmin', 30, 'Brand', 'wearable', 'garmin';
insert into #import select 0 ,1, '% best buy %', 'best buy', 31, 'Retail', 'retail', 'best buy';
insert into #import select 0 ,1, '% bestbuy %', 'best buy', 31, 'Retail', 'retail', 'best buy';
insert into #import select 0 ,0, '% computer %', 'computer', 90, 'Topic', '[exclude: false +]', 'computer';
insert into #import select 0 ,1, '% laptops %', 'laptops', 90, 'Topic', 'computer', 'laptops';
insert into #import select 0 ,1, '% wireless %', 'wireless', 90, 'Topic', 'general electronics', 'wireless';
insert into #import select 0 ,1, '% phones %', 'phones', 90, 'Topic', 'phone', 'phones';
insert into #import select 0 ,1, '% ereader %', 'tablet', 90, 'Topic', 'device', 'ereader';
insert into #import select 0 ,1, '% tablet %', 'tablet', 90, 'Topic', 'tablet', 'tablet';
insert into #import select 0 ,1, '% smart tv %', 'smart tv', 90, 'Topic', 'television', 'smart tv';
insert into #import select 0 ,1, '% televisions %', 'televisons', 90, 'Topic', 'television', 'televisons';
insert into #import select 0 ,1, '% tvs %', 'tvs', 90, 'Topic', 'television', 'tvs';




-- drop table wrk.all_tea_v2;
begin try drop table ref.pattern_table_electronics end try begin catch end catch;
create table ref.pattern_table_electronics (
	pattern_id	int identity,
	white int,
	green int,
	pattern nvarchar(300),
	keyword	nvarchar(300),
	filter_id	int,
	filter_name	nvarchar(300),
	label_id	int,
	label		nvarchar(300),
	sublabel_id	int,
	sublabel	nvarchar(300)
);

-- First 2 digits of pattern_id is the filter_id. Last 7 digits is hash of pattern
SET IDENTITY_INSERT ref.pattern_table_electronics ON;

insert into ref.pattern_table_electronics (
	pattern_id, white, green, pattern, keyword, filter_id, filter_name,
	label_id, label, sublabel_id, sublabel
)
select (filter_id*10000000)+abs(convert(bigint,hashbytes('SHA1',pattern))%10000000) as pattern_id,
	white, green, pattern, keyword,
	filter_id, filter_name,	
	abs(convert(bigint,hashbytes('SHA1',label)))%1000000000 as label_id, label,
	abs(convert(bigint,hashbytes('SHA1',sublabel)))%1000000000 as sublabel_id, sublabel
from #import;

create index pattern_table_electronics1 on ref.pattern_table_electronics (pattern_id);
create index pattern_table_electronics2 on ref.pattern_table_electronics (keyword);
create index pattern_table_electronics3 on ref.pattern_table_electronics (pattern);
create index pattern_table_electronics4 on ref.pattern_table_electronics (filter_id);

select * from core.ref.pattern_table_electronics order by filter_id, label, sublabel, pattern_id;

-- END OF FILE --