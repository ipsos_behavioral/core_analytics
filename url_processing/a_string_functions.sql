---------------------------------------------------------------------------------------------------
-- String Functions
---------------------------------------------------------------------------------------------------
-- Jimmy	(2018/06/25)
---------------------------------------------------------------------------------------------------

-- CREATE FUNCTION: ALPHA NUM ONLY
----------------------------------
-- Replaces non-aplha numeric with spaces.
use core;

-- drop function dbo.alphanum;
create function dbo.alphanum(@temp varchar(5000))
returns varchar(5000)
as
begin
    declare @keep_values as varchar(300)
    set @keep_values = '%[^A-Z0-9a-z ]%'
    while PatIndex(@keep_values, @temp) > 0
        Set @temp = Stuff(@temp, PatIndex(@keep_values, @temp), 1, ' ')
    return replace(concat(' ',@temp,' '), '  ', ' ')
end;

-- CREATE FUNCTION: REPLACE 3 CHAR UNICODE (e.g. %3A) WITH SINGLE SPACE
-----------------------------------------------------------------------
-- drop function dbo.nixuni;
----------------------------------
-- USE [GoogleAuto];

create function dbo.nixuni(@temp varchar(5000))
returns varchar(5000)
as
begin
    declare @nix_values as varchar(300)
    set @nix_values = '%[%][A-Z0-9a-z][A-Z0-9a-z]%'
    while PatIndex(@nix_values, @temp) > 0
        Set @temp = Stuff(@temp, PatIndex(@nix_values, @temp), 3, ' ')
    return @temp
end;

-- CREATE FUNCTION: SIMPLE PII MASKING RULE
-------------------------------------------
-- Replaces 5 or more consecutive digits with 00000
-- Replaces 30 or more consecutive alphanum (including '-') with *****
-- drop function dbo.maskpii;
----------------------------------
-- 448391

create function dbo.maskpii(@temp varchar(5000))
returns varchar(5000)
as
begin
    declare @mask_unicode as varchar(300)
    declare @mask_hash as varchar(300)
    declare @mask_digits as varchar(300)
	set @mask_unicode = '%[%][A-Z0-9a-z][A-Z0-9a-z]%'
	set @mask_hash = '%[0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-][0-9a-z-]%'
    set @mask_digits = '%[0-9][0-9][0-9][0-9][0-9]%'
    while PatIndex(@mask_unicode, @temp) > 0
        Set @temp = Stuff(@temp, PatIndex(@mask_unicode, @temp), 3, '*')
    while PatIndex('%*****%', @temp) = 0 and PatIndex(@mask_hash, @temp) > 0 
        Set @temp = Stuff(@temp, PatIndex(@mask_hash, @temp), len(@temp)-PatIndex(@mask_hash, @temp)-PatIndex(@mask_hash, reverse(@temp))+2, '*****')
    while PatIndex('%00000%', @temp) = 0 and PatIndex(@mask_digits, @temp) > 0 
        Set @temp = Stuff(@temp, PatIndex(@mask_digits, @temp), len(@temp)-PatIndex(@mask_digits, @temp)-PatIndex(@mask_digits, reverse(@temp))+2, '00000')
    return @temp
end;

-- END OF FILE --