---------------------------------------------------------------------------------------------------
-- URL Splitter
---------------------------------------------------------------------------------------------------
-- Dylan	(2018/06/22)
-- Jimmy	(2018/06/25)
---------------------------------------------------------------------------------------------------
-- RUN GOOGLE AUTO TEST
----------------------------------

USE [GoogleAuto];

/****** Script for SelectTopNRows command from SSMS  ******/
begin try drop table #url_query_split end try begin catch end catch;
begin try drop table #url_protocol_split end try begin catch end catch;
begin try drop table #url_host_split end try begin catch end catch;
begin try drop table #url_dir_split end try begin catch end catch;
begin try drop table #url_gra end try begin catch end catch;

-- select top 100 * from #url_query_split;
-- drop table #url_query_split;
select url_key, url,
	-- charindex('?',url) as index_num, charindex('#',url) as index_num2,
	case when charindex('?',url)>0 then left(lower(url), charindex('?',url)-1)
		else case when charindex('#',url)>0 then left(lower(url), charindex('#',url)-1)		
		else lower(url) end end as url_not_query,
	case when charindex('?',url)>0 then right(lower(url), len(url)-charindex('?',url))
		else case when charindex('#',url)>0 then right(lower(url), len(url)-charindex('#',url))
		else '' end end as query
INTO #url_query_split
FROM ref.URL;
-- (3099812 row(s) affected)
-- 1:01

-- SELECT Protocol, Max(Place) As Place FROM #url_protocol_split GROUP BY Protocol ORDER BY Protocol

-- select top 100 * from #url_protocol_split;
-- drop table #url_protocol_split;
select url_key, url,
	case when charindex('://', url_not_query)>0 then right(url_not_query, len(url_not_query)-charindex('://',url_not_query)-2)
		else url_not_query end as url_not_protocol,
	case when charindex('://',url_not_query)>0 then left(url_not_query, charindex('://',url_not_query)-1)
		else '' end as protocol,
		-- Note for Dylan. Script works fine for 1000 example records, but didn't work for larger dataset (~3MM... needs edit)
	query
into #url_protocol_split
from #url_query_split;

/***	drop table dbo.jk_url_protocol_split;
		select *
		into dbo.jk_url_protocol_split
		from #url_protocol_split;
***/

-- select top 100 * from #url_host_split where url like '%www.%'
-- drop table #url_host_split;
select url_key, url, protocol,
	case when charindex('/', url_not_protocol)=0 then url_not_protocol
		else substring(replace(url_not_protocol,'www.',''), 1, charindex('/', replace(url_not_protocol,'www.',''))-1) end as host,
	case when charindex('/', url_not_protocol)=0 then ''
		else substring(url_not_protocol, charindex('/', url_not_protocol)+1, 300) end as url_not_host,
	query
into #url_host_split
from #url_protocol_split;
-- (3099812 row(s) affected)

-- select top 100 * from #url_dir_split;
-- drop table #url_dir_split;
select url_key, url, protocol, host,
	-- url_not_host, charindex('/', reverse(url_not_host))-1 as reverse_index,
	case when url_not_host = '' then ''
		else case when charindex('/', reverse(url_not_host))-1 >=0
			then left(url_not_host, len(url_not_host)-charindex('/', reverse(url_not_host))+1)
		else '' end end as dir,
	case when url_not_host = '' then ''
		else case when charindex('/', reverse(url_not_host))-1 >=0
			then right(url_not_host, charindex('/', reverse(url_not_host))-1)
		else url_not_host end end as page,
	query
into #url_dir_split
from #url_host_split;
-- (3099812 row(s) affected)

/***
select round(url_len/100,0) as len_group, count(*)
from #url_gra
group by round(url_len/100,0)
order by 1 desc;
-- select * from #url_gra where url_len <=10;
***/

-- select top 100 * from #url_gra where url_len >100 order by url_len desc;
-- select top 100 * from #url_gra where host_len >90 order by host_len desc;
-- select top 100 * from #url_gra where dir_len >100 order by dir_len desc;
-- select top 100 * from #url_gra where page_len >100 order by page_len desc;
-- select top 100 * from #url_gra where query_len >100 order by query_len desc;
-- drop table #url_gra;
select	url_key,	--int
		url,		--ncarchar(4000)
		cast(replace(protocol,'//','') as varchar(300)) as protocol,	-- occasional instances URL is preceeded by //
		cast(replace(host,':443', '') as varchar(300)) as host,			-- occasional instances domain is succeeeded by :443
		cast(dir as varchar(3000)) as dir,
		cast(page as varchar(3000)) as page,
		cast(query as varchar(3000)) as query,
		cast(' '+left(host,97)+' '+left(dir,100)+' '+left(page,100)+' '+left(query,300)+' ' as varchar(600)) as url_input,
		len(host) as host_len,
		len(dir) as dir_len,
		len(page) as page_len,
		len(query) as query_len,
		len(url) as url_len
into #url_gra
from #url_dir_split;
-- (3099812 row(s) affected)
-- select top 100 * from #url_gra_nix_uni
-- drop table #url_gra_nix_uni;
select url_key, url, protocol, host, dir, page, query,
	core.dbo.nixuni(url_input) as url_gra_nix_uni
into #url_gra_nix_uni
from #url_gra;
--	10K		=	0 seconds
--	100K	=	5 seconds 
--	1MM		~	40 seconds
--	3MM		~	3 min
-- (3099812 row(s) affected)	00:02:23


-- select top 100 * from ref.url_gra;
-- drop table ref.url_gra
select url_key, url, protocol, host, dir, page, query,
	core.dbo.alphanum(url_gra_nix_uni) as url_gra
into ref.url_gra
from #url_gra_nix_uni;

create index url_gra1 on ref.url_gra(url_key);

--	10K		=	3 seconds
--	100K	=	50 seconds 
--	1MM		~	8 min
--	3MM		~	25 min
-- (3099812 row(s) affected)	00:15:42


-- Sanity check distinct values in protocol, and hosts
------------------------------------------------------
select protocol, count(*) as ct_records
from ref.url_gra
group by protocol
order by 2 desc;

select host, count(*) as ct_records
from ref.url_gra
group by host
order by 2 desc;

-- select top 100 * from ref.url_gra;
-- select top 100 * from googleauto.ref.url_gra where url_gra like '% ford %';
-- select count(*) from googleauto.ref.url_gra where url_gra like '% ford %';

select top 100 * from googleauto.ref.url_gra where url_gra like '% ford %';

select top 100
	url_key,
	url,
	case when (dir <> '' or page <> '') and query <> '' then host+'/'+dir+page+'?'+passivemaster.dbo.maskpii(query)
		else case when  (dir <> '' or page <> '') and query = '' then host+'/'+dir+page
		else host end end as url_maskpii,
	url_gra
from googleauto.ref.url_gra where url_gra like '% mazda %' and url like '%amazda%';


select top 100
	url_key,
	url,
	case when (dir <> '' or page <> '') and query <> '' then host+'/'+dir+page+'?'+passivemaster.dbo.maskpii(query)
		else case when  (dir <> '' or page <> '') and query = '' then host+'/'+dir+page
		else host end end as url_maskpii,
	url_gra
from googleauto.ref.url_gra where url_gra like '%dodge%' and url like '%ram%';
-- END OF FILE --